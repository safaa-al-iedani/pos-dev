Imports HBCG_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization

'JAM added 20Oct2014
Imports DevExpress.Web.ASPxEditors
Imports System.Data


Partial Class LCOMCommissionLookup
    Inherits POSBasePage

    Public Const gs_version As String = "Version 3.0"
    
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen. This form allows the connected user to view their commission. They can choose by delivery document,
    '           written OR finalized date ranges. The results displayed are a summarized view of delivery document(s) which can be drilled
    '           into to see delivery document details.
    '   1.0.1:  Minor bug fixes during testing:
    '           (1) maximum date set on end dates past current day's date - needs to be max of yesterday's date
    '   2.0:    Added manager and national access to commission lookup:
    '           (1) manager can view all non-terminated salespeople within their own store
    '           (2) national access (regional managers, corporate, etc.) can view all non-terminated salespeople within their own company
    '               and select specific stores. Two new drop-downs added - store and salesperson
    '   2.1:    Bug fixes
    '           (1) when a national employee, selected store code and salesperson not retained when reviewing document details - FIXED
    '           (2) during the above fix, found another bug where salesperson code tied to the document details link was empty - FIXED
    '   2.2:    Bug fix - when national employee goes "back to summary" from details page, sometimes an exception occurs. Fix appears
    '           to be related to some code which was trying to set the salesperson code prior to the drop-down list being populated. 
    '   2.3:    When sale order is having more than 10 records, next page(after 10 records) is not shown properly in detail page
    '   2.4:    Addressed the Performance issue as suggested by JDA. 
    '           Added a new disclaimer text and increased the size of the text.
    '           Increased the search days limit from 7 to 14.
    '   2.5:    Employee list combo sorted using employee first name, 
    '           commented the line which makes home_store_code session variable as null, which is not required.
    '   3.0:    POS+ French conversion
    '           Labels, button captions, tooltip, gridview captions are converted into French strings.
    '------------------------------------------------------------------------------------------------------------------------------------

    Public Const gn_minimumCalendarDaysLimit As Integer = 7   '7 days
    Public Const gn_maximumCalendarDaysLimit As Integer = 7   '7 days

    Public Const gs_minimumDateRestriction As String = "10-May-2015"
    Public Const gn_searchCalendarDaysLimit As Integer = 14   '14 days

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           ResetAllFields()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Resets/clears all fields.
    '   Called by:      btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub ResetAllFields()

        lbl_resultsInfo.Visible = False
        lbl_resultsInfo.Text = ""

        'txt_salesperson_code.Text = ""
        'txt_del_doc_num.Text = ""

        'Session("HOME_STORE_CD") = ""
        Session("SELECTED_STORE_CD") = ""
        Session("SALESPERSON_CODE") = ""
        Session("DEL_DOC_NUM") = ""
        Session("WRITTEN_DATE_START") = ""
        Session("WRITTEN_DATE_END") = ""
        Session("FINALIZED_DATE_START") = ""
        Session("FINALIZED_DATE_END") = ""

        Session("dt_commissionSummary") = New DataSet

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   SECURITY
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           isStoreManager()
    '   Created by:     John Melenko
    '   Date:           Jun2015
    '   Description:    Checks if user is a store manager
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function isStoreManager() As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim b_Continue As Boolean = False

        Dim s_companyCode As String = Session("CO_CD")
        Dim s_employeeCode As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        If (Not (s_companyCode Is Nothing)) AndAlso s_companyCode.isNotEmpty Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            SQL = "SELECT count(*) AS n_SLM_counter "
            SQL = SQL & "FROM emp$emp_tp ee "
            SQL = SQL & "WHERE ee.emp_tp_cd IN ( 'SLM' ) "
            SQL = SQL & "AND ee.emp_cd = UPPER(:x_employeeCode) "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
            objsql.Parameters(":x_employeeCode").Value = s_employeeCode

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then

                    Dim n_SLM_count As Integer = Mydatareader.Item("n_SLM_counter").ToString
                    If (n_SLM_count > 0) Then
                        b_Continue = True
                    End If
                    Mydatareader.Close()

                End If

            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

        End If

        Return b_Continue

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           isNationalEmployee()
    '   Created by:     John Melenko
    '   Date:           Jun2015
    '   Description:    Checks if user is a national employee (regional, corporate, etc.)
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function isNationalEmployee() As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim b_Continue As Boolean = False

        Dim s_companyCode As String = Session("CO_CD")
        Dim s_employeeCode As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        If (Not (s_companyCode Is Nothing)) AndAlso s_companyCode.isNotEmpty Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            SQL = "SELECT count(*) AS n_count "
            SQL = SQL & "FROM emp e "
            SQL = SQL & "WHERE e.home_store_cd IN ( '00', '10' ) "
            SQL = SQL & "AND e.emp_cd = UPPER(:x_employeeCode) "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
            objsql.Parameters(":x_employeeCode").Value = s_employeeCode

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then

                    Dim n_count As Integer = Mydatareader.Item("n_count").ToString
                    If (n_count > 0) Then
                        b_Continue = True
                    End If
                    Mydatareader.Close()

                End If

            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

        End If

        Return b_Continue

    End Function


    '------------------------------------------------------------------------------------------------------------------------------------
    '   POPULATE DROP DOWN LISTS
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateEmployeeCodes()
    '   Created by:     John Melenko
    '   Date:           Jun2015
    '   Description:    Procedure which populates the salesperson employee drop-down list
    '   Called by:      Page_Load()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateEmployeeCodes(p_homeStoreCode As String)

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim ds As New DataSet

        Dim s_companyCode As String = Session("CO_CD")
        Dim s_employeeCode As String = Session("EMP_CD")
        'Dim s_store_code As String = Session("STORE_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "] store code [" + s_store_code + "]")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        If (Not (s_companyCode Is Nothing)) AndAlso s_companyCode.isNotEmpty Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            If Not String.IsNullOrEmpty(Trim(p_homeStoreCode.ToString())) Then
                '            If (p_isManager) Then

                SQL = "SELECT e.emp_cd AS employee_code, e.fname || ' ' || e.lname AS employee_name, e.emp_cd || ' - ' || e.fname || ' ' || e.lname AS full_emp_name_desc "
                SQL = SQL & "FROM emp e, emp_slsp es "
                SQL = SQL & "WHERE e.emp_cd = es.emp_cd "
                SQL = SQL & "AND es.slsp_dept_cd <> 'TERM' "
                SQL = SQL & "AND e.home_store_cd = :x_homeStoreCode ORDER BY e.fname"

            Else

                SQL = "SELECT e.emp_cd AS employee_code, e.fname || ' ' || e.lname AS employee_name, e.emp_cd || ' - ' || e.fname || ' ' || e.lname AS full_emp_name_desc "
                SQL = SQL & "FROM emp e "
                SQL = SQL & "WHERE e.emp_cd = :x_employeeCode "

            End If

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            If Not String.IsNullOrEmpty(Trim(p_homeStoreCode.ToString())) Then

                objsql.Parameters.Add(":x_homeStoreCode", OracleType.VarChar)
                objsql.Parameters(":x_homeStoreCode").Value = p_homeStoreCode

            Else

                objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
                objsql.Parameters(":x_employeeCode").Value = s_employeeCode

            End If

            Try
                With objsql
                    .Connection = conn
                    .CommandText = SQL
                End With

                conn.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
                oAdp.Fill(ds)

                With txt_salesperson_code
                    'With txt_employee_code
                    .DataSource = ds
                    .DataValueField = "employee_code"
                    .DataTextField = "full_emp_name_desc"
                    .DataBind()

                End With

                conn.Close()

            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            Dim s_salesperson_code As String = Session("SALESPERSON_CODE")

            If (s_salesperson_code Is Nothing) OrElse s_salesperson_code.isEmpty Then
                s_salesperson_code = s_employeeCode
                Session("SALESPERSON_CODE") = s_salesperson_code
            Else
                'set default choice to current salesperson
            End If
            txt_salesperson_code.SelectedValue.Equals(s_salesperson_code)

        End If

    End Sub


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateStoreCodes()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Procedure which populates the store code drop-down list.
    '   Called by:      Page_Load()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim ds As New DataSet

        Dim s_companyCode As String = Session("CO_CD")
        Dim s_employeeCode As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        If (Not (s_companyCode Is Nothing)) AndAlso s_companyCode.isNotEmpty Then

            SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc "
            SQL = SQL & "FROM store s, co_grp cg "
            SQL = SQL & "WHERE s.co_cd = cg.co_cd "
            SQL = SQL & "AND cg.co_grp_cd = ( select co_grp_cd from co_grp where co_cd = UPPER(:x_companyCode) ) "
            SQL = SQL & "ORDER BY store_cd"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, connErp)
            objsql.Parameters.Add(":x_companyCode", OracleType.VarChar)
            objsql.Parameters(":x_companyCode").Value = s_companyCode

            Try
                With objsql
                    .Connection = connErp
                    .CommandText = SQL
                End With

                connErp.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
                oAdp.Fill(ds)

                With txt_store_cd
                    .DataSource = ds
                    .DataValueField = "store_cd"
                    .DataTextField = "full_store_desc"
                    .DataBind()
                End With

                connErp.Close()

            Catch ex As Exception
                connErp.Close()
                Throw
            End Try

            'Dim s_homeStoreCode As String = getEmployeeHomeStore()
            'txt_store_cd.SelectedValue.Equals(s_homeStoreCode)
            txt_store_cd.SelectedValue.Equals(Session("HOME_STORE_CD"))

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           getEmployeeHomeStore()
    '   Created by:     John Melenko
    '   Date:           Jun2015
    '   Description:    Retrieves the connected user's home store code
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function getEmployeeHomeStore() As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim SQL As String
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim s_homeStoreCode As String = ""
        Dim s_companyCode As String = Session("CO_CD")
        Dim s_employeeCode As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        Dim b_Continue As Boolean = False
        Dim s_errorMessage As String = ""

        If (s_companyCode Is Nothing OrElse s_companyCode.isEmpty) Then
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Company code is required - your login did not retrieve a proper company code
            s_errorMessage = Resources.CustomMessages.MSG0041
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return s_homeStoreCode

        End If

        If (s_employeeCode Is Nothing OrElse s_employeeCode.isEmpty) Then
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Employee code is required
            s_errorMessage = Resources.CustomMessages.MSG0043
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return s_homeStoreCode

        End If

        SQL = ""
        SQL = "SELECT e_mgr.home_store_cd AS home_store_cd "
        SQL = SQL & "FROM emp e_mgr "
        SQL = SQL & "WHERE e_mgr.emp_cd = :x_employeeCode "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
        objsql.Parameters(":x_employeeCode").Value = s_employeeCode

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read Then
                Try
                    'nothing to do
                    s_homeStoreCode = Mydatareader.Item("home_store_cd").ToString

                Catch ex As Exception
                    conn.Close()
                End Try

                Mydatareader.Close()
            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Session("HOME_STORE_CD") = s_homeStoreCode

        Return s_homeStoreCode

    End Function


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveCommissionSummary()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Retrieves and populates the 
    '   Called by:      RetrieveItemInfo() - if the item is found/valid
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function RetrieveCommissionSummary(p_salesperson_code As String, p_del_doc_num As String, d_written_date_start As String, d_written_date_end As String, d_finalized_date_start As String, d_finalized_date_end As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim SQL As String
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim s_salesperson_code As String = p_salesperson_code.ToUpper
        Dim s_del_doc_num As String = p_del_doc_num.ToUpper

        Dim s_companyCode As String = Session("CO_CD")
        Dim s_employeeCode As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        Dim b_Continue As Boolean = False
        Dim s_errorMessage As String = ""

        If (String.IsNullOrEmpty(Trim(s_companyCode.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Company code is required - your login did not retrieve a proper company code
            s_errorMessage = Resources.CustomMessages.MSG0041
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return False

        End If

        If (String.IsNullOrEmpty(Trim(s_salesperson_code.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = s_errorMessage = "ERROR: Salesperson is required"
            s_errorMessage = Resources.CustomMessages.MSG0044
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return False

        End If

        If (String.IsNullOrEmpty(Trim(s_del_doc_num.ToString()))) _
        AndAlso (String.IsNullOrEmpty(Trim(d_written_date_start.ToString()))) AndAlso (String.IsNullOrEmpty(Trim(d_finalized_date_start.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: One of the following is required: (1) delivery document, (2) written date range OR (3) finalized date range
            s_errorMessage = Resources.CustomMessages.MSG0045
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return False

        End If

        'if either written start or end have a value, then make sure both do...
        If (Not (String.IsNullOrEmpty(Trim(d_written_date_start.ToString()))) OrElse Not (String.IsNullOrEmpty(Trim(d_written_date_end.ToString())))) Then

            If (String.IsNullOrEmpty(Trim(d_written_date_start.ToString())) OrElse String.IsNullOrEmpty(Trim(d_written_date_end.ToString()))) Then

                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                's_errorMessage = "ERROR: Written date range is required
                s_errorMessage = Resources.CustomMessages.MSG0046
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")
                Return False

            End If

        End If

        'if either finalized start or end have a value, then make sure both do...
        If (Not (String.IsNullOrEmpty(Trim(d_finalized_date_start.ToString()))) OrElse Not (String.IsNullOrEmpty(Trim(d_finalized_date_end.ToString())))) Then

            If (String.IsNullOrEmpty(Trim(d_finalized_date_start.ToString())) OrElse String.IsNullOrEmpty(Trim(d_finalized_date_end.ToString()))) Then

                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                's_errorMessage = "ERROR: Finalized date range is required
                s_errorMessage = Resources.CustomMessages.MSG0047
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")
                Return False

            End If

        End If

        SQL = ""
        SQL = SQL & "SELECT del_doc_num, stat_cd, ord_tp_cd, finalized_date, "
        SQL = SQL & "TO_CHAR(SUM(ext_price),'999,999.99') AS sales$, TO_CHAR(SUM(NVL(commission$,0)),'999,999.99') AS commission$, TO_CHAR(SUM(NVL(spiff$,0)),'999,999.99') AS spiff$, "
        SQL = SQL & "TO_CHAR(SUM(NVL(spiff$,0) + NVL(commission$,0)),'999,999.99') AS total$ "
        SQL = SQL & "FROM ( "

        '--------------------------------------------------------
        '--QUERY 1 - SO_EMP_SLSP_CD1
        '--------------------------------------------------------
        SQL = SQL & "SELECT /*+ ordered */ s.del_doc_num, s.stat_cd, s.ord_tp_cd, TO_CHAR( s.final_dt, 'DD-MON-YYYY' ) AS finalized_date, sl.so_emp_slsp_cd1 AS emp_slsp_cd, sl.pct_of_sale1 AS pct_of_sale, "
        SQL = SQL & "es.store_cd, sl.del_doc_ln#, DECODE( s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) AS so_line_qty, "
        SQL = SQL & "sl.itm_cd, i.itm_tp_cd, i.des as itm_des, sl.out_cd, "

        SQL = SQL & "ROUND( DECODE( s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * ( sl.pct_of_sale1 / 100 ), 2) AS unit_price, "
        SQL = SQL & "ROUND( sl.qty * DECODE( s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale1 / 100), 2) AS ext_price, "
        SQL = SQL & "sl.comm_cd, MIN(scr.ret_pct) AS retail_percentage, "
        SQL = SQL & "ROUND( sl.qty * DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (MIN(scr.ret_pct) / 100) * (sl.pct_of_sale1 / 100), 2) AS commission$, "
        SQL = SQL & "ROUND( DECODE( s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) * sl.spiff * (sl.pct_of_sale1 / 100), 2) AS spiff$ "

        SQL = SQL & "FROM so s, so_ln sl, itm i, emp_slsp es, store s, co_grp cg, store_comm_ret scr "
        SQL = SQL & "WHERE s.del_doc_num = sl.del_doc_num "
        SQL = SQL & "AND i.itm_cd = sl.itm_cd "
        SQL = SQL & "AND sl.so_emp_slsp_cd1 = es.emp_cd "
        SQL = SQL & "AND scr.comm_cd = sl.comm_cd "
        SQL = SQL & "AND s.store_cd = s.so_store_cd "
        SQL = SQL & "AND s.co_cd = cg.co_cd "

        SQL = SQL & "AND cg.co_grp_cd = ( select co_grp_cd from co_grp where co_cd = UPPER(:x_companyCode) ) "
        SQL = SQL & "AND scr.store_cd = s.so_store_cd "

        SQL = SQL & "AND sl.VOID_FLAG = 'N' "

        '--------------------------------------------------------
        '-- will be ONE of the following:
        If Not (String.IsNullOrEmpty(Trim(d_finalized_date_start.ToString()))) Then
            SQL = SQL & "AND s.final_dt BETWEEN :x_finalized_date_start AND :x_finalized_date_end "
            SQL = SQL & "AND s.stat_cd = 'F' "

            'OR
        ElseIf Not (String.IsNullOrEmpty(Trim(d_written_date_start.ToString()))) Then
            SQL = SQL & "AND s.so_wr_dt BETWEEN :x_written_date_start AND :x_written_date_end "
            SQL = SQL & "AND s.stat_cd <> 'V' "
        End If
        '--------------------------------------------------------

        If Not (String.IsNullOrEmpty(Trim(s_salesperson_code.ToString()))) Then
            SQL = SQL & "AND sl.so_emp_slsp_cd1 = :x_salesperson_code "
        End If
        If Not (String.IsNullOrEmpty(Trim(s_del_doc_num.ToString()))) Then
            SQL = SQL & "AND sl.del_doc_num = :x_del_doc_num "
        End If

        SQL = SQL & "GROUP BY s.del_doc_num, s.stat_cd, s.ord_tp_cd, TO_CHAR( s.final_dt, 'DD-MON-YYYY' ), sl.so_emp_slsp_cd1, sl.pct_of_sale1, es.store_cd, sl.del_doc_ln#, "
        SQL = SQL & "           DECODE(s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty), "
        SQL = SQL & "           sl.itm_cd, i.itm_tp_cd, i.des, sl.out_cd, "
        SQL = SQL & "           ROUND(DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale1 / 100), 2), "
        SQL = SQL & "           ROUND(sl.qty * DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale1 / 100), 2), "
        SQL = SQL & "           sl.comm_cd, sl.qty, sl.unit_prc, "
        SQL = SQL & "           ROUND(DECODE(s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) * sl.spiff * (sl.pct_of_sale1 / 100), 2) "
        SQL = SQL & " "
        SQL = SQL & "UNION "
        SQL = SQL & " "

        '--------------------------------------------------------
        '--QUERY 2 - SO_EMP_SLSP_CD2
        '--------------------------------------------------------

        SQL = SQL & "SELECT /*+ ordered */ s.del_doc_num, s.stat_cd, s.ord_tp_cd, TO_CHAR( s.final_dt, 'DD-MON-YYYY' ) AS finalized_date, sl.so_emp_slsp_cd2 AS emp_slsp_cd, sl.pct_of_sale2 AS pct_of_sale, "
        SQL = SQL & "es.store_cd, sl.del_doc_ln#, DECODE( s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) AS so_line_qty, "
        SQL = SQL & "sl.itm_cd, i.itm_tp_cd, i.des as itm_des, sl.out_cd, "
        SQL = SQL & "ROUND( DECODE( s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * ( sl.pct_of_sale2 / 100 ), 2) AS unit_price, "
        SQL = SQL & "ROUND( sl.qty * DECODE( s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale2 / 100), 2) AS ext_price, "
        SQL = SQL & "sl.comm_cd, MIN(scr.ret_pct) AS retail_percentage, "
        SQL = SQL & "ROUND( sl.qty * DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (MIN(scr.ret_pct) / 100) * (sl.pct_of_sale2 / 100), 2) AS commission$, "
        SQL = SQL & "ROUND( DECODE( s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) * sl.spiff * (sl.pct_of_sale2 / 100), 2) AS spiff$ "

        SQL = SQL & "FROM so s, so_ln sl, itm i, emp_slsp es, store s, co_grp cg, store_comm_ret scr "
        SQL = SQL & "WHERE s.del_doc_num = sl.del_doc_num "
        SQL = SQL & "AND i.itm_cd = sl.itm_cd "
        SQL = SQL & "AND sl.so_emp_slsp_cd2 = es.emp_cd "
        SQL = SQL & "AND sl.so_emp_slsp_cd2 IS NOT NULL "
        SQL = SQL & "AND scr.comm_cd = sl.comm_cd "
        SQL = SQL & "AND s.store_cd = s.so_store_cd "
        SQL = SQL & "AND s.co_cd = cg.co_cd "

        SQL = SQL & "AND cg.co_grp_cd = ( select co_grp_cd from co_grp where co_cd = UPPER(:x_companyCode) ) "
        SQL = SQL & "AND scr.store_cd = s.so_store_cd "

        SQL = SQL & "AND sl.VOID_FLAG = 'N' "

        '--------------------------------------------------------
        '-- will be ONE of the following:
        If Not (String.IsNullOrEmpty(Trim(d_finalized_date_start.ToString()))) Then

            SQL = SQL & "AND s.final_dt BETWEEN :x_finalized_date_start AND :x_finalized_date_end "
            SQL = SQL & "AND s.stat_cd = 'F' "

            'OR
        ElseIf Not (String.IsNullOrEmpty(Trim(d_written_date_start.ToString()))) Then

            SQL = SQL & "AND s.so_wr_dt BETWEEN :x_written_date_start AND :x_written_date_end "
            SQL = SQL & "AND s.stat_cd <> 'V' "
        End If
        '--------------------------------------------------------

        If Not (String.IsNullOrEmpty(Trim(s_salesperson_code.ToString()))) Then
            SQL = SQL & "AND sl.so_emp_slsp_cd2 = :x_salesperson_code "
        End If
        If Not (String.IsNullOrEmpty(Trim(s_del_doc_num.ToString()))) Then
            SQL = SQL & "AND sl.del_doc_num = :x_del_doc_num "
        End If

        SQL = SQL & "GROUP BY s.del_doc_num, s.stat_cd, s.ord_tp_cd, TO_CHAR( s.final_dt, 'DD-MON-YYYY' ), sl.so_emp_slsp_cd2, sl.pct_of_sale2, es.store_cd, sl.del_doc_ln#, "
        SQL = SQL & "           DECODE(s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty), "
        SQL = SQL & "           sl.itm_cd, i.itm_tp_cd, i.des, sl.out_cd, "
        SQL = SQL & "           ROUND(DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale2 / 100), 2), "
        SQL = SQL & "           ROUND(sl.qty * DECODE(s.ord_tp_cd, 'SAL', sl.unit_prc, 'MDB', sl.unit_prc, -sl.unit_prc) * (sl.pct_of_sale2 / 100), 2), "
        SQL = SQL & "           sl.comm_cd, sl.qty, sl.unit_prc, "
        SQL = SQL & "           ROUND(DECODE(s.ord_tp_cd, 'SAL', sl.qty, 'MDB', sl.qty, -sl.qty) * sl.spiff * (sl.pct_of_sale2 / 100), 2) "

        SQL = SQL & ") "
        SQL = SQL & "GROUP BY del_doc_num, stat_cd, ord_tp_cd, finalized_date "


        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)
        objsql.Parameters.Add(":x_companyCode", OracleType.VarChar)
        objsql.Parameters(":x_companyCode").Value = s_companyCode

        If Not (String.IsNullOrEmpty(Trim(s_salesperson_code.ToString()))) Then
            objsql.Parameters.Add(":x_salesperson_code", OracleType.VarChar)
            objsql.Parameters(":x_salesperson_code").Value = s_salesperson_code
        End If
        If Not (String.IsNullOrEmpty(Trim(s_del_doc_num.ToString()))) Then
            objsql.Parameters.Add(":x_del_doc_num", OracleType.VarChar)
            objsql.Parameters(":x_del_doc_num").Value = s_del_doc_num
        End If

        'if finalized date is being searched, then
        If Not (String.IsNullOrEmpty(Trim(d_finalized_date_start.ToString()))) Then
            objsql.Parameters.Add(":x_finalized_date_start", OracleType.VarChar)
            objsql.Parameters(":x_finalized_date_start").Value = d_finalized_date_start

            objsql.Parameters.Add(":x_finalized_date_end", OracleType.VarChar)
            objsql.Parameters(":x_finalized_date_end").Value = d_finalized_date_end

        ElseIf Not (String.IsNullOrEmpty(Trim(d_written_date_start.ToString()))) Then
            'if written date is being searched, then
            objsql.Parameters.Add(":x_written_date_start", OracleType.VarChar)
            objsql.Parameters(":x_written_date_start").Value = d_written_date_start

            objsql.Parameters.Add(":x_written_date_end", OracleType.VarChar)
            objsql.Parameters(":x_written_date_end").Value = d_written_date_end

        End If

        Dim ds_GridViewALLDATA As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
        oAdp.Fill(ds_GridViewALLDATA)


        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read Then
                Try
                    'nothing to do
                Catch ex As Exception
                    conn.Close()
                End Try

                Mydatareader.Close()
            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Dim dv_GridViewALLDATA As DataView = New DataView(ds_GridViewALLDATA.Tables(0))

        'Now build a new data table based on the above data
        Dim dt_commissionSummary As DataTable = New DataTable

        dt_commissionSummary.Columns.Add("DEL_DOC_NUM", Type.GetType("System.String"))
        dt_commissionSummary.Columns.Add("STAT_CD", Type.GetType("System.String"))
        dt_commissionSummary.Columns.Add("FINALIZED_DATE", Type.GetType("System.String"))
        dt_commissionSummary.Columns.Add("ORD_TP_CD", Type.GetType("System.String"))
        dt_commissionSummary.Columns.Add("SALES$", Type.GetType("System.String"))
        dt_commissionSummary.Columns.Add("COMMISSION$", Type.GetType("System.String"))
        dt_commissionSummary.Columns.Add("SPIFF$", Type.GetType("System.String"))
        dt_commissionSummary.Columns.Add("TOTAL$", Type.GetType("System.String"))

        For Each drv_GridViewALLDATA As DataRowView In dv_GridViewALLDATA

            Dim dr_newDataRow As DataRow = dt_commissionSummary.NewRow()
            dr_newDataRow("DEL_DOC_NUM") = drv_GridViewALLDATA("DEL_DOC_NUM")
            dr_newDataRow("STAT_CD") = drv_GridViewALLDATA("STAT_CD")
            dr_newDataRow("FINALIZED_DATE") = drv_GridViewALLDATA("FINALIZED_DATE")
            dr_newDataRow("ORD_TP_CD") = drv_GridViewALLDATA("ORD_TP_CD")

            dr_newDataRow("SALES$") = drv_GridViewALLDATA("SALES$")
            dr_newDataRow("COMMISSION$") = drv_GridViewALLDATA("COMMISSION$")
            dr_newDataRow("SPIFF$") = drv_GridViewALLDATA("SPIFF$")
            dr_newDataRow("TOTAL$") = drv_GridViewALLDATA("TOTAL$")

            dt_commissionSummary.Rows.Add(dr_newDataRow)

        Next

        'Session("dt_commissionSummary") = New DataSet
        Session("dt_commissionSummary") = Nothing
        If (dv_GridViewALLDATA.Count > 0) Then
            Session("dt_commissionSummary") = dt_commissionSummary
        End If

        'moved binding to "GridViewCommissionSummary_DataBinding"
        'GridViewCommissionSummary.DataSource = dt_commissionSummary
        GridViewCommissionSummary.DataBind()
        'GridViewCommissionSummary_DataBinding()

        If b_Continue Then
            'do nothing
        End If

        Return True

    End Function


    '------------------------------------------------------------------------------------------------------------------------------------
    '   OTHER
    '------------------------------------------------------------------------------------------------------------------------------------

    'Private Sub PrintRows(ByVal dataSet As DataSet)
    '    Dim table As DataTable
    '    Dim row As DataRow
    '    Dim column As DataColumn
    '    ' For each table in the DataSet, print the row values. 
    '    For Each table In dataSet.Tables
    '        For Each row In table.Rows
    '            For Each column In table.Columns
    '                Console.WriteLine(row(column))
    '            Next column
    '        Next row
    '    Next table
    'End Sub


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PrintDataView()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Debugging procedure for dataviews
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    'Private Shared Sub PrintDataView(dv As DataView)
    '    ' Printing first DataRowView to demo that the row in the first index of the DataView changes depending on sort and filters
    '    'Console.WriteLine("First DataRowView value is '{0}'", dv(0)("priority"))
    '    MsgBox("First DataRowView value is '{0}' " + dv(0)("priority") + "; prc_cd: " + dv(0)("prc_cd") + "; des: " + dv(0)("des"))

    '    ' Printing all DataRowViews 
    '    For Each drv As DataRowView In dv

    '        'Console.WriteLine(vbTab & " {0}", drv("priority"))
    '        'MsgBox(" {0}" + drv("priority"))
    '        MsgBox(" {0} " + drv("priority") + "; prc_cd: " + drv("prc_cd") + "; des: " + drv("des"))

    '    Next
    'End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AlertMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display alert messages
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub AlertMessage(p_messageText)
        MsgBox(p_messageText, MsgBoxStyle.Exclamation, Title:="Error")
        'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           DisplayMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display messages - can toggle between a label or message box
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub DisplayMessage(p_messageText, p_messageType, p_msgBoxStyle)

        Dim color_fontColor As Color
        Dim msgBoxStyle As MsgBoxStyle

        If (p_msgBoxStyle = "ERROR") Then
            color_fontColor = Color.Red
            msgBoxStyle = msgBoxStyle.Critical
        ElseIf (p_msgBoxStyle = "WARNING") Then
            color_fontColor = Color.Orange
            msgBoxStyle = msgBoxStyle.Exclamation
        ElseIf (p_msgBoxStyle = "INFO") Then
            color_fontColor = Color.Green
            msgBoxStyle = msgBoxStyle.Information
        End If

        If (p_messageType = "LABEL") Then
            lbl_resultsInfo.Text = p_messageText
            lbl_resultsInfo.ForeColor = color_fontColor
            lbl_resultsInfo.Visible = True
        ElseIf (p_messageType = "MSGBOX") Then
            MsgBox(p_messageText, msgBoxStyle)
            'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_Load()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form - minor changes
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_resultsInfo.Visible = False

        If Not IsPostBack Then

            If Request("back_from_details") = "Y" Then

                txt_store_cd.Text = Session("SELECTED_STORE_CD")
                ' = Session("HOME_STORE_CD")
                'txt_salesperson_code.Text = Session("SALESPERSON_CODE")

                txt_del_doc_num.Text = Session("DEL_DOC_NUM")
                dateEdit_written_date_start.Text = Session("WRITTEN_DATE_START")
                dateEdit_written_date_end.Text = Session("WRITTEN_DATE_END")
                dateEdit_finalized_date_start.Text = Session("FINALIZED_DATE_START")
                dateEdit_finalized_date_end.Text = Session("FINALIZED_DATE_END")

                If Not (String.IsNullOrEmpty(Trim(dateEdit_written_date_start.Text.ToString()))) Then
                    dateEdit_written_date_end.Enabled = True

                    dateEdit_finalized_date_start.Enabled = False
                    dateEdit_finalized_date_end.Enabled = False

                ElseIf Not (String.IsNullOrEmpty(Trim(dateEdit_finalized_date_start.Text.ToString()))) Then
                    dateEdit_finalized_date_end.Enabled = True

                    dateEdit_written_date_start.Enabled = False
                    dateEdit_written_date_end.Enabled = False
                Else

                    dateEdit_written_date_start.Enabled = True
                    dateEdit_written_date_end.Enabled = False

                    dateEdit_finalized_date_start.Enabled = True
                    dateEdit_finalized_date_end.Enabled = False

                End If

                GridViewCommissionSummary.DataSource = Session("dt_commissionSummary")
                GridViewCommissionSummary.DataBind()

            Else

                'JAM: populate the store codes drop-down list
                'PopulateStoreCodes()

                'Dim s_arrival_date As String = txt_po_arrival_date_soonest.Text
                'Dim d_arrival_date As Date
                'Dim n_buffer_days As Integer = Convert.ToDecimal(txt_po_buffer_days.Text)

                ''d_arrival_date = Date.ParseExact(s_arrival_date, "dd-MMM-yyyy", Nothing)  'System.Globalization.CultureInfo.InvariantCulture
                '                d_arrival_date = Date.Parse(s_arrival_date)
                '                d_arrival_date = d_arrival_date.AddDays(n_buffer_days)
                '                txt_po_arrival_date_soonest.Text = d_arrival_date.ToString("dd-MMM-yyyy")

                'Dim s_employee_code As String = Session("EMP_CD")
                'txt_salesperson_code.Text = s_employee_code
                'txt_salesperson_code.ReadOnly = True

                dateEdit_written_date_end.Enabled = False
                dateEdit_finalized_date_end.Enabled = False

            End If

            Dim d_minimumDateRestriction As Date = Date.Parse(gs_minimumDateRestriction) 'cannot search prior 10May2015
            Dim d_dateToday As Date = Date.Today
            'Dim d_mindate As Date = Date.Today
            'Dim d_maxdate As Date = Date.Today

            '(1) default all dates to empty
            'dateEdit_written_date_start.Date = d_dateToday
            'dateEdit_written_date_end.Date = d_dateToday
            'dateEdit_finalized_date_start.Date = d_dateToday
            'dateEdit_finalized_date_end.Date = d_dateToday

            '(2) default restrict END dates to a date range of "YESTERDAY minus 7 (mindate) and YESTERDAY (maxdate)" - can't search beyond yesterday OR more than 7 days
            dateEdit_written_date_end.MaxDate = d_dateToday.AddDays(-1)
            dateEdit_finalized_date_end.MaxDate = d_dateToday.AddDays(-1)

            'dateEdit_written_date_end.MinDate = dateEdit_written_date_end.MaxDate.AddDays(-gn_searchCalendarDaysLimit)
            'dateEdit_finalized_date_end.MinDate = dateEdit_finalized_date_end.MaxDate.AddDays(-gn_searchCalendarDaysLimit)
            dateEdit_written_date_end.MinDate = d_minimumDateRestriction.AddDays(+gn_searchCalendarDaysLimit)
            dateEdit_finalized_date_end.MinDate = d_minimumDateRestriction.AddDays(+gn_searchCalendarDaysLimit)

            '(3) default restrict START dates to a date range of "10May2015 (business requirement) and YESTERDAY (maxdate)" - can't search prior 10May2015 OR beyond yesterday
            dateEdit_written_date_start.MinDate = d_minimumDateRestriction
            dateEdit_finalized_date_start.MinDate = d_minimumDateRestriction

            dateEdit_written_date_start.MaxDate = d_dateToday.AddDays(-1)
            dateEdit_finalized_date_start.MaxDate = d_dateToday.AddDays(-1)

            dateEdit_written_date_start.TimeSectionProperties.Visible = False
            'chkShowTimeSection.Checked()
            dateEdit_written_date_start.UseMaskBehavior = True
            'chkMaskBehavior.Checked()
            dateEdit_written_date_start.EditFormatString = "dd MMM yyyy"
            'GetFormatString(cbDateEditFormatString.Value)
            dateEdit_written_date_start.DisplayFormatString = "dd MMM yyyy"
            'GetFormatString(cbDateDisplayFormatString.Value)
            dateEdit_written_date_start.ClientEnabled = True

            dateEdit_written_date_end.TimeSectionProperties.Visible = False
            dateEdit_written_date_end.UseMaskBehavior = True
            dateEdit_written_date_end.EditFormatString = "dd MMM yyyy"
            dateEdit_written_date_end.DisplayFormatString = "dd MMM yyyy"
            dateEdit_written_date_end.ClientEnabled = True

            dateEdit_finalized_date_start.TimeSectionProperties.Visible = False
            dateEdit_finalized_date_start.UseMaskBehavior = True
            dateEdit_finalized_date_start.EditFormatString = "dd MMM yyyy"
            dateEdit_finalized_date_start.DisplayFormatString = "dd MMM yyyy"
            dateEdit_finalized_date_start.ClientEnabled = True

            dateEdit_finalized_date_end.TimeSectionProperties.Visible = False
            dateEdit_finalized_date_end.UseMaskBehavior = True
            dateEdit_finalized_date_end.EditFormatString = "dd MMM yyyy"
            dateEdit_finalized_date_end.DisplayFormatString = "dd MMM yyyy"
            dateEdit_finalized_date_end.ClientEnabled = True

            If (isNationalEmployee()) Then

                'MsgBox("hello national employee - creating store AND salesperson drop-down lists")

                lbl_store_cd.Visible = True
                txt_store_cd.Visible = True
                PopulateStoreCodes()
                txt_store_cd.Enabled = True

            End If

            If (isStoreManager()) Then
                '    'MsgBox("hello store manager - creating salesperson drop-down list")
                '    PopulateEmployeeCodes(p_isManager:=True)

                Dim s_homeStoreCode As String = Session("HOME_STORE_CD")
                Dim s_selectedStoreCode As String = Session("SELECTED_STORE_CD")

                'If (String.IsNullOrEmpty(Trim(s_homeStoreCode.ToString()))) Then
                's_homeStoreCode = getEmployeeHomeStore()
                'End If
                If (s_homeStoreCode Is Nothing) OrElse s_homeStoreCode.isEmpty Then
                    s_homeStoreCode = getEmployeeHomeStore()
                End If

                Dim s_useStoreCode As String = s_homeStoreCode

                'If Not (String.IsNullOrEmpty(Trim(s_selectedStoreCode.ToString()))) Then
                's_useStoreCode = s_selectedStoreCode
                'End If

                'If (s_selectedStoreCode Is Nothing) OrElse s_selectedStoreCode.isEmpty Then
                If (Not (s_selectedStoreCode Is Nothing)) AndAlso s_selectedStoreCode.isNotEmpty Then
                    s_useStoreCode = s_selectedStoreCode
                End If
                PopulateEmployeeCodes(p_homeStoreCode:=s_useStoreCode)

            Else

                'Session("HOME_STORE_CD") = ""
                PopulateEmployeeCodes(p_homeStoreCode:="")

            End If

            Dim s_employee_code As String = Session("EMP_CD")
            Dim s_selectedSalespersonCode As String = Session("SALESPERSON_CODE")

            If (s_selectedSalespersonCode Is Nothing) OrElse s_selectedSalespersonCode.isEmpty Then
                s_selectedSalespersonCode = s_employee_code
            End If
            txt_salesperson_code.Text = s_selectedSalespersonCode

        End If

        'txt_salesperson_code.ReadOnly = True
        lbl_versionInfo.Text = gs_version

    End Sub

    'JAM added 20Oct2014
    'Protected Function GetFormatString(ByVal value As Object) As String
    '    If value Is Nothing Then
    '        Return String.Empty
    '    Else
    '        Return value.ToString()
    '    End If
    'End Function
    'JAM added 20Oct2014

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Clear_Click()
    '   Created by:     John Melenko
    '   Date:           May2015
    '   Description:    Copied from IPQ and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_salesperson_code.Enabled = True
        txt_del_doc_num.Enabled = True
        ResetAllFields()

        Response.Redirect("LCOMCommissionLookup.aspx")

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Search_Click()
    '   Created by:     PRE-EXISTING
    '   Date:           May2015
    '   Description:    Copied from IPQ and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click

        ResetAllFields()
        '"HOU7F"

        'Session("HOME_STORE_CD") = txt_store_cd.Text
        Session("SELECTED_STORE_CD") = txt_store_cd.Text
        Session("SALESPERSON_CODE") = txt_salesperson_code.Text
        Session("DEL_DOC_NUM") = txt_del_doc_num.Text
        Session("WRITTEN_DATE_START") = dateEdit_written_date_start.Text
        Session("WRITTEN_DATE_END") = dateEdit_written_date_end.Text
        Session("FINALIZED_DATE_START") = dateEdit_finalized_date_start.Text
        Session("FINALIZED_DATE_END") = dateEdit_finalized_date_end.Text

        RetrieveCommissionSummary(txt_salesperson_code.Text, txt_del_doc_num.Text, dateEdit_written_date_start.Text, dateEdit_written_date_end.Text, dateEdit_finalized_date_start.Text, dateEdit_finalized_date_end.Text)
        GridViewCommissionSummary.Visible = True

    End Sub

    Protected Sub StoreCodeChangedHandler() Handles txt_store_cd.TextChanged

        'MsgBox("store code changed to " + txt_store_cd.Text)


        'Dim ds_empty As DataSet = New DataSet
        'txt_salesperson_code.DataSource = ds_empty
        'txt_salesperson_code.DataBind()

        PopulateEmployeeCodes(txt_store_cd.Text)

    End Sub

    Protected Sub WrittenDateStartChangedHandler() Handles dateEdit_written_date_start.DateChanged

        If (String.IsNullOrEmpty(Trim(dateEdit_written_date_start.Text.ToString()))) Then

            'date changed to be empty, so clear out written end date and disable; re-enable finalized start date
            dateEdit_written_date_end.Text = ""
            dateEdit_written_date_end.Enabled = False

            dateEdit_finalized_date_start.Enabled = True

        Else
            'MsgBox("date changed to " + dateEdit_written_date_start.Text)
            dateEdit_written_date_end.Enabled = True

            dateEdit_written_date_end.MinDate = dateEdit_written_date_start.Date
            dateEdit_written_date_end.MaxDate = dateEdit_written_date_start.Date.AddDays(+gn_searchCalendarDaysLimit)

            Dim d_dateToday As Date = Date.Today
            If dateEdit_written_date_end.MaxDate >= d_dateToday Then
                dateEdit_written_date_end.MaxDate = d_dateToday.AddDays(-1)
            End If
            dateEdit_written_date_end.Date = dateEdit_written_date_end.MaxDate

            dateEdit_finalized_date_start.Enabled = False
            dateEdit_finalized_date_end.Enabled = False

        End If

    End Sub

    Protected Sub FinalizedDateStartChangedHandler() Handles dateEdit_finalized_date_start.DateChanged

        If (String.IsNullOrEmpty(Trim(dateEdit_finalized_date_start.Text.ToString()))) Then

            'date changed to be empty, so clear out finalized end date and disable; re-enable written start date
            dateEdit_finalized_date_end.Text = ""
            dateEdit_finalized_date_end.Enabled = False

            dateEdit_written_date_start.Enabled = True

        Else
            'MsgBox("date changed to " + dateEdit_finalized_date_start.Text)
            dateEdit_finalized_date_end.Enabled = True

            dateEdit_finalized_date_end.MinDate = dateEdit_finalized_date_start.Date
            dateEdit_finalized_date_end.MaxDate = dateEdit_finalized_date_start.Date.AddDays(+gn_searchCalendarDaysLimit)

            Dim d_dateToday As Date = Date.Today
            If dateEdit_finalized_date_end.MaxDate >= d_dateToday Then
                dateEdit_finalized_date_end.MaxDate = d_dateToday.AddDays(-1)
            End If
            dateEdit_finalized_date_end.Date = dateEdit_finalized_date_end.MaxDate

            dateEdit_written_date_start.Enabled = False
            dateEdit_written_date_end.Enabled = False

        End If

    End Sub


    Protected Sub GridViewCommissionSummary_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridViewCommissionSummary.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If Not IsNothing(e.GetValue("DEL_DOC_NUM").ToString()) Then
            Dim ASPxMenu2 As DevExpress.Web.ASPxMenu.ASPxMenu = TryCast(GridViewCommissionSummary.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "ASPxMenu2"), DevExpress.Web.ASPxMenu.ASPxMenu)

            Dim n_index As Integer = 0

            If Not IsNothing(ASPxMenu2) Then

                Dim s_selectedSalespersonCode As String = txt_salesperson_code.Text.ToString()
                If (s_selectedSalespersonCode Is Nothing) OrElse s_selectedSalespersonCode.isEmpty Then
                    s_selectedSalespersonCode = Session("SALESPERSON_CODE")
                End If

                ASPxMenu2.RootItem.Items(0).NavigateUrl = "LCOMCommissionDetails.aspx?SALESPERSON_CODE=" & s_selectedSalespersonCode & "&DEL_DOC_NUM=" & e.GetValue("DEL_DOC_NUM").ToString()
                'MsgBox("position=" + e.GetValue("COLUMN_ROWID").ToString())
                'MsgBox("blah " + e.KeyValue)

                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'ASPxMenu2.RootItem.Items(0).ToolTip = "Select the row and view the details of this document"
                ASPxMenu2.RootItem.Items(0).ToolTip = GetLocalResourceObject("tool_tip_root_item.Text").ToString()
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            End If

        End If
    End Sub

    Protected Sub GridViewCommissionSummary_DataBinding(ByVal sender As Object, ByVal e As EventArgs)

        Dim dt_commissionSummary As DataTable = Session("dt_commissionSummary")

        If IsNothing(dt_commissionSummary) Then
            GridViewCommissionSummary.DataSource = Nothing
            GridViewCommissionSummary.Enabled = False
        Else
            GridViewCommissionSummary.DataSource = dt_commissionSummary
            GridViewCommissionSummary.Enabled = True

        End If

    End Sub


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_PreInit()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AddError()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub AddError(ByVal errors As Dictionary(Of GridViewColumn, String), ByVal column As GridViewColumn, ByVal errorText As String)
        If errors.ContainsKey(column) Then
            Return
        End If
        errors(column) = errorText
    End Sub

End Class