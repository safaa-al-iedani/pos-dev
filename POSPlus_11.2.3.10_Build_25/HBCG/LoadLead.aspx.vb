Imports System.Data.OracleClient
Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization


Partial Class LoadLead
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Load_ve_lead(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview1.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            '           objSql.CommandText = "select distinct itm_cd, ve_cd, mnr_cd, des,  " &
            '              "decode(itm_cd,null,'Item Code cannot be null.', ' ') || " &
            '             "decode(sign(length(trim(des))-30), 1, 'Item Description exceeds 30 characters', ' ') err_msg " &
            '            "from ITM_GENERATION_EXT a "
            ' June 27 as per Nick
            objSql.CommandText = "select distinct ve_cd,mnr_cd,store_grp_cd,days_lead,days_transit, " &
                "decode(ve_cd,null,'ve_cd cannot be null.', ' ') || " &
                "decode(sign(length(trim(ve_cd))-4), 1, 've_cd exceeds 4 characters', ' ') || " &
                "decode(sign(length(trim(store_grp_cd))-3), 1, 'store_grp_cd exceeds 3 characters', ' ') || " &
                "decode(sign(length(trim(days_lead))-4), 1, 'days_lead exceeds 4 numbers', ' ') || " &
                "decode(sign(length(trim(days_transit))-4), 1, 'days_transit exceeds 4 numbers', ' '  ) err_msg " &
                "from VE_LEAD_GEN_EXT a "


            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                            Button1.Visible = False
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg1.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process.Visible = True
                lbl_msg1.Text = "No Error Found - Please Process the file."
                lbl_msg1.ForeColor = Color.Blue

            Else
                btn_process.Visible = False
                Button1.Visible = True
                lbl_msg1.ForeColor = Color.Red
                lbl_msg1.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            btn_process.Visible = False
            Button1.Visible = True
            lbl_msg1.ForeColor = Color.Red
            lbl_msg1.Text = "Errors found. Please correct the data and upload file again."
            Throw
        End Try

    End Sub

    Protected Sub Load_Itm_Lead(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview2.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select distinct itm_cd,store_grp_cd,days_lead,days_transit,ear_avl_dt, " &
                 "decode(sign(length(trim(store_grp_cd))-3), 1, 'store_grp_cd exceeds 3 characters', ' ') || " &
                "decode(sign(length(trim(days_lead))-4), 1, 'days_lead exceeds 4 numbers', ' ') || " &
                "decode(sign(length(trim(days_transit))-4), 1, 'days_transit exceeds 4 numbers', ' '  ) err_msg " &
                "from ITM_LEAD_GEN_EXT  "



            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview2.DataSource = ds
                Gridview2.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        '       If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then

                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then



                            Button1.Visible = False
                            Button2.Visible = True
                            Button3.Visible = False
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview2.DataSource = ds
                Gridview2.DataBind()
                lbl_msg2.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process_itm_lead.Visible = True
                btn_process.Visible = False
                btn_process_FCM_Lead.Visible = False
                lbl_msg2.Text = "No Error Found - Please Process the file."
                lbl_msg2.ForeColor = Color.Blue
            Else
                btn_process_itm_lead.Visible = False
                Button2.Visible = True
                lbl_msg1.Text = ""
                lbl_msg3.Text = ""
                lbl_msg2.ForeColor = Color.Red
                lbl_msg2.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            btn_process_itm_lead.Visible = False
            Button2.Visible = True
            lbl_msg2.ForeColor = Color.Red
            lbl_msg2.Text = "Errors found. Please correct the data and upload file again."
            Throw
        End Try

    End Sub

    Protected Sub Load_FCM_Lead(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview3.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            
            objSql.CommandText = "select distinct ve_cd,days_lead,' ' err_msg " &
                                 "from FCM_LEAD_GEN_EXT  "


            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview3.DataSource = ds
                Gridview3.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then

                            Button1.Visible = False
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview3.DataSource = ds
                Gridview3.DataBind()
                lbl_msg3.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process_FCM_Lead.Visible = True
                btn_process_itm_lead.Visible = False
                btn_process.Visible = False
                lbl_msg3.Text = "No Error Found - Please Process the file."
                lbl_msg3.ForeColor = Color.Blue

            Else
                btn_process_FCM_Lead.Visible = False
                Button3.Visible = True
                lbl_msg1.Text = ""
                lbl_msg2.Text = ""
                lbl_msg3.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            btn_process_FCM_Lead.Visible = False
            Button3.Visible = True
            lbl_msg3.Text = "Errors found. Please correct the data and upload file again."
            Throw
        End Try

    End Sub

    Protected Sub Process_Ve_Lead(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview1.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select ve_cd,mnr_cd,store_grp_cd,days_lead,days_transit, NVL(err_msg, ' ') err_msg  from ve_lead_creation  "

            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg1.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process.Visible = False
                lbl_msg1.Text = "Vendor lead is successfully uploaded."
                lbl_msg1.ForeColor = Color.Blue
            Else
                btn_process.Visible = False
                Button1.Visible = True
                lbl_msg2.Text = ""
                lbl_msg3.Text = ""
                lbl_msg1.ForeColor = Color.Red
                lbl_msg1.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception
            lbl_msg1.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Protected Sub Process_Itm_Lead(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview2.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select itm_cd,store_grp_cd,days_lead,ear_avl_dt,days_transit, NVL(err_msg, ' ') err_msg  from itm_lead_creation  "

            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview2.DataSource = ds
                Gridview2.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview2.DataSource = ds
                Gridview2.DataBind()
                lbl_msg2.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process_itm_lead.Visible = False
                lbl_msg2.Text = "Item lead is successfully uploaded."
                lbl_msg2.ForeColor = Color.Blue
            Else
                btn_process_itm_lead.Visible = False
                Button2.Visible = True
                lbl_msg2.ForeColor = Color.Red
                lbl_msg2.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception
            lbl_msg2.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Protected Sub Process_FCM_Lead(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0

            ds = New DataSet

            Gridview3.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "select ve_cd, days_lead , NVL(err_msg, ' ') err_msg  from fcm_lead_creation  "

            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview3.DataSource = ds
                Gridview3.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("err_msg")) Then
                            err_cnt = 1
                        End If
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview3.DataSource = ds
                Gridview3.DataBind()
                lbl_msg3.Text = "No data found. Please upload the file."
            End If

            If err_cnt = 0 Then
                btn_process.Visible = False
                lbl_msg3.Text = "FCM lead is successfully uploaded."
                lbl_msg3.ForeColor = Color.Blue
            Else
                btn_process.Visible = False
                Button3.Visible = True
                lbl_msg3.ForeColor = Color.Red
                lbl_msg3.Text = "Errors found. Please correct the data and upload file again."
            End If

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception
            lbl_msg3.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Protected Sub btn_process_Click(sender As Object, e As EventArgs) Handles btn_process.Click
        ' Validate and process
        lbl_msg1.Text = ""
        lbl_msg2.Text = ""
        lbl_msg3.Text = ""
        Dim result = validate_ve_lead()

        If result = "Y" Then
            Process_Ve_Lead(sender, e)
        End If

        If result = "N" Then
            lbl_msg1.ForeColor = Color.Red
            lbl_msg1.Text = "Errors found. Please correct the data and upload file again."
        End If

    End Sub

    Protected Sub btn_process_itm_lead_Click(sender As Object, e As EventArgs) Handles btn_process_itm_lead.Click
        ' Validate and process
        lbl_msg1.Text = ""
        lbl_msg2.Text = ""
        lbl_msg3.Text = ""
        Dim result = validate_items_lead()

        If result = "Y" Then
            Process_Itm_lead(sender, e)
        End If

        If result = "N" Then
            lbl_msg2.ForeColor = Color.Red
            lbl_msg2.Text = "Errors found. Please correct the data and upload file again."
        End If

    End Sub

    Protected Sub btn_process_FCM_Lead_Click(sender As Object, e As EventArgs) Handles btn_process_FCM_Lead.Click
        ' Validate and process
        lbl_msg1.Text = ""
        lbl_msg2.Text = ""
        lbl_msg3.Text = ""
        Dim result = validate_FCM_lead()
        result = "Y"
        If result = "Y" Then
            Process_FCM_Lead(sender, e)
        End If

        If result = "N" Then
            lbl_msg3.ForeColor = Color.Red
            lbl_msg3.Text = "Errors found. Please correct the data and upload file again."
            '  Process_FCM_Lead(sender, e)
        End If

    End Sub

    Protected Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        ' Clear
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)

    End Sub

    Public Function validate_ve_lead() As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim emp_init As String = Session("emp_init")
        Dim x As Exception
        Dim Ds As New DataSet()

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "std_upload_ve_lead.validate_items"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_emp_init", OracleType.VarChar)).Value = emp_init

            myCMD.ExecuteNonQuery()

            'Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

            'MyDA.Fill(Ds)
            'myCMD.Cancel()
            'myCMD.Dispose()

            Return "Y"

        Catch x
            objConnection.Close()
            objConnection.Dispose()
            Label1.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function validate_items_lead() As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim emp_init As String = Session("emp_init")
        Dim x As Exception
        Dim Ds As New DataSet()

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "std_upload_itm_lead.validate_items"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_emp_init", OracleType.VarChar)).Value = emp_init

            myCMD.ExecuteNonQuery()


            Return "Y"

        Catch x
            objConnection.Close()
            objConnection.Dispose()
            Label2.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function validate_FCM_lead() As String
        Dim p_flag As String = "N"
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim emp_init As String = Session("emp_init")

        Dim x As Exception
        Dim Ds As New DataSet()

        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "std_upload_FCM_lead.validate_items"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_emp_init", OracleType.VarChar)).Value = emp_init
            myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.ReturnValue
            myCMD.ExecuteNonQuery()



            myCMD.ExecuteNonQuery()

            If Not IsDBNull(myCMD.Parameters("p_ind").Value) Then
                p_flag = myCMD.Parameters("p_ind").Value
            End If



        Catch x
            objConnection.Close()
            objConnection.Dispose()
            Label3.Text = "Error: " & x.Message.ToString
            Return "N"
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return p_flag
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, _
      ByVal e As System.EventArgs)
        Try
            lbl_msg1.Text = ""
            lbl_msg2.Text = ""
            lbl_msg3.Text = ""
            Label1.Text = ""
            Label2.Text = ""
            Label3.Text = ""
            Gridview1.DataSource = ""
            Gridview2.DataSource = ""
            Gridview3.DataSource = ""
            Gridview1.Visible = True
            Gridview2.Visible = False
            Gridview3.Visible = False
            If FileUpload1.HasFile Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload1.FileName)
                Dim filePath = FileUpload1.PostedFile.FileName
                Dim fileNameUnix = "ve_lead_gen.csv"

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")
                Dim env As String
                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live" 'RPTGEN is an exception
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                If (fileExt = ".csv") Then
                    Try
                        'FileUpload1.SaveAs("C:\Uploads\" & FileUpload1.FileName)
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadVeLead\done\" & FileUpload1.FileName)
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadVeLead\done\" & FileUpload1.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                        Label1.Text = "File name: " & _
                          FileUpload1.PostedFile.FileName & "<br>" & _
                          "File Size: " & _
                          FileUpload1.PostedFile.ContentLength

                        Dim userId As String = Nothing
                        Dim password As String = Nothing
                        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                        Dim dsn_text As String = ""
                        Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                        Dim x As Integer
                        For x = LBound(conStr) To UBound(conStr) - 1
                            If LCase(Left(conStr(x), 3)) = "uid" Then
                                userId = conStr(x)
                                userId = userId.Substring(userId.IndexOf("=") + 1)
                            End If
                            If LCase(Left(conStr(x), 3)) = "pwd" Then
                                password = conStr(x)
                                password = password.Substring(password.IndexOf("=") + 1)
                            End If
                        Next

                        'Upload file using FTP
                        'UploadFile("C:\Uploads\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadVeLead\done\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)

                        ' Log the file name and size in AUDIT_LOG
                        'theSystemBiz.SaveAuditLogComment("LOAD_ITEMS_BRICK", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))

                        Label1.Text = Label1.Text & "<br>" & _
                            "Upload Successful. Please Validate and Process."

                        Button1.Visible = False

                        ' Display data from external table
                        Load_ve_lead(sender, e)

                    Catch ex As Exception
                        Label1.Text = "ERROR: " & ex.Message.ToString()
                        Exit Sub
                    End Try
                Else
                    Label1.Text = "Only .csv files allowed!"
                End If
            Else
                Label1.Text = "You have not specified a file."
            End If

        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("LOAD_VE_LEAD", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label1.Text = Label1.Text & "<br>" & _
                            "Upload failed. Please check the errors."
        End Try

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lbl_msg1.Text = ""
            lbl_msg2.Text = ""
            lbl_msg3.Text = ""
            Label1.Text = ""
            Label2.Text = ""
            Label3.Text = ""
            Gridview1.DataSource = ""
            Gridview2.DataSource = ""
            Gridview3.DataSource = ""
            Gridview1.Visible = False
            Gridview2.Visible = True
            Gridview3.Visible = False
            If FileUpload2.HasFile Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload2.FileName)
                Dim filePath = FileUpload2.PostedFile.FileName
                Dim fileNameUnix = "itm_lead_gen.csv"

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")
                Dim env As String
                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live" 'RPTGEN is an exception
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                If (fileExt = ".csv") Then
                    Try
                        'FileUpload1.SaveAs("C:\Uploads\" & FileUpload1.FileName)
                        FileUpload2.SaveAs("\\10.128.11.60\Upload\LoadItmLead\done\" & FileUpload2.FileName)
                        FileUpload2.SaveAs("\\10.128.11.60\Upload\LoadItmLead\done\" & FileUpload2.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                        Label2.Text = "File name: " & _
                          FileUpload2.PostedFile.FileName & "<br>" & _
                          "File Size: " & _
                          FileUpload2.PostedFile.ContentLength

                        Dim userId As String = Nothing
                        Dim password As String = Nothing
                        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                        Dim dsn_text As String = ""
                        Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                        Dim x As Integer
                        For x = LBound(conStr) To UBound(conStr) - 1
                            If LCase(Left(conStr(x), 3)) = "uid" Then
                                userId = conStr(x)
                                userId = userId.Substring(userId.IndexOf("=") + 1)
                            End If
                            If LCase(Left(conStr(x), 3)) = "pwd" Then
                                password = conStr(x)
                                password = password.Substring(password.IndexOf("=") + 1)
                            End If
                        Next

                        'Upload file using FTP
                        'UploadFile("C:\Uploads\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadItmLead\done\" & FileUpload2.FileName, unixPath & fileNameUnix, userId, password)

                        ' Log the file name and size in AUDIT_LOG
                        'theSystemBiz.SaveAuditLogComment("LOAD_ITEMS", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))

                        Label2.Text = Label2.Text & "<br>" & _
                            "Upload Successful. Please Validate and Process."

                        Button2.Visible = False

                        ' Display data from external table
                        Load_Itm_Lead(sender, e)

                    Catch ex As Exception
                        Label2.Text = "ERROR: " & ex.Message.ToString()
                        Exit Sub
                    End Try
                Else
                    Label2.Text = "Only .csv files allowed!"
                End If
            Else
                Label2.Text = "You have not specified a file."
            End If

        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("Load_Itm_Lead", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label2.Text = Label1.Text & "<br>" & _
                            "Upload failed. Please check the errors."
        End Try

    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lbl_msg1.Text = ""
            lbl_msg2.Text = ""
            lbl_msg3.Text = ""
            Label1.Text = ""
            Label2.Text = ""
            Label3.Text = ""
            Gridview1.DataSource = ""
            Gridview2.DataSource = ""
            Gridview3.DataSource = ""
            Gridview1.Visible = False
            Gridview2.Visible = False
            Gridview3.Visible = True
            If FileUpload3.HasFile Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload3.FileName)
                Dim filePath = FileUpload3.PostedFile.FileName
                Dim fileNameUnix = "fcm_lead_gen.csv"

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")
                Dim env As String
                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live" 'RPTGEN is an exception
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                If (fileExt = ".csv") Then
                    Try
                        'FileUpload1.SaveAs("C:\Uploads\" & FileUpload1.FileName)
                        FileUpload3.SaveAs("\\10.128.11.60\Upload\LoadFCMLead\done\" & FileUpload3.FileName)
                        FileUpload3.SaveAs("\\10.128.11.60\Upload\LoadFCMLead\done\" & FileUpload3.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                        Label3.Text = "File name: " & _
                          FileUpload3.PostedFile.FileName & "<br>" & _
                          "File Size: " & _
                          FileUpload3.PostedFile.ContentLength

                        Dim userId As String = Nothing
                        Dim password As String = Nothing
                        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                        Dim dsn_text As String = ""
                        Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                        Dim x As Integer
                        For x = LBound(conStr) To UBound(conStr) - 1
                            If LCase(Left(conStr(x), 3)) = "uid" Then
                                userId = conStr(x)
                                userId = userId.Substring(userId.IndexOf("=") + 1)
                            End If
                            If LCase(Left(conStr(x), 3)) = "pwd" Then
                                password = conStr(x)
                                password = password.Substring(password.IndexOf("=") + 1)
                            End If
                        Next

                        'Upload file using FTP
                        'UploadFile("C:\Uploads\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadFCMLead\done\" & FileUpload3.FileName, unixPath & fileNameUnix, userId, password)

                        ' Log the file name and size in AUDIT_LOG
                        'theSystemBiz.SaveAuditLogComment("LOAD_WARR_BRICK", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))

                        Label3.Text = Label3.Text & "<br>" & _
                            "Upload Successful. Please Validate and Process."

                        Button3.Visible = False

                        ' Display data from external table
                        Load_FCM_Lead(sender, e)

                    Catch ex As Exception
                        Label3.Text = "ERROR: " & ex.Message.ToString()
                        Exit Sub
                    End Try
                Else
                    Label3.Text = "Only .csv files allowed!"
                End If
            Else
                Label3.Text = "You have not specified a file."
            End If

        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("LoadFCMLead", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label3.Text = Label1.Text & "<br>" & _
                            "Upload failed. Please check the errors."
        End Try

    End Sub

    'HOW TO USE:

    ' Upload file using FTP
    'UploadFile("c:\UploadFile.doc", "ftp://FTPHostName/UploadPath/UploadFile.doc", "UserName", "Password")

End Class
