<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Settlement_Promo_Plans.aspx.vb" Inherits="Settlement_Promo_Plans" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td height="100%" align="left" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Promotion Plans:" Font-Bold="True">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right" valign="middle">
                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Finance Company:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_finance_company" runat="server" AutoPostBack="True" ValueType="System.String"
                                Width="190px" IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Text="No Filter" Value="NONE" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Filter:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="DropDownList1" runat="server" AutoPostBack="True"
                                IncrementalFilteringMode ="StartsWith">
                                <Items>
                                    <dx:ListEditItem Text="No Filter" Value="NONE" />
                                    <dx:ListEditItem Text="Active" Value="Active" />
                                    <dx:ListEditItem Text="Available" Value="Available" />
                                    <dx:ListEditItem Text="Expired" Value="Expired" />
                                    <dx:ListEditItem Text="Pending" Value="Pending" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            <dx:ASPxButton ID="Button2" runat="server" Text="Update from E1">
                            </dx:ASPxButton>
                        </td>
                        <td colspan="3">
                            <dx:ASPxButton ID="Button1" runat="server" Text="Update Promo Plans from ADS" Visible="False">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <hr />
                <br />
                <table border="0" cellpadding="0" cellspacing="0" width="95%">
                    <tr>
                        <td align="center" valign="middle" style="height: 266px">
                            <asp:DataGrid ID="GridView1" runat="server" AutoGenerateColumns="False" Height="200px"
                                Width="100%" DataKeyField="PROMO_CD">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Promo">
                                        <HeaderTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel40" runat="server" Text="Promo">
                                            </dx:ASPxLabel>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PROMO_CD") %>'>
                                            </dx:ASPxLabel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Vendor Desc">
                                        <HeaderTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel41" runat="server" Text="Vendor Desc">
                                            </dx:ASPxLabel>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LONG_DESC") %>'>
                                            </dx:ASPxLabel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Description">
                                        <HeaderTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel42" runat="server" Text="Description">
                                            </dx:ASPxLabel>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <dx:ASPxTextBox ID="TextBox1" AutoPostBack="True" OnTextChanged="Update_Text" runat="server"
                                                Width="200px" Text='<%# DataBinder.Eval(Container, "DataItem.SHORT_DESC") %>'>
                                            </dx:ASPxTextBox>
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="Begin Date:">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text='<%# FormatDateTime(DataBinder.Eval(Container, "DataItem.EFF_DT"),vbShortDate) %>'>
                                                        </dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="End Date:">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text='<%# FormatDateTime(DataBinder.Eval(Container, "DataItem.END_DT"),vbShortDate) %>'>
                                                        </dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Defer Date:">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DEFER_DT") %>'>
                                                        </dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Defer Months:">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxTextBox ID="txt_defer_mths" runat="server" AutoPostBack="True" Width="40px"
                                                            OnTextChanged="Update_defer_mths" Text='<%# DataBinder.Eval(Container, "DataItem.DEFER_MONTHS") %>'>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="EFF_DT" HeaderText="Begin Date" DataFormatString="{0:MM/dd/yyyy}"
                                        Visible="False" />
                                    <asp:BoundColumn DataField="END_DT" HeaderText="End Date" DataFormatString="{0:MM/dd/yyyy}"
                                        Visible="False" />
                                    <asp:BoundColumn DataField="DEFER_DT" HeaderText="Defer Date" DataFormatString="{0:MM/dd/yyyy}"
                                        Visible="False" />
                                    <asp:TemplateColumn HeaderText="Defer Months" Visible="False">
                                        <ItemTemplate>
                                            &nbsp;
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" />
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="UFM w/DP">
                                        <HeaderTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel43" runat="server" Text="UFM w/DP">
                                            </dx:ASPxLabel>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <dx:ASPxTextBox ID="txt_invoice_wdr" runat="server" AutoPostBack="True" Width="40px"
                                                OnTextChanged="Update_invoice_wdr" Text='<%# DataBinder.Eval(Container, "DataItem.INVOICE_NAME_WDR") %>'>
                                            </dx:ASPxTextBox>
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" />
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="UFM">
                                        <HeaderTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel44" runat="server" Text="UFM">
                                            </dx:ASPxLabel>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <dx:ASPxTextBox ID="txt_invoice_no_wdr" runat="server" AutoPostBack="True" Width="40px"
                                                OnTextChanged="Update_invoice_no_wdr" Text='<%# DataBinder.Eval(Container, "DataItem.INVOICE_NAME") %>'>
                                            </dx:ASPxTextBox>
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" />
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="WDR_CODE" Visible="False" />
                                    <asp:BoundColumn DataField="PLAN_ACTIVE" Visible="False" />
                                    <asp:TemplateColumn HeaderText="Status">
                                        <HeaderTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel45" runat="server" Text="Status">
                                            </dx:ASPxLabel>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <dx:ASPxLabel ID="lbl_plan_status" runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DP?">
                                        <HeaderTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel46" runat="server" Text="DP?">
                                            </dx:ASPxLabel>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="Update_WDR" />
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Active">
                                        <HeaderTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel47" runat="server" Text="Active?">
                                            </dx:ASPxLabel>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="True" OnCheckedChanged="Update_Active" />
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="PROMO_CD" Visible="False" />
                                </Columns>
                                <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False"></AlternatingItemStyle>
                                <HeaderStyle Font-Bold="True" ForeColor="Black" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" />
                                <EditItemStyle />
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" />
                                <SelectedItemStyle BackColor="Silver" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" />
                            </asp:DataGrid>
                        </td>
                        <td align="left" valign="middle" bgcolor="#ffffff" style="height: 266px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="middle" style="height: 19px">
                <asp:Label ID="lbl_header" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <dx:ASPxLabel ID="ASPxLabelUpdateStatus" runat="server" Text="" Font-Bold="True" ></dx:ASPxLabel>
</asp:Content>
