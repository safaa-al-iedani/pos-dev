<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comments.aspx.vb" Inherits="comments"
    MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <asp:Label ID="lblSalesComments" runat="server" Text="<%$ Resources:LibResources, Label509 %>"></asp:Label>
        <%--<asp:Button ID="lucy_btn_cmnt" runat="server" OnClientClick="lucy_add_predined_cmnt" Text="Add Predefined Comments" ForeColor ="#9933FF"/>--%>
        <asp:Button ID="lucy_btn_cmnt" runat="server" Text="<%$ Resources:LibResources, Label18 %>" ForeColor ="#9933FF"/>
        <dx:ASPxMemo ID="txtSalesComments" AutoPostBack="true" OnTextChanged="Sales_Changed" runat="server" Height="98px" Width="549px">
        </dx:ASPxMemo>
        
        &nbsp;<br />
        <asp:Label ID="lblDeliveryComments" runat="server" Height="17px" Text="<%$ Resources:LibResources, Label153 %>"
            Width="170px"></asp:Label>
        <br />
        <dx:ASPxMemo ID="txtDeliveryComments" AutoPostBack="true" OnTextChanged="Delivery_Changed" runat="server" Height="98px" Width="549px">
        </dx:ASPxMemo>
        &nbsp;<br />
        <asp:Label ID="lblARComments" runat="server" Height="17px" Text="<%$ Resources:LibResources, Label7 %>"
            Width="170px"></asp:Label>
        <br />
        <dx:ASPxMemo ID="txtARComments" AutoPostBack="true" OnTextChanged="AR_Changed" runat="server" Height="98px" Width="549px">
        </dx:ASPxMemo>
        <br />
        &nbsp;
        <br />
        <dx:ASPxButton ID="btnSubmit" runat="server" Text="Save Comments" Width="112px" Visible="false">
        </dx:ASPxButton>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" Width="515px" Modal="True"
        PopupAction="None" HeaderText="Swatch Check Out" CloseAction="CloseButton" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <table width="100%">
                    <tr>
                        <td align="left" style="text-align: center" width="40%">
                            <img src="images/icons/Symbol-Exclamation.png" style="width: 171px; height: 171px" />
                        </td>
                        <td class="style5">
                            To complete the swatch/sample check-out process, please complete the following items:
                            <ul>
                                <li>Enter fabric or sample comments </li>
                                <li>Enter customer information</li>
                                <li>Provide payment information</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
</asp:Content>
