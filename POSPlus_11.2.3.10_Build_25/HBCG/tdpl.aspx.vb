﻿Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.OracleClient
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Microsoft.VisualBasic
Imports System.Web.SessionState
Imports HBCG_Utils
Imports SalesUtils
Imports System.Math
Partial Class tdpl
    Inherits POSBasePage
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Sep 29 not allow payment if no emp code
        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If

        Session("td_instpmt") = "Y"

        ' Daniela June 24
        CreateCustomer()

        If Not IsDBNull(Session("cust_cd")) And Not isEmpty(Session("cust_cd")) Then
            tdpl_cust_cd.Text = Session("cust_cd")
            Session("tdpl_cust_cd") = Session("cust_cd")
        End If
        ' Daniela April 10 to clear previos card for new transactions
        If Not IsPostBack = True Then
            Session("tdpl_acct_num") = ""
        End If
        If IsPostBack = True Then
            If Not IsDBNull(Session("tdpl_acct_num")) And Not isEmpty(Session("tdpl_acct_num")) Then
                tdpl_acct_num.Text = Session("tdpl_acct_num")
            End If
        End If

    End Sub

    Protected Sub TextBox1_acct_TextChanged(sender As Object, e As EventArgs) Handles tdpl_acct_num.TextChanged

        Dim v_valid As String
        Session("tdpl_acct_num") = tdpl_acct_num.Text
        v_valid = valid_acct()

    End Sub

    Protected Sub Button1_click(sender As Object, e As EventArgs) Handles Button1.Click
        'cancel - exit

        Session("td_instpmt") = "N"
        Txt_msg.Text = ""

        Dim str_amt As String = 0
        Dim str_ivc_cd As String = "" 'Session("ivc_cd")
        Dim URL_String As String
        Dim str_csh_pss As String = Session("csh_pss")
        Dim str_csh_drw As String = Session("csh_drw")
        Dim str_store_cd As String = Session("store_cd")

        Session("cust_cd") = ""
        Session("tdpl_cust_cd") = ""
        Session("tdpl_acct_num") = ""

        URL_String = "?ip_ord_total=" & str_amt & "&refresh=true" & "&ip_csh_pass=" & str_csh_pss & "&ip_store_cd=" & str_store_cd & "&ip_cust_cd=" & tdpl_cust_cd.Text & "&ip_ivc_cd=" & str_ivc_cd & "&ip_csh_drw=" & str_csh_drw
        Response.Redirect("IndependentPaymentProcessing.aspx" & URL_String)

    End Sub

    Protected Sub button2_click(sender As Object, e As EventArgs) Handles Button2.Click
        'process payment 
        ' Daniela april 10 clear error message before process
        txt_msg.Text = ""

        If valid_acct() = "N" Then
            Exit Sub
        End If

        If valid_cust() = "N" Then
            Exit Sub
        End If

        process_paymnt()


    End Sub
    Public Sub process_paymnt()

        Dim str_space As String = " "
        Dim str_amt As String = 0
        Dim str_ivc_cd As String

        Session("tdpl_acct_num") = tdpl_acct_num.Text
        Session("tdpl_cust_cd") = UCase(tdpl_cust_cd.Text)
        Session("td_instpmt") = "Y"

        If InStr(Session("ivc_cd"), "TD PL") > 0 Or InStr(Session("IVC_CD"), "TDPL") Or InStr(Session("IVC_CD"), "BRICK") > 0 Then
            str_ivc_cd = "BRICK PAYMENT"
        Else
            str_ivc_cd = Session("ivc_cd")
        End If

        Dim URL_String As String
        Dim str_csh_pss As String = Session("csh_pss")
        Dim str_csh_drw As String = Session("csh_drw")
        Dim str_store_cd As String = Session("store_cd")

        URL_String = "?ip_ord_total=" & str_amt & "&refresh=true" & "&ip_csh_pass=" & str_csh_pss & "&ip_store_cd=" & str_store_cd & "&ip_cust_cd=" & UCase(tdpl_cust_cd.Text) & "&ip_ivc_cd=" & str_ivc_cd & "&ip_csh_drw=" & str_csh_drw
        'URL_String = "?ip_ord_total=" & str_amt & "&refresh=true" & "&ip_csh_pass=" & str_csh_pss & "&cbo_store_cd=" & str_store_cd & "&ip_cust_cd=" & tdpl_cust_cd.Text & "&ip_ivc_cd=" & str_ivc_cd & "&cbo_csh_drw=" & str_csh_drw
        Response.Redirect("IndependentPaymentProcessing.aspx" & URL_String)

    End Sub

    Public Function valid_acct()

        Dim v_valid As String = ""

        Session("tdpl_acct_num") = tdpl_acct_num.Text

        If IsDBNull(tdpl_acct_num.Text) = True Then

            txt_msg.Text = "Enter Account Number"
            tdpl_acct_num.Focus()
            v_valid = "N"
        Else
            If Left(tdpl_acct_num.Text, 5) = "60061" Then
                If Len(tdpl_acct_num.Text) = 16 Then

                    tdpl_cust_cd.Focus()
                    v_valid = "Y"
                Else
                    txt_msg.Text = "Invalid TD Account Number"

                    tdpl_acct_num.Focus()
                    v_valid = "N"
                End If
            Else
                txt_msg.Text = "Invalid TD Account Number"
                Session("tdpl_acct_num") = ""
                tdpl_acct_num.Focus()
                v_valid = "N"
            End If
        End If

        Return v_valid

    End Function


    Public Function valid_cust() As String

        Dim v_valid As String = ""

        If IsDBNull(tdpl_cust_cd.Text) = True Or isEmpty(tdpl_cust_cd.Text) = True Then
            Session("tdpl_cust_cd") = ""
            Txt_msg.Text = "Enter Customer Code"
            tdpl_cust_cd.Focus()
            v_valid = "N"
        End If

        Dim conn As OracleConnection
        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        tdpl_cust_cd.Text = UCase(tdpl_cust_cd.Text)

        sql = "select cust_cd as CUST_CD from cust "
        sql = sql & "where cust_cd = '" & UCase(tdpl_cust_cd.Text) & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Session("tdpl_cust_cd") = UCase(MyDatareader2.Item("CUST_CD").ToString())
                v_valid = "Y"
            Else
                Session("tdpl_cust_cd") = ""
                Txt_msg.Text = "ERROR - Customer does not exist - please try again"
                v_valid = "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()



        Return v_valid

    End Function

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        ' Daniela april 10 clear error message before process
        txt_msg.Text = ""

        Dim connString As String
        Dim objConnection As OracleConnection
        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_rt1 As String
        Dim l_rt2 As String
        Dim l_tr1 As String
        Dim l_tr2 As String
        Dim l_crn As String
        Dim l_exp As String
        Dim l_cst As String
        Dim l_dsp As String

        Dim v_term_id As String = ""
        'Dim v_term_id As String = LeonsBiz.get_term_id(Session("clientip"))

        ' sabrina feb26
        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            txt_msg.Text = "No designated pinpad"
            Exit Sub
        End If

        'Sep 29 hide swipe and process button
        Button3.Enabled = False
        Button2.Enabled = False

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_brk_pos1.sendsubTp7_swipe"
            myCMD.CommandType = CommandType.StoredProcedure

            'myCMD.CommandTimeout = 300 'Daniela timeout 5 min

            myCMD.Parameters.Add(New OracleParameter("p_trantp", OracleType.VarChar)).Value = "SW"
            myCMD.Parameters.Add(New OracleParameter("p_subtp", OracleType.VarChar)).Value = "7"
            myCMD.Parameters.Add(New OracleParameter("p_displn1", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_displn2", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_rsvd", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_term_id", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cst", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 500).Direction = ParameterDirection.Output

            myCMD.ExecuteNonQuery()

            l_dsp = myCMD.Parameters("p_dsp").Value

            If InStr(UCase(myCMD.Parameters("p_dsp").Value), "SUCCESSFUL") > 0 Then

                l_crn = myCMD.Parameters("p_crn").Value
                tdpl_acct_num.Text = l_crn
                Session("tdpl_acct_num") = l_crn
            Else   'transation not done 
                l_crn = ""
                Session("tdpl_acct_num") = l_crn
                'Daniela show the error message
                txt_msg.Text = l_dsp
            End If

            myCMD.Cancel()
            myCMD.Dispose()

            'Sep 29 enable buttons when done
            Button3.Enabled = True
            Button2.Enabled = True

        Catch
            Throw
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub

    Private Sub CreateCustomer()

        If Session("cust_cd") = "NEW" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim hphone As String = ""
            Dim bphone As String = ""
            Dim cust_tp_cd As String = ""
            Dim email As String = ""

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("cust_cd") & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Try
                dbConnection.Open()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    fname = dbReader.Item("BILL_FNAME").ToString.Trim
                    If fname & "" = "" Then fname = dbReader.Item("FNAME").ToString.Trim
                    lname = dbReader.Item("BILL_LNAME").ToString.Trim
                    If lname & "" = "" Then lname = dbReader.Item("LNAME").ToString.Trim
                    cust_tp_cd = dbReader.Item("CUST_TP").ToString.Trim
                    addr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                    If addr1 & "" = "" Then addr1 = dbReader.Item("ADDR1").ToString.Trim
                    addr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                    If addr2 & "" = "" Then addr2 = dbReader.Item("ADDR2").ToString.Trim
                    city = dbReader.Item("BILL_CITY").ToString.Trim
                    If city & "" = "" Then city = dbReader.Item("CITY").ToString.Trim
                    state = dbReader.Item("BILL_ST").ToString.Trim
                    If state & "" = "" Then state = dbReader.Item("ST").ToString.Trim
                    zip = dbReader.Item("BILL_ZIP").ToString.Trim
                    If zip & "" = "" Then zip = dbReader.Item("ZIP").ToString.Trim
                    hphone = StringUtils.FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                    bphone = StringUtils.FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)
                    email = dbReader.Item("EMAIL").ToString.Trim
                End If
                dbReader.Close()
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
            ' a single transaction to be able to rollback all changes if something fails
            Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")

            Session("CUST_CD") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"),
                                                                 UCase(addr1),
                                                                 Left(UCase(new_cust_lname), 20),
                                                                 Left(UCase(new_cust_fname), 15))
            Try
                dbAtomicConnection.Open()
                dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction()

                ' --- Save Customer to the E1 Database
                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
                sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_CD").Value = Session("CUST_CD").ToString.Trim
                dbAtomicCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
                dbAtomicCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
                dbAtomicCommand.Parameters.Add(":FNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":FNAME").Value = UCase(fname)
                dbAtomicCommand.Parameters.Add(":LNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":LNAME").Value = UCase(lname)
                dbAtomicCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_TP").Value = cust_tp_cd
                dbAtomicCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR1").Value = UCase(addr1)
                dbAtomicCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR2").Value = UCase(addr2)
                dbAtomicCommand.Parameters.Add(":CITY", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CITY").Value = UCase(city)
                dbAtomicCommand.Parameters.Add(":ST", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ST").Value = UCase(state)
                dbAtomicCommand.Parameters.Add(":ZIP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ZIP").Value = zip
                dbAtomicCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":HPHONE").Value = hphone
                dbAtomicCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":BPHONE").Value = bphone
                dbAtomicCommand.Parameters.Add(":EMAIL", OracleType.VarChar)
                dbAtomicCommand.Parameters(":EMAIL").Value = UCase(email)
                dbAtomicCommand.ExecuteNonQuery()
                dbAtomicCommand.Parameters.Clear()  'to get it ready for the next statement

                ' --- Update newly created Customer Code to the table in the CRM schema 
                sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("CUST_CD") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='NEW'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves all the pending changes.
                dbAtomicCommand.Transaction.Commit()

            Catch ex As Exception
                dbAtomicCommand.Transaction.Rollback()  'reverts any changes made so far
                Throw
            Finally
                dbAtomicCommand.Dispose()
                dbAtomicConnection.Close()
            End Try
        End If
    End Sub

End Class
