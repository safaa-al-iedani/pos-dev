Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class CashierActivation
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        lbl_error.Text = ""

        If Not IsPostBack Then
            If Request("LEAD") <> "TRUE" Then
                txt_post_dt.MinDate = Today.Date.AddDays(-SysPms.numDaysPostDtTrans)
                txt_post_dt.MaxDate = Today.Date.AddDays(SysPms.numDaysPostDtTrans)

                'If the System Parameter for disallowing post date transactions is set to "Y" then lock down the calendar
                If SysPms.skipTransDate Then txt_post_dt.ReadOnly = True
            End If
            ' populate the store drop down
            PopulateStores()
            CshaPopulate()
            'lbl_title.Text = "Cashier Activation for " & Session("EMP_FNAME") & " " & Session("EMP_LNAME") & " (" & Session("EMP_INIT") & ")"  'cshaPopulate extracts and sets the emp_init
            lbl_title.Text = Resources.LibResources.Label67 & " " & Session("EMP_FNAME") & " " & Session("EMP_LNAME") & " (" & Session("EMP_INIT") & ")"  'cshaPopulate extracts and sets the emp_init
            If isNotEmpty(cbo_csh_drw.Value) Then
                CheckDrawBalance()
            Else
                lbl_error.Text = "Cashier needs to be created in Cashier Activation."
                Exit Sub
            End If
            store_cd.SelectedItem = store_cd.Items.FindByValue(lbl_store_orig.Text)
            cbo_csh_drw.SelectedItem = cbo_csh_drw.Items.FindByValue(lbl_csh_orig.Text)
        End If

        If Not IsDate(lbl_post_orig.Text) Then lbl_post_orig.Text = FormatDateTime(Today.Date, DateFormat.ShortDate)

        If lbl_store_orig.Text = store_cd.Value And lbl_csh_orig.Text = cbo_csh_drw.Value And FormatDateTime(lbl_post_orig.Text, DateFormat.ShortDate) = FormatDateTime(txt_post_dt.SelectedDate, DateFormat.ShortDate) Then
            btn_save.Enabled = False
        Else
            btn_save.Enabled = True
        End If

    End Sub


    ''' <summary>
    ''' Populates the store drop down and also any cash drawer and other info associated with it.
    ''' </summary>
    Private Sub PopulateStores()

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String = ""
        Dim MyDataReader As OracleDataReader

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        txt_post_dt.SelectedDate = Today.Date

        Dim userType As Double = -1

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")
        Dim empCd = Session("EMP_CD")

        Try
            If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
                SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store s " +
                    " where exists (select 1 from CSH_DWR cd where cd.store_cd = s.store_cd) "
            End If

            If (isNotEmpty(userType) And userType > 0) Then

                SQL = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc " +
                   "from MCCL_STORE_GROUP a, store s, store_grp$store b " +
                   "where a.user_id = :userType and " +
                   "s.store_cd = b.store_cd and " +
                   "a.store_grp_cd = b.store_grp_cd and " +
                   "exists (select 1 from CSH_DWR cd where cd.store_cd = s.store_cd) and s.co_cd = :coCd "
                If ConfigurationManager.AppSettings("store_based") = "Y" And Skip_Data_Security() = "N" Then
                    SQL = SQL & " and s.STORE_CD IN (select store_grp$store.store_cd from store_grp$store, emp2store_grp " +
                    " where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd and emp2store_grp.emp_cd=:empCd) "
                End If
            End If
            SQL = SQL & " order by store_cd"

            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            ' Parameters only if not Super User
            If (isNotEmpty(userType) And userType > 0) Then

                cmdGetStores.Parameters.Add(":userType", OracleType.VarChar)
                cmdGetStores.Parameters(":userType").Value = userType
                cmdGetStores.Parameters.Add(":coCd", OracleType.VarChar)
                cmdGetStores.Parameters(":coCd").Value = coCd
                If ConfigurationManager.AppSettings("store_based") = "Y" And Skip_Data_Security() = "N" Then
                    cmdGetStores.Parameters.Add(":empCd", OracleType.VarChar)
                    cmdGetStores.Parameters(":empCd").Value = empCd
                End If

            End If

            Dim ds2 As New DataSet
            Dim oAdp2 = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp2.Fill(ds2)

            With store_cd
                .DataSource = ds2
                .TextField = "FULL_DESC"
                .ValueField = "STORE_CD"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CshaPopulate()

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim SQL As String
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            SQL = "SELECT CSHR_ACTIVATION.* FROM CSHR_ACTIVATION, EMP WHERE EMP.EMP_INIT=CSHR_ACTIVATION.CSHR_INIT AND EMP_CD='" & Session("emp_cd") & "'"

            objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If MyDataReader.Read Then
                    lbl_store_orig.Text = MyDataReader.Item("STORE_CD").ToString
                    lbl_csh_orig.Text = MyDataReader.Item("CSH_DWR_CD").ToString
                    lbl_post_orig.Text = MyDataReader.Item("POST_DT").ToString
                    store_cd.Value = MyDataReader.Item("STORE_CD").ToString
                    CashDrawerPopulate(store_cd.Value.ToString())
                    cbo_csh_drw.Value = MyDataReader.Item("CSH_DWR_CD").ToString
                    txt_post_dt.SelectedDate = MyDataReader.Item("POST_DT").ToString
                    Session("emp_init") = MyDataReader.Item("CSHR_INIT").ToString
                Else
                    lbl_new.Visible = True
                End If
                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Session("emp_init") & "" = "" Then
            Try
                SQL = "SELECT EMP_INIT FROM EMP WHERE EMP.EMP_CD='" & Session("emp_cd") & "'"

                objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If MyDataReader.Read Then
                        Session("emp_init") = MyDataReader.Item("EMP_INIT").ToString
                    Else
                        lbl_new.Visible = True
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If
        conn.Close()

    End Sub


    ''' <summary>
    ''' Updates the related store info such as cash drawers and pickup-delv store
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub UpdateStoreInfo()

        If store_cd.Value.ToString.isNotEmpty Then

            Try
                Dim ds As DataSet = theSystemBiz.GetCashDrawers(store_cd.Value.ToString)
                With cbo_csh_drw
                    .DataSource = ds
                    .ValueField = "csh_dwr_cd"
                    .TextField = "full_desc"
                    .DataBind()
                    .SelectedIndex = 0
                End With
            Catch ex As Exception
                Throw
            End Try

            Try
                lbl_co_cd.Text = theSystemBiz.GetCompanyCd(store_cd.Value.ToString)
            Catch ex As Exception
                Throw
            End Try

            CheckDrawBalance()

        End If

    End Sub

    Private Sub CheckDrawBalance()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        sql = "SELECT POST_DT FROM CSH_DWR_BAL WHERE STORE_CD=:STORE_CD AND CSH_DWR_CD=:CSHDRW AND CLOSED='N' "
        sql = sql & "AND POST_DT <> TO_DATE(:POST_DT,'mm/dd/RRRR')"

        cmdGetStores = DisposablesManager.BuildOracleCommand(sql, conn)
        cmdGetStores.Parameters.Add(":STORE_CD", OracleType.VarChar)
        cmdGetStores.Parameters(":STORE_CD").Value = UCase(store_cd.Value.ToString.Trim)
        cmdGetStores.Parameters.Add(":CSHDRW", OracleType.VarChar)
        cmdGetStores.Parameters(":CSHDRW").Value = cbo_csh_drw.Value.ToString
        cmdGetStores.Parameters.Add(":POST_DT", OracleType.VarChar)
        cmdGetStores.Parameters(":POST_DT").Value = FormatDateTime(txt_post_dt.Value, DateFormat.ShortDate)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(cmdGetStores)

            If MyDataReader.Read() Then
                ' chk_soft_close.Text = "Soft close the previous drawer for " & FormatDateTime(MyDataReader.Item("POST_DT").ToString, vbShortDate)
                chk_soft_close.Text = Resources.LibResources.Label558 & FormatDateTime(MyDataReader.Item("POST_DT").ToString, vbShortDate)
                chk_soft_close.Visible = True
            Else
                chk_soft_close.Visible = False
            End If
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub


    Protected Sub btn_save_Click(sender As Object, e As System.EventArgs) Handles btn_save.Click

        lbl_error.Text = ""
        If isEmpty(cbo_csh_drw.Value) Then
            lbl_error.Text = "Cash Drawer Code is invalid."
            Exit Sub
        End If

        If chk_soft_close.Visible = True And chk_soft_close.Checked = False Then
            lbl_error.Text = "You must soft close the previous drawer to open a new cash drawer"
            Exit Sub
        End If

        Dim postDate As Date = FormatDateTime(txt_post_dt.Value, DateFormat.ShortDate)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                   ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        SQL = "SELECT * FROM CSHR_ACTIVATION WHERE CSHR_INIT='" & Session("emp_init") & "'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Dim add_cshr As Boolean = False

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                add_cshr = False
            Else
                add_cshr = True
            End If
        Catch
            Throw
        End Try

        If add_cshr = True Then
            SQL = "INSERT INTO CSHR_ACTIVATION (CSHR_INIT, CO_CD, STORE_CD, CSH_DWR_CD, POST_DT) "
            SQL = SQL & "VALUES(:CSHR_INIT, :CO_CD, :STORE_CD, :CSH_DWR_CD, :POST_DT)"
            objSql = DisposablesManager.BuildOracleCommand(SQL, conn)
            objSql.Parameters.Add(":CSHR_INIT", OracleType.VarChar)
            objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
            objSql.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
            objSql.Parameters.Add(":POST_DT", OracleType.DateTime)

            objSql.Parameters(":CSHR_INIT").Value = Session("emp_init")
            objSql.Parameters(":CO_CD").Value = lbl_co_cd.Text
            objSql.Parameters(":STORE_CD").Value = store_cd.Value
            objSql.Parameters(":CSH_DWR_CD").Value = cbo_csh_drw.Value
            objSql.Parameters(":POST_DT").Value = FormatDateTime(txt_post_dt.Value, DateFormat.ShortDate)

            objSql.ExecuteNonQuery()
            objSql.Parameters.Clear()
        Else
            SQL = "UPDATE CSHR_ACTIVATION SET CO_CD=:CO_CD, STORE_CD=:STORE_CD, CSH_DWR_CD=:CSH_DWR_CD, POST_DT=:POST_DT "
            SQL = SQL & "WHERE CSHR_INIT=:CSHR_INIT"
            objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objSql.Parameters.Add(":CSHR_INIT", OracleType.VarChar)
            objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
            objSql.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
            objSql.Parameters.Add(":POST_DT", OracleType.DateTime)

            objSql.Parameters(":CSHR_INIT").Value = Session("emp_init")
            objSql.Parameters(":CO_CD").Value = lbl_co_cd.Text
            objSql.Parameters(":STORE_CD").Value = store_cd.Value
            objSql.Parameters(":CSH_DWR_CD").Value = cbo_csh_drw.Value
            objSql.Parameters(":POST_DT").Value = FormatDateTime(txt_post_dt.Value, DateFormat.ShortDate)

            objSql.ExecuteNonQuery()
            objSql.Parameters.Clear()
        End If

        lbl_error.Text = "Cashier Activation complete."
        ' lbl_error.Text =
        btn_save.Enabled = False
        Try
            If chk_soft_close.Visible = True And chk_soft_close.Checked = True Then
                SQL = "UPDATE CSH_DWR_BAL SET CLOSED='S', CLOSE_TIME=SYSDATE "

                If Session("EMP_CD") IsNot Nothing Then
                    SQL = SQL & ", EMP_CD_OP = :EMP_CD_OP "
                End If

                SQL = SQL & "WHERE CO_CD=:CO_CD AND STORE_CD=:STORE_CD AND CSH_DWR_CD=:CSH_DWR_CD "
                SQL = SQL & "AND POST_DT<:POST_DT AND CLOSED='N'"
                objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

                objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
                objSql.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
                objSql.Parameters.Add(":POST_DT", OracleType.DateTime)

                If Session("EMP_CD") IsNot Nothing Then
                    objSql.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)
                    objSql.Parameters(":EMP_CD_OP").Value = Session("EMP_CD")
                End If

                objSql.Parameters(":CO_CD").Value = lbl_co_cd.Text
                objSql.Parameters(":STORE_CD").Value = store_cd.Value
                objSql.Parameters(":CSH_DWR_CD").Value = cbo_csh_drw.Value
                objSql.Parameters(":POST_DT").Value = postDate

                objSql.ExecuteNonQuery()
                objSql.Parameters.Clear()
            End If

            'jkl
            'SQL = "SELECT POST_DT, NVL(CLOSED,'N') AS CLOSED FROM CSH_DWR_BAL WHERE STORE_CD='" & UCase(store_cd.Value.ToString.Trim) & "' AND CSH_DWR_CD='" & cbo_csh_drw.Value.ToString & "' "
            'SQL = SQL & "AND POST_DT = TO_DATE('" & txt_post_dt.SelectedDate & "','mm/dd/RRRR')"
            SQL = "SELECT POST_DT, NVL(CLOSED,'N') AS CLOSED FROM CSH_DWR_BAL WHERE STORE_CD=:STORE_CD AND CSH_DWR_CD=:CSH_DWR_CD "
            SQL = SQL & "AND POST_DT = TO_DATE(:POST_DT ,'mm/dd/RRRR')"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(SQL, conn)
            'jkl
            objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
            objSql.Parameters(":STORE_CD").Value = UCase(store_cd.Value.ToString.Trim)
            objSql.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
            objSql.Parameters(":CSH_DWR_CD").Value = cbo_csh_drw.Value.ToString
            objSql.Parameters.Add(":POST_DT", OracleType.VarChar)
            objSql.Parameters(":POST_DT").Value = FormatDateTime(txt_post_dt.Value, DateFormat.ShortDate)

            Dim add_drw As Boolean = False
            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    If MyDataReader.Item("CLOSED").ToString <> "N" Then
                        lbl_error.Text = "The date, store, and cash drawer you selected are already closed."
                        add_drw = False
                        Exit Sub
                    Else
                        lbl_error.Text = "The date, store, and cash drawer you selected are already opened.  The drawer will be shared."
                        add_drw = False
                        Exit Sub
                    End If
                Else
                    add_drw = True
                End If
                If add_drw = True Then
                    SQL = "INSERT INTO CSH_DWR_BAL (CSHR_INIT, CO_CD, STORE_CD, CSH_DWR_CD, POST_DT, OPEN_TIME, OPEN_BALANCE, BALANCED, CLOSED, EMP_CD_OP) "
                    SQL = SQL & "VALUES(:CSHR_INIT, :CO_CD, :STORE_CD, :CSH_DWR_CD, :POST_DT, SYSDATE, 0, 'N', 'N', :emp_cd)"
                    objSql = DisposablesManager.BuildOracleCommand(SQL, conn)
                    objSql.Parameters.Add(":CSHR_INIT", OracleType.VarChar)
                    objSql.Parameters.Add(":CO_CD", OracleType.VarChar)
                    objSql.Parameters.Add(":STORE_CD", OracleType.VarChar)
                    objSql.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
                    objSql.Parameters.Add(":POST_DT", OracleType.DateTime)
                    objSql.Parameters.Add(":emp_cd", OracleType.VarChar)

                    objSql.Parameters(":CSHR_INIT").Value = Session("emp_init")
                    objSql.Parameters(":CO_CD").Value = lbl_co_cd.Text
                    objSql.Parameters(":STORE_CD").Value = store_cd.Value
                    objSql.Parameters(":CSH_DWR_CD").Value = cbo_csh_drw.Value
                    objSql.Parameters(":POST_DT").Value = FormatDateTime(txt_post_dt.Value, DateFormat.ShortDate)
                    objSql.Parameters(":emp_cd").Value = Session("EMP_CD")

                    objSql.ExecuteNonQuery()
                    objSql.Parameters.Clear()
                End If
                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub txt_post_dt_ValueChanged(sender As Object, e As System.EventArgs) Handles txt_post_dt.ValueChanged

        CheckDrawBalance()

    End Sub

    Protected Sub cbo_csh_drw_ValueChanged(sender As Object, e As System.EventArgs) Handles cbo_csh_drw.ValueChanged

        CheckDrawBalance()

    End Sub

    Private Sub CashDrawerPopulate(strStoreCode As String)
        Try
            lbl_co_cd.Text = theSystemBiz.GetCompanyCd(strStoreCode)

            Dim dsCashDrwrs As DataSet = theSystemBiz.GetCashDrawers(strStoreCode)

            With cbo_csh_drw
                .DataSource = dsCashDrwrs
                .ValueField = "csh_dwr_cd"
                .TextField = "full_desc"
                .DataBind()
                .SelectedIndex = 0
            End With
            If cbo_csh_drw.Value = "" Then cbo_csh_drw.SelectedIndex = 0
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        HBCG_Utils.Update_Theme()
    End Sub
End Class
