<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Utilities.aspx.vb" Inherits="Utilities" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink1" NavigateUrl="admin.aspx" runat="server" Text="System Parameters">
    </dx:ASPxHyperLink>
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink2" NavigateUrl="Lead_Book_Transfer.aspx" runat="server"
        Text="Transfer Book of Business">
    </dx:ASPxHyperLink>
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink3" NavigateUrl="admin_rights.aspx" runat="server"
        Text="User Securities">
    </dx:ASPxHyperLink>
    <br />
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink4" NavigateUrl="Terminal_Setup.aspx" runat="server"
        Text="Terminal Setup">
    </dx:ASPxHyperLink>
    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="** This could take several minutes to load.." ForeColor="Red">
    </dx:ASPxLabel>
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink5" NavigateUrl="Terminal_Status.aspx" runat="server"
        Text="Configure Terminal Status">
    </dx:ASPxHyperLink>
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink6" NavigateUrl="Email_Template_Manager.aspx" runat="server"
        Text="Configure Email Templates">
    </dx:ASPxHyperLink>
    <br />
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink7" NavigateUrl="Payment_xref.aspx" runat="server"
        Text="Payment Setup">
    </dx:ASPxHyperLink>
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink14" NavigateUrl="MOP_Maint.aspx" runat="server"
        Text="Method of Payment Setup">
    </dx:ASPxHyperLink>
    <br /><dx:ASPxHyperLink ID="ASPxHyperLink15" NavigateUrl="OrderSort_Maint.aspx" runat="server"
        Text="Order Sort Code Setup">
    </dx:ASPxHyperLink>
    <br /><dx:ASPxHyperLink ID="ASPxHyperLink17" NavigateUrl="Related_Maint.aspx" runat="server"
        Text="Related SKU Add-On Setup">
    </dx:ASPxHyperLink>
    <br />
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink8" NavigateUrl="GeneralInterest.aspx" runat="server"
        Text="Configure General Interest Categories">
    </dx:ASPxHyperLink>
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink9" NavigateUrl="NewsMaintenance.aspx" runat="server"
        Text="News and Updates Maintenance">
    </dx:ASPxHyperLink>
    <br /><dx:ASPxHyperLink ID="ASPxHyperLink13" NavigateUrl="~/help_admin.aspx" runat="server"
        Text="Help Screen Setup">
    </dx:ASPxHyperLink>
    <br />
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink10" NavigateUrl="invoice_triggers.aspx" runat="server"
        Text="Invoice Triggers">
    </dx:ASPxHyperLink>
    <br />
    <br />
    <dx:ASPxHyperLink ID="hpl_invoice" NavigateUrl="invoice_admin.aspx" runat="server"
        Text="Invoice Selection/Assignment">
    </dx:ASPxHyperLink>
    <br />
    <dx:ASPxHyperLink ID="hpl_quote" NavigateUrl="quote_admin.aspx" runat="server"
        Text="Quote Selection/Assignment">
    </dx:ASPxHyperLink>
    <br />
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink11" NavigateUrl="Customer_Upload.aspx" runat="server"
        Text="Upload Customers">
    </dx:ASPxHyperLink>
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink16" NavigateUrl="Picture_Upload.aspx" runat="server"
        Text="Upload Images">
    </dx:ASPxHyperLink>
    <br />
    <br />
     <dx:ASPxHyperLink ID="ASPxHyperLink12" NavigateUrl="KioskAnalysis.aspx" runat="server"
        Text="Kiosk Analysis">
    </dx:ASPxHyperLink>
    <br />
    <br />
     <dx:ASPxHyperLink ID="ASPxHyperLink18" NavigateUrl="LoadEHPriceItems.aspx" runat="server"
        Text="EasyHome Price Upload">
    </dx:ASPxHyperLink>
    <br />  
    <br />
    <%--<a href="Campaign_main.aspx" class="style5">Launch Campaign</a>
    <br />
    <br />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
