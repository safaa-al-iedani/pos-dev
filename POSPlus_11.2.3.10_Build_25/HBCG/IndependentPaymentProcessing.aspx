<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="IndependentPaymentProcessing.aspx.vb" Inherits="IndependentPaymentProcessing" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        function SelectAndClosePopup1() {
            PopupCCPmt.Hide();
        }
        function stopEnterKey(evt) {
            evt = (evt) ? evt : event;
            var charCode = evt.which || evt.charCode || evt.keyCode || 0;
            if (charCode == 13) {
                evt.preventDefault();
                return false;
            }
        }
    </script>
     <script language="javascript">
         function disableButton(id) {
             // Daniela B 20140722 Disable the 2 buttons to prevent double click
             try {
                 var a = document.getElementById(id);
                 a.style.display = 'none';
                 var b = document.getElementById('ctl00_ContentPlaceHolder1_PopupFinancePmt_lucy_btn_new_cust');
                 b.style.display = 'none';
                 return true;
             } catch (err) {
                 alert('Error ' + err.message);
                 return false;
             }
         }
    </script>

    <table width="100%" class="style5">
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <td valign="top">
                            <b><u>
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label435 %>">
                                </dx:ASPxLabel>
                            </u></b>&nbsp; &nbsp;
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_payment_type" runat="server" AutoPostBack="True" Enabled="False"
                               IncrementalFilteringMode ="StartsWith"  Width="105px">
                                <Items>
                                    <dx:ListEditItem Text="<%$ Resources:LibResources, Label384 %>" Value="PMT" Selected="True" />
                                    <dx:ListEditItem Text="<%$ Resources:LibResources, Label452 %>" Value="R" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td valign="top">
                            <asp:Label ID="lbl_fin_exists" runat="server" Visible="False" Width="1px"></asp:Label>
                            <asp:Label ID="lbl_auth_no" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <b><u>
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label424 %>">
                    </dx:ASPxLabel>
                </u></b>
                <dx:ASPxLabel ID="lbl_post_dt" runat="server" Text="">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label68 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="txt_cshr_pwd" runat="server" TextMode="Password" AutoPostBack="true"
                    Password="True" Width="224px">
                </dx:ASPxTextBox>
                <asp:Label ID="lbl_emp_cd" runat="server" Text="" Width="1" Visible="false"></asp:Label>
                <asp:Label ID="lbl_email" runat="server" Visible="False" Width="1px"></asp:Label></td>
            <td>
                <dx:ASPxLabel ID="Label5" runat="server" Text="">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label573 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxComboBox ID="cbo_store_cd" runat="server" Width="224px" AutoPostBack="true" IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
            </td>
            <td>
                <dx:ASPxLabel ID="txt_cust_cd" runat="server" Text="">
                </dx:ASPxLabel>
                <asp:Label ID="lbl_co_cd" runat="server" Text="" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label64 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxComboBox ID="cbo_csh_drw" runat="server" Width="224px" AutoPostBack="True"
                   SelectedIndex="0" IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
            </td>
            <td>
                <dx:ASPxLabel ID="Label9" runat="server" Text="">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_qs_avail" runat="server" Visible="False"></asp:Label></td>
            <td valign="top">
                <asp:ImageButton ID="img_qs" runat="server" ImageUrl="~/images/zip.png" Visible="False"
                    Width="30px" />&nbsp; &nbsp;<dx:ASPxLabel ID="lbl_qs_desc" runat="server" Text="">
                    </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxLabel ID="Label6" runat="server" Text="">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:LibResources, Label511 %>">
                </dx:ASPxLabel>
            </td>
            <td valign="middle">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxTextBox ID="txt_ivc_cd" runat="server" Width="101px" AutoPostBack="true" OnTextChanged="txt_ivc_cd_TextChanged" >
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_lookup_ivc" runat="server" Text="<%$ Resources:LibResources, Label222 %>" Enabled="False" Width="71px" CausesValidation="true" ValidationGroup="OrderIdValidationGroup">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnOrderSearch" runat="server" Text="<%$ Resources:LibResources, Label526 %>" OnClick="btnOrderSearch_Click" CausesValidation="false">
                                <Image Url="~/images/icons/Find-icon.png" Height="15" Width="15">
                                </Image>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <asp:CustomValidator ID="OrderValidator" runat="server" Visible="true" SetFocusOnError="true" ErrorMessage="" Display="None"
                                ControlToValidate="txt_ivc_cd" OnServerValidate="ValidateOrder" ValidationGroup="OrderIdValidationGroup">
                            </asp:CustomValidator></td>
                    </tr>
                </table>
                <dx:ASPxLabel ID="Label8" runat="server" Text="">
                </dx:ASPxLabel>
                <asp:hiddenfield id="hdnOrderStatus"  EnableViewState ="true" value="" runat="server"/>
            </td>
            <td>
                <dx:ASPxLabel ID="Label10" runat="server" Text="">
                </dx:ASPxLabel>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td>
                <b>
                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="<%$ Resources:LibResources, Label292 %>">
                    </dx:ASPxLabel>
                </b>
            </td>
            <td style="width: 18px">
                <b>
                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="<%$ Resources:LibResources, Label28 %>">
                    </dx:ASPxLabel>
                </b>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxComboBox ID="mop_drp" runat="server" AutoPostBack="True" Width="305px">
                </dx:ASPxComboBox>
                 <asp:Label ID="lucy_lbl_ind_aprv" runat="server" Text="lucy_lbl_ind_aprv"></asp:Label>
                <asp:Label ID="lbl_pmt_amt" runat="server" Visible="False"></asp:Label>
            </td>
            <td style="width: 18px">
                <dx:ASPxTextBox ID="txt_pmt_amt" runat="server" AutoPostBack="True" Width="75px" DisplayFormatString="C" onkeydown="javascript:stopEnterKey(event)"></dx:ASPxTextBox>

            </td>
            <td align="right">
                <dx:ASPxButton ID="btn_add" runat="server" Text="<%$ Resources:LibResources, Label17 %>" CausesValidation="false">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <asp:DataGrid ID="GridviewPmts" Style="position: relative; top: 9px;" runat="server"
        AutoGenerateColumns="False" CellPadding="2" DataKeyField="row_id" OnDeleteCommand="DoItemDelete"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="1px" Width="100%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left"
        PageSize="5">
        <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
            Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
        <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
            Font-Bold="True" Height="20px" HorizontalAlign="Left"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="mop_cd" HeaderText="SKU" ReadOnly="True" Visible="False">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="des" HeaderText="<%$ Resources:LibResources, Label390 %>" ReadOnly="True">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="amt" HeaderText="<%$ Resources:LibResources, Label28 %>" DataFormatString="{0:0.00}">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="row_id" HeaderText="ID" Visible="False">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="mop_tp" HeaderText="ID" Visible="False">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label176 %>">
                <%--lucy commented the 3 lines  
                    <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="images/icons/tick.png" Height="20"
                        Width="20" /> 
                </ItemTemplate>  
                    end lucy comment --%>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
            </asp:TemplateColumn>
            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"
                HeaderText="<%$ Resources:LibResources, Label150 %>">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>
        </Columns>
        <ItemStyle VerticalAlign="Top" />
    </asp:DataGrid>
    
    <br />
    <dx:ASPxLabel ID="lbl_Error" runat="server" Text="" Font-Bold="true" ForeColor="Red">
    </dx:ASPxLabel>

    <asp:Label ID="Label7" runat="server" Font-Bold="True" ForeColor="Transparent" Visible="False"></asp:Label>
    <%--lucy commented the 3 lines  
                    <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="images/icons/tick.png" Height="20"
                        Width="20" /> 
                </ItemTemplate> 
                    end lucy comment --%>
    <table width="100%">
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btn_return" runat="server" Text="<%$ Resources:LibResources, Label490 %>" OnClick="btn_return_Click"
                                Visible="False" CausesValidation="false">
                            </dx:ASPxButton>
                        </td>
                     <%--Daniela comment  <td>
                            <dx:ASPxButton ID="btn_pickup" runat="server" Text="Print Pick-Up Ticket" CausesValidation="false" Enabled="False"
                                OnClick="btn_pickup_Click" Visible="False">
                            </dx:ASPxButton>
                        </td>--%>
                        <td>
                            <dx:ASPxButton ID="btn_show_invoices" runat="server" Text="Show Invoices" CausesValidation="false" Enabled="False"
                                OnClick="btn_show_invoices_Click" Visible="False">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_reset" runat="server" Text="<%$ Resources:LibResources, Label474 %>" CausesValidation="false">
                            </dx:ASPxButton>
                        </td>
                        <%--'Daniela Dec 30 print pick ticket --%>
                         <td>              
                            <dx:ASPxComboBox ID="printer_drp" runat="server" Width="150px" DropDownRows="5">               
                            </dx:ASPxComboBox>
                        </td>      
                        <td>
                           <%--'Daniela Dec 30 print pick ticket --%>
                            <dx:ASPxButton ID="btn_pickup" runat="server" Text="<%$ Resources:LibResources, Label431 %>" Enabled="False">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_commit" runat="server" Text="<%$ Resources:LibResources, Label97 %>" CausesValidation="false">
                            </dx:ASPxButton>
                        </td>
                        </tr>
                </table>
            </td>
        </tr>
    </table>
     <br />
    <%--Daniela added print message--%>
    <dx:ASPxLabel ID="lbl_pickticket" runat="server" Text="" Font-Bold="true" ForeColor="Red">
    </dx:ASPxLabel>

    <dxpc:ASPxPopupControl ID="PopupCCPmt" runat="server" ClientInstanceName="PopupCCPmt" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label125 %>" Height="300px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="500px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="PopupOrigRefundPmts" runat="server" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label536 %>" Height="207px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="178px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <table>
                    <tr>
                        <td>
                            <table style="width: 100%;">
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="lblMinDue" runat="server" Text="Minimum Due:"  Visible="false" CssClass="style5" Style="font-weight: bold"></asp:Label>
                                        <asp:Label ID="lblMinDueAmt" runat="server" CssClass="style5" Visible="false" Style="margin-right: 3px; font-weight: bold"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="center" style="width: 443px">
                            <br />
                            <asp:DataGrid ID="MyDataGrid" runat="server" Width="413" BorderColor="Black" CellPadding="3"
                                AutoGenerateColumns="False" DataKeyField="ID" Height="16px" PageSize="7"
                                AlternatingItemStyle-BackColor="Beige" OnItemCreated="MyDataGrid_ItemCreated" CssClass="style5">
                                <Columns>
                                    <asp:BoundColumn HeaderText="Description" DataField="DES" />
                                    <asp:BoundColumn HeaderText="Card Number" DataField="BNK_CRD_NUM" />
                                    <asp:BoundColumn HeaderText="Expiration Date" DataField="EXP_DT" DataFormatString="{0:MM/dd/yyyy}" />
                                    <asp:BoundColumn HeaderText="ACCT_NUM" DataField="ACCT_NUM" Visible="false" />
                                    <asp:BoundColumn HeaderText="MOP_CD" DataField="MOP_CD" Visible="false" />
                                    <asp:BoundColumn HeaderText="MOP_TYPE" DataField="MOP_TP" Visible="false" />
                                    <asp:TemplateColumn HeaderText="Refund Amount">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="txt_refund_amt" Text='<%# Replace(FormatNumber(DataBinder.Eval(Container, "DataItem.AMT"),2),",","") %>'
                                                runat="server" AutoPostBack="true" OnTextChanged="txt_refund_amt_textchanged" Width="75" CssClass="style5"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkBoxSelectPmt" runat="server" AutoPostBack="true"  OnCheckedChanged="lucy_refund" CssClass="style5" /> 
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn HeaderText="ID" DataField="ID" Visible="False" />
                                </Columns>
                                <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                <EditItemStyle CssClass="style5" />
                                <ItemStyle CssClass="style5" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="lblRefundError" runat="server" Width="100%" Font-Bold="true" ForeColor="Red"
                                Style="margin-right: 1px">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%;">
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnSelectForRefund" runat="server" CssClass="style5" Text="Selected for Refund"
                                            Width="135px" Style="margin-left: 2px" Enabled="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
<%--                <br />
                <dx:ASPxLabel ID="lblRefundError" runat="server" Width="100%" Font-Bold="true" ForeColor="Red" Style="margin-left: 7px"></dx:ASPxLabel>
                <br />
                <br />
                <asp:Button ID="btnSelectForRefund" runat="server" CssClass="style5" Text="Add Refund Detail" Width="124px" Style="margin-left: 8px" />--%>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="PopupCheckPmt" runat="server" CloseAction="CloseButton"
        HeaderText="Check Details" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="494px" CssClass="style5">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" DefaultButton="btn_save_chk" runat="server">
                <table class="style5">
                    <tr>
                        <td>Check #:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_check" runat="server" CssClass="style5" MaxLength="6"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Bank Routing #:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="TextBox2" runat="server" Width="194px" CssClass="style5" MaxLength="9"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />:</td>
                        <td align="left">
                            <asp:TextBox ID="TextBox3" runat="server" Width="194px" CssClass="style5" MaxLength="25"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left">Type:
                        </td>
                        <td align="left">&nbsp;<asp:DropDownList ID="DropDownList2" runat="server" CssClass="style5">
                            <asp:ListItem Selected="True" Value="01">Personal</asp:ListItem>
                            <asp:ListItem Value="90">Business</asp:ListItem>
                        </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>Approval #:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_appr" runat="server" CssClass="style5" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>DL State:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_dl_state" runat="server" CssClass="style5" MaxLength="2" Width="20px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>DL #:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_dl_number" runat="server" CssClass="style5" MaxLength="30"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lbl_chk_warnings" runat="server" CssClass="style5" Font-Bold="True"
                    ForeColor="Red"></asp:Label>
                <br />
                <asp:Button ID="btn_save_chk" runat="server" CssClass="style5" Text="Save Check Details" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="PopupPmtWarnings" runat="server" CloseAction="CloseButton"
        HeaderText="Payment Warnings" Height="475px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="608px" ContentUrl="~/Payment_Hold.aspx"
        AllowDragging="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="PopupFinancePmt" runat="server"   CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label205%>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="429px" CssClass="style5" AllowRecreatePopupContent="True" ImmediatePopup="True" >
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                <asp:Panel ID="Panel2" runat="server" DefaultButton="btn_save_fi_co" BorderStyle="None">
                      <%--Daniela B 20140722 <asp:Button ID="btn_save_fi_co" runat="server" CssClass="style5" Text="Click to Swipe/Insert" Visible="False" ForeColor="#FF33CC"/> 
                    <asp:Button ID="btn_save_fi_co" runat="server" CssClass="style5" Text="Click to Swipe/Insert" Visible="False" ForeColor="#FF33CC" OnClientClick="return disableButton(this.id);"/>
                          --%>
                    <%-- sabrina R1999 comment out and replace with --%>
                   <%-- <asp:Button ID="Button2" runat="server" CssClass="style5" Text="<%$ Resources:LibResources, Label90 %>" Visible="False" ForeColor="#FF33CC" />--%>
               <asp:Button ID="btn_save_fi_co" runat="server" Text="Click to Swipe/Insert" CssClass="style5"  UseSubmitBehavior="true" />  

                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="lucy_btn_new_cust" runat="server" Text="New Customer" Visible="False" />
                    <table class="style5">
                        <tr>
                            <%--Daniela french--%>
                            <td style="height: 24px"> <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3%>"/>:
                            </td>
                            <td style="height: 24px">
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="style5">
                                    <asp:ListItem Value="O">Open</asp:ListItem>
                                    <asp:ListItem Value="R">Revolving</asp:ListItem>
                                    <asp:ListItem Value="I">Installment</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />:
                            </td>
                            <td>
                                <asp:DropDownList ID="cbo_finance_company" runat="server" CssClass="style5" Width="318px"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="cbo_promo" runat="server" CssClass="style5" Width="318px" AutoPostBack="True" Visible="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                             <%--Daniela french--%>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5%>"/>:
                            </td>
                            <td>
                                <asp:TextBox ID="txt_fi_acct" runat="server" OnTextChanged="txt_fi_acct_TextChanged"  ></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;<asp:Literal runat="server" Text="<%$ Resources:LibResources, Label192 %>" />:
                            <asp:TextBox ID="txt_fi_exp_dt" runat="server" CssClass="style5" MaxLength="4" Width="40px"  OnTextChanged="txt_fi_exp_dt_OnTextChanged"></asp:TextBox>
                                &nbsp;MMYY
                            </td>
                        </tr>
                        <tr><%--sabrina R1999 --%>
                            <td><asp:Literal ID="Literal1" runat="server" Text="Auth#"/>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_fi_appr" runat="server" CssClass="style5" MaxLength="10" Visible="False"></asp:TextBox>
                               <%-- &nbsp;&nbsp;&nbsp; &nbsp;--%>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="txt_fi_sec_cd" runat="server" CssClass="style5" MaxLength="10" Width="33px" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:TextBox ID="txt_fi_dl_st" runat="server" CssClass="style5" MaxLength="2" Width="20px" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:TextBox ID="txt_fi_dl_no" runat="server" CssClass="style5" MaxLength="30" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
              
                <%--Daniela Label change from Save Finance to Click to Swipe/Insert --%>
                <%--sabina moved up <asp:Button ID="btn_save_fi_co" runat="server" CssClass="style5" Text="Click to Swipe/Insert" UseSubmitBehavior="true" /> --%>
                <asp:Label ID="lbl_pmt_tp" runat="server" Visible="False" CssClass="style5"></asp:Label>
                <asp:Label ID="lbl_fi_warning" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                <asp:Label ID="lbl_GE_tran_id" runat="server" CssClass="style5" Visible="False"></asp:Label>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

     <%--Alice added on Oct 03, 2018 for request 4115--%>
     <dxpc:ASPxPopupControl ID="PopupFinancePmt_FLX" runat="server" ClientInstanceName="ASPxPopupControl8" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label205%>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="450px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlFinanceDetails_flx" runat="server" DefaultButton="btn_save_flx">
                  
                    <table class="style5">
                      <tr>
                        <td>
                          
                        </td>
                        <td align ="right" >
                           <asp:Button ID="btn_save_flx" runat="server" Width="150px" Text="<%$ Resources:LibResources, Label520 %>" />
                        </td>
                     </tr>
                      <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList3" runat="server" CssClass="style5">
                                <asp:ListItem Value="O" Text="<%$ Resources:LibResources, Label359 %>"></asp:ListItem>
                                <asp:ListItem Value="R" Text="<%$ Resources:LibResources, Label1010 %>"></asp:ListItem>
                                <asp:ListItem Value="I" Text="<%$ Resources:LibResources, Label1011 %>"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_finance_company_flx" runat="server" CssClass="style5" Width="318px"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                  
                     <tr>
                            <td><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:LibResources, Label874 %>"/>
                            </td>
                             <td>
                                <asp:TextBox ID="txt_fi_appr_flx" runat="server" CssClass="style5" MaxLength="10" Visible="False"></asp:TextBox>
                               
                            </td>
                    </tr>
                         
                </table>
                                      
                <br />
                
                <asp:Label ID="lbl_pmt_tp_flx" runat="server" Visible="False" CssClass="style5"></asp:Label>
                <asp:Label ID="lbl_fi_warning_flx" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                    
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>


    <%--Alice added on May 31, 2019 for request 235--%>
     <dxpc:ASPxPopupControl ID="PopupFinancePmt_FM" runat="server" ClientInstanceName="ASPxPopupControl11" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label1024%>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="450px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlFinanceDetails_fm" runat="server" DefaultButton="btn_save_fm_co">
                 <%--<asp:Button ID="btn_save_fm_co" runat="server" CssClass="style5" Text="<%$ Resources:LibResources, Label90 %>" Visible="False" ForeColor="Fuchsia"  />--%>
                    <asp:Button ID="btn_save_fm_co" runat="server"  Text="<%$ Resources:LibResources, Label90 %>" ForeColor="Fuchsia" />
                    <%--<asp:Button ID="Button1" runat="server" Text="Button" />     --%>        
                    <table class="style5">
                   
                      <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList5" runat="server" CssClass="style5">
                                <asp:ListItem Value="O" Text="<%$ Resources:LibResources, Label359 %>"></asp:ListItem>
                                <asp:ListItem Value="R" Text="<%$ Resources:LibResources, Label1010 %>"></asp:ListItem>
                                <asp:ListItem Value="I" Text="<%$ Resources:LibResources, Label1011 %>"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_finance_company_fm" runat="server" CssClass="style5" Width="318px"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                  
                   <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_fm_acct" runat="server" CssClass="style5" AutoPostBack ="true"   OnTextChanged="txt_fm_acct_TextChanged"></asp:TextBox>
                            &nbsp; &nbsp; <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label192 %>" />:
                            <asp:TextBox ID="txt_fm_exp" runat="server" CssClass="style5" AutoPostBack ="true"  MaxLength="4" Width="40px"  OnTextChanged="txt_fm_exp_OnTextChanged"></asp:TextBox>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label997 %>" />
                        </td>
                    </tr>

                    <tr>
                            <td><asp:Literal ID="lit_fm" runat="server" Text="<%$ Resources:LibResources, Label874 %>"/>
                            </td>
                             <td>
                                <asp:TextBox ID="txt_fm_appr" runat="server" CssClass="style5" MaxLength="10" Visible="False" AutoPostBack ="true"  OnTextChanged="txt_fm_appr_OnTextChanged" ></asp:TextBox>
                            </td>
                    </tr>
    
                </table>
                        
                <br />
                
                <asp:Label ID="lbl_pmt_tp_fm" runat="server" Visible="False" CssClass="style5"></asp:Label>
                <asp:Label ID="lbl_fm_warning" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                 <asp:Label ID="lbl_fm_tran_id" runat="server" CssClass="style5" Visible="False"></asp:Label>
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>


     <%--Alice added on Dec 13, 2018 for request 4394--%>
     <dxpc:ASPxPopupControl ID="PopupFinancePmt_China" runat="server" ClientInstanceName="ASPxPopupControlChina" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label205%>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="450px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlFinanceDetails_china" runat="server" DefaultButton="btn_save_china">
                  
                   <table class="style5" style=" width:400px; ">
                      <tr>
                        <td width="120px">
                            &nbsp;
                        </td>
                          <td width="200px">
                            &nbsp;
                        </td>
                        <td align ="right" >
                           <asp:Button ID="btn_save_china" runat="server" Width="120px" Text="<%$ Resources:LibResources, Label520 %>" />
                        </td>
                     </tr>
                      <tr>
                        <td width="120px">
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:
                        </td>
                        <td width="200px" >
                            <asp:DropDownList ID="DropDownList4" runat="server" CssClass="style5">
                                <asp:ListItem Value="O" Text="<%$ Resources:LibResources, Label359 %>"></asp:ListItem>
                                <asp:ListItem Value="R" Text="<%$ Resources:LibResources, Label1010 %>"></asp:ListItem>
                                <asp:ListItem Value="I" Text="<%$ Resources:LibResources, Label1011 %>"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                          <td></td>
                    </tr>
  
                    <tr>
                            <td width="120px"><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:LibResources, Label874 %>"/>
                            </td>
                             <td width="200px">
                                <asp:TextBox ID="txt_fi_appr_china" runat="server" CssClass="style5" MaxLength="10" Visible="False"></asp:TextBox>
                            </td>
                        <td></td>
                    </tr>
                        <tr>
                            <td colspan ="3">
                               <span style ="color :red">
                                   <asp:Label ID="Label2" runat="server" Text="<%$ Resources:LibResources, Label1012 %>"></asp:Label>

                               </span>
                              
                            </td>
                        </tr>
                  
                </table>
                  
                   
                <br />
                
                 <asp:Label ID="lbl_pmt_tp_china" runat="server" Visible="False" CssClass="style5"></asp:Label>
                <asp:Label ID="lbl_fi_warning_china" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                    
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>



    <dxpc:ASPxPopupControl ID="PopupSalesOrderSearch" runat="server" Width="673px" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label824 %>" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" CssClass="style5">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btn_lookup_so" BorderStyle="None">
                    <table width="100%" class="style5">
                        <tr>
                            <td nowrap><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label511%>"/>:
                            </td>
                            <td>
                                <asp:TextBox ID="txt_del_doc_num" runat="server" Style="position: relative" Enabled="True"
                                    CssClass="style5" Width="110px"></asp:TextBox>
                            </td>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label668%>"/>:
                            </td>
                            <td>
                                <asp:TextBox ID="txt_wr_dt" runat="server" Style="position: relative" Width="88px"
                                    CssClass="style5"></asp:TextBox>
                            </td>
                            <td style="text-align: right; width: 79px;">
                                <asp:TextBox ID="txt_ord_tp_cd" runat="server" Style="position: relative" Width="26px"
                                    CssClass="style5"></asp:TextBox>
                                -
                                <asp:TextBox ID="txt_store_cd" runat="server" Style="position: relative" Width="19px"
                                    CssClass="style5"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label130%>"/>:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox4" runat="server" Style="position: relative; left: 0px; top: 0px;"
                                    CssClass="style5" Width="110px"></asp:TextBox>
                                <asp:Label ID="lbl_so_doc_num" runat="server" Visible="False"></asp:Label></td>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label571%>"/>:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txt_Stat_cd" runat="server" Style="position: relative; left: 1px; top: 0px;"
                                    Width="16px" CssClass="style5"></asp:TextBox>
                                <asp:TextBox ID="txt_Stat_dt" runat="server" Width="88px" CssClass="style5"></asp:TextBox>
                                <asp:TextBox ID="txt_tax_cd" runat="server" CssClass="style5" ReadOnly="True" Style="left: 1px; position: relative; top: 0px"
                                    Visible="False" Width="16px" Wrap="False"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="height: 7px">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label110 %>" />:
                            </td>
                            <td colspan="4">
                                <asp:TextBox ID="txt_corp_name" runat="server" Width="233px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label196 %>" />:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txt_f_name" runat="server" Width="86px"></asp:TextBox>
                                <asp:TextBox ID="txt_l_name" runat="server" Width="86px"></asp:TextBox>
                            </td>
                            <td colspan="2" align="right"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label225 %>" />:&nbsp;<asp:TextBox ID="txt_h_phone" runat="server" Width="86px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label20 %>" />:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txt_addr1" runat="server" Width="233px" CssClass="style5"></asp:TextBox>
                            </td>
                            <td colspan="2" align="right">C:&nbsp;<asp:TextBox ID="txt_b_phone" runat="server" Width="86px" CssClass="style5"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label21 %>" />:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txt_addr2" runat="server" Width="233px" CssClass="style5"></asp:TextBox>
                            </td>
                            <td colspan="2" align="right"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label553 %>" />:&nbsp;<asp:TextBox ID="txt_slsp1" runat="server" Width="42px" CssClass="style5"></asp:TextBox>
                                <asp:TextBox ID="txt_comm1" runat="server" Width="22px" CssClass="style5" AutoPostBack="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>City/St/Zip:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txt_city" runat="server" Width="136px" CssClass="style5"></asp:TextBox>&nbsp;
                                <asp:TextBox ID="txt_state" runat="server" Width="24px" CssClass="style5"></asp:TextBox>&nbsp;
                                <asp:TextBox ID="txt_zip" runat="server" Width="43px" CssClass="style5"></asp:TextBox>
                            </td>
                            <td colspan="2" align="right"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label554 %>" />:&nbsp;<asp:TextBox ID="txt_slsp2" runat="server" Width="42px" CssClass="style5"></asp:TextBox>
                                <asp:TextBox ID="txt_comm2" runat="server" Width="22px" CssClass="style5" AutoPostBack="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label178 %>" />:
                            </td>
                            <td colspan="4">
                                <asp:TextBox ID="txt_email" runat="server" Width="322px" CssClass="style5"></asp:TextBox>
                                <asp:TextBox ID="txt_SHU" runat="server" Visible="False"></asp:TextBox></td>
                        </tr>
                    </table>
                    <br />
                    <asp:Button ID="btn_lookup_so" runat="server" CssClass="style5" Text="<%$ Resources:LibResources, Label277 %>"
                        OnClick="btn_lookup_so_Click" CausesValidation="false" />
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="PopupCreditApp" runat="server" CloseAction="CloseButton"
        HeaderText="Credit Application" Height="84px" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="452px" AllowDragging="True" CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl DefaultButton="btn_accept"  runat="server">
                Congratulations, you've been pre-approved for a GE Credit Card.
                <br />
                <br />
                <asp:Label ID="lbl_response" runat="server" CssClass="style5" Width="419px"></asp:Label>
                <br />
                <br />
                <asp:Button ID="btn_accept" runat="server" Text="Accept" CssClass="style5" OnClick="btn_accept_Click" />&nbsp;&nbsp;<asp:Button
                    ID="btn_decline" runat="server" Text="Decline" CssClass="style5" OnClick="btn_decline_Click" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="PopupCustomerEMail" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="<%$ Resources:LibResources, Label826%>" Height="64px"
        PopupAction="None" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        Width="494px" CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl DefaultButton="btn_Proceed" runat="server">
                <table class="style5">
                    <tr>
                        <td align="center" valign="middle">
                            <img src="images/icons/Symbol-Stop.png" />
                        </td>
                        <td>&nbsp;&nbsp;
                        </td>
                        <td valign="middle">
                            <asp:Label ID="lbl_warning" runat="server" CssClass="style5" Font-Bold="False" Text="<%$ Resources:LibResources, Label827%>"></asp:Label>
                            <br />
                            <br />
                            <asp:TextBox ID="TextBox5" runat="server" CssClass="style5" Height="16px" Width="293px"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Button ID="btn_Proceed" runat="server" CssClass="style5" OnClick="btn_Proceed_Click"
                                Text="<%$ Resources:LibResources, Label12 %>" />
                            &nbsp;&nbsp;<asp:Button ID="btn_Cancel" runat="server" CssClass="style5" OnClick="btn_Cancel_Click"
                                Text="<%$ Resources:LibResources, Label350 %>" />
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="PopupGiftCardPmt" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Gift Card" Height="64px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="494px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl5" DefaultButton="btnSubmit" runat="server">
                <table class="style5">
                    <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Card Number:">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txt_gc" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" OnTextChanged="txt_gc_TextChanged" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click">
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btn_Clear" runat="server" Text="Clear">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            <dx:ASPxLabel ID="ASPxLabel15" runat="server" Width="100%">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="PopupSignatureCapture" runat="server" AllowDragging="True"
        CloseAction="CloseButton" ContentUrl="~/SigCapture.aspx" HeaderText="Signature Capture"
        Height="327px" PopupAction="None" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        Width="575px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <table class="style5">
                    <tr>
                        <td align="center" valign="middle">
                            <img src="images/icons/Symbol-Stop.png" />
                        </td>
                        <td>&nbsp;&nbsp;
                        </td>
                        <td valign="middle">
                            <asp:Label ID="Label59" runat="server" Font-Bold="True" Text="You have processed a credit memo, would you like to issue a refund?"></asp:Label>
                            <br />
                            <br />
                            <asp:Button ID="Button1" runat="server" CssClass="style5" Text="Process Refund" />
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>   
     <%--'sabrina add cncrefund  --%>
    <dxpc:ASPxPopupControl ID="ASPxPopupcncrefund" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Refund to Gift Card" Height="64px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="494px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl6" DefaultButton="btnSubmit" runat="server">
                <table class="style5">
                    <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="Gift Card#:">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:TextBox ID="cnc_gc_no" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="false" ReadOnly="True"></asp:TextBox>
                            <asp:CheckBox ID="CheckBox_reload" text="Reload?" runat="server" />
                        </td>
                        <td align="left">
                            <dx:ASPxButton ID="cnc_gc_swipe_btn" runat="server" Text="Click to Swipe">
                                        </dx:ASPxButton>
                            </td>
                    </tr>
                         <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel17" halign="top" runat="server" Text="Refund Amount:">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:TextBox ID="cnc_refund_amt" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"></asp:TextBox>
                            <br />
                        </td>
                            </tr>
                     </tr>
                         <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel23" halign="top" runat="server" Text="">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                             <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel24" halign="top" runat="server" Text="">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                        
                            </tr>
                    <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="Order No.:">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:TextBox ID="cnc_del_doc_num" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"  Enabled="false" ReadOnly="True"></asp:TextBox>
                            <br />
                        </td>
                            </tr>
                    <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="Customer:">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:TextBox ID="cnc_cust_cd" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"  Enabled="false" ReadOnly="True"></asp:TextBox>
                        </td>
                            </tr>
                    <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="">
                            </dx:ASPxLabel>
                            &nbsp;
                        <td align="left">
                            <asp:TextBox ID="cnc_cust_name" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"  Enabled="false" ReadOnly="True"></asp:TextBox>
                        </td>
                        </tr>
                        <tr>
                            <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="">
                            </dx:ASPxLabel>
                            &nbsp;
                        <td align="left">
                            <asp:TextBox ID="cnc_cust_phone" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"  Enabled="false" ReadOnly="True"></asp:TextBox>
                        </td>
                            </tr>
                    <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel21" runat="server" Text="">
                            </dx:ASPxLabel>
                            &nbsp;
                        <td align="left">
                            <asp:TextBox ID="cnc_cust_addr1" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="false" ReadOnly="True"></asp:TextBox>
                        </td>
                        </tr>
                    <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text="">
                            </dx:ASPxLabel>
                            &nbsp;
                        <td align="left">
                            <asp:TextBox ID="cnc_cust_addr2" runat="server" AutoPostBack="true" Width="194px" CssClass="style5" Enabled="false" ReadOnly="True"></asp:TextBox>
                        </td>
                            </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="cnc_commit_btn" runat="server" Text="Commit Refund" >
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="cnc_cancel_btn" runat="server" Text="Exit" >
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            <asp:TextBox ID="cnc_msg" runat="server" AutoPostBack="true" Width="638px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="Larger" ForeColor="Red"></asp:TextBox>
                                                       
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <%--'sabrina add cncrefund end  --%>

   <%-- Alice added for request 167 on Oct 21, 2019 -- allow payment for future sales or for non inventory purposes--%>
    <dxpc:ASPxPopupControl ID="ASPxPopupOtherInvoice" runat="server" CloseAction="CloseButton"  ShowCloseButton="False"
        HeaderText="<%$ Resources:LibResources, Label1019 %>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="600px" CssClass="style5">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlOtherInvoice" DefaultButton="btnReturn" runat="server">
                <table  class="style5" width ="100%" >
                    <tr>
                        <td width="150px">
                             <dx:ASPxButton ID="btn_findCustomer" runat="server" CausesValidation ="false"  Text="<%$ Resources:LibResources, Label208 %>" />
                        </td>
                        <td>
                           
                        </td>
                    </tr>
                    <tr>
                        <td align="left"> <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label132 %>" />:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtCustCode" runat="server" Width="194px" CssClass="style5" MaxLength="10"></asp:TextBox>
                             <asp:CustomValidator ID="CustCdValidator" runat="server" Visible="true" ForeColor ="red" SetFocusOnError="true" ValidateEmptyText="True" ErrorMessage="<%$ Resources:LibResources, Label721 %>"
                                ControlToValidate="txtCustCode" OnServerValidate="CustCdValidator_ServerValidate"  Display="Dynamic" >
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr style ="line-height:10px">
                        <td></td>
                         <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label1019 %>" />:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtInvoiceNum" runat="server" Width="194px" CssClass="style5" MaxLength="12"></asp:TextBox>
                             <asp:CustomValidator ID="InvoiceValidator" runat="server" Visible="true" ForeColor ="red" SetFocusOnError="true" ValidateEmptyText="True" ErrorMessage="<%$ Resources:LibResources, Label1023 %>"
                                ControlToValidate="txtInvoiceNum" OnServerValidate="InvoiceValidator_ServerValidate"  Display="Dynamic" >
                            </asp:CustomValidator>
                             <asp:RegularExpressionValidator
                                    id="regInvoiceNum"
                                    ControlToValidate="txtInvoiceNum"
                                    ErrorMessage="<%$ Resources:LibResources, Label1023 %>"
                                    ValidationExpression="[a-zA-Z0-9]*[^!@%~?:#$%^&*()0']{1,12}$"
                                    SetFocusOnError="true"
                                     Display="Dynamic"
                                    Runat="server" ForeColor ="red" />


                        
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                           <dx:ASPxButton  ID="btnReturn" runat="server"  Text="<%$ Resources:LibResources, Label478 %>"  Width ="80px" CausesValidation ="true"  ToolTip="<%$ Resources:LibResources, Label1022 %>" />
                             <dx:ASPxButton  ID="btnExit" runat="server"  Text="<%$ Resources:LibResources, Label1021 %>" width="80px" CausesValidation ="false"  ToolTip="<%$ Resources:LibResources, Label1020 %>"/>  
                        </td>
                    </tr>
                </table>
                <asp:Label ID="litmessage" runat="server" CssClass="style5" Font-Bold="True"
                    ForeColor="Red"></asp:Label>
                <br />
               
            </dxpc:PopupControlContentControl>

        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <hr />
    <div style="display: inline-block; padding-left: 12px; vertical-align: top;">
        <asp:Label ID="Label1" runat="server" Font-Underline="True" Text="SHU DATE:"
            Width="106px"></asp:Label><br />
        <dx:ASPxLabel ID="txtSHU" runat="server" Text="">
        </dx:ASPxLabel>
    </div>


    <div align="right" style="display: inline-block; float: right; padding-right: 12px;">
        <table>
            <tr>
                <td colspan="2">
                    <b>
                        <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label744 %>">
                        </dx:ASPxLabel>
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="2" nowrap="nowrap">
                    <hr />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:LibResources, Label129 %>">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxLabel ID="lbl_current_balance" runat="server" Text="0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="<%$ Resources:LibResources, Label385 %>">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxLabel ID="lbl_current_pmts_amt" runat="server" Text="0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="<%$ Resources:LibResources, Label328 %>">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxLabel ID="lbl_new_pmts_amt" runat="server" Text="0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>
    <% ' Cash breakdown section %>
    <asp:Panel ID="CashBreakdownPanel" runat="server">
        <table cellpadding="2" runat="server" id="cashBreakdownSection" visible="false"><%-- Cash Breakdown section --%>
            <tr>
                <td colspan="8" style="white-space: nowrap;">
                    <asp:Label ID="cashBreakdownError" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="8" style="white-space: nowrap;">
                    <br /><b><asp:Label ID="lbl_cash_breakdown" runat="server" Text="<%$ Resources:LibResources, Label1014 %>"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right">
                    <asp:Label ID="lbl_cash_breakdown_coins" runat="server" Text="<%$ Resources:LibResources, Label1017 %>"></asp:Label>
                </td>
                <td align="right">=</td>
                <td align="right" style="white-space:nowrap;">
                    <asp:TextBox ID="txt_cash_breakdown_coins_total" runat="server" Width="113px" AutoPostBack="True" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_cash_breakdown_5" runat="server"  Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                </td>
                <td align="center">x</td>
                <td align="right">$5</td>
                <td align="right">=</td>
                <td align="right">
                    <asp:TextBox ID="txt_cash_breakdown_5_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_cash_breakdown_10" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                </td>
                <td align="center">x</td>
                <td align="right">$10</td>
                <td align="right">=</td>
                <td align="right">
                    <asp:TextBox ID="txt_cash_breakdown_10_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_cash_breakdown_20" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                </td>
                <td align="center">x</td>
                <td align="right">$20</td>
                <td align="right">=</td>
                <td align="right">
                    <asp:TextBox ID="txt_cash_breakdown_20_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td align="right" style="white-space: nowrap;">
                    <b><asp:Label ID="lbl_cash_breakdown_total" runat="server" Text="<%$ Resources:LibResources, Label1015 %>"></asp:Label>:</b>
                </td>
                <td>
                    <asp:TextBox ID="txt_cash_breakdown_total" runat="server" Width="113px" AutoPostBack="True" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_cash_breakdown_50" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                </td>
                <td align="center">x</td>
                <td align="right">$50</td>
                <td align="right">=</td>
                <td align="right">
                    <asp:TextBox ID="txt_cash_breakdown_50_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_cash_breakdown_100" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                </td>
                <td align="center">x</td>
                <td align="right">$100</td>
                <td align="right">=</td>
                <td align="right">
                    <asp:TextBox ID="txt_cash_breakdown_100_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_cash_breakdown_1000" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                </td>
                <td align="center">x</td>
                <td align="right">$1000</td>
                <td align="right">=</td>
                <td align="right">
                    <asp:TextBox ID="txt_cash_breakdown_1000_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td align="right" style="white-space: nowrap;">
                    <b><asp:Label ID="lbl_cash_breakdown_change" runat="server" Text="<%$ Resources:LibResources, Label1016 %>"></asp:Label>:</b>
                </td>
                <td>
                    <asp:TextBox ID="txt_cash_breakdown_change" runat="server" Width="113px" AutoPostBack="True" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
