<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="FranchiseItemCreation.aspx.vb" Inherits="FranchiseItemCreation" meta:resourcekey="PageResource1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
     <%-- =============== HEADING ==========================================   --%> 
    <table  width="100%" class="style5">
      <tr>
      <td align="center" > 
      <asp:Label ID="Label1" runat="server" Text="Franchise Item Creation" Font-Bold="True" ></asp:Label>
      </td>
      </tr>
     </table>
       <br />
      <%-- ===============MESSAGES =========lbl_msg ==========================================   --%> 
     <table>
         <tr>
             <td>
     <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" Font-Size="small" ForeColor="Blue" BackColor="White" runat="server"
        Text="" Width="100%"> 
     </dx:ASPxLabel>
            </td>
             </tr>
     </table>
    <br />
      <%-- ===============NEW SKU IND ==========================================   --%> 
     <table>
         <tr>
             <td>
                <%-- Suggested SKU --%>
                <dx:ASPxLabel ID="select_option_lbl" ForeColor="red" Text="Please select an option to proceed : " Font-Bold="true" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
                 <asp:DropDownList ID="select_option_list" width="160px" runat="server" AppendDataBoundItems="true" autopostback="true"  OnSelectedIndexChanged="SelectOptionChanged" >
                </asp:DropDownList>
                </td>
          </tr>
        </table>
    <br />
    <br />
         <%-- ===============NEW SKU, LIST OF SKUS ==========================================   --%> 
    <asp:Panel ID="Panel5" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >
        <table>
        <tr>
             <td>  
                <dx:ASPxLabel ID="new_sku_lbl" text="SKU" Font-Bold="true" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
                 <asp:TextBox ID="new_sku" maxlength="8" width="110px" runat="server"></asp:TextBox>
             </td>
             <td> 
                <%-- SKU_drp_list --%>
                <dx:ASPxLabel ID="sku_drp_lbl" text="SKU" Font-Bold="true" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
                <asp:DropDownList ID="sku_drp_list" width="110px" runat="server" AppendDataBoundItems="true" autopostback="true" 
                     OnSelectedIndexChanged="SkuSelectionChanged"  >
                </asp:DropDownList>
              </td>
            <td> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <%-- buyer approve y, n --%>
                <dx:ASPxLabel ID="buyer_app_lbl" Text="Approve" Font-Bold="true" runat="server">
                </dx:ASPxLabel>
                <asp:DropDownList ID="buyer_app_ind_list" runat="server" AppendDataBoundItems="true" >
                </asp:DropDownList>
              </td>  
       <%-- </tr>
        </table>
        <table>
        <tr> --%>
             <td>                                                         
                <dx:ASPxLabel ID="buyer_cmnt_lbl" Text="Buyer's Comments" Font-Bold="true" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
               <asp:TextBox ID="buyer_cmnt" Width="250px" maxlength="50" runat="server"></asp:TextBox>
              </td>                    
           </tr>
           </table>
    </asp:Panel>
         <%-- ===============COMPANY, STORE ==========================================   --%> 
     <asp:Panel ID="Panel4" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  

         <table style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr>
            <td>
                <%-- Company code --%>
                <dx:ASPxLabel ID="ASPxLabel1" Text="<%$ Resources:LibResources, Label100 %>" Font-Bold="true" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
                <asp:DropDownList ID="srch_co_cd" runat="server" AppendDataBoundItems="true" >
                </asp:DropDownList> &nbsp&nbsp
             <%-- Store  --%>
                <dx:ASPxLabel ID="ASPxLabel2" Text="<%$ Resources:LibResources, Label572 %>" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
                <asp:DropDownList ID="srch_str_cd" runat="server" AppendDataBoundItems="true" >
                </asp:DropDownList>
            </td>
            </tr>
        </table>
        <%-- <br />--%>
        <%-- ================= ve_cd, mjr ==================================================== --%>
       <table style="table-layout:fixed" cellpadding="0" cellspacing="20" >
      <tr>
      <td style="width: 100px;">
                 <%-- ve-cd  --%>
                <dx:ASPxLabel ID="ASPxLabel3" Text="Vendor" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
                <asp:DropDownList ID="ve_cd_drp_list" Width="60" runat="server" AutoPostBack="True" OnSelectedIndexChanged="veCdChanged" AppendDataBoundItems="true" >                
                </asp:DropDownList>
             </td>
              <td style="width: 100px;">
                 <%-- Mjr_cd  --%>
                <dx:ASPxLabel ID="ASPxLabel5" Text="Major" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
                <asp:DropDownList ID="mjr_cd_drp_list" Width="60" runat="server" AppendDataBoundItems="true" 
                    AutoPostBack="True" OnSelectedIndexChanged="MjrCdChangeded" >
                </asp:DropDownList>
         </td>
         </tr>
      <%-- ================= Mnr, Cat, Style, Label, Itm type, Warrant, Treat, Comm============= --%>
         <tr>
      <td style="width: 100px;">
                 <%-- Mnr_cd  --%>
                <dx:ASPxLabel ID="ASPxLabel6" Text="Minor" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
                <asp:DropDownList ID="mnr_cd_drp_list" Width="60" runat="server" Style="position: relative" 
                    AutoPostBack="True" AppendDataBoundItems="true" OnSelectedIndexChanged="MnrCdChanged"  >
                </asp:DropDownList>
         </td>
    <td style="width: 100px;">
                 <%-- cat_cd  --%>
                <dx:ASPxLabel ID="ASPxLabel7" Text="Category" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
                <asp:DropDownList ID="cat_cd_drp_list" Width="60" runat="server" AppendDataBoundItems="true"  >
           </asp:DropDownList>
    </td>
    <td style="width: 100px;">
                 <%-- style  --%>
                <dx:ASPxLabel ID="ASPxLabel9" Text="Style" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
                <asp:DropDownList ID="style_drp_list" Width="60" runat="server" AppendDataBoundItems="true" >
                </asp:DropDownList>
    </td>
    <td style="width: 100px;">
       <%-- label code  --%>
         <dx:ASPxLabel ID="ASPxLabel4" Text="Labels" Font-Bold="true" runat="server" >
         </dx:ASPxLabel>
         <asp:DropDownList ID="label_drp_list" Width="60" runat="server" AppendDataBoundItems="true" >
         </asp:DropDownList>
    </td>
              
    <td style="width: 100px;">
                 <%-- itm type  --%>
               <%--<asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true"  autopostback="true" OnSelectedIndexChanged="BuyerSelected" ID="buyer_drp_list">                   
                  </asp:dropdownlist>--%>
                <dx:ASPxLabel ID="ASPxLabel8" Text="Type" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
                <asp:DropDownList ID="itm_tp_drp_list" Width="60" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ItmTpChanged"  AppendDataBoundItems="true" > 
                </asp:DropDownList>
    </td>
    <td style="width: 100px;">
          <%-- Warranties  --%>
           <dx:ASPxLabel ID="ASPxLabel13" Text="Warranties" Font-Bold="true" runat="server" >
         </dx:ASPxLabel>
         <asp:DropDownList ID="warrant_drp_list"  Width="60" runat="server" Style="position: relative" 
             AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="WarrantSelected" >
         </asp:DropDownList>
     </td>
     <td style="width: 100px;">
         <%-- treatable  --%>
        <dx:ASPxLabel ID="ASPxLabel14" Text="Treatable" Font-Bold="true" runat="server" >
        </dx:ASPxLabel>
        <asp:DropDownList ID="treat_drp_list" Enabled="false" Width="60" runat="server" >
        </asp:DropDownList>
     </td>
     <td style="width: 100px;">
       <%-- comm. code  --%>
         <dx:ASPxLabel ID="ASPxLabel12" Text="Commission" Font-Bold="true" runat="server" >
         </dx:ASPxLabel>
         <asp:DropDownList ID="comm_cd_drp_list" Width="60" runat="server" AppendDataBoundItems="true" >
         </asp:DropDownList>
     </td>
         </tr>
   </table>
   </asp:Panel>
    
      <%-- ================= repl cost, retail price  ==================================================== --%>
   <asp:Panel ID="Panel1" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
    
     <table style="table-layout:fixed" cellpadding="0" cellspacing="2" >
     <tr>
     <td style="width:300px;">
     <%-- Repl cost  --%>
        <dx:ASPxLabel ID="ASPxLabel10" Text="Repl. Cost" Font-Bold="true" runat="server" >
        </dx:ASPxLabel> 
        <asp:TextBox ID="repl_cost" runat="server"></asp:TextBox>
         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="repl_cost" runat="server" 
             ErrorMessage="Invalid Value" Font-Bold="true"  ForeColor="red"  ValidationExpression="\d+"></asp:RegularExpressionValidator>
     </td>
     <td style="width:300px;">
     <%-- Retail price  --%>          
        <dx:ASPxLabel ID="ASPxLabel11" Text="Retail Price" Font-Bold="true" runat="server" >
        </dx:ASPxLabel>
        <asp:TextBox ID="retail_price" runat="server"></asp:TextBox>
         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="retail_price" runat="server" 
         ErrorMessage="Invalid Value" Font-Bold="true"  ForeColor="red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
    </td>
    </tr>
    </table>
     <%-- =================Item Des, vsn  ================================================ --%>
    <table style="table-layout:fixed" cellpadding="0" cellspacing="2" >
    <tr>
     <td style="width:600px;">
     <%-- Item desc  --%>
      <dx:ASPxLabel ID="ASPxLabel19" Text="Item Description" Font-Bold="true" runat="server" >
        </dx:ASPxLabel> 
        <asp:TextBox ID="itm_des" maxlength="30" Width="100%" runat="server"></asp:TextBox>
     </td>
     <td style="width:600px;">
     <%-- VSN  --%>
        <dx:ASPxLabel ID="ASPxLabel18" Text="VSN/Model" Font-Bold="true" runat="server" >
        </dx:ASPxLabel> 
        <asp:TextBox ID="vsn" MaxLength="30" width="100%" runat="server"></asp:TextBox>
     </td>
     </tr>
     </table>
     <%-- =================group name, family cd, price tag des=============================== --%>
    <table style="table-layout:fixed" cellpadding="0" cellspacing="0" >
     <tr>
      <td style="width:250px;">
     <%-- group name  --%>
       <dx:ASPxLabel ID="ASPxLabel20" Text="Group Name" Font-Bold="true" runat="server" >
        </dx:ASPxLabel>
        <asp:TextBox ID="grp_name" MaxLength="15" runat="server"></asp:TextBox>
      </td>
     <td style="width:250px;">
     <%-- family  --%>
        <dx:ASPxLabel ID="ASPxLabel16" Text="Family Code" Font-Bold="true" runat="server"> </dx:ASPxLabel> 
        <asp:TextBox ID="family_cd" maxlength="5" runat="server"></asp:TextBox>
    </td>
     <td style="width:250px;">
     <%-- price tag desc  --%>
        <dx:ASPxLabel ID="ASPxLabel17" Text="Price Tag Des." Font-Bold="true" runat="server" >
        </dx:ASPxLabel> 
        <asp:TextBox ID="price_tag_des" MaxLength="15" runat="server"></asp:TextBox>
     </td>
         <td style="width:250px;">
            <%--  <td style="width:auto">  ErrorMessage="Error"--%>
     <%-- weight  --%>
        <dx:ASPxLabel ID="ASPxLabel21" Text="Weight" Font-Bold="true" runat="server" >
        </dx:ASPxLabel> 
        <asp:TextBox ID="weight" MaxLength="5" runat="server"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="weight" runat="server" 
         ErrorMessage="Invalid Value" Display="Dynamic" ForeColor="red" Font-Bold="true"  ValidationExpression="\d+"></asp:RegularExpressionValidator>
     </td>
     </tr> 
     </table>
       <br />
       <br />
    </asp:Panel>
    
      <%-- ================= PANEL =============================== --%>
    <asp:Panel ID="Panel2" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
     <table>
    <%-- =================SPIFF =============================== --%>
    <tr>
   <td>
     <%-- spiff amount  --%>
        <dx:ASPxLabel ID="ASPxLabel15" Text="Spiff Amount" Font-Bold="true" runat="server" >
        </dx:ASPxLabel> 
        <asp:TextBox ID="spiff_amt" runat="server"></asp:TextBox>
       <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="spiff_amt" runat="server" 
             ErrorMessage="Invalid Value" ForeColor="red" Font-Bold="true"  ValidationExpression="\d+"></asp:RegularExpressionValidator>
    </td>
      
    <%-- =================SPIFF =============================== --%>
    <%-- spiff eff date  --%> 
    <td>    
     <asp:Label ID="Label5" runat="server" style="text-align: start" Text="Effective DT" ></asp:Label>
      <asp:Calendar font-size="Xx-Small" hieght="1px" Width="1px" ID="spiff_eff_dt" 
          DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server">
      <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
      <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
     </asp:Calendar>
    </td>

    <%-- spiff end date  --%>
    <td>
     <asp:Label ID="lbl_end_dt" runat="server" style="text-align: start" Text="END DT" ></asp:Label>
    <asp:Calendar Font-Size="XX-Small" hieght="1px" Width="1px" 
     DisplayDateFormat="MM/dd/yyyy" ID="spiff_end_dt" ForeColor="Black" runat="server">
     <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
     <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
     </asp:Calendar>
    </td>

    
    </tr>
         <tr>
             <td>
                 <asp:Label ID="Label2" runat="server" visible="false" Text=""></asp:Label>
             </td>
             <td>
                 <asp:Button ID="btn_reset_dates" font-size="XX-Small"  Font-Bold="true" runat="server" Text="Reset Dates" />
             </td>
         </tr>
    </table>
         </asp:Panel>
     <%-- ==================================comp sku  detail=========================================   --%>  
          <dxpc:ASPxPopupControl ID="ASPxPopupPkgComp" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Package Component Detail" Height="80px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="750px"
        CssClass="style5">
         <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" DefaultButton="btnSubmit" runat="server">

        <asp:Panel ID="Panel3" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
<%-- message --%>
            <br />
            <table>
                <tr>
                    <td>
                        <asp:TextBox ID="new_sku2" ReadOnly="true" Enabled="false" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
     <table>
         
         <dx:ASPxLabel ID="lbl_msg2" Font-Bold="True" ForeColor="Red" BackColor="White" runat="server"
        Text="" Width="100%"> 
        </dx:ASPxLabel>
         
     </table>
            <br />
<%-- gridview --%>
       <dx:ASPxGridView Visible="false" ID="GridView1" Settings-ShowFooter="true" runat="server" keyfieldname="COMP_SKU" AutoGenerateColumns="False" 
          Width="98%" ClientInstanceName="GridView1" oncelleditorinitialize="GridView1_CellEditorInitialize"  
            OnRowUpdating="ASPxGridView1_RowUpdating" OnRowInserting="ASPxGridView1_RowInserting"  
            OnRowValidating="rowvalidation" OnRowDeleting="ASPxGridView1_Rowdeleting" >

         <SettingsEditing mode="EditForm" NewItemRowPosition="top" />
           
        
        <Columns>
          <dx:GridViewDataTextColumn Caption="Component SKU" FieldName="COMP_SKU" VisibleIndex="5">
                <PropertiesTextEdit Width="160" MaxLength="16">
                    <ValidationSettings>
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="QTY in PKG" FieldName="COMP_SKU_QTY" VisibleIndex="5">
                <PropertiesTextEdit Width="100"  MaxLength="10">
                    <ValidationSettings>
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="\d+" ErrorText="<%$ Resources:LibResources, Label909 %>" />
                    </ValidationSettings>
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Retail" FieldName="COMP_SKU_RTL" VisibleIndex="5">
                <PropertiesTextEdit Width="120"  MaxLength="12">
                    <ValidationSettings>
                        <%-- "^-?\d*\.{0,1}\d+$" --%>
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="\d+" ErrorText="<%$ Resources:LibResources, Label909 %>" />
                    </ValidationSettings>
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Ext. Retail" FieldName="COMP_SKU_EXT_RTL" VisibleIndex="5">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="16">
                </PropertiesTextEdit>
              </dx:GridViewDataTextColumn>
            <%-- ShowDeleteButton="True"  --%>
          <dx:GridViewCommandColumn ShowEditButton="true" ShowDeleteButton="True"  ShowNewButtonInHeader="True" VisibleIndex="14"/>
        </Columns>
        
        <SettingsBehavior  AllowSort="False" />
        <SettingsPager Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />
        <SettingsBehavior  ConfirmDelete="true"    />
       <%-- <SettingsText  ConfirmDelete="Please press 'OK' to confirm and Delete this record" />--%>
    </dx:ASPxGridView>


            <%-- gridview2 browse --%>
       <dx:ASPxGridView Visible="false" ID="GridView2" Settings-ShowFooter="true" runat="server" keyfieldname="COMP_SKU" AutoGenerateColumns="False" 
          Width="98%" ClientInstanceName="GridView2" >

         <SettingsEditing mode="EditForm" NewItemRowPosition="top" />
        <Columns>
          <dx:GridViewDataTextColumn Caption="Component SKU" FieldName="COMP_SKU" VisibleIndex="5">
                <PropertiesTextEdit Width="160" MaxLength="16">
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="QTY in PKG" FieldName="COMP_SKU_QTY" VisibleIndex="5">
                <PropertiesTextEdit Width="100"  MaxLength="10">
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Retail" FieldName="COMP_SKU_RTL" VisibleIndex="5">
                <PropertiesTextEdit Width="120"  MaxLength="12">
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Ext. Retail" FieldName="COMP_SKU_EXT_RTL" VisibleIndex="5">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="16">
                </PropertiesTextEdit>
              </dx:GridViewDataTextColumn>
            <%-- ShowDeleteButton="True"  --%>
         <%-- <dx:GridViewCommandColumn ShowEditButton="true" ShowDeleteButton="True"  ShowNewButtonInHeader="True" VisibleIndex="14"/>--%>
        </Columns>
        
        <%--<SettingsBehavior  AllowSort="False" />
        <SettingsPager Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />
        <SettingsBehavior  ConfirmDelete="true"    />--%>
       <%-- <SettingsText  ConfirmDelete="Please press 'OK' to confirm and Delete this record" />--%>
    </dx:ASPxGridView>
    </asp:Panel>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
      <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btn_save" runat="server" Text="SAVE">
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td> 
            <td>
                <dx:ASPxButton ID="btn_comp_sku" runat="server" Text="COMP. SKU">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label888 %>">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
   
    <div>
    <br />
   </div>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
