<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Lead_Book_Transfer.aspx.vb" Inherits="Lead_Book_Transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frmsubmit" runat="server" class="style5">
        &nbsp;<asp:Label ID="Label2" runat="server" Text="Transfer From:" CssClass="style5"></asp:Label>&nbsp;
        <asp:DropDownList ID="cbo_original" runat="server" Width="202px" AutoPostBack="true"
            CssClass="style5">
        </asp:DropDownList>
        &nbsp;
        <br />
        &nbsp;<asp:Label ID="Label1" runat="server" Text="Transfer To:" CssClass="style5"></asp:Label>
        &nbsp; &nbsp; &nbsp;<asp:DropDownList ID="cbo_new" runat="server" Width="202px" CssClass="style5">
        </asp:DropDownList>
        <br />
        <br />
        <asp:CheckBox ID="chk_converted" runat="server" AutoPostBack="True" Text="Transfer Converted Orders" />
        <br />
        <br />
        <asp:Label ID="lbl_information" runat="server" Width="412px" CssClass="style5"></asp:Label>
        <br />
        <br />
        <asp:Button ID="btn_transfer" runat="server" Text="Transfer Prospects" CssClass="style5" />
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="style5">
        This process transfers prospect data from one salesperson to another.&nbsp; Converted
        orders will remain with the original salesperson unless indicated.
    </div>
</asp:Content>
