<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="NASOT.aspx.vb" Inherits="NASOT" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table>
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td align="right">
                            <dx:ASPxLabel ID="lbl_company_code" runat="server" meta:resourcekey="lbl_company_code"></dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_company_code" runat="server" Width="50PX" Enabled="false"></dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td align="right">
                            <dx:ASPxLabel ID="lbl_store_cd" runat="server" meta:resourcekey="lbl_store_cd"></dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_store_cd" runat="server"
                                Width="350px" IncrementalFilteringMode="Contains" ClientInstanceName="cbo_store_cd">
                                <ClientSideEvents GotFocus="function(s, e) {cbo_store_cd.SelectAll();}" />
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <dx:ASPxLabel ID="lbl_days_display" runat="server" meta:resourcekey="lbl_days_display"></dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_display_days" runat="server" ValueType="System.String" DropDownStyle="DropDownList" Width="100px">
                                <Items>
                                    <dx:ListEditItem Text="ALL" Value="1" Selected="true" />
                                    <dx:ListEditItem Text="1-7 Days" Value="2" />
                                    <dx:ListEditItem Text="8-14 Days" Value="3" />
                                    <dx:ListEditItem Text=">14 Days" Value="4" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="btn_search" runat="server" meta:resourcekey="lbl_btn_search" AutoPostBack="true" Width="100px"></dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btn_clr_screen" runat="server" meta:resourcekey="lbl_btn_clear_screen" AutoPostBack="true" Width="100px"></dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>

        <tr align="center">
            <td>
                <dx:ASPxGridView ID="GV_NASOT_SUMMARY" runat="server" AutoGenerateColumns="False" EnableRowsCache="true" Settings-UseFixedTableLayout="true"
                    KeyFieldName="STORE_CD" Width="1250px"
                    OnDataBinding="GV_NASOT_DataBinding">
                    <SettingsPager Position="TopAndBottom">
                        <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                    </SettingsPager>

                    <TotalSummary>
                        <dx:ASPxSummaryItem FieldName="ITEMS" ShowInColumn="# of Items Reporting" SummaryType="Sum" DisplayFormat="{0:n0}" />
                        <dx:ASPxSummaryItem FieldName="COST" ShowInColumn="Cost of Items Reporting" SummaryType="Sum" DisplayFormat="{0:c}" />
                    </TotalSummary>
                    <Columns>
                        <dx:GridViewDataTextColumn meta:resourcekey="gv_col_store_code" FieldName="STORE_CD" Visible="True" VisibleIndex="0" Width="100px" />
                        <dx:GridViewDataTextColumn meta:resourcekey="gv_col_days_reporting" FieldName="DAYS" Visible="True" VisibleIndex="1" Width="150px" />
                        <dx:GridViewDataTextColumn meta:resourcekey="gv_col_items_reporting" FieldName="ITEMS" Visible="True" VisibleIndex="2" Width="150px">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="lbl_sum_of_item_count" runat="server" Text="" OnLoad="lbl_sum_of_item_count_load">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn meta:resourcekey="gv_col_cost_of_items_reporting" FieldName="COST" Visible="True" VisibleIndex="3" Width="150px">
                            <PropertiesTextEdit DisplayFormatString="{0:c}"></PropertiesTextEdit>
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="lbl_sum_of_item_cost" runat="server" Text="" OnLoad="lbl_sum_of_item_cost_load">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="GV_NASOT_DETAIL" runat="server" AutoGenerateColumns="False" OnBeforePerformDataSelect="PrepareDetailGrid"
                                Settings-UseFixedTableLayout="true" Width="100%">
                                <SettingsPager Position="Top">
                                    <PageSizeItemSettings Items="5, 10, 20, 50" Visible="true" ShowAllItem="true" />
                                </SettingsPager>
                                <Columns>
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_item_code" FieldName="ITM_CD" Visible="True" VisibleIndex="0" Width="60px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_refid" FieldName="RF_ID_CD" Visible="True" VisibleIndex="0" Width="50px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_days_in" FieldName="DAYS_IN" Visible="True" VisibleIndex="1" Width="35px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_nas" FieldName="NAS" Visible="True" VisibleIndex="2" Width="20px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_description" FieldName="DES" Visible="True" VisibleIndex="4" Width="120px">
                                        <CellStyle Wrap="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_location" FieldName="LOC_CD" Visible="True" VisibleIndex="5" Width="55px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_cost" FieldName="COST" Visible="True" VisibleIndex="6" Width="40px">
                                        <PropertiesTextEdit DisplayFormatString="{0:c}"></PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_sales_order" FieldName="DEL_DOC_NUM" Visible="True" VisibleIndex="7" Width="65px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_nas_out" FieldName="NAS_OUT_ID" Visible="True" VisibleIndex="8" Width="50px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_nas_comments" FieldName="NAS_OUT_CMNT" Visible="True" VisibleIndex="8" Width="150px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_last_action_date" FieldName="LAST_ACTN_DT" Visible="True" VisibleIndex="9" Width="45px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_last_cycle" FieldName="LAST_CYCLE_DT" Visible="True" VisibleIndex="10" Width="45px" />
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_col_last_action" FieldName="LAST_ACTN_EMP_CD" Visible="True" VisibleIndex="11" Width="45px" />
                                </Columns>
                                <SettingsDetail />
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                    <SettingsDetail ShowDetailRow="true" />
                </dx:ASPxGridView>
            </td>
        </tr>
    </table>
    <dx:ASPxLabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:ASPxLabel>
    <br />
    <br />
    <div runat="server" id="div_version">
        <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
    </dx:ASPxLabel>
    </div>
</asp:Content>
