<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NCT_Test.aspx.vb" Inherits="NCT_Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style.css" type="text/css" rel="stylesheet">
    <title></title>
</head>
<body class="style5">
    <form id="form1" runat="server">
        <div width="100%" align="center">
            <br />
            <br />
            <img src="images/nct.jpg" />
            <br />
            <br />
            <b>Test System</b>
            <table class="style5">
                <tr>
                    <td align="left">
                        Bank Routing #:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="TextBox1" runat="server" Width="194px" CssClass="style5" MaxLength="9"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Account #:</td>
                    <td align="left">
                        <asp:TextBox ID="TextBox2" runat="server" Width="194px" CssClass="style5" MaxLength="12"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                        Check #:</td>
                    <td align="left">
                        <asp:TextBox ID="TextBox3" runat="server" Width="194px" CssClass="style5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                        Amount:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txt_amt" runat="server" Width="194px" CssClass="style5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Type:
                    </td>
                    <td align="left">
                        &nbsp;<asp:DropDownList ID="DropDownList1" runat="server" CssClass="style5">
                            <asp:ListItem Selected="True" Value="01">Personal</asp:ListItem>
                            <asp:ListItem Value="90">Business</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="left">
                        Store:
                    </td>
                    <td align="left">
                        &nbsp;<asp:DropDownList ID="cbo_store" runat="server" CssClass="style5">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lbl_Full_Name" runat="server" Width="286px"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="100px" CssClass="style5" />&nbsp;
                        <asp:Button ID="btn_Clear" runat="server" Text="Clear" Width="100px" CssClass="style5" /></td>
                </tr>
            </table>
            <br />
            <br />
            <b><u>Response:</u></b>
            <br />
            <asp:Literal ID="NCT_Response" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
