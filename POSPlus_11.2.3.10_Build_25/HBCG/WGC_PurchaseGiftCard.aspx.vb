Imports System.Xml
Imports System.Data.OracleClient
Imports World_Gift_Utils
Imports HBCG_Utils


Imports System
Imports System.Net
Imports System.Security.Cryptography
Imports SD_Utils

Imports Renci.SshNet   'lucy add the 5 lines
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp


Partial Class WGC_PurchaseGiftCard
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private PaymentBiz As PaymentBiz = New PaymentBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.Theme = ConfigurationManager.AppSettings("app_theme").ToString
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CreateCustomer()                    'sabrina R1922
        Session("gift_card_purchase") = "Y" 'sabrina R1922

        'txt_amount.Focus()   'this is the Account field
        '    PopulateTranTypes()
        ' PopulateStores()
        If Not IsPostBack Then
            PopulateMop()
        End If

        bindgrid()

        If Request("GIFT") = "TRUE" Then
            Session("cust_cd") = Request("Custid")
        End If
        If Not isEmpty(Session("cust_cd")) Then
            txt_cust_cd.Text = Session("cust_cd")
        End If
        Dim store As StoreData
        If Not isEmpty(Session("HOME_STORE_CD")) Then
            store = theSalesBiz.GetStoreInfo(UCase(Session("HOME_STORE_CD").ToString))
            Session("CO_CD") = store.coCd
        End If

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        ClearObjects()
        lbl_warnings.Text = ""
        lbl_response.Text = ""

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        lbl_response.Text = ""
        ' Add2 records in AR_TRN, remove payment transaction
        If isEmpty(txt_gift_card.Text) Then
            ' lbl_response.Text = "You must enter or swipe a gift card " + txt_gift_card.Text.ToString
            lbl_response.Text = Resources.LibResources.Label725 + txt_gift_card.Text.ToString
            Exit Sub
        End If
        If Session("subtotal") = 0 Then
            ' lbl_response.Text = "You must make a payment before purchasing the gift card " + txt_gift_card.Text.ToString
            lbl_response.Text = Resources.LibResources.Label726 + txt_gift_card.Text.ToString
            Exit Sub
        End If
       
        'Dim strIP As String

        'strIP = GetIPAddress()
        'strIP = Request.ServerVariables("REMOTE_ADDR")
        'strIP = Session("clientip")
        Dim v_term_id As String

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Daniela Oct 21
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            lbl_response.Text = "Invalid Terminal ID " & v_term_id
            Exit Sub
        End If

        ' Call STD_BRK_POS.SENDSUBTP4 to Issue new card and Increment Trans type M3
        Try
            ' Dim giftc As String = giftcard_process(txt_gift_card.Text.ToString.Trim, "M3", get_term_id(), "124", Session("subtotal"))
            'lbl_response.Text = "Gift Card purchase status: " + giftc
            Dim giftc As String = giftcard_process(txt_gift_card.Text.ToString.Trim, "M3", v_term_id, "124", Session("subtotal"))
            If InStr(giftc, "DECLINED") > 0 Then
                ' Transaction was declined
                ' lbl_response.Text = "Error processing gift card " + giftc
                lbl_response.Text = Resources.LibResources.Label717 + giftc
                Exit Sub
            End If
            ' lbl_response.Text = "Gift Card Purchase status: " + giftc
            lbl_response.Text = Resources.LibResources.Label720 + giftc
        Catch ex As Exception
            ' lbl_response.Text = "Error processing gift card."
            lbl_response.Text = Resources.LibResources.Label717
            Exit Sub
        End Try

        ' Add 2 records in AR_TRN table, MOP codes and GCS
        Try
            If isEmpty(Session("CO_CD")) Then
                Dim store As StoreData = theSalesBiz.GetStoreInfo(UCase(Session("HOME_STORE_CD").ToString))
                Session("CO_CD") = store.coCd
            End If
            Dim pmtStore As String
            If Not Session("clientip") Is Nothing Then
                pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
            Else ' if not go global then default to remote ip
                pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
            End If
            If pmtStore & "" = "" Then
                pmtStore = Session("HOME_STORE_CD")
            End If
            add_ar_record(txt_gift_card.Text, "GCS", pmtStore)
            add_ar_mop(txt_gift_card.Text, "PMT", pmtStore)
        Catch ex As Exception
            ' lbl_response.Text = "Error saving AR Transactions " + ex.ToString
            lbl_response.Text = Resources.LibResources.Label719 + ex.ToString
            Exit Sub
        End Try

        ' Print gift card receipt

        Dim doc As String = "GIFT CARD PAYMENT"
        Dim signonusrcd As String = Session("emp_cd")
        Dim doc_seq_num As String = ""
        If (Not isEmpty(Session("gc_del_doc_num"))) Then
            doc_seq_num = Right(Session("gc_del_doc_num"), 4)
        End If
        Dim custcd As String = Session("cust_cd")
        Dim cocd As String = Session("CO_CD")

        'Daniela french lang parameter
        Dim cult As String = UICulture
        Dim ufm As String = "GCE"
        If InStr(cult.ToLower, "fr") >= 1 Then
            ufm = "GCF"
        End If

        Try
            print_giftcard(doc, signonusrcd, doc_seq_num, custcd, cocd, ufm)
        Catch ex As Exception
            ' lbl_response.Text = "Error printing Gift Card Receipt for parameters: " + doc + "," + signonusrcd + "," + doc_seq_num +
            lbl_response.Text = Resources.LibResources.Label718 + ": " + doc + "," + signonusrcd + "," + doc_seq_num +
                          "," + custcd + "," + cocd + "," + ufm + " Error:" + ex.ToString
            Exit Sub
        End Try

        ' Delete from payment table for the current session
        Try
            delete_records()
        Catch ex As Exception
            ' lbl_response.Text = "Error deleting Payment Transactions " + ex.ToString
            lbl_response.Text = Resources.LibResources.Label716 + ex.ToString
            Exit Sub
        End Try

        '  lbl_response.Text = lbl_response.Text & "<br /><b>Receipt Created: " & Session("gc_del_doc_num") & "</b>"
        lbl_response.Text = lbl_response.Text & "<br /><b>Resources.LibResources.Label723  " & Session("gc_del_doc_num") & "</b>"

        Session("gc_del_doc_num") = ""
        Session("cust_cd") = ""
        Session("del_doc_num") = ""
        Session("subtotal") = ""
        Session("gift_card_balance") = ""
        txt_amount.Text = ""
        txt_cust_cd.Text = ""
        txt_gift_card.Text = ""
        Session("gift_card_purchase") = ""   'sabrina R1922

        bindgrid()

    End Sub


 

    Protected Sub btnReload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReload.Click
        lbl_response.Text = ""

        If isEmpty(txt_gift_card.Text) Then
            ' lbl_response.Text = "You must enter or swipe a gift card " + txt_gift_card.Text.ToString
            lbl_response.Text = Resources.LibResources.Label725 + txt_gift_card.Text.ToString
            Exit Sub
        End If
        If Session("subtotal") = 0 Then
            ' lbl_response.Text = "You must make a payment before reloading the gift card " + txt_gift_card.Text.ToString
            lbl_response.Text = Resources.LibResources.Label726 + txt_gift_card.Text.ToString
            Exit Sub
        End If

        'Dim strIP As String

        'strIP = GetIPAddress()
        'strIP = Request.ServerVariables("REMOTE_ADDR")
        'strIP = Session("clientip")

        Dim v_term_id As String

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Daniela Oct 21
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            lbl_response.Text = "Invalid Terminal ID " & v_term_id
            Exit Sub
        End If

        ' Call STD_BRK_POS.SENDSUBTP4 to Issue new card and Increment Trans type M3
        Try
            Dim giftc As String = giftcard_process(txt_gift_card.Text.ToString.Trim, "Y3", v_term_id, "124", Session("subtotal"))
            If InStr(giftc, "DECLINED") > 0 Then
                ' Transaction was declined
                ' lbl_response.Text = "Error processing gift card " + giftc
                lbl_response.Text = Resources.LibResources.Label717 + giftc
                Exit Sub
            End If
            ' lbl_response.Text = "Total Gift Card balance: " + Session("gift_card_balance")
            lbl_response.Text = Resources.LibResources.Label724 + ":" + Session("gift_card_balance")
        Catch ex As Exception
            '  lbl_response.Text = "Error processing gift card."
            lbl_response.Text = Resources.LibResources.Label717
            Exit Sub
        End Try

        ' Add records in AR_TRN table

        Try
            If isEmpty(Session("CO_CD")) Then
                Dim store As StoreData = theSalesBiz.GetStoreInfo(UCase(Session("HOME_STORE_CD").ToString))
                Session("CO_CD") = store.coCd
            End If
            'Session("giftcardno") = txt_gift_card.Text.ToString.Trim
            Dim pmtStore As String
            If Not Session("clientip") Is Nothing Then
                pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
            Else ' if not go global then default to remote ip
                pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
            End If
            If pmtStore & "" = "" Then
                pmtStore = Session("HOME_STORE_CD")
            End If

            add_ar_record(txt_gift_card.Text, "GCS", pmtStore)
            add_ar_mop(txt_gift_card.Text, "PMT", pmtStore)
        Catch ex As Exception
            ' lbl_response.Text = "Error saving AR Transactions " + ex.ToString
            lbl_response.Text = Resources.LibResources.Label719 + ex.ToString
            Exit Sub
        End Try

        ' Print gift card receipt

        Dim doc As String = "GIFT CARD PAYMENT"
        Dim signonusrcd As String = Session("emp_cd")
        Dim doc_seq_num As String = ""
        If (Not isEmpty(Session("gc_del_doc_num"))) Then
            doc_seq_num = Right(Session("gc_del_doc_num"), 4)
        End If
        Dim custcd As String = Session("cust_cd")
        Dim cocd As String = Session("CO_CD")
        'Daniela french lang parameter
        Dim cult As String = UICulture
        Dim ufm As String = "GCE"
        If InStr(cult.ToLower, "fr") >= 1 Then
            ufm = "GCF"
        End If


        Try
            print_giftcard(doc, signonusrcd, doc_seq_num, custcd, cocd, ufm)
        Catch ex As Exception
            ' lbl_response.Text = "Error printing Gift Card Receipt for parameters: " + doc + "," + signonusrcd + "," + doc_seq_num +
            lbl_response.Text = Resources.LibResources.Label718 + doc + "," + signonusrcd + "," + doc_seq_num +
                           "," + custcd + "," + cocd + "," + ufm + " Error:" + ex.ToString
            Exit Sub
        End Try

        ' Delete from payment table for the current session
        Try
            delete_records()
        Catch ex As Exception
            '  lbl_response.Text = "Error deleting Payment Transactions " + ex.ToString
            lbl_response.Text = Resources.LibResources.Label716 + ex.ToString
            Exit Sub
        End Try

        ' lbl_response.Text = lbl_response.Text & "<br /><b>Receipt Created: " & Session("gc_del_doc_num") & "</b>"
        lbl_response.Text = lbl_response.Text & "<br /><b>Resources.LibResources.Label723  " & Session("gc_del_doc_num") & "</b>"

        Session("cust_cd") = ""
        Session("gc_del_doc_num") = ""
        Session("del_doc_num") = ""
        Session("subtotal") = ""
        Session("gift_card_balance") = ""
        txt_amount.Text = ""
        txt_cust_cd.Text = ""
        txt_gift_card.Text = ""
        Session("gift_card_purchase") = ""   'sabrina R1922

        bindgrid()

    End Sub

    Protected Sub btnGiftCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGiftCard.Click
        lbl_response.Text = ""
        Dim giftcard_num As String = ""
        If (Not isEmpty(txt_gift_card.Text)) Then
            txt_gift_card.Text = ""
        Else

        End If

        'Dim strIP As String

        'strIP = GetIPAddress()
        'strIP = Request.ServerVariables("REMOTE_ADDR")
        'strIP = Session("clientip")

        Dim v_term_id As String

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        Try
            txt_gift_card.Text = giftcard_process("", "O3", v_term_id, "124", "")
        Catch ex As Exception
            ' lbl_response.Text = "Error processing gift card."
            lbl_response.Text = Resources.LibResources.Label717
            Exit Sub
        End Try

    End Sub

    Protected Sub btnProcessPaymnt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessPaymnt.Click
        lbl_response.Text = ""
        Dim limitMinLeon As Integer = 5
        Dim limitMaxLeon As Integer = 5000
        Dim limitMinBrick As Integer = 10
        Dim limitMaxBrick As Integer = 2500
        If Not IsNumeric(txt_amount.Text) Then
            ' lbl_response.Text = "You must enter a valid amount " + txt_amount.Text
            lbl_response.Text = Resources.LibResources.Label729 + txt_amount.Text
            Exit Sub
        Else
            If Session("CO_CD") = "LFL" Then
                If Not InRange(CDbl(txt_amount.Text), limitMinLeon, limitMaxLeon) Then
                    ' lbl_response.Text = "Purchase amount must be between " + limitMinLeon.ToString + " and " + limitMaxLeon.ToString
                    lbl_response.Text = Resources.LibResources.Label722 + limitMinLeon.ToString + " and " + limitMaxLeon.ToString
                    Exit Sub
                End If
            Else
                If Not InRange(CDbl(txt_amount.Text), limitMinBrick, limitMaxBrick) Then
                    ' lbl_response.Text = "Purchase amount must be between " + limitMinBrick.ToString + " and " + limitMaxBrick.ToString
                    lbl_response.Text = Resources.LibResources.Label722 + limitMinBrick.ToString + " and " + limitMaxBrick.ToString
                    Exit Sub
                End If
            End If
        End If
        If isEmpty(txt_cust_cd.Text) Then
            If isEmpty(Session("cust_cd")) Then
                ' lbl_response.Text = "You must enter a valid Customer Code " + txt_cust_cd.Text
                lbl_response.Text = Resources.LibResources.Label728 + txt_cust_cd.Text
                Exit Sub
            Else
                txt_cust_cd.Text = Session("cust_cd")
            End If
        End If
        If Not isEmpty(txt_cust_cd.Text) Then
            Session("cust_cd") = txt_cust_cd.Text.ToString.ToUpper
        End If
        'If Not isEmpty(txt_gift_card.Text) Then
        'Session("giftcardno") = txt_gift_card.Text.ToString.Trim
        'End If
        If isEmpty(DropDownMop.SelectedItem.Value) Then
            ' lbl_response.Text = "You must select a Method of Payment"
            lbl_response.Text = Resources.LibResources.Label727
            Exit Sub
        End If
        ' Set the del_doc_num to be inserted into elec_ pos_electronic_transaction
        If isEmpty(Session("gc_del_doc_num")) Then
            If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
            Dim pmtStore As String
            If Not Session("clientip") Is Nothing Then
                pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
            Else ' if not go global then default to remote ip
                pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
            End If
            If pmtStore & "" = "" Then
                pmtStore = Session("HOME_STORE_CD")
            End If
            Dim soKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(pmtStore, Session("tran_dt"))
            Dim next_del_doc_num As String = soKeyInfo.soDocNum.delDocNum
            Session("gc_del_doc_num") = next_del_doc_num
        End If
        Session("del_doc_num") = Session("gc_del_doc_num")
        If Not DropDownMop.SelectedItem.Value = "CS" Then
            ASPxPopupControl1.ContentUrl = "Credit_Card_Processing.aspx?page=WGC_PurchaseGiftCard.aspx&AMT=" & CDbl(txt_amount.Text) & "&MOP_CD=" & DropDownMop.SelectedItem.Value & "&DES=" & DropDownMop.SelectedItem.Text
            ASPxPopupControl1.ShowOnPageLoad = True
        Else
            Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim oraCmd As OracleCommand
            Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim oraTransaction As OracleTransaction
            connErp.Open()
            connLocal.Open()
            oraCmd = DisposablesManager.BuildOracleCommand

            oraCmd.Connection = connErp
            oraTransaction = connLocal.BeginTransaction
            With oraCmd
                .Transaction = oraTransaction
                .Connection = connLocal
                'oraCmd.CommandText = "INSERT INTO payment (sessionid, MOP_CD, AMT, DES, MOP_TP, APPROVAL_CD) values (:SESSIONID, :MOP, :AMT, :DES, :BC, :APPROVAL_CD)"
                oraCmd.CommandText = "INSERT INTO payment (sessionid, MOP_CD, AMT, DES, MOP_TP) values (:SESSIONID, :MOP, :AMT, :DES, :BC)"
                oraCmd.Parameters.Add(":SESSIONID", OracleType.VarChar)
                oraCmd.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
                oraCmd.Parameters.Add(":MOP", OracleType.VarChar)
                oraCmd.Parameters(":MOP").Value = DropDownMop.SelectedItem.Value
                oraCmd.Parameters.Add(":AMT", OracleType.VarChar)
                oraCmd.Parameters(":AMT").Value = CDbl(txt_amount.Text)
                oraCmd.Parameters.Add(":DES", OracleType.VarChar)
                oraCmd.Parameters(":DES").Value = DropDownMop.SelectedItem.Text
                oraCmd.Parameters.Add(":BC", OracleType.VarChar)
                oraCmd.Parameters(":BC").Value = ""
                'oraCmd.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
                'oraCmd.Parameters(":APPROVAL_CD").Value = Session("app_cd")
            End With
            oraCmd.ExecuteNonQuery()
            oraTransaction.Commit()
            connErp.Close()
            connLocal.Close()
        End If
        txt_amount.Text = ""
        bindgrid()

    End Sub

    Private Sub ClearObjects()
        txt_amount.Text = ""
        txt_amount.Enabled = True
        txt_cust_cd.Text = ""
        txt_gift_card.Text = ""
        'txt_gift_card.Enabled = True
        txt_amount.Focus()
    End Sub


    Private Function GetCustomerName(ByVal custCd As String) As String
        Dim custName As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT CUST_CD, FNAME, LNAME FROM CUST WHERE CUST_CD=upper(:CUST_CD)"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
            dbCommand.Parameters(":CUST_CD").Value = custCd
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                custName = dbReader.Item("LNAME").ToString & " " & dbReader.Item("CUST_CD").ToString
            Else
                '  lbl_response.Text = "Invalid Customer Code " + custCd
                lbl_response.Text = Resources.LibResources.Label721 + custCd
            End If
            dbReader.Close()
            dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try
        Return custName
    End Function


    Protected Sub txt_cust_cd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cust_cd.TextChanged
        txt_amount.Enabled = True
        Dim custName As String = LeonsBiz.GetCustomerName(txt_cust_cd.Text)
        If (String.IsNullOrEmpty(custName)) Then
            txt_cust_cd.Text = String.Empty
            txt_cust_cd.Focus()
            ' lbl_response.Text = "Invalid Customer Code " + custName
            lbl_response.Text = Resources.LibResources.Label721 + custName
        Else
            Session("cust_cd") = txt_cust_cd.Text.ToString
        End If
    End Sub




    Private Sub PopulateMop()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim ds As New DataSet

        'Dim SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc from store where store_cd in (select store_cd from frtbl) order by store_cd"

        'Dim SQL = "SELECT MOP_CD, DES FROM MOP WHERE MOP_CD in ('CS', 'DC', 'VI', 'MC', 'AMX', '03A', '03B', '01K', '01L') Order by MOP_CD"

        Dim SQL = "SELECT MOP_CD, DES FROM MOP WHERE MOP_CD in ('CS', 'DC', 'VI', 'MC', 'AMX', '03B', '01K', '01L') Order by MOP_CD"

        ' DropDownMop.Items.Insert(0, "Select a Mop Code")
        'DropDownMop.Items.FindByText("Select a Mop Code").Value = ""
        DropDownMop.Items.Insert(0, Resources.LibResources.Label531)
        DropDownMop.Items.FindByText(Resources.LibResources.Label531).Value = ""
        DropDownMop.SelectedIndex = 0


        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With DropDownMop
                .DataSource = ds
                .DataValueField = "mop_cd"
                .DataTextField = "des"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Sub DoItemUpdate(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)
        Dim objRetPrc As TextBox
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim thistransaction As OracleTransaction

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        objRetPrc = e.Item.Cells(2).Controls(0)

        If IsNumeric(objRetPrc.Text) Then
            objRetPrc.Text = CDbl(objRetPrc.Text)
        End If

        If objRetPrc.Text = "" Then
            objRetPrc.Text = 0
        End If

        If Not IsNumeric(objRetPrc.Text) Then
            '      vld_price.Text = "Please Enter A Numeric Value For Amount"
            '     vld_price.Visible = True
            '    vld_price.IsValid = False
        Else
            thistransaction = conn.BeginTransaction

            Try
                With cmdUpdateItems
                    .Transaction = thistransaction
                    .Connection = conn
                    .CommandText = "UPDATE payment SET amt = :AMT WHERE row_id = :UNIQUEID"
                    .Parameters.Add(":AMT", OracleType.Double)
                    .Parameters(":AMT").Value = Convert.ToDouble(objRetPrc.Text)
                    .Parameters.Add(":UNIQUEID", OracleType.VarChar)
                    .Parameters(":UNIQUEID").Value = Gridview1.DataKeys(e.Item.ItemIndex).ToString
                End With
                cmdUpdateItems.ExecuteNonQuery()
                thistransaction.Commit()
            Catch ex As Exception
                Throw
            End Try

        End If
        Gridview1.EditItemIndex = -1
        conn.Close()

        bindgrid()
        '     calculate_total()

    End Sub
    Sub DoItemEdit(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)

        Dim objRetPrc As TextBox

        Dim str_mop As String    'lucy
        str_mop = Left(e.Item.Cells(1).Text, 6)
        Dim str_y_n As String

        str_y_n = e.Item.Cells(4).Text

        If InStr(str_y_n, "YES") = 0 Then   ' not credit card
            'If str_mop <> "VISA" And str_mop <> "FINA" And str_mop <> "MAST" And str_mop <> "DEBI" Then  'lucy add the if condition
            Gridview1.EditItemIndex = e.Item.ItemIndex
            bindgrid()
            objRetPrc = CType(Gridview1.Items(Gridview1.EditItemIndex).Cells(2).Controls(0), TextBox)
            objRetPrc.Width = Unit.Parse("1.5 cm")
            calculate_total()
        Else  'lucy
            Exit Sub 'lucy
        End If

    End Sub

    Public Sub bindgrid()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim SoLnGItm As DataGridItem 'lucy
        ds = New DataSet

        Gridview1.DataSource = ""

        Gridview1.Visible = True

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        objSql.CommandText = "SELECT * FROM payment where sessionid= :SESSIONID order by row_id"
        objSql.Parameters.Add(":SESSIONID", OracleType.VarChar)
        objSql.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()

                Dim sumTotal = ds.Tables(0).Compute("SUM(amt)", String.Empty).ToString
                'lbl_response.Text = "TOTAL :" & sumTotal
                Session("subtotal") = sumTotal
                'lblSubtotal = sumTotal

                Dim i As Integer
                For i = 0 To numrows - 1

                    SoLnGItm = Gridview1.Items(i)

                    Dim str_mop As String
                    Dim str_mop_tp As String
                    str_mop = Left(SoLnGItm.Cells(1).Text, 6)

                    str_mop_tp = SoLnGItm.Cells(7).Text

                    If str_mop_tp <> "BC" And str_mop_tp <> "FI" Then   ' not credit card

                        SoLnGItm.Cells(4).Text = "NO"
                        SoLnGItm.Cells(4).ForeColor = Color.Gray

                    Else
                        SoLnGItm.Cells(4).Text = "YES"
                        SoLnGItm.Cells(4).ForeColor = Color.Green

                    End If
                    ds.Tables(0).NewRow()
                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            Else
                Gridview1.Visible = False
                Session("payment") = System.DBNull.Value
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Sub DoItemCancel(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)
        Gridview1.EditItemIndex = -1
        bindgrid()
        '  calculate_total()
    End Sub

    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim deletecd As String
        Dim str_mop As String    'lucy
        str_mop = Left(e.Item.Cells(1).Text, 4)

        Dim str_y_n As String

        str_y_n = e.Item.Cells(4).Text

        If str_y_n = "NO" Then   ' not credit card
            'If str_mop <> "VISA" And str_mop <> "FINA" And str_mop <> "MAST" And str_mop <> "DEBI" Then  'lucy add the if condition
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            deletecd = Gridview1.DataKeys(e.Item.ItemIndex)
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from payment where row_id = :UNIQUEID"
                .Parameters.Add(":UNIQUEID", OracleType.VarChar)
                .Parameters(":UNIQUEID").Value = deletecd
            End With
            cmdDeleteItems.ExecuteNonQuery()
            conn.Close()
            Response.Redirect("WGC_PurchaseGiftCard.aspx")
        Else  'lucy
            Exit Sub 'lucy
        End If

    End Sub

    Public Sub calculate_total()

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand 'jkl added
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim total As Double
        Dim Take_deposit As Double = 0

        If Session("tet_cd") & "" = "" Then
            Take_deposit = Calculate_Take_With()
        Else
            Take_deposit = CDbl(Session("Take_Deposit"))
        End If

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Dim Session_String As String

        Session_String = Session.SessionID.ToString()

        'jkl
        'sql = "select sum(AMT) As Total_Order from Payment where (MOP_TP != 'CD' or MOP_TP IS NULL) and sessionid='" & Session_String & "'"

        objSql.CommandText = "select sum(AMT) As Total_Order from Payment where (MOP_TP != 'CD' or MOP_TP IS NULL) and sessionid=:SESSIONID"
        objSql.Parameters.Add(":SESSIONID", OracleType.VarChar)
        objSql.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
        objSql.Connection = conn

        sAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count
        total = 0

        Try
            If MyTable.Rows(0).Item("Total_Order").ToString & "" <> "" Then
                total = total + CDbl(MyTable.Rows(0).Item("Total_Order").ToString)
            End If
            Dim footer As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("ASPxRoundPanel1").FindControl("lbl_Payments")
            'Dim footer As Label = Master.FindControl("lbl_Payments")
            If Not footer Is Nothing Then
                footer.Text = FormatCurrency(CDbl(total))
            End If

            Session("payments") = CDbl(total)
            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Dim lblTotal As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("ASPxRoundPanel1").FindControl("lblTotal")
        'Dim lblTotal As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("lblTotal")
        Dim lbl_balance As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("ASPxRoundPanel1").FindControl("lbl_balance")
        'Dim lbl_balance As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("lbl_balance")

        If Not IsNothing(lblTotal.Text) And Not IsNothing(lbl_balance.Text) Then
            If IsNumeric(lblTotal.Text) And IsNumeric(lbl_balance.Text) Then
                lbl_balance.Text = FormatCurrency(CDbl(lblTotal.Text) - CDbl(total), 2)
            End If
        End If

        If total = 0 Then
            Session("payment") = System.DBNull.Value
        End If
        If FormatNumber((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * CDbl(Session("Grand_Total")), 2) <= Session("payments") Then
            'lbl_Error.Text = ""
        Else
            If CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) > 0 Then
                If ConfigurationManager.AppSettings("dep_req").ToString = "REQ" Then
                    '              lbl_Error.Text = "The minimum required deposit on this order is " & FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                    'TextBox1.Text = FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                Else
                    '           lbl_Error.Text = "The recommended deposit on this order is " & FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                    'TextBox1.Text = FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                End If

            End If
        End If

    End Sub

    Public Sub delete_records()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        If Not IsNothing(Session.SessionID.ToString.Trim) Then
            With cmdDeleteItems
                .Connection = conn
                'jkl
                '.CommandText = "delete from payment where sessionid='" & Session.SessionID.ToString.Trim & "'"
                .CommandText = "delete from payment where sessionid=:SESSIONID"
                .Parameters.Add(":SESSIONID", OracleType.VarChar)
                .Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            End With
            cmdDeleteItems.ExecuteNonQuery()
        End If
        conn.Close()
        'calculate_total()

    End Sub


    Public Sub add_ar_record(ByVal p_gift_card As String, ByVal p_trn_tp_cd As String, ByVal p_pmt_store As String)

        Dim merchant_id As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdAddRecords As OracleCommand = DisposablesManager.BuildOracleCommand

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Dim sql As String
        conn.Open()

        Try

            If Not IsNothing(Session.SessionID.ToString.Trim) Then
                With cmdAddRecords
                    .Connection = conn
                    .CommandText = "insert into ar_trn "
                    .CommandText = .CommandText & "(app_cd, co_cd, cust_cd, ivc_cd, origin_store, pmt_store "
                    .CommandText = .CommandText & ",trn_tp_cd, amt, post_dt, adj_ivc_cd "
                    .CommandText = .CommandText & ",stat_cd, ar_tp, emp_cd_cshr, emp_cd_op, mop_cd "
                    .CommandText = .CommandText & ",origin_cd, csh_dwr_cd, create_dt, doc_seq_num, des, acct_num, bnk_crd_num, bnk_num) "
                    .CommandText = .CommandText & "Select  '" + Session("app_cd") + "', upper(:CO_CD),   upper(:CUST_CD),  :IVC_CD,   :ORIGIN_STORE, :PMT_STORE, :TRN_TP_CD, "
                    .CommandText = .CommandText & "abs(sum(AMT)), trunc(SYSDATE), :ADJ_IVC_CD , 'T', 'O', '" + Session("emp_cd") + "','"
                    .CommandText = .CommandText & Session("emp_cd") + "', '', 'PGC', '01', trunc(sysdate), :DOC_SEQ_NUM, 'GIFT CARD', :ACCT_NUM, :ACCT_NUM, '" + merchant_id + "' "
                    .CommandText = .CommandText & "from payment Where sessionid= :SESSIONID "
                    .CommandText = .CommandText & " order by row_id"

                    cmdAddRecords.Parameters.Add(":CO_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":CUST_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":IVC_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":ORIGIN_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":PMT_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":ADJ_IVC_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":SESSIONID", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":DOC_SEQ_NUM", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":ACCT_NUM", OracleType.VarChar)

                    cmdAddRecords.Parameters(":CO_CD").Value = Session("CO_CD").ToString.Trim
                    cmdAddRecords.Parameters(":CUST_CD").Value = Session("cust_cd").ToString.Trim
                    cmdAddRecords.Parameters(":ORIGIN_STORE").Value = Session("HOME_STORE_CD").ToString.Trim
                    cmdAddRecords.Parameters(":PMT_STORE").Value = p_pmt_store
                    cmdAddRecords.Parameters(":TRN_TP_CD").Value = p_trn_tp_cd
                    cmdAddRecords.Parameters(":ADJ_IVC_CD").Value = System.DBNull.Value
                    cmdAddRecords.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim

                    cmdAddRecords.Parameters(":ACCT_NUM").Value = p_gift_card

                    Dim doc_seq_num As String = ""
                    'If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
                    'Dim soKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(Session("HOME_STORE_CD").ToString.Trim, Session("tran_dt"))
                    'Dim next_del_doc_num As String = soKeyInfo.soDocNum.delDocNum
                    'If (Not isEmpty(next_del_doc_num)) Then
                    'doc_seq_num = Right(next_del_doc_num, 4)
                    'End If
                    If (Not isEmpty(Session("gc_del_doc_num"))) Then
                        doc_seq_num = Right(Session("gc_del_doc_num"), 4)
                    End If
                    ' Not needed for GCE
                    cmdAddRecords.Parameters(":DOC_SEQ_NUM").Value = ""
                    cmdAddRecords.Parameters(":IVC_CD").Value = Session("gc_del_doc_num")

                End With
                cmdAddRecords.ExecuteNonQuery()

            End If
            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw ex
        Finally
            conn.Close()
        End Try

        'calculate_total()

    End Sub

    Public Sub add_ar_mop(ByVal p_gift_card As String, ByVal p_trn_tp_cd As String, ByVal p_pmt_store As String)

        Dim merchant_id As String = ""
        'merchant_id = Lookup_Merchant("GC", Session("home_store_cd"), "GC")

        'If String.IsNullOrEmpty(merchant_id) Then
        '    lbl_response.Text = "No Gift Card Merchant ID found for your home store code"
        '    Exit Sub
        'End If
        'sabrina R1875
        Dim v_emp_init As String = ""
        Dim v_csh_dwr As String = ""

        v_emp_init = LeonsBiz.GetEmpInit(Session("emp_cd"))
        v_csh_dwr = LeonsBiz.get_csh_dwr(v_emp_init, Session("CO_CD").ToString.Trim)


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdAddRecords As OracleCommand = DisposablesManager.BuildOracleCommand

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        'Dim sql As String
        conn.Open()

        Try

            If Not IsNothing(Session.SessionID.ToString.Trim) Then
                With cmdAddRecords
                    .Connection = conn
                    .CommandText = "insert into ar_trn "
                    .CommandText = .CommandText & "(exp_dt, app_cd, co_cd, cust_cd, ivc_cd, origin_store, pmt_store "
                    .CommandText = .CommandText & ",trn_tp_cd, amt, post_dt, adj_ivc_cd "
                    .CommandText = .CommandText & ",stat_cd, ar_tp, emp_cd_cshr, emp_cd_op, mop_cd "
                    .CommandText = .CommandText & ",origin_cd, csh_dwr_cd, create_dt, doc_seq_num, des, bnk_crd_num, bnk_num) "
                    .CommandText = .CommandText & "Select  to_date(exp_dt,'mm/dd/yyyy'), APPROVAL_CD, upper(:CO_CD),   upper(:CUST_CD),  :IVC_CD,   :ORIGIN_STORE, :PMT_STORE, :TRN_TP_CD, "
                    .CommandText = .CommandText & "abs(AMT), trunc(SYSDATE), :ADJ_IVC_CD , 'T', 'O', '" + Session("emp_cd") + "','"
                    .CommandText = .CommandText & Session("emp_cd") + "', mop_cd, 'PGC',:CSH_DWR_CD , trunc(sysdate), :DOC_SEQ_NUM, 'GIFT CARD', BC, '" + merchant_id + "' "
                    .CommandText = .CommandText & "from payment Where sessionid= :SESSIONID order by row_id"

                    cmdAddRecords.Parameters.Add(":CO_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":CUST_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":IVC_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":ORIGIN_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":ADJ_IVC_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":SESSIONID", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)          'sabrina R1875
                    cmdAddRecords.Parameters.Add(":DOC_SEQ_NUM", OracleType.VarChar)
                    cmdAddRecords.Parameters.Add(":PMT_STORE", OracleType.VarChar)
                    'cmdAddRecords.Parameters.Add(":ACCT_NUM", OracleType.VarChar)

                    cmdAddRecords.Parameters(":CO_CD").Value = Session("CO_CD").ToString.Trim
                    cmdAddRecords.Parameters(":CUST_CD").Value = Session("cust_cd").ToString.Trim
                    cmdAddRecords.Parameters(":ORIGIN_STORE").Value = Session("HOME_STORE_CD").ToString.Trim
                    cmdAddRecords.Parameters(":TRN_TP_CD").Value = p_trn_tp_cd
                    cmdAddRecords.Parameters(":ADJ_IVC_CD").Value = System.DBNull.Value
                    cmdAddRecords.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
                    cmdAddRecords.Parameters(":PMT_STORE").Value = p_pmt_store

                    'cmdAddRecords.Parameters(":ACCT_NUM").Value = p_gift_card

                    Dim doc_seq_num As String = ""
                    'If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
                    'Dim soKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(Session("HOME_STORE_CD").ToString.Trim, Session("tran_dt"))
                    'Dim next_del_doc_num As String = soKeyInfo.soDocNum.delDocNum
                    'If (Not isEmpty(next_del_doc_num)) Then
                    'doc_seq_num = Right(next_del_doc_num, 4)
                    'End If
                    If (Not isEmpty(Session("gc_del_doc_num"))) Then
                        doc_seq_num = Right(Session("gc_del_doc_num"), 4)
                    End If
                    cmdAddRecords.Parameters(":CSH_DWR_CD").Value = v_csh_dwr   'sabrina R1875
                    cmdAddRecords.Parameters(":DOC_SEQ_NUM").Value = doc_seq_num
                    cmdAddRecords.Parameters(":IVC_CD").Value = Session("gc_del_doc_num")

                End With
                cmdAddRecords.ExecuteNonQuery()

            End If

            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw ex
        Finally
            conn.Close()
        End Try

        'calculate_total()

    End Sub
    'sabrina R1922 comment out
    'Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

    '    Response.Redirect("Customer.aspx?GIFT=TRUE")

    'End Sub

    Public Sub print_giftcard(ByVal p_delDocNum As String, ByVal p_emp_cd As String, ByVal p_doc_seq_num As String, ByVal p_cust_num As String,
                                ByVal p_co_cd As String, ByVal p_ufm As String)

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "STD_TENDERRETAIL_UTIL.RECEIPT_PRINT"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_doc_seq_num", OracleType.Char)).Value = p_doc_seq_num
        myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.Char)).Value = p_cust_num
        myCMD.Parameters.Add(New OracleParameter("p_co_cd", OracleType.Char)).Value = p_co_cd
        myCMD.Parameters.Add(New OracleParameter("p_ufm", OracleType.Char)).Value = p_ufm

        Try
            myCMD.ExecuteNonQuery()
            objConnection.Close()
        Catch ex As Exception
            objConnection.Close()
            Throw ex
        End Try
    End Sub

    Public Function giftcard_process(ByVal p_gift_card As String, ByVal p_transtp As String, ByVal p_term_id As String, ByVal p_currency As String, ByVal amt As String) As String

        Dim mybal As Integer = 0
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        sycmd.Connection = conn
        sycmd.CommandText = "std_brk_pos.sendsubTp4"
        sycmd.CommandType = CommandType.StoredProcedure

        sycmd.Parameters.Add("p_req_tp", OracleType.VarChar).Value = p_transtp  'Sign On to get credit card number.
        sycmd.Parameters.Add("p_crdnum", OracleType.VarChar).Value = p_gift_card
        sycmd.Parameters.Add("p_crdtoken", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_pswd", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_amt", OracleType.VarChar).Value = amt
        sycmd.Parameters.Add("p_tran_no", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_term_id", OracleType.VarChar).Value = p_term_id
        sycmd.Parameters.Add("p_currency", OracleType.VarChar).Value = p_currency
        sycmd.Parameters.Add("p_crdnum2", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_opid", OracleType.VarChar).Value = ""

        sycmd.Parameters.Add("p_sessionid", OracleType.VarChar).Value = sessionid
        sycmd.Parameters.Add("p_cust_cd", OracleType.VarChar).Value = Session("cust_cd")  ' added for pos_elec table
        sycmd.Parameters.Add("p_doc_num", OracleType.VarChar).Value = Session("gc_del_doc_num")
        sycmd.Parameters.Add("p_ackind", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_authno", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_transtp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_aut", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crn", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_exp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_res", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_rcp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_nba", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bnb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bpb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crf", OracleType.VarChar, 150).Direction = ParameterDirection.Output

        sycmd.Parameters.Add("p_outfile", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

        Dim rcp As String = ""

        Try
            sycmd.ExecuteNonQuery()
            If p_transtp <> "O3" Then    'Issuance and reload will get the balance
                Session("gift_card_balance") = sycmd.Parameters("p_nba").Value
                Session("app_cd") = sycmd.Parameters("p_aut").Value
            End If
            rcp = sycmd.Parameters("p_rcp").Value
            If rcp = "DECLINED" Or (InStr(rcp, "APPR") = 0 And rcp <> "?") Then
                Return "DECLINED" & ": " & sycmd.Parameters("p_dsp").Value
            End If
            ' Daniela french
            If InStr(rcp, "REFUSEE") > 0 Or (InStr(rcp, "APPR") = 0 And rcp <> "?") Then
                Return "DECLINED" & ": " & sycmd.Parameters("p_dsp").Value
            End If
            If p_transtp = "M3" Then    'Issuance
                Return sycmd.Parameters("p_dsp").Value
            Else
                Return sycmd.Parameters("p_crn").Value
            End If
            'syda.Fill(syds)
        Catch ex As Exception
            lbl_response.Text = ex.Message.ToString()
            Throw ex
        Finally
            Try
                sycmd.Cancel()
                sycmd.Dispose()
                conn.Close()
                conn.Dispose()
            Catch
                'do nothing
            End Try
        End Try

    End Function

    Public Function InRange(ByVal value As Double, ByVal min As Integer, ByVal max As Integer) As Boolean

        Return (value >= min AndAlso value <= max)

    End Function

    Private Sub CreateCustomer()

        'sabrina R1922 (new sub)
        If Session("cust_cd") = "NEW" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim hphone As String = ""
            Dim bphone As String = ""
            Dim cust_tp_cd As String = ""
            Dim email As String = ""

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("cust_cd") & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Try
                dbConnection.Open()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    fname = dbReader.Item("BILL_FNAME").ToString.Trim
                    If fname & "" = "" Then fname = dbReader.Item("FNAME").ToString.Trim
                    lname = dbReader.Item("BILL_LNAME").ToString.Trim
                    If lname & "" = "" Then lname = dbReader.Item("LNAME").ToString.Trim
                    cust_tp_cd = dbReader.Item("CUST_TP").ToString.Trim
                    addr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                    If addr1 & "" = "" Then addr1 = dbReader.Item("ADDR1").ToString.Trim
                    addr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                    If addr2 & "" = "" Then addr2 = dbReader.Item("ADDR2").ToString.Trim
                    city = dbReader.Item("BILL_CITY").ToString.Trim
                    If city & "" = "" Then city = dbReader.Item("CITY").ToString.Trim
                    state = dbReader.Item("BILL_ST").ToString.Trim
                    If state & "" = "" Then state = dbReader.Item("ST").ToString.Trim
                    zip = dbReader.Item("BILL_ZIP").ToString.Trim
                    If zip & "" = "" Then zip = dbReader.Item("ZIP").ToString.Trim
                    hphone = StringUtils.FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                    bphone = StringUtils.FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)
                    email = dbReader.Item("EMAIL").ToString.Trim
                End If
                dbReader.Close()
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
            ' a single transaction to be able to rollback all changes if something fails
            Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")

            Session("CUST_CD") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"),
                                                                 UCase(addr1),
                                                                 Left(UCase(new_cust_lname), 20),
                                                                 Left(UCase(new_cust_fname), 15))
            Try
                dbAtomicConnection.Open()
                dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction()

                ' --- Save Customer to the E1 Database
                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
                sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_CD").Value = Session("CUST_CD").ToString.Trim
                dbAtomicCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
                dbAtomicCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
                dbAtomicCommand.Parameters.Add(":FNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":FNAME").Value = UCase(fname)
                dbAtomicCommand.Parameters.Add(":LNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":LNAME").Value = UCase(lname)
                dbAtomicCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_TP").Value = cust_tp_cd
                dbAtomicCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR1").Value = UCase(addr1)
                dbAtomicCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR2").Value = UCase(addr2)
                dbAtomicCommand.Parameters.Add(":CITY", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CITY").Value = UCase(city)
                dbAtomicCommand.Parameters.Add(":ST", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ST").Value = UCase(state)
                dbAtomicCommand.Parameters.Add(":ZIP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ZIP").Value = zip
                dbAtomicCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":HPHONE").Value = hphone
                dbAtomicCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":BPHONE").Value = bphone
                dbAtomicCommand.Parameters.Add(":EMAIL", OracleType.VarChar)
                dbAtomicCommand.Parameters(":EMAIL").Value = UCase(email)
                dbAtomicCommand.ExecuteNonQuery()
                dbAtomicCommand.Parameters.Clear()  'to get it ready for the next statement

                ' --- Update newly created Customer Code to the table in the CRM schema 
                sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("CUST_CD") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='NEW'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves all the pending changes.
                dbAtomicCommand.Transaction.Commit()

            Catch ex As Exception
                dbAtomicCommand.Transaction.Rollback()  'reverts any changes made so far
                Throw
            Finally
                dbAtomicCommand.Dispose()
                dbAtomicConnection.Close()
            End Try
        End If
    End Sub

End Class
