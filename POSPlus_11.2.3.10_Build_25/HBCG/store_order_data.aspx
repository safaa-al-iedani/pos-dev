<%@ Page Language="VB" AutoEventWireup="false" CodeFile="store_order_data.aspx.vb"
    Inherits="store_order_data" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Remove Customer?</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="white">
    <form id="form1" runat="server">
        <table bgcolor="white" width="100%">
            <tr>
                <td valign="middle" align="center">
                    <img src="images/icons/Symbol-Information.png" style="width: 174px; height: 174px" />
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td valign="middle" align="center" style="width: 325px">
                    <br />
                    <dx:ASPxLabel ID="lbl_warning" runat="server" Text="<%$ Resources:LibResources, Label676 %>">
                    </dx:ASPxLabel>
                    <dx:ASPxLabel ID="lbl_warning2" runat="server" Text="<%$ Resources:LibResources, Label666 %>">
                    </dx:ASPxLabel>
                    <br />
                    <br />
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <dx:ASPxButton ID="btn_Proceed" runat="server" Text="<%$ Resources:LibResources, Label675 %>">
                                </dx:ASPxButton>
                            </td>
                            <td align="center">
                                <dx:ASPxButton ID="btn_Cancel" runat="server" Text="<%$ Resources:LibResources, Label468 %>">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <dx:ASPxButton ID="btn_Return" runat="server" Text="<%$ Resources:LibResources, Label489 %>">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
