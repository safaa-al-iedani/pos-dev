﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tdpl.aspx.vb" Inherits="tdpl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<script language="javascript" type="text/javascript">

    function disableButton(id) {
        // Daniela B 20140723 prevent double click
        try {
            var a = document.getElementById(id);
            //a.disabled = true;
            a.style.display = 'none';
        } catch (err) {
            alert('Error in disableButton ' + err.message);
            return false;
        }
        return true;
    }
</script>

<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Large" Font-Underline="True" Text="Brick Finance Card Payment"></asp:Label>
        <br />
        <br />
&nbsp;&nbsp;&nbsp;
        
        &nbsp;
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Customer Code" Font-Bold="True"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tdpl_cust_cd" runat="server" AutoPostBack="True"></asp:TextBox>
&nbsp;
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Customer.aspx">Click To Find A Customer</asp:HyperLink>
&nbsp;<br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:Label ID="Label1" runat="server" Text="Brick Card Number" Font-Bold="True"></asp:Label>
&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tdpl_acct_num" runat="server" AutoPostBack="True"></asp:TextBox>

        &nbsp;&nbsp;
        <asp:Button ID="Button3" runat="server" Font-Bold="True" Text="Click To Swipe" OnClientClick="return disableButton(this.id);"/>

        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Cancel - Exit" Font-Bold="True" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" Text="Process Payment" Font-Bold="True" OnClientClick="return disableButton(this.id);"/>
        <br />
        <br />
        <asp:TextBox ID="txt_msg" runat="server" BorderStyle="None" ReadOnly="True" Width="755px" Font-Bold="True" Font-Size="Large" ForeColor="Red"></asp:TextBox>
    
    </div>
    </form>
</body>
</html>
