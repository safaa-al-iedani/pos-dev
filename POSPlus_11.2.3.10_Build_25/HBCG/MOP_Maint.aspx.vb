Imports System.Data.OracleClient
Imports System.Data
Imports System.Xml
Imports HBCG_Utils

Partial Class MOP_Maint
    Inherits POSBasePage
    Private Const cResponseCriteria As String = "/merchantCode20Response/response/merchantData"
    Dim _requestXML As XmlDataDocument
    Dim _responseXML As New XmlDataDocument()

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim MyTable As DataTable
        Dim numrows As Integer
        ds = New DataSet

        GridView1.DataSource = ""

        GridView1.Visible = True

        'Open Connection 
        conn.Open()

        sql = "SELECT MOP_CD, DES, DEPOSIT_FLAG, BALANCE_FLAG, BANK_FLAG, MOP_TP, MOP_ACTIVE, FINANCE_DEPOSIT_TP FROM SETTLEMENT_MOP "
        If DropDownList1.Value = "Active" Then
            sql = sql & " WHERE MOP_ACTIVE='Y' "
        ElseIf DropDownList1.Value = "Inactive" Then
            sql = sql & " WHERE MOP_ACTIVE='N' "
        End If
        sql = sql & "ORDER BY DES"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        GridView1.Visible = True
        GridView1.DataSource = dv
        GridView1.DataBind()

        'Close Connection 
        conn.Close()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If
        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lbl_header.Text = "<font color=red>* TRAIN MODE *</font>"
        End If

        If Find_Security("MCM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "<br /><br />We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator.<br /><br /><br />"
            Exit Sub
        End If

        If Not IsPostBack Then
            bindgrid()
        End If

    End Sub

    Protected Sub GridView1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView1.ItemDataBound

        Dim chk_fin_dp As CheckBox = CType(e.Item.FindControl("chk_fin_dp"), CheckBox)

        If Not IsNothing(chk_fin_dp) Then
            If e.Item.Cells(8).Text = "Y" Then
                chk_fin_dp.Checked = True
            End If
        End If

        Dim CheckBox2 As CheckBox = CType(e.Item.FindControl("CheckBox2"), CheckBox)

        If Not IsNothing(CheckBox2) Then
            If e.Item.Cells(7).Text = "Y" Then
                CheckBox2.Checked = True
            End If
        End If

    End Sub

    Public Sub Update_Active(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck_value As String = ""
        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            ck_value = "Y"
        Else
            ck_value = "N"
        End If

        sql = "UPDATE SETTLEMENT_MOP SET MOP_ACTIVE='" & ck_value & "' WHERE MOP_CD='" & dgItem.Cells(0).Text & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Public Sub Update_Fin_DP(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Dim objsql2 As OracleCommand
        Dim sql As String
        Dim ck_value As String = ""
        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)

        If ck1.Checked = True Then
            ck_value = "Y"
        Else
            ck_value = "N"
        End If

        sql = "UPDATE SETTLEMENT_MOP SET FINANCE_DEPOSIT_TP='" & ck_value & "' WHERE MOP_CD='" & dgItem.Cells(0).Text & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objsql2.ExecuteNonQuery()

        conn.Close()

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        SQL = "SELECT * FROM SETTLEMENT_MOP ORDER BY MOP_CD"

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = SQL
            End With


            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)
        Catch
            Throw
        End Try

        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow

        Dim match_found As Boolean = False
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn2.Open()

        SQL = "SELECT * FROM MOP ORDER BY MOP_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn2)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read
                match_found = False
                For Each dr In ds.Tables(0).Rows
                    If dr("MOP_CD") = MyDataReader.Item("MOP_CD").ToString Then
                        match_found = True
                        Exit For
                    End If
                Next
                If match_found = True Then
                    SQL = "UPDATE SETTLEMENT_MOP SET DES='" & Replace(MyDataReader.Item("DES").ToString, "'", "''") & "', "
                    SQL = SQL & "DEPOSIT_FLAG='" & Replace(MyDataReader.Item("DEPOSIT_FLAG").ToString, "'", "''") & "', "
                    SQL = SQL & "BALANCE_FLAG='" & Replace(MyDataReader.Item("BALANCE_FLAG").ToString, "'", "''") & "', "
                    SQL = SQL & "BANK_FLAG='" & Replace(MyDataReader.Item("BANK_FLAG").ToString, "'", "''") & "', "
                    SQL = SQL & "MOP_TP='" & Replace(MyDataReader.Item("MOP_TP").ToString, "'", "''") & "' "
                    SQL = SQL & "WHERE MOP_CD='" & Replace(MyDataReader.Item("MOP_CD").ToString, "'", "''") & "'"
                Else
                    SQL = "INSERT INTO SETTLEMENT_MOP (DES, DEPOSIT_FLAG, BALANCE_FLAG, BANK_FLAG, MOP_TP, MOP_CD) VALUES('" & Replace(MyDataReader.Item("DES").ToString, "'", "''") & "', "
                    SQL = SQL & "'" & Replace(MyDataReader.Item("DEPOSIT_FLAG").ToString, "'", "''") & "', "
                    SQL = SQL & "'" & Replace(MyDataReader.Item("BALANCE_FLAG").ToString, "'", "''") & "', "
                    SQL = SQL & "'" & Replace(MyDataReader.Item("BANK_FLAG").ToString, "'", "''") & "', "
                    SQL = SQL & "'" & Replace(MyDataReader.Item("MOP_TP").ToString, "'", "''") & "',"
                    SQL = SQL & "'" & Replace(MyDataReader.Item("MOP_CD").ToString, "'", "''") & "')"
                End If
                cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, conn)
                cmdGetCodes.ExecuteNonQuery()
            Loop

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
            conn2.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try
        bindgrid()

    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged

        bindgrid()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class
