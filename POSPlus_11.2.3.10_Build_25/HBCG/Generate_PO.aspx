<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Generate_PO.aspx.vb" Inherits="Generate_PO" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%">
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Sales Order: ">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Width="170px" AutoPostBack="True" OnTextChanged="ASPxTextBox1_TextChanged">
                </dx:ASPxTextBox>
            </td>
            <td>
                &nbsp;
            </td>
            <td align="right">
                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Generate Purchase Orders" OnClick="ASPxButton1_Click">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Dest Store Code: ">
                </dx:ASPxLabel>
            </td>
            <td colspan="3">
                <dx:ASPxComboBox ID="store_cd" runat="server" IncrementalFilteringMode="StartsWith" >
                </dx:ASPxComboBox>
            </td>
        </tr>
         <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="PO Sort Code: ">
                </dx:ASPxLabel>
            </td>
            <td colspan="3">
                <dx:ASPxComboBox ID="cbo_po_sort" runat="server" IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
            </td>
        </tr>
    </table>
    <dx:ASPxLabel ID="lbl_PO_MSG" runat="server" Width="100%" Font-Bold="True">
    </dx:ASPxLabel>
    <br />
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
        Width="100%" Mode="ShowAllRecords" KeyFieldName="DEL_DOC_LN#">
        <Columns>
            <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Caption="">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="SO Line#" FieldName="DEL_DOC_LN#" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Vendor" FieldName="VE_CD" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SKU" FieldName="ITM_CD" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Qty" FieldName="QTY" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Vendor Stock Number" FieldName="VSN" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description" FieldName="DES" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsPager Visible="False">
        </SettingsPager>
    </dx:ASPxGridView>
    <%--<asp:SqlDataSource ID="ORACLEDB" runat="server" ConnectionString="<%$ ConnectionStrings:ERP %>"
        ProviderName="<%$ ConnectionStrings:ERP.ProviderName %>" SelectCommand="select b.DEL_DOC_NUM, b.DEL_DOC_LN#, b.itm_cd, c.ve_cd, c.vsn, b.QTY, c.des from so a, so_ln b, itm c where a.STAT_CD='O' and a.del_doc_num=b.del_doc_num and b.ITM_CD=c.ITM_CD and b.STORE_CD IS NULL and b.VOID_FLAG='N' and b.DEL_DOC_LN# NOT IN (SELECT DEL_DOC_LN# FROM SO_LN$PO_LN where DEL_DOC_NUM=:DEL_DOC_NUM) and b.DEL_DOC_NUM=:DEL_DOC_NUM ORDER BY c.VE_CD, b.DEL_DOC_LN#">
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxTextBox1" Name="DEL_DOC_NUM" PropertyName="Value"
                Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
