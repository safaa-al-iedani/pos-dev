﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.Business
Imports HBCG_Utils

Partial Class POSMaster
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("EMP_CD") & "" = "" Then
                Response.Redirect("login.aspx")
            End If

            If Me.Page.Request.FilePath.ToString().ToLower().Contains("cashdrawerbalancing") And Not SecurityUtils.hasSecurity("CDB", Session("EMP_CD")) Then
                Response.Redirect("newmain.aspx")
            End If

            Page.Title = ConfigurationManager.AppSettings("app_title").ToString & Page.Title

            BindData()
        End If
    End Sub

    Private Sub BindData()

        If Request("LEAD") = "TRUE" Then
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx?LEAD=TRUE")
        Else
            Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
        End If

        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lblHeader.Text = "* TRAIN MODE *"
            lblHeader.ForeColor = Color.Red
        End If

        lblHeader3.Text = "Terminal: " & Request.ServerVariables("REMOTE_ADDR")

        lblVersion.Text = "RedPrairie MM POS Version " & Get_Version("/HandleCode/HighlandsPOS", "Version")

        If Request("D") = "D" Then
            'lblTabID.Text = Session("MP")
        End If
        If Request("D") = "I" Then
            'lblTabID.Text = Session("IP")
        End If

        Dim objEmp As New EmpDtc
        objEmp.empCd = UCase(Session("EMP_CD"))

        Dim objEmpBusiness As New EmpBusiness
        objEmpBusiness.GetEmpDetail(objEmp)

        hiddenSessions.Value = "EmpCd:" + Session("EMP_CD") + ";EmpInit:" + objEmp.empInit &
            ";HomeStoreCd:" + objEmp.homeStoreCd + ";ShipToStoreCd:" + objEmp.shipToStoreCd + ";"

        Session("pd_store_cd") = objEmp.shipToStoreCd

        If objEmp.firstName & "" <> "" And objEmp.lastName & "" <> "" Then
            lblHeader1.Text = "Welcome " & Left(objEmp.firstName, 1) & LCase(Right(objEmp.firstName, Len(objEmp.firstName) - 1)) &
                " " & Left(objEmp.lastName, 1) & LCase(Right(objEmp.lastName, Len(objEmp.lastName) - 1))
        End If
        If objEmp.homeStoreCd & "" <> "" Then
            lblHeader2.Text = Resources.LibResources.Label573 & ": " & objEmp.homeStoreCd
        End If

    End Sub

End Class

