<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DeliveryDate.aspx.vb" Inherits="DeliveryDate"
    MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="~/Usercontrols/ManagerOverride.ascx" TagPrefix="ucMgr" TagName="ManagerOverride" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Label ID ="lblFilterDate" runat="server" Text ="Search By Date" />&nbsp;
                    <dx:ASPxDateEdit ID="cboTargetDate" runat="server" EnableViewState="true" AutoPostBack="true" OnDateChanged="cboTargetDatechanged" ToolTip="Select a date to filter the delivery dates.">
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                <td>
        <asp:DataGrid ID="grid_del_date" runat="server" AutoGenerateColumns="False" Style="position: relative"
            Height="192px" Width="100%" AllowPaging="True" PageSize="7"
            BorderStyle="None" BorderColor="White" BorderWidth="0px" CaptionAlign="Left"
            OnItemDataBound="grid_del_date_ItemDataBound" PagerStyle-Visible="False">
            <Columns>
                <asp:BoundColumn DataField="DEL_DT" SortExpression="Delivery Date" HeaderText="Date"
                    DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
                <asp:BoundColumn DataField="DAY" SortExpression="Day" HeaderText="Day">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="DEL_STOPS" SortExpression="Max" HeaderText="Max">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ACTUAL_STOPS" SortExpression="Sched" HeaderText="Sched">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="REMAINING_STOPS" SortExpression="Avail" HeaderText="Avail">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                </asp:BoundColumn>
                <asp:ButtonColumn ButtonType="PushButton" Text="Select"></asp:ButtonColumn>
                <asp:BoundColumn DataField="EXMPT_DEL_CD"  Visible ="false" HeaderText="ExceptionCode"></asp:BoundColumn>
            </Columns>
            <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
            <HeaderStyle Font-Bold="True" ForeColor="Black" />
        </asp:DataGrid>
                </td>
            </tr>
        </table>

        <table width="98%">
            <tr>
                <td align="center">
                    <table width="75%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                    <dx:ASPxLabel ID="lblDesc" runat="server" Text="" Visible="false" EncodeHtml="False">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>
    <table>
        <tr>
            <td style="height: 19px" valign="top">
                <asp:Label ID="lbl_Delivery" runat="server" Text="Delivery:"></asp:Label>
            </td>
            <td style="height: 19px" valign="top">
                <asp:Label ID="lbl_Setup" runat="server" Text="Setup:"></asp:Label>
            </td>
            <td style="height: 19px">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="height: 28px" valign="top">
                <asp:TextBox ID="txt_Delivery_Charge" runat="server" Style="position: relative;"
                    Width="67px" CssClass="style5"></asp:TextBox>
            </td>
            <td style="height: 28px" valign="top">
                <asp:TextBox ID="txt_setup_charge" runat="server" Style="position: relative;" Width="67px"
                    CssClass="style5"></asp:TextBox>
            </td>
            <td style="height: 28px" valign="top">
                <dx:ASPxButton ID="btn_Change" runat="server" Text="Update Charges">
                </dx:ASPxButton>
            </td>
            <td style="height: 28px" valign="top">
                <asp:Label ID="lblZoneCd" runat="server" Text=""></asp:Label></td>
        </tr>
    </table>
     <table width="98%" runat="server" id="tblError">
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPlblError" runat="server" Text="" ForeColor="Red">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_curr_data" runat="server" Text=""></asp:Label>
    <br />
    <br />
    <br />
    <br />
    <br />
     <dxpc:ASPxPopupControl ID="popupMgrOverride" runat="server" ClientInstanceName="popupMgrOverride"
        HeaderText="Manager Approval" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Modal="true" CloseAction="None"
        AllowDragging="true" ShowCloseButton="false" ShowPageScrollbarWhenModal="true" Width="400px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="pcccMgrOverride" runat="server">
                <ucMgr:ManagerOverride runat="server" ID="ucMgrOverride" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <asp:HiddenField ID ="hidOriginalDeliveryCharges" runat ="server" />
</asp:Content>
