﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="ProductAttributeMaintenance.aspx.vb" Inherits="ProductAttributeMaintenance"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="lbl_attr_cd" runat="server" meta:resourcekey="lbl_attr_cd"></dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_attr_cd" runat="server" Width="150PX" MaxLength="3"></dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_search" runat="server" Text="Search"></dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="GV_product_attribute" runat="server" AutoGenerateColumns="False" KeyFieldName="ROWID" EnableRowsCache="true"
                                OnRowUpdating="GV_row_update" OnRowInserting="GV_row_insert" OnRowDeleting="GV_row_delete" OnDataBinding="GV_Product_Attribute_DataBind"
                                OnCellEditorInitialize="GVCellEditorInitialize" Settings-ShowFooter="true" Width="950px" Enabled="true">
                                <Settings ShowFooter="true" />
                                <SettingsBehavior ConfirmDelete="true" />
                                <SettingsPager Position="Bottom">
                                    <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                                </SettingsPager>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowEditButton="true" ShowNewButtonInHeader="true" ShowDeleteButton="true"></dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="" FieldName="ROWID" Visible="False" VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_attr_cd" FieldName="ATTRIBUTE_CODE" Visible="True" VisibleIndex="0" Width="90" PropertiesTextEdit-MaxLength="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_eng_description" FieldName="ATTRIBUTE_DESCRIPTION" Visible="True" VisibleIndex="1" Width="370" PropertiesTextEdit-MaxLength="30">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="gv_fr_description" FieldName="ATTRIBUTE_DESCRIPTION_FR" Visible="True" VisibleIndex="2" Width="370" PropertiesTextEdit-MaxLength="30">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsEditing Mode="Inline"></SettingsEditing>
                                <Settings UseFixedTableLayout="True" />

                                <SettingsDataSecurity AllowDelete="True" />
                                <Templates>
                                    <FooterRow>
                                        <dx:ASPxLabel ID="GV_Footer_label" runat="server" ClientInstanceName="GV_Footer_label" Text="TEST" OnInit="GV_Footer_label_init" ForeColor="Red"></dx:ASPxLabel>
                                    </FooterRow>
                                </Templates>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="lbl_message" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:ASPxLabel>
            </td>
        </tr>
    </table>

      <div runat="server" id="div_version">
        <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
        </dx:ASPxLabel>
    </div>
</asp:Content>








