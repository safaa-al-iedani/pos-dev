﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="PrinterMaintenance.aspx.vb" Inherits="PrinterMaintenance" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <table border="0" cellpadding="0" cellspacing="0">
        <asp:Label ID="Label8" runat="server" Text="Printer Name:"></asp:Label>
        <asp:TextBox ID="txt_printer_nm" runat="server"  MaxLength="20"></asp:TextBox>
        <tr>
            <td style="width: 338px">
                   <asp:Label ID="Label3" runat="server" Text="Store Code:" Visible="False"></asp:Label>
                     <asp:DropDownList ID="txt_store_cd" runat="server" Style="position: relative" AppendDataBoundItems="true" Visible="False">
                     </asp:DropDownList>
            </td>
        </tr>   
        <tr> 
            <td style="width: 109px">
                <asp:Label ID="Label2" runat="server" Text="Traffic Date ( DD-MON-YYYY ):" Visible="False"></asp:Label>
                <asp:TextBox ID="txt_traffic_dt" runat="server" Width="100px" MaxLength="11" CausesValidation = "true" Visible="False">
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate = "txt_traffic_dt"
                    ValidationExpression="^([012]?\d|3[01])-([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][u]l|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc]|[Nn][oO][Vv]|[Dd][Ee][Cc])-(19|20)\d\d$"
                    runat="server" ErrorMessage="Invalid Date format. Valid Date Format DD-MON-YYYY">
                </asp:RegularExpressionValidator>
            </td>
          
        </tr>
    </table>

    <br />

   

    

     <asp:DataGrid ID="Gridview1" runat="server" Width="60%" BorderColor="Black" 
         OnCancelCommand="DoItemCancel" OnEditCommand="DoItemEdit" OnUpdateCommand="DoItemUpdate"
        OnDeleteCommand="DoItemDelete" AutoGenerateColumns="False" CellPadding="2" DataKeyField="row_id"
     Height="16px" PageSize="10" AlternatingItemStyle-BackColor="Beige" ReadOnly ="false" AllowCustomPaging="True">
        <Columns>
           
          <asp:BoundColumn DataField="printer_nm" HeaderText="Printer Name" ></asp:BoundColumn> 
             <%--<asp:BoundColumn DataField="store_cd" HeaderText="Store Code" ></asp:BoundColumn> Commented to add dropdown instead--%>
           <asp:TemplateColumn HeaderText="Store Code">
         <ItemTemplate>
            <%#Container.DataItem("store_cd")%>
         </ItemTemplate>
         <EditItemTemplate>
            <asp:DropDownList runat="server" ID="Dropdownlist1" Font-Size="Small"/>
         </EditItemTemplate>
          </asp:TemplateColumn>
             <asp:BoundColumn DataField="printer_ip" HeaderText="Printer IP"></asp:BoundColumn> 
            <%--  <asp:BoundColumn DataField="sys_belong" HeaderText="Sys Belong" ></asp:BoundColumn> --%>
            <asp:TemplateColumn HeaderText="sys belong">
         <ItemTemplate>
            <%#Container.DataItem("sys_belong")%>
         </ItemTemplate>
         <EditItemTemplate>
            <asp:DropDownList runat="server" ID="DropDownList_sys_belong" Font-Size="Small"/>
         </EditItemTemplate>
          </asp:TemplateColumn>

        <%--     <asp:BoundColumn DataField="printer_tp"  HeaderText="Printer Type" ></asp:BoundColumn>  --%>
            <asp:TemplateColumn HeaderText=" printer_tp ">
         <ItemTemplate>
            <%#Container.DataItem("printer_tp")%>
         </ItemTemplate>
         <EditItemTemplate>
            <asp:DropDownList runat="server" ID="DropDownList_printer_tp"  Font-Size="Small"/>
         </EditItemTemplate>
          </asp:TemplateColumn>

            <asp:BoundColumn DataField="row_id" HeaderText="ID" Visible="False"></asp:BoundColumn>
           <%-- <asp:TemplateColumn HeaderText="">
                <ItemTemplate>--%>

                    <%-- lucy remove  
                    <asp:Image ID="Image1" runat="server" ImageUrl="images/icons/tick.png" Height="20"
                        Width="20" /> --%>

             <%--   </ItemTemplate>--%>
            <%--    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
            </asp:TemplateColumn>--%>
            <asp:EditCommandColumn CancelText="&lt;img src='images/icons/Undo24.gif' border='0'&gt;"
                UpdateText="&lt;img src='images/icons/Save24.gif' border='0'&gt;" EditText="&lt;img src='images/icons/Edit24.gif' border='0'&gt;"
                HeaderText="Edit">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:EditCommandColumn>
             <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"
                HeaderText="Delete">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>
        </Columns>
       
    </asp:DataGrid>

    
    <table border="0" cellpadding="0" cellspacing="0">
     <tr>
         <td>
           <asp:Button ID="btn_lookup" runat="server" Text="Search">
           </asp:Button>
         </td>
         <td>
           <asp:Button ID="btn_add" runat="server" Text="Add A New Printer">
           </asp:Button>
         </td>
         <td>
           <asp:Button ID="btn_clear" runat="server" Text="Clear Screen"></asp:Button>
         </td>
     </tr>
     </table>
    <br />
    <asp:Label ID="lbl_info" Font-Bold="True" ForeColor="Red" runat="server"
        Text="" Width="100%"> 
    </asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
