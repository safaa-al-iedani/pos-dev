<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Special_Order_Popup.aspx.vb"
    Inherits="Special_Order_Popup" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="~/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <br />
            <table style="position: relative; top: 0px" class="style5">
                <tr>
                    <td>SKU Number</td>
                    <td colspan="3">
                        <asp:TextBox ID="txt_SKU" runat="server" MaxLength="9" Style="position: relative"
                            Width="64px" CssClass="style5" Enabled="False">S</asp:TextBox>
                        &nbsp;
                        <asp:Label ID="lbl_SKU_Info" runat="server" Style="position: relative" Text="Enter up to 9 characters"
                            Width="293px"></asp:Label></td>
                </tr>
                <tr>
                    <td>Item Type Code</td>
                    <td colspan="3">
                        <asp:DropDownList ID="cbo_ITM_TP_CD" runat="server" Width="320px" CssClass="style5"
                            Enabled="False">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Vendor Code</td>
                    <td colspan="3" valign="top">
                        <asp:TextBox ID="txt_Vendor" runat="server" MaxLength="4" Width="45px" CssClass="style5"
                            AutoPostBack="true" Enabled="False"></asp:TextBox>&nbsp;
                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageUrl="~/images/icons/find.gif"
                            Width="19px" OnClick="ImageButton2_Click" Enabled="False" />&nbsp; &nbsp; &nbsp;&nbsp;
                        <asp:Label ID="lbl_ve_label" runat="server" Width="295px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Minor Code
                    </td>
                    <td colspan="3" valign="top">
                        <asp:DropDownList ID="cbo_minor" runat="server" Width="374px" AutoPostBack="True"
                            CssClass="style5" Enabled="False">
                        </asp:DropDownList>&nbsp;
                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/images/icons/find.gif"
                            Width="19px" Enabled="False" />
                    </td>
                </tr>
                <tr>
                    <td nowrap>Vendor Stock Number</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtVSN" runat="server" Width="368px" CssClass="style5" MaxLength="30"
                            Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtDES" runat="server" Width="368px" CssClass="style5" MaxLength="30"
                            Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="height: 23px">Cover</td>
                    <td colspan="3" style="height: 23px">
                        <asp:TextBox ID="txtCover" runat="server" Width="368px" CssClass="style5" MaxLength="30"
                            Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="height: 23px">Retail Price</td>
                    <td style="height: 23px">
                        <asp:TextBox ID="txtRetail" runat="server" CssClass="style5" Enabled="False"></asp:TextBox></td>
                    <td style="width: 77px; height: 23px; text-align: right">Mfg List</td>
                    <td style="width: 132px; height: 23px">
                        <asp:TextBox ID="txtMFG" runat="server" Width="120px" CssClass="style5" Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="height: 23px">Cost:</td>
                    <td style="height: 23px">
                        <asp:TextBox ID="txt_cost" runat="server" CssClass="style5" Enabled="False"></asp:TextBox></td>
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Grade</td>
                    <td colspan="3" style="height: 21px">
                        <asp:TextBox ID="txtGrade" runat="server" Width="368px" CssClass="style5" MaxLength="30"
                            Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Size</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtSize" runat="server" Width="368px" CssClass="style5" MaxLength="30"
                            Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Finish</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtFinish" runat="server" Width="368px" CssClass="style5" MaxLength="30"
                            Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Comment</td>
                    <td></td>
                    <td style="width: 77px"></td>
                    <td style="width: 132px"></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="lbl_comments" runat="server"></asp:Label><br />
                        <asp:TextBox ID="txtComment" runat="server" Width="472px" TextMode="MultiLine" CssClass="style5"
                            Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Style Code</td>
                    <td colspan="3">
                        <asp:DropDownList ID="cboStyle" runat="server" Width="344px" CssClass="style5" Enabled="False">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Category</td>
                    <td colspan="3">
                        <asp:DropDownList ID="cbo_category" runat="server" Width="344px" CssClass="style5"
                            AutoPostBack="True" OnSelectedIndexChanged="cbo_category_SelectedIndexChanged"
                            Enabled="False">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Volume</td>
                    <td>
                        <asp:TextBox ID="txtVolume" runat="server" CssClass="style5" MaxLength="13" Enabled="False"></asp:TextBox></td>
                    <td style="width: 77px">Measure Code</td>
                    <td style="width: 132px">
                        <asp:TextBox ID="txtMeasure" runat="server" CssClass="style5" Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Freight Factor</td>
                    <td>
                        <asp:TextBox ID="txtFreight" runat="server" CssClass="style5" Enabled="False"></asp:TextBox></td>
                    <td style="width: 77px">Stop Time</td>
                    <td style="width: 132px">
                        <asp:TextBox ID="txtStop" runat="server" CssClass="style5" Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Carpet Width</td>
                    <td>
                        <asp:TextBox ID="txtCarpet" runat="server" CssClass="style5" Enabled="False"></asp:TextBox></td>
                    <td style="width: 77px">Warrantable</td>
                    <td style="width: 132px">
                        <asp:DropDownList ID="cbo_Warr" runat="server" CssClass="style5" Enabled="False">
                            <asp:ListItem Selected="True" Value="Y">Yes</asp:ListItem>
                            <asp:ListItem Value="N">No</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Pad Days</td>
                    <td>
                        <asp:TextBox ID="txtPad" runat="server" CssClass="style5" Enabled="False"></asp:TextBox></td>
                    <td style="width: 77px">Delivery Points</td>
                    <td style="width: 132px">
                        <asp:TextBox ID="txtPoints" runat="server" CssClass="style5" Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Treatable</td>
                    <td>
                        <asp:DropDownList ID="cboTreatable" runat="server" CssClass="style5" Enabled="False">
                            <asp:ListItem Selected="True" Value="Y">Yes</asp:ListItem>
                            <asp:ListItem Value="N">No</asp:ListItem>
                        </asp:DropDownList></td>
                    <td>Family Code</td>
                    <td>
                        <asp:TextBox ID="txtFamily" runat="server" CssClass="style5" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Sort Codes
                    </td>
                    <td colspan="4">
                        <%--<div runat="server" id="divSortCodes" style="overflow: auto; height: 30px; text-align: left; width: 95%; padding-top: 5px;">
                        </div>--%>
                        <%--the backgroung color of the text box is set to #efebe7 is to mimik the disabled display --%>
                            <asp:TextBox Style ="background-color: #efebe7; OVERFLOW-X: visible; OVERFLOW-Y: visible;" ID="txtSortCodes" runat="server" Width="472px" TextMode="MultiLine" CssClass="style5" 
                            Enabled="true" ReadOnly="true"></asp:TextBox></td>
                </tr
                    </td>
                </tr>
            </table>
        </div>
        <asp:DropDownList ID="cbo_Special_Forms" runat="server" AppendDataBoundItems="True"
            AutoPostBack="True" Width="235px" CssClass="style5" Visible="False">
        </asp:DropDownList>
        &nbsp;<asp:HyperLink ID="hpl_excel" runat="server" Visible="False" Target="_blank">[hpl_excel]</asp:HyperLink>
        <asp:DropDownList ID="cbo_Frames" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
            Width="235px" CssClass="style5" Visible="False">
        </asp:DropDownList>&nbsp;<br />
        <asp:Button ID="btn_save_exit" runat="server" Text="Save Changes" Width="119px" CssClass="style5"
            Enabled="False" />
        &nbsp;&nbsp;
        <dxpc:ASPxPopupControl ID="ASPxPopupControl4" runat="server" CloseAction="CloseButton"
            HeaderText="Vendor Lookup" Height="207px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="400px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <table width="100%">
                        <tr>
                            <td valign="middle" align="center">
                                <img src="images/icons/Symbol-Information.png" style="width: 174px; height: 174px" />
                            </td>
                            <td>&nbsp;&nbsp;
                            </td>
                            <td valign="middle" align="center" style="width: 413px">Vendor Search
                                <br />
                                <table class="style5">
                                    <tr>
                                        <td>Vendor Code
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_ve_cd" runat="server" CssClass="style5" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Vendor Name
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_ve_name" runat="server" CssClass="style5" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <asp:Button ID="btn_ve_submit" runat="server" Text="Search" Width="100px" CssClass="style5" />
                                <br />
                                <br />
                                <asp:DataGrid ID="ve_grid" runat="server" Width="413" BorderColor="Black" CellPadding="3"
                                    AutoGenerateColumns="False" DataKeyField="ve_cd" Height="16px" PageSize="7" AlternatingItemStyle-BackColor="Beige"
                                    CssClass="style5" Visible="true">
                                    <Columns>
                                        <asp:BoundColumn HeaderText="Vendor Code" DataField="VE_CD" />
                                        <asp:BoundColumn HeaderText="Vendor Name" DataField="VE_NAME" />
                                        <asp:ButtonColumn ButtonType="PushButton" CommandName="Select" Text="Select" ItemStyle-CssClass="style5"></asp:ButtonColumn>
                                    </Columns>
                                    <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                    <EditItemStyle CssClass="style5" />
                                    <ItemStyle CssClass="style5" />
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CloseAction="CloseButton"
            HeaderText="Minor Lookup" Height="207px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Width="400px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <table width="100%">
                        <tr>
                            <td valign="middle" align="center">
                                <img src="images/icons/Symbol-Information.png" style="width: 174px; height: 174px" />
                            </td>
                            <td>&nbsp;&nbsp;
                            </td>
                            <td valign="middle" align="center" style="width: 413px">Minor Search
                                <br />
                                <table class="style5">
                                    <tr>
                                        <td>Minor Code
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_mnr_search" runat="server" CssClass="style5" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Minor Description
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_mnr_desc_search" runat="server" CssClass="style5" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <asp:Button ID="btn_mnr_submit" runat="server" Text="Search" Width="100px" CssClass="style5" />
                                <br />
                                <br />
                                <asp:DataGrid ID="mnr_grid" runat="server" Width="413" BorderColor="Black" CellPadding="3"
                                    AutoGenerateColumns="False" DataKeyField="mnr_cd" Height="16px" PageSize="7"
                                    AlternatingItemStyle-BackColor="Beige" CssClass="style5" Visible="true">
                                    <Columns>
                                        <asp:BoundColumn HeaderText="Minor Code" DataField="MNR_CD" />
                                        <asp:BoundColumn HeaderText="Description" DataField="FULL_DES" />
                                        <asp:ButtonColumn ButtonType="PushButton" CommandName="Select" Text="Select" ItemStyle-CssClass="style5"></asp:ButtonColumn>
                                    </Columns>
                                    <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                    <EditItemStyle CssClass="style5" />
                                    <ItemStyle CssClass="style5" />
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ModalBackgroundStyle BackColor="#E0E0E0">
            </ModalBackgroundStyle>
        </dxpc:ASPxPopupControl>
    </form>
</body>
</html>
