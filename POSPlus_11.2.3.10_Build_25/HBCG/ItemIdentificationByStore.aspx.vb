Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports AppUtils
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxTreeList
Imports DevExpress.Web

Partial Class ItemIdentificationByStore
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Dim g_custmpc_id As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If

        If Find_Security("POSIICSM", Session("EMP_CD")) = "Y" OrElse isSuperUser(Session("EMP_CD")) = "Y" Then
        Else
            Div1.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If

        Dim v_name As String = ""
        lbl_msg.Text = ""

        'If isAuthToInsert() = "Y" Then
        '    btn_new.Visible = True
        'Else
        '    btn_new.Visible = False
        'End If


        If Not IsPostBack Then
            Populate_company()
            Populate_codes()
            btn_search_Click(sender, e)
            btn_search.Visible = True
            srch_company_cd_SelectedIndexChanged(Nothing, Nothing)
        End If

    End Sub

    Public Function isAuthToInsert() As String
        Dim v_ind As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoInsert('" & Session("emp_cd") & "','ItemIdentificationByStore.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                v_ind = MyDatareader2.Item("V_RES").ToString
            Else
                v_ind = "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

        If v_ind & "" = "" Then
            Return "N"
        ElseIf v_ind <> "Y" And v_ind <> "N" Then
            Return "N"
        Else
            Return v_ind
        End If

    End Function

    Public Function isAuthToUpd() As String
        Dim v_ind As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoUpdate('" & Session("emp_cd") & "','ItemIdentificationByStore.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                v_ind = MyDatareader2.Item("V_RES").ToString
            Else
                v_ind = "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

        If v_ind & "" = "" Then
            Return "N"
        ElseIf v_ind <> "Y" And v_ind <> "N" Then
            Return "N"
        Else
            Return v_ind
        End If

    End Function

    Public Function isAuthToDelete() As String
        Dim v_ind As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoDelete('" & Session("emp_cd") & "','ItemIdentificationByStore.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                v_ind = MyDatareader2.Item("V_RES").ToString
            Else
                v_ind = "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

        If v_ind & "" = "" Then
            Return "N"
        ElseIf v_ind <> "Y" And v_ind <> "N" Then
            Return "N"
        Else
            Return v_ind
        End If

    End Function

    Protected Sub btn_new_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_new.Click
        pnl_search.Visible = False
        pnl_update.Visible = True
        pnl_grid.Visible = False
        btn_delete.Visible = False
        btn_update.Text = "Create New"
    End Sub
    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click

        Dim v_msg As String = ""

        v_msg = validate_search_fields()
        If v_msg & "" <> "" Then
            lbl_msg.Text = v_msg
            Exit Sub
        End If

        GV_ident_codes.DataBind()
        GV_ident_codes.CancelEdit()
    End Sub
    Function validate_search_fields() As String
        Dim d As Date
        If srch_purch_dt_from.Text & "" <> "" Then
            If (Date.TryParse(srch_purch_dt_from.Text, d)) = False Then
                Return "Invalid effective date"
            Else
                clndr_purch_dt_from.SelectedDate = d
            End If
        Else
            clndr_purch_dt_from.SelectedDate = "1/1/2000"
        End If

        If srch_purch_dt_to.Text & "" <> "" Then
            If (Date.TryParse(srch_purch_dt_to.Text, d)) = False Then
                Return "Invalid ending date"
            Else
                clndr_purch_dt_to.SelectedDate = d
            End If
        Else
            clndr_purch_dt_to.SelectedDate = "1/1/2000"
        End If

        If srch_purch_dt_from.Text & "" <> "" And srch_purch_dt_to.Text & "" <> "" Then
            If (clndr_purch_dt_from.SelectedDate > clndr_purch_dt_to.SelectedDate) Then
                Return "Ending date should be greater tham effective date"
            End If
        End If

        Return ""
    End Function

    Function validate_update_fields() As String
        If (upd_ident_cd.SelectedIndex = 0) Then
            Return "Please select identification code"
        End If

        If (upd_str_cd.SelectedIndex = 0) Then
            Return "Please select store code"
        End If

        If (upd_item_cd.Text = "") Then
            Return "Please select item code"
        End If

        If (upd_dt_from.Text & "" = "" Or isEmpty(upd_dt_from.Text)) Then
            Return "Please select effective date"
        End If

        If (upd_dt_to.Text & "" = "" Or isEmpty(upd_dt_to.Text)) Then
            Return "Please select ending date"
        End If

        Dim d As Date
        If (Date.TryParse(upd_dt_from.Text, d)) = False Then
            Return "Invalid effective date"
        Else
            clndr_purch_dt_from2.SelectedDate = d
        End If

        If (Date.TryParse(upd_dt_to.Text, d)) = False Then
            Return "Invalid ending date"
        Else
            clndr_purch_dt_to2.SelectedDate = d
        End If

        If (clndr_purch_dt_from2.SelectedDate < Today.Date) Then
            Return "Starting date should NOT be before today's date"
        End If

        If (clndr_purch_dt_to2.SelectedDate < Today.Date) Then
            Return "Ending date should NOT be before today's date"
        End If

        If (clndr_purch_dt_from2.SelectedDate > clndr_purch_dt_to2.SelectedDate) Then
            Return "Ending date should be greater tham effective date"
        End If

        If IsValidItem(upd_item_cd.Text, upd_company_cd.Text) <> "Y" Then
            Return "Invalid item code. Please correct and try again"
        End If

        Return ""
    End Function

    Protected Sub GV_DataBind()
        populate_gridview()
    End Sub

    Protected Sub populate_gridview()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim v_st_cd_sql As String = ""
        Dim v_comp_cd_sql As String = ""
        Dim v_item_cd_sql As String = ""
        Dim v_ident_cd_sql As String = ""
        Dim v_eff_date_sql As String = ""
        Dim v_end_date_sql As String = ""

        If srch_company_cd.Text & "" = "ALL" Or isEmpty(srch_company_cd.Text) Then
            v_comp_cd_sql = ""
        Else
            v_comp_cd_sql = " and upper(b.CO_CD) like '" & UCase(srch_company_cd.SelectedValue) & "%'"
        End If

        If srch_str_cd.Text.ToUpper() = "ALL" Or isEmpty(srch_str_cd.Text) Then
            v_st_cd_sql = ""
        Else
            v_st_cd_sql = " and upper(a.ST_CD) = '" & UCase(srch_str_cd.SelectedValue) & "'"
        End If

        If srch_item_code.Text & "" = "" Or isEmpty(srch_item_code.Text) Then
            v_item_cd_sql = ""
        ElseIf srch_item_code.Text.Contains("%") Then
            v_item_cd_sql = " and upper(a.ITM_CD) like '" & UCase(srch_item_code.Text) & "%'"
        Else
            v_item_cd_sql = " and upper(a.ITM_CD) = '" & UCase(srch_item_code.Text) & "'"
        End If

        If srch_ident_cd.Text.ToUpper() = "ALL" Or isEmpty(srch_ident_cd.Text) Then
            v_ident_cd_sql = ""
        Else
            v_ident_cd_sql = " and upper(a.II_CD) = '" & UCase(srch_ident_cd.Text) & "'"
        End If

        If (srch_purch_dt_from.Text & "" = "") Then
            v_eff_date_sql = ""
        Else
            v_eff_date_sql = " and NOT (a.END_DT < '" & clndr_purch_dt_from.SelectedDate.ToString("dd/MMM/yyyy") & "')"
        End If

        If (srch_purch_dt_to.Text & "" = "") Then
            v_end_date_sql = ""
        Else
            v_end_date_sql = " and NOT (a.EFF_DT > '" & clndr_purch_dt_to.SelectedDate.ToString("dd/MMM/yyyy") & "')"
        End If


        GV_ident_codes.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        objSql.CommandText = "Select a.ST_CD, a.CO_CD, a.ITM_CD, a.II_CD, " &
            "a.EFF_DT, a.END_DT, b.DESCRIPTION, c.DES " &
            "From IICST a Join IICT b on a.II_CD = b.II_CD AND a.CO_CD = b.CO_CD " &
            "Join ITM c On a.ITM_CD = c.ITM_CD Where 1 = 1 "

        If v_comp_cd_sql <> "" Then
            objSql.CommandText &= v_comp_cd_sql
        End If

        If v_st_cd_sql <> "" Then
            objSql.CommandText &= v_st_cd_sql
        End If

        If v_item_cd_sql <> "" Then
            objSql.CommandText &= v_item_cd_sql
        End If

        If v_ident_cd_sql <> "" Then
            objSql.CommandText &= v_ident_cd_sql
        End If

        If (v_eff_date_sql <> "") Then
            objSql.CommandText &= v_eff_date_sql
        End If

        If (v_end_date_sql <> "") Then
            objSql.CommandText &= v_end_date_sql
        End If

        objSql.CommandText &= " and std_multi_co2.isvalidco3('" & Session("emp_init") & "', a.CO_CD) = 'Y' "

        objSql.CommandText &= " order by CO_CD, II_CD"

        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GV_ident_codes.DataSource = ds
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_ident_codes.DataSource = ds
                lbl_msg.Text = "No data to display"
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Sub

    Function getStat(ByVal p_cust_cd As String, ByVal p_custmpc_id As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT min(stat_cd) stat "
        sql = sql & " from so_aeroplan_custmpc_dtl "
        sql = sql & " where cust_cd = '" & p_cust_cd & "' "
        sql = sql & "   and custmpc_id = '" & p_custmpc_id & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("stat")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function getCustMpcStr(ByVal p_cust_cd As String, ByVal p_id As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT store_cd str "
        sql = sql & " from so_aeroplan "
        sql = sql & " where cust_cd = '" & p_cust_cd & "' "
        sql = sql & "   and custmpc_id = '" & p_id & "' "
        sql = sql & "   and del_doc_num = '" & p_id & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("str")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function

    Function getStartDt(ByVal p_mpc As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim v_mpc As String = ""

        Dim sql As String = "SELECT start_dt dt "
        sql = sql & " from aimia_mpc_hdr "
        sql = sql & " where mpc_code = '" & p_mpc & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("dt")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function

    Function IsValidItem(ByVal itm_cd As String, ByVal co_cd As String) As String
        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from itm a "
        sql &= " inner Join itm$itm_srt b on a.itm_cd = b.itm_cd "
        sql &= " where a.itm_cd = '" & itm_cd & "'"
        sql &= " and a.Inventory = 'Y'"
        'sql &= " and b.ITM_SRT_CD = '" & co_cd & "'"
        sql &= " and std_multi_co.isValidItm2('" & Session("emp_cd") & "', a.itm_cd) = 'Y'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value < 1 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function

    Public Function getStores() As DataSet

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        sql = " select store_cd str_cd, store_cd||' - '||store_name str_nm from store "
        sql = sql & " where std_multi_co2.isvalidstr2('" & Session("emp_init") & "',store_cd) = 'Y' "
        sql = sql & " order by 1 "

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)
            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

        Return ds

    End Function

    Public Function getIdentCode(ident_cd As String) As String
        Dim v_value As String = ""
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String

        sql = " select II_CD, DESCRIPTION from IICT "
        sql = sql & " where II_CD = '" & ident_cd & "' "
        sql = sql & " order by 1 "

        Try
            connErp.Open()
            cmdGetCodes = DisposablesManager.BuildOracleCommand(sql, connErp)
            dbReader = DisposablesManager.BuildOracleDataReader(cmdGetCodes)

            If dbReader.Read() Then
                v_value = dbReader.Item("DESCRIPTION")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        cmdGetCodes.Dispose()
        connErp.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "x"
        Else
            Return v_value
        End If

    End Function

    Protected Sub srch_dt1x_btn(ByVal sender As Object, ByVal e As System.EventArgs)
        srch_purch_dt_from.Text = ""
    End Sub
    Protected Sub srch_dt2x_btn(ByVal sender As Object, ByVal e As System.EventArgs)
        srch_purch_dt_to.Text = ""
    End Sub
    Protected Sub srch_dt1_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        srch_purch_dt_from.Text = clndr_purch_dt_from.SelectedDate.ToString("dd/MMM/yyyy")
    End Sub
    Protected Sub srch_dt2_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        srch_purch_dt_to.Text = clndr_purch_dt_to.SelectedDate.ToString("dd/MMM/yyyy")
    End Sub

    Protected Sub upd_dt1x_btn(ByVal sender As Object, ByVal e As System.EventArgs)
        upd_dt_from.Text = ""
    End Sub

    Protected Sub upd_dt2x_btn(ByVal sender As Object, ByVal e As System.EventArgs)
        upd_dt_to.Text = ""
    End Sub

    Protected Sub upd_dt1_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        upd_dt_from.Text = clndr_purch_dt_from2.SelectedDate.ToString("dd/MMM/yyyy")
    End Sub

    Protected Sub upd_dt2_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        upd_dt_to.Text = clndr_purch_dt_to2.SelectedDate.ToString("dd/MMM/yyyy")
    End Sub


    Public Sub Populate_str()
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_str_cd.Items.Clear()
        upd_str_cd.Items.Clear()

        sql = " select store_cd str_cd, store_cd||' - '||store_name name from store "
        sql = sql & " where std_multi_co2.isvalidstr7('" & Session("emp_init") & "',store_cd) = 'Y' "
        sql = sql & " order by 1 "

        If isSuperUser(Session("EMP_CD")) = "Y" Then
            srch_str_cd.Items.Insert(0, "All stores")
            srch_str_cd.Items.FindByText("All stores").Value = "ALL"
            srch_str_cd.SelectedIndex = 0
        End If

        upd_str_cd.Items.Insert(0, "Select the store")
        upd_str_cd.Items.FindByText("Select the store").Value = "ALL"
        upd_str_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_str_cd
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            With upd_str_cd
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Public Sub Populate_company()
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_company_cd.Items.Clear()

        sql = " select co_cd, co_cd||' - '||DES name from CO "
        sql = sql & " where std_multi_co2.isvalidco3('" & Session("emp_init") & "', co_cd) = 'Y' "
        sql = sql & " order by 2 "

        If isSuperUser(Session("EMP_CD")) = "Y" Then
            srch_company_cd.Items.Insert(0, "All companies")
            srch_company_cd.Items.FindByText("All companies").Value = "ALL"
            srch_company_cd.SelectedIndex = 0
        End If

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_company_cd
                .DataSource = ds
                .DataValueField = "co_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Public Sub Populate_codes()
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_ident_cd.Items.Clear()

        sql = " select ii_cd, ii_cd||' - '||description name from iict "
        sql = sql & " where std_multi_co2.isvalidco3('" & Session("emp_init") & "', co_cd) = 'Y' "
        sql = sql & " order by 1 "

        srch_ident_cd.Items.Insert(0, "All codes")
        srch_ident_cd.Items.FindByText("All codes").Value = "ALL"
        srch_ident_cd.SelectedIndex = 0

        upd_ident_cd.Items.Insert(0, "Select identification code")
        upd_ident_cd.Items.FindByText("Select identification code").Value = "ALL"
        upd_ident_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_ident_cd
                .DataSource = ds
                .DataValueField = "ii_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            With upd_ident_cd
                .DataSource = ds
                .DataValueField = "ii_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Protected Sub UserChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

    Function get_mpc_name(ByVal p_mpc As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT mpc_name nm "
        sql = sql & " from aimia_mpc_hdr "
        sql = sql & " where mpc_code = '" & p_mpc & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("nm")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function get_user_name(ByVal p_user As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT fname||' '||lname nm "
        sql = sql & " from emp "
        sql = sql & " where emp_init = '" & p_user & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("nm")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function IfAlreadyExists(ByVal p_e As ASPxDataValidationEventArgs) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "SELECT count(*) cnt from so_aeroplan_custmpc_dtl "
        sql = sql & " where cust_cd = '" & p_e.NewValues("CUST_CD") & "' "
        sql = sql & "   and mpc_cd = '" & p_e.NewValues("MPC_CODE") & "'"
        sql = sql & "   and nvl(qualify_pts,0) = nvl(" & p_e.NewValues("QUALIFY_PTS") & " ,0) " &
        sql = sql & "   and nvl(stat_cd,'x') = 'O' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value < 1 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function

    Protected Sub GV_ident_codes_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)

        If Session("aero_cust_cd") & "" <> "" Then
        Else
        End If

        e.NewValues("AEROPLAN_CRD_NUM") = ""
        e.NewValues("STORE_CD") = ""
        e.NewValues("AEROPLAN_PURCH_DT") = DateTime.Now
        e.NewValues("CRE_BY") = Session("EMP_INIT")
        e.NewValues("MPC_CODE") = ""
        e.NewValues("QUALIFY_PTS") = ""
        e.NewValues("CONTRACT") = ""
        e.NewValues("STAT_CD") = "O"
        e.NewValues("PROCESSING_DT") = DateTime.Now
        e.NewValues("CUSTMPC_ID") = ""
        e.NewValues("CMNT") = ""


    End Sub
    Protected Sub ASPxGV_ident_codes_RowValidating(ByVal sender As Object, ByVal e As ASPxDataValidationEventArgs)

    End Sub
    Protected Sub GV_row_update(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)

        Dim hasError As Boolean = False
        Dim errorMsg As String = ""
        Dim v_upd_ind As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        '--------------------------------------------------------------------
        ' update aeroplan# on so_aeroplan
        '--------------------------------------------------------------------
        conn.Open()

        sql = "UPDATE SO_AEROPLAN SET "
        sql = sql & " aeroplan_crd_num = '" & e.NewValues("AEROPLAN_CRD_NUM") & "' "
        sql = sql & " ,aeroplan_purch_dt = to_date ('" & FormatDateTime(e.NewValues("AEROPLAN_PURCH_DT"), 2) & "','mm/dd/RRRR') "
        sql = sql & " ,upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString(), 2) & "','mm/dd/RRRR') "
        sql = sql & " ,upd_by = '" & Session("emp_init") & "' "
        sql = sql & " ,store_cd = '" & Left(e.NewValues("STORE_CD"), 2) & "' "
        sql = sql & " where del_doc_num = '" & e.Keys.Item("CUSTMPC_ID") & "' "
        sql = sql & "   and cust_cd     = '" & e.OldValues("CUST_CD") & "' "
        sql = sql & "   and custmpc_id  = '" & e.Keys.Item("CUSTMPC_ID") & "' "

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()
        v_upd_ind = "Y"

        '-----------------------------------------------------------------------------------------
        'ADD DETAIL
        '-----------------------------------------------------------------------------------------

        Dim connstring As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim v_dt As String = getStartDt(Left(e.OldValues("MPC_CODE"), 4))

        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_aeroplan.populateDetail"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_id", OracleType.VarChar)).Value = e.Keys.Item("CUSTMPC_ID")
            myCMD.Parameters.Add(New OracleParameter("p_cus", OracleType.VarChar)).Value = UCase(e.NewValues("CUST_CD"))
            myCMD.Parameters.Add(New OracleParameter("p_mpc", OracleType.VarChar)).Value = Left(e.NewValues("MPC_CODE"), 4)
            myCMD.Parameters.Add(New OracleParameter("p_mpc_strt_dt", OracleType.DateTime)).Value = FormatDateTime(v_dt, DateFormat.ShortDate)
            myCMD.Parameters.Add(New OracleParameter("p_pts", OracleType.Number)).Value = CInt(e.NewValues("QUALIFY_PTS"))
            myCMD.Parameters.Add(New OracleParameter("p_cmnt", OracleType.VarChar)).Value = UCase(e.NewValues("CMNT"))

            myCMD.ExecuteNonQuery()

            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Error in creating Aeroplan Detail ")
        End Try

        '-----------------------------------------------------------------------------------------
        GV_ident_codes.CancelEdit()
        populate_gridview()
        GV_ident_codes.DataBind()
        e.Cancel = True

        If v_upd_ind = "Y" Then
            GV_ident_codes.Settings.ShowTitlePanel = True
            GV_ident_codes.SettingsText.Title = "** CUSTOMER RECORD IS UPDATED"
        End If

    End Sub
    Protected Sub GV_row_insert(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)

        Dim hasError As Boolean = False
        Dim errorMsg As String = ""
        Dim v_upd_ind As String = ""

        '---------------------------------------------------------------------------------------------
        '---------------------------------------- insert new SO_AEROPLAN ----------------------------
        '---------------------------------------------------------------------------------------------

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""

        'g_custmpc_id = GetCustMPC()
        Dim v_dummy_doc As String = g_custmpc_id.ToString()

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'Req2778 -purch_dt change from trunc(sysdate)
        sql = "Insert into so_aeroplan " &
              "(del_doc_num,aeroplan_crd_num,aeroplan_purch_dt,cust_cd,cre_dt,cre_by,store_cd, custmpc_id) " &
              " values ('" & v_dummy_doc & "','" & e.NewValues("AEROPLAN_CRD_NUM") & "' " &
              ",to_date ('" & FormatDateTime(e.NewValues("AEROPLAN_PURCH_DT"), 2) & "','mm/dd/RRRR'), '" & UCase(e.NewValues("CUST_CD")) & "'" &
              " , trunc(sysdate), '" & Session("emp_init") & "','" & Left(e.NewValues("STORE_CD"), 2) & "'" &
              " , '" & g_custmpc_id & "' )"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()

        '-----------------------------------------------------------------------------------------
        'ADD DETAIL   - 
        '-----------------------------------------------------------------------------------------
        Dim connstring As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim v_dt As String = getStartDt(Left(e.NewValues("MPC_CODE"), 4))

        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_aeroplan.populateDetail"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_id", OracleType.VarChar)).Value = g_custmpc_id
            myCMD.Parameters.Add(New OracleParameter("p_cus", OracleType.VarChar)).Value = UCase(e.NewValues("CUST_CD"))
            myCMD.Parameters.Add(New OracleParameter("p_mpc", OracleType.VarChar)).Value = Left(e.NewValues("MPC_CODE"), 4)
            myCMD.Parameters.Add(New OracleParameter("p_mpc_strt_dt", OracleType.DateTime)).Value = FormatDateTime(v_dt, DateFormat.ShortDate)
            myCMD.Parameters.Add(New OracleParameter("p_pts", OracleType.Number)).Value = e.NewValues("QUALIFY_PTS")
            myCMD.Parameters.Add(New OracleParameter("p_cmnt", OracleType.VarChar)).Value = UCase(e.NewValues("CMNT"))

            myCMD.ExecuteNonQuery()

            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Error in creating Aeroplan Detail ")
        End Try
        '-----------------------------------------------------------------------------------------

        e.Cancel = True
        populate_gridview()
        GV_ident_codes.CancelEdit()
        GV_ident_codes.DataBind()

        GV_ident_codes.Settings.ShowTitlePanel = True
        GV_ident_codes.SettingsText.Title = "*** CUSTOMER RECORD IS ADDED"

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Function getErr(ByVal p_cd As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT des err "
        sql = sql & " from aeroplan_errcode "
        sql = sql & " where err_code = '" & p_cd & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("err")
            Else
                v_value = "Error Code Not Found"
            End If
        Catch
            v_value = "Error Code Not Found"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "Error Code Not Found"
        Else
            Return v_value
        End If

    End Function

    Function getProcessCd(ByVal p_custmpc_id As Integer) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT processing_cd cd "
        sql = sql & " from so_aeroplan_custmpc_dtl "
        sql = sql & " where custmpc_id  = '" & p_custmpc_id & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cd")
            Else
                v_value = "x"
            End If
        Catch
            v_value = "x"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "x"
        Else
            Return v_value
        End If

    End Function

    Private Sub btn_update_Click(sender As Object, e As EventArgs) Handles btn_update.Click
        Dim v_msg As String = ""

        v_msg = validate_update_fields()
        If v_msg & "" <> "" Then
            lbl_msg.Text = v_msg
            Exit Sub
        End If

        If btn_update.Text = "Update" Then
            'If isAuthToUpd() = "Y" Then
            'End If
            v_msg = update()
        Else
            'If isAuthToInsert() = "Y" Then
            'End If
            v_msg = create()
        End If

        If v_msg & "" <> "" Then
            lbl_msg.Text = v_msg
            Exit Sub
        End If

        Response.Redirect("ItemIdentificationByStore.aspx")
    End Sub

    Private Sub btn_cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click
        Response.Redirect("ItemIdentificationByStore.aspx")
    End Sub

    Function create() As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand
        Try
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            objsql2 = DisposablesManager.BuildOracleCommand
            objsql2.CommandText = "INSERT INTO IICST (ST_CD, CO_CD, ITM_CD, II_CD, EFF_DT, END_DT) " &
                " VALUES(:ST_CD, :CO_CD, :ITM_CD, :II_CD, :EFF_DT, :END_DT)"
            objsql2.Connection = conn
            conn.Open()
            objsql2.Parameters.Add(":ST_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":CO_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":II_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":EFF_DT", OracleType.VarChar)
            objsql2.Parameters.Add(":END_DT", OracleType.VarChar)

            objsql2.Parameters(":ST_CD").Value = upd_str_cd.SelectedValue
            objsql2.Parameters(":CO_CD").Value = upd_company_cd.Text
            objsql2.Parameters(":ITM_CD").Value = upd_item_cd.Text
            objsql2.Parameters(":II_CD").Value = upd_ident_cd.Text
            objsql2.Parameters(":EFF_DT").Value = clndr_purch_dt_from2.SelectedDate.ToString("dd/MMM/yyyy")
            objsql2.Parameters(":END_DT").Value = clndr_purch_dt_to2.SelectedDate.ToString("dd/MMM/yyyy")

            objsql2.ExecuteNonQuery()

        Catch ex As Exception
            conn.Close()
            Return ex.Message
        End Try
        conn.Close()

        Return ""
    End Function

    Function update() As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand
        Try
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            objsql2 = DisposablesManager.BuildOracleCommand
            objsql2.CommandText = "UPDATE IICST SET CO_CD = :NEW_CO_CD, II_CD = :NEW_II_CD, ST_CD = :NEW_ST_CD, ITM_CD = :NEW_ITM_CD, EFF_DT = :EFF_DT, END_DT = :END_DT WHERE ST_CD = :ST_CD AND II_CD = :II_CD AND ITM_CD = :ITM_CD"
            objsql2.Connection = conn
            conn.Open()
            objsql2.Parameters.Add(":ST_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":II_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":NEW_CO_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":NEW_ST_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":NEW_ITM_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":NEW_II_CD", OracleType.VarChar)
            objsql2.Parameters.Add(":EFF_DT", OracleType.VarChar)
            objsql2.Parameters.Add(":END_DT", OracleType.VarChar)

            objsql2.Parameters(":NEW_CO_CD").Value = upd_company_cd.Text
            objsql2.Parameters(":ST_CD").Value = hid_str_cd.Value
            objsql2.Parameters(":ITM_CD").Value = hid_item_cd.Value
            objsql2.Parameters(":II_CD").Value = hid_ident_cd.Value
            objsql2.Parameters(":NEW_ST_CD").Value = upd_str_cd.SelectedValue
            objsql2.Parameters(":NEW_ITM_CD").Value = upd_item_cd.Text
            objsql2.Parameters(":NEW_II_CD").Value = upd_ident_cd.Text
            objsql2.Parameters(":EFF_DT").Value = clndr_purch_dt_from2.SelectedDate.ToString("dd/MMM/yyyy")
            objsql2.Parameters(":END_DT").Value = clndr_purch_dt_to2.SelectedDate.ToString("dd/MMM/yyyy")

            objsql2.ExecuteNonQuery()
        Catch ex As Exception
            conn.Close()
            Return ex.Message
        End Try
        conn.Close()
        Return ""
    End Function


    Protected Sub GV_ident_codes_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles GV_ident_codes.RowCommand
        If e.CommandArgs.CommandName = "Select" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgs.CommandArgument)
            Dim fieldValues As List(Of Object) = New List(Of Object)

            upd_company_cd.Text = GV_ident_codes.GetRowValues(index, "CO_CD").ToString()
            filter_upd_str(upd_company_cd.Text)
            upd_str_cd.Text = GV_ident_codes.GetRowValues(index, "ST_CD").ToString()
            hid_str_cd.Value = GV_ident_codes.GetRowValues(index, "ST_CD").ToString()
            upd_item_cd.Text = GV_ident_codes.GetRowValues(index, "ITM_CD").ToString()
            hid_item_cd.Value = GV_ident_codes.GetRowValues(index, "ITM_CD").ToString()
            upd_ident_cd.Text = GV_ident_codes.GetRowValues(index, "II_CD").ToString()
            hid_ident_cd.Value = GV_ident_codes.GetRowValues(index, "II_CD").ToString()
            upd_item_desc.Text = GV_ident_codes.GetRowValues(index, "DES").ToString()
            upd_ident_desc.Text = GV_ident_codes.GetRowValues(index, "DESCRIPTION").ToString()
            upd_dt_from.Text = Date.Parse(GV_ident_codes.GetRowValues(index, "EFF_DT")).ToString("dd/MMM/yyyy")
            upd_dt_to.Text = Date.Parse(GV_ident_codes.GetRowValues(index, "END_DT")).ToString("dd/MMM/yyyy")
            clndr_purch_dt_from2.SelectedDate = Date.Parse(GV_ident_codes.GetRowValues(index, "EFF_DT")).ToString("dd/MMM/yyyy")
            clndr_purch_dt_to2.SelectedDate = Date.Parse(GV_ident_codes.GetRowValues(index, "END_DT")).ToString("dd/MMM/yyyy")


            pnl_search.Visible = False
            pnl_update.Visible = True
            pnl_grid.Visible = False
            btn_delete.Visible = True
            lbl_company_cd.Visible = True
            lbl_ident_cd.Visible = True
            lbl_item_desc.Visible = True
            'upd_str_cd.Enabled = False
            'upd_ident_cd.Enabled = False
            btn_update.Text = "Update"
        End If
    End Sub

    Private Sub btn_delete_Click(sender As Object, e As EventArgs) Handles btn_delete.Click
        Dim v_msg As String = ""

        v_msg = delete()

        If v_msg & "" <> "" Then
            lbl_msg.Text = v_msg
            Exit Sub
        End If

        Response.Redirect("ItemIdentificationByStore.aspx")
    End Sub

    Private Function delete() As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            sql = "delete IICST " &
                  " where ST_CD = '" & upd_str_cd.SelectedValue & "' " &
                  "   and II_CD = '" & upd_ident_cd.SelectedValue & "'" &
                  "   and ITM_CD = '" & upd_item_cd.Text & "'"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()

        Catch ex As Exception
            conn.Close()
            Throw ex
        End Try

        conn.Close()
    End Function

    Function getCompanyCd() As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT CO_CD "
        sql = sql & " from IICT "
        sql = sql & " where II_CD = '" & upd_ident_cd.SelectedValue & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("CO_CD")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "x"
        Else
            Return v_value
        End If

    End Function

    Function getCompanyStores() As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT CO_CD "
        sql = sql & " from STORE "
        sql = sql & " where store_cd = '" & upd_str_cd.SelectedValue & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("CO_CD")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "x"
        Else
            Return v_value
        End If

    End Function

    Private Sub upd_ident_cd_SelectedIndexChanged(sender As Object, e As EventArgs) Handles upd_ident_cd.SelectedIndexChanged
        If upd_ident_cd.SelectedIndex <> 0 Then
            upd_company_cd.Text = getCompanyCd()
            upd_ident_desc.Text = getIdentCode(upd_ident_cd.SelectedValue)
            filter_upd_str(upd_company_cd.Text)
        Else
            upd_company_cd.Text = ""
            upd_ident_desc.Text = ""
            upd_str_cd.Items.Clear()
        End If
    End Sub

    Public Sub filter_upd_str(co_cd As String)
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        upd_str_cd.Items.Clear()

        sql = " select store_cd str_cd, store_cd||' - '||store_name name from store "
        sql = sql & " where std_multi_co2.isvalidstr7('" & Session("emp_init") & "',store_cd) = 'Y' "
        sql = sql & " and co_cd = '" & co_cd & "'"
        sql = sql & " order by 1 "

        upd_str_cd.Items.Insert(0, "Select the store")
        upd_str_cd.Items.FindByText("Select the store").Value = "ALL"
        upd_str_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With upd_str_cd
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Public Sub filter_srch_str(co_cd As String)
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_str_cd.Items.Clear()

        sql = " select store_cd str_cd, store_cd||' - '||store_name name from store "
        sql = sql & " where std_multi_co2.isvalidstr7('" & Session("emp_init") & "',store_cd) = 'Y' "
        If co_cd <> "ALL" Then sql = sql & " and co_cd = '" & co_cd & "'"
        sql = sql & " order by 1 "

        srch_str_cd.Items.Insert(0, "Select the store")
        srch_str_cd.Items.FindByText("Select the store").Value = "ALL"
        srch_str_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_str_cd
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Private Sub srch_company_cd_SelectedIndexChanged(sender As Object, e As EventArgs) Handles srch_company_cd.SelectedIndexChanged
        filter_srch_str(srch_company_cd.SelectedValue)
    End Sub

    Public Shared Function isSuperUser(ByRef p_emp_cd As String)
        Dim p_sup_flag As String = "N'"
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_multi_co.isSuperUsr"
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_emp_cd", OracleType.VarChar)).Value = p_emp_cd
        myCMD.Parameters.Add("p_co_grp_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_co_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_ind", OracleType.VarChar, 1).Direction = ParameterDirection.Output

        Try

            myCMD.ExecuteNonQuery()

            If Not IsDBNull(myCMD.Parameters("p_ind").Value) Then
                p_sup_flag = myCMD.Parameters("p_ind").Value
            End If

        Catch x As Exception
            Throw
        Finally
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return p_sup_flag
    End Function

    Private Sub srch_ident_cd_SelectedIndexChanged(sender As Object, e As EventArgs) Handles srch_ident_cd.SelectedIndexChanged
        If srch_ident_cd.SelectedIndex <> 0 Then
            srch_ident_desc.Text = getIdentCode(srch_ident_cd.SelectedValue)
        Else
            srch_ident_desc.Text = ""
        End If

    End Sub

    Private Sub srch_item_code_TextChanged(sender As Object, e As EventArgs) Handles srch_item_code.TextChanged
        If (String.IsNullOrEmpty(srch_item_code.Text) OrElse srch_item_code.Text.Contains("%")) Then
            srch_item_desc.Text = ""
        Else
            srch_item_desc.Text = Populate_item_desc(srch_item_code.Text, True)
        End If
    End Sub

    Private Sub upd_item_cd_TextChanged(sender As Object, e As EventArgs) Handles upd_item_cd.TextChanged
        If (String.IsNullOrEmpty(upd_item_cd.Text)) Then
            upd_item_desc.Text = ""
        Else
            upd_item_desc.Text = Populate_item_desc(upd_item_cd.Text, True)
        End If
    End Sub

    Private Function Populate_item_desc(itm_cd As String, isInventory As Boolean) As String
        Dim v_value As String = "Invalid Item"

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT DES from itm"
        sql &= " where itm_cd = '" & itm_cd & "'"
        If (isInventory) Then sql &= " and Inventory = 'Y'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("DES")
            End If
        Catch
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        Return v_value

    End Function

    Private Sub srch_purch_dt_from_TextChanged(sender As Object, e As EventArgs) Handles srch_purch_dt_from.TextChanged
        If srch_purch_dt_from.Text & "" = "" Then Exit Sub
        Dim d As Date
        If (Date.TryParse(srch_purch_dt_from.Text, d)) = False Then
            lbl_msg.Text = "Invalid effective date"
        Else
            clndr_purch_dt_from.SelectedDate = d
        End If
    End Sub

    Private Sub srch_purch_dt_to_TextChanged(sender As Object, e As EventArgs) Handles srch_purch_dt_to.TextChanged
        If srch_purch_dt_to.Text & "" = "" Then Exit Sub
        Dim d As Date
        If (Date.TryParse(srch_purch_dt_to.Text, d)) = False Then
            lbl_msg.Text = "Invalid ending date"
        Else
            clndr_purch_dt_to.SelectedDate = d
        End If

    End Sub

    Private Sub upd_dt_from_TextChanged(sender As Object, e As EventArgs) Handles upd_dt_from.TextChanged
        If upd_dt_from.Text & "" = "" Then Exit Sub
        Dim d As Date
        If (Date.TryParse(upd_dt_from.Text, d)) = False Then
            lbl_msg.Text = "Invalid effective date"
        Else
            clndr_purch_dt_from2.SelectedDate = d
        End If
    End Sub

    Private Sub upd_dt_to_TextChanged(sender As Object, e As EventArgs) Handles upd_dt_to.TextChanged
        If upd_dt_to.Text & "" = "" Then Exit Sub
        Dim d As Date
        If (Date.TryParse(upd_dt_to.Text, d)) = False Then
            lbl_msg.Text = "Invalid ending date"
        Else
            clndr_purch_dt_to2.SelectedDate = d
        End If
    End Sub
End Class
