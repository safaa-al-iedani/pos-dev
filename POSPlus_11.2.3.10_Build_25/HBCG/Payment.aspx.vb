Imports System.Data.OracleClient
Imports AppUtils
Imports HBCG_Utils
Imports SD_Utils

Imports Renci.SshNet   'lucy add the 5 lines
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp

Partial Class Payment
    Inherits POSBasePage

    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private thePaymentBiz As PaymentBiz = New PaymentBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Dim add_pmt_amt As Double = 0


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("csh_act_init") = ""

        ' start lucy add
        Dim str_as_cd As String = cbo_finance_company.SelectedValue '
        Dim str_mop_tp As String = lbl_pmt_tp.Text                  'lucy add

        Dim v_brk_user As String   'lucy, req-214, nov-2019
        v_brk_user = is_brk_user()
        If v_brk_user = "Y" Then
            btn_add_spc.Visible = True
        Else
            btn_add_spc.Visible = False
        End If

        'mariam -Oct 4,2016-Price Change Approval
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            If Request("LEAD") <> "TRUE" Then
                Response.Redirect("order_detail.aspx")
            Else
                Response.Redirect("order_detail.aspx?LEAD=TRUE")
            End If
        End If
        'mariam -end

        If Len(str_as_cd) > 2 And str_mop_tp = "FI" And InStr(UCase(Session("lucy_display")), "APPR") = 0 Then    'lucy add ,but does not work
            ASPxPopupControl1.ShowOnPageLoad = False  'lucy add 
            ASPxPopupControl2.ShowOnPageLoad = False  'lucy add 
            ASPxPopupControl8.ShowOnPageLoad = False
            lucy_lbl_pay.Text = "Click Save after entering financial information"
            lucy_lbl_pay.Visible = False
            ' IT Req 3354
            Dim custTp As String = "I"
            If Session("cust_cd") = "NEW" Then
                custTp = LeonsBiz.GetCustomerType(Session("cust_cd"), Session.SessionID.ToString.Trim)
            End If
            If Session("cust_cd") <> "NEW" Then
                custTp = LeonsBiz.GetCustomerType(Session("cust_cd"))
            End If
            If (Not Session("cust_cd") Is Nothing And
                Not custTp.Equals("C")) Then
                txt_fi_appr.Visible = True
                Literal1.Visible = True
            End If  'lucy add             
        End If  'lucy add 

        If InStr(UCase(Session("lucy_display")), "APPR") > 0 Then
            lucy_lbl_aprv.ForeColor = Color.Green
            'lbl_Error.Text = "Authorized transaction on file, commit to save the transaction"
            lbl_Error.Text = Resources.LibResources.Label837
        ElseIf InStr(UCase(Session("lucy_display")), "DECLINE") > 0 Or InStr(UCase(Session("lucy_display")), "REFUSEE") > 0 Then
            lucy_lbl_aprv.ForeColor = Color.Red

        Else
            lucy_lbl_aprv.ForeColor = Color.White

        End If
        lucy_lbl_aprv.Text = Session("lucy_display")
        'end lucy add

        ' MM - 3939 - Enter key is causing exit from amount field in payment window
        Me.Form.DefaultButton = Me.btn_add.UniqueID
        Dim Take_deposit As Double = 0
        If ConfigurationManager.AppSettings("show_po").ToString = "Y" And Session("ord_tp_cd") = "SAL" Then
            PO_Table.Visible = True
            If Session("PO") & "" <> "" Then
                txt_po.Text = Session("PO")
            End If
        Else
            PO_Table.Visible = False
        End If

        If Session("tet_cd") & "" = "" Then
            Take_deposit = Calculate_Take_With()
        Else
            Take_deposit = CDbl(Session("Take_Deposit"))
        End If

        If Session("sub_total") & "" = "" And Session("ord_tp_cd") <> "CRM" Then
            lbl_Error.Text = "There is currently not a balance on this order"
            mop_drp.Enabled = False
            btn_add.Enabled = False
            TextBox1.Enabled = False
            lbl_cod.Visible = False
            txt_dp.Visible = False
            btn_calc_dep.Visible = False
        ElseIf Session("ord_tp_cd") = "CRM" Then
            'If Refund_Security() = True Then
            '    lbl_Error.Text = "** PLEASE NOTE ** - You are processing a refund.  Payments entered are being returned to the customer."
            '    mop_drp.Enabled = True
            '    btn_add.Enabled = True
            '    TextBox1.Enabled = True
            '    lbl_cod.Visible = False
            '    txt_dp.Visible = True
            '    btn_calc_dep.Visible = True
            '    If Not IsPostBack = True Then
            '        mop_drp_Populate()
            '        If Session("payment") & "" <> "" Then
            '            'Populate Datagrid with new records
            '            bindgrid()
            '        End If
            '    End If
            'Else
            lbl_Error.Text = "** Refunds cannot be processed through the order entry screens **"
            mop_drp.Enabled = False
            btn_add.Enabled = False
            TextBox1.Enabled = False
            lbl_cod.Visible = False
            txt_dp.Visible = False
            btn_calc_dep.Visible = False
            'End If
        ElseIf Session("ord_tp_cd") = "MCR" Then
            'If Refund_Security() = True Then
            '    lbl_Error.Text = "** PLEASE NOTE ** - You are processing a refund.  Payments entered are being returned to the customer."
            '    mop_drp.Enabled = True
            '    btn_add.Enabled = True
            '    TextBox1.Enabled = True
            '    lbl_cod.Visible = False
            '    txt_dp.Visible = True
            '    btn_calc_dep.Visible = True
            '    If Not IsPostBack = True Then
            '        mop_drp_Populate()
            '        If Session("payment") & "" <> "" Then
            '            'Populate Datagrid with new records
            '            bindgrid()
            '        End If
            '    End If
            'Else
            lbl_Error.Text = "** Refunds cannot be processed through the order entry screens **"
            mop_drp.Enabled = False
            btn_add.Enabled = False
            TextBox1.Enabled = False
            lbl_cod.Visible = False
            txt_dp.Visible = False
            btn_calc_dep.Visible = False
            'End If
        Else
            If Not IsPostBack = True Then
                lbl_Error.Text = ""
                calculate_total()
                If Session("payment") & "" <> "" Then
                    'Populate Datagrid with new records
                    bindgrid()
                End If
                If Session("cash_carry") = "Y" Then
                    lbl_Error.Text = "The minimum required deposit on this order is " & FormatCurrency(CDbl(Session("Grand_Total")), 2)
                    If IsNumeric(Session("payments")) Then
                        TextBox1.Text = CDbl(Session("grand_total")) - CDbl(Session("payments"))
                    Else
                        TextBox1.Text = CDbl(Session("grand_total"))
                    End If
                ElseIf CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) > 0 Then
                    If ConfigurationManager.AppSettings("dep_req").ToString = "REQ" Then
                        lbl_Error.Text = "The minimum required deposit on this order is " & FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                        TextBox1.Text = FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                    Else
                        'Daniela french
                        'lbl_Error.Text = "The recommended deposit on this order is " & FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                        lbl_Error.Text = Resources.LibResources.Label600 & " " & FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                        TextBox1.Text = FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                    End If
                ElseIf Take_deposit > 0 Then
                    lbl_Error.Text = Resources.LibResources.Label600 & " " & FormatCurrency(Take_deposit, 2)
                    TextBox1.Text = FormatCurrency(Take_deposit, 2)
                End If
                'get the cod value
                If Session("grand_total") > 0 Then
                    If IsNumeric(Session("grand_total")) And IsNumeric(Session("payments")) Then
                        lbl_cod.Text = CDbl(Session("grand_total")) - CDbl(Session("payments"))
                    End If
                    lbl_cod.Text = FormatCurrency(lbl_cod.Text, 2)
                    mop_drp.Enabled = True
                    btn_add.Enabled = True
                    TextBox1.Enabled = True
                    lbl_cod.Visible = True
                    txt_dp.Visible = True
                    btn_calc_dep.Visible = True
                End If
                ' populate the Method Of Payment drop down
                mop_drp_Populate()
            End If
            Verify_Take_With()
        End If

        If Not IsPostBack And Session("payment") & "" = "" Then
            Add_Existing_Payments()
        End If

        If IsNumeric(TextBox1.Text) And IsNumeric(lbl_cod.Text) Then
            If CDbl(TextBox1.Text) > CDbl(lbl_cod.Text) Then
                TextBox1.Text = lbl_cod.Text
            End If
        End If

        If IsNumeric(lbl_cod.Text) Then
            If Session("cash_carry") = "TRUE" And CDbl(lbl_cod.Text) <> 0 Then
                'lbl_Error.Text = "Cash and carry orders must be paid in full"
                lbl_Error.Text = Resources.LibResources.Label59
                ' TextBox1.Text = lbl_cod.Text  'lucy comment to allow multiple payments
            End If
            If CDbl(lbl_cod.Text) = 0 Then
                lbl_Error.Text = ""
                'TextBox1.Text = 0
            End If
        End If

        ' MM-6376
        ' Checking the current cash drawer is active or not     

        If Not IsPostBack Then
            If (Session("tran_dt")) Is Nothing Then
                Session("tran_dt") = FormatDateTime(Date.Today, DateFormat.ShortDate)
            End If

            If Not (Session("store_cd")) Is Nothing AndAlso Not (Session("csh_dwr_cd")) Is Nothing Then
                If (theSalesBiz.IsCashDrawerActive(Session("CO_CD"), Session("store_cd"), Session("csh_dwr_cd"), Session("tran_dt"))) Then
                    ' If the cash drawer is InActive then payment should not be allowed
                    btn_add.Enabled = True
                    mop_drp.Enabled = True
                    TextBox1.Enabled = True
                Else
                    btn_add.Enabled = False
                    mop_drp.Enabled = False
                    TextBox1.Enabled = False
                    lbl_Error.Text = Resources.POSMessages.MSG0032
                End If
            End If
        End If

        If InStr(UCase(Session("lucy_display")), "APPR") > 0 Then  'lucy

            'lbl_Error.Text = "Authorized transaction on file, commit to save the transaction"
            lbl_Error.Text = Resources.LibResources.Label837
        End If
        ' Daniela April 09 prevent payment if no customer selected
        If Session("cust_cd") & "" = "" Then
            'lbl_Error.Text = "Customer has not been selected"
            lbl_Error.Text = Resources.LibResources.Label136
            btn_add.Enabled = False
            btn_calc_dep.Enabled = False
        End If
        ' Daniela Nov 26 prevent payment if cashier not activated
        If Session("csh_act_init") = "INVALIDCASHIER" Then
            lbl_Error.Text = "There is a problem with your cashier activation (not activated, invalid post date)"
            btn_add.Enabled = False
            btn_calc_dep.Enabled = False
        End If

        'Alice added for request #2197 on Aug 01, 2018

        Dim custType As String = String.Empty
        If Session("cust_cd") = "NEW" Then
            custType = LeonsBiz.GetCustomerType(Session("cust_cd"), Session.SessionID.ToString.Trim)
        Else
            custType = LeonsBiz.GetCustomerType(Session("cust_cd"))
        End If

        Session("PO#") = txt_po.Text.Trim()
        Session("POchecked") = rbtPo.SelectedValue
        Session("custType") = custType
        If txt_po.Text.Trim() = "" And custType.ToUpper = "C" And Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL Then
            litPo.Visible = True
            rbtPo.Visible = True
        Else
            litPo.Visible = False
            rbtPo.Visible = False
        End If

        If Not IsPostBack Then
            rbtPo.SelectedValue = Session("POchecked")
            If rbtPo.SelectedValue = "YES" And txt_po.Text.Trim() = String.Empty And custType.ToUpper = "C" Then
                txt_po.Focus()
            End If

        End If
    End Sub
    Protected Sub rbtPo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbtPo.SelectedIndexChanged
        If rbtPo.SelectedValue = "YES" Then
            Session("POchecked") = "YES"
            txt_po.Focus()
        Else
            Session("POchecked") = "NO"
        End If
    End Sub

    Protected Function is_brk_user() As String
        ' Lucy Req-214, nov-2019

        Dim v_ind As String = String.Empty
        Dim v_emp_init As String = Session("emp_init")
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String =
        " SELECT 'Y' as AAA FROM STORE S ,co_grp cg,emp" &
        " WHERE cg.co_grp_cd='BRK'" &
        " AND cg.co_cd = s.co_cd " &
        " AND emp.home_store_cd = s.store_cd " &
        " AND emp.emp_init='" & v_emp_init & "'"



        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("AAA")
            Else
                Return "N"
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_ind & "" = "" Then
            Return "N"
        ElseIf v_ind = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function


    Protected Sub lucy_Btn_new_cust_Click(sender As Object, e As EventArgs) Handles lucy_btn_new_cust.Click
        Dim strfinCo = cbo_finance_company.SelectedValue
        Session("lucy_as_cd") = strfinCo
        ' ASPxPopupControl2.ShowOnPageLoad = False


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        'IT Req 3354
        If lbl_pmt_tp.Text = "FI" Then
            If txt_fi_appr.Text.isNotEmpty() And (Len(txt_fi_appr.Text) <> 7 Or Not IsNumeric(txt_fi_appr.Text)) Then
                lbl_fi_warning.Text = Resources.LibResources.Label835 '"Auth# must be 7 Digits"
                txt_fi_appr.Focus()
                ASPxPopupControl2.ShowOnPageLoad = True
                Exit Sub
            End If
        End If

        'R1999 sabrina
        If lbl_pmt_tp.Text = "DCS" Then
            'sabrina NOV20
            If txt_fi_acct.Text & "" <> "" Then
                'sabrina-french dec14 lbl_fi_warning.Text = "Cannot proceed-Account# not allowed for NEW CUSTOMER"
                lbl_fi_warning.Text = Resources.LibResources.Label838
                txt_fi_acct.Focus()
                Exit Sub
            End If
            'sabrina Nov20
            txt_fi_acct.Enabled = False
            txt_fi_exp_dt.Enabled = False
            If txt_fi_appr.Text & "" = "" Then
                txt_fi_appr.Focus()
                lbl_fi_warning.Text = Resources.LibResources.Label836 'sabrina-french dec14 "7 digit Auth# Required"
                Exit Sub
            Else
                If Len(txt_fi_appr.Text) < 5 Or Len(txt_fi_appr.Text) > 10 Then ' Alice update on Feb01, 2019
                    lbl_fi_warning.Text = Resources.LibResources.Label1013 'sabrina-french dec14 "Auth# must be between 5 - 10 characters" 
                    txt_fi_appr.Focus()
                    Exit Sub
                End If
            End If

            If LeonsBiz.isvalidDCSplan(cbo_finance_company.SelectedValue()) <> "Y" Then
                cbo_finance_company.Focus()
                lbl_fi_warning.Text = Resources.LibResources.Label861 'sabrina-french dec14 "Select a Plan"
                Exit Sub
            End If

        End If
        'R1999 sabrina end        
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        cmdInsertItems.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = :PMT"
        cmdInsertItems.Parameters.Add(":PMT", OracleType.VarChar)
        cmdInsertItems.Parameters(":PMT").Value = lbl_pmt_tp.Text
        cmdInsertItems.Connection = conn
        sAdp = DisposablesManager.BuildOracleDataAdapter(cmdInsertItems)


        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        'If lbl_pmt_tp.Text = "WDR" Then
        If Is_Finance_DP(lbl_pmt_tp.Text) = True Then
            bc = "FI"
        End If
        txt_fi_exp_dt.Text = Session("lucy_exp_dt") 'lucy
        Dim str_fi_acct_num As String = txt_fi_acct.Text 'lucy
        Dim str_enc_acct_num = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")  'lucy
        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO,  FIN_PROMO_CD, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO,  :FIN_PROMO_CD, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            objSql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objSql.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp.Text
            objSql.Parameters(":SECURITY_CD").Value = txt_fi_sec_cd.Text
            objSql.Parameters(":EXP_DT").Value = txt_fi_exp_dt.Text
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":FIN_CO").Value = cbo_finance_company.SelectedValue
            ' objSql.Parameters(":FI_ACCT_NUM").Value = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")  'lucy may need to use our function
            objSql.Parameters(":FIN_PROMO_CD").Value = cbo_promo.SelectedValue
            objSql.Parameters(":APPROVAL_CD").Value = txt_fi_appr.Text
            objSql.Parameters(":DL_STATE").Value = txt_fi_dl_st.Text
            objSql.Parameters(":DL_LICENSE_NO").Value = txt_fi_dl_no.Text

            objSql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        ASPxPopupControl2.ShowOnPageLoad = False
        lbl_pmt_tp.Text = ""
        Session("payment") = True
        lbl_pmt_amt.Text = ""
        calculate_total()
        ASPxPopupControl1.ShowOnPageLoad = True
        Response.Redirect("Payment.aspx")
        ASPxPopupControl2.ShowOnPageLoad = False
    End Sub
    'Alice added on Oct 05, 2018 for request 4115
    Protected Sub btn_save_flx_Click(sender As Object, e As EventArgs) Handles btn_save_flx.Click

        Dim strfinCo = cbo_finance_company_flx.SelectedValue
        Session("lucy_as_cd_flx") = strfinCo
        ' ASPxPopupControl2.ShowOnPageLoad = False


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        ''IT Req 3354
        'If lbl_pmt_tp_flx.Text = "FI" Then
        '    If txt_fi_appr_flx.Text.isNotEmpty() And (Len(txt_fi_appr_flx.Text) <> 6 Or Not IsNumeric(txt_fi_appr_flx.Text)) Then
        '        lbl_fi_warning_flx.Text = Resources.LibResources.Label1002 '"Auth# must be 6 Digits"
        '        txt_fi_appr_flx.Focus()
        '        ASPxPopupControl8.ShowOnPageLoad = True
        '        Exit Sub
        '    End If
        'End If

        'R1999 sabrina
        If lbl_pmt_tp_flx.Text = PaymentUtils.Bank_MOP_CD.FLEXITI Then


            If txt_fi_appr_flx.Text & "" = "" Then
                txt_fi_appr_flx.Focus()
                lbl_fi_warning_flx.Text = Resources.LibResources.Label836 'sabrina-french dec14 "6 digit Auth# Required"
                Exit Sub
            Else
                If Len(txt_fi_appr_flx.Text) <> 6 Then
                    lbl_fi_warning_flx.Text = Resources.LibResources.Label1002 'sabrina-french dec14 "Auth# must be 6 Digits"
                    txt_fi_appr_flx.Focus()
                    Exit Sub
                End If
            End If

            If LeonsBiz.isvalidDCSplan(cbo_finance_company_flx.SelectedValue()) <> "Y" Then
                cbo_finance_company_flx.Focus()
                lbl_fi_warning_flx.Text = Resources.LibResources.Label861 'sabrina-french dec14 "Select a Plan"
                Exit Sub
            End If

        End If
        'R1999 sabrina end        
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        cmdInsertItems.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = :PMT"
        cmdInsertItems.Parameters.Add(":PMT", OracleType.VarChar)
        cmdInsertItems.Parameters(":PMT").Value = lbl_pmt_tp_flx.Text
        cmdInsertItems.Connection = conn
        sAdp = DisposablesManager.BuildOracleDataAdapter(cmdInsertItems)


        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        If Is_Finance_DP(lbl_pmt_tp_flx.Text) = True Then
            bc = "FI"
        End If

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO,  FIN_PROMO_CD, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO,  :FIN_PROMO_CD, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            objSql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objSql.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp_flx.Text
            objSql.Parameters(":SECURITY_CD").Value = String.Empty
            objSql.Parameters(":EXP_DT").Value = ""
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":FIN_CO").Value = cbo_finance_company_flx.SelectedValue
            objSql.Parameters(":FIN_PROMO_CD").Value = String.Empty
            objSql.Parameters(":APPROVAL_CD").Value = txt_fi_appr_flx.Text
            objSql.Parameters(":DL_STATE").Value = String.Empty
            objSql.Parameters(":DL_LICENSE_NO").Value = String.Empty

            objSql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        ASPxPopupControl8.ShowOnPageLoad = False
        lbl_pmt_tp.Text = ""
        Session("payment") = True
        lbl_pmt_amt.Text = ""
        calculate_total()
        ASPxPopupControl1.ShowOnPageLoad = True
        Response.Redirect("Payment.aspx")
        ASPxPopupControl8.ShowOnPageLoad = False

    End Sub


    'Alice added on Dec 12, 2018 for request 4394
    Protected Sub btn_save_china_Click(sender As Object, e As EventArgs) Handles btn_save_china.Click


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        'Validate Auth# number
        If txt_fi_appr_china.Text & "" = "" Then
            txt_fi_appr_china.Focus()
            lbl_fi_warning_china.Text = Resources.LibResources.Label836 '6 digit Auth# Required"
            Exit Sub
        Else
            If Len(txt_fi_appr_china.Text) <> 6 Then
                lbl_fi_warning_china.Text = Resources.LibResources.Label1002 'Auth# must be 6 Digits"
                txt_fi_appr_china.Focus()
                Exit Sub
            End If
        End If


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        cmdInsertItems.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = :PMT"
        cmdInsertItems.Parameters.Add(":PMT", OracleType.VarChar)
        cmdInsertItems.Parameters(":PMT").Value = lbl_pmt_tp_china.Text
        cmdInsertItems.Connection = conn
        sAdp = DisposablesManager.BuildOracleDataAdapter(cmdInsertItems)


        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        If Is_Finance_DP(lbl_pmt_tp_china.Text) = True Then
            bc = "FI"
        End If

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO,  FIN_PROMO_CD, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO,  :FIN_PROMO_CD, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            objSql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objSql.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp_china.Text
            objSql.Parameters(":SECURITY_CD").Value = String.Empty
            objSql.Parameters(":EXP_DT").Value = String.Empty
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":FIN_CO").Value = String.Empty
            objSql.Parameters(":FIN_PROMO_CD").Value = String.Empty
            objSql.Parameters(":APPROVAL_CD").Value = txt_fi_appr_china.Text
            objSql.Parameters(":DL_STATE").Value = String.Empty
            objSql.Parameters(":DL_LICENSE_NO").Value = String.Empty

            objSql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        ASPxPopupControl10.ShowOnPageLoad = False
        lbl_pmt_tp.Text = ""
        Session("payment") = True
        lbl_pmt_amt.Text = ""
        calculate_total()
        ASPxPopupControl1.ShowOnPageLoad = True
        Response.Redirect("Payment.aspx")
        ASPxPopupControl10.ShowOnPageLoad = False

    End Sub

    Protected Sub lucy_change_gridview1()
        ' lucy add  , this dropdownlist1 is defined at designer time
        Dim grid_find As GridView
        ' grid_find = Gridview1.FindControl("EditCommandColumn")
        Gridview1.Columns(5).HeaderText = ""
        Gridview1.Columns(6).HeaderText = ""



    End Sub

    Public Sub Add_Td_Company()
        'Daniela Td finance
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "select plan_cd mop_cd, des from brick_finance_plan order by des"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)


            If (MyDataReader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "mop_cd"
                cbo_finance_company.DataTextField = "des"
                cbo_finance_company.DataBind()
            End If

            'cbo_finance_company.Items.Insert(0, "Please select TD finance plan")
            'cbo_finance_company.Items.FindByText("Please select TD finance plan").Value = ""
            cbo_finance_company.SelectedIndex = 0

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub


    Public Sub Add_Finance_Company()
        'lucy add it was not at this version
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT AS_CD, NAME FROM ASP WHERE ACTIVE='Y' ORDER BY NAME"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)


            If (MyDataReader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "AS_CD"
                cbo_finance_company.DataTextField = "NAME"
                cbo_finance_company.DataBind()
            End If

            'cbo_finance_company.Items.Insert(0, "Please select finance company")
            'cbo_finance_company.Items.FindByText("Please select finance company").Value = ""
            cbo_finance_company.Items.Insert(0, Resources.LibResources.Label415)
            cbo_finance_company.Items.FindByText(Resources.LibResources.Label415).Value = ""

            cbo_finance_company.SelectedIndex = 0

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub lucy_change_DropDownList1()
        ' lucy add  , this dropdownlist1 is defined at designer time, need to test if thisw is needed
        If mop_drp.Value = "FI" Then
            Dim str_list As ListItem
            str_list = DropDownList1.Items.FindByValue("R")
            DropDownList1.Items.Remove(str_list)
            str_list = DropDownList1.Items.FindByValue("I")
            DropDownList1.Items.Remove(str_list)
        End If
        ' Dim str_sel As String = DropDownList1.SelectedItem.Text
        ' If str_sel <> "OPEN" Then


    End Sub

    Public Function lucy_decrypt(ByVal p_as_cd As String) As String
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim strAsCd As String = p_as_cd
        Dim strCustCd As String = Session("cust_cd")
        Dim strCardNum As String
        Dim strFiAcct As String



        Try

            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()
            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()


            myCMD.Connection = objConnection

            myCMD.CommandText = "std_dec_enc.decrypt_acct_cd"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add("strCardNum", OracleType.VarChar, 30).Direction = ParameterDirection.ReturnValue
            myCMD.Parameters.Add("p_as_cd", OracleType.VarChar).Direction = ParameterDirection.Input
            myCMD.Parameters.Add("p_cust_cd", OracleType.VarChar).Direction = ParameterDirection.Input
            myCMD.Parameters.Add(New OracleParameter("p_as_cd", OracleType.VarChar)).Value = strAsCd
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = strCustCd


            myCMD.ExecuteNonQuery()


            strFiAcct = myCMD.Parameters("strCardNum").Value

            objConnection.Close()

            Return strFiAcct
        Catch x

            strCardNum = x.Message.ToString
        End Try

    End Function

    Protected Sub print_receipt(ByVal p_prt_cmd As String)

        ' lblInfo.Text = "in testprint1 "

        Dim str_ip As String
        Dim str_user As String
        Dim str_pswd As String


        str_ip = get_database_ip()
        str_user = get_user()
        str_pswd = get_pswd()


        Dim syclient As New Renci.SshNet.SshClient(str_ip, str_user, str_pswd)
        Dim sycmd As Renci.SshNet.SshCommand
        Dim str_excep As String
        Dim str_print_cmd As String

        ' str_print_cmd = Session("str_print_cmd")
        '---------'connect to test
        Try
            syclient.Connect()

        Catch
            Throw

            str_excep = Err.Description
        End Try

        '  sycmd = syclient.RunCommand("touch /moved5/lucy/pr.lst")
        sycmd = syclient.RunCommand(p_prt_cmd)
        Try
            ' Catch syex3 As Exception
        Catch syex3 As SshException
            Throw
            str_excep = Err.Description
        End Try


        sycmd.Dispose()
        syclient.Disconnect()


    End Sub
    Public Function get_database_ip()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_ip As String


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()



        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshIP"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_ip", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue


        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            str_ip = myCMD.Parameters("p_ip").Value


        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_ip
    End Function



    Public Function get_user()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_user As String
        Dim p_user As String



        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()


        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshUsr"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_user", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try


            myCMD.ExecuteNonQuery()

            str_user = myCMD.Parameters("p_user").Value

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_user
    End Function
    Public Function get_pswd()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim p_pswd As String
        Dim str_pswd As String



        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()




        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshPwd"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_pswd", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue






        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

            str_pswd = myCMD.Parameters("p_pswd").Value

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_pswd
    End Function
    Public Function GetIPAddress() As String


        ' lucy created
        Dim strHostName As String

        Dim strIPAddress As String
        Dim str_term_id As String

        strHostName = System.Net.Dns.GetHostName()

        strIPAddress = System.Net.Dns.GetHostByName(strHostName).AddressList(0).ToString()



        Return strIPAddress

        ' MessageBox.Show("Host Name: " & strHostName & "; IP Address: " & strIPAddress)

    End Function


    Public Function MonthLastDay(ByVal dCurrDate As Date)
        'lucy
        MonthLastDay = DateSerial(Year(dCurrDate), Month(dCurrDate), 1).AddMonths(1).AddDays(-1)

    End Function

    Public Function td_send_out_fi_request() As String

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_term_id As String

        Dim v_trans_type As String
        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_auth_num As String
        Dim v_auth_no As String
        Dim v_mop_cd As String

        Dim v_private_lbl_finance_tp As String

        Dim l_filename As String
        Dim l_printcmd As String
        Dim strAsCd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim str_cust_cd As String = ""
        '  If Left(Session("lucy_as_cd"), 1) <> "E" And Left(Session("lucy_as_cd"), 1) <> "D" Then
        'strAsCd = "E12"
        ' Else
        strAsCd = Session("lucy_as_cd")
        ' End If
        ' Daniela Nov 19
        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else ' if not go global then default to remote ip
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            lbl_fi_warning.Text = "No designated pinpad"
            Return "empty"
        End If
        'v_term_id = get_term_id()

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Session("auth_no") = ""
        Session("lucy_approval_cd") = ""

        v_sub_type = ""

        Dim str_del_doc_num As String = Session("del_doc_num")
        Dim str_emp_cd As String = Session("emp_cd")
        If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
            str_del_doc_num = ""
        End If

        ' DB Oct 31, 2014 Add the customer before Invoice Print
        CreateCustomer()

        If IsDBNull(Session("cust_cd")) Or isEmpty(Session("cust_cd")) Then
            str_cust_cd = ""
        Else
            str_cust_cd = Session("cust_cd")
        End If
        Session("del_doc_num") = ""

        v_auth_no = "          "    '10 spaces

        v_mop_cd = "FI"

        v_trans_type = "12"

        v_amt = Session("lucy_amt")

        ' deferred payment 
        v_private_lbl_finance_tp = strAsCd
        If Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = True Then   'manualy enterred
            v_card_num = Trim(txt_fi_acct.Text)
            If isEmpty(txt_fi_exp_dt.Text) Then
                lbl_fi_warning.Text = "Please enter expiry date"
                Return "empty"
            End If
            v_expire_date = txt_fi_exp_dt.Text
            'Session("lucy_card_num") = v_card_num
            'Session("lucy_exp_dt") = v_expire_date
            str_s_or_m = "M"
        ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False And Len(Trim(Session("lucy_card_num"))) = 16 And IsNumeric(Session("lucy_card_num")) = True Then 'auto populated
            v_card_num = Session("lucy_card_num")
            v_card_num = ""   'force to swipe or need a exp_dt enterred
            v_expire_date = ""
            str_s_or_m = "S"
        Else    'swipe
            v_card_num = ""
            v_expire_date = ""
            str_s_or_m = "S"
        End If

        'Daniela prevent sending wrong exp_dt
        If isNotEmpty(v_expire_date) Then
            If Len((v_expire_date)) > 4 Then
                lbl_fi_warning.Text = "Please enter valid expiry date"
                Return "empty" 'Daniela 4 characters
            End If
        End If

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection

        Try

            ' Daniela TD call 
            myCMD.CommandText = "std_brk_pos1.sendsubTp8_td"

            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust_cd
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"
            End If

            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces  
            'v_trans_no = myCMD.Parameters("p_transTp").Value

            l_dsp = myCMD.Parameters("p_dsp").Value
            lbl_fi_warning.Text = l_dsp
            If l_aut <> "empty" Then   'transation done successfully
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value

                l_dsp = myCMD.Parameters("p_dsp").Value
                'If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Or str_s_or_m = "M" Then ' manual trans included
                    If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                        l_tr2 = myCMD.Parameters("p_tr2").Value
                    End If
                    If IsDBNull(myCMD.Parameters("p_exp").Value) = False Then
                        l_exp = myCMD.Parameters("p_exp").Value
                    End If
                    l_crn = myCMD.Parameters("p_crn").Value
                    Session("lucy_bnk_card") = Left(l_crn, 16)

                    Dim str_year As String
                    Dim str_month As String
                    If str_s_or_m = "M" Then
                        str_month = Left(l_exp, 2)
                        str_year = Right(l_exp, 2)
                    Else
                        str_year = Left(l_exp, 2)
                        str_month = Right(l_exp, 2)
                    End If
                    'Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/20" & str_year) ' Daniela fix 1946 issue
                    'Daniela prevent Invalid Date yellow traingle
                    Try
                        Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/20" & str_year) ' Daniela fix 1946 issue
                    Catch ex As Exception
                        Session("lucy_exp_dt") = MonthLastDay("12/1/2049")
                    End Try

                    If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                        txt_fi_acct.Text = Left(l_crn, 16)
                    ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False Then 'auto populated account#
                        txt_fi_acct.Text = Left(l_crn, 16)
                    End If
                Else
                    l_exp = myCMD.Parameters("p_exp").Value
                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/20" & str_year)
                    l_crn = myCMD.Parameters("p_crn").Value
                    Session("lucy_bnk_card") = Left(l_crn, 16) ' Daniela added for manual transactions
                End If

                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value
                Session("str_print_cmd") = l_printcmd
                Session("auth_no") = l_aut
                Session("lucy_approval_cd") = l_aut

                print_receipt(l_printcmd)

                txt_fi_appr.Text = l_aut

            Else   'transation not done 

                '   Session("auth_no") = l_aut
                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = ""
                Session("lucy_exp_dt") = "" ' Daniela clear exp date
                l_dsp = "empty"
                If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                    txt_fi_acct.Text = ""
                End If
            End If
            'Daniela Dec 16 save payment status for multiple payments
            If Not InStr(UCase(Session("cust_display")), "APPR") > 0 Then
                Session("cust_display") = Left(l_dsp, 20)
            End If
            Session("lucy_display") = Left(l_dsp, 20)
            objConnection.Close()

            Return l_dsp

        Catch ex As Exception
            Throw ex
        End Try

        Try
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        Catch
        End Try

    End Function


    Public Function send_out_fi_request() As String
        'Lucy created

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_crt As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_dis_line_1 As String
        Dim v_dis_line_2 As String
        Dim v_reserve As String
        Dim v_term_id As String

        Dim v_rpt_name As String
        Dim v_trans_type As String
        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_trans_no As String
        Dim v_auth_num As String
        Dim v_track2 As String
        Dim v_auth_no As String
        Dim v_oth As String
        Dim v_length As Integer
        Dim v_response As String
        Dim v_mop_cd As String

        Dim v_private_lbl_finance_tp As String
        Dim v_private_lbl_plan_num As String
        Dim v_count As Integer

        Dim l_filename As String
        Dim l_printcmd As String
        Dim strAsCd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim str_cust_cd As String = ""
        '  If Left(Session("lucy_as_cd"), 1) <> "E" And Left(Session("lucy_as_cd"), 1) <> "D" Then
        'strAsCd = "E12"
        ' Else
        strAsCd = Session("lucy_as_cd")
        ' End If
        ' Daniela Nov 19
        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else ' if not go global then default to remote ip
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            Return "No designated pinpad"
        End If
        'v_term_id = get_term_id()

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Session("auth_no") = ""
        Session("lucy_approval_cd") = ""

        v_sub_type = ""

        Dim str_del_doc_num As String = Session("del_doc_num")
        Dim str_emp_cd As String = Session("emp_cd")
        If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
            str_del_doc_num = ""
        End If

        ' DB Oct 31, 2014 Add the customer before Invoice Print
        CreateCustomer()

        If IsDBNull(Session("cust_cd")) Or isEmpty(Session("cust_cd")) Then
            str_cust_cd = ""
        Else
            str_cust_cd = Session("cust_cd")
        End If
        Session("del_doc_num") = ""

        '  v_rpt_name ="IPRT"


        ' If TextBox1.Text & "" = "" And Len(TextBox1.Text) = 16 Then
        'v_card_num = TextBox1.Text
        ' End If

        v_auth_no = "          "    '10 spaces

        v_mop_cd = "FI"

        v_trans_type = "12"


        v_amt = Session("lucy_amt")


        ' deferred payment 
        v_private_lbl_finance_tp = strAsCd
        '   v_private_lbl_finance_tp = "D"
        ' v_private_lbl_plan_num = "081"

        '  if l_data_rec.private_lbl.finance_tp ='D' then
        '  l_data_rec.private_lbl.grc_prd :=99;
        '  l_data_rec.private_lbl.noPayinMth :='00';
        '  equal payment 
        '  elsif	l_data_rec.private_lbl.finance_tp ='E' then 
        ' l_data_rec.private_lbl.noPayinMth :=99;
        ' l_data_rec.private_lbl.grc_prd :='00';
        ' end if;
        If Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = True Then   'manualy enterred
            v_card_num = Trim(txt_fi_acct.Text)
            v_expire_date = txt_fi_exp_dt.Text
            Session("lucy_bnk_card") = v_card_num
            Session("lucy_exp_dt") = v_expire_date
            str_s_or_m = "M"
        ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False And Len(Trim(Session("lucy_card_num"))) = 16 And IsNumeric(Session("lucy_card_num")) = True Then 'auto populated
            v_card_num = Session("lucy_card_num")
            ' v_expire_date = txt_fi_exp_dt.Text
            v_card_num = ""   'force to swipe or need a exp_dt enterred
            v_expire_date = ""
            str_s_or_m = "S"
        Else    'swipe
            v_card_num = ""
            v_expire_date = ""
            str_s_or_m = "S"
        End If

        Try

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection

            myCMD.CommandText = "std_tenderretail_pos.sendsubtp8"

            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust_cd
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num
            'select lpad(glpmt_seq.nextval,12,'0') into v_trans_no from dual;
            ' peter will get v_trans_no in his procedure which is a sequence#
            ' myCMD.Parameters.Add(New OracleParameter("p_transTp", OracleType.VarChar)).Value = v_trans_type
            ' myCMD.Parameters.Add(New OracleParameter("p_authNo", OracleType.VarChar)).Value = v_auth_no

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output


            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            ' myCMD.Parameters.Add("p_rct", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
            'sabrina changed 5 to 10
            'myCMD.Parameters.Add("p_res", OracleType.VarChar, 5).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"
            End If

            ' If l_aut <> "empty" Then ' peter put empty means l_aut has null value
            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces  
            'v_trans_no = myCMD.Parameters("p_transTp").Value

            l_dsp = myCMD.Parameters("p_dsp").Value
            lbl_fi_warning.Text = l_dsp
            If l_aut <> "empty" Then   'transation done successfully
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value

                l_dsp = myCMD.Parameters("p_dsp").Value
                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                    l_exp = myCMD.Parameters("p_exp").Value
                    l_crn = myCMD.Parameters("p_crn").Value
                    Session("lucy_bnk_card") = Left(l_crn, 16)
                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)
                    If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                        txt_fi_acct.Text = Left(l_crn, 16)
                    ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False Then 'auto populated account#
                        txt_fi_acct.Text = Left(l_crn, 16)
                    End If
                Else
                    l_exp = myCMD.Parameters("p_exp").Value
                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)

                End If

                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value
                Session("str_print_cmd") = l_printcmd
                Session("auth_no") = l_aut
                Session("lucy_approval_cd") = l_aut

                print_receipt(l_printcmd)


                txt_fi_appr.Text = l_aut

            Else   'transation not done 

                '   Session("auth_no") = l_aut
                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = ""
                l_dsp = "empty"
                If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                    txt_fi_acct.Text = ""
                End If
            End If
            'Daniela Dec 16 save payment status for multiple payments
            If Not InStr(UCase(Session("cust_display")), "APPR") > 0 Then
                Session("cust_display") = Left(l_dsp, 20)
            End If
            Session("lucy_display") = Left(l_dsp, 20)

            myCMD.Cancel()
            myCMD.Dispose()

            Return l_dsp

        Catch x
            Throw
            ' lbl_Lucy_Test.Text = x.Message.ToString
        Finally

            objConnection.Close() ' DANIELA
            objConnection.Dispose()
        End Try

    End Function
    Protected Sub lucy_btn_save_fi_co_Click()

        'IT Req 3354
        txt_fi_appr.Text = ""
        Dim strfinCo = cbo_finance_company.SelectedValue
        Session("lucy_as_cd") = strfinCo
        Dim merchant_id As String = "" 'lucy add

        Dim lucy_fi_co As String
        ' lucy_fi_co = txt_fi_acct.Text  'lucy add

        ASPxPopupControl1.ShowOnPageLoad = True
        ASPxPopupControl2.ShowOnPageLoad = False   'does not work
        btn_save_fi_co.Enabled = False
        btn_save_fi_co.Visible = False
        Dim str_display As String  'lucy add to call tenderretail
        ' Daniela 
        If Session("emp_cd") & "" = "" Then
            ' Better to prevent payment in emp_cd is null
            Response.Redirect("login.aspx")
        End If
        If Session("lucy_mop_cd") = "TDF" Then
            str_display = td_send_out_fi_request()
        ElseIf Session("lucy_mop_cd") = "DCS" Then
            str_display = sy_brk_process_visad()          'R1999 sabrina        
        Else
            str_display = send_out_fi_request()
        End If

        If str_display = "no designated pinpad " Or str_display = "No designated pinpad" Then
            ASPxPopupControl1.ShowOnPageLoad = False
            lucy_lbl_aprv.Text = "no designated pinpad "
            ' txt_fi_acct.Text = ""
            Exit Sub
        End If
        '  If InStr(str_display, "APPR") = 0 Then  'lucy add, not approved
        If Left(str_display, 5) = "empty" Then
            ' Response.Redirect("payment.aspx")      'lucy add
            ASPxPopupControl1.ShowOnPageLoad = False
            ASPxPopupControl2.ShowOnPageLoad = True
            btn_save_fi_co.Enabled = True
            btn_save_fi_co.Visible = True
            ASPxLabel1.Visible = False
            ' lucy_lbl_pay.Text = "Please enter the card information and then click Save"
            Exit Sub
        End If    'lucy add

        ASPxPopupControl1.ShowOnPageLoad = True

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        ' conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        cmdInsertItems.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = :PMT"
        cmdInsertItems.Parameters.Add(":PMT", OracleType.VarChar)
        cmdInsertItems.Parameters(":PMT").Value = lbl_pmt_tp.Text
        cmdInsertItems.Connection = conn
        sAdp = DisposablesManager.BuildOracleDataAdapter(cmdInsertItems)


        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        'If lbl_pmt_tp.Text = "WDR" Then
        If Is_Finance_DP(lbl_pmt_tp.Text) = True Then
            bc = "FI"
        End If
        txt_fi_exp_dt.Text = Session("lucy_exp_dt") 'lucy
        Dim str_fi_acct_num As String = txt_fi_acct.Text 'lucy
        Dim str_enc_acct_num = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")  'lucy

        '  conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            ' Daniela add bnk card
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, BC, MOP_TP, FIN_CO, FI_ACCT_NUM, FIN_PROMO_CD, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :BC, :MOP_TP, :FIN_CO, :FI_ACCT_NUM, :FIN_PROMO_CD, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            objSql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":BC", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objSql.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp.Text
            objSql.Parameters(":SECURITY_CD").Value = txt_fi_sec_cd.Text
            objSql.Parameters(":EXP_DT").Value = txt_fi_exp_dt.Text
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":BC").Value = Session("lucy_bnk_card")
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":FIN_CO").Value = cbo_finance_company.SelectedValue
            objSql.Parameters(":FI_ACCT_NUM").Value = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")
            objSql.Parameters(":FIN_PROMO_CD").Value = cbo_promo.SelectedValue
            objSql.Parameters(":APPROVAL_CD").Value = txt_fi_appr.Text
            objSql.Parameters(":DL_STATE").Value = txt_fi_dl_st.Text
            objSql.Parameters(":DL_LICENSE_NO").Value = txt_fi_dl_no.Text

            objSql.ExecuteNonQuery()
            objSql.Cancel()
            objSql.Dispose()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        ASPxPopupControl2.ShowOnPageLoad = False
        lbl_pmt_tp.Text = ""
        Session("payment") = True
        lbl_pmt_amt.Text = ""
        calculate_total()
        ASPxPopupControl1.ShowOnPageLoad = True
        Response.Redirect("Payment.aspx")
        ASPxPopupControl2.ShowOnPageLoad = False

    End Sub


    ' start lucy add, Req-214, Nov, 2019
    Public Sub Save_spc_record()

        Dim str_spc_num As String
        If txt_spc_num.Text.Length = 16 Then


            Session("USR_FLD_5") = "SPC" + txt_spc_num.Text

            str_spc_num = Session("USR_FLD_5")

        End If

    End Sub



     


    Public Sub btn_spc_save_click(sender As Object, e As EventArgs)


        If txt_spc_num.Text.Length = 16 Then

            Save_spc_record()

            lbl_msg.Text = Resources.LibResources.Label1203

        Else
            lbl_msg.Text = Resources.LibResources.Label1205
        End If
    End Sub


    Protected Sub btn_spc_exit_Click(sender As Object, e As EventArgs)
        txt_spc_num.Text = Nothing
        lbl_msg.Text = Nothing
        ASPxPopupControl_spc_num.ShowOnPageLoad = False

    End Sub

    Protected Sub btn_spc_clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_spc_clear.Click

        txt_spc_num.Text = Nothing
        lbl_msg.Text = Nothing
    End Sub

    Protected Sub btn_add_spc_Click(sender As Object, e As EventArgs) Handles btn_add_spc.Click

        Dim strco_cd As String


        strco_cd = Session("CO_CD")

        If strco_cd <> "LFL" Then
            ASPxPopupControl_spc_num.ShowOnPageLoad = True
        End If

    End Sub

    ' end lucy add


    Protected Sub MyDataGrid_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles MyDataGrid.ItemCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim thistransaction As OracleTransaction
        Dim NEW_EXP_DT As String = ""
        Dim txt_amt_transfer As TextBox
        txt_amt_transfer = CType(e.Item.FindControl("txt_amt_transfer"), TextBox)

        If Not IsNumeric(txt_amt_transfer.Text) Then
            lbl_card_on_file.Text = "Amount must be numeric to proceed"
            Exit Sub
        ElseIf CDbl(txt_amt_transfer.Text) <= 0 Then
            lbl_card_on_file.Text = Resources.LibResources.Label802
            Exit Sub
        End If

        objConnection.Open()
        thistransaction = objConnection.BeginTransaction
        With cmdInsertItems
            .Transaction = thistransaction
            .Connection = objConnection
            'jkl
            .CommandText = "insert into payment (sessionid, MOP_CD, AMT, DES, BC, EXP_DT, MOP_TP) values (:SESSIONID,:MOP_CD, :AMT,:DES,:CCDATA,:NEW_EXP_DT,'BC')"
            .Parameters.Add(":SESSIONID", OracleType.VarChar)
            .Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            .Parameters.Add(":MOP_CD", OracleType.VarChar)
            .Parameters(":MOP_CD").Value = Request("MOP_CD")
            .Parameters.Add(":AMT", OracleType.VarChar)
            .Parameters(":AMT").Value = Request("AMT")
            .Parameters.Add(":DES", OracleType.VarChar)
            .Parameters(":DES").Value = Request("DES")
            .Parameters.Add(":CCDATA", OracleType.VarChar)
            .Parameters(":CCDATA").Value = Encrypt(e.Item.Cells(2).Text, "CrOcOdIlE")
            .Parameters.Add(":NEW_EXP_DT", OracleType.VarChar)
            .Parameters(":NEW_EXP_DT").Value = e.Item.Cells(1).Text

        End With
        cmdInsertItems.ExecuteNonQuery()
        thistransaction.Commit()
        objConnection.Close()
        ASPxPopupControl4.ShowOnPageLoad = False
        Session("payment") = True
        bindgrid()
        calculate_total()

    End Sub


    Public Sub Verify_Take_With()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand 'jkl added NEW
        Dim MyDataReader As OracleDataReader
        Dim Total_Count As Double
        Dim Take_Count As Double

        conn.Open()

        'sql = "SELECT count(ROW_ID) As TOTAL_COUNT, SUM(QTY*RET_PRC) As T_SUM FROM TEMP_ITM where TAKE_WITH='Y' AND session_id='" & Session.SessionID.ToString.Trim & "'"

        'jkl
        objSql.CommandText = "SELECT count(ROW_ID) As TOTAL_COUNT, SUM(QTY*RET_PRC) As T_SUM FROM TEMP_ITM where TAKE_WITH='Y' AND session_id=:SESSIONID"
        objSql.Parameters.Add(":SESSIONID", OracleType.VarChar)
        objSql.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
        objSql.Connection = conn

        'Set SQL OBJECT 
        'objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                If IsNumeric(MyDataReader.Item("TOTAL_COUNT").ToString) Then
                    Take_Count = CDbl(MyDataReader.Item("TOTAL_COUNT").ToString)
                End If
                If IsNumeric(MyDataReader.Item("T_SUM").ToString) Then
                    Session("Take_Deposit") = CDbl(MyDataReader.Item("T_SUM").ToString)
                End If
            End If

            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Take_Count > 0 Then
            objSql.CommandText = "SELECT count(ROW_ID) As TOTAL_COUNT FROM TEMP_ITM where session_id=:SESSIONID"
            objSql.Parameters.Add(":SESSIONID", OracleType.VarChar)
            objSql.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            objSql.Connection = conn

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (MyDataReader.Read()) Then
                    If IsNumeric(MyDataReader.Item("TOTAL_COUNT").ToString) Then
                        Total_Count = CDbl(MyDataReader.Item("TOTAL_COUNT").ToString)
                    End If
                End If

                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            If Total_Count - Take_Count = 0 Then
                If Session("PD") = "D" Then
                    lbl_Error.Text = "All items on the sale are Take-With.  Your order was changed to a pickup, and the delivery charges were removed." & vbCrLf
                End If
                Session("PD") = "P"
                Session("DEL_CHG") = "0"
                lbl_Error.Visible = True
                If Left(lbl_Error.Text, 3) = "All" Then
                    lbl_Error.Text = lbl_Error.Text & "The minimum required deposit on this order is " & FormatCurrency(CDbl(Session("Grand_Total")), 2)
                Else
                    lbl_Error.Text = "The minimum required deposit on this order is " & FormatCurrency(CDbl(Session("Grand_Total")), 2)
                End If
                TextBox1.Text = FormatNumber(CDbl(Session("Grand_Total")), 2)

                'jkl
                'sql = "UPDATE TEMP_ITM SET TAKE_WITH='N' where session_id='" & Session.SessionID.ToString.Trim & "'"

                'Set SQL OBJECT 
                'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql.Parameters.Clear()
                objSql.CommandText = "UPDATE TEMP_ITM SET TAKE_WITH='N' where session_id=:SESSIONID"
                objSql.Parameters.Add(":SESSIONID", OracleType.VarChar)
                objSql.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
                objSql.Connection = conn


                objSql.ExecuteNonQuery()
            End If
        End If
        conn.Close()

    End Sub



    Public Sub bindgrid()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand 'jkl added new
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim SoLnGItm As DataGridItem 'lucy
        ds = New DataSet

        Gridview1.DataSource = ""

        Gridview1.Visible = True

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        'objSql.CommandText = "SELECT * FROM payment where sessionid= :SESSIONID order by row_id" ' Daniela comment
        'sabrina R1999 check for DCS
        objSql.CommandText = "SELECT ROW_ID,SESSIONID,MOP_CD,AMT,decode(mop_cd, 'TDF', DES || ' - ' || FIN_CO,'DCS',DES||'-'||FIN_CO, DES) DES,BC,EXP_DT,MOP_TP,FIN_CO,FIN_PROMO_CD,FI_ACCT_NUM,APPROVAL_CD,CHK_NUM,CHK_TP,TRANS_ROUTE,CHK_ACCOUNT_NO," +
            " SECURITY_CD, HOST_RESP_MSG, HOST_RESPONSE_CD, PB_RESPONSE_MSG, DL_STATE, DL_LICENSE_NO FROM payment where sessionid= :SESSIONID order by row_id"
        objSql.Parameters.Add(":SESSIONID", OracleType.VarChar)
        objSql.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()

                ' start lucy
                Dim i As Integer
                Dim str_app_cd As String = Session("lucy_approval_cd")
                For i = 0 To numrows - 1

                    SoLnGItm = Gridview1.Items(i)

                    Dim str_mop As String
                    Dim str_mop_tp As String
                    str_mop = Left(SoLnGItm.Cells(1).Text, 6)


                    str_mop_tp = SoLnGItm.Cells(7).Text


                    'sabrina add GC
                    If str_mop_tp <> "BC" And str_mop_tp <> "FI" And str_mop_tp <> "GC" Then   ' not credit card

                        '  If InStr(str_mop, "VISA") = 0 And InStr(str_mop, "VISA") = 0 And InStr(str_mop, "FINA") = 0 And InStr(str_mop, "DEBI") = 0 Or (InStr(str_mop, "FINA") > 0 And Len(str_app_cd) < 6) Then

                        ' If str_mop <> "VISA" And str_mop <> "FINA" And str_mop <> "MAST" And str_mop <> "DEBI" Or (str_mop = "FINA" And Len(str_app_cd) < 5) Then
                        SoLnGItm.Cells(4).Text = "NO"
                        SoLnGItm.Cells(4).ForeColor = Color.Gray
                        Session("lucy_approval_cd") = ""
                    Else
                        SoLnGItm.Cells(4).Text = "YES"
                        SoLnGItm.Cells(4).ForeColor = Color.Green
                        Session("lucy_approval_cd") = ""
                    End If
                Next
                ' end lucy

            Else
                Gridview1.Visible = False
                Session("payment") = System.DBNull.Value
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Public Sub mop_drp_Populate()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim mop_found As Boolean = False
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        ' Daniela add multi company logic
        Dim emp_init As String
        If Session("emp_cd") & "" = "" Then
            Response.Redirect("login.aspx")
        End If
        If isEmpty(Session("emp_init")) Then
            emp_init = LeonsBiz.GetEmpInit(Session("emp_cd"))
            Session("emp_init") = emp_init
        Else : emp_init = Session("emp_init")
        End If

        Dim MyDataReader As OracleDataReader

        'sabrina tdpl
        If Session("tdpl") = "Y" Then
            sql = "SELECT MOP_CD,DES || '     ' || MOP_TP AS DES FROM MOP WHERE MOP_CD IN ('CS','DC')"
        Else
            ' Nov 4 improve performance
            sql = "SELECT MOP_CD,DES || '     ' || DECODE(MOP_TP,'CK','(CHECK)','BC','(BANKCARD)','TC','(TRANSFER CREDIT)','IN','(FINANCE INSTALLMENT)','RV','(REVOLVING)','OP','(OPEN A/R)','LW','(LAYAWAY)','FI','(FINANCE)','') AS DES FROM SETTLEMENT_MOP a WHERE MOP_ACTIVE='Y'"
            'sql = sql & " and Std_multi_co.isValidMop('" & emp_init & "', mop_cd) = 'Y' "
            sql = sql & " and exists ("
            sql = sql & "       Select 1"
            sql = sql & "       from   co_grp$mop"
            sql = sql & "       where  ("
            sql = sql & "              co_grp_cd  =  (select CO_GRP_CD from co_grp where co_cd = nvl(upper('" & Session("CO_CD") & "'),co_cd))"
            sql = sql & "              or"
            sql = sql & "              co_grp_cd  = 'ALL'"
            sql = sql & "              )"
            sql = sql & "       and    mop_cd     =  a.mop_cd"
            sql = sql & "       ) "
        End If
        'sql = "SELECT MOP_CD,DES || '     ' || DECODE(MOP_TP,'CK','(CHECK)','BC','(BANKCARD)','TC','(TRANSFER CREDIT)','IN','(FINANCE INSTALLMENT)','RV','(REVOLVING)','OP','(OPEN A/R)','LW','(LAYAWAY)','FI','(FINANCE)','') AS DES FROM SETTLEMENT_MOP WHERE MOP_ACTIVE='Y'"
        'Sep 30 as per Dave
        'If Session("cash_carry") = "TRUE" Then
        '    sql = sql & " AND NVL(MOP_TP,'B') <> 'FI'"
        'End If
        sql = sql & " ORDER BY DES"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mop_drp.Items.Clear()

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                mop_found = True
                mop_drp.DataSource = ds
                mop_drp.ValueField = "MOP_CD"
                mop_drp.TextField = "DES"
                mop_drp.DataBind()
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()

            If mop_found = False Then
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                'Nov 4 improve performance
                sql = "SELECT MOP_CD,DES || '     ' || DECODE(MOP_TP,'CK','(CHECK)','BC','(BANKCARD)','TC','(TRANSFER CREDIT)','IN','(FINANCE INSTALLMENT)','RV','(REVOLVING)','OP','(OPEN A/R)','LW','(LAYAWAY)','FI','(FINANCE)','') AS DES FROM MOP a "
                'sql = sql & "WHERE Std_multi_co.isValidMop('" & emp_init & "', mop_cd) = 'Y' "
                sql = sql & " WHERE exists ("
                sql = sql & "       Select 1"
                sql = sql & "       from   co_grp$mop"
                sql = sql & "       where  ("
                sql = sql & "              co_grp_cd  =  (select CO_GRP_CD from co_grp where co_cd = nvl(upper('" & Session("CO_CD") & "'),co_cd))"
                sql = sql & "              or"
                sql = sql & "              co_grp_cd  = 'ALL'"
                sql = sql & "              )"
                sql = sql & "       and    mop_cd     =  a.mop_cd"
                sql = sql & "       ) "
                sql = sql & "and (MOP_TP != 'CD' or MOP_TP IS NULL) "
                'Sep 30 as per Dave
                'If Session("cash_carry") = "TRUE" Then
                'sql = sql & " AND MOP_TP <> 'FI'"
                'End If
                sql = sql & " ORDER BY DES"

                conn.Open()
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
                oAdp.Fill(ds)

                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (MyDataReader.Read()) Then
                    mop_found = True
                    mop_drp.DataSource = ds
                    mop_drp.ValueField = "MOP_CD"
                    mop_drp.TextField = "DES"
                    mop_drp.DataBind()
                End If

                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            End If

            ' put the default first item
            'Daniela french
            mop_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label698, ""))
            mop_drp.Items.FindByText(Resources.LibResources.Label698).Value = ""
            mop_drp.SelectedIndex = 0

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add.Click
        lbl_Error.Visible = True
        lbl_Error.Text = ""

        lbl_fi_warning.Text = "" 'lucy add 4 lines
        btn_save_fi_co.Visible = False
        lucy_btn_new_cust.Visible = False
        lucy_lbl_aprv.Text = ""
        TextBox1.Text = Replace(TextBox1.Text, "$", "")

        If mop_drp.SelectedIndex = 0 Then
            lbl_Error.Text = Resources.LibResources.Label803
        Else
            Dim pmtAmtStr As String = TextBox1.Text.ToString()
            Dim pmtAmt As Double = 0
            If (Not String.IsNullOrEmpty(pmtAmtStr)) Then
                pmtAmt = CDbl(pmtAmtStr)
            End If

            If Not (pmtAmt > 0) Then
                lbl_Error.Text = "Payment must be a number greater than zero"
            Else
                Session("lucy_amt") = pmtAmt 'lucy
                Session("lucy_mop_cd") = mop_drp.Value 'lucy
                If mop_drp.Value = "FI" Then
                    lucy_change_DropDownList1() 'lucy
                End If
                If mop_drp.Value = PaymentUtils.Bank_MOP_CD.TDFinancial Or mop_drp.Value = PaymentUtils.Bank_MOP_CD.DESJARDINS Or mop_drp.Value = PaymentUtils.Bank_MOP_CD.FLEXITI Then 'SABRINA r1999  'Alice added FLX on Oct 3, 2018 
                    lucy_change_DropDownList1() 'daniela
                End If
                lbl_pmt_amt.Text = pmtAmtStr
                If (InsertRecord()) Then
                    Session("payment") = True
                    TextBox1.Text = 0.0
                    mop_drp.SelectedIndex = 0
                    bindgrid()
                    calculate_total()

                    ' Reset cashbreakdown section values
                    reset_cash_breakdown()
                End If
            End If
        End If
    End Sub

    Public Sub calculate_total()

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand 'jkl added
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim total As Double
        Dim Take_deposit As Double = 0

        If Session("tet_cd") & "" = "" Then
            Take_deposit = Calculate_Take_With()
        Else
            Take_deposit = CDbl(Session("Take_Deposit"))
        End If

        conn.Open()

        Dim Session_String As String

        Session_String = Session.SessionID.ToString()

        objSql.CommandText = "select sum(AMT) As Total_Order from Payment where (MOP_TP != 'CD' or MOP_TP IS NULL) and sessionid=:SESSIONID"
        objSql.Parameters.Add(":SESSIONID", OracleType.VarChar)
        objSql.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
        objSql.Connection = conn

        sAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count
        total = 0

        Try
            If MyTable.Rows(0).Item("Total_Order").ToString & "" <> "" Then
                total = total + CDbl(MyTable.Rows(0).Item("Total_Order").ToString)
            End If
            Dim footer As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("ASPxRoundPanel1").FindControl("lbl_Payments")
            'Dim footer As Label = Master.FindControl("lbl_Payments")
            If Not footer Is Nothing Then
                footer.Text = FormatCurrency(CDbl(total))
            End If

            Session("payments") = CDbl(total)
            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Dim lblTotal As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("ASPxRoundPanel1").FindControl("lblTotal")
        'Dim lblTotal As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("lblTotal")
        Dim lbl_balance As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("ASPxRoundPanel1").FindControl("lbl_balance")
        'Dim lbl_balance As DevExpress.Web.ASPxEditors.ASPxLabel = Master.FindControl("lbl_balance")

        If Not IsNothing(lblTotal.Text) And Not IsNothing(lbl_balance.Text) Then
            If IsNumeric(lblTotal.Text) And IsNumeric(lbl_balance.Text) Then
                lbl_balance.Text = FormatCurrency(CDbl(lblTotal.Text) - CDbl(total), 2)
            End If
        End If

        If total = 0 Then
            Session("payment") = System.DBNull.Value
        End If
        If FormatNumber((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * CDbl(Session("Grand_Total")), 2) <= Session("payments") Then
            'lbl_Error.Text = ""
        Else
            If CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) > 0 Then
                If ConfigurationManager.AppSettings("dep_req").ToString = "REQ" Then
                    lbl_Error.Text = "The minimum required deposit on this order is " & FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                    TextBox1.Text = FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                Else
                    lbl_Error.Text = Resources.LibResources.Label600 & " " & FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                    TextBox1.Text = FormatCurrency(((CDbl(ConfigurationManager.AppSettings("min_deposit").ToString) / 100) * (CDbl(Session("Grand_Total")) - Take_deposit)) + Take_deposit, 2)
                End If

            End If
        End If

        lbl_cod.Text = FormatCurrency(CDbl(Session("Grand_Total")) - total, 2)
        If CDbl(lbl_cod.Text) = 0 Then
            lbl_Error.Text = ""
            TextBox1.Text = 0
        End If

        If IsNumeric(TextBox1.Text) And IsNumeric(lbl_cod.Text) Then
            If CDbl(TextBox1.Text) > CDbl(lbl_cod.Text) Then
                TextBox1.Text = lbl_cod.Text
            End If
        End If

        If Session("cash_carry") = "TRUE" And CDbl(lbl_cod.Text) <> 0 Then
            lbl_Error.Text = Resources.LibResources.Label59
            TextBox1.Text = lbl_cod.Text
        End If

    End Sub

    Private Function Retrieve_Finance_Account(ByVal cust_cd As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select rtrim(substr(text,26,16)) as ACCT_NO "
        sql = sql & "FROM CUST_CMNT WHERE CUST_CD='" & cust_cd & "' "
        sql = sql & "AND EMP_CD_OP='KIOSK' "
        sql = sql & "AND TEXT LIKE 'Approved Account Number:%'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                If IsNumeric(MyDatareader2.Item("ACCT_NO").ToString) And Len(MyDatareader2.Item("ACCT_NO").ToString) = 16 Then
                    Return MyDatareader2.Item("ACCT_NO").ToString
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Public Sub delete_records()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        If Not IsNothing(Session.SessionID.ToString.Trim) Then
            With cmdDeleteItems
                .Connection = conn
                'jkl
                '.CommandText = "delete from payment where sessionid='" & Session.SessionID.ToString.Trim & "'"
                .CommandText = "delete from payment where sessionid=:SESSIONID"
                .Parameters.Add(":SESSIONID", OracleType.VarChar)
                .Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
            End With
            cmdDeleteItems.ExecuteNonQuery()
        End If
        conn.Close()
        calculate_total()

    End Sub

    Public Function InsertRecord() As Boolean
        lbl_Error.Text = ""

        Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim oraTransaction As OracleTransaction
        Dim oraCmd As OracleCommand
        Dim oraAdp As OracleDataAdapter
        Dim dTable As DataTable
        Dim dSet As DataSet
        Dim depFlag As String = ""
        Dim desc As String = ""
        Dim mopTp As String = ""
        Dim isFinanceDeposit As Boolean
        isFinanceDeposit = Is_Finance_DP(mop_drp.Value)

        connErp.Open()
        connLocal.Open()

        oraCmd = DisposablesManager.BuildOracleCommand

        oraCmd.Connection = connErp
        oraCmd.CommandText = "SELECT DES, MOP_TP, DEPOSIT_FLAG FROM MOP WHERE MOP_CD = :MOP"
        oraCmd.Parameters.Add(":MOP", OracleType.VarChar)
        oraCmd.Parameters(":MOP").Value = mop_drp.Value

        dSet = New DataSet
        oraAdp = DisposablesManager.BuildOracleDataAdapter(oraCmd)
        oraAdp.Fill(dSet)
        dTable = New DataTable
        dTable = dSet.Tables(0)
        desc = dTable.Rows(0).Item("DES").ToString
        depFlag = dTable.Rows(0).Item("DEPOSIT_FLAG").ToString
        mopTp = dTable.Rows(0).Item("MOP_TP").ToString
        If (isFinanceDeposit) Then mopTp = "FI"
        ' Daniela
        If mop_drp.Value = "TDF" Then
            mopTp = "TD" ' added to procees TD finance
        End If

        'multiple finance deposits ARE allowed per transaction, so skip this check
        'however if this was a finance, only ONE finance is allowed per transaction
        'therefore this check will stop the user if a second finance is added
        If (Not isFinanceDeposit AndAlso "FI" = mopTp) Then
            If (thePaymentBiz.HasFinancePayment(Session.SessionID.ToString)) Then
                lbl_Error.Visible = True
                lbl_Error.Text = "Only one finance payment allowed per order."
                Return False
            End If
        End If

        'sabrina R1930
        If Session("emp_init") & "" = "" Then
            Session("emp_init") = LeonsBiz.GetEmpInit(Session("emp_cd"))
        End If

        'sabrina R1999
        lucy_btn_new_cust.Visible = False
        txt_fi_appr.Visible = False
        Literal1.Visible = False
        txt_fi_acct.Enabled = True
        txt_fi_exp_dt.Enabled = True
        'sabrina R1999 end

        If mopTp = "BC" Then
            If mop_drp.Value = PaymentUtils.Bank_MOP_CD.DESJARDINS Then
                Literal1.Visible = True
                txt_fi_acct.Text = ""
                txt_fi_appr.Text = ""
                btn_save_fi_co.Enabled = True
                btn_save_fi_co.Visible = True
                lucy_btn_new_cust.Visible = True
                txt_fi_appr.Visible = True
                sy_addDCScompany()
                If cbo_finance_company.SelectedValue() & "" = "" Then
                    cbo_finance_company.Focus()
                    lbl_fi_warning.Text = "Select a Plan"
                    Exit Function
                End If
                lbl_pmt_tp.Text = mop_drp.Value
                ASPxPopupControl2.ShowOnPageLoad = True

            ElseIf mop_drp.Value = PaymentUtils.Bank_MOP_CD.FLEXITI Then 'Alice added on Oct 05, 2018 for request 4115
                Literal2.Visible = True
                txt_fi_appr_flx.Text = ""
                btn_save_flx.Visible = True
                txt_fi_appr_flx.Visible = True
                sy_addFLXcompany()
                lbl_pmt_tp_flx.Text = mop_drp.Value
                ASPxPopupControl8.ShowOnPageLoad = True

            ElseIf mop_drp.Value = PaymentUtils.Bank_MOP_CD.MCFinancial Then 'Alice added on May 28, 2019 for request 235
                lit_fm.Visible = True
                txt_fm_acct.Text = ""
                txt_fm_appr.Text = ""
                btn_save_fm_co.Enabled = True
                btn_save_fm_co.Visible = True
                txt_fm_appr.Visible = True
                sy_addFMcompany()
                If cbo_finance_company_fm.SelectedValue() & "" = "" Then
                    cbo_finance_company_fm.Focus()
                    lbl_fm_warning.Text = "Select a Plan"
                    Exit Function
                End If

                lbl_pmt_tp_fm.Text = mop_drp.Value
                ASPxPopupControl11.ShowOnPageLoad = True


                'Alice added on DEC 12, 2018 for request 4394
            ElseIf mop_drp.Value = PaymentUtils.Bank_MOP_CD.ALI Or mop_drp.Value = PaymentUtils.Bank_MOP_CD.UNION Or mop_drp.Value = PaymentUtils.Bank_MOP_CD.WECHAT Then
                Literal3.Visible = True
                txt_fi_appr_china.Text = ""
                btn_save_china.Visible = True
                txt_fi_appr_china.Visible = True
                lbl_pmt_tp_china.Text = mop_drp.Value
                ASPxPopupControl10.ShowOnPageLoad = True
                Select Case mop_drp.Value
                    Case PaymentUtils.Bank_MOP_CD.ALI
                        ASPxPopupControl10.HeaderText = Resources.LibResources.Label1007
                    Case PaymentUtils.Bank_MOP_CD.WECHAT
                        ASPxPopupControl10.HeaderText = Resources.LibResources.Label1008
                    Case PaymentUtils.Bank_MOP_CD.UNION
                        ASPxPopupControl10.HeaderText = Resources.LibResources.Label1009
                End Select

            Else
                'jkl just wondering about this URL being vulnerable
                ASPxPopupControl1.ContentUrl = "credit_card_processing.aspx?AMT=" & CDbl(TextBox1.Text) & "&MOP_CD=" & mop_drp.Value & "&DES=" & desc
                ASPxPopupControl1.ShowOnPageLoad = True

            End If

        ElseIf mopTp = "" Or (mopTp = "GC" And LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y") Then   'sabrina R1930 Then
            If depFlag = "N" Then mopTp = "CD"

            oraTransaction = connLocal.BeginTransaction
            With oraCmd
                .Transaction = oraTransaction
                .Connection = connLocal
                oraCmd.CommandText = "INSERT INTO payment (sessionid, MOP_CD, AMT, DES, MOP_TP) values (:SESSIONID, :MOP, :AMT, :DES, :BC)"
                oraCmd.Parameters.Add(":SESSIONID", OracleType.VarChar)
                oraCmd.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
                oraCmd.Parameters.Add(":MOP", OracleType.VarChar)
                oraCmd.Parameters(":MOP").Value = mop_drp.Value
                oraCmd.Parameters.Add(":AMT", OracleType.VarChar)
                oraCmd.Parameters(":AMT").Value = CDbl(TextBox1.Text)
                oraCmd.Parameters.Add(":DES", OracleType.VarChar)
                oraCmd.Parameters(":DES").Value = desc
                oraCmd.Parameters.Add(":BC", OracleType.VarChar)
                oraCmd.Parameters(":BC").Value = mopTp
            End With
            oraCmd.ExecuteNonQuery()
            oraTransaction.Commit()

        ElseIf mopTp = "FI" Then

            If mop_drp.Value = "ESR" Then
                PaymentUtils.PopulateFinanceProviders(Session("store_cd"), cbo_finance_company_esr)
                Add_Finance_Company_esr()
                lbl_pmt_tp_esr.Text = mop_drp.Value
                'Display the finance details window for entry
                ASPxPopupControl12.ShowOnPageLoad = True
            Else
                txt_fi_acct.Text = ""
                txt_fi_appr.Text = ""
                PaymentUtils.PopulateFinanceProviders(Session("store_cd"), cbo_finance_company)
                Add_Finance_Company() 'lucy add
                ' GetPromoCodes() 'lucy comment
                lbl_pmt_tp.Text = mop_drp.Value
                If txt_fi_acct.Text & "" = "" Then
                    txt_fi_acct.Text = Retrieve_Finance_Account(Session("CUST_CD"))
                End If
                'Display the finance details window for entry
                ASPxPopupControl2.ShowOnPageLoad = True
            End If


        ElseIf mopTp = "TD" Then ' Daniela Dec 08 TD finance new 
            txt_fi_acct.Text = ""
            txt_fi_appr.Text = ""
            btn_save_fi_co.Enabled = True
            btn_save_fi_co.Visible = True
            'PaymentUtils.PopulateFinanceProviders(Session("store_cd"), cbo_finance_company)
            Add_Td_Company()
            lbl_pmt_tp.Text = mop_drp.Value
            If txt_fi_acct.Text & "" = "" Then
                txt_fi_acct.Text = Retrieve_Finance_Account(Session("CUST_CD"))
            End If
            'Display the finance details window for entry
            ASPxPopupControl2.ShowOnPageLoad = True
        ElseIf mopTp = "CK" Then
            lbl_pmt_tp.Text = mop_drp.Value
            ASPxPopupControl3.ShowOnPageLoad = True
            'sabrina R1930 ElseIf mopTp = "GC" Then
        ElseIf (mopTp = "GC" And LeonsBiz.isLeonsUser(Session("CO_CD")) <> "Y") Then 'SABRINA R1930
            lbl_pmt_tp.Text = mop_drp.Value
            ASPxPopupControl9.ShowOnPageLoad = True
            sy_processGC() 'sabrina add
        End If

        connErp.Close()
        connLocal.Close()
        Return True

    End Function

    Sub DoItemEdit(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)

        Dim objRetPrc As TextBox

        Dim str_mop As String    'lucy
        str_mop = Left(e.Item.Cells(1).Text, 6)
        Dim str_y_n As String

        str_y_n = e.Item.Cells(4).Text

        If InStr(str_y_n, "YES") = 0 Then   ' not credit card
            'If str_mop <> "VISA" And str_mop <> "FINA" And str_mop <> "MAST" And str_mop <> "DEBI" Then  'lucy add the if condition
            Gridview1.EditItemIndex = e.Item.ItemIndex
            bindgrid()
            objRetPrc = CType(Gridview1.Items(Gridview1.EditItemIndex).Cells(2).Controls(0), TextBox)
            objRetPrc.Width = Unit.Parse("1.5 cm")
            calculate_total()
        Else  'lucy
            Exit Sub 'lucy
        End If

    End Sub

    Sub DoItemCancel(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)
        Gridview1.EditItemIndex = -1
        bindgrid()
        calculate_total()
    End Sub

    Sub DoItemUpdate(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)
        Dim objRetPrc As TextBox
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim thistransaction As OracleTransaction

        conn.Open()

        objRetPrc = e.Item.Cells(2).Controls(0)

        If IsNumeric(objRetPrc.Text) Then
            objRetPrc.Text = CDbl(objRetPrc.Text)
        End If

        If objRetPrc.Text = "" Then
            objRetPrc.Text = 0
        End If

        If Not IsNumeric(objRetPrc.Text) Then
            vld_price.Text = "Please Enter A Numeric Value For Amount"
            vld_price.Visible = True
            vld_price.IsValid = False
        Else
            thistransaction = conn.BeginTransaction

            Try
                With cmdUpdateItems
                    .Transaction = thistransaction
                    .Connection = conn
                    .CommandText = "UPDATE payment SET amt = :AMT WHERE row_id = :UNIQUEID"
                    .Parameters.Add(":AMT", OracleType.Double)
                    .Parameters(":AMT").Value = Convert.ToDouble(objRetPrc.Text)
                    .Parameters.Add(":UNIQUEID", OracleType.VarChar)
                    .Parameters(":UNIQUEID").Value = Gridview1.DataKeys(e.Item.ItemIndex).ToString
                End With
                cmdUpdateItems.ExecuteNonQuery()
                thistransaction.Commit()
            Catch ex As Exception
                Throw
            End Try

        End If
        Gridview1.EditItemIndex = -1
        conn.Close()

        bindgrid()
        calculate_total()

    End Sub


    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim rowSeqNum As String = Gridview1.DataKeys(e.Item.ItemIndex)

        Dim str_mop As String    'lucy
        str_mop = Left(e.Item.Cells(1).Text, 4)
        Dim str_y_n As String
        str_y_n = e.Item.Cells(4).Text
        If str_y_n = "NO" Then   ' not credit card
            thePaymentBiz.DeletePayment(rowSeqNum)

            Response.Redirect("Payment.aspx")
        Else  'lucy
            Exit Sub 'lucy
        End If
    End Sub
    Protected Sub doitemdelete_old(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand
        'lucy this is the old  sub doitemdelete 
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim deletecd As String
        Dim str_mop As String    'lucy
        str_mop = Left(e.Item.Cells(1).Text, 4)

        Dim str_y_n As String

        str_y_n = e.Item.Cells(4).Text

        If str_y_n = "NO" Then   ' not credit card

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            deletecd = Gridview1.DataKeys(e.Item.ItemIndex)
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "delete from payment where row_id = :UNIQUEID"
                .Parameters.Add(":UNIQUEID", OracleType.VarChar)
                .Parameters(":UNIQUEID").Value = deletecd
            End With
            cmdDeleteItems.ExecuteNonQuery()
            conn.Close()
            Response.Redirect("Payment.aspx")
        Else  'lucy
            Exit Sub 'lucy
        End If

    End Sub

    Protected Sub btn_calc_dep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_calc_dep.Click

        If txt_dp.Text & "" <> "" Then
            If IsNumeric(txt_dp.Text) Then
                If CDbl(txt_dp.Text) >= 0 And CDbl(txt_dp.Text) <= 1 Then
                    TextBox1.Text = FormatNumber(CDbl(Session("Grand_Total")) * CDbl(txt_dp.Text), 2)
                ElseIf CDbl(txt_dp.Text) >= 0 And CDbl(txt_dp.Text) <= 100 Then
                    TextBox1.Text = FormatNumber(CDbl(Session("Grand_Total")) * (CDbl(txt_dp.Text) / 100), 2)
                Else
                    txt_dp.Text = ""
                    If IsNumeric(Session("grand_total")) And IsNumeric(Session("payments")) Then
                        lbl_cod.Text = CDbl(Session("grand_total")) - CDbl(Session("payments"))
                        TextBox1.Text = CDbl(Session("grand_total")) - CDbl(Session("payments"))
                    End If
                End If
            Else
                txt_dp.Text = ""
                If IsNumeric(Session("grand_total")) And IsNumeric(Session("payments")) Then
                    lbl_cod.Text = CDbl(Session("grand_total")) - CDbl(Session("payments"))
                    TextBox1.Text = CDbl(Session("grand_total")) - CDbl(Session("payments"))
                End If
            End If
        Else
            txt_dp.Text = ""
            If IsNumeric(Session("grand_total")) And IsNumeric(Session("payments")) Then
                lbl_cod.Text = CDbl(Session("grand_total")) - CDbl(Session("payments"))
                TextBox1.Text = CDbl(Session("grand_total")) - CDbl(Session("payments"))
            End If
        End If

    End Sub


    ''' <summary>
    ''' Fires whe nthe user clicks on the 'Save' button on the Finance popup window
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_save_fi_co_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_fi_co.Click

        Dim merchant_id As String = Lookup_Merchant("FI", Session("store_cd"), lbl_pmt_tp.Text)
        Dim regex1 As Regex = New Regex("^[0-9]")
        Dim match1 As Match = regex1.Match(txt_fi_exp_dt.Text)

        If ConfigurationManager.AppSettings("finance") = "N" Then merchant_id = ""



        If (Not String.IsNullOrEmpty(merchant_id)) Then

            'verify the card/acct number for FI  first 
            Dim myVerify As New VerifyCC
            Dim myTypeValid As New VerifyCC.TypeValid
            myTypeValid = myVerify.GetCardInfo(txt_fi_acct.Text)
            If String.IsNullOrEmpty(myTypeValid.CCType) Or myTypeValid.CCType = "Unknown" Then
                lbl_fi_warning.Text = "Invalid account #"
                txt_fi_acct.Focus()
                Exit Sub
            End If

            If txt_fi_acct.Text & "" = "" Then
                lbl_fi_warning.Text = "You must enter an account #"
                Exit Sub
            End If
            If Not IsNumeric(txt_fi_acct.Text) Then
                lbl_fi_warning.Text = "Account # must be numeric"
                Exit Sub
            End If
        End If

        'sabrina R1999 Nov12
        If Session("lucy_mop_cd") = "DCS" Then
            If LeonsBiz.isLeonsUser(Session("CO_CD")) <> "Y" Then
                If LeonsBiz.isvalidDCSplan(cbo_finance_company.SelectedValue()) <> "Y" Then
                    lbl_fi_warning.Text = "Please Select a Plan"
                    cbo_finance_company.Focus()
                    Exit Sub
                End If
            End If
        End If
        'sabrina R1999

        ASPxPopupControl1.ShowOnPageLoad = True  'lucy add


        lucy_btn_save_fi_co_Click() 'lucy


        'lucy removed rest of the codes
    End Sub

    Protected Sub btn_save_chk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_chk.Click

        If txt_check.Text & "" = "" Then
            lbl_chk_warnings.Text = "Check # cannot be blank"
            Exit Sub
        End If
        If ConfigurationManager.AppSettings("chk_guar") = "Y" Then
            If TextBox2.Text & "" = "" Then
                lbl_chk_warnings.Text = "Routing # cannot be blank"
                Exit Sub
            End If
            If TextBox3.Text & "" = "" Then
                lbl_chk_warnings.Text = "Account # cannot be blank"
                Exit Sub
            End If
            If txt_dl_state.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
            If txt_dl_number.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
        End If
        If ConfigurationManager.AppSettings("dl_verify") = "Y" Then
            If txt_dl_state.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
            If txt_dl_number.Text & "" = "" Then
                lbl_chk_warnings.Text = "Must enter driver's license and state"
                Exit Sub
            End If
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        cmdInsertItems.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = :PMT"
        cmdInsertItems.Parameters.Add(":PMT", OracleType.VarChar)
        cmdInsertItems.Parameters(":PMT").Value = lbl_pmt_tp.Text
        cmdInsertItems.Connection = conn
        sAdp = DisposablesManager.BuildOracleDataAdapter(cmdInsertItems)

        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, CHK_NUM, CHK_TP, TRANS_ROUTE, CHK_ACCOUNT_NO, APPROVAL_CD, DL_STATE, DL_LICENSE_NO)"
            sql = sql & " VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :CHK_NUM, :CHK_TP, :TRANS_ROUTE, :CHK_ACCOUNT_NO, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO)"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":CHK_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objSql.Parameters.Add(":CHK_TP", OracleType.VarChar)
            objSql.Parameters.Add(":TRANS_ROUTE", OracleType.VarChar)
            objSql.Parameters.Add(":CHK_ACCOUNT_NO", OracleType.VarChar)
            objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp.Text
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":CHK_NUM").Value = txt_check.Text.ToString
            objSql.Parameters(":APPROVAL_CD").Value = txt_appr.Text.ToString
            objSql.Parameters(":CHK_TP").Value = DropDownList2.SelectedValue.ToString
            objSql.Parameters(":TRANS_ROUTE").Value = TextBox2.Text.ToString
            objSql.Parameters(":CHK_ACCOUNT_NO").Value = TextBox3.Text.ToString
            objSql.Parameters(":DL_STATE").Value = txt_dl_state.Text.ToString
            objSql.Parameters(":DL_LICENSE_NO").Value = txt_dl_number.Text.ToString

            objSql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        ASPxPopupControl3.ShowOnPageLoad = False
        lbl_pmt_tp.Text = ""
        Session("payment") = True
        lbl_pmt_amt.Text = ""
        calculate_total()

        Response.Redirect("Payment.aspx")

    End Sub

    Protected Sub cbo_finance_company_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_finance_company.SelectedIndexChanged

        ' GetPromoCodes()
        Dim mopcd As String = Session("lucy_mop_cd")

        ASPxPopupControl2.ShowOnPageLoad = True 'lucy add
        btn_save_fi_co.Visible = True
        btn_save_fi_co.Focus()
        If (mopcd = "TDF") Then
            lucy_btn_new_cust.Visible = False
        Else
            lucy_btn_new_cust.Visible = True
        End If

    End Sub

    ''' <summary>
    ''' Retrieves and populates the promotions for a particular finance provider.
    ''' </summary>
    Private Sub GetPromoCodes()
        PaymentUtils.PopulatePromotions(lbl_pmt_tp.Text, cbo_finance_company.SelectedValue, cbo_promo)
    End Sub

    Protected Sub txt_fi_acct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        ' If IsSwiped(txt_fi_acct.Text) Then

        'TODO- we are not currently doing anything with the swiped data- ideally the raw track info should be sent in the auth
        ' instead of the parsed account# - similar to IndependenPmt. For that, we'd need another placeholder vbl.
        'get the parsed acct# and exp date from the swiped track info 
        '  Dim swipedData = txt_fi_acct.Text
        '  Dim encryptedSwipedData As String = Encrypt(txt_fi_acct.Text, "CrOcOdIlE")
        ' txt_fi_acct.Text = GetAccountNumber(swipedData)
        'txt_fi_exp_dt.Text = GetExpirationDate(swipedData)
        ' Else
        ' TextBox2.Focus()
        'End If


        txt_fi_exp_dt.Focus() 'lucy comment all except this line

        ' verify the card/acct number 
        Dim myVerify As New VerifyCC
        Dim myTypeValid As New VerifyCC.TypeValid
        myTypeValid = myVerify.GetCardInfo(txt_fi_acct.Text)
        Dim txt_fi_dt As TextBox = pnlFinanceDetails.FindControl("txt_fi_exp_dt")
        Dim txt_fi_ac As TextBox = pnlFinanceDetails.FindControl("txt_fi_acct")
        If myTypeValid.CCValid = True Then
            If Not txt_fi_dt Is Nothing Then
                txt_fi_dt.Focus()
            Else
                lbl_fi_warning.Text = String.Empty
                txt_fi_ac.Focus()
            End If
        Else
            txt_fi_dt.Focus()
        End If
    End Sub


    Protected Sub txt_fi_exp_dt_OnTextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim regex1 As Regex = New Regex("^[0-9]")
            Dim match1 As Match = regex1.Match(txt_fi_exp_dt.Text)
            If match1.Success AndAlso txt_fi_exp_dt.Text <> "" AndAlso CInt((txt_fi_exp_dt.Text).Substring(0, 2)) >= 1 AndAlso CInt((txt_fi_exp_dt.Text).Substring(0, 2)) <= 12 Then
                'lbl_fi_warning.Visible = False
                txt_fi_appr.Focus()
            Else

                lbl_fi_warning.Visible = True
                lbl_fi_warning.Text = String.Empty
                lbl_fi_warning.Text = " ** Invalid Month and Year ** "
                txt_fi_exp_dt.Focus()
            End If
        Catch ex As Exception

        End Try
    End Sub


    Public Sub Add_Existing_Payments()

        If Session("adj_ivc_cd") & "" = "" Then
            Exit Sub
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT b.MOP_CD, b.DES, a.ACCT_NUM, a.BNK_CRD_NUM, a.EXP_DT FROM MOP b, AR_TRN a "
        sql = sql & "WHERE IVC_CD='" & Session("adj_ivc_cd") & "' AND SUBSTR(ACCT_NUM,1,2) = 'ID' "
        sql = sql & "AND POST_DT > SYSDATE-60 AND a.MOP_CD=b.MOP_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                MyDataGrid.DataSource = ds
                MyDataGrid.DataBind()
                ASPxPopupControl4.ShowOnPageLoad = True
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Sub txt_po_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_po.TextChanged

        Session("po") = txt_po.Text

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        'sabrina add GC
        Dim merchant_id As String = ""
        ASPxPopupControl9.ShowOnPageLoad = True
        Dim str_display As String '= gc_send_subTp8()

        If str_display = "no designated pinpad " Or str_display = "No designated pinpad" Then
            ASPxPopupControl9.ShowOnPageLoad = False
            ASPxLabel15.Text = "no designated pinpad "
            Exit Sub
        End If

        If txt_gc.Text & "" = "" Then
            ASPxLabel15.Text = "Account # cannot be blank"
            Exit Sub
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'jkl
        'sql = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = '" & lbl_pmt_tp.Text & "'"
        'sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        cmdInsertItems.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD =:MOP_CD"
        cmdInsertItems.Parameters.Add(":MOP_CD", OracleType.VarChar)
        cmdInsertItems.Parameters(":MOP_CD").Value = lbl_pmt_tp.Text
        cmdInsertItems.Connection = conn
        sAdp = DisposablesManager.BuildOracleDataAdapter(cmdInsertItems)

        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, BC, APPROVAL_CD)"
            sql = sql & " VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :BC, :APPROVAL_CD)"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":BC", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp.Text
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":BC").Value = txt_gc.Text.ToString
            objSql.Parameters(":APPROVAL_CD").Value = Session("GC_auth_no")

            objSql.ExecuteNonQuery()

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        ASPxPopupControl9.ShowOnPageLoad = False

        lbl_pmt_tp.Text = ""
        Session("payment") = True
        lbl_pmt_amt.Text = ""
        calculate_total()

        Response.Redirect("Payment.aspx")

    End Sub

    Protected Sub txt_gc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim merchant_id As String = ""
        'sabrina comment out GC
        'merchant_id = Lookup_Merchant("GC", Session("store_cd"), "GC")

        'If String.IsNullOrEmpty(merchant_id) Then
        '    ASPxLabel15.Text = "No Gift Card Merchant ID found for your home store code"
        '    Exit Sub
        'End If

        'Dim xml_response As System.Xml.XmlDocument = World_Gift_Utils.WGC_SendRequestAndGetResponse(merchant_id, "balance", txt_gc.Text)
        'ASPxLabel15.Text = "Response Received: " & Now.Date & vbCrLf
        'If IsNumeric(xml_response.InnerText) Then
        '    ASPxLabel15.Text = "Card Balance: $" & FormatNumber(CDbl(xml_response.InnerText), 2) & vbCrLf
        '    If FormatNumber(CDbl(xml_response.InnerText), 2) < CDbl(lbl_pmt_amt.Text) Then
        '        lbl_pmt_amt.Text = FormatNumber(CDbl(xml_response.InnerText), 2)
        '        ASPxLabel15.Text = "Payment Amount was changed!  The balance on this gift card was less than the amount requested for payment."
        '    End If
        'Else
        '    ASPxLabel15.Text = xml_response.InnerText
        'End If

    End Sub

    Protected Sub Gridview1_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

        Dim Ping_Image As System.Web.UI.WebControls.Image

        Ping_Image = e.Item.FindControl("Image1")
        If Not Ping_Image Is Nothing Then
            If e.Item.Cells(7).Text & "" <> "" And e.Item.Cells(7).Text <> "&nbsp;" Then
                If Lookup_Merchant(e.Item.Cells(7).Text, Session("store_cd"), e.Item.Cells(0).Text) & "" <> "" Then
                    Ping_Image.ImageUrl = "images/icons/tick.png"
                Else
                    Ping_Image.ImageUrl = "images/icons/error.png"
                End If
            Else
                Ping_Image.ImageUrl = "images/icons/error.png"
            End If
        End If

    End Sub

    Private Function sy_gc_send_subTp8() As String
        'sabrina add(new sub)

        Dim v_card_num As String = ""
        If InStr(TextBox1.Text, "?") > 0 Then
            Dim sCC As Array
            sCC = Split(TextBox1.Text.ToString, "?")
            TextBox1.Text = Right(sCC(0).Trim, Len(sCC(0)) - 1)
            TextBox1.Enabled = False
        End If

        'TextBox1.Focus()
        Dim merchant_id As String = ""
        Dim myipadd As String = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim

        'ask donna if this validation is required
        'merchant_id = Lookup_Merchant("GC", Session("home_store_cd"), "GC")

        'If String.IsNullOrEmpty(merchant_id) Then
        '    ASPxLabel15.Text = "No Gift Card Merchant ID found for your home store code"
        '    Exit Function
        'End If

        Dim myret_string As String = ""
        Dim v_term_id As String = ""
        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else ' if not go global then default to remote ip
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        Try
            If IsDBNull(v_term_id) Or isEmpty(v_term_id) Or v_term_id = "empty" Then
                ASPxLabel15.Text = "Invalid Terminal ID " & v_term_id
                Exit Function
            End If
        Catch
            ASPxLabel15.Text = Err.Description
        End Try

        Dim mybal As Integer = 0
        Dim str_del_doc_num As String = Session("del_doc_num")
        Dim str_cust_cd As String = Session("cust_cd")
        Dim v_amt As String = CDbl(lbl_pmt_amt.Text)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        ' DB May 11, 2015 Add the customer before Invoice Print
        CreateCustomer()

        If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
            str_del_doc_num = ""
        End If
        If IsDBNull(Session("cust_cd")) Or isEmpty(Session("cust_cd")) Then
            str_cust_cd = ""
        Else
            str_cust_cd = Session("cust_cd")
        End If

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        'subtp8 for GC
        conn.Open()
        sycmd.Connection = conn
        sycmd.CommandText = "std_brk_pos1.sendsubTp8"
        sycmd.CommandType = CommandType.StoredProcedure

        sycmd.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = "" 'sabrina (brick requirement for GC) v_card_num
        sycmd.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = ""
        sycmd.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
        sycmd.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
        sycmd.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = "8"
        sycmd.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = ""
        sycmd.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = ""
        sycmd.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = "" 'str_s_or_m
        sycmd.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = "GC"
        sycmd.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = ""
        sycmd.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = ""
        sycmd.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = ""

        sycmd.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
        sycmd.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust_cd
        sycmd.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num
        sycmd.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

        Dim l_aut As String = ""
        Dim l_dsp As String = ""
        Dim l_ack As String = ""
        Dim l_res As String = ""
        Dim l_crn As String = ""
        Dim l_exp As String = ""
        Dim l_tr2 As String = ""
        Dim l_rcp As String = ""
        Dim l_filename As String = ""
        Dim l_printcmd As String = ""

        'sycmd.ExecuteNonQuery()

        Try
            syda.Fill(syds)

        Catch ex As Exception
            ASPxLabel15.Text = ex.Message.ToString()
            conn.Close()
            Throw
        End Try


        If IsDBNull(sycmd.Parameters("p_aut").Value) = False Then
            l_aut = sycmd.Parameters("p_aut").Value
        Else
            l_aut = "empty"
        End If

        l_dsp = sycmd.Parameters("p_dsp").Value
        l_ack = sycmd.Parameters("p_ackInd").Value
        l_res = sycmd.Parameters("p_res").Value
        l_dsp = sycmd.Parameters("p_dsp").Value

        If IsDBNull(sycmd.Parameters("p_tr2").Value) = False Then
            l_tr2 = sycmd.Parameters("p_tr2").Value
        Else
            l_tr2 = "empty"
        End If

        If IsDBNull(sycmd.Parameters("p_exp").Value) = False Then
            l_exp = sycmd.Parameters("P_exp").Value
        Else
            l_exp = "empty"
        End If

        If IsDBNull(sycmd.Parameters("p_crn").Value) = False Then
            l_crn = sycmd.Parameters("p_crn").Value.ToString()
        Else
            l_crn = "empty"
        End If

        ASPxLabel15.Text = l_dsp

        'Daniela french fix
        'If l_dsp = "01 Approval" Then   'transation done successfully
        If isNotEmpty(l_dsp) AndAlso InStr(UCase(l_dsp), "APPR") > 0 Then   'transation done successfully
            btnSubmit.Visible = True
            ASPxLabel15.Text = l_dsp & " - Press Save to continue"
            l_filename = sycmd.Parameters("p_outfile").Value
            l_printcmd = sycmd.Parameters("p_cmd").Value
            Session("str_print_cmd") = l_printcmd
            Session("GC_auth_no") = l_aut
            txt_gc.Text = l_crn
            print_receipt(l_printcmd)
        Else   'transation not done 
            btnSubmit.Visible = False
            l_dsp = sycmd.Parameters("p_dsp").Value
            ASPxLabel15.Text = l_dsp & " - press Clear to try again"
            txt_gc.Text = l_crn
        End If

        'Daniela Dec 16 save payment status for multiple payments
        If Not InStr(UCase(Session("cust_display")), "APPR") > 0 Then
            Session("cust_display") = Left(l_dsp, 20)
        End If
        Session("lucy_display") = Left(l_dsp, 20)
        Try
            sycmd.Cancel()
            sycmd.Dispose()
            conn.Close()
            conn.Dispose()
        Catch
        End Try

        Return l_dsp

    End Function
    Private Sub CreateCustomer()

        If Session("cust_cd") = "NEW" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim hphone As String = ""
            Dim bphone As String = ""
            Dim cust_tp_cd As String = ""
            Dim email As String = ""

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("cust_cd") & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Try
                dbConnection.Open()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    fname = dbReader.Item("BILL_FNAME").ToString.Trim
                    If fname & "" = "" Then fname = dbReader.Item("FNAME").ToString.Trim
                    lname = dbReader.Item("BILL_LNAME").ToString.Trim
                    If lname & "" = "" Then lname = dbReader.Item("LNAME").ToString.Trim
                    cust_tp_cd = dbReader.Item("CUST_TP").ToString.Trim
                    addr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                    If addr1 & "" = "" Then addr1 = dbReader.Item("ADDR1").ToString.Trim
                    addr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                    If addr2 & "" = "" Then addr2 = dbReader.Item("ADDR2").ToString.Trim
                    city = dbReader.Item("BILL_CITY").ToString.Trim
                    If city & "" = "" Then city = dbReader.Item("CITY").ToString.Trim
                    state = dbReader.Item("BILL_ST").ToString.Trim
                    If state & "" = "" Then state = dbReader.Item("ST").ToString.Trim
                    zip = dbReader.Item("BILL_ZIP").ToString.Trim
                    If zip & "" = "" Then zip = dbReader.Item("ZIP").ToString.Trim
                    hphone = StringUtils.FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                    bphone = StringUtils.FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)
                    email = dbReader.Item("EMAIL").ToString.Trim
                End If
                dbReader.Close()
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
            ' a single transaction to be able to rollback all changes if something fails
            Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")

            Session("CUST_CD") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"),
                                                                 UCase(addr1),
                                                                 Left(UCase(new_cust_lname), 20),
                                                                 Left(UCase(new_cust_fname), 15))
            Try
                dbAtomicConnection.Open()
                dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction()

                ' --- Save Customer to the E1 Database
                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
                sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_CD").Value = Session("CUST_CD").ToString.Trim
                dbAtomicCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
                dbAtomicCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
                dbAtomicCommand.Parameters.Add(":FNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":FNAME").Value = UCase(fname)
                dbAtomicCommand.Parameters.Add(":LNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":LNAME").Value = UCase(lname)
                dbAtomicCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_TP").Value = cust_tp_cd
                dbAtomicCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR1").Value = UCase(addr1)
                dbAtomicCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR2").Value = UCase(addr2)
                dbAtomicCommand.Parameters.Add(":CITY", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CITY").Value = UCase(city)
                dbAtomicCommand.Parameters.Add(":ST", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ST").Value = UCase(state)
                dbAtomicCommand.Parameters.Add(":ZIP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ZIP").Value = zip
                dbAtomicCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":HPHONE").Value = hphone
                dbAtomicCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":BPHONE").Value = bphone
                dbAtomicCommand.Parameters.Add(":EMAIL", OracleType.VarChar)
                dbAtomicCommand.Parameters(":EMAIL").Value = UCase(email)
                dbAtomicCommand.ExecuteNonQuery()
                dbAtomicCommand.Parameters.Clear()  'to get it ready for the next statement

                ' --- Update newly created Customer Code to the table in the CRM schema 
                sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("CUST_CD") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='NEW'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves all the pending changes.
                dbAtomicCommand.Transaction.Commit()

            Catch ex As Exception
                dbAtomicCommand.Transaction.Rollback()  'reverts any changes made so far
                Throw
            Finally
                dbAtomicCommand.Dispose()
                dbAtomicConnection.Close()
            End Try
        End If
    End Sub
    Private Sub sy_processGC()
        'sabrina add
        ASPxPopupControl9.ShowOnPageLoad = True
        sy_gc_send_subTp8()
    End Sub
    Protected Sub btn_Clear_Click(sender As Object, e As EventArgs) Handles btn_Clear.Click
        'sabrina add
        txt_gc.Text = ""
        sy_gc_send_subTp8()
    End Sub
    Public Sub sy_addDCScompany()

        'sabrina R1999 (new sub)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        ''sabrina R2335 - for Quebec only show EMP (equal monthly payment plans)
        'Dim v_str As String = ""

        'If Not Session("clientip") Is Nothing Then
        '    v_str = LeonsBiz.GetPaymentStore(Session("clientip"))
        'Else
        '    v_str = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        'End If
        'If v_str & "" = "" Then
        '    v_str = LeonsBiz.GetHomeStore(Session("emp_init"))
        'End If


        'If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
        '    If LeonsBiz.isQuebecStore(v_str) = "Y" Then
        '        sql = "select plan_cd mop_cd, des from brick_dcs_plan "
        '        sql = sql & " where pmt_plan_id = 'EMP' and bank_mop_cd='DCS' and (leon_show_in_pos IS NULL OR leon_show_in_pos='Y') "
        '        sql = sql & " order by des"
        '    Else
        '        sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='DCS' and (leon_show_in_pos IS NULL OR leon_show_in_pos='Y') order by des"
        '    End If
        'Else
        '    If LeonsBiz.isQuebecStore(v_str) = "Y" Then
        '        sql = "select plan_cd mop_cd, des from brick_dcs_plan "
        '        sql = sql & " where pmt_plan_id = 'EMP' and bank_mop_cd='DCS' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  "
        '        sql = sql & " order by des"
        '    Else
        '        sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='DCS' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  order by des"
        '    End If
        'End If

        'sabrina R2335 - for Quebec only show EMP (equal monthly payment plans) 



        'Alice udpated On Oct 11, 2018 for reuqest 4115, plus:for Quebec only show EMP for only Brick store

        Dim v_str As String = ""

        If Not Session("clientip") Is Nothing Then
            v_str = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else
            v_str = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If
        If v_str & "" = "" Then
            v_str = LeonsBiz.GetHomeStore(Session("emp_init"))
        End If


        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Or LeonsBiz.isLeonFranStr(Session("store_cd")) = "Y" Then 'Alice updated on Mar25, 2019 for Leons's franchise store
            sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='DCS' and (leon_show_in_pos IS NULL OR leon_show_in_pos='Y') order by des"

        Else
            If LeonsBiz.isQuebecStore(v_str) = "Y" Then
                sql = "select plan_cd mop_cd, des from brick_dcs_plan "
                sql = sql & " where pmt_plan_id = 'EMP' and bank_mop_cd='DCS' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  "
                sql = sql & " order by des"
            Else
                sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='DCS' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  order by des"
            End If
        End If

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "mop_cd"
                cbo_finance_company.DataTextField = "des"
                cbo_finance_company.DataBind()
            End If

            cbo_finance_company.Items.Insert(0, "Please select VISAD finance plan")
            cbo_finance_company.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub

    'Alice added on Oct 03, 2018 for request 4115, Flx plan only show up for brick user
    Public Sub sy_addFLXcompany()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim v_str As String = ""

        If Not Session("clientip") Is Nothing Then
            v_str = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else
            v_str = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If
        If v_str & "" = "" Then
            v_str = LeonsBiz.GetHomeStore(Session("emp_init"))
        End If

        If LeonsBiz.isQuebecStore(v_str) = "Y" Then
            sql = "select plan_cd mop_cd, des from brick_dcs_plan "
            sql = sql & " where pmt_plan_id = 'EMP' and bank_mop_cd='FLX' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')"
            sql = sql & " order by des"
        Else
            sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='FLX' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y') order by des"
        End If

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company_flx.ClearSelection()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                cbo_finance_company_flx.DataSource = ds
                cbo_finance_company_flx.DataValueField = "mop_cd"
                cbo_finance_company_flx.DataTextField = "des"
                cbo_finance_company_flx.DataBind()
            End If

            cbo_finance_company_flx.Items.Insert(0, "Please Select Flexiti finance plan")
            cbo_finance_company_flx.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub



    Public Function sy_brk_process_visad() As String
        'R1999 sabrina new function

        Dim connString As String
        Dim objConnection As OracleConnection
        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_crt As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_dis_line_1 As String
        Dim v_dis_line_2 As String
        Dim v_reserve As String
        Dim v_term_id As String

        Dim v_rpt_name As String
        Dim v_trans_type As String
        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_trans_no As String
        Dim v_auth_num As String
        Dim v_track2 As String
        Dim v_auth_no As String
        Dim v_oth As String
        Dim v_length As Integer
        Dim v_response As String
        Dim v_mop_cd As String

        Dim v_private_lbl_finance_tp As String
        Dim v_private_lbl_plan_num As String
        Dim v_count As Integer

        Dim l_filename As String
        Dim l_printcmd As String
        Dim strAsCd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim str_cust_cd As String = ""
        strAsCd = cbo_finance_company.SelectedValue

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            Return "No designated pinpad"
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Session("auth_no") = ""
        Session("lucy_approval_cd") = ""

        v_sub_type = ""

        Dim str_del_doc_num As String = Session("del_doc_num")
        Dim str_emp_cd As String = Session("emp_cd")
        If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
            str_del_doc_num = ""
        End If


        CreateCustomer()

        If IsDBNull(Session("cust_cd")) Or isEmpty(Session("cust_cd")) Then
            str_cust_cd = ""
        Else
            str_cust_cd = Session("cust_cd")
        End If
        Session("del_doc_num") = ""

        v_auth_no = "          "    '10 spaces
        v_mop_cd = "DCS"
        v_trans_type = ""

        v_amt = Session("lucy_amt")

        v_private_lbl_finance_tp = strAsCd


        v_card_num = ""
        v_expire_date = ""

        If Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = True Then   'manualy enterred
            v_card_num = Trim(txt_fi_acct.Text)
            v_expire_date = txt_fi_exp_dt.Text
            Session("lucy_bnk_card") = v_card_num
            Session("lucy_exp_dt") = v_expire_date
            str_s_or_m = "M"
        ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False And Len(Trim(Session("lucy_card_num"))) = 16 And IsNumeric(Session("lucy_card_num")) = True Then 'auto populated
            v_card_num = Session("lucy_card_num")
            v_card_num = ""   'force to swipe or need a exp_dt enterred
            v_expire_date = ""
            str_s_or_m = "S"
        Else    'swipe
            v_card_num = ""
            v_expire_date = ""
            str_s_or_m = "S"
        End If

        Try

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection

            myCMD.CommandText = "std_tenderretail_pos.sendsubtp8"

            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust_cd
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output


            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"
            End If

            ' If l_aut <> "empty" Then ' peter put empty means l_aut has null value
            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces  
            'v_trans_no = myCMD.Parameters("p_transTp").Value

            l_dsp = myCMD.Parameters("p_dsp").Value
            lbl_fi_warning.Text = l_dsp
            If l_aut <> "empty" Then   'transation done successfully
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value

                l_dsp = myCMD.Parameters("p_dsp").Value
                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                    l_exp = myCMD.Parameters("p_exp").Value
                    l_crn = myCMD.Parameters("p_crn").Value
                    Session("lucy_bnk_card") = Left(l_crn, 16)
                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)
                    If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                        txt_fi_acct.Text = Left(l_crn, 16)
                    ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False Then 'auto populated account#
                        txt_fi_acct.Text = Left(l_crn, 16)
                    End If
                Else
                    l_exp = myCMD.Parameters("p_exp").Value
                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)

                End If

                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value
                Session("str_print_cmd") = l_printcmd
                Session("auth_no") = l_aut
                Session("lucy_approval_cd") = l_aut

                print_receipt(l_printcmd)


                txt_fi_appr.Text = l_aut

            Else   'transation not done 

                '   Session("auth_no") = l_aut
                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = ""
                l_dsp = "empty"
                If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                    txt_fi_acct.Text = ""
                End If
            End If
            'Daniela Dec 16 save payment status for multiple payments
            If Not InStr(UCase(Session("cust_display")), "APPR") > 0 Then
                Session("cust_display") = Left(l_dsp, 20)
            End If
            Session("lucy_display") = Left(l_dsp, 20)

            myCMD.Cancel()
            myCMD.Dispose()

            Return l_dsp

        Catch x
            Throw
            ' lbl_Lucy_Test.Text = x.Message.ToString
        Finally

            objConnection.Close() ' DANIELA
            objConnection.Dispose()
        End Try

    End Function
#Region "Alice added on May 29, 2019 for request 235"



    Protected Sub txt_fm_exp_OnTextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim regex1 As Regex = New Regex("^[0-9]")
            Dim match1 As Match = regex1.Match(txt_fm_exp.Text)
            If match1.Success AndAlso txt_fm_exp.Text <> "" AndAlso CInt((txt_fm_exp.Text).Substring(0, 2)) >= 1 AndAlso CInt((txt_fm_exp.Text).Substring(0, 2)) <= 12 Then
            txt_fm_appr.Focus()
            lbl_fm_warning.Text = ""
        Else

                lbl_fm_warning.Visible = True
                lbl_fm_warning.Text = String.Empty
                lbl_fm_warning.Text = " ** Invalid Month and Year ** "
                txt_fm_exp.Focus()
            End If

    End Sub

    Protected Sub txt_fm_appr_OnTextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If Len(txt_fm_appr.Text) <> 6 Then
            lbl_fm_warning.Text = Resources.LibResources.Label1002 'sabrina-french dec14 "Auth# must be 6 Digits"
            txt_fm_appr.Focus()
        Else
            lbl_fm_warning.Text = ""
        End If

    End Sub

    Protected Sub txt_fm_acct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        txt_fm_exp.Focus()

        ' verify the card/acct number 
        Dim myVerify As New VerifyCC
        Dim myTypeValid As New VerifyCC.TypeValid
        myTypeValid = myVerify.GetCardInfo(txt_fm_acct.Text)

        If myTypeValid.CCValid = False Then
            txt_fm_acct.Focus()
            lbl_fm_warning.Text = "Mastercard is invalid"
        Else
            txt_fm_exp.Focus()
            lbl_fm_warning.Text = ""
        End If
    End Sub



    Protected Sub btn_save_fm_co_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_fm_co.Click


        txt_fm_appr.Text = ""
        Dim strfinCo = cbo_finance_company_fm.SelectedValue
        Session("fm_cd") = strfinCo
        Dim merchant_id As String = ""

        ASPxPopupControl1.ShowOnPageLoad = True
        ASPxPopupControl11.ShowOnPageLoad = False
        btn_save_fm_co.Enabled = False
        btn_save_fm_co.Visible = False
        Dim str_display As String  'lucy add to call tenderretail

        If Session("emp_cd") & "" = "" Then
            ' Better to prevent payment in emp_cd is null
            Response.Redirect("login.aspx")
        End If


        str_display = sy_brk_process_mc()


        If str_display = "no designated pinpad " Or str_display = "No designated pinpad" Then
            ASPxPopupControl1.ShowOnPageLoad = False
            lucy_lbl_aprv.Text = "no designated pinpad "

            Exit Sub
        End If

        If Left(str_display, 5) = "empty" Then

            ASPxPopupControl1.ShowOnPageLoad = False
            ASPxPopupControl11.ShowOnPageLoad = True
            btn_save_fm_co.Enabled = True
            btn_save_fm_co.Visible = True
            ASPxLabel1.Visible = False
            Exit Sub
        End If

        ASPxPopupControl1.ShowOnPageLoad = True


        If txt_fm_appr.Text & "" = "" Then
            txt_fm_appr.Focus()
            lbl_fm_warning.Text = Resources.LibResources.Label836 'sabrina-french dec14 "6 digit Auth# Required"
            Exit Sub
        Else
            If Len(txt_fm_appr.Text) <> 6 Then
                lbl_fm_warning.Text = Resources.LibResources.Label1002 'sabrina-french dec14 "Auth# must be 6 Digits"
                txt_fm_appr.Focus()
                Exit Sub
            End If
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String

        ' conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        cmdInsertItems.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = :PMT"
        cmdInsertItems.Parameters.Add(":PMT", OracleType.VarChar)
        cmdInsertItems.Parameters(":PMT").Value = lbl_pmt_tp_fm.Text
        cmdInsertItems.Connection = conn
        sAdp = DisposablesManager.BuildOracleDataAdapter(cmdInsertItems)


        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString


        If Is_Finance_DP(lbl_pmt_tp_fm.Text) = True Then
            bc = "FI"
        End If
        txt_fm_exp.Text = Session("lucy_exp_dt")
        Dim str_fm_acct_num As String = txt_fm_acct.Text
        Dim str_enc_acct_num = Encrypt(txt_fm_acct.Text, "CrOcOdIlE")

        '  conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        Try
            ' Daniela add bnk card
            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, BC, MOP_TP, FIN_CO, FI_ACCT_NUM, FIN_PROMO_CD, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :BC, :MOP_TP, :FIN_CO, :FI_ACCT_NUM, :FIN_PROMO_CD, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            objSql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":BC", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            objSql.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp_fm.Text
            objSql.Parameters(":SECURITY_CD").Value = txt_fi_sec_cd.Text
            objSql.Parameters(":EXP_DT").Value = txt_fm_exp.Text
            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":BC").Value = Session("lucy_bnk_card")
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":FIN_CO").Value = cbo_finance_company_fm.SelectedValue
            objSql.Parameters(":FI_ACCT_NUM").Value = Encrypt(txt_fm_acct.Text, "CrOcOdIlE")
            objSql.Parameters(":FIN_PROMO_CD").Value = cbo_promo.SelectedValue
            objSql.Parameters(":APPROVAL_CD").Value = txt_fm_appr.Text
            objSql.Parameters(":DL_STATE").Value = txt_fi_dl_st.Text
            objSql.Parameters(":DL_LICENSE_NO").Value = txt_fi_dl_no.Text

            objSql.ExecuteNonQuery()
            objSql.Cancel()
            objSql.Dispose()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        ASPxPopupControl11.ShowOnPageLoad = False
        lbl_pmt_tp_fm.Text = ""
        Session("payment") = True
        lbl_pmt_amt.Text = ""
        calculate_total()
        ASPxPopupControl1.ShowOnPageLoad = True
        Response.Redirect("Payment.aspx")
        ASPxPopupControl11.ShowOnPageLoad = False



    End Sub

    Public Function sy_brk_process_mc() As String

        Dim connString As String
        Dim objConnection As OracleConnection
        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_crt As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_dis_line_1 As String
        Dim v_dis_line_2 As String
        Dim v_reserve As String
        Dim v_term_id As String

        Dim v_rpt_name As String
        Dim v_trans_type As String
        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_trans_no As String
        Dim v_auth_num As String
        Dim v_track2 As String
        Dim v_auth_no As String
        Dim v_oth As String
        Dim v_length As Integer
        Dim v_response As String
        Dim v_mop_cd As String

        Dim v_private_lbl_finance_tp As String
        Dim v_private_lbl_plan_num As String
        Dim v_count As Integer

        Dim l_filename As String
        Dim l_printcmd As String
        Dim strAsCd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim str_cust_cd As String = ""
        strAsCd = cbo_finance_company_FM.SelectedValue

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            Return Resources.LibResources.Label849 'sabrina-french dec14 "No designated pinpad"
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Session("auth_no") = ""
        Session("lucy_approval_cd") = ""

        v_sub_type = ""

        Dim str_del_doc_num As String = Session("del_doc_num")
        Dim str_emp_cd As String = Session("emp_cd")
        If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
            str_del_doc_num = ""
        End If


        '  CreateCustomer()

        If IsDBNull(Session("cust_cd")) Or isEmpty(Session("cust_cd")) Then
            str_cust_cd = ""
        Else
            str_cust_cd = Session("cust_cd")
        End If
        Session("del_doc_num") = ""

        v_auth_no = "          "    '10 spaces
        v_mop_cd = "FM"
        v_trans_type = ""

        v_amt = Session("lucy_amt")

        v_private_lbl_finance_tp = strAsCd


        v_card_num = ""
        v_expire_date = ""

        If Len(Trim(txt_fm_acct.Text)) >= 16 And IsNumeric(txt_fm_acct.Text) = True Then   'manualy enterred
            v_card_num = Trim(txt_fm_acct.Text)
            If isEmpty(txt_fm_exp.Text) Then
                lbl_fm_warning.Text = "Please enter expiry date"
                Return "empty"
            End If

            v_expire_date = txt_fm_exp.Text
            Session("lucy_bnk_card") = v_card_num
            Session("lucy_exp_dt") = v_expire_date
            str_s_or_m = "M"
        ElseIf Len(Trim(txt_fm_acct.Text)) >= 16 And IsNumeric(txt_fm_acct.Text) = False And Len(Trim(Session("lucy_card_num"))) = 16 And IsNumeric(Session("lucy_card_num")) = True Then 'auto populated
            v_card_num = Session("lucy_card_num")
            v_card_num = ""   'force to swipe or need a exp_dt enterred
            v_expire_date = ""
            str_s_or_m = "S"
        Else    'swipe
            v_card_num = ""
            v_expire_date = ""
            str_s_or_m = "S"
        End If

        Try

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection

            myCMD.CommandText = "std_tenderretail_pos.sendsubtp8"

            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust_cd
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output


            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"
            End If

            ' If l_aut <> "empty" Then ' peter put empty means l_aut has null value
            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces  
            'v_trans_no = myCMD.Parameters("p_transTp").Value

            l_dsp = myCMD.Parameters("p_dsp").Value
            lbl_fm_warning.Text = l_dsp
            If l_aut <> "empty" Then   'transation done successfully
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value

                l_dsp = myCMD.Parameters("p_dsp").Value
                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                    l_exp = myCMD.Parameters("p_exp").Value
                    l_crn = myCMD.Parameters("p_crn").Value
                    Session("lucy_bnk_card") = Left(l_crn, 16)
                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)
                    If Len(Trim(txt_fm_acct.Text)) < 16 Then  ' suppose not manual entered
                        txt_fm_acct.Text = Left(l_crn, 16)
                    ElseIf Len(Trim(txt_fm_acct.Text)) >= 16 And IsNumeric(txt_fm_acct.Text) = False Then 'auto populated account#
                        txt_fm_acct.Text = Left(l_crn, 16)
                    End If
                Else
                    l_exp = myCMD.Parameters("p_exp").Value
                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)

                End If

                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value
                Session("str_print_cmd") = l_printcmd
                Session("auth_no") = l_aut
                Session("lucy_approval_cd") = l_aut

                print_receipt(l_printcmd)


                txt_fm_appr.Text = l_aut

            Else   'transation not done 

                '   Session("auth_no") = l_aut
                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = ""
                l_dsp = "empty"
                If Len(Trim(txt_fm_acct.Text)) < 16 Then  ' suppose not manual entered
                    txt_fm_acct.Text = ""
                End If
            End If
            'Daniela Dec 16 save payment status for multiple payments
            If Not InStr(UCase(Session("cust_display")), "APPR") > 0 Then
                Session("cust_display") = Left(l_dsp, 20)
            End If
            Session("lucy_display") = Left(l_dsp, 20)

            myCMD.Cancel()
            myCMD.Dispose()

            Return l_dsp

        Catch x
            Throw
            ' lbl_Lucy_Test.Text = x.Message.ToString
        Finally

            objConnection.Close() ' DANIELA
            objConnection.Dispose()
        End Try

    End Function




    Public Sub sy_addFMcompany()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim v_str As String = ""

        If Not Session("clientip") Is Nothing Then
            v_str = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else
            v_str = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If
        If v_str & "" = "" Then
            v_str = LeonsBiz.GetHomeStore(Session("emp_init"))
        End If

        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Or LeonsBiz.isLeonFranStr(Session("STORE_CD")) = "Y" Then
            sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='FM' and (leon_show_in_pos IS NULL OR leon_show_in_pos='Y') order by des"

        Else
            If LeonsBiz.isQuebecStore(v_str) = "Y" Then
                sql = "select plan_cd mop_cd, des from brick_dcs_plan "
                sql = sql & " where pmt_plan_id = 'EMP' and bank_mop_cd='FM' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  "
                sql = sql & " order by des"
            Else
                sql = "select plan_cd mop_cd, des from brick_dcs_plan where bank_mop_cd='FM' and (brick_show_in_pos IS NULL OR brick_show_in_pos='Y')  order by des"
            End If
        End If

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company_fm.ClearSelection()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                cbo_finance_company_fm.DataSource = ds
                cbo_finance_company_fm.DataValueField = "mop_cd"
                cbo_finance_company_fm.DataTextField = "des"
                cbo_finance_company_fm.DataBind()
            End If

            cbo_finance_company_fm.Items.Insert(0, "Please Select MC finance plan")
            cbo_finance_company_fm.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub
#End Region

#Region "Alice added on Sep 11, 2019 for request 449"
    Public Sub Add_Finance_Company_esr()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT AS_CD, NAME FROM ASP WHERE ACTIVE='Y' and as_cd='ESR'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company_esr.ClearSelection()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)


            If (MyDataReader.Read()) Then
                cbo_finance_company_esr.DataSource = ds
                cbo_finance_company_esr.DataValueField = "AS_CD"
                cbo_finance_company_esr.DataTextField = "NAME"
                cbo_finance_company_esr.DataBind()
            End If

            cbo_finance_company_esr.Items.Insert(0, Resources.LibResources.Label415)
            cbo_finance_company_esr.Items.FindByText(Resources.LibResources.Label415).Value = ""

            cbo_finance_company_esr.SelectedIndex = 0

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_new_cust_esr_Click(sender As Object, e As EventArgs) Handles btn_new_cust_esr.Click
        Dim strfinCo = cbo_finance_company_esr.SelectedValue
        Session("lucy_as_cd") = strfinCo

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim bc As String
        Dim MyTable As DataTable
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim des As String




        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        cmdInsertItems.CommandText = "SELECT DES, MOP_TP FROM MOP WHERE MOP_CD = :PMT"
        cmdInsertItems.Parameters.Add(":PMT", OracleType.VarChar)
        cmdInsertItems.Parameters(":PMT").Value = lbl_pmt_tp_esr.Text
        cmdInsertItems.Connection = conn
        sAdp = DisposablesManager.BuildOracleDataAdapter(cmdInsertItems)


        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        des = MyTable.Rows(loop1).Item("DES").ToString
        bc = MyTable.Rows(loop1).Item("MOP_TP").ToString

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Try
            'sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO,  FIN_PROMO_CD, APPROVAL_CD, DL_STATE, DL_LICENSE_NO, SECURITY_CD, EXP_DT)"
            'sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO,  :FIN_PROMO_CD, :APPROVAL_CD, :DL_STATE, :DL_LICENSE_NO, :SECURITY_CD, :EXP_DT)"


            sql = "INSERT INTO PAYMENT (sessionid, MOP_CD, AMT, DES, MOP_TP, FIN_CO, APPROVAL_CD, SECURITY_CD)"
            sql = sql & "VALUES (:sessionid, :MOP_CD, :AMT, :DES, :MOP_TP, :FIN_CO, :APPROVAL_CD, :SECURITY_CD)"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":sessionid", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SECURITY_CD", OracleType.VarChar)
            'objSql.Parameters.Add(":EXP_DT", OracleType.VarChar)
            objSql.Parameters.Add(":AMT", OracleType.VarChar)
            objSql.Parameters.Add(":DES", OracleType.VarChar)
            objSql.Parameters.Add(":MOP_TP", OracleType.VarChar)
            objSql.Parameters.Add(":FIN_CO", OracleType.VarChar)
            'objSql.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
            'objSql.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
            objSql.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
            'objSql.Parameters.Add(":DL_STATE", OracleType.VarChar)
            'objSql.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)

            objSql.Parameters(":sessionid").Value = Session.SessionID.ToString.Trim
            objSql.Parameters(":MOP_CD").Value = lbl_pmt_tp_esr.Text
            objSql.Parameters(":SECURITY_CD").Value = txt_fi_sec_cd.Text

            objSql.Parameters(":AMT").Value = CDbl(lbl_pmt_amt.Text)
            objSql.Parameters(":DES").Value = des
            objSql.Parameters(":MOP_TP").Value = bc
            objSql.Parameters(":FIN_CO").Value = cbo_finance_company_esr.SelectedValue
            objSql.Parameters(":APPROVAL_CD").Value = "ESTAR"

            objSql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        ASPxPopupControl12.ShowOnPageLoad = False
        lbl_pmt_tp_esr.Text = ""
        Session("payment") = True
        lbl_pmt_amt.Text = ""
        calculate_total()
        ASPxPopupControl1.ShowOnPageLoad = True
        Response.Redirect("Payment.aspx")

    End Sub
#End Region


    Public Sub mop_drp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles mop_drp.SelectedIndexChanged
        ' Display cash breakdown section
        updated_cash_breakdown(sender, e)
    End Sub

    '<summaryGet dollar types</summary>
    Public Function get_cash_breakdown_dollar_types() As Integer()
        ' Declare dolalr types
        Return (New Integer() {5, 10, 20, 50, 100, 1000})
    End Function

    '<summary>Reset cash breakdown values</summary>
    Public Sub reset_cash_breakdown()
        ' Declare dolalr types
        Dim dollarTypes = get_cash_breakdown_dollar_types()

        ' Hide cash breakdown section
        cashBreakdownSection.Visible = False

        ' Reset coins input values
        txt_cash_breakdown_change.Text = String.Empty
        txt_cash_breakdown_total.Text = String.Empty

        ' Reset coins total value
        txt_cash_breakdown_coins_total.Text = String.Empty

        ' Process all dollar types except 'coins'
        For Each dollarType In dollarTypes
            ' Dollar type id
            Dim dollarTypeTextboxId As String = "txt_cash_breakdown_" & dollarType.ToString

            ' Dollar type total id
            Dim dollarTypeTotalTextboxId As String = dollarTypeTextboxId & "_total"

            ' Dollar type error id
            Dim dollarTypeErrorLabelId As String = dollarTypeTextboxId & "_error"

            ' Find and store dollar type textbox and label controls into variables
            Dim txtboxControl As Control = CashBreakDownPanel.FindControl(dollarTypeTextboxId)
            Dim txtboxTotalControl As Control = CashBreakdownPanel.FindControl(dollarTypeTotalTextboxId)

            ' Process only textbox and label fields
            If TypeOf txtboxControl Is TextBox AndAlso TypeOf txtboxTotalControl Is TextBox Then
                ' Dollar type textbox
                Dim dollarTypeTextbox As TextBox = CType(txtboxControl, TextBox)

                ' Dollar type total textbox
                Dim dollarTypeTotalTextbox As TextBox = CType(txtboxTotalControl, TextBox)

                ' Reset value of each dollar type textbox
                dollarTypeTextbox.Text = String.Empty

                ' Reset value of each dollar type total textbox
                dollarTypeTotalTextbox.Text = String.Empty

                ' Set Total cash value and change to empty
                txt_cash_breakdown_change.Text = String.Empty
                txt_cash_breakdown_total.Text = String.Empty

                ' Hide and cash breakdown change label to default
                cashBreakdownError.Visible = False
                lbl_cash_breakdown_change.Text = Resources.LibResources.Label1018.ToString
                lbl_cash_breakdown_change.ForeColor = System.Drawing.Color.Black
            End If
        Next
    End Sub

    '<summary>Update cash breakdown</summary>
    Public Sub updated_cash_breakdown(sender As Object, e As EventArgs)

        ' Hide cash breakdown section by default
        cashBreakdownSection.Visible = False

        '' Set default sale order amount value
        Dim saleOrderAmount As Double = 0

        ' Set sale order amount if not empty
        If TextBox1.Text.isNotEmpty Then
            saleOrderAmount = CDbl(TextBox1.Text)
        End If

        ' Do not display cash breakdown if sale order amount less than 1
        ' OR method of payment is not "CASH/ARGENT"
        If saleOrderAmount <= 0 OrElse LCase(mop_drp.Value) <> LCase("CS") Then
            Exit Sub
        End If

        ' Display cash breakdown section
        cashBreakdownSection.Visible = True

        ' Declare dollar types
        Dim dollarTypes As Integer() = get_cash_breakdown_dollar_types()

        ' Declare variable to store dollar type value
        Dim parseDollarTypeValue As Integer

        ' Declare variable to store dollar type multiplied value
        Dim parseDollarTypeCalValue As Integer

        ' Declare maximum dollar type digits length
        Dim maxAllowedDigitsLen As Integer = 5

        ' Declare variable to store final total amount
        Dim totalCashAmount As Double = 0

        ' Hide coins error message by default
        cashBreakdownError.Visible = False

        ' Reset border color of coins total textbox
        txt_cash_breakdown_coins_total.Attributes.Add("style", "border-color: inherit")

        ' Process coins value
        If Not String.IsNullOrEmpty(txt_cash_breakdown_coins_total.Text) Then
            ' Declare coins total integer value maximum digits length
            Dim maxCoinsIntegerPartLen As Integer = 4

            ' Declare coins total fraction value maximum digits length
            Dim maxCoinsFractionalPartLen As Integer = 2

            ' Declare coins total parse variable
            Dim coinsTotalValue As Double

            ' Declare variable to store integer and fraction part of coins total
            Dim coinsTotalArr() As String

            ' Declare variable to store integer part of coins total
            Dim coinsTotalIntegerPartValue As Integer = 0

            ' Declare variable to store fraction part of coins total
            ' Declare variale as string type to store exact entered fraction value instead of parse decimal value
            Dim coinsTotalFractionPartValue As String = String.Empty

            ' Declare to store error message
            Dim errorMsg As String = String.Empty

            ' Check if only digits are entered
            If Not (Double.TryParse(txt_cash_breakdown_coins_total.Text, coinsTotalValue)) Then
                errorMsg = "Please enter only digits."

            Else
                ' If comma exists in coins total (french price) then replace all commas with "." to process price later
                If InStr(1, txt_cash_breakdown_coins_total.Text.ToString, ",") > 0 Then
                    coinsTotalValue = CDbl(New StringBuilder(txt_cash_breakdown_coins_total.Text.ToString).
                        Replace(",", ".", 0, txt_cash_breakdown_coins_total.Text.ToString.Length).ToString())
                Else
                    coinsTotalValue = CDbl(txt_cash_breakdown_coins_total.Text)
                End If

                ' Display error message if negative amount entered
                If coinsTotalValue < 0 Then
                    errorMsg = "Negative amount is not allowed."
                End If
            End If

            ' Check if error is not set
            If errorMsg.isEmpty Then
                ' Check if dot(.) exists in coins total amount
                If InStr(1, coinsTotalValue.ToString, ".") Then
                    ' Replace all dots except last one
                    Dim cash_amount_value_parse As String = New StringBuilder(coinsTotalValue.ToString).
                        Replace(".", String.Empty, 0, coinsTotalValue.ToString.LastIndexOf(".")).ToString()

                    ' Split total by comma to get integer and fraction value of coins total
                    coinsTotalArr = Split(cash_amount_value_parse, ".")
                    coinsTotalIntegerPartValue = CInt(coinsTotalArr(0))
                    coinsTotalFractionPartValue = coinsTotalArr(1).ToString

                Else
                    coinsTotalIntegerPartValue = CInt(coinsTotalValue)
                End If

                ' Display error message if entered digits before decimal exceeds maximum allowed digits
                If coinsTotalIntegerPartValue.ToString.Length > maxCoinsIntegerPartLen Then
                    errorMsg = "Exceeds maximum length """ & maxCoinsIntegerPartLen.ToString & """" &
                           " of integer part of total """ & coinsTotalValue.ToString & """."

                ElseIf Not String.IsNullOrEmpty(coinsTotalFractionPartValue) Then
                    ' Display error message if entered digits after decimal exceeds maximum allowed digits
                    If coinsTotalFractionPartValue.Length > maxCoinsFractionalPartLen Then
                        errorMsg = "Exceeds maximum length """ & maxCoinsFractionalPartLen.ToString & """" &
                               " of fraction part of total """ & coinsTotalValue.ToString & """."
                    End If
                End If
            End If

            ' Display error message if exists
            If errorMsg.isNotEmpty Then
                ' Reset cash breakdown change and total amount
                txt_cash_breakdown_change.Text = String.Empty
                txt_cash_breakdown_total.Text = String.Empty

                ' Set error message
                cashBreakdownError.Text = errorMsg
                cashBreakdownError.ForeColor = System.Drawing.Color.Red
                cashBreakdownError.Attributes.Add("style", "display:block")
                cashBreakdownError.Visible = True

                ' Change border color of textbox to red
                txt_cash_breakdown_coins_total.Attributes.Add("style", "border-color: red")
                Exit Sub
            End If

            ' Add coins total to cashbeakdown total amount
            totalCashAmount += coinsTotalValue

        End If

        ' Process all dollar types except 'coins'
        For Each dollarType In dollarTypes
            ' Dollar type id
            Dim dollarTypeTextboxId As String = "txt_cash_breakdown_" & dollarType.ToString

            ' Dollar type total id
            Dim dollarTypeTotalTextboxId As String = dollarTypeTextboxId & "_total"

            ' Find and store dollar type textbox and label controls into variables
            Dim txtboxControl As Control = CashBreakdownPanel.FindControl(dollarTypeTextboxId)
            Dim txtboxTotalControl As Control = CashBreakdownPanel.FindControl(dollarTypeTotalTextboxId)

            ' Process only textbox and label fields
            If TypeOf txtboxControl Is TextBox AndAlso TypeOf txtboxTotalControl Is TextBox Then
                ' Dollar type textbox
                Dim dollarTypeTextbox As TextBox = CType(txtboxControl, TextBox)

                ' Dollar type total textbox
                Dim dollarTypeTotalTextbox As TextBox = CType(txtboxTotalControl, TextBox)

                ' Make dollar type total value empty by default
                dollarTypeTotalTextbox.Text = String.Empty

                ' Reset dollar type textbox border color
                dollarTypeTextbox.Attributes.Add("style", "border-color: inherit")

                If Not String.IsNullOrEmpty(dollarTypeTextbox.Text) Then
                    ' Declare varibale to check if input value can be parsed
                    Dim tryParseVal As Integer = 0

                    ' Declare to store error message
                    Dim errorMsg As String = String.Empty

                    If Not Integer.TryParse(dollarTypeTextbox.Text, tryParseVal) OrElse Not IsNumeric(dollarTypeTextbox.Text) Then
                        ' Display error message if entered value is not numeric
                        errorMsg = "Please enter only digits."

                    ElseIf dollarTypeTextbox.Text.ToString.Length > maxAllowedDigitsLen Then
                        ' Display error message if entered digits exceeds maximum allowed digits
                        errorMsg = "Entered value exceeds the maximum length of """ & maxAllowedDigitsLen & """."

                    ElseIf tryParseVal < 0 Then
                        ' Display error message if entered value is negative
                        errorMsg = "Negative amount is not allowed."
                    End If

                    ' Display error messsage if exists
                    If errorMsg.isNotEmpty Then
                        ' Make cashbreakdown change due value empty
                        txt_cash_breakdown_change.Text = String.Empty

                        ' Make cashbreakdown total value empty
                        txt_cash_breakdown_total.Text = String.Empty

                        ' Make dollar type total value empty
                        dollarTypeTotalTextbox.Text = String.Empty

                        ' Set error message to display
                        cashBreakdownError.Text = errorMsg
                        cashBreakdownError.ForeColor = System.Drawing.Color.Red
                        cashBreakdownError.Visible = True

                        ' Change border color of textbox to red
                        dollarTypeTextbox.Attributes.Add("style", "border-color: red")
                        Exit Sub
                    End If

                    ' Parse dollar type value
                    parseDollarTypeValue = CInt(dollarTypeTextbox.Text)

                    ' Calculate dollar type total value
                    parseDollarTypeCalValue = parseDollarTypeValue * dollarType

                    ' Set total dollar type value
                    dollarTypeTotalTextbox.Text = parseDollarTypeCalValue.ToString()

                    ' Add total dollar value to cashbreakdown total amount
                    totalCashAmount += parseDollarTypeCalValue
                End If
            End If
        Next

        ' Set total of all entered dollar types
        If totalCashAmount > 0 Then
            txt_cash_breakdown_total.Text = FormatNumber(totalCashAmount.ToString, 2)

            ' Check if total cash amount given less than sub total amount
            If totalCashAmount < saleOrderAmount Then
                ' Change label from 'Change Due' to 'Amount Due'
                lbl_cash_breakdown_change.Text = Resources.LibResources.Label1018.ToString
                lbl_cash_breakdown_change.ForeColor = System.Drawing.Color.Black
                txt_cash_breakdown_change.Text = FormatNumber((saleOrderAmount - totalCashAmount).ToString, 2)
            Else
                lbl_cash_breakdown_change.Text = Resources.LibResources.Label1016.ToString
                lbl_cash_breakdown_change.ForeColor = System.Drawing.Color.Red
                txt_cash_breakdown_change.Text = FormatNumber((totalCashAmount - saleOrderAmount).ToString, 2)
            End If
        Else
            ' Set Total cash value and change to empty
            txt_cash_breakdown_change.Text = String.Empty
            txt_cash_breakdown_total.Text = String.Empty

            ' Change label to default
            lbl_cash_breakdown_change.Text = Resources.LibResources.Label1018.ToString
            lbl_cash_breakdown_change.ForeColor = System.Drawing.Color.Black
        End If
    End Sub
End Class
