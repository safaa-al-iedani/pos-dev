<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="LeonsCustom.aspx.vb" Inherits="LeonsCustom" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Style="position: relative"
            BorderColor="White" PagerSettings-Visible="false"
            CaptionAlign="Left" GridLines="None" CellSpacing="1" ShowHeader="false" CellPadding="4">
            <PagerSettings Visible="False"></PagerSettings>
            <Columns>
                <asp:HyperLinkField headertext="Program Name"
                    datatextfield="PRG_NM" DataNavigateUrlFields="PRG_NM">
                 </asp:HyperLinkField>
                <asp:BoundField DataField="SUB_SYS" SortExpression="SUBSYS" HeaderText="Group"></asp:BoundField>
                <asp:BoundField DataField="DES" SortExpression="Description" HeaderText="Description"></asp:BoundField>
                <asp:BoundField DataField="LINK_EXT_FLAG" SortExpression="Link" HeaderText="Link" Visible="false"></asp:BoundField>                
            </Columns>
             <HeaderStyle Font-Bold="True" ForeColor="Black" />
        </asp:GridView>
     <br />    
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" Style="position: relative"
            BorderColor="White" PagerSettings-Visible="false"
            CaptionAlign="Left" GridLines="None" CellSpacing="1" ShowHeader="false" CellPadding="4">
            <PagerSettings Visible="False"></PagerSettings>
            <Columns>
                <asp:HyperLinkField headertext="Program Name"
                    datatextfield="DES" DataNavigateUrlFields="PRG_NM" Target="_blank"> 
                 </asp:HyperLinkField>
                <asp:BoundField DataField="SUB_SYS" SortExpression="SUBSYS" HeaderText="Group"></asp:BoundField>
                <asp:BoundField DataField="DES" SortExpression="Description" HeaderText="Description"></asp:BoundField>
                <asp:BoundField DataField="LINK_EXT_FLAG" SortExpression="Link" HeaderText="Link" Visible="false"></asp:BoundField>               
            </Columns>
             <HeaderStyle Font-Bold="True" ForeColor="Black" />
        </asp:GridView>
     <br />

    <%--<a href="Campaign_main.aspx" class="style5">Launch Campaign</a>
    <br />
    <br />--%>
     <div id="divError" runat="server">
        <asp:label ID="ASPlblError" runat="server" Text=""  ForeColor ="Red">
                    </asp:label>
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
