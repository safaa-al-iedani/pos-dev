<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ADS_Test.aspx.vb" Inherits="ADS_Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style.css" type="text/css" rel="stylesheet">
    <title>Untitled Page</title>
</head>
<body class="style5">
    <form id="form1" runat="server">
        <div width="100%" align="center">
            <br />
            <br />
            <img src="images/ads.gif" />
            <br />
            <b>Test System</b>
            <table class="style5">
                <tr>
                    <td align="left">
                        Card Number:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label1" runat="server" Text="Exp Date: "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="TextBox2" runat="server" MaxLength="2" Width="17px" CssClass="style5"></asp:TextBox>
                        /&nbsp;<asp:TextBox ID="TextBox3" runat="server" MaxLength="2" Width="17px" CssClass="style5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Amount:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txt_amt" runat="server" Width="194px" CssClass="style5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Swiped?:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txt_swiped" runat="server" AutoPostBack="false" Enabled="false"
                            Width="10px">N</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Return/Sale:
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="cbo_sale_or_ret" runat="server" CssClass="style5">
                            <asp:ListItem Value="SAL">Sale</asp:ListItem>
                            <asp:ListItem Value="RET">Return</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lbl_Full_Name" runat="server" Width="286px"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="100px" CssClass="style5" />&nbsp;
                        <asp:Button ID="btn_Clear" runat="server" Text="Clear" Width="100px" CssClass="style5" /></td>
                </tr>
            </table>
            <br />
            <br />
            <b><u>Responses:</u></b>
            <table class="style5">
                <tr>
                    <td align="left" width="50%">
                        <asp:Label ID="Label4" runat="server" Text="Return Code:"></asp:Label>
                    </td>
                    <td align="left" width="50%">
                        <asp:Label ID="lbl_returncode" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label6" runat="server" Text="Authorization Code:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_authcode" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label8" runat="server" Text="Reason Code:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_reasoncode" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label2" runat="server" Text="Correlation Data:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_Correlation" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label3" runat="server" Text="eTail ID:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_etailID" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label5" runat="server" Text="Open to Buy:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_otb" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label7" runat="server" Text="Balance Amount:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_balanceamount" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label9" runat="server" Text="Down Payment:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_downpayment" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label10" runat="server" Text="Match Number:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_matchnumber" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label11" runat="server" Text="AVS Response:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_avsresponse" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label12" runat="server" Text="Store Number:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_storenumber" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
