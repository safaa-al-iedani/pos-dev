Imports HBCG_Utils

Partial Class comments
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' lucy_btn_cmnt.Visible = True   'lucy 

        btnSubmit.Enabled = False
        Session("comments") = "TRUE"

        'mariam -Oct 4,2016-Price Change Approval
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            If Request("LEAD") <> "TRUE" Then
                Response.Redirect("order_detail.aspx")
            Else
                Response.Redirect("order_detail.aspx?LEAD=TRUE")
            End If
        End If
        'mariam -end

        If Request("LEAD") = "TRUE" Then
            txtDeliveryComments.Visible = False
            lblDeliveryComments.Visible = False
            txtARComments.Visible = False
            lblARComments.Visible = False
            'Daniela french
            'lblSalesComments.Text = "Quote Comments (Will print on quote):"
            lblSalesComments.Text = Resources.LibResources.Label734
            'lucy_btn_cmnt.Visible = False   'lucy 
            'txtSalesComments. = 7500
        End If
        If Not IsPostBack Then

            ' txtSalesComments.Focus() 'lucy
            txtARComments.Text = Session("arcomments")
            txtSalesComments.Text = Session("scomments")
            txtDeliveryComments.Text = Session("dcomments")
            If Request("swatch") = "TRUE" Then
                ASPxPopupControl1.ShowOnPageLoad = True
            End If

            ' Daniela calculate total only one time 
            calculate_total(Page.Master)
        End If
        'calculate_total(Page.Master)

    End Sub

    Protected Sub lucy_add_predined_cmnt(sender As Object, e As EventArgs) Handles lucy_btn_cmnt.Click
        Response.Redirect("pdcm.aspx")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Session("scomments") = txtSalesComments.Text
        Session("arcomments") = txtARComments.Text
        Session("dcomments") = txtDeliveryComments.Text

    End Sub

    Protected Sub AR_Changed(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("arcomments") = txtARComments.Text
        Dim sm As ScriptManager = ScriptManager.GetCurrent(Me)
        sm.SetFocus(txtSalesComments)

    End Sub

    Protected Sub Delivery_Changed(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("dcomments") = txtDeliveryComments.Text
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtARComments.ClientID + "').focus();", True)
        Dim sm As ScriptManager = ScriptManager.GetCurrent(Me)
        sm.SetFocus(txtARComments)

    End Sub

    Protected Sub Sales_Changed(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("scomments") = txtSalesComments.Text
        If Request("LEAD") <> "TRUE" Then
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "selectAndFocus", "$get('" + txtDeliveryComments.ClientID + "').focus();", True)
            Dim sm As ScriptManager = ScriptManager.GetCurrent(Me)
            sm.SetFocus(txtDeliveryComments)
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "Regular.Master"
        End If
        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub
End Class
