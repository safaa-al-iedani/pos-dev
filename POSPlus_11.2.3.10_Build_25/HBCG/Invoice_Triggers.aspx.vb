Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Invoice_Triggers
    Inherits POSBasePage

    Protected Sub btn_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click

        Dim Error_Found As Boolean = False
        Select Case cbo_trigger_types.SelectedValue
            Case "P_D"
                If txt_value.Text = "P" Or txt_value.Text = "D" Then
                    '
                Else
                    Error_Found = True
                    lbl_warnings.Text = "Valid entries are ""P"" or ""D"""
                End If
            Case "DROP_DT"
                If Not IsDate(txt_value.Text) Then
                    lbl_warnings.Text = "An invalid date has been entered for the Drop Date"
                    Error_Found = True
                End If
            Case "ITM_TP_CD"
                If Len(txt_value.Text) > 3 Then
                    lbl_warnings.Text = "Item Type Codes must be 3 characters or less"
                    Error_Found = True
                ElseIf String.IsNullOrEmpty(txt_value.Text) Then
                    lbl_warnings.Text = "Item Type Codes must be 3 characters or less"
                    Error_Found = True
                End If
            Case "MAJOR_CD"
                If Len(txt_value.Text) > 5 Then
                    lbl_warnings.Text = "Major Codes must be 5 characters or less"
                    Error_Found = True
                ElseIf String.IsNullOrEmpty(txt_value.Text) Then
                    lbl_warnings.Text = "Major Codes must be 5 characters or less"
                    Error_Found = True
                End If
            Case "MINOR_CD"
                If Len(txt_value.Text) > 5 Then
                    lbl_warnings.Text = "Minor Codes must be 5 characters or less"
                    Error_Found = True
                ElseIf String.IsNullOrEmpty(txt_value.Text) Then
                    lbl_warnings.Text = "Minor Codes must be 5 characters or less"
                    Error_Found = True
                End If
            Case "ITM_CD"
                If Len(txt_value.Text) > 9 Then
                    lbl_warnings.Text = "Item Codes must be 9 characters or less"
                    Error_Found = True
                ElseIf String.IsNullOrEmpty(txt_value.Text) Then
                    lbl_warnings.Text = "Item Codes must be 9 characters or less"
                    Error_Found = True
                End If
            Case "STORE_CD"
                If Len(txt_value.Text) > 2 Then
                    lbl_warnings.Text = "Store Codes must be 2 characters or less"
                    Error_Found = True
                ElseIf String.IsNullOrEmpty(txt_value.Text) Then
                    lbl_warnings.Text = "Store Codes must be 2 characters or less"
                    Error_Found = True
                End If
            Case "ORD_TP_CD"
                If Len(txt_value.Text) > 3 Then
                    lbl_warnings.Text = "Order Type Codes must be 3 characters or less"
                    Error_Found = True
                End If
        End Select

        If Len(txt_trigger_text.Text) > 2000 Then
            Error_Found = True
            lbl_warnings.Text = "Triggers are limited to 2,000 characters"
        End If

        If Error_Found = False Then
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim sql As String

            conn.Open()

            sql = "INSERT INTO INVOICE_TRIGGERS (TRIGGER_FIELD,TRIGGER_EQ,TRIGGER_VALUE,TRIGGER_RESPONSE) "
            sql = sql & "VALUES(:TFIELD, :TEQ, :TVALUE, :TRESPONSE)"

            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

            objSql.Parameters.Add(":TFIELD", OracleType.VarChar)
            objSql.Parameters.Add(":TEQ", OracleType.VarChar)
            objSql.Parameters.Add(":TVALUE", OracleType.VarChar)
            objSql.Parameters.Add(":TRESPONSE", OracleType.VarChar)

            objSql.Parameters(":TFIELD").Value = cbo_trigger_types.SelectedValue
            objSql.Parameters(":TEQ").Value = cbo_sequence.SelectedValue
            objSql.Parameters(":TVALUE").Value = txt_value.Text
            objSql.Parameters(":TRESPONSE").Value = Left(txt_trigger_text.Text, 2000)

            objSql.ExecuteNonQuery()

            objSql.Dispose()
            conn.Close()

            cbo_trigger_types.SelectedIndex = 0
            cbo_sequence.SelectedIndex = 0
            txt_value.Text = ""
            txt_trigger_text.Text = ""
            If InStr(Session("sort_order"), "asc") > 0 Then
                Session("sort_order") = Replace(Session("sort_order"), "asc", "desc")
            Else
                Session("sort_order") = Replace(Session("sort_order"), "desc", "asc")
            End If
            BindData(SortOrder(Session("SortOrder"))) 'Rebind our DataGrid

        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            admin_form.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If
        lbl_warnings.Text = ""
        If Not IsPostBack Then
            'Set up default column sorting   
            If IsNothing(Session("SortOrder")) Or IsDBNull(Session("SortOrder")) Then
                BindData("trigger_eq asc")
            Else
                BindData(Session("SortOrder"))
            End If
        End If

    End Sub

    Sub MyDataGrid_Page(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)

        MyDataGrid.CurrentPageIndex = e.NewPageIndex

    End Sub

    'Public Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

    '    Dim conn As New OracleConnection
    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As OracleDataReader

    '    conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '    conn.Open()

    '    sql = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

    '    'Set SQL OBJECT 
    '    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

    '    Try
    '        MyDataReader = objSql.ExecuteReader
    '        If (MyDataReader.Read()) Then
    '            If MyDataReader.Item("EMP_CD") & "" <> "" Then
    '                Authorize_User = True
    '            Else
    '                Authorize_User = False
    '            End If
    '        Else
    '            Authorize_User = False
    '        End If
    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try

    'End Function

    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder") = SortOrder

    End Function

    Sub MyDataGrid_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        MyDataGrid.CurrentPageIndex = 0 'To sort from top
        BindData(SortOrder(E.SortExpression).ToString()) 'Rebind our DataGrid

    End Sub

    Sub BindData(ByVal SortField As String)

        Dim objConnect As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim myDataAdapter As OracleDataAdapter
        Dim myCommand As OracleCommand = DisposablesManager.BuildOracleCommand


        'Setup Session Cache for different users         
        Dim Source As New DataView
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer

        MyDataGrid.Visible = True
        'If (IsNothing(Source)) Then
        sql = "SELECT TRIGGER_ID, TRIGGER_FIELD, TO_NUMBER(REPLACE(TRIGGER_EQ,'=','1')) AS TRIGGER_EQ, TRIGGER_VALUE, TRIGGER_RESPONSE FROM INVOICE_TRIGGERS ORDER BY TO_NUMBER(REPLACE(TRIGGER_EQ,'=','1')) ASC"

        sql = UCase(sql)

        myDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        ds = New DataSet()
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        myDataAdapter.Fill(ds, "MyDataGrid")

        'Assign sort expression to Session              
        Session("SortOrder") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        'Insert DataView into Session           
        Session("dgCache") = Source

        'End If

        If numrows = 0 Then
            MyDataGrid.Visible = False
        Else
            Source.Sort = SortField
            MyDataGrid.DataSource = Source
            MyDataGrid.DataBind()
        End If
        'Close connection           
        objConnect.Close()

    End Sub

    Protected Sub MyDataGrid_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles MyDataGrid.ItemCommand

        Dim Trigger_Text As TextBox
        Dim Trigger_Seq As DropDownList
        Dim trigger_id As String = e.Item.Cells(0).Text
        Trigger_Text = e.Item.FindControl("txt_response")
        Trigger_Seq = e.Item.FindControl("cbo_grid_sequence")
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String

        conn.Open()

        Select Case e.CommandName
            Case "Delete"
                sql = "DELETE FROM INVOICE_TRIGGERS WHERE TRIGGER_ID=:TRIGGER_ID"

                Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":TRIGGER_ID", OracleType.VarChar)
                objSql.Parameters(":TRIGGER_ID").Value = trigger_id

                objSql.ExecuteNonQuery()
                objSql.Dispose()

                BindData(SortOrder(Session("SortOrder"))) 'Rebind our DataGrid
            Case "Save"
                sql = "UPDATE INVOICE_TRIGGERS SET TRIGGER_RESPONSE=:TRIGGER_RESPONSE, TRIGGER_EQ=:TRIGGER_EQ WHERE TRIGGER_ID='" & trigger_id & "'"

                Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql.Parameters.Add(":TRIGGER_RESPONSE", OracleType.VarChar)
                objSql.Parameters(":TRIGGER_RESPONSE").Value = Left(Trigger_Text.Text, 2000)
                objSql.Parameters.Add(":TRIGGER_EQ", OracleType.VarChar)
                objSql.Parameters(":TRIGGER_EQ").Value = Trigger_Seq.SelectedValue

                objSql.ExecuteNonQuery()
                objSql.Dispose()

                BindData(SortOrder(Session("SortOrder"))) 'Rebind our DataGrid
        End Select

        conn.Close()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub MyDataGrid_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles MyDataGrid.ItemDataBound

        Dim sequence As DropDownList = CType(e.Item.FindControl("cbo_grid_sequence"), DropDownList)
        If Not IsNothing(sequence) Then
            If IsNumeric(e.Item.Cells(2).Text) Then
                sequence.SelectedValue = e.Item.Cells(2).Text
            Else
                sequence.SelectedValue = "1"
            End If
        End If

    End Sub
End Class
