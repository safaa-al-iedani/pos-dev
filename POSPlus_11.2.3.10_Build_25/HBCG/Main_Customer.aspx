<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Main_Customer.aspx.vb" Inherits="Main_Customer" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label132 %>">
                </dx:ASPxLabel>
            </td>
            <td colspan="3" style="width: 338px">
                <asp:TextBox ID="txt_Cust_cd" runat="server" Style="position: relative" CssClass="style5"></asp:TextBox>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label110 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_corp_name" runat="server" Style="position: relative" CssClass="style5"
                    Width="320px"></asp:TextBox>&nbsp;</td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label229 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_h_phone" runat="server" Width="80px" CssClass="style5" MaxLength="12"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label211 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_Fname" runat="server" Style="position: relative" CssClass="style5"
                    MaxLength="15"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="<%$ Resources:LibResources, Label633 %>">
                </dx:ASPxLabel>
                &nbsp;&nbsp;&nbsp;<asp:DropDownList
                        ID="cbo_cust_tp" Style="position: relative;" runat="server" CssClass="style5">
                        <asp:ListItem Selected="True" Value="I" Text="<%$ Resources:LibResources, Label236 %>"></asp:ListItem>
                        <asp:ListItem Value="C" Text="<%$ Resources:LibResources, Label112 %>"></asp:ListItem>
                    </asp:DropDownList>
            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label49 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_b_phone" runat="server" Style="left: 0px; position: relative"
                    Width="80px" CssClass="style5" MaxLength="12"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:LibResources, Label263 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:TextBox ID="txt_Lname" runat="server" Style="position: relative" CssClass="style5"
                    MaxLength="20"></asp:TextBox></td>
            <td>
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="<%$ Resources:LibResources, Label195 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_Ext" runat="server" Style="left: 0px; position: relative" Width="40px"
                    CssClass="style5" MaxLength="4"></asp:TextBox></td>
        </tr>
    </table>
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" Width="99%" ActiveTabIndex="0"
        AutoPostBack="True" EnableCallBacks="False">
        <TabPages>
            <dx:TabPage Text="<%$ Resources:LibResources, Label135 %>">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table width="100%" class="style5">
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="<%$ Resources:LibResources, Label20 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_addr1" runat="server" Style="position: relative" Width="320px"
                                        CssClass="style5" MaxLength="30"></asp:TextBox></td>
                                <td colspan="2" rowspan="9" valign="top" align="left" bgcolor="beige" bordercolor="#000000">
                                    <asp:Label ID="lbl_summary" runat="server" CssClass="style5"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label21 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_addr2" runat="server" Style="position: relative" Width="320px"
                                        CssClass="style5" MaxLength="30"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:LibResources, Label80 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_City" runat="server" Style="position: relative; left: 0px;"
                                        CssClass="style5" MaxLength="20" Width="122px"></asp:TextBox>
                                    &nbsp;
                                    <asp:DropDownList ID="cbo_State" Width="100px" runat="server" CssClass="style5">
                                    </asp:DropDownList>&nbsp;<asp:TextBox ID="txt_Zip" runat="server" Style="position: relative"
                                        Width="58px" CssClass="style5" MaxLength="10" AutoPostBack="True" OnTextChanged="txt_Zip_TextChanged"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="<%$ Resources:LibResources, Label115 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_county" runat="server" Style="left: 0px; position: relative"
                                        CssClass="style5" MaxLength="20"></asp:TextBox>
                                    /
                                    <asp:TextBox ID="txt_country" runat="server" Style="left: 0px; position: relative"
                                        CssClass="style5" MaxLength="20" Width="153px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="<%$ Resources:LibResources, Label179 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:TextBox ID="txt_Email" runat="server" Style="position: relative" Width="320px"
                                        CssClass="style5" MaxLength="60"></asp:TextBox></td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="<%$ Resources:LibResources, Label19 %>">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table width="100%" class="style5">
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="<%$ Resources:LibResources, Label155 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:DropDownList ID="cbo_Zone_cd" runat="server" Style="position: relative" Width="224px"
                                        AppendDataBoundItems="true" CssClass="style5">
                                        <asp:ListItem Selected="True">
                                        </asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="<%$ Resources:LibResources, Label539 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:DropDownList ID="cbo_se_zone" runat="server" Style="position: relative" Width="224px"
                                        AppendDataBoundItems="true" CssClass="style5">
                                        <asp:ListItem Selected="True">
                                        </asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="<%$ Resources:LibResources, Label560 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td style="width: 338px">
                                    <asp:DropDownList ID="cbo_srt_cd" runat="server" Style="position: relative" Width="224px"
                                        AppendDataBoundItems="true" CssClass="style5">
                                        <asp:ListItem Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 109px">
                                    <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="<%$ Resources:LibResources, Label701 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td nowrap style="width: 338px">
                                    <asp:DropDownList ID="cbo_tet_cd" runat="server" Style="position: relative" Width="224px"
                                        AppendDataBoundItems="true" AutoPostBack="true" CssClass="style5">
                                        <asp:ListItem Selected="True"></asp:ListItem>
                                    </asp:DropDownList>&nbsp;
                                    <asp:TextBox ID="txt_tax_exempt_cd" runat="server" Style="left: 0px; position: relative"
                                        Width="88px" CssClass="style5"></asp:TextBox>
                                </td>
                            </tr><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label701 %>" />
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="<%$ Resources:LibResources, Label441 %>">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div class="style5" width="100%">
                            <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="<%$ Resources:LibResources, Label6 %>">
                            </dx:ASPxLabel>
                            &nbsp; &nbsp;<asp:TextBox ID="txt_acct_balance" runat="server" ReadOnly="True" CssClass="style5"
                                Width="102px" Text="$0.00"></asp:TextBox>
                            <br />
                            <br />
                            <asp:DataGrid ID="DataGrid1" runat="server" AllowSorting="True" AlternatingItemStyle-BackColor="Beige"
                                AutoGenerateColumns="False" CellPadding="3" DataKeyField="ar_trn_pk" Height="16px"
                                OnSortCommand="MyDataGrid_Sort" PagerStyle-HorizontalAlign="Right" PagerStyle-Mode="NumericPages"
                                Width="99%" CssClass="style5">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label249 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="style5"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="post_dt" HeaderText="<%$ Resources:LibResources, Label143 %>" SortExpression="post_dt asc"
                                        DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TRN_TP_CD" HeaderText="<%$ Resources:LibResources, Label624 %>" SortExpression="trn_tp_cd asc">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="STAT_CD" HeaderText="<%$ Resources:LibResources, Label571 %>" SortExpression="stat_cd asc">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="mop_cd" HeaderText="<%$ Resources:LibResources, Label322 %>" SortExpression="mop_cd asc"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="amt" DataFormatString="{0:N2}" HeaderText="<%$ Resources:LibResources, Label28 %>" SortExpression="amt asc">
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label44 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_balance" runat="server" CssClass="style5" Text="0"></asp:Label><br />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ivc_cd" HeaderText="" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Mode="NumericPages" />
                                <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" ForeColor="Black" CssClass="style5" />
                            </asp:DataGrid>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="<%$ Resources:LibResources, Label448 %>">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl1" runat="server">
                        <div class="style5" width="100%">
                            <dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="<%$ Resources:LibResources, Label449 %>">
                            </dx:ASPxLabel>
                            &nbsp; &nbsp;<asp:TextBox ID="txt_quote_summary" runat="server" ReadOnly="True" CssClass="style5"
                                Width="102px" Text="$0.00"></asp:TextBox>
                            <br />
                            <br />
                            <asp:DataGrid ID="GridView2" runat="server" AllowSorting="True" AlternatingItemStyle-BackColor="Beige"
                                AutoGenerateColumns="False" OnSortCommand="MyDataGrid2_Sort" CellPadding="3"
                                DataKeyField="REL_NO" Height="16px" Width="99%" CssClass="style5">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label459 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="style5"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="WR_DT" DataFormatString="{0:MM/dd/yyyy}" HeaderText="<%$ Resources:LibResources, Label668 %>"
                                        SortExpression="WR_DT asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="FOLLOW_UP_DT" DataFormatString="{0:MM/dd/yyyy}" HeaderText="<%$ Resources:LibResources, Label212 %>"
                                        SortExpression="FOLLOW_UP_DT asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PROSPECT_TOTAL" DataFormatString="{0:c}" HeaderText="Total"
                                        SortExpression="PROSPECT_TOTAL asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="STORE_CD" HeaderText="<%$ Resources:LibResources, Label572 %>" SortExpression="STORE_CD asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="REL_STATUS" HeaderText="<%$ Resources:LibResources, Label571 %>" SortExpression="REL_STATUS asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="REL_NO" HeaderText="<%$ Resources:LibResources, Label459 %>" SortExpression="REL_NO asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                </Columns>
                                <HeaderStyle Font-Bold="True" ForeColor="Black" />
                            </asp:DataGrid>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="<%$ Resources:LibResources, Label95 %>">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl2" runat="server">
                        <table width="100%" height="100%" class="style5">
                            <tr>
                                <td valign="top" align="left" width="50%">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/comments.gif" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txt_cust_comments" runat="server" Height="69px" Rows="3" TextMode="MultiLine"
                                        Width="624px" CssClass="style5"></asp:TextBox><br />
                                    <br />
                                    <asp:TextBox ID="txt_cust_past" runat="server" Height="75px" Rows="3" TextMode="MultiLine"
                                        Width="624px" CssClass="style5"></asp:TextBox>
                                    <br />
                                    <br />
                                    <asp:Button ID="btn_save_comments" runat="server" Text="<%$ Resources:LibResources, Label521 %>" Width="119px"
                                        CssClass="style5" />
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
    <br />
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label275 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_Submit" runat="server" Text="<%$ Resources:LibResources, Label702 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label83 %>">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
    <dx:ASPxLabel ID="lbl_Cust_Info" Font-Bold="True" ForeColor="Red" runat="server"
        Text="ASPxLabel" Width="100%">
    </dx:ASPxLabel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="<%$ Resources:LibResources, Label134 %>">
    </dx:ASPxLabel>
</asp:Content>
