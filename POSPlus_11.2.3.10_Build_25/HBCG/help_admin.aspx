<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="help_admin.aspx.vb" Inherits="help_admin" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frmsubmit" runat="server">
        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
        <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server"
            AutoGenerateColumns="False" CellPadding="2" DataKeyField="help_id" Font-Bold="False"
            Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
            Height="157px" Width="681px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left"
            OnDeleteCommand="DoItemSave">
            <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
            <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                Font-Bold="True" Height="20px" HorizontalAlign="Left"></HeaderStyle>
            <Columns>
                <asp:BoundColumn DataField="html_file" HeaderText="HTML Help File" ReadOnly="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Page Description">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" VerticalAlign="Top" />
                    <ItemTemplate>
                        <asp:TextBox ID="txt_desc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.des") %>'
                            Width="300px" MaxLength="100" CssClass="style5"></asp:TextBox>
                    </ItemTemplate>
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Edit Help File">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" VerticalAlign="Top" />
                    <ItemTemplate>
                        <asp:Button ID="Button1" runat="server" CssClass="style5" Text="Modify Help File" />
                    </ItemTemplate>
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="help_id" HeaderText="ID" Visible="False"></asp:BoundColumn>
                <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Save24.gif' border='0'&gt;">
                    <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" VerticalAlign="Top" CssClass="style5" />
                </asp:ButtonColumn>
            </Columns>
            <ItemStyle VerticalAlign="Top" />
        </asp:DataGrid><br />
        <dx:ASPxHtmlEditor ID="WebHtmlEditor1" runat="server" Visible="False">
        </dx:ASPxHtmlEditor>
        <br />
        <asp:Button ID="btn_save" runat="server" CssClass="style5" Text="Save Changes" />
        <asp:Button ID="btn_cancel" runat="server" CssClass="style5" Text="Cancel Changes" />
        <asp:Label ID="lbl_help_id" runat="server" Visible="False"></asp:Label></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
