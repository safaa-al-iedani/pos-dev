<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Franchise.aspx.vb" Inherits="Franchise" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%--<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
   <table style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr>
            <td style="width: 150px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                     <asp:DropDownList ID="txt_store_cd" Width="335px" runat="server" Style="position: relative" AppendDataBoundItems="true">
                     </asp:DropDownList>
             </td>
             <td style="width: 100px"></td>
             <td style="width: 200px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" meta:resourcekey="Label2">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_co_cd" runat="server" Width="80px" MaxLength="12"></asp:TextBox>
            </td> 
        </tr>
        <tr>
            <td style="width: 150px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" meta:resourcekey="Label3">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
               <asp:DropDownList ID="txt_whse_store_cd" Width="335px" runat="server" Style="position: relative" AppendDataBoundItems="true">
                </asp:DropDownList>
            </td>
            <td style="width: 100px"></td>
            <td style="width: 200px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" meta:resourcekey="Label4">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_whse_co_cd" runat="server" Width="80px" MaxLength="12"></asp:TextBox>
               
            </td> 
        </tr>
        <tr>
            <td style="width: 150px">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" meta:resourcekey="Label5">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:DropDownList ID="txt_tax_cd" runat="server" Width="335px" Style="position: relative" AppendDataBoundItems="true">
                </asp:DropDownList>
            </td>  
            
           
        </tr>
        
    </table>
    <dx:ASPxLabel ID="lbl_wr_dt" runat="server" Text="" Visible="false">
    </dx:ASPxLabel>
    <dx:ASPxLabel ID="lbl_seq_num" runat="server" Text="" Visible="false">
    </dx:ASPxLabel>
    <br />
    
    <dx:ASPxGridView ID="GridView2" runat="server" AutoGenerateColumns="False" Width="98%" KeyFieldName="STORE_CD;WHSE_STORE_CD"  ClientInstanceName="GridView2" OnBatchUpdate="GridView2_BatchUpdate" OnInitNewRow="GridView2_InitNewRow" oncelleditorinitialize="GridView2_CellEditorInitialize" OnParseValue="GridView2_ParseValue">
    <%--<dx:ASPxGridView ID="GridView2" runat="server" AutoGenerateColumns="False" Width="98%" KeyFieldName="STORE_CD;WHSE_STORE_CD"  ClientInstanceName="GridView2" OnBatchUpdate="GridView2_BatchUpdate" OnInitNewRow="GridView2_InitNewRow" oncelleditorinitialize="GridView2_CellEditorInitialize"> --%>

         <SettingsEditing Mode="Batch" />
         <SettingsCommandButton>
         <UpdateButton Text="Update"></UpdateButton>
        <CancelButton Text="Cancel"></CancelButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewDataTextColumn Caption="Store Cd" FieldName="STORE_CD" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit MaxLength="2">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="Please enter the Store Code"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Store #" FieldName="STORE_NUM" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit MaxLength="2" >
                    <%--<ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="Please enter the Franchise Store #"/>
                    </ValidationSettings>--%>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Cust Cd" FieldName="CUST_CD" VisibleIndex="2">
                <PropertiesTextEdit MaxLength="10" Width="80px">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="Please enter the Customer Code"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="&nbsp&nbsp&nbspTax&nbsp&nbsp&nbsp" FieldName="TAX_CD" VisibleIndex="3" >
                <PropertiesTextEdit MaxLength="3" Width="50px">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="Please enter the Tax Code"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Whse Store Cd" FieldName="WHSE_STORE_CD" VisibleIndex="4">
                <PropertiesTextEdit MaxLength="2">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="Please enter the Whse Store Code"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Furn PCT" FieldName="FURN_PCT" VisibleIndex="5">
           </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Appl PCT" FieldName="APPL_PCT" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Elec PCT" FieldName="ELEC_PCT" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Matt PCT" FieldName="MATT_PCT" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Tra Furn PCT" FieldName="FURN_TRANS_PCT" VisibleIndex="9">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Tra Appl PCT" FieldName="APPL_TRANS_PCT" VisibleIndex="10">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Tra Elec PCT" FieldName="ELEC_TRANS_PCT" VisibleIndex="11">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Tra Matt PCT" FieldName="MATT_TRANS_PCT" VisibleIndex="12">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="QST" FieldName="QST" VisibleIndex="13">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Excpt" FieldName="EXCP" VisibleIndex="14">
                <PropertiesTextEdit MaxLength="1">                    
                </PropertiesTextEdit>               
            </dx:GridViewDataTextColumn>            
            <dx:GridViewCommandColumn ShowNewButtonInHeader="True" ShowDeleteButton="True" VisibleIndex="15"/>
          <%-- <dx:GridViewDataTextColumn Caption="SO#" VisibleIndex="15">
                <DataItemTemplate>
                    <dx:ASPxLabel ID="lbl_so" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>  --%>           
        </Columns>
         <Settings ShowFooter="true" />
          <Templates>
                <FooterRow>
                    <dx:ASPxLabel ID="GV_Footer_label" runat="server" ClientInstanceName="GV_Footer_label" Text="TEST" OnInit="GV_Footer_label_init" ForeColor="Red"></dx:ASPxLabel>
                </FooterRow>
            </Templates>

        <SettingsBehavior AllowSort="False" />
        <SettingsText ConfirmDelete="Are you sure you wish to delete this row?" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
         <SettingsBehavior ConfirmDelete="true"    />
        <SettingsText  ConfirmDelete="Please confirm and then click Update to premanently delete the record." />
    </dx:ASPxGridView>
    <br />
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="Search">
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;
            </td>
          <%--  <td>
                <dx:ASPxButton ID="btn_Save" runat="server" Text="Save">
                </dx:ASPxButton>
            </td> --%>
            <td>
                &nbsp;
            </td> 
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="Clear Screen">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
   <%-- <dx:ASPxLabel ID="lbl_Cust_Info" Font-Bold="True" ForeColor="Red" runat="server"
        Text="" Width="100%"> 
    </dx:ASPxLabel>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
