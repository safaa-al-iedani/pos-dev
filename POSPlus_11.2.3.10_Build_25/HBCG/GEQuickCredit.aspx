<%@ Page Language="VB" AutoEventWireup="false" CodeFile="GEQuickCredit.aspx.vb" Inherits="GEQuickCredit" %>

<%@ Register Assembly="DevExpress.XtraReports.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body class="style5" leftmargin="30" topmargin="30">
    <form id="form1" runat="server">
        <div style="margin-top: 30px; margin-left: 30px">
            <img src="images/ge_money_logo_US.gif" />&nbsp;<br />
            <br />
             &nbsp;Enter Customer:
            <asp:TextBox ID="txt_cust_cd" runat="server" CssClass="style5" MaxLength="12" TabIndex="4" Width="93px"></asp:TextBox>&nbsp;&nbsp;
            <asp:Button ID="btn_lookup" runat="server" Text="Lookup Customer" Width="144px" CssClass="style5" CausesValidation="False" />
            <br />
            <br />
            &nbsp;<asp:Button ID="btn_payment" runat="server" Text="Return to Payment Screen" Width="191px" CssClass="style5" CausesValidation="False" />
            &nbsp;
            <asp:Button ID="btn_reset" runat="server" Text="Reset Application" Width="145px" CssClass="style5" CausesValidation="False" />
            <br /> <br />
            &nbsp;Requested Amt:
            <asp:TextBox ID="txt_req_amt" runat="server" CssClass="style5" MaxLength="6" TabIndex="4"  Width="93px"></asp:TextBox>
             <br /><br />
            &nbsp;Auth Code:
            <asp:TextBox ID="txt_auth" runat="server" CssClass="style5" MaxLength="6"  TabIndex="4" Width="122px"></asp:TextBox>
            <br /><br />
            <table style="position: relative; top: 0px" class="style5" runat="server" id="init_app">
                <tr>
                    <td nowrap style="width: 120px">
                        &nbsp;
                    </td>
                    <td colspan="3" style="width: 165px; text-align: left;">
                        <span style="color: #000000">Primary Applicant </span>
                    </td>
                </tr>
                <tr>
                    <td nowrap style="width: 120px">
                        First Name:
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_fname" runat="server" Width="129px" CssClass="style5" MaxLength="30"
                            TabIndex="1" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredFNameFieldValidator" runat="server" ErrorMessage="First Name is required" ControlToValidate="txt_fname"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td nowrap style="width: 120px">
                        Middle Init:
                    </td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_middle_init" runat="server" Width="13px" CssClass="style5" MaxLength="30" 
                            TabIndex="1" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Last Name:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_lname" runat="server" Width="129px" CssClass="style5" MaxLength="30"
                            TabIndex="2" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredLNameFieldValidator" runat="server" ErrorMessage="Last Name is required." ControlToValidate="txt_lname"  Display="Dynamic"></asp:RequiredFieldValidator>                                        
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Address1:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_addr1" runat="server" Width="215px" CssClass="style5" MaxLength="30"
                            TabIndex="3" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px; height: 23px;">
                        Address2:</td>
                    <td colspan="3" style="width: 165px; height: 23px;">
                        <asp:TextBox ID="txt_addr2" runat="server" Width="215px" CssClass="style5" MaxLength="30"
                            TabIndex="4"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        City:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_city" runat="server" Width="129px" CssClass="style5" MaxLength="30"
                            TabIndex="5" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        State:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_state" runat="server" Width="20px" CssClass="style5" MaxLength="5"
                            TabIndex="6" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Zip Code:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_zip" runat="server" Width="55px" CssClass="style5" MaxLength="5"
                            TabIndex="7" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredZipFieldValidator" runat="server" ErrorMessage="ZIP is required." ControlToValidate="txt_zip"  Display="Dynamic"></asp:RequiredFieldValidator>
                        <br /><asp:RegularExpressionValidator id="ZipCodeExpressionValidator" runat="server" ValidationExpression="\d{5}"  ErrorMessage="ZIP code must be 5 numeric digits" ControlToValidate="txt_zip" Display="Dynamic" />                        
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Home Phone:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_home_phone" runat="server" CssClass="style5" MaxLength="10"
                            Width="131px" TabIndex="8" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox></td>
                </tr>
            </table>
            <table runat="server" id="full_app" visible="False">
                <tr>
                    <td style="width: 120px">
                        Alt Phone:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_alt_phone" runat="server" CssClass="style5" MaxLength="10" Width="131px"
                            TabIndex="9"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Birth Date:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_month" runat="server" CssClass="style5" MaxLength="2" Width="16px"
                            TabIndex="10" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        /
                        <asp:TextBox ID="txt_day" runat="server" CssClass="style5" MaxLength="2" Width="16px"
                            TabIndex="11" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        /
                        <asp:TextBox ID="txt_year" runat="server" CssClass="style5" MaxLength="4" Width="32px"
                            TabIndex="12" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="height: 23px; width: 120px;">
                        SSN:</td>
                    <td colspan="3" style="height: 23px; width: 165px;">
                        <asp:TextBox ID="txt_ssn1" runat="server" Width="29px" CssClass="style5" MaxLength="3"
                            TabIndex="13" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        -
                        <asp:TextBox ID="txt_ssn2" runat="server" CssClass="style5" MaxLength="2" Width="21px"
                            TabIndex="14" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        -
                        <asp:TextBox ID="txt_ssn3" runat="server" CssClass="style5" MaxLength="4" Width="46px"
                            TabIndex="15" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Emp Time:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_emp_time_years" runat="server" Width="19px" CssClass="style5"
                            MaxLength="2" TabIndex="16" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        years
                        <asp:TextBox ID="txt_emp_time_months" runat="server" CssClass="style5" MaxLength="2"
                            TabIndex="16" Width="19px" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        months</td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Employer Phone:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_emp_phone" runat="server" Width="92px" CssClass="style5" MaxLength="10"
                            TabIndex="17" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Residence:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:DropDownList ID="cbo_res_status" runat="server" CssClass="style5" BackColor="#6699CC"
                            ForeColor="#FFFFFF" TabIndex="18">
                            <asp:ListItem Value="0">Own</asp:ListItem>
                            <asp:ListItem Value="1">Rent</asp:ListItem>
                            <asp:ListItem Value="2">Parents</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Time at Residence:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_time_at_res_years" runat="server" Width="19px" CssClass="style5"
                            MaxLength="2" TabIndex="19" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        years
                        <asp:TextBox ID="txt_time_at_res_months" runat="server" CssClass="style5" MaxLength="2"
                            TabIndex="19" Width="19px" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox>
                        months</td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Monthly Income:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_annual_income" runat="server" Width="70px" CssClass="style5"
                            MaxLength="6" TabIndex="20" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        DL # / State:</td>
                    <td colspan="3" style="width: 165px">
                        <asp:TextBox ID="txt_dl_license_no" runat="server" CssClass="style5" MaxLength="10"
                            Width="94px" TabIndex="21"></asp:TextBox>&nbsp;
                        <asp:TextBox ID="txt_dl_license_state" runat="server" CssClass="style5" MaxLength="2"
                            TabIndex="22" Width="20px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Credit Protection:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:DropDownList ID="cbo_credit_protection" runat="server" CssClass="style5" TabIndex="25">
                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 120px">
                        Relative Phone:</td>
                    <td colspan="3" style="height: 21px; width: 165px;">
                        <asp:TextBox ID="txt_rel_phone" runat="server" Width="92px" CssClass="style5" MaxLength="10"
                            TabIndex="26" BorderColor="#6699CC" BorderStyle="Solid"></asp:TextBox></td>
                </tr>
            </table>
        </div>
        <br />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Button ID="btn_save_exit" runat="server" Text="Submit Application"
            Width="144px" CssClass="style5" />
        <asp:Button ID="btn_accept" runat="server" Text="Accept Offer" Width="144px" CssClass="style5" Enabled="False" />
        <asp:Button ID="btn_decline" runat="server" Text="Decline Offer" Width="144px" CssClass="style5" Enabled="False" />
        &nbsp;<br />
        &nbsp;
        <br />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <asp:Label ID="lbl_response" runat="server" Width="452px"></asp:Label><br />
        <br />
        <asp:Panel ID="Panel1" runat="server" Height="150px" Width="453px" ScrollBars="Auto">
            <asp:Label ID="lbl_warnings" runat="server" Width="427px" ForeColor="Black"></asp:Label></asp:Panel>
        &nbsp;<br />
        &nbsp;&nbsp;
    </form>
</body>
</html>
