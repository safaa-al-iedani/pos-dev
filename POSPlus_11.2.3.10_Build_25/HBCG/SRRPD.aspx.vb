﻿Imports System.Collections.Generic
Imports System.Data.OracleClient
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports Renci.SshNet
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Partial Class SRRPD
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lbl_msg.Text = ""
            ASPxPopupSummary.ShowOnPageLoad = False
            ASPxPopupDetail.ShowOnPageLoad = False
            Populate_str()
            Populate_sls_tp()
        Else
            lbl_msg.Text = ""
        End If


    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    Public Sub Populate_str()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_store_grp_list.Items.Clear()

        sql = " select store_grp_cd store_grp_cd, store_grp_cd||' - '||des name from store_grp "
        sql = sql & " where std_multi_co2.isvalidstrgrp7('" & Session("emp_init") & "',store_grp_cd) = 'Y' "
        sql = sql & " order by 1 "

        srch_store_grp_list.Items.Insert(0, "-")
        srch_store_grp_list.Items.FindByText("-").Value = ""
        srch_store_grp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_store_grp_list
                .DataSource = ds
                .DataValueField = "store_grp_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Public Sub Populate_sls_tp()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_sls_tp.Items.Clear()

        sql = " select 'Written' tp from dual "
        sql = sql & " union select 'Delivered' tp from dual "

        srch_sls_tp.Items.Insert(0, "-")
        srch_sls_tp.Items.FindByText("-").Value = ""
        srch_sls_tp.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_sls_tp
                .DataSource = ds
                .DataValueField = "tp"
                .DataTextField = "tp"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Protected Sub srch_dt1_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        srch_dt.Text = clndr_dt.SelectedDate
        clndr_dt.Visible = False
    End Sub
    Protected Sub srch_dt1_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt1_btn.Click
        clndr_dt.Visible = True
        clndr_dt.Focus()
    End Sub
    Protected Sub srch_dt1x_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt1x_btn.Click
        clndr_dt.Visible = False
        srch_dt.Text = ""
    End Sub
    Protected Sub clear_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clear_btn.Click

        clndr_dt.Visible = False
        srch_dt.Text = ""
        srch_store_grp_list.ClearSelection()
        srch_sls_tp.ClearSelection()
        lbl_msg.Text = ""

        ASPxPopupSummary.ShowOnPageLoad = False
        ASPxPopupDetail.ShowOnPageLoad = False

    End Sub

    Protected Sub summary_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles summary_btn.Click

        Dim v_sls_tp As String = ""
        Dim v_id As OracleNumber = 0

        If srch_store_grp_list.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select a Store Group to continue"
            srch_store_grp_list.Focus()
            Exit Sub
        End If

        If srch_sls_tp.SelectedIndex < 1 Then
            lbl_msg.Text = "Please select Sales Type to continue"
            srch_sls_tp.Focus()
            Exit Sub
        Else
            If srch_sls_tp.SelectedItem.Value.ToString() = "Written" Then
                v_sls_tp = "W"
            Else
                v_sls_tp = "D"
            End If
        End If

        If srch_dt.Text & "" = "" Then
            lbl_msg.Text = "Please select a Date to continue"
            srch_dt.Focus()
            Exit Sub
        End If

        v_id = getSalesID()
        If v_id = 0 Then
            v_id = GetSales()
        End If

        If IsDBNull(v_id) Then
            lbl_msg.Text = "No Data Found - please try again"
        ElseIf v_id = 0 Then
            lbl_msg.Text = "No Data Found - please try again"
        Else
            ASPxPopupDetail.ShowOnPageLoad = False
            ASPxPopupSummary.ShowOnPageLoad = True
            populate_sales_summary(v_id)
        End If


    End Sub
    Protected Sub detail_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles detail_btn.Click

        Dim v_id As OracleNumber = 0

        v_id = getSalesID()
        If v_id = 0 Then
            v_id = GetSales()
        End If
        ASPxPopupDetail.ShowOnPageLoad = True
        populate_sales_detail(v_id)

    End Sub

    Public Function getSalesID() As String


        Dim v_sls_tp As String = ""
        Dim v_id As OracleNumber = 0

        If srch_sls_tp.SelectedItem.Value.ToString() = "Written" Then
            v_sls_tp = "W"
        Else
            v_sls_tp = "D"
        End If

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception


        Try
            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()
            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_srrpd.getSalesID"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_user", OracleType.VarChar)).Value = Session("emp_init")
            myCMD.Parameters.Add(New OracleParameter("p_store_grp_cd", OracleType.VarChar)).Value = srch_store_grp_list.SelectedItem.Value.ToString()
            myCMD.Parameters.Add(New OracleParameter("p_wr_cd", OracleType.VarChar)).Value = v_sls_tp
            myCMD.Parameters.Add(New OracleParameter("p_dt", OracleType.VarChar)).Value = FormatDateTime(srch_dt.Text, DateFormat.ShortDate)
            myCMD.Parameters.Add("p_id", OracleType.Number).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_id").Value) = False Then
                v_id = myCMD.Parameters("p_id").Value.ToString()
            End If

        Catch x
            Throw
            lbl_msg.Text = x.Message.ToString

        Finally

            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return v_id

    End Function
    Public Function GetSales() As String


        Dim v_sls_tp As String = ""
        Dim v_id As OracleNumber = 0

        If srch_sls_tp.SelectedItem.Value.ToString() = "Written" Then
            v_sls_tp = "W"
        Else
            v_sls_tp = "D"
        End If

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception


        Try
            connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
            objConnection = DisposablesManager.BuildOracleConnection(connString)
            objConnection.Open()
            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_srrpd.getSales"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_user", OracleType.VarChar)).Value = Session("emp_init")
            myCMD.Parameters.Add(New OracleParameter("p_store_grp_cd", OracleType.VarChar)).Value = srch_store_grp_list.SelectedItem.Value.ToString()
            myCMD.Parameters.Add(New OracleParameter("p_wr_cd", OracleType.VarChar)).Value = v_sls_tp
            myCMD.Parameters.Add(New OracleParameter("p_dt", OracleType.VarChar)).Value = FormatDateTime(srch_dt.Text, DateFormat.ShortDate)
            myCMD.Parameters.Add("p_id", OracleType.Number).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_id").Value) = False Then
                v_id = myCMD.Parameters("p_id").Value.ToString()
            End If

        Catch x
            Throw
            lbl_msg.Text = x.Message.ToString

        Finally

            objConnection.Close()
            objConnection.Dispose()
        End Try

        Return v_id

    End Function
    Public Sub CleanupOldData()

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim x As Exception


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_srrpd.cleanup"
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_user", OracleType.VarChar)).Value = Session("emp_init")

        Try
            myCMD.ExecuteNonQuery()
            objConnection.Close()
        Catch x
            Throw
            lbl_msg.Text = x.Message.ToString
            objConnection.Close()


        End Try

    End Sub
    Public Sub populate_sales_summary(ByVal p_id As String)


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        GridView1.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        '" select store_cd STORE_CD, lpad(to_char(SUM(sls_amt),'999,999,999.99'),14,' ') TOT_SLS, " &
        objSql.CommandText =
         " select store_cd STORE_CD, SUM(sls_amt) TOT_SLS, " &
          " (select a.sls_amt from srrpd_output a where a.id = " & p_id & " and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '1FURN' and a.store_cd = 'ZZ') FURN_SLS," &
         " (select a.sls_amt from srrpd_output a where a.id = " & p_id & " and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '2APPL' and a.store_cd = 'ZZ') APPL_SLS," &
         " (select a.sls_amt from srrpd_output a where a.id = " & p_id & " and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '3ELEC' and a.store_cd = 'ZZ') ELEC_SLS," &
         " (select a.sls_amt from srrpd_output a where a.id = " & p_id & " and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '4MATT' and a.store_cd = 'ZZ') MATT_SLS," &
         " (select a.sls_amt from srrpd_output a where a.id = " & p_id & "and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '5MISC' and a.store_cd = 'ZZ') MISC_SLS " &
         " from srrpd_output b where b.id = " & p_id & " and b.emp_init = '" & Session("emp_init") & "' and b.store_cd = 'ZZ' group by store_cd "


        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GridView1.DataSource = ds
                GridView1.DataBind()

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView1.DataSource = ds
                GridView1.DataBind()


            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub
    Public Sub populate_sales_detail(ByVal p_id As String)


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        GridView2.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        objSql.CommandText =
         " select distinct store_cd STORE_CD, sum(nvl(sls_amt,0)) TOT_SLS " &
          " ,(select sum(nvl(a.sls_amt,0)) from srrpd_output a where a.id = " & p_id & " and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '1FURN' and a.store_cd = b.store_cd) FURN_SLS" &
         " ,(select sum(nvl(a.sls_amt,0)) from srrpd_output a where a.id = " & p_id & " and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '2APPL' and a.store_cd = b.store_cd) APPL_SLS" &
         " ,(select sum(nvl(a.sls_amt,0)) from srrpd_output a where a.id = " & p_id & " and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '3ELEC' and a.store_cd = b.store_cd) ELEC_SLS" &
         " ,(select sum(nvl(a.sls_amt,0)) from srrpd_output a where a.id = " & p_id & " and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '4MATT' and a.store_cd = b.store_cd) MATT_SLS" &
         " ,(select sum(nvl(a.sls_amt,0)) from srrpd_output a where a.id = " & p_id & " and a.emp_init = '" & Session("emp_init") & "' and a.prod_grp_cd = '5MISC' and a.store_cd = b.store_cd) MISC_SLS " &
         " from srrpd_output b where b.id = " & p_id & " and b.emp_init = '" & Session("emp_init") & "' and b.store_cd <> 'ZZ' " &
         " group by store_cd " &
         " ORDER BY 1 "


        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GridView2.DataSource = ds
                GridView2.DataBind()

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView2.DataSource = ds
                GridView2.DataBind()


            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Sub
End Class
