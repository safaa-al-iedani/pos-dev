﻿Imports HBCG_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports System.Data.SqlClient


Partial Class PrinterMaintenance
    Inherits POSBasePage

    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If isNotEmpty(Session("CO_CD")) Then
                '  PopulateStoreCodes(Session("CO_CD"))
                PopulateSys_belong()
            End If
        End If

        If (Not Gridview1.DataSource Is Nothing) Then
            Gridview1.DataBind()
        End If

    End Sub



    Protected Function check_store(ByVal storeCd As String) As String

        Dim str_y_n As String
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select store_cd  from store "
        sql = sql & "where store_cd = :STORE "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            dbCommand.Parameters.Add(":STORE", OracleType.VarChar)
            dbCommand.Parameters(":STORE").Value = UCase(storeCd)


            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                Return "Y"

            Else
                Return "N"
            End If
            dbReader.Close()
            dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try


    End Function
    Protected Function check_printer_tp(ByVal p_printer_tp As String) As String


        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select printer_tp  from printer_info "
        sql = sql & "where printer_tp = :printer_tp "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            dbCommand.Parameters.Add(":printer_tp", OracleType.VarChar)
            dbCommand.Parameters(":printer_tp").Value = UCase(p_printer_tp)


            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                Return "Y"

            Else
                Return "N"
            End If
            dbReader.Close()
            dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try


    End Function

    Protected Function check_sys_belong(ByVal p_sys_belong As String) As String


        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select sys_belong from printer_info "
        sql = sql & "where sys_belong = :sys_belong "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            dbCommand.Parameters.Add(":sys_belong", OracleType.VarChar)
            dbCommand.Parameters(":sys_belong").Value = UCase(p_sys_belong)


            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                Return "Y"

            Else
                Return "N"
            End If
            dbReader.Close()
            dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try


    End Function
    Sub do_insert_item()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand 'jkl added new
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim SoLnGItm As DataGridItem 'lucy
        ds = New DataSet
        Gridview1.DataSource = ""

        Gridview1.Visible = True

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        objSql.CommandText = "SELECT store_cd,printer_nm,printer_ip,sys_belong,printer_tp,rowid row_id from printer_info where printer_nm=:printer_nm and store_cd is null and printer_ip is null "
        objSql.Parameters.Add(":printer_nm", OracleType.VarChar)
        objSql.Parameters(":printer_nm").Value = UCase(txt_printer_nm.Text)
        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                Dim i As Integer

                For i = 0 To 0

                    SoLnGItm = Gridview1.Items(i)

                Next
            Else
                Gridview1.Visible = False

            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    'Private Sub PopulateStoreCodes(ByVal coCd As String)

    '    Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
    '    Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand

    '    Dim ds As New DataSet

    '    Dim SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc from store WHERE co_cd = nvl(:CO_CD, co_cd) order by store_cd"

    '    cmdGetCodes.Parameters.Add(":CO_CD", OracleType.VarChar)
    '    cmdGetCodes.Parameters(":CO_CD").Value = coCd

    '    Try
    '        With cmdGetCodes
    '            .Connection = connErp
    '            .CommandText = SQL
    '        End With

    '        connErp.Open()
    '        Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
    '        oAdp.Fill(ds)

    '        With txt_store_cd
    '            .DataSource = ds
    '            .DataValueField = "store_cd"
    '            .DataTextField = "full_store_desc"
    '            .DataBind()
    '        End With

    '        connErp.Close()

    '    Catch ex As Exception
    '        connErp.Close()
    '        Throw
    '    End Try

    'End Sub

    Private Sub PopulateStoreCodes(ByVal coCd As String)

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc from store WHERE co_cd = nvl(:CO_CD, co_cd) order by store_cd"

        cmdGetCodes.Parameters.Add(":CO_CD", OracleType.VarChar)
        cmdGetCodes.Parameters(":CO_CD").Value = coCd

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_store_desc"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Private Sub PopulateSys_belong()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT distinct sys_belong from printer_info  WHERE sys_belong is not null"

        'cmdGetCodes.Parameters.Add(":CO_CD", OracleType.VarChar)
        ' cmdGetCodes.Parameters(":CO_CD").Value = coCd

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_store_cd
                .DataSource = ds
                .DataValueField = "sys_belong"
                .DataTextField = "sys_belong"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_lookup.Click
        lbl_info.Text = ""

        If isEmpty(txt_printer_nm.Text) Then
            lbl_info.Text = "Printer not found."
        End If

        BindGrid()

    End Sub
    Sub BindGrid()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand 'jkl added new
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim SoLnGItm As DataGridItem 'lucy
        ds = New DataSet
        Gridview1.DataSource = ""

        Gridview1.Visible = True
        txt_printer_nm.Text = UCase(txt_printer_nm.Text)
        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        objSql.CommandText = "SELECT  store_cd,printer_nm,printer_ip,sys_belong,printer_tp,rowid row_id from printer_info where printer_nm=:printer_nm or printer_nm like :printer_nm "
        objSql.Parameters.Add(":printer_nm", OracleType.VarChar)
        objSql.Parameters(":printer_nm").Value = txt_printer_nm.Text
        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                Dim i As Integer

                For i = 0 To numrows - 1

                    SoLnGItm = Gridview1.Items(i)
                Next
            Else
                Gridview1.Visible = False
                lbl_info.Text = "Printer " & txt_printer_nm.Text & " not found."

            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            'Throw
        End Try

    End Sub


    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_clear.Click
        lbl_info.Text = ""
        Response.Redirect("PrinterMaintenance.aspx")
    End Sub



    Public Sub Delete_Item(ByVal rowSeqNum As String)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim deletecd As String

        Try
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            ' deletecd = Gridview1.DataKeys(e.Item.ItemIndex)
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "DELETE FROM printer_info WHERE rowid = :UNIQUEID or store_cd is null or printer_ip is null or printer_tp is null or sys_belong is null or printer_nm is null"
                .Parameters.Add(":UNIQUEID", OracleType.VarChar)
                .Parameters(":UNIQUEID").Value = rowSeqNum
            End With
            cmdDeleteItems.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            conn.Close()
        End Try


    End Sub


    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim rowSeqNum As String
        rowSeqNum = e.Item.Cells(5).Text

        Delete_Item(rowSeqNum)
        BindGrid()
    End Sub

    Sub DoItemUpdate(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)
        Dim txt_c As TextBox
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim thistransaction As OracleTransaction

        Dim str_store As String
        Dim str_printer_nm As String
        Dim str_printer_ip As String
        Dim str_sys_belong As String
        Dim str_printer_tp As String
        Dim str_rowid As String

        lbl_info.Text = ""

        txt_c = e.Item.Cells(0).Controls(0)
        str_printer_nm = txt_c.Text   'printer_nm
        'txt_c = e.Item.Cells(1).Controls(0)
        'Drop down 
        Dim DDL As DropDownList = CType(e.Item.FindControl("DropDownList1"), DropDownList)
        str_store = DDL.SelectedItem.Value
        'str_store = txt_c.Text   'store
        txt_c = e.Item.Cells(2).Controls(0)
        str_printer_ip = txt_c.Text  'printer_ip


        'Drop down 
        Dim DDL1 As DropDownList = CType(e.Item.FindControl("DropDownList_sys_belong"), DropDownList)
        str_sys_belong = DDL1.SelectedItem.Value
        ' txt_c = e.Item.Cells(3).Controls(0)
        ' str_sys_belong = txt_c.Text   'sys_belong

        ' txt_c = e.Item.Cells(4).Controls(0)
        ' str_printer_tp = txt_c.Text   ' printer_tp
        'Drop down 
        Dim DDL2 As DropDownList = CType(e.Item.FindControl("DropDownList_printer_tp"), DropDownList)
        str_printer_tp = DDL2.SelectedItem.Value
        txt_c = e.Item.Cells(5).Controls(0)
        str_rowid = txt_c.Text  'rowid
        str_rowid = Session("rowid")  'rowid
        conn.Open()

        If check_store(str_store) <> "Y" Then
            lbl_info.Text = "Please enter a valid store"
        ElseIf isEmpty(str_printer_ip) Then
            lbl_info.Text = "Please enter a valid IP"
        ElseIf isEmpty(str_printer_nm) Then
            lbl_info.Text = "Please enter a valid Printer Name"
        ElseIf check_sys_belong(str_sys_belong) <> "Y" Then
            lbl_info.Text = "Please enter a valid sys_belong"
        ElseIf check_printer_tp(str_printer_tp) <> "Y" Then
            lbl_info.Text = "Please enter a valid Printer Type"

        Else
            thistransaction = conn.BeginTransaction

            Try
                With cmdUpdateItems
                    .Transaction = thistransaction
                    .Connection = conn
                    .CommandText = "UPDATE printer_info SET store_cd=:store_cd,printer_nm=:printer_nm,printer_ip=:printer_ip,sys_belong=:sys_belong,printer_tp=:printer_tp WHERE rowid = :UNIQUEID"

                    '.CommandText = "Insert into PRINTER_INFO (STORE_CD,PRINTER_NM,PRINTER_IP,SYS_BELONG,PRINTER_TP) values (:store_cd,:printer_nm,:printer_ip,:sys_belong,:printer_tp)"

                    .Parameters.Add(":store_cd", OracleType.VarChar)
                    .Parameters(":store_cd").Value = UCase(str_store)
                    .Parameters.Add(":printer_nm", OracleType.VarChar)
                    .Parameters(":printer_nm").Value = UCase(str_printer_nm)
                    .Parameters.Add(":printer_ip", OracleType.VarChar)
                    .Parameters(":printer_ip").Value = str_printer_ip
                    .Parameters.Add(":printer_tp", OracleType.VarChar)
                    .Parameters(":printer_tp").Value = UCase(str_printer_tp)
                    .Parameters.Add(":sys_belong", OracleType.VarChar)
                    .Parameters(":sys_belong").Value = UCase(str_sys_belong)
                    'Feb 10
                    .Parameters.Add(":UNIQUEID", OracleType.VarChar)
                    .Parameters(":UNIQUEID").Value = Gridview1.DataKeys(e.Item.ItemIndex).ToString
                    .Parameters(":UNIQUEID").Value = Session("rowid")
                End With
                cmdUpdateItems.ExecuteNonQuery()
                thistransaction.Commit()
                lbl_info.Text = "Printer is added/updated"
            Catch ex As Exception
                lbl_info.Text = "System Error"
                Throw
            End Try

        End If
        Gridview1.EditItemIndex = -1
        conn.Close()

        BindGrid()


    End Sub
    Sub edit_grid()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand 'jkl added new
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim SoLnGItm As DataGridItem 'lucy
        ds = New DataSet
        Gridview1.DataSource = ""

        Gridview1.Visible = True

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        objSql.CommandText = "SELECT  nvl(store_cd, ' ') store_cd,printer_nm,printer_ip,nvl(sys_belong,' ') sys_belong,nvl(printer_tp,' ') printer_tp,rowid row_id from printer_info where rowid=:row_id"
        objSql.Parameters.Add(":row_id", OracleType.VarChar)
        objSql.Parameters(":row_id").Value = Session("rowid")
        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                Dim i As Integer

                For i = 0 To numrows - 1

                    SoLnGItm = Gridview1.Items(i)
                Next
            Else
                Gridview1.Visible = False

            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            'Throw
        End Try
    End Sub
    Sub DoItemEdit(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)



        Dim str_c0 As String
        Dim str_c1 As String
        Dim str_c2 As String
        Dim str_c3 As String
        Dim str_c4 As String
        Dim str_c5 As String


        str_c0 = e.Item.Cells(0).Text   'printer_nm
        str_c1 = e.Item.Cells(1).Text   'store
        str_c2 = e.Item.Cells(2).Text   'printer_ip
        str_c3 = e.Item.Cells(3).Text   'sys_belong
        str_c4 = e.Item.Cells(4).Text   ' printer_tp
        str_c5 = e.Item.Cells(5).Text   'rowid
        Session("rowid") = str_c5

        Gridview1.EditItemIndex = e.Item.ItemIndex
        edit_grid()




    End Sub

    Sub DoItemCancel(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)
        Gridview1.EditItemIndex = -1
        BindGrid()

    End Sub

    Protected Function insert_to_table(ByVal p_printer_nm As String) As Boolean



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        sql = "Insert into printer_info (printer_nm ) "
        sql = sql & "values (:printer_nm ) "


        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        objsql.Parameters.Add(":printer_nm", OracleType.VarChar)
        objsql.Parameters(":printer_nm").Value = p_printer_nm

        Try
            'Execute DataReader 
            objsql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            'Throw New Exception("Error inserting the record. Please try again.")
            conn.Close()
            Return False
        End Try

        Return True
    End Function


    Sub add_grid()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand 'jkl added new
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim SoLnGItm As DataGridItem 'lucy
        ds = New DataSet
        Gridview1.DataSource = ""

        Gridview1.Visible = True

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        objSql.CommandText = "SELECT  store_cd,printer_nm,printer_ip,sys_belong,printer_tp,rowid row_id from printer_info where printer_nm=:printer_nm and store_cd is null"
        objSql.Parameters.Add(":printer_nm", OracleType.VarChar)
        objSql.Parameters(":printer_nm").Value = txt_printer_nm.Text
        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                Dim i As Integer

                For i = 0 To 0

                    SoLnGItm = Gridview1.Items(i)
                    Dim str_rowid As String
                    Dim str_cd As String
                    str_rowid = MyDataReader.Item("row_id").ToString
                    str_cd = MyDataReader.Item("store_cd").ToString
                    Session("rowid") = str_rowid
                    Session("strCd") = str_cd
                Next
            Else
                Gridview1.Visible = False

            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub
    Protected Sub btn_add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add.Click

        Dim seq As Integer = 1
        Dim str_rowid As String
        Dim slspCntArr(23) As String
        Dim intRows As Integer = Gridview1.Items.Count - 1

        If isEmpty(txt_printer_nm.Text) Then
            lbl_info.Text = "Please enter a printer name first, then click the button"
            Exit Sub
        End If
        txt_printer_nm.Text = UCase(txt_printer_nm.Text)
        lbl_info.Text = ""
        Delete_Item("")   'delete the record not properly inserted
        insert_to_table(txt_printer_nm.Text)
        add_grid()
        str_rowid = Session("rowid")
        ' cp_DoItemEdit(str_rowid)

    End Sub

    Private Sub Gridview1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

        If e.Item.ItemType = ListItemType.EditItem Then

            Dim oAdp As OracleDataAdapter
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim currentStore As String = "00" ' Default to home store 


            Dim DRV As DataRowView = CType(e.Item.DataItem, DataRowView)
            currentStore = DRV("store_cd")
            If isEmpty(currentStore.Trim) Then
                currentStore = "00"
            End If


            'Dim DDL As DropDownList = CType(e.Item.Cells(1).Controls(0), DropDownList)
            Dim DDL As DropDownList = CType(e.Item.FindControl("DropDownList1"), DropDownList)

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            objSql.CommandText = "SELECT store_cd, store_cd || ' - ' || substr(store_name,1,20) store_name FROM store ORDER BY store_cd"

            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

            Dim DS As New DataSet
            Dim item As ListItem
            oAdp.Fill(DS)
            DDL.DataSource = DS
            DDL.DataTextField = "store_name"
            DDL.DataValueField = "store_cd"
            DDL.DataBind()
            item = DDL.Items.FindByValue(currentStore)
            If Not item Is Nothing Then item.Selected = True

        End If

        If e.Item.ItemType = ListItemType.EditItem Then

            Dim oAdp As OracleDataAdapter
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            Dim currentSys_belong As String = "MMHF" ' Default  


            Dim DRV As DataRowView = CType(e.Item.DataItem, DataRowView)
            currentSys_belong = DRV("sys_belong")
            If isEmpty(currentSys_belong.Trim) Then
                currentSys_belong = "MMHF"
            End If
            Dim DDL = CType(e.Item.FindControl("Dropdownlist_sys_belong"), DropDownList)


            objSql.CommandText = "SELECT distinct sys_belong, sys_belong sys_belong_name FROM lucy_printer_info where sys_belong is not null "
            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

            Dim DS As New DataSet
            Dim item As ListItem
            oAdp.Fill(DS)
            DDL.DataSource = DS
            DDL.DataTextField = "sys_belong_name"
            DDL.DataValueField = "sys_belong"
            DDL.DataBind()
            item = DDL.Items.FindByValue(currentSys_belong)
            If Not item Is Nothing Then item.Selected = True
        End If

        If e.Item.ItemType = ListItemType.EditItem Then

            Dim oAdp As OracleDataAdapter
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            Dim currentPrinter_tp As String = "INVOICE" ' Default  


            Dim DRV As DataRowView = CType(e.Item.DataItem, DataRowView)
            currentPrinter_tp = DRV("printer_tp")
            If isEmpty(currentPrinter_tp.Trim) Then
                currentPrinter_tp = "INVOICE"
            End If
            Dim DDL = CType(e.Item.FindControl("Dropdownlist_printer_tp"), DropDownList)


            objSql.CommandText = "SELECT distinct printer_tp, printer_tp FROM lucy_printer_info where printer_tp is not null "
            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

            Dim DS As New DataSet
            Dim item As ListItem
            oAdp.Fill(DS)
            DDL.DataSource = DS
            DDL.DataTextField = "printer_tp"
            DDL.DataValueField = "printer_tp"
            DDL.DataBind()
            item = DDL.Items.FindByValue(currentPrinter_tp)
            If Not item Is Nothing Then item.Selected = True
        End If
    End Sub

End Class

