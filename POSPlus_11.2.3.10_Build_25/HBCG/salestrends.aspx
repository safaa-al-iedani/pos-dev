<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="salestrends.aspx.vb" Inherits="salestrends" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0">
        <tr>
            <td align="right">
                <asp:DropDownList ID="cbo_store" runat="server" Width="193px" CssClass="style5" AutoPostBack="true">
                </asp:DropDownList>
                <br />
                <asp:DropDownList ID="cbo_slsp" runat="server" Width="193px" CssClass="style5" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="ASPxLabel">
    </dx:ASPxLabel>
    <br />
    <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" Height="213px" Width="686px">
        <seriestemplate>
            <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
            </cc1:PointOptions>
</PointOptionsSerializable>
            <LabelSerializable>
<cc1:SideBySideBarSeriesLabel HiddenSerializableString="to be serialized">
                <FillStyle >
                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                </FillStyle>
            </cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
            <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
            </cc1:PointOptions>
</LegendPointOptionsSerializable>
            <ViewSerializable>
<cc1:SideBySideBarSeriesView HiddenSerializableString="to be serialized">
            </cc1:SideBySideBarSeriesView>
</ViewSerializable>
        </seriestemplate>
        <fillstyle>
            <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
        </fillstyle>
        <titles>
            <cc1:ChartTitle Text="Relationship Counts">
            </cc1:ChartTitle>
        </titles>
    </dxchartsui:WebChartControl>
    <br />
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="685px">
        <SettingsPager Visible="False">
        </SettingsPager>
        <Columns>
            <dx:GridViewDataTextColumn Caption="Relationship" FieldName="REL_NO" ReadOnly="True"
                VisibleIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Name" FieldName="LNAME" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Phone" FieldName="HPHONE" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Type" FieldName="APPT_TYPE" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Time" FieldName="APPT_TIME" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Total" FieldName="PROSPECT_TOTAL" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
