﻿Imports System.Data.OracleClient

Partial Class SPIFFCalendarLookup
    Inherits POSBasePage

    Public Const gs_version As String = "Version 1.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen. Spiff Calendar Lookup.
    '------------------------------------------------------------------------------------------------------------------------------------

    ''' <summary>
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            If FindEmployeeDetails() = False Then
                Return
            End If

            'Populate the store list in drop down list
            Dim s_home_store_code = Session("HOME_STORE_CD")

            If (s_home_store_code = "10" Or s_home_store_code = "00") Then
                PopulateHeadOfficeStores()
            ElseIf (Not ((s_home_store_code = "00") And (s_home_store_code = "10"))) Then
                PopulateEmployeeStore()
                ddlist_store_cd.Enabled = False
            End If
            PopulateProductGroup()
            PopulateProductMajor("ALL")
            lbl_versionInfo.Text = gs_version
        End If

    End Sub

    ''' <summary>
    ''' Name:           PopulateEmployeeStores()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    To retrieve employee store information
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateEmployeeStore()

        Dim ds As New DataSet
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand

        Dim s_home_store_code = Session("HOME_STORE_CD")

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sqlString = New StringBuilder("SELECT store_cd, store_cd || ' - ' || store_name as full_store_desc ")
        sqlString.Append("FROM store s ")
        sqlString.Append("WHERE store_cd =UPPER(:x_homeStoreCode) ")

        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn
        objcmd.CommandText = sqlString.ToString()

        objcmd.Parameters.Add(":x_homeStoreCode", OracleType.VarChar)
        objcmd.Parameters(":x_homeStoreCode").Value = s_home_store_code

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
        conn.Open()
        objAdaptor.Fill(ds)
        conn.Close()

        With ddlist_store_cd
            .DataSource = ds
            .DataValueField = "store_cd"
            .DataTextField = "full_store_desc"
            .DataBind()
        End With

    End Sub

    ''' <summary>
    ''' Name:           PopulateHeadOfficeStores()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    To populate the HeadOffice stores
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateHeadOfficeStores()

        Dim ds As New DataSet
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim s_companyCode As String = Session("CO_CD")

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sqlString = New StringBuilder("SELECT store_cd, store_cd || ' - ' || store_name as full_store_desc ")
        sqlString.Append("FROM store s, co_grp cg ")
        sqlString.Append("WHERE s.co_cd = cg.co_cd ")
        sqlString.Append("AND cg.co_grp_cd = ( select co_grp_cd from co_grp where co_cd = UPPER(:x_companyCode) ) ")
        sqlString.Append("ORDER BY store_cd ")

        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn
        objcmd.CommandText = sqlString.ToString()

        objcmd.Parameters.Add(":x_companyCode", OracleType.VarChar)
        objcmd.Parameters(":x_companyCode").Value = s_companyCode

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
        conn.Open()
        objAdaptor.Fill(ds)
        conn.Close()

        With ddlist_store_cd
            .DataSource = ds
            .DataValueField = "store_cd"
            .DataTextField = "full_store_desc"
            .DataBind()
        End With

    End Sub

    ''' <summary>
    ''' Name:           PopulateProductGroup()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    To populate the product group drop-down list 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateProductGroup()

        Dim ds As New DataSet
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        sqlString = New StringBuilder("select '1' as sorting, 'ALL' as product_group from dual ")
        sqlString.Append("union all select '2', prod_grp_cd from prod_grp ")
        sqlString.Append("order by sorting, product_group")

        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn
        objcmd.CommandText = sqlString.ToString()

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
        conn.Open()
        objAdaptor.Fill(ds)
        conn.Close()

        With ddlist_product_group
            .DataSource = ds
            .DataValueField = "product_group"
            .DataBind()
        End With

        Session("PRODUCT_GROUP") = ddlist_product_group.SelectedValue

    End Sub

    ''' <summary>
    ''' Name:           PopulateProductMajor()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    To populate the product majors in drop-down list
    ''' </summary>
    ''' <param name="p_productGroup"></param>
    ''' <remarks></remarks>
    Private Sub PopulateProductMajor(p_productGroup As String)

        Dim ds As New DataSet
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        If (p_productGroup = "ALL") Then

            sqlString = New StringBuilder("SELECT '1' AS sorting, 'ALL' AS mjr_cd, 'ALL' AS des FROM DUAL ")
            sqlString.Append("UNION ALL SELECT '2',  mjr_cd,  mjr_cd || ' -  ' || des As des ")
            sqlString.Append("FROM inv_mjr ")
            sqlString.Append("ORDER BY sorting, mjr_cd ")

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn
            objcmd.CommandText = sqlString.ToString()

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            conn.Open()
            objAdaptor.Fill(ds)
            conn.Close()

            With ddlist_product_major
                .DataSource = ds
                .DataValueField = "mjr_cd"
                .DataTextField = "des"
                .DataBind()
            End With
        Else
            sqlString = New StringBuilder("SELECT '1' AS sorting, 'ALL' AS mjr_cd, 'ALL' AS des FROM DUAL ")
            sqlString.Append("UNION ALL SELECT '2',  mjr_cd,  mjr_cd || ' -  ' || des As des ")
            sqlString.Append("FROM inv_mjr ")
            sqlString.Append("WHERE prod_grp_cd IN ")
            sqlString.Append("(SELECT prod_grp_cd FROM prod_grp WHERE prod_grp_cd = :x_productGroup) ")
            sqlString.Append("ORDER BY sorting, mjr_cd ")

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn
            objcmd.CommandText = sqlString.ToString()

            objcmd.Parameters.Add(":x_productGroup", OracleType.VarChar)
            objcmd.Parameters(":x_productGroup").Value = p_productGroup

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            conn.Open()
            objAdaptor.Fill(ds)
            conn.Close()

            With ddlist_product_major
                .DataSource = ds
                .DataValueField = "mjr_cd"
                .DataTextField = "des"
                .DataBind()
            End With
        End If

    End Sub

    ''' <summary>
    ''' Name:           ProductGroupChangedHandler()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    Product Group combo box Event Handler. Based on the selected product group, product major list will get changed. 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ProductGroupChangedHandler() Handles ddlist_product_group.TextChanged

        PopulateProductMajor(ddlist_product_group.Text)

    End Sub

    ''' <summary>
    ''' Name:           btn_Search_Click()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    Search button click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click

        GridViewSpiffSummary.PageIndex = 0
        GridViewSpiffSummary.DataSource = Nothing
        RetrivedSpiffSummary(ddlist_store_cd.SelectedValue, ddlist_product_group.SelectedValue, ddlist_product_major.SelectedValue)
        GridViewSpiffSummary.Visible = True

    End Sub

    ''' <summary>
    ''' Name:           btn_Clear_Click()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    Clear button click event 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        lbl_warning.Text = ""
        ddlist_store_cd.SelectedIndex = 0
        ddlist_product_group.SelectedIndex = 0
        PopulateProductMajor("ALL")
        Session("spiff_clear") = "TRUE"
        GridViewSpiffSummary.DataBind()

    End Sub

    ''' <summary>
    ''' Name:           RetrivedSpiffSummary()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    Retrieves and populates the SPIFF$ amount
    ''' </summary>
    ''' <param name="p_selectedStoreCode"></param>
    ''' <param name="p_selectedProductGroup"></param>
    ''' <param name="p_selectedProductMajor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function RetrivedSpiffSummary(p_selectedStoreCode As String, p_selectedProductGroup As String, p_selectedProductMajor As String) As Boolean

        Dim ds As New DataSet
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim s_companyCode As String = Session("CO_CD")

        sqlString = New StringBuilder("")
        sqlString.Append("SELECT itm.itm_cd As SKU#, itm. ")
        sqlString.Append(Resources.CustomMessages.MSG0080)
        sqlString.Append(" As description, ")
        sqlString.Append("itm.vsn As vsn, ")
        sqlString.Append("itm.comm_cd As Comm_cd, ")
        sqlString.Append("TO_CHAR(isc.eff_dt, 'DD-MON-YYYY') As EFF_DATE, ")
        sqlString.Append("TO_CHAR(isc.end_dt, 'DD-MON-YYYY') As END_DATE, ")
        sqlString.Append("LTRIM(TO_CHAR(isc.spiff_amt, '99999999999.99')) As spiff$, ")
        sqlString.Append("LTRIM(TO_CHAR(isc.min_retail, '99999999999.99')) As min_retail ")
        sqlString.Append("FROM inv_mnr mn, inv_mjr mj, itm, itm$spiff_clndr isc, prod_grp pg ")
        sqlString.Append("WHERE itm.itm_cd = isc.itm_cd ")
        sqlString.Append("AND itm.mnr_cd = mn.mnr_cd ")
        sqlString.Append("AND mn.mjr_cd = mj.mjr_cd ")
        sqlString.Append("AND mj.prod_grp_cd = pg.prod_grp_cd ")
        sqlString.Append("AND SYSDATE BETWEEN isc.eff_dt AND isc.end_dt ")
        sqlString.Append("AND EXISTS ")
        sqlString.Append("(SELECT 1 FROM itm$itm_srt iis WHERE iis.itm_srt_cd = :p_itm_srt_cd AND iis.itm_cd = isc.itm_cd ) ")
        sqlString.Append("AND isc.store_cd = :x_storeCode ")

        If p_selectedProductGroup = "ALL" And p_selectedProductMajor = "ALL" Then

            sqlString.Append("ORDER BY itm.itm_cd ")

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn
            objcmd.CommandText = sqlString.ToString()

        ElseIf p_selectedProductMajor = "ALL" Then

            sqlString.Append("AND mj.prod_grp_cd = :x_productGroup ")
            sqlString.Append("ORDER BY itm.itm_cd ")

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn
            objcmd.CommandText = sqlString.ToString()

            objcmd.Parameters.Add(":x_productGroup", OracleType.VarChar)
            objcmd.Parameters(":x_productGroup").Value = p_selectedProductGroup

        ElseIf p_selectedProductGroup = "ALL" Then

            sqlString.Append("AND mj.mjr_cd = :x_productMajor ")
            sqlString.Append("ORDER BY itm.itm_cd ")

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn
            objcmd.CommandText = sqlString.ToString()

            objcmd.Parameters.Add(":x_productMajor", OracleType.VarChar)
            objcmd.Parameters(":x_productMajor").Value = p_selectedProductMajor

        Else

            sqlString.Append("AND mj.prod_grp_cd = :x_productGroup ")
            sqlString.Append("AND mj.mjr_cd = :x_productMajor ")
            sqlString.Append("ORDER BY itm.itm_cd ")

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn
            objcmd.CommandText = sqlString.ToString()

            objcmd.Parameters.Add(":x_productMajor", OracleType.VarChar)
            objcmd.Parameters(":x_productMajor").Value = p_selectedProductMajor

            objcmd.Parameters.Add(":x_productGroup", OracleType.VarChar)
            objcmd.Parameters(":x_productGroup").Value = p_selectedProductGroup

        End If

        objcmd.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objcmd.Parameters(":x_storeCode").Value = p_selectedStoreCode

        objcmd.Parameters.Add(":p_itm_srt_cd", OracleType.VarChar)
        If Session("CO_GRP_CD").ToString().Equals("LEO") Then
            objcmd.Parameters(":p_itm_srt_cd").Value = "LFL"
        Else
            objcmd.Parameters(":p_itm_srt_cd").Value = "BW1"
        End If

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
        conn.Open()
        objAdaptor.Fill(ds)
        conn.Close()

        If (ds.Tables(0).Rows.Count > 0) Then
            Session("dt_spiffSummary") = ds.Tables(0)
        Else
            Session("dt_spiffSummary") = Nothing
        End If
        GridViewSpiffSummary.DataBind()

        Return True

    End Function

    ''' <summary>
    ''' Name:           GridViewSpiffSummary_DataBinding()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    GridView SpiffSummary Event Handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridViewSpiffSummary_DataBinding(ByVal sender As Object, ByVal e As EventArgs)

        If Session("spiff_clear") = "TRUE" Then
            GridViewSpiffSummary.DataSource = Nothing
            Session("spiff_clear") = Nothing
            Return
        End If

        GridViewSpiffSummary.DataSource = Session("dt_spiffSummary")
        If IsNothing(Session("dt_spiffSummary")) Then
            'No records found for the given criteria
            lbl_warning.Text = Resources.CustomMessages.MSG0040
        Else
            lbl_warning.Text = ""
        End If

    End Sub

    ''' <summary>
    ''' Name:           Page_PreInit()
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    Copied from Franchise form and kept same functionality
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Name:           Find Employee Details
    ''' Created by:     Kumaran
    ''' Date:           Sep2015
    ''' Description:    To find employee home store code, company code, company group code 
    ''' </summary>
    ''' <remarks></remarks>
    Private Function FindEmployeeDetails() As Boolean
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim sqlString As StringBuilder
        Dim ds As New DataSet
        Dim Mydatareader As OracleDataReader
        Dim homeStoreCode As String = ""
        Dim companyCode As String = ""
        Dim coGroupCode As String = ""
        lbl_versionInfo.Text = gs_version
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        If (String.IsNullOrEmpty(Session("EMP_CD"))) Then
            Return False
        End If

        sqlString = New StringBuilder("SELECT e.home_store_cd, s.co_cd, cg.co_grp_cd FROM emp e, store s, co_grp cg  ")
        sqlString.Append(" WHERE e.home_store_cd = s.store_cd AND cg.co_cd = s.co_cd AND e.emp_cd = UPPER(:p_emp_cd)")
        objCmd.CommandText = sqlString.ToString()
        objCmd.Connection = conn
        objCmd.Parameters.Add(":p_emp_cd", OracleType.VarChar)
        objCmd.Parameters(":p_emp_cd").Value = Session("EMP_CD").ToString().ToUpper()
        Try
            conn.Open()
            Mydatareader = DisposablesManager.BuildOracleDataReader(objCmd)
            If Mydatareader.Read() Then

                companyCode = Mydatareader.Item("CO_CD").ToString()

                homeStoreCode = Mydatareader.Item("HOME_STORE_CD").ToString()
                If IsNothing(Session("HOME_STORE_CD")) Then
                    Session("HOME_STORE_CD") = homeStoreCode
                End If

                coGroupCode = Mydatareader.Item("CO_GRP_CD").ToString()
                If IsNothing(Session("CO_GRP_CD")) Then
                    Session("CO_GRP_CD") = coGroupCode
                End If
            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        If String.IsNullOrEmpty(homeStoreCode) Then
            lbl_warning.Text = Resources.CustomMessages.MSG0037
            conn.Close()
            Return False
        ElseIf String.IsNullOrEmpty(coGroupCode) Then
            lbl_warning.Text = Resources.CustomMessages.MSG0038
            conn.Close()
            Return False
        End If
        Return True
    End Function

End Class