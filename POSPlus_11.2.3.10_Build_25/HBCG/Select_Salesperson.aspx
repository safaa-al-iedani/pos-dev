<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Select_Salesperson.aspx.vb"
    Inherits="Select_Salesperson" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select Salesperson</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="styles.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table height="100%" width="100%">
                <tr>
                    <td valign="bottom" align="center" style="height: 172px">
                        <font color="white">Please select the appropriate salesperson:</font>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="center">
                        <asp:ListBox ID="cbo_salespeople" runat="server" AutoPostBack="true" Width="282px"></asp:ListBox>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
