<%@ Page Language="VB" MasterPageFile="~/Regular.master" AutoEventWireup="false"
    CodeFile="Lead_Stage.aspx.vb" Inherits="Lead_Stage" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript">
        function SelectAndClosePopupEmailTemplate() {
            ASPxPopupControl1.Hide();
        }
        function SelectAndClosePopup2(){
            ASPxPopupControl2.Hide();
        }
    </script>

    <asp:Label ID="lbl_Summary" runat="server" Text=""></asp:Label>
    <br />
    <asp:Label ID="lbl_itm_summary" runat="server" Text=""></asp:Label>
    <br />
    <br />
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btn_commit" runat="server" Text="<%$ Resources:LibResources, Label98 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="ImageButton1" runat="server" Text="<%$ Resources:LibResources, Label102 %>" OnClick="ImageButton1_Click1">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxCheckBox ID="chk_email" runat="server" Text="<%$ Resources:LibResources, Label182 %>" OnCheckedChanged="chk_email_CheckedChanged">
                </dx:ASPxCheckBox>
            </td>
            <td>
                <dx:ASPxCheckBox ID="CheckBox1" runat="server" Checked="True" Text="<%$ Resources:LibResources, Label429 %>">
                </dx:ASPxCheckBox>
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lbl_Status" runat="server" Width="320px"></asp:Label>
    <br />
    <asp:Label ID="lbl_Follow_Up" runat="server" Width="320px" Text="<%$ Resources:LibResources, Label240 %>">Internal Salesperson Notes:</asp:Label>
    <br />
    <asp:TextBox ID="txt_Follow_up" runat="server" Height="42px" Width="500px" Rows="3"
        TextMode="MultiLine" CssClass="style5"></asp:TextBox>
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" ClientInstanceName="ASPxPopupControl2" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label180 %>" Height="64px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="494px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <table class="style5">
                    <tr>
                        <td valign="middle" align="center">
                            <img src="images/icons/Symbol-Stop.png" />
                        </td>
                        <td>
                            &nbsp;&nbsp;
                        </td>
                        <td valign="middle">
                            <asp:Label ID="lbl_warning" runat="server" Text="<%$ Resources:LibResources, Label29 %>"
                                Font-Bold="True"></asp:Label>
                            <br />
                            <br />
                            <asp:TextBox ID="txt_email" runat="server" Height="16px" CssClass="style5" Width="293px"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Button ID="btn_Proceed" runat="server" Text="<%$ Resources:LibResources, Label12 %>" CssClass="style5" />
                            &nbsp;&nbsp;<asp:Button ID="btn_Cancel" runat="server" Text="<%$ Resources:LibResources, Label350 %>" CssClass="style5" />
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="ASPxPopupControl1" AllowDragging="True"
        AllowResize="True" ContentUrl="~/Email_Templates.aspx" HeaderText="Update Email Template"
        Height="345px" Width="546px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <br />
    <br />
</asp:Content>
