<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Ecofee.aspx.vb" Inherits="Ecofee"    %>
<%--  CodeFile="SRRTD.aspx.vb" Inherits="SRRTD" meta:resourcekey="PageResource1" UICulture="auto" %>--%>
<%--
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>  --%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    

    <table> 
        
        <tr>
            <td align="left">
                <table>

                    <tr>
                        <td align="left">

                            <asp:Label runat="server" Text="Co_grp_cd" ID="Label4" Width="160px"></asp:Label>
                           
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDown_co_grp_cd" runat="server"></asp:DropDownList>
                        </td>

                        <td >
                            <asp:CheckBox ID="CheckBox_end_dt" runat="server" Text="Search end_date as 12/31/2049 only" ></asp:CheckBox>

                        </td>
                          
                          

                    </tr>

                </table>
            </td>
            </tr>



            <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Prov  " ID="lbl_w_d"  Width="160px" ></asp:Label>
                          
                        </td>
                        <td>
                            <asp:DropDownList ID="drpdwn_prov" runat="server"  Width="60px"></asp:DropDownList>
                            
                        </td>
                        
                        
                    </tr>
                </table>
            </td>
        </tr>
            <tr>
               
            <td align="left">
                <table>
                    <tr>
                        
                        <td>
                         <asp:Label runat="server" Text="Style" ID="lbl_inv_type" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             <asp:textbox ID="text_style" runat="server"      AutoPostBack="True"  Width="60px"></asp:textbox>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
             <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Match_sku" ID="lbl_itm_type" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             <asp:textbox ID="text_match_sku" runat="server"  Width="60px"></asp:textbox>
                             
                        </td>
                    </tr>
                </table>
            </td>
        </tr>





         <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="Eff_dt" ID="Label1" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             
                        <asp:Label ID="Label5" runat="server" style="text-align: start" Text="" ></asp:Label>
               <asp:Calendar font-size="Xx-Small" hieght="1px" Width="1px" ID="can_eff_date" DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server">
               <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
               </asp:Calendar>
                             
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
       
         <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">

                         <asp:Label runat="server" Text="End_dt" ID="Label2" Width="160px"></asp:Label>
                          
                        </td>
                        <td>
                             
                        <asp:Label ID="Label3" runat="server" style="text-align: start" Text="" ></asp:Label>
               <asp:Calendar font-size="Xx-Small" hieght="1px" Width="1px" ID="can_end_date" DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server">
               <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
               </asp:Calendar>
                             
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
       
         
        <tr>
            <td align="right">
                <table>
                    
                                   

                    <tr align="right">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btn_search" runat="server" Text="Search" onclick="click_btn_search" Width="60px"/> </asp:Button>
                                         
                                    </td>
                                    <td>
                                         <asp:Button ID="btn_clr_screen" runat="server" Text="Clear" onclick="btn_clr_screen_Click" Width="60px"/> </asp:Button>
                                        
                                    </td>
                                    <td  align="right">

                            <asp:Button ID="btn_new" runat="server" Text="NEW"  onclick="click_btn_insert" Width="60px"/> </asp:Button>

                        </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
         
        
            <tr>
            <td>
                <asp:DataGrid ID="GV_SRR" runat="server"  Width="680px" BorderColor="Black"  runat="server" AutoPostBack="true"
                   AutoGenerateColumns="False" CellPadding="2" DataKeyField="rowid"   OnDeleteCommand="DoItemDelete"
                   Height="16px" AlternatingItemStyle-BackColor="Beige"  PagerStyle-NextPageText=" " PagerStyle-PrevPageText=" ">            
                    
               
                <Columns>
                    
                    
                
                  <asp:BoundColumn DataField="rowid" HeaderText="Rowid" Visible="False"   ReadOnly="True" /> 
                   <asp:BoundColumn DataField="prov" HeaderText="Province"    ReadOnly="True" Visible="true" />
                  <asp:BoundColumn DataField="style" HeaderText="Style" ReadOnly="false" Visible="False"  HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center" />    
                  <asp:BoundColumn DataField="match_sku" HeaderText="Match_sku"    ReadOnly="false" Visible="False" />
                <asp:BoundColumn DataField="co_grp_cd" HeaderText="Co_grp_cd"    ReadOnly="True" Visible="true" />
                  <asp:BoundColumn DataField="eco_fee" HeaderText="Ecofee"    DataFormatString="{0:0.00}" ReadOnly="True" Visible="False" />
                   
                  <asp:BoundColumn DataField="eff_dt" HeaderText="Eff_dt"             ReadOnly="false" Visible="False" />   
                     <asp:BoundColumn DataField="end_dt" HeaderText="End_dt"             ReadOnly="false" Visible="False" />   
                        

                  
                 
                    <asp:TemplateColumn HeaderText="Style" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:textbox ID="txt_style"  runat="server" AutoPostBack="true"  OnTextChanged="txt_style_textchanged" ReadOnly="false"  Width="60px"></asp:textbox>
                     </ItemTemplate>
                   </asp:TemplateColumn> 

                  <asp:TemplateColumn HeaderText="Match_sku" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:textbox ID="txt_match_sku"  runat="server" AutoPostBack="true" OnTextChanged="txt_match_sku_textchanged" Width="80px" ></asp:textbox>
                     </ItemTemplate>
                   </asp:TemplateColumn> 

                    <asp:TemplateColumn HeaderText="Des" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="245px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:textbox ID="txt_Des"  runat="server" AutoPostBack="true"   ReadOnly="true" Width="300px"></asp:textbox>
                     </ItemTemplate>
                   </asp:TemplateColumn>
                   
                     
                    <asp:TemplateColumn HeaderText="Eco_fee" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:textbox ID="txt_eco_fee"  runat="server" AutoPostBack="true" DataFormatString="{0:0.00}"  OnTextChanged="txt_eco_fee_textchanged" Width="60px"></asp:textbox>
                     </ItemTemplate>
                   </asp:TemplateColumn> 

                     <asp:TemplateColumn HeaderText="Eff_date" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:textbox ID="txt_eff_date"  runat="server" AutoPostBack="true"  OnTextChanged="txt_eff_date_TextChanged"     ></asp:textbox>
                     </ItemTemplate>
                   </asp:TemplateColumn> 

                     <asp:TemplateColumn HeaderText="End_date" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="145px">
                     <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Center" />
                   <ItemTemplate>
                    <asp:textbox ID="txt_end_date"  runat="server" AutoPostBack="true"  OnTextChanged="txt_end_date_TextChanged" ></asp:textbox>
                     </ItemTemplate>
                   </asp:TemplateColumn> 

                         
  
                
                       <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"
                HeaderText="<%$ Resources:LibResources, Label150 %>">
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>

                </Columns>

    </asp:DataGrid>
                
                </td>
                <td>
                    <asp:Button ID="btn_update" runat="server" Text="Update" OnClick="click_btn_update" Width="60px" />
                  
                    </td>
       </tr>
       
    </table>
     <asp:Label runat="server"   ID="lbl_msg"  Width="100%" ForeColor="Red"></asp:Label>
     <asp:Label runat="server"   ID="lbl_warning"  Width="100%" ForeColor="Red"></asp:Label>
  <%-- <dx:aspxlabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:aspxlabel> --%>
    <br />
    <br />
    
    <div runat="server" id="div_version">
          
        <asp:label ID="lbl_versionInfo" runat="server" >
       &nbsp;&nbsp;&nbsp; </asp:label>  
    </div>
</asp:Content>   
