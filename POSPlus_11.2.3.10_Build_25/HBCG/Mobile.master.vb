Imports System.IO
Imports ErrorManager

Partial Class Mobile
    Inherits System.Web.UI.MasterPage

    Public Sub Catch_errors(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs)

        Dim IMSLogError As New IMSErrorLogger(Server.GetLastError, Session, Request)
        'log the error to the event log, database, and/or a file. The web.config specifies where to log it
        'and this class will read those settings to determine that. 
        IMSLogError.LogError()

        Response.Redirect("Error_handling.aspx")

    End Sub


    Public Sub Populate_Totals()

        If Session("sub_total") & "" <> "" Then
            lblSubtotal.Text = FormatCurrency(CDbl(Session("sub_total")))
            If IsNumeric(Session("DEL_CHG")) Then
                lblDelivery.Text = FormatNumber(CDbl(Session("DEL_CHG")), 2)
            Else
                lblDelivery.Text = FormatNumber(0, 2)
            End If
            If IsNumeric(Session("SETUP_CHG")) Then
                If IsNumeric(lblDelivery.Text) Then
                    lblDelivery.Text = FormatNumber(CDbl(lblDelivery.Text) + FormatNumber(CDbl(Session("SETUP_CHG")), 2), 2)
                Else
                    lblDelivery.Text = FormatNumber(CDbl(Session("SETUP_CHG")), 2)
                End If
            End If
            If IsNumeric(Session("TAX_CHG")) And String.IsNullOrEmpty(Session("tet_cd")) Then
                lblTax.Text = FormatNumber(CDbl(Session("TAX_CHG")), 2)
            Else
                lblTax.Text = FormatNumber(0, 2)
            End If
            lblTotal.Text = FormatCurrency((CDbl(lblSubtotal.Text)) + FormatNumber(CDbl(lblTax.Text)) + FormatNumber(CDbl(lblDelivery.Text)), 2)
            Session("grand_total") = lblTotal.Text
        End If
        If Session("payment") & "" <> "" Then
            lbl_Payments.Text = FormatCurrency(CDbl(Session("payments")), 2)
        Else
            lbl_Payments.Text = FormatCurrency(0, 2)
        End If
        If IsNumeric(lbl_Payments.Text) And IsNumeric(lblTotal.Text) Then
            lbl_balance.Text = FormatCurrency(CDbl(lblTotal.Text) - CDbl(lbl_Payments.Text), 2)
        Else
            lbl_balance.Text = FormatCurrency(0, 2)
        End If

    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lbl_header.Text = "<font color=red>* TRAIN MODE *</font>"
        End If

        Populate_Totals()

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("logout.aspx")
        Else
            ''mm -swp,20,2016 - back button security concern
            Response.ClearHeaders()
            Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
            Response.AddHeader("Pragma", "no-cache")
        End If

        Dim ProgressCount As Integer
        Dim strURL As String
        Dim arrayURL As Array
        Dim pagename As String
        Dim HEADER As String
        Dim LEAD As String
        LEAD = Request("LEAD")

        If Session("EMP_FNAME") & "" <> "" And Session("EMP_LNAME") & "" <> "" Then
            HEADER = Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
            If Session("CUST_LNAME") & "" <> "" Then
                HEADER = HEADER & " working with customer " & Left(Session("CUST_LNAME"), 1) & LCase(Right(Session("CUST_LNAME"), Len(Session("CUST_LNAME")) - 1))
            End If
            lbl_Header1.Text = HEADER
        End If

        strURL = Request.ServerVariables("SCRIPT_NAME")
        arrayURL = Split(strURL, "/", -1, 1)
        pagename = arrayURL(UBound(arrayURL))

        hpl_help.NavigateUrl = "#"
        hpl_help.Attributes.Add("onClick", "window.open('help.aspx?pagename=" & pagename & "','helpwindow','status=0,toolbar=0,menubar=0,location=0,resizable=1,scrollbars=1,height=500,width=450');return false;")

        If InStr(LCase(Request.ServerVariables("SCRIPT_NAME")), "startup.aspx") > 0 Then
            back_btn.Visible = False
        End If

        If LEAD = "TRUE" Then
            HyperLink7.Enabled = False
            HyperLink7.Visible = False
            img_payment.Visible = False

            HyperLink1.NavigateUrl = "startup.aspx?LEAD=TRUE"
            HyperLink2.NavigateUrl = "customer.aspx?LEAD=TRUE"
            hlOrder.NavigateUrl = "order_detail.aspx?LEAD=TRUE"
            HyperLink3.NavigateUrl = "discount.aspx?LEAD=TRUE"
            HyperLink4.NavigateUrl = "marketing.aspx?LEAD=TRUE"
            HyperLink5.NavigateUrl = "comments.aspx?LEAD=TRUE"
            HyperLink6.NavigateUrl = "delivery.aspx?LEAD=TRUE"
            save_link.NavigateUrl = "lead_stage.aspx?LEAD=TRUE"

            back_btn.ImageUrl = "images/icons/backblue.gif"
            next_btn.ImageUrl = "images/icons/nextblue.gif"

            'Display should change to handle data necessary for a prospect entry ONLY!
            Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Relationship Entry"
            save_link.Visible = True

            hpl_help.NavigateUrl = "#"
            hpl_help.Attributes.Add("onClick", "window.open('help.aspx?pagename=" & pagename & "','helpwindow','status=0,toolbar=0,menubar=0,location=0,resizable=1,scrollbars=1,height=500,width=450');return false;")

            If InStr(LCase(Request.ServerVariables("SCRIPT_NAME")), "startup.aspx?LEAD=TRUE") > 0 Then
                back_btn.Visible = False
            End If
            If InStr(LCase(Request.ServerVariables("SCRIPT_NAME")), "delivery.aspx?LEAD=TRUE") > 0 Then
                next_btn.Visible = False
            End If
            Select Case LCase(pagename)
                Case "startup.aspx"
                    next_link.NavigateUrl = "Customer.aspx?LEAD=TRUE"
                    lblTabID.Text = Resources.LibResources.Label465
                    If Session("wr_dt") & "" = "" Then
                        save_link.Visible = False
                    End If
                Case "customer.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Startup.aspx?LEAD=TRUE"
                    lblTabID.Text = Resources.LibResources.Label463
                Case "prs.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Startup.aspx?LEAD=TRUE"
                    lblTabID.Text = Resources.LibResources.Label464
                Case "cust_edit.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Startup.aspx?LEAD=TRUE"
                    lblTabID.Text = Resources.LibResources.Label462
                Case "order.aspx"
                    next_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Customer.aspx?LEAD=TRUE"
                    lblTabID.Text = "RELATIONSHIP : ORDER : FIND MERCHANDISE"
                Case "order_detail.aspx"
                    next_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Customer.aspx?LEAD=TRUE"
                    lblTabID.Text = Resources.LibResources.Label731
                Case "discount.aspx"
                    If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                        next_link.NavigateUrl = "Comments.aspx?LEAD=TRUE"
                    Else
                        next_link.NavigateUrl = "Marketing.aspx?LEAD=TRUE"
                    End If
                    back_link.NavigateUrl = "Order_detail.aspx?LEAD=TRUE"
                    'Daniela french
                    lblTabID.Text = Resources.LibResources.Label730
                Case "inventoryresults.aspx"
                    next_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Customer.aspx?LEAD=TRUE"
                    lblTabID.Text = "RELATIONSHIP : ORDER : SEARCH RESULTS"
                Case "marketing.aspx"
                    next_link.NavigateUrl = "Comments.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                    lblTabID.Text = "RELATIONSHIP : OTHER : MARKETING"
                Case "comments.aspx"
                    next_link.NavigateUrl = "Delivery.aspx?LEAD=TRUE"
                    If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                        back_link.NavigateUrl = "Discount.aspx?LEAD=TRUE"
                    Else
                        back_link.NavigateUrl = "Marketing.aspx?LEAD=TRUE"
                    End If
                    lblTabID.Text = Resources.LibResources.Label732
                Case "delivery.aspx"
                    next_link.NavigateUrl = "Lead_Stage.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Comments.aspx?LEAD=TRUE"
                    lblTabID.Text = "RELATIONSHIP : OTHER : DELIVERY ZONE"
                Case "deliverydate.aspx"
                    next_link.NavigateUrl = "Order_Stage.aspx?LEAD=TRUE"
                    back_link.NavigateUrl = "Comments.aspx?LEAD=TRUE"
                    lblTabID.Text = "RELATIONSHIP : OTHER : DELIVERY DATE"
                Case "lead_stage.aspx"
                    next_link.Enabled = False
                    back_link.Enabled = False
                    save_link.Enabled = False
                    next_link.Visible = False
                    back_link.Visible = False
                    save_link.Visible = False
                    lblTabID.Text = "RELATIONSHIP : COMPLETION"
            End Select
            hlOrder.NavigateUrl = "order_detail.aspx?LEAD=TRUE"
            ProgressCount = 0

            If (Not SessVar.discCd.isEmpty) Then
                img_disc.ImageUrl = "images/lead_icons/interest-icon.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_disc.ImageUrl = "images/lead_icons/interest-icon.gif"
            End If

            If Session("PD") = "D" Then
                If Session("zone_cd") & "" <> "" Then
                    If Session("zone_cd") & "" = "" Then
                        img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                    Else
                        img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                        ProgressCount = ProgressCount + 1
                    End If
                Else
                    img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                End If
            Else
                If Session("del_dt") & "" <> "" Then
                    If Session("del_dt") & "" = "" Then
                        img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                    Else
                        img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                        ProgressCount = ProgressCount + 1
                    End If
                Else
                    img_del_dt.ImageUrl = "images/lead_icons/school-van-icon.gif"
                End If
            End If
            If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                img_mrkt.ImageUrl = "images/icons/marketing-grey.gif"
                ProgressCount = ProgressCount + 1
            Else
                If Session("mark_cd") & "" <> "" Then
                    img_mrkt.ImageUrl = "images/icons/marketingCheck.gif"
                    ProgressCount = ProgressCount + 1
                Else
                    img_mrkt.ImageUrl = "images/icons/marketing.gif"
                End If
            End If
            If Session("cust_cd") & "" <> "" Then
                img_cust.ImageUrl = "images/lead_icons/contact-information.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_cust.ImageUrl = "images/lead_icons/contact-information.gif"
            End If
            If Session("itemid") & "" <> "" Then
                img_order.ImageUrl = "images/lead_icons/shopping-cart.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_order.ImageUrl = "images/lead_icons/shopping-cart.gif"
            End If
            If Session("comments") & "" <> "" Then
                img_cmnts.ImageUrl = "images/lead_icons/sales-order-icon.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_cmnts.ImageUrl = "images/lead_icons/sales-order-icon.gif"
            End If
            If Session("tran_dt") & "" <> "" And Session("store_cd") & "" <> "" And Session("slsp1") & "" <> "" And Session("pct_1") & "" <> "" And Session("PD") & "" <> "" And Session("ord_srt") & "" <> "" Then
                ProgressCount = ProgressCount + 4
                img_start.ImageUrl = "images/lead_icons/school-bag-icon.gif"
            Else
                img_start.ImageUrl = "images/lead_icons/school-bag-icon.gif"
            End If
        Else
            'Data validation and screen displays that are pertinent to a sale entry
            Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Sales Order Entry"
            save_link.Visible = True

            Select Case LCase(pagename)
                Case "startup.aspx"
                    next_link.NavigateUrl = "Customer.aspx"
                    lblTabID.Text = "SALE : CUSTOMER : STARTUP"
                Case "customer.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx"
                    back_link.NavigateUrl = "Startup.aspx"
                    lblTabID.Text = "SALE : CUSTOMER : FIND CUSTOMER"
                Case "prs.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx"
                    back_link.NavigateUrl = "Startup.aspx"
                    lblTabID.Text = "SALE : CUSTOMER : SEARCH RESULTS"
                Case "cust_edit.aspx"
                    next_link.NavigateUrl = "Order_detail.aspx"
                    back_link.NavigateUrl = "Startup.aspx"
                    lblTabID.Text = "SALE : CUSTOMER : EDIT/DETAILS"
                Case "order.aspx"
                    next_link.NavigateUrl = "Discount.aspx"
                    back_link.NavigateUrl = "Customer.aspx"
                    lblTabID.Text = "SALE : ORDER : FIND MERCHANDISE"
                Case "order_detail.aspx"
                    next_link.NavigateUrl = "Discount.aspx"
                    back_link.NavigateUrl = "Customer.aspx"
                    lblTabID.Text = "SALE : ORDER : SHOPPING BAG"
                Case "discount.aspx"
                    If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                        next_link.NavigateUrl = "Comments.aspx"
                    Else
                        next_link.NavigateUrl = "Marketing.aspx"
                    End If
                    back_link.NavigateUrl = "Order_detail.aspx"
                    lblTabID.Text = "SALE : ORDER : DISCOUNT"
                Case "marketing.aspx"
                    next_link.NavigateUrl = "Comments.aspx"
                    back_link.NavigateUrl = "Discount.aspx"
                    lblTabID.Text = "SALE : ORDER : MARKETING"
                Case "payment.aspx"
                    next_link.NavigateUrl = "Order_Stage.aspx"
                    back_link.NavigateUrl = "Delivery.aspx"
                    lblTabID.Text = "SALE : PAYMENT : ADD/CHANGE"
                Case "inventoryresults.aspx"
                    next_link.NavigateUrl = "Discount.aspx"
                    back_link.NavigateUrl = "Customer.aspx"
                    lblTabID.Text = "SALE : ORDER : SEARCH RESULTS"
                Case "comments.aspx"
                    next_link.NavigateUrl = "Delivery.aspx"
                    If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                        back_link.NavigateUrl = "Discount.aspx"
                    Else
                        back_link.NavigateUrl = "Marketing.aspx"
                    End If
                    lblTabID.Text = "SALE : OTHER : COMMENTS"
                Case "delivery.aspx"
                    next_link.NavigateUrl = "Payment.aspx"
                    back_link.NavigateUrl = "Comments.aspx"
                    If Session("PD") = "P" Then
                        lblTabID.Text = "SALE : OTHER : PICKUP DATE"
                    Else
                        lblTabID.Text = "SALE : OTHER : DELIVERY ZONE"
                    End If
                Case "deliverydate.aspx"
                    next_link.NavigateUrl = "Payment.aspx"
                    back_link.NavigateUrl = "Comments.aspx"
                    lblTabID.Text = "SALE : OTHER : DELIVERY DATE"
                Case "order_stage.aspx"
                    next_link.Enabled = False
                    back_link.Enabled = False
                    save_link.Enabled = False
                    next_link.Visible = False
                    back_link.Visible = False
                    save_link.Visible = False
                    lblTabID.Text = "SALE : ORDER COMPLETION"
            End Select
            Select Case Session("ord_tp_cd")
                Case "CRM"
                    lblTabID.Text = Replace(lblTabID.Text, "SALE", "<font color=""red"">RETURN</font>")
                Case "MCR"
                    lblTabID.Text = Replace(lblTabID.Text, "SALE", "<font color=""red"">MISCELLANEOUS CREDIT</font>")
                Case "MDB"
                    lblTabID.Text = Replace(lblTabID.Text, "SALE", "<font color=""red"">MISCELLANEOUS DEBIT</font>")
            End Select
            HyperLink8.NavigateUrl = "~/newmain.aspx?verify_save=TRUE"
            ProgressCount = 0
            If (Not SessVar.discCd.isEmpty) Then
                img_disc.ImageUrl = "images/icons/discountCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_disc.ImageUrl = "images/icons/discount.gif"
            End If
            If Session("del_dt") & "" <> "" Then
                If Not IsDate(Session("del_dt")) Then
                    img_del_dt.ImageUrl = "images/icons/delivery.gif"
                Else
                    img_del_dt.ImageUrl = "images/icons/deliveryCheck.gif"
                    ProgressCount = ProgressCount + 1
                End If
            Else
                img_del_dt.ImageUrl = "images/icons/delivery.gif"
            End If
            If Session("cust_cd") & "" <> "" Then
                img_cust.ImageUrl = "images/icons/customerCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_cust.ImageUrl = "images/icons/customer.gif"
            End If
            If Session("itemid") & "" <> "" Then
                img_order.ImageUrl = "images/icons/orderCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_order.ImageUrl = "images/icons/order.gif"
            End If
            If Session("comments") & "" <> "" Then
                img_cmnts.ImageUrl = "images/icons/commentsCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_cmnts.ImageUrl = "images/icons/comments.gif"
            End If
            If ConfigurationManager.AppSettings("show_marketing").ToString = "N" Then
                img_mrkt.ImageUrl = "images/icons/marketing-grey.gif"
                ProgressCount = ProgressCount + 1
            Else
                If Session("mark_cd") & "" <> "" Then
                    img_mrkt.ImageUrl = "images/icons/marketingCheck.gif"
                    ProgressCount = ProgressCount + 1
                Else
                    img_mrkt.ImageUrl = "images/icons/marketing.gif"
                End If
            End If
            If Session("payment") & "" <> "" Then
                img_payment.ImageUrl = "images/icons/paymentCheck.gif"
                ProgressCount = ProgressCount + 1
            Else
                img_payment.ImageUrl = "images/icons/payment.gif"
            End If
            If Session("tran_dt") & "" <> "" And Session("store_cd") & "" <> "" And Session("slsp1") & "" <> "" And Session("pct_1") & "" <> "" And Session("PD") & "" <> "" Then
                ProgressCount = ProgressCount + 2
                img_start.ImageUrl = "images/icons/creditCheck.gif"
            Else
                img_start.ImageUrl = "images/icons/credit.gif"
            End If
        End If

    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
    End Sub

End Class

