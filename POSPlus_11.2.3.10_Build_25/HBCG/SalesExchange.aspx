<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" CodeFile="SalesExchange.aspx.vb" Inherits="SalesExchange" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Src="~/Usercontrols/ZoneDisplay.ascx" TagPrefix="ucZones" TagName="ZoneDisplay" %>
<%@ Register Src="~/Usercontrols/MultiWarranties.ascx" TagPrefix="ucWar" TagName="MultiWarranties" %>
<%@ Register Src="~/Usercontrols/RelatedSKU.ascx" TagPrefix="ucRelSku" TagName="RelatedSKU" %>
<%@ Register Src="~/Usercontrols/ManagerOverride.ascx" TagPrefix="ucMgr" TagName="ManagerOverride" %>
<%@ Register Src="~/Usercontrols/SummaryPriceChangeApproval.ascx" TagPrefix="ucPC" TagName="SummaryPriceChangeApproval" %>
<%@ Register Src="~/Usercontrols/TaxUpdate.ascx" TagPrefix="ucTax" TagName="TaxUpdate" %>
<%@ Register TagPrefix="ucZone" Src="~/Usercontrols/ZoneCode.ascx" TagName="ZonePopup" %>
<%@ Register TagPrefix="ucDeliveryDate" Src="~/Usercontrols/DeliveryDateDisplay.ascx" TagName="DeliveryDatePopup" %>


<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v13.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .Table {
            display: table;
        }

        .Title {
            display: table-caption;
            text-align: left;
        }

        .Heading {
            display: table-row;
            text-align: left;
        }

        .Row {
            display: table-row;
        }

        .Cell {
            width: 100px;
            display: table-cell;
            border: none;
            padding-left: 5px;
            padding-right: 5px;
            vertical-align: top;
        }

        .Cell1 {
            width: 125px;
            display: table-cell;
            border: none;
            padding-left: 5px;
            padding-right: 5px;
            vertical-align: top;
        }

         .gridRow-static-height {
            height: 15px;
        }
        .ClearTheme {
            background:none;
        }

    </style>
    <asp:HiddenField ID="hidIsCashierActivated" runat="server" Value="True" />
    <asp:HiddenField ID="hidIsZoneSelected" runat="server" Value="False" />
    <asp:HiddenField ID="hidStoreZipCode" runat="server" />
    <asp:HiddenField ID="hidWrittenStoreZipCode" runat="server" Value="" />

    <div style="display: table; width: 100%;">
        <div style="display: table-row">
            <div style="display: table-cell; white-space: nowrap;">
                <label style="margin-left: 5px;"><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label18 %>" /></label>
            </div>
        </div>
        <br />

       <div style="display: table; width: 100%;">
        <div style="display: table-row">
              <div style="display: table-cell; white-space: nowrap;">
                 
                <table id="tblOriginalSale" runat="server" cellpadding="0" cellspacing="0" style="width: 880px">
                    <tr>
                        <td valign="top" align="left">
                            <dx:ASPxTextBox ID="txt_del_doc_num" runat="server" NullText="<%$ Resources:SalesExchange, Label19 %>"  Width="170px"></dx:ASPxTextBox>
                        </td>
                        <td valign="top"  align="left">
                           <dx:ASPxComboBox ID="ddlReturnReason" runat="server" IncrementalFilteringMode="StartsWith" OnLoad="ddlReturnReason_Load" Width="200px"></dx:ASPxComboBox>
                        </td>
                         <td valign="top"  align="left">
                               <dx:ASPxButton ID="btn_submit" runat="server" Text="<%$ Resources:SalesExchange, Label28 %>" OnClick="btn_submit_Click" Width="70px"></dx:ASPxButton>
                         </td>
                         <td valign="top"  align="left">
                             <dx:ASPxButton ID="btn_Save" runat="server" Text="<%$ Resources:SalesExchange, Label29 %>" OnClick="btn_submit_Click" Visible="False" Width="100px">
                                   <ClientSideEvents Click="function(s,e){  s.SendPostBack('Click'); s.SetEnabled(false);}" />
                             </dx:ASPxButton>
                         </td>
                         <td valign="top"  align="left">
                             <dx:ASPxButton ID="btn_Clear" Visible="False" runat="server" Text="<%$ Resources:SalesExchange, Label30 %>" OnClick="btn_submit_Click" Width="50px"></dx:ASPxButton>
                         </td>
                         <td valign="top"  align="right">
                              <dx:ASPxLabel ID="lbl_cust_cd" runat="server" Text="" Width="100px"></dx:ASPxLabel>
                         </td>
                         <td valign="top"  align="right">
                               <dx:ASPxLabel ID="lbl_fname" runat="server" Text="" Width="100px"></dx:ASPxLabel>
                         </td>
                         <td valign="top"  align="right">
                              <dx:ASPxLabel ID="lbl_lname" runat="server" Text=""  Width="100px" ></dx:ASPxLabel>
                         </td>
                    </tr>
                </table>
               </div>
          </div>
      </div>


      
        <div style="display: table; width: 100%;">
            <fieldset>
                <legend><b><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label31 %>" /></b></legend>
                <div style="display: table-row;">
                    <div class="Cell">
                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label32 %>" /></label>
                    </div>
                    <div class="Cell">
                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label33 %>" /></label>
                    </div>
                    <div class="Cell">
                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label34 %>" /></label>
                    </div>
                    <div class="Cell">
                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label35 %>" /></label>
                    </div>
                    <div class="Cell">
                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label36 %>" /></label>
                    </div>
                     <div class="Cell">
                         <asp:Label ID="lbl_original_zone" runat="server" Text="<%$ Resources:SalesExchange, Label37 %>"></asp:Label>
                    </div>

                    <div class="Cell">
                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label38 %>" /></label>
                    </div>
                    <div class="Cell">
                        <label>Tax(</label>
                        <asp:LinkButton ID="lnk_origsale_TaxCode" runat="server" Enabled="false" CssClass="dxeBase_Office2010Blue">
                        </asp:LinkButton>
                        <label>):</label>
                    </div>
                    <div class="Cell">
                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label40 %>" /></label>
                    </div>

                    <div class="Cell">
                    </div>
                </div>
                <div style="display: table-row;">
                    <div class="Cell">
                        <dx:ASPxTextBox ID="txt_del_doc_existing" runat="server" Width="120px" Enabled="False" ReadOnly="true"></dx:ASPxTextBox>
                    </div>
                    <div class="Cell">
                        <asp:DropDownList ID="cbo_PD" runat="server" Width="35px" AutoPostBack="True" Enabled="False">
                            <asp:ListItem>P</asp:ListItem>
                            <asp:ListItem>D</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="Cell">
                        <dx:ASPxTextBox ID="txt_store_cd" runat="server" Width="27px" Enabled="False"></dx:ASPxTextBox>
                        <asp:TextBox ID="txt_ord_tp_cd" runat="server" Visible="false">
                        </asp:TextBox>
                    </div>
                    <div class="Cell">
                        <dx:ASPxTextBox ID="txtOriginalDelChgs" Width="60px" runat="server" Enabled="false"></dx:ASPxTextBox>
                    </div>
                    <div class="Cell">
                        <dx:ASPxTextBox ID="txtOriginalSetupChgs" Width="60px" runat="server" Enabled="false"></dx:ASPxTextBox>
                    </div>
                    <div class="Cell">
                        <dx:ASPxTextBox ID="txtOriginalZone" Width="60px" runat="server" Enabled="false"></dx:ASPxTextBox>
                    </div>
                    <div class="Cell">
                        <dx:ASPxTextBox ID="txt_origsale_subtotal" runat="server" Width="80px" Enabled="false" ></dx:ASPxTextBox>
                    </div>
                    <div class="Cell">
                        <dx:ASPxTextBox ID="txt_origsale_taxes" runat="server" Width="80px" Enabled="false" ></dx:ASPxTextBox>
                    </div>
                    <div class="Cell">
                        <dx:ASPxTextBox ID="txt_origsale_grand" runat="server" Width="80px" Enabled="false" >
                             <%--<masksettings Mask="$<0..9999g>" />--%>
                        </dx:ASPxTextBox>
                    </div>
                   
                </div>
                <div style="display: table-row;">
                </div>
                <div style="display: table-row;">
                </div>
            </fieldset>
        </div>
        <br />

        <div style="display: table;">
            <div style="display: table-row;">
                <div style="display: table-cell; vertical-align: middle; padding-left: 4px;">
                    <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label41 %>" /></label>
                </div>
                <div style="display: table-cell; vertical-align: middle; padding-left: 4px;">
                </div>
                <div style="display: table-cell; vertical-align: middle; padding-left: 4px;">
                    <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label42 %>" /></label>
                </div>
                <div style="display: table-cell; vertical-align: middle; padding-left: 4px;">
                    <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label43 %>" /></label>
                </div>
            </div>
            <div style="display: table-row;">
                <div style="display: table-cell; vertical-align: middle; padding-left: 4px;">
                    <dx:ASPxDateEdit ID="cbo_pd_date" runat="server"></dx:ASPxDateEdit>
                    <asp:HiddenField ID="hidOriginalDate" runat="server" />
                </div>
                <div style="display: table-cell; vertical-align: middle; padding-left: 4px;">
                    <asp:ImageButton ID="btnDateLookup" runat="server" Visible="false"  ImageUrl="~/images/icons/delivery.gif" Width="20px" />
                </div>
                <div style="display: table-cell; vertical-align: middle; padding-left: 4px;">
                    <dx:ASPxComboBox ID="cboOrdSrt" AutoPostBack="True" runat="server"
                        IncrementalFilteringMode="StartsWith">
                    </dx:ASPxComboBox>
                </div>
                <div style="display: table-cell; vertical-align: middle; padding-left: 4px;">
                    <dx:ASPxTextBox ID="txt_finance" runat="server" Width="60px" Text="0.00" AutoPostBack="True" Enabled="False">
                    </dx:ASPxTextBox>
                    <asp:TextBox ID="txt_setup_tax" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="txt_del_tax" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="txt_total_finance" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="txt_tax_cd" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="txt_zip_cd" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                </div>
            </div>
        </div>
        <br />
        <dx:ASPxLabel ID="lbl_error" ForeColor="Red" Width="100%" runat="server">
        </dx:ASPxLabel>
        <dx:ASPxLabel ID="lblErrorMessage" ForeColor="Red" Visible="false" Width="100%" runat="server"></dx:ASPxLabel>
    </div>
    <asp:Panel ID="pnlExchange" runat="server">
        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="99%" ClientIDMode="Static"
            AutoPostBack="True" ClientInstanceName="ASPxPageControl1">
            <TabPages>
                <dx:TabPage Text="<%$ Resources:SalesExchange, Label24 %>">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <asp:Panel ID="pnlExchangeOrderHeader" runat="server" Width="100%">
                                <div>
                                    <div style="padding-bottom: 4px;">
                                        <dx:ASPxMenu ID="ASPxMenu1" runat="server" HorizontalAlign="Center" Width="100%">
                                            <Items>
                                                <dx:MenuItem Text="<%$ Resources:SalesExchange, Label66 %>">
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:ASPxMenu>
                                    </div>
                                </div>
                                <div>
                                    <div style="padding-bottom: 4px;">
                                      <%--  2018 JDA update -- change datagrid to gridview--%>
                                      <%--  <asp:DataGrid ID="grd_lines" Style="position: relative; top: 1px; left: 1px;" runat="server"
                                            AutoGenerateColumns="False" CellPadding="2" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" Height="50px" Width="100%" PageSize="1">
                                            <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                                                Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                                                Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" VerticalAlign="Top" />
                                            <Columns>
                                                <asp:BoundColumn DataField="DEL_DOC_LN#" HeaderText="Ln" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="QTY" HeaderText="Qty" ReadOnly="True" DataFormatString="{0:0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%" />
                                                <asp:BoundColumn DataField="STORE_CD" HeaderText="St" ReadOnly="True" ItemStyle-Width="12%" />
                                                <asp:BoundColumn DataField="LOC_CD" HeaderText="Loc" ReadOnly="True" ItemStyle-Width="12%" />
                                                <asp:BoundColumn DataField="ITM_CD" HeaderText="SKU" ReadOnly="True" ItemStyle-Width="25%" />
                                                <asp:BoundColumn DataField="UNIT_PRC" HeaderText="Retail" ReadOnly="True" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundColumn DataField="WARRANTABLE" HeaderText="WARR" ReadOnly="True" Visible="false" />
                                                <asp:TemplateColumn HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Image1" CommandName="move_inventory_to_new" runat="server" ImageUrl="images/icons/arrow_right.png"
                                                            Height="20" Width="20" />
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    <EditItemTemplate>
                                                        <asp:ImageButton ID="LinkButton2" CommandName="move_inventory_to_new" runat="server" />
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-Width="5%" ItemStyle-Width="5%" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Image2" runat="server" CommandName="delete_line" ImageUrl="images/icons/symbol-delete.png"
                                                            Height="20" Width="20" />
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    <EditItemTemplate>
                                                        <asp:ImageButton ID="LinkButton3" CommandName="delete_line" runat="server" />
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="ITM_TP_CD" HeaderText="Item Type" ReadOnly="True" Visible="false" />
                                                <asp:BoundColumn DataField="WARR_BY_LN" HeaderText="WarByLn" ReadOnly="True" Visible="false" />
                                                <asp:BoundColumn DataField="WARR_BY_SKU" HeaderText="WarBySku" ReadOnly="True" Visible="false" />
                                                <asp:BoundColumn DataField="WARR_BY_DOC_NUM" HeaderText="WarByDocNo" ReadOnly="True" Visible="false" />
                                            </Columns>
                                        </asp:DataGrid>--%>

                                         <dx:ASPxGridView ID="orig_Sale" runat="server" AutoGenerateColumns="false" Width="100%" SettingsText-EmptyDataRow="<%$ Resources:SalesExchange, Label67 %>"
                                             OnCustomButtonCallback="orig_Sale_CustomButtonCallback" EnableCallBacks="false" OnHtmlRowCreated="orig_Sale_HtmlRowCreated"
                                             OnCustomButtonInitialize="orig_Sale_CustomButtonInitialize" OnDataBound="orig_Sale_DataBound">
                                             
                                            <Columns>                                                
                                            <dx:GridViewDataTextColumn FieldName="DEL_DOC_LN#" VisibleIndex="0" Caption="<%$ Resources:SalesExchange, Label44 %>" Width="8%" CellStyle-HorizontalAlign="Center">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True"  Font-Bold="True"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="QTY" VisibleIndex="1" Caption="<%$ Resources:SalesExchange, Label45 %>" Width="8%" PropertiesTextEdit-DisplayFormatString="{0:0}" CellStyle-HorizontalAlign="Right">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True"  Font-Bold="True"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="STORE_CD" VisibleIndex="2" Caption="<%$ Resources:SalesExchange, Label46 %>" Width="12%">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="LOC_CD" VisibleIndex="3" Caption="<%$ Resources:SalesExchange, Label47 %>" Width="12%">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True"  Font-Bold="True"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="ITM_CD" VisibleIndex="4" Caption="<%$ Resources:SalesExchange, Label48 %>" Width="20%">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="UNIT_PRC" VisibleIndex="5" Caption="<%$ Resources:SalesExchange, Label49 %>" Width="20%" CellStyle-HorizontalAlign="Right" PropertiesTextEdit-DisplayFormatString="{0:c}"> 
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="WARRANTABLE" Visible="false" Caption="<%$ Resources:SalesExchange, Label50 %>"> </dx:GridViewDataTextColumn>                                               
                                                
                                            <dx:GridViewDataTextColumn Caption="<%$ Resources:SalesExchange, Label51 %>" VisibleIndex="6" Width="12%">
                                                <DataItemTemplate>
                                                   <asp:TextBox ID="txtQty" DataField="QTY" Text="1" runat="server" Width="60" Height="15" BorderColor="YellowGreen" style="text-align:right" ></asp:TextBox>
                                                    <asp:TextBox ID="LblAllowFraction" DataField="AllowFraction" Text='<%# DataBinder.Eval(Container, "DataItem.AllowFraction") %>' runat="server" Visible="false"></asp:TextBox>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Green"  />
                                                
                                            </dx:GridViewDataTextColumn>

                                            <dx:GridViewCommandColumn VisibleIndex="7"  ButtonType ="Image"  Caption=" "  Width="5%">
                                                
                                               <CustomButtons>
                                                   
                                                <dx:GridViewCommandColumnCustomButton ID="img_Move" Image-Url="images/icons/Arrow_Right.png" Image-Height="20" Image-Width="20">
                                                </dx:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                               
                                            </dx:GridViewCommandColumn>
                                                <dx:GridViewCommandColumn VisibleIndex="8" Visible="false" >
                                               <CustomButtons>
                                                <dx:GridViewCommandColumnCustomButton ID="img_Delete">
                                                    <Image Url="images/icons/symbol-delete.png" />
                                                </dx:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                            </dx:GridViewCommandColumn>

                                            <dx:GridViewDataTextColumn FieldName="ITM_TP_CD" Visible="false" Caption="Item Type"> </dx:GridViewDataTextColumn>                                               
                                            <dx:GridViewDataTextColumn FieldName="WARR_BY_LN" Visible="false" Caption="WarByLn"> </dx:GridViewDataTextColumn>                                               
                                            <dx:GridViewDataTextColumn FieldName="WARR_BY_SKU" Visible="false" Caption="WarBySku"> </dx:GridViewDataTextColumn>                                               
                                            <dx:GridViewDataTextColumn FieldName="WARR_BY_DOC_NUM" Visible="false" Caption="WarByDocNo"> </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <Styles Row-CssClass="gridRow-static-height">
                                                <AlternatingRow Enabled="true" BackColor="Beige" />                                                
                                            </Styles>
                                            <SettingsPager Visible="False" Mode="ShowAllRecords">
                                            </SettingsPager>
                                            <ClientSideEvents 
                                            CustomButtonClick="function(s, e) {
                                            e.processOnServer = true; 
                                            }"/>

                                        </dx:ASPxGridView>
                                    </div>
                                </div>
                                <br />
                                <div>
                                    <div style="padding-bottom: 4px;">
                                        <dx:ASPxMenu ID="ASPxMenu2" runat="server" HorizontalAlign="Center" Width="100%">
                                            <Items>
                                                <dx:MenuItem Text="<%$ Resources:SalesExchange, Label01 %>">
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:ASPxMenu>
                                    </div>
                                </div>
                                <div>
                                    <div style="padding-bottom: 4px;">
                                       <%-- <asp:DataGrid ID="grd_lines_new" runat="server" Width="100%" AutoGenerateColumns="False"
                                            Height="50px" PageSize="1">
                                            <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                                                Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                                            <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                                                Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="DEL_DOC_LN#" HeaderText="Ln" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="QTY" HeaderText="Qty" ReadOnly="True" DataFormatString="{0:0}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%" />
                                                <asp:BoundColumn DataField="STORE_CD" HeaderText="St" ReadOnly="True" ItemStyle-Width="12%" />
                                                <asp:BoundColumn DataField="LOC_CD" HeaderText="Loc" ReadOnly="True" ItemStyle-Width="12%" />
                                                <asp:BoundColumn DataField="ITM_CD" HeaderText="SKU" ReadOnly="True" ItemStyle-Width="25%" />
                                                <asp:BoundColumn DataField="UNIT_PRC" HeaderText="Retail" ReadOnly="True" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundColumn DataField="WARRANTABLE" HeaderText="Warr" ReadOnly="True" Visible="false" />
                                                <asp:BoundColumn DataField="WARR_BY_LN" HeaderText="WarByLn" ReadOnly="True" Visible="false" />
                                                <asp:BoundColumn DataField="WARR_BY_SKU" HeaderText="WarBySku" ReadOnly="True" Visible="false" />
                                                <asp:BoundColumn DataField="WARR_BY_DOC_NUM" HeaderText="WarByDocNo" ReadOnly="True" Visible="false" />
                                                <asp:TemplateColumn ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Image2" runat="server" CommandName="move_inventory_to_old" ImageUrl="images/icons/symbol-delete.png"
                                                            Height="20" Width="20" Style="padding-right: 6px;" />
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Right" VerticalAlign="Top" />
                                                    <EditItemTemplate>
                                                        <asp:ImageButton ID="LinkButton3" CommandName="move_inventory_to_old" runat="server" />
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="ITM_TP_CD" HeaderText="Item Type" ReadOnly="True" Visible="false" />
                                            </Columns>
                                        </asp:DataGrid>--%>
                                          <dx:ASPxGridView ID="grd_lines_new" runat="server" AutoGenerateColumns="false" Width="100%" SettingsText-EmptyDataRow="<%$ Resources:SalesExchange, Label67 %>"
                                             OnCustomButtonCallback="grd_lines_new_CustomButtonCallback" EnableCallBacks="false" OnHtmlRowCreated="grd_lines_new_HtmlRowCreated"
                                             OnCustomButtonInitialize="grd_lines_new_CustomButtonInitialize" OnDataBound="grd_lines_new_DataBound">                                             
                                            <Columns>                                                
                                            <dx:GridViewDataTextColumn FieldName="DEL_DOC_LN#" VisibleIndex="0" Caption="<%$ Resources:SalesExchange, Label44 %>" Width="8%" CellStyle-HorizontalAlign="Center">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True"  Font-Bold="True"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="QTY" VisibleIndex="1" Caption="<%$ Resources:SalesExchange, Label45 %>" Width="8%" PropertiesTextEdit-DisplayFormatString="{0:0}" CellStyle-HorizontalAlign="Right">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True"  Font-Bold="True"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="STORE_CD" VisibleIndex="2"  Caption="<%$ Resources:SalesExchange, Label46 %>" Width="12%">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True"  />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="LOC_CD" VisibleIndex="3" Caption="<%$ Resources:SalesExchange, Label47 %>" Width="12%">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True"  Font-Bold="True"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="ITM_CD" VisibleIndex="4" Caption="<%$ Resources:SalesExchange, Label48 %>" Width="25%">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="UNIT_PRC" VisibleIndex="5" Caption="<%$ Resources:SalesExchange, Label49 %>" Width="25%" CellStyle-HorizontalAlign="Right" PropertiesTextEdit-DisplayFormatString="{0:c}"> 
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="WARRANTABLE" Visible="false" Caption="<%$ Resources:SalesExchange, Label50 %>"> </dx:GridViewDataTextColumn>                                               
                                             <dx:GridViewDataTextColumn FieldName="WARR_BY_LN" Visible="false" Caption="WarByLn"> </dx:GridViewDataTextColumn>                                               
                                            <dx:GridViewDataTextColumn FieldName="WARR_BY_SKU" Visible="false" Caption="WarBySku"> </dx:GridViewDataTextColumn>                                               
                                            <dx:GridViewDataTextColumn FieldName="WARR_BY_DOC_NUM" Visible="false" Caption="WarByDocNo"> </dx:GridViewDataTextColumn>
                                            <dx:GridViewCommandColumn VisibleIndex="6" ButtonType ="Image"  Caption=" " Width="5%" >
                                            <CustomButtons>
                                            <dx:GridViewCommandColumnCustomButton ID="img_Remove" Image-Url ="images/icons/symbol-delete.png" Image-Height ="20" Image-Width ="20">
                                            </dx:GridViewCommandColumnCustomButton>
                                            </CustomButtons>
                                            </dx:GridViewCommandColumn>

                                            <dx:GridViewDataTextColumn FieldName="ITM_TP_CD" Visible="false" Caption="Item Type"> </dx:GridViewDataTextColumn>                                               
                                            <dx:GridViewDataTextColumn FieldName="WARR_BY_LN" Visible="false" Caption="WarByLn"> </dx:GridViewDataTextColumn>                                               
                                            <dx:GridViewDataTextColumn FieldName="WARR_BY_SKU" Visible="false" Caption="WarBySku"> </dx:GridViewDataTextColumn>                                               
                                            <dx:GridViewDataTextColumn FieldName="WARR_BY_DOC_NUM" Visible="false" Caption="WarByDocNo"> </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <Styles Row-CssClass="gridRow-static-height">
                                                <AlternatingRow Enabled="true" BackColor="Beige" />                                                
                                            </Styles>
                                            <SettingsPager Visible="False" Mode="ShowAllRecords">
                                            </SettingsPager>
                                            <ClientSideEvents 
                                            CustomButtonClick="function(s, e) {
                                            e.processOnServer = true; 
                                            }"/>

                                        </dx:ASPxGridView>
                                    </div>
                                </div>
                                <div style="display: table-row;">&nbsp</div>

                                <div style="display: table; width: 100%;">
                                    <div style="display: table-row;">
                                        <div style="display: table-cell;">
                                            <fieldset>
                                                <legend><b><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label52 %>" /></b></legend>
                                                <div>
                                                    <div>
                                                        <div style="display: table; margin: auto; margin-right: 0px;">
                                                            <div style="display: table-row;">
                                                                <div style="display: table-cell; vertical-align: middle; padding-left: 5px; padding-right: 5px;">
                                                                    <asp:Label ID="lblZone" runat="server" Text="Zone"></asp:Label>
                                                                </div>
                                                                <div style="display: table-cell; vertical-align: middle; padding-left: 5px; padding-right: 5px;">
                                                                    <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label35 %>" /></label>
                                                                </div>
                                                                <div style="display: table-cell; vertical-align: middle; padding-left: 5px; padding-right: 5px;">
                                                                    <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label36 %>" /></label>
                                                                </div>

                                                            </div>
                                                            <div style="display: table-row;">

                                                                <div style="display: table-cell; vertical-align: middle; text-align: right; padding-left: 5px; padding-right: 5px;">
                                                                    <dx:ASPxTextBox ID="txt_zone_cd" runat="server" Width="60px" Enabled="False"></dx:ASPxTextBox>
                                                                </div>
                                                                <div style="display: table-cell; vertical-align: middle; text-align: right; padding-left: 5px; padding-right: 5px;">
                                                                    <dx:ASPxTextBox ID="txt_del_chg" runat="server" Width="60px" AutoPostBack="True" Text="0.00"></dx:ASPxTextBox>
                                                                </div>
                                                                <div style="display: table-cell; vertical-align: middle; text-align: right; padding-left: 5px; padding-right: 5px;">
                                                                    <dx:ASPxTextBox ID="txt_setup" runat="server" Width="60px" AutoPostBack="True" Text="0.00"></dx:ASPxTextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="display: table; margin: auto; margin-right: 0px;">
                                                            <div style="display: table-row;">
                                                                <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                                                    <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label38 %>" /></label>
                                                                </div>
                                                                <div style="display: table-cell; padding: 2px;">
                                                                    <dx:ASPxTextBox ID="txt_crm_subtotal" runat="server" Width="100px" ReadOnly="true"  HorizontalAlign="Right">
                                                                    </dx:ASPxTextBox>
                                                                </div>
                                                            </div>
                                                            <div style="display: table-row;">
                                                                <div style="display: table-cell; vertical-align: middle; text-align: right; padding-left: 5px; padding-right: 5px;">
                                                                    <label>Tax(</label>
                                                                    <asp:LinkButton ID="lnkTaxCode" runat="server" Enabled="false" CssClass="dxeBase_Office2010Blue">
                                                                    </asp:LinkButton>
                                                                    <label>):</label>
                                                                </div>
                                                                <div style="display: table-cell; padding: 2px;">
                                                                    <dx:ASPxTextBox ID="txt_crm_taxes" runat="server" Width="100px" ReadOnly="true"  HorizontalAlign="Right">
                                                                    </dx:ASPxTextBox>
                                                                </div>
                                                            </div>
                                                            <div style="display: table-row;">                                                               
                                                                <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                                                    <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label40 %>" /></label>
                                                                </div>
                                                                <div style="display: table-cell; padding: 2px;">
                                                                    <dx:ASPxTextBox ID="txt_crm_grand" runat="server" Width="100px" ReadOnly="true"  HorizontalAlign="Right">
                                                                    </dx:ASPxTextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                         <div style="display: table-cell;">
                                            <fieldset>
                                                <legend><b><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label53 %>" /></b></legend>
                                                <div>
                                                    <div>
                                                        <div>
                                                            <div style="display: table; margin: auto; margin-right: 0px;">
                                                                <div style="display: table-row;">
                                                                    <div style="display: table-cell; vertical-align: middle; padding-left: 5px; padding-right: 5px;">
                                                                        <asp:Label ID="lblNewZone" runat="server" Text="<%$ Resources:SalesExchange, Label37 %>"></asp:Label>
                                                                    </div>
                                                                    <div style="display: table-cell; vertical-align: middle; padding-left: 5px; padding-right: 5px;">
                                                                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label35 %>" /></label>
                                                                    </div>
                                                                    <div style="display: table-cell; vertical-align: middle; padding-left: 5px; padding-right: 5px;">
                                                                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label36 %>" /></label>
                                                                    </div>

                                                                </div>
                                                                <dx:ASPxTextBox ID="txt_new_pd_store_cd" runat="server" Enabled="false" Visible="false">
                                                                </dx:ASPxTextBox>
                                                                <div style="display: table-row;">
                                                                    <div style="display: table-cell; vertical-align: middle; text-align: right; padding-left: 5px; padding-right: 5px;">
                                                                        <dx:ASPxTextBox ID="txtNewZone" runat="server" Width="60px" Enabled="False"></dx:ASPxTextBox>

                                                                    </div>
                                                                    <div style="display: table-cell; vertical-align: middle; text-align: right; padding-left: 5px; padding-right: 5px;">
                                                                        <dx:ASPxTextBox ID="txtNewDeliveryChgs" runat="server" Width="60px" AutoPostBack="true" Text="0.00"></dx:ASPxTextBox>
                                                                    </div>
                                                                    <div style="display: table-cell; vertical-align: middle; text-align: right; padding-left: 5px; padding-right: 5px;">
                                                                        <dx:ASPxTextBox ID="txtNewSetupChg" runat="server" Width="60px" AutoPostBack="true" Text="0.00"></dx:ASPxTextBox>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div style="display: table; margin: auto; margin-right: 0px;">
                                                                <div style="display: table-row;">
                                                                    <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                                                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label38 %>" /></label>
                                                                    </div>
                                                                    <div style="display: table-cell; padding: 2px;">
                                                                        <dx:ASPxTextBox ID="txt_sal_subtotal" runat="server" Width="100px" ReadOnly="true"  HorizontalAlign="Right">
                                                                        </dx:ASPxTextBox>
                                                                    </div>
                                                                </div>
                                                                <div style="display: table-row;">
                                                                    <div style="display: table-cell; vertical-align: middle; text-align: right; padding-left: 5px; padding-right: 5px;">
                                                                        <label>Tax(</label>
                                                                        <asp:LinkButton ID="lnkUpdatedTax" runat="server" Enabled="false" CssClass="dxeBase_Office2010Blue">
                                                                        </asp:LinkButton>
                                                                        <label>):</label>
                                                                    </div>
                                                                    <div style="display: table-cell; padding: 2px;">
                                                                        <dx:ASPxTextBox ID="txt_sal_taxes" runat="server" Width="100px" ReadOnly="true"  HorizontalAlign="Right">
                                                                        </dx:ASPxTextBox>
                                                                    </div>
                                                                </div>
                                                                <div style="display: table-row;">                                                                   
                                                                    <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                                                        <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label40 %>" /></label>
                                                                    </div>
                                                                    <div style="display: table-cell; padding: 2px;">
                                                                        <dx:ASPxTextBox ID="txt_sal_grand" runat="server" Width="100px" ReadOnly="true"  HorizontalAlign="Right">
                                                                        </dx:ASPxTextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div>
                                        <div style="display: table; margin: auto; position: center;">
                                            <div style="vertical-align: middle; display: table-cell; white-space: nowrap;">
                                                <label><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label21 %>" /></label>
                                                &nbsp;
                                            </div>
                                            <div>
                                                <dx:ASPxTextBox ID="txt_difference" runat="server" Width="84px" ReadOnly="true" HorizontalAlign="center" >
                                                </dx:ASPxTextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div>
                                        <div style="vertical-align: middle; display: table-cell; white-space: nowrap;">
                                            <dx:ASPxLabel ID="lblNewSOComment" runat="server" Text="<%$ Resources:SalesExchange, Label22 %>">
                                            </dx:ASPxLabel>
                                            &nbsp;
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtComments" AutoPostBack="True" runat="server" Rows="2" TextMode="MultiLine"
                                                Height="70px" Width="99%"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="<%$ Resources:SalesExchange, Label23 %>">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <asp:Panel ID="pnlCustomerDetails" runat="server" Enabled="false">
                                <asp:Label ID="lbl_tax_cd" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="lbl_tax_rate" runat="server" Visible="False"></asp:Label>


                                <div style="display: table; margin: 10px; width: 100%;">
                                    <div style="display: table-row;">
                                        <div style="display: table-cell; padding: 2px;">
                                            <label><b><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label25 %>" /></b></label>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                    </div>

                                    <div style="display: table-row;">
                                        <div style="display: table-cell; vertical-align: middle; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="lblShipToCorp" runat="server" Text="<%$ Resources:SalesExchange, Label55 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 6px;">
                                            <dx:ASPxTextBox ID="txtShippingCorpName" runat="server" Width="330px" CssClass="style5" MaxLength="30"></dx:ASPxTextBox>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                            <dx:ASPxLabel ID="lblShipToPO" runat="server" Text="<%$ Resources:SalesExchange, Label61 %>"></dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                            <dx:ASPxTextBox ID="txtShipToPO" runat="server" Width="99px" MaxLength="15"></dx:ASPxTextBox>
                                        </div>
                                    </div>

                                    <div style="display: table-row; vertical-align: middle;">
                                        <div style="display: table-cell; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:SalesExchange, Label56 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 2px;">
                                            <div style="display: table;">
                                                <div style="display: table-row; vertical-align: middle;">
                                                    <div style="display: table-cell; padding: 2px;">
                                                        <dx:ASPxTextBox ID="txtShipToFName" runat="server" Width="160px" CssClass="style5"></dx:ASPxTextBox>
                                                    </div>
                                                    <div style="display: table-cell; padding: 2px;">
                                                        <dx:ASPxTextBox ID="txtShipToLname" runat="server" Width="163px" CssClass="style5"></dx:ASPxTextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;"></div>
                                        <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:SalesExchange, Label62 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                            <dx:ASPxTextBox ID="txtShipToHomePhone" MaxLength="12" runat="server" Width="99px" NullText="XXX-XXX-XXXX" CssClass="style5">
                                                <ClientSideEvents KeyPress="function(s, e){ var keycode =_getKeyCode(e.htmlEvent);  if (!(keycode == 37 || keycode == 45 || keycode == 46 || keycode == 8 || keycode == 13) && (keycode < 48 || keycode > 57)) {return _aspxPreventEvent(e.htmlEvent);}}" />
                                                <ClientSideEvents LostFocus="function(s, e){ var searchPhoneNum = s.GetText();searchPhoneNum = searchPhoneNum.replace(/[^0-9]/g, '');if (searchPhoneNum.length == 10) {searchPhoneNum = searchPhoneNum.replace(/(\d{3})(\d{3})(\d{4})/,'$1-$2-$3');s.SetText(searchPhoneNum);}}" />
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                    </div>


                                    <div style="display: table-row; vertical-align: middle;">
                                        <div style="display: table-cell; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:SalesExchange, Label57 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 6px;">
                                            <dx:ASPxTextBox ID="txtShipToAdd1" runat="server" Width="330px" CssClass="style5"></dx:ASPxTextBox>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;"></div>
                                        <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                            <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="A:">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                            <dx:ASPxTextBox ID="txtShipToAlternatePhone" MaxLength="12" runat="server" Width="99px" NullText="XXX-XXX-XXXX">
                                                <ClientSideEvents KeyPress="function(s, e){ var keycode =_getKeyCode(e.htmlEvent);  if (!(keycode == 37 || keycode == 45 || keycode == 46 || keycode == 8 || keycode == 13) && (keycode < 48 || keycode > 57)) {return _aspxPreventEvent(e.htmlEvent);}}" />
                                                <ClientSideEvents LostFocus="function(s, e){ var searchPhoneNum = s.GetText();searchPhoneNum = searchPhoneNum.replace(/[^0-9]/g, '');if (searchPhoneNum.length == 10) {searchPhoneNum = searchPhoneNum.replace(/(\d{3})(\d{3})(\d{4})/,'$1-$2-$3');s.SetText(searchPhoneNum);}}" />
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>

                                    <div style="display: table-row;">
                                        <div style="display: table-cell; vertical-align: middle; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:SalesExchange, Label58 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 6px;">
                                            <dx:ASPxTextBox ID="txtShipToAdd2" runat="server" Width="330px" CssClass="style5"></dx:ASPxTextBox>
                                        </div>
                                    </div>

                                    <div style="display: table-row;">
                                        <div style="display: table-cell; padding: 2px;">
                                            <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:SalesExchange, Label59 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                            <div style="display: table;">
                                                <div style="display: table-row; vertical-align: middle;">
                                                    <div style="display: table-cell; padding: 2px;">
                                                        <dx:ASPxTextBox ID="txtShipToCity" runat="server" Width="130px" CssClass="style5"></dx:ASPxTextBox>
                                                    </div>
                                                    <div style="display: table-cell; padding: 2px; vertical-align: middle;">
                                                        <asp:DropDownList ID="cboShipToState" Width="100px" runat="server" CssClass="style5">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div style="display: table-cell; padding: 2px;">
                                                        <dx:ASPxTextBox ID="txtShipToZip" runat="server" Width="88px" CssClass="style5" AutoPostBack="true" OnTextChanged="txt_zip_TextChanged"></dx:ASPxTextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: table-row;">
                                        <div style="display: table-cell; vertical-align: middle; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="<%$ Resources:SalesExchange, Label60 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 6px;">
                                            <dx:ASPxTextBox ID="txtShipToEmail" runat="server" Width="330px" CssClass="style5"></dx:ASPxTextBox>
                                            <dx:ASPxTextBox ID="ASPxTextBox12" runat="server" Visible="False"></dx:ASPxTextBox>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                    </div>
                                </div>

                                <hr />

                                <div style="display: table; margin: 10px; width: 100%;">
                                    <div style="display: table-row;">
                                        <div style="display: table-cell; padding: 2px;">
                                            <label><b><asp:Literal runat="server" Text="<%$ Resources:SalesExchange, Label54 %>" /></b></label>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                    </div>

                                    <div style="display: table-row;">
                                        <div style="display: table-cell; vertical-align: middle; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="<%$ Resources:SalesExchange, Label55 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 6px;">
                                            <dx:ASPxTextBox ID="txt_corp_name" runat="server" Width="330px" CssClass="style5" MaxLength="30"></dx:ASPxTextBox>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                            <dx:ASPxLabel ID="lbl_po" runat="server" Text="<%$ Resources:SalesExchange, Label61 %>"></dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                            <dx:ASPxTextBox ID="txt_po" runat="server" Width="99px" MaxLength="15"></dx:ASPxTextBox>
                                        </div>
                                    </div>

                                    <div style="display: table-row; vertical-align: middle;">
                                        <div style="display: table-cell; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="<%$ Resources:SalesExchange, Label56 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 2px;">
                                            <div style="display: table;">
                                                <div style="display: table-row; vertical-align: middle;">
                                                    <div style="display: table-cell; padding: 2px;">
                                                        <dx:ASPxTextBox ID="txt_f_name" runat="server" Width="160px" CssClass="style5"></dx:ASPxTextBox>
                                                    </div>
                                                    <div style="display: table-cell; padding: 2px;">
                                                        <dx:ASPxTextBox ID="txt_l_name" runat="server" Width="163px" CssClass="style5"></dx:ASPxTextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;"></div>
                                        <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                            <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="<%$ Resources:SalesExchange, Label62 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                            <dx:ASPxTextBox ID="txt_h_phone" MaxLength="12" runat="server" Width="99px" NullText="XXX-XXX-XXXX" CssClass="style5">
                                                <ClientSideEvents KeyPress="function(s, e){ var keycode =_getKeyCode(e.htmlEvent);  if (!(keycode == 37 || keycode == 45 || keycode == 46 || keycode == 8 || keycode == 13) && (keycode < 48 || keycode > 57)) {return _aspxPreventEvent(e.htmlEvent);}}" />
                                                <ClientSideEvents LostFocus="function(s, e){ var searchPhoneNum = s.GetText();searchPhoneNum = searchPhoneNum.replace(/[^0-9]/g, '');if (searchPhoneNum.length == 10) {searchPhoneNum = searchPhoneNum.replace(/(\d{3})(\d{3})(\d{4})/,'$1-$2-$3');s.SetText(searchPhoneNum);}}" />
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                    </div>


                                    <div style="display: table-row; vertical-align: middle;">
                                        <div style="display: table-cell; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="<%$ Resources:SalesExchange, Label57 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 6px;">
                                            <dx:ASPxTextBox ID="txt_addr1" runat="server" Width="330px" CssClass="style5"></dx:ASPxTextBox>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;"></div>
                                        <div style="display: table-cell; vertical-align: middle; text-align: right;">
                                            <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="A:">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                            <dx:ASPxTextBox ID="txt_b_phone" MaxLength="12" runat="server" Width="99px" NullText="XXX-XXX-XXXX">
                                                <ClientSideEvents KeyPress="function(s, e){ var keycode =_getKeyCode(e.htmlEvent);  if (!(keycode == 37 || keycode == 45 || keycode == 46 || keycode == 8 || keycode == 13) && (keycode < 48 || keycode > 57)) {return _aspxPreventEvent(e.htmlEvent);}}" />
                                                <ClientSideEvents LostFocus="function(s, e){ var searchPhoneNum = s.GetText();searchPhoneNum = searchPhoneNum.replace(/[^0-9]/g, '');if (searchPhoneNum.length == 10) {searchPhoneNum = searchPhoneNum.replace(/(\d{3})(\d{3})(\d{4})/,'$1-$2-$3');s.SetText(searchPhoneNum);}}" />
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>

                                    <div style="display: table-row;">
                                        <div style="display: table-cell; vertical-align: middle; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="<%$ Resources:SalesExchange, Label58 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 6px;">
                                            <dx:ASPxTextBox ID="txt_addr2" runat="server" Width="330px" CssClass="style5"></dx:ASPxTextBox>
                                        </div>
                                    </div>

                                    <div style="display: table-row;">
                                        <div style="display: table-cell; padding: 2px;">
                                            <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="<%$ Resources:SalesExchange, Label59 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                            <div style="display: table;">
                                                <div style="display: table-row; vertical-align: middle;">
                                                    <div style="display: table-cell; padding: 2px;">
                                                        <dx:ASPxTextBox ID="txt_city" runat="server" Width="130px" CssClass="style5"></dx:ASPxTextBox>
                                                    </div>
                                                    <div style="display: table-cell; padding: 2px; vertical-align: middle;">
                                                        <asp:DropDownList ID="cbo_State" Width="100px" runat="server" CssClass="style5">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div style="display: table-cell; padding: 2px;">
                                                        <dx:ASPxTextBox ID="txt_zip" runat="server" Width="88px" CssClass="style5" AutoPostBack="true" OnTextChanged="txt_zip_TextChanged"></dx:ASPxTextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: table-row;">
                                        <div style="display: table-cell; vertical-align: middle; text-align: left; width: 76px;">
                                            <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text="<%$ Resources:SalesExchange, Label60 %>">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div style="display: table-cell; padding: 2px; padding-left: 6px;">
                                            <dx:ASPxTextBox ID="txt_email" runat="server" Width="330px" CssClass="style5"></dx:ASPxTextBox>
                                            <dx:ASPxTextBox ID="txt_SHU" runat="server" Visible="False"></dx:ASPxTextBox>
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                        <div style="display: table-cell; padding: 2px;">
                                        </div>
                                    </div>

                                </div>

                                 <asp:Panel ID="taxExcepmtion" Visible="false" runat="server" Width="100%">
                                    <div style="float: none;">
                                        <table width="100%" class="style5">
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br />
                                                    <asp:Image ID="img_warn" runat="server" Height="20px" ImageUrl="images/warning.jpg"
                                                        Width="26px" Visible="False" />&nbsp;
                                    <dx:ASPxLabel ID="lbl_cust_warn" runat="server" Text="" Visible="false">
                                    </dx:ASPxLabel>
                                                    <br />
                                                    <br />
                                                </td>
                                                <td align="right" valign="top">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="lblTaxCd" runat="server" Text="<%$ Resources:SalesExchange, Label64 %>">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtTaxCd" runat="server" Enabled="false" Width="160px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <dx:ASPxLabel ID="AspxLabelExempt" runat="server" Text="<%$ Resources:SalesExchange, Label65 %>">
                                                    </dx:ASPxLabel>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox ID="cbo_tax_exempt" runat="server" ClientInstanceName="cbo_tax_exempt" ClientIDMode="Predictable" Enabled="false"
                                                                    IncrementalFilteringMode="StartsWith" ValueType="System.String" Width="305px" AutoPostBack="True">
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txt_exempt_id" runat="server" Enabled="false" Width="103px" MaxLength="20">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>

                            </asp:Panel>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
            </TabPages>
        </dx:ASPxPageControl>
    </asp:Panel>
    <asp:HiddenField ID="hidItemCd" runat="server" />
    <asp:HiddenField ID="hidIsWarrChecked" runat="server" />
    <ucWar:MultiWarranties runat="server" ID="ucMultiWarranties" ClientIDMode="Static" />
    <ucRelSku:RelatedSKU runat="server" ID="ucRelatedSKU" ClientIDMode="Static" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <dxpc:ASPxPopupControl ID="PopupDeliveryDate" runat="server" CloseAction="CloseButton" ShowOnPageLoad="false" ClientInstanceName="PopupDeliveryDate"
        HeaderText="<%$ Resources:SalesExchange, Label70 %>" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="400px" Width="500px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="DeliveryDatePopupContentControl" runat="server">
                <ucDeliveryDate:DeliveryDatePopup runat="server" ID="UcDeliveryDatePopup" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="popupMgrOverride" runat="server" ClientInstanceName="popupMgrOverride"
        HeaderText="Manager Approval" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Modal="true" CloseAction="None"
        AllowDragging="true" ShowCloseButton="false" ShowPageScrollbarWhenModal="true" Width="400px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <ucMgr:ManagerOverride runat="server" ID="ucMgrOverride" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <ucPC:SummaryPriceChangeApproval runat="server" ID="ucSummaryPrcChange" ClientIDMode="Static" />
    <ucMsg:MsgPopup runat="server" ID="ucMsgPopup" />
    <dxpc:ASPxPopupControl ID="PopupControlTax" runat="server" ClientInstanceName="PopupTax" CloseAction="CloseButton"
        HeaderText="Tax Update" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="420px" Width="670px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <div style="width: 100%; height: 420px; overflow-y: scroll;">
                    <ucTax:TaxUpdate runat="server" ID="ucTaxUpdate" />
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="PopupZone" ClientInstanceName="PopupZone" runat="server" ShowOnPageLoad="false" CloseAction="None" ShowCloseButton="false"
        HeaderText="Zone Update" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="350px" Width="500px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                <ucZone:ZonePopup runat="server" ID="UcZonePopup" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <asp:HiddenField ID="hidTaxChange" runat="server" Value="false" />
    <asp:HiddenField ID="hideZipCode" runat="server" />
    <asp:HiddenField ID="hidWrittenStoreCode" runat="server" />
    <asp:HiddenField ID="IsZoneSelected" runat="server" Value="False" />
    <asp:HiddenField ID="hidDTDAmount" runat="server" Value="False" />
    <asp:Label ID="lbl_old_or_new" runat="server" Visible="False"></asp:Label>
</asp:Content>
