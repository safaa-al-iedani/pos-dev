<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="LoadItemsBrick.aspx.vb" Inherits="LoadItemsBrick" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:Label ID="txt_title" runat="server" Style="position: relative" CssClass="style5">Load Item File</asp:Label>
    <br />
    <div>
        <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
        <asp:FileUpload ID="FileUpload1" runat="server" ClientIDMode="Static"/><br />
            </ContentTemplate>
            <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" 
         Text="Upload Item File" />&nbsp;<br />
        <br />
        <asp:Label ID="Label1" runat="server" ForeColor="Blue"></asp:Label></div>

    <br />
     <asp:TextBox ID="lbl_msg1" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
     
    <br />

    <asp:Label ID="txt_price" runat="server" Style="position: relative" CssClass="style5">Load Price File</asp:Label>
    <br />
    <div>
        <asp:UpdatePanel ID="updatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
        <asp:FileUpload ID="FileUpload2" runat="server" ClientIDMode="Static"/><br />
            </ContentTemplate>
            <Triggers>
            <asp:PostBackTrigger ControlID="Button2" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" 
         Text="Upload Price File" />&nbsp;<br />
        <br />
        <asp:Label ID="Label2" runat="server"  ForeColor="Blue"></asp:Label></div>

    <br />
     <asp:TextBox ID="lbl_msg2" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
     
    <br />

    <asp:Label ID="txt_warranty" runat="server" Style="position: relative" CssClass="style5">Load Warranty File</asp:Label>
    <br />
    <div>
        <asp:UpdatePanel ID="updatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
        <asp:FileUpload ID="FileUpload3" runat="server" ClientIDMode="Static"/><br />
            </ContentTemplate>
            <Triggers>
            <asp:PostBackTrigger ControlID="Button3" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" 
         Text="Upload Warranty File" />&nbsp;<br />
        <br />
        <asp:Label ID="Label3" runat="server"  ForeColor="Blue"></asp:Label></div>

    <br />
     <asp:TextBox ID="lbl_msg3" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
     
    <br />

    <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server" 
         AutoGenerateColumns="false" CellPadding="2" DataKeyField="itm_cd"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="itm_cd" headertext="Item Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ve_cd" headertext="Vendor Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="mnr_cd" headertext="Minor Code" ReadOnly="True" Visible="True">                
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="des" headertext="Description" ReadOnly="True" Visible="True">
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="err_msg" HeaderText="<font color=red>Error Message</font>" ReadOnly="True" Visible="True" ItemStyle-Wrap="False">
                <ItemStyle ForeColor="Red"/>
            </asp:BoundColumn>
    </columns> 
    <itemstyle verticalalign="Top" />
    </asp:DataGrid>

       <asp:DataGrid ID="Gridview2" Style="position: relative; top: 9px;" runat="server" 
         AutoGenerateColumns="false" CellPadding="2" DataKeyField="itm_cd"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="store_grp_cd" headertext="Store Grp Cd" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="itm_cd" headertext="Item Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="eff_dt" headertext="Eff Dt" ReadOnly="True" Visible="True">                
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="end_dt" headertext="End Dt" ReadOnly="True" Visible="True">
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="prc" headertext="Prc" ReadOnly="True" Visible="True">
                    </asp:BoundColumn>                           
    </columns> 
    <itemstyle verticalalign="Top" />
    </asp:DataGrid>

       <asp:DataGrid ID="Gridview3" Style="position: relative; top: 9px;" runat="server" 
         AutoGenerateColumns="false" CellPadding="2" DataKeyField="itm_cd"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="itm_cd" headertext="Item Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="war_itm_cd" headertext="Warr Item Cd" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="war_ret_prc" headertext="Warr Ret Prc" ReadOnly="True" Visible="True">                
                </asp:BoundColumn>                
    </columns> 
    <itemstyle verticalalign="Top" />
    </asp:DataGrid>

    <br />

     <asp:Button ID="btn_process" runat="server" Text="Process Items" Width="200px" Visible="false">
     </asp:Button>
    <asp:Button ID="btn_process_price" runat="server" Text="Process Item Price" Width="200px" Visible="false">
     </asp:Button>
    <asp:Button ID="btn_process_warr" runat="server" Text="Process Warranty" Width="200px" Visible="false">
     </asp:Button>
    <br />
    <br />
    
    <asp:Button ID="btn_clear" runat="server" Text="Clear" Width="100px" Visible="true">
     </asp:Button>
</asp:Content>
