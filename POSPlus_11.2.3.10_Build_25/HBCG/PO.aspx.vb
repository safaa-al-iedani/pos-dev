Imports HBCG_Utils
Imports System.Data.OracleClient

Partial Class PO
    Inherits POSBasePage

    Private objPOBiz As POBiz = New POBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()

    Protected Sub Populate_Results()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        'MCCL get user type
        Dim userType As Double = -1

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        sql = "SELECT a.PO_CD, a.STAT_CD, a.EMP_CD_BUY, a.PO_SRT_CD, a.VE_CD, a.STORE_CD, a.TERMS_PCT, "
        sql = sql & "a.TERMS_DAYS, a.DAYS_NET, a.LST_CHNG_DT, a.WR_DT, a.FINAL_DT "
        sql = sql & "FROM PO a, PO_LN b, ITM c WHERE a.PO_CD = b.PO_CD and b.ITM_CD=c.ITM_CD AND a.PO_CD='" & Request("po") & "' "
        sql = sql & " and a.store_cd in (select s.store_cd from MCCL_STORE_GROUP m, store s, store_grp$store sg " +
                         "where m.user_id = '" & userType & "' And s.store_cd = sg.store_cd and " +
                         "m.store_grp_cd = sg.store_grp_cd and a.store_cd = s.store_cd "
            ' Leons Franchise exception
            If isNotEmpty(userType) And userType = 5 Then
                sql = sql & " and '" & coCd & "' = s.co_cd "

            End If
            sql = sql & ") "

        sql = sql & " and STD_MULTI_CO2.isValidStr6('" & Session("emp_init") & "', " +
                              " (select s.store_cd from emp e, store s " +
                              " where e.home_store_cd = s.store_cd " +
                              " and (e.emp_cd = '" & Session("emp_init") & "' or e.emp_init = '" & Session("emp_init") & "'))) = CASE " +
                              " WHEN ops$daemon.std_multi_co2.getstoretp(A.store_cd) = '41' then " +
                            "std_franchise.isValidCOStr('" & Session("emp_init") & "', store_cd) " +
                                "else           STD_MULTI_CO2.isValidStr6('" & Session("emp_init") & "', " +
                                 " (select s.store_cd from emp e, store s " +
                                "where a.EMP_CD_BUY Is Not null And e.home_store_cd = s.store_cd" +
                                    "  and (e.emp_cd = a.EMP_CD_BUY or " +
                                   "       e.emp_init = a.EMP_CD_BUY))) " +
                                "End "
        sql = sql & "ORDER BY LN# ASC"


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read Then
                txt_po.Text = MyDataReader.Item("PO_CD").ToString
                txt_store_cd.Text = MyDataReader.Item("STORE_CD").ToString
                txt_srt_cd.Text = MyDataReader.Item("PO_SRT_CD").ToString
                txt_buyer.Text = MyDataReader.Item("EMP_CD_BUY").ToString
                txt_status.Text = MyDataReader.Item("STAT_CD").ToString
                txt_vendor.Text = MyDataReader.Item("VE_CD").ToString
                txt_terms_pct.Text = MyDataReader.Item("TERMS_PCT").ToString
                txt_terms_days.Text = MyDataReader.Item("TERMS_DAYS").ToString
                txt_days_net.Text = MyDataReader.Item("DAYS_NET").ToString
                If IsDate(MyDataReader.Item("FINAL_DT").ToString) Then
                    txt_finalized.Text = FormatDateTime(MyDataReader.Item("FINAL_DT").ToString, DateFormat.ShortDate)
                End If
                If IsDate(MyDataReader.Item("WR_DT").ToString) Then
                    txt_written.Text = FormatDateTime(MyDataReader.Item("WR_DT").ToString, DateFormat.ShortDate)
                End If
                If IsDate(MyDataReader.Item("LST_CHNG_DT").ToString) Then
                    txt_last_chg.Text = FormatDateTime(MyDataReader.Item("LST_CHNG_DT").ToString, DateFormat.ShortDate)
                End If
                MyDataReader.Close()
            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        ' MM-5570
        ' Displaying of PO Line details
        If Not (String.IsNullOrWhiteSpace(txt_po.Text)) Then
            BindPoLineDetails(txt_po.Text.Trim)
        End If
    End Sub

    ' MM-5570
    ''' <summary>
    ''' Displays the PO Line details
    ''' </summary>
    ''' <param name="poNumber">PO number</param>
    ''' <remarks></remarks>
    Private Sub BindPoLineDetails(ByVal poNumber As String)

        Dim poDetails As DataTable = objPOBiz.GetPOLineDetails(poNumber)

	' Add new column into data table #ITREQUEST-337
        poDetails.Columns.Add(New DataColumn("BOOKING_STATUS"))

        ' Process po details to get booking status #ITREQUEST-337
        If poDetails.Rows.Count > 0 Then
            For Each dataRow As DataRow In poDetails.Rows
                ' Get purchase order line booking status #ITREQUEST-377
                dataRow("BOOKING_STATUS") = theSalesBiz.getPOLineBookingStatus(dataRow("PO_CD").ToString, dataRow("LN#").ToString)
            Next
        End If

        GridView2.DataSource = poDetails
        GridView2.DataBind()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_warning.Text = ""

        If Not IsPostBack Then
            If Find_Security("POMA", Session("emp_cd")) = "N" Then
                btn_Lookup.Enabled = False
                btn_Clear.Enabled = False
                lbl_Cust_Info.Text = "You don't have the appropriate securities to view Purchase Orders."

            End If
            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then
                Populate_Results()
                btn_Lookup.Enabled = False
            Else
                lbl_Cust_Info.Text = ""
            End If
        End If

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_po.Enabled = True
        txt_po.Text = ""
        txt_store_cd.Text = ""
        txt_status.Text = ""
        lbl_Cust_Info.Text = ""
        Response.Redirect("PO.aspx")

    End Sub

    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        If Not IsDate(txt_written.Text) And txt_written.Text & "" <> "" Then
            lbl_warning.Text = "You must enter a valid written date to continue"
        Else
            Response.Redirect("Main_PO_Lookup.aspx?po=" & HttpUtility.UrlEncode(txt_po.Text) & "&store_cd=" & HttpUtility.UrlEncode(txt_store_cd.Text) _
                & "&srt_cd=" & HttpUtility.UrlEncode(txt_srt_cd.Text) & "&wr_dt=" & HttpUtility.UrlEncode(txt_written.Text) & "&stat_cd=" & HttpUtility.UrlEncode(txt_status.Text))
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub GridView2_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView2.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim objSql1 As OracleCommand
        Dim sql As String = ""
        Dim sql1 As String = ""
        Dim ds As New DataSet
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDatareader As OracleDataReader

        If Not IsNothing(e.GetValue("PO_CD").ToString()) And e.GetValue("PO_CD").ToString <> "&nbsp;" Then
            Dim lbl_rcv As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView2.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_rcv"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_rcv) Then

                Try
                    conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                    conn2.Open()

                    sql = sql & "SELECT SUM(c.QTY) AS QTY_RCV "
                    sql = sql & "FROM PO a, PO_LN b, PO_LN$ACTN_HST c  "
                    sql = sql & "WHERE a.PO_CD = b.PO_CD AND b.ITM_CD = '" & e.GetValue("ITM_CD").ToString & "'  "
                    sql = sql & "AND c.PO_CD = b.PO_CD "
                    sql = sql & "AND b.LN# = c.LN# "
                    sql = sql & "and c.PO_ACTN_TP_CD = 'RCV' "
                    sql = sql & "AND a.PO_CD = '" & e.GetValue("PO_CD").ToString() & "'  "
                    sql = sql & "AND b.LN# = " & e.GetValue("LN#").ToString() & "  "

                    objSql = DisposablesManager.BuildOracleCommand(sql, conn2)

                    MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

                    If MyDatareader.Read() Then
                        If IsNumeric(MyDatareader.Item("QTY_RCV").ToString) Then
                            lbl_rcv.Text = CDbl(MyDatareader.Item("QTY_RCV").ToString)
                        Else
                            lbl_rcv.Text = "0"
                        End If
                    End If
                Catch
                    Throw
                End Try
            End If
        End If
        conn2.Close()

        ''Dec 15 add Ack field
        If Not IsNothing(e.GetValue("PO_CD").ToString()) And e.GetValue("PO_CD").ToString <> "&nbsp;" Then
            Dim lbl_ack As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView2.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_ack"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_ack) Then

                Try
                    conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                    conn2.Open()

                    sql1 = "SELECT 'Y' ACK "
                    sql1 = sql1 & "FROM PO_LN$ACTN_HST ph  "
                    sql1 = sql1 & "WHERE rownum < 2 and ph.PO_ACTN_TP_CD IN ('AKF','AKR','AKI','ADC','AKQ','855') "
                    sql1 = sql1 & "AND ph.PO_CD = '" & e.GetValue("PO_CD").ToString() & "'  "
                    sql1 = sql1 & "AND ph.LN# = " & e.GetValue("LN#").ToString() & "  "

                    objSql1 = DisposablesManager.BuildOracleCommand(sql1, conn2)

                    MyDatareader = DisposablesManager.BuildOracleDataReader(objSql1)

                    If MyDatareader.Read() Then
                        lbl_ack.Text = MyDatareader.Item("ACK").ToString
                    Else
                        lbl_ack.Text = "N"
                    End If
                Catch
                    Throw
                End Try
            End If
        End If
        conn2.Close()

    End Sub
End Class
