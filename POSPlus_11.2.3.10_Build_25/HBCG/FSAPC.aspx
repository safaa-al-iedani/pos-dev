<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="FSAPC.aspx.vb" Inherits="FSAPC" meta:resourcekey="PageResource1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" meta:resourcekey="Label2">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:DropDownList ID="srch_st_cd" runat="server" Style="position: relative" AppendDataBoundItems="true">
                </asp:DropDownList>
            </td>
            </tr>
        <tr>
             <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" meta:resourcekey="Label2">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                <asp:DropDownList ID="srch_zone_cd" runat="server" Style="position: relative" AppendDataBoundItems="true">
                </asp:DropDownList>
            </td>
                    
        </tr>
        <tr>
            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" meta:resourcekey="Label1">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                     <asp:DropDownList ID="srch_fsa" runat="server"  Style="position: relative" AppendDataBoundItems="true">
                     </asp:DropDownList>
            </td>
                        
        </tr>
         </table>
    <br />
         <dx:ASPxGridView ID="GridView2" runat="server" KeyFieldName="POSTAL_CD;ZONE_CD" AutoGenerateColumns="False" Width="98%" ClientInstanceName="GridView2" OnInitNewRow="GridView2_InitNewRow" oncelleditorinitialize="GridView2_CellEditorInitialize" OnBatchUpdate="GridView2_BatchUpdate" OnParseValue="GridView2_ParseValue">
    <%--<dx:ASPxGridView ID="GridView2" runat="server" enablecallbacks="false"  KeyFieldName="CO_GRP_CD;POSTAL_CD;ZONE_CD" AutoGenerateColumns="False" Width="98%" ClientInstanceName="GridView2" OnInitNewRow="GridView2_InitNewRow" OnBatchUpdate="GridView2_BatchUpdate">    --%>
     <SettingsEditing Mode="Batch"  />
                  <SettingsCommandButton>
         <UpdateButton Text="<%$ Resources:LibResources, Label885 %>" ></UpdateButton>
         <CancelButton Text="<%$ Resources:LibResources, Label886 %>"></CancelButton>
         <NewButton Text="<%$ Resources:LibResources, Label328 %>"></NewButton>
         <DeleteButton Text="<%$ Resources:LibResources, Label150 %>"></DeleteButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewDataTextColumn Caption="CO GROUP" FieldName="CO_GRP_CD" ReadOnly="true"  Visible="True" VisibleIndex="1">
                <PropertiesTextEdit MaxLength="3"></PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label924 %>"  FieldName="POSTAL_CD" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit MaxLength="6">
                    <ValidationSettings>
                        <RequiredField IsRequired="True"  ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <%--<RegularExpression ValidationExpression="[A-Z]\d[A-Z]\d[A-Z]\d" ErrorText="<%$ Resources:LibResources, Label909 %>" />--%>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label902 %>" FieldName="ZONE_CD" VisibleIndex="2">
                <PropertiesTextEdit MaxLength="3">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label158 %>" ReadOnly="true" FieldName="DES" VisibleIndex="3">
                <PropertiesTextEdit MaxLength="30">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="PR" FieldName="ST_CD" VisibleIndex="4">
                <PropertiesTextEdit MaxLength="2">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit> 
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label74 %>" FieldName="CHARGES" VisibleIndex="5">
                <PropertiesTextEdit MaxLength="10">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
<%--                         <RegularExpression ValidationExpression="^-?\d*\.{0,1}\d+$" ErrorText="<%$ Resources:LibResources, Label909 %>" />--%>
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

                        
            <dx:GridViewCommandColumn ShowNewButtonInHeader="True" ShowDeleteButton="True" VisibleIndex="14"/>
                  
        </Columns>
        
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="true"    />
        <SettingsText  ConfirmDelete="<%$ Resources:LibResources, Label908 %>" />
    </dx:ASPxGridView>
    <br />
    <table>
        <tr>
           
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label887 %>">
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;
            </td>
         
            <td>
                &nbsp;
            </td> 
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label888 %>">
                </dx:ASPxButton>
            </td>
           
            
        </tr>
    </table>
    <br />
    <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" ForeColor="Red" runat="server"
        Text="" Width="100%"> 
    </dx:ASPxLabel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
