'Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient
Imports System.Linq


Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization

Partial Class Ecofee
    Inherits POSBasePage
    Public Const gs_version As String = "Version 2.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen.
    '   2.0:
    '   (1) French implementation
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : Page Init Event Handler
    ''' Loads store Group/Store combo box based on logged on users company group code.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        


        populate_prov()

        populate_co_grp_cd()




        btn_search.Enabled = True
        btn_clr_screen.Enabled = True

        btn_update.Visible = False
    End Sub

    Protected Sub populate_co_grp_cd()

        If IsPostBack = False Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
            Dim sqlString As StringBuilder
            Dim ds As New DataSet
            Dim Mydatareader As OracleDataReader
            Dim homeStoreCode As String
            Dim companyCode As String
            Dim coGroupCode As String
            ' lbl_versionInfo.Text = gs_version
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            btn_search.Enabled = False
            btn_clr_screen.Enabled = False

            If (String.IsNullOrEmpty(Session("EMP_CD"))) Then
                Return
            End If

            Dim str_emp_init As String = LeonsBiz.GetEmpInit(Session("EMP_CD"))



            sqlString = New StringBuilder("SELECT distinct cg.co_grp_cd FROM store s, co_grp cg  ")
            sqlString.Append(" WHERE  cg.co_cd = s.co_cd ")
            sqlString.Append(" and std_multi_co2.isvalidstr3(:str_emp_init,s.store_cd) ='Y'") 'lucy





            objCmd.CommandText = sqlString.ToString()
            objCmd.Connection = conn
            objCmd.Parameters.Clear()
            '  objCmd.Parameters.Add(":co_grp_cd", OracleType.VarChar)
            '  objCmd.Parameters(":co_grp_cd").Value = coGroupCode
            '  If Not (homeStoreCode = "00" Or homeStoreCode = "10") Then
            'objCmd.Parameters.Add(":co_cd", OracleType.VarChar)
            ' objCmd.Parameters(":co_cd").Value = companyCode
            ' End If
            objCmd.Parameters.Add(":str_emp_init", OracleType.VarChar)
            objCmd.Parameters(":str_emp_init").Value = str_emp_init

            Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)

            Try
                dataAdapter.Fill(ds)
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

            '  DropDown_co_grp_cd.Items.Add("")

            If ds.Tables(0).Rows.Count = 0 Then
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                'lbl_warning.Text = Unable to find Store group/Store. Cannot continue
                lbl_warning.Text = Resources.CustomMessages.MSG0039
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
                Return
            End If
            Dim defaultCo_grp_cdIndex As Integer = 0
            For Each row As DataRow In ds.Tables(0).Rows

                DropDown_co_grp_cd.Items.Add(row.ItemArray(0).ToString())


            Next
            DropDown_co_grp_cd.SelectedIndex = defaultCo_grp_cdIndex

        End If




    End Sub
    Private Sub populate_prov()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim sqlString As StringBuilder
        Dim ds As New DataSet
        Dim Mydatareader As OracleDataReader
        Dim homeStoreCode As String
        Dim companyCode As String
        Dim coGroupCode As String
        ' lbl_versionInfo.Text = gs_version
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        btn_search.Enabled = False
        btn_clr_screen.Enabled = False

        sqlString = New StringBuilder("SELECT distinct s.st as prov  FROM lucy_prov s   ")

        ' sqlString = New StringBuilder("SELECT distinct s.st as prov  FROM store s   ")

        ' sqlString.Append("where std_multi_co2.isvalidstr4(:str_emp_init,s.store_cd) ='Y'")


        ' sqlString.Append(")")

        objCmd.CommandText = sqlString.ToString()
        objCmd.Connection = conn
        objCmd.Parameters.Clear()



        Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objCmd)

        Try
            dataAdapter.Fill(ds)
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()
        If ds.Tables(0).Rows.Count = 0 Then
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            'lbl_warning.Text = Unable to find Store group/Store. Cannot continue
            lbl_warning.Text = Resources.CustomMessages.MSG0039
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            Return
        End If
        Dim defaultProvIndex As Integer = 0
        For Each row As DataRow In ds.Tables(0).Rows


            drpdwn_prov.Items.Add(row.ItemArray(0).ToString())


        Next
        'this is defaul to home_store, commented
        drpdwn_prov.SelectedIndex = defaultProvIndex





    End Sub
    Private Function get_itm_des(ByRef p_itm_cd As String)

        Dim txtDes As String

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader




        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()


        sql = "select des from itm where itm_cd='" & UCase(p_itm_cd) & "'  "


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                txtDes = MyDataReader.Item("des")

            Else
                lbl_warning.Text = "Invalid sku"
                btn_update.Enabled = False
            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try
        Return txtDes
    End Function

    Private Function check_dup_record(ByRef p_rowid As String, ByRef p_prov As String, ByRef p_co_grp_cd As String, ByRef p_style As String, ByRef p_match_sku As String, ByRef p_eff_dt As Date, p_end_dt As Date)

        Dim l_rowid As String
        Dim l_co_grp_cd As String
        Dim l_prov As String
        Dim l_style As String
        Dim l_match_sku As String
        Dim l_eco_fee As Double
        Dim l_eff_dt As Date
        Dim l_end_dt As Date
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtCo_grp_cd As String = "Y"


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append("  co_grp_cd   ")
        sqlString.Append(" FROM electron_recycle_master ")
        sqlString.Append(" WHERE co_grp_cd = :l_co_grp_cd ")
        sqlString.Append(" and prov = :l_prov ")



        If Not isEmpty(p_style) Then
            sqlString.Append(" and style =upper( : l_style) ")

        End If

        If Not isEmpty(p_match_sku) Then
            sqlString.Append(" and match_sku =upper( : l_match_sku) ")

        End If

        If Not isEmpty(p_rowid) Then
            sqlString.Append(" and  rowid <>  : l_rowid ")
        End If

        If Not isEmpty(p_eff_dt) Then
            sqlString.Append("  and ( ")
            sqlString.Append(" ( eff_dt <=  : l_eff_dt  and end_dt >= :l_eff_dt )")
        End If

        If Not isEmpty(p_end_dt) Then
            sqlString.Append(" or  (eff_dt <=  : l_end_dt  and end_dt >= :l_end_dt) ")
            sqlString.Append(" ) ")
        End If


        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()

        '  If Not isEmpty(l_store_grp_cd) Then
        'objcmd.Parameters.Add(":l_store_grp_cd", OracleType.VarChar)
        ' objcmd.Parameters(":l_store_grp_cd").Value = l_store_grp_cd
        ' End If
        objcmd.Parameters.Add(":l_co_grp_cd", OracleType.VarChar)
        objcmd.Parameters(":l_co_grp_cd").Value = p_co_grp_cd

        If Not isEmpty(p_prov) Then

            objcmd.Parameters.Add(":l_prov", OracleType.VarChar)
            objcmd.Parameters(":l_prov").Value = p_prov
        End If
        If Not isEmpty(p_style) Then
            objcmd.Parameters.Add(":l_style", OracleType.VarChar)
            objcmd.Parameters(":l_style").Value = p_style
        End If
        If Not isEmpty(p_match_sku) Then
            objcmd.Parameters.Add(":l_match_sku", OracleType.VarChar)
            objcmd.Parameters(":l_match_sku").Value = p_match_sku
        End If

        If Not isEmpty(p_rowid) Then
            objcmd.Parameters.Add(":l_rowid", OracleType.VarChar)
            objcmd.Parameters(":l_rowid").Value = p_rowid
        End If

        If Not isEmpty(p_eff_dt) Then
            objcmd.Parameters.Add(":l_eff_dt", OracleType.DateTime)
            objcmd.Parameters(":l_eff_dt").Value = p_eff_dt
        End If

        If Not isEmpty(p_end_dt) Then
            objcmd.Parameters.Add(":l_end_dt", OracleType.DateTime)
            objcmd.Parameters(":l_end_dt").Value = p_end_dt
        End If


        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                txtCo_grp_cd = "N"
                lbl_warning.Text = "Similar record existed"
            Else
                txtCo_grp_cd = "Y"


            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try
        Return txtCo_grp_cd
    End Function


    Private Sub populate_txt_box()

        Dim dgItem As DataGridItem
        Dim drpdwn As TextBox
        Dim txtPD As String
        Dim txtStore As String
        Dim l_txt_style As TextBox
        Dim l_txt_match_sku As TextBox
        Dim l_txt_co_grp_cd As TextBox
        Dim l_txt_eco_fee As TextBox
        Dim l_txt_eff_date As TextBox
        Dim l_txt_end_date As TextBox
        Dim l_txt_des As TextBox

        Dim txtRowId As String
        Dim txtStyle As String
        Dim txtProv As String

        Dim txtMatch_sku As String
        Dim txtCo_grp_cd As String
        Dim txtEco_fee As String
        Dim txtEff_date As String
        Dim txtEnd_date As String

        'iterate through all the rows  
        For i As Integer = 0 To GV_SRR.Items.Count - 1
            dgItem = GV_SRR.Items(i)

            l_txt_style = CType(GV_SRR.Items(i).FindControl("txt_style"), TextBox)
            l_txt_match_sku = CType(GV_SRR.Items(i).FindControl("txt_match_sku"), TextBox)
            l_txt_des = CType(GV_SRR.Items(i).FindControl("txt_des"), TextBox)

            l_txt_eco_fee = CType(GV_SRR.Items(i).FindControl("txt_eco_fee"), TextBox)
            l_txt_eff_date = CType(GV_SRR.Items(i).FindControl("txt_eff_date"), TextBox)
            l_txt_end_date = CType(GV_SRR.Items(i).FindControl("txt_end_date"), TextBox)



            txtProv = dgItem.Cells(1).Text
            txtStyle = dgItem.Cells(2).Text
            txtMatch_sku = dgItem.Cells(3).Text   ' provice (index 0 is  rowid, 1 is province, 2 style, 3 match_sku,5 eco_fee, 6 eff_dt,7 end_dt)
            ' txtStyle = dgItem.Cells(4).Text
            txtEco_fee = dgItem.Cells(5).Text
            txtEff_date = dgItem.Cells(6).Text
            txtEnd_date = dgItem.Cells(7).Text

            '  txtCo_grp_cd = dgItem.Cells(5).Text
            '  If isEmpty(txtCo_grp_cd) Then
            'txtCo_grp_cd = Nothing
            ' End If

            If isEmpty(txtEco_fee) Then
                txtEco_fee = Nothing
            End If

            If isEmpty(txtEff_date) Then
                txtEff_date = Nothing
            End If

            If isEmpty(txtEnd_date) Then
                txtEnd_date = Nothing
            End If
            l_txt_style.Text = txtStyle
            l_txt_match_sku.Text = txtMatch_sku
            ' l_txt_co_grp_cd.Text = txtCo_grp_cd
            l_txt_eco_fee.Text = txtEco_fee
            l_txt_eff_date.Text = txtEff_date
            l_txt_end_date.Text = txtEnd_date

            If IsDate(txtEff_date) Then
                l_txt_eff_date.Text = FormatDateTime(txtEff_date, DateFormat.ShortDate)
            End If
            If IsDate(txtEnd_date) Then
                l_txt_end_date.Text = FormatDateTime(txtEnd_date, DateFormat.ShortDate)
            End If

            l_txt_des.Text = get_itm_des(txtMatch_sku)
        Next




    End Sub





    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Dim _PanelHeader As DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel = Master.FindControl("arpMain")
        Dim _lblHeader As DevExpress.Web.ASPxEditors.ASPxLabel = _PanelHeader.FindControl("lbl_Round_Header")
        _lblHeader.Text = Resources.CustomMessages.MSG0079
        _lblHeader.Text = "Ecofee Maintenance"
    End Sub



    Protected Sub click_btn_search(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_prov As String
        Dim l_style As String
        Dim l_match_sku As String


        lbl_warning.Text = Nothing


        '  l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        ' dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        ' l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd

        ' l_style = drpdwn_min_pick_status.SelectedItem.Value
        ' l_match_sku = drpdwn_enabled.SelectedItem.Value


        lbl_msg.Text = Nothing

        reload()



    End Sub
    Protected Sub reload()

        Dim l_id As String
        Dim l_co_grp_cd As String
        Dim l_prov As String
        Dim l_style As String
        Dim l_match_sku As String
        Dim l_des As String
        Dim txtDes As String
        lbl_warning.Text = Nothing




        ' Dim DDL As DropDownList = CType(e.Item.FindControl("dropdownstore"), DropDownList)






        l_co_grp_cd = DropDown_co_grp_cd.SelectedItem.Value
        l_prov = drpdwn_prov.SelectedItem.Value
        ' l_style = drpdwn_min_pick_status.SelectedItem.Value
        l_style = text_style.Text
        l_match_sku = text_match_sku.Text


        lbl_msg.Text = Nothing
        If isEmpty(l_co_grp_cd) Then
            lbl_msg.Text = "Please select a value from dropdown co_grp_cd"
            Exit Sub
        End If
        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            ds = New DataSet
            GV_SRR.DataSource = ""



            sqlString = New StringBuilder("SELECT  ")

            '  sqlString.Append(" rowid,prov,style,match_sku,ecofee,eff_dt,end_dt ")
            sqlString.Append(" rowid,prov ,upper(style) style ,match_sku,co_grp_cd,eco_fee, eff_dt, end_dt")
            sqlString.Append(" FROM electron_recycle_master ")
            ' sqlString.Append(" WHERE prov in(select prov FROM store_grp$store ")
            sqlString.Append(" WHERE co_grp_cd = :l_co_grp_cd ")
            If CheckBox_end_dt.Checked = True Then
                sqlString.Append(" and trunc(end_dt) ='31-DEC-2049' ")
            End If

            If Not isEmpty(l_prov) Then
                sqlString.Append(" and prov = :l_prov ")

            End If
            If Not isEmpty(l_style) Then
                sqlString.Append(" and style =upper( : l_style) ")

            End If

            If Not isEmpty(l_match_sku) Then
                sqlString.Append(" and match_sku =upper( : l_match_sku) ")

            End If
            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()

            '  If Not isEmpty(l_store_grp_cd) Then
            'objcmd.Parameters.Add(":l_store_grp_cd", OracleType.VarChar)
            ' objcmd.Parameters(":l_store_grp_cd").Value = l_store_grp_cd
            ' End If
            objcmd.Parameters.Add(":l_co_grp_cd", OracleType.VarChar)
            objcmd.Parameters(":l_co_grp_cd").Value = l_co_grp_cd

            If Not isEmpty(l_prov) Then

                objcmd.Parameters.Add(":l_prov", OracleType.VarChar)
                objcmd.Parameters(":l_prov").Value = l_prov
            End If
            If Not isEmpty(l_style) Then
                objcmd.Parameters.Add(":l_style", OracleType.VarChar)
                objcmd.Parameters(":l_style").Value = l_style
            End If
            If Not isEmpty(l_match_sku) Then
                objcmd.Parameters.Add(":l_match_sku", OracleType.VarChar)
                objcmd.Parameters(":l_match_sku").Value = l_match_sku
            End If




            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


            objAdaptor.Fill(ds)


            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count




            If (MyDataReader.Read()) Then
                GV_SRR.Enabled = True
                GV_SRR.Visible = True
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    Dim l_va As String
                    Dim l_st As String





                    If Not IsDBNull(ds.Tables(0).Rows(i)("prov")) Then

                        l_prov = ds.Tables(0).Rows(i)("prov")
                    Else
                        l_prov = ""
                    End If



                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("prov")) Then
                            err_cnt = 1
                        End If


                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)
                btn_update.Visible = True



            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()
                lbl_msg.Text = "No data found."
                GV_SRR.Enabled = False
                GV_SRR.Visible = False
                btn_update.Visible = False
            Else
                '   populate_pd()
                populate_txt_box()

            End If

            MyDataReader.Close()
            conn.Close()


        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try


    End Sub






    Public Sub insert_to_table(ByRef p_co_grp_cd As String, ByRef p_prov As String, ByRef p_style As String, ByRef p_match_sku As String,
                               ByRef p_eco_fee As Double, ByRef p_eff_dt As Date, ByRef p_end_dt As Date)



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            lbl_msg.Text = Nothing

            sql = "Insert into electron_recycle_master "
            sql = sql & " (co_grp_cd,prov,style,match_sku,eco_fee,eff_dt,end_dt  ) "
            sql = sql & " select upper(:co_grp_cd),:prov,upper(:style),upper(:match_sku),:eco_fee,:eff_dt ,:end_dt  "
            sql = sql & " from dual"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":co_grp_cd", OracleType.VarChar)
            objsql.Parameters(":co_grp_cd").Value = p_co_grp_cd
            objsql.Parameters.Add(":prov", OracleType.VarChar)
            objsql.Parameters(":prov").Value = p_prov
            objsql.Parameters.Add(":style", OracleType.VarChar)
            objsql.Parameters(":style").Value = p_style
            objsql.Parameters.Add(":match_sku", OracleType.VarChar)
            objsql.Parameters(":match_sku").Value = p_match_sku
            objsql.Parameters.Add(":eco_fee", OracleType.Double)
            objsql.Parameters(":eco_fee").Value = p_eco_fee
            objsql.Parameters.Add(":eff_dt", OracleType.DateTime)
            objsql.Parameters(":eff_dt").Value = p_eff_dt
            objsql.Parameters.Add(":end_dt", OracleType.DateTime)
            objsql.Parameters(":end_dt").Value = p_end_dt
            'Execute DataReader 
            objsql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Protected Sub click_btn_insert(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim l_id As String
        Dim l_co_grp_cd As String
        Dim l_prov As String
        Dim l_style As String
        Dim l_match_sku As String
        Dim l_eco_fee As Double
        Dim l_eff_dt As Date
        Dim l_end_dt As Date

        '  l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        ' dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        '  l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd

        lbl_msg.Text = Nothing
        lbl_warning.Text = Nothing
        Dim l_rowid As String = Nothing
        l_co_grp_cd = DropDown_co_grp_cd.SelectedItem.Value
        l_style = text_style.Text
        l_match_sku = text_match_sku.Text

        l_eff_dt = can_eff_date.SelectedDate


        If can_eff_date.SelectedDate.Date = DateTime.MinValue Then
            l_eff_dt = FormatDateTime(Today, DateFormat.ShortDate)

        End If

        l_end_dt = can_end_date.SelectedDate
        If can_end_date.SelectedDate.Date = DateTime.MinValue Then
            l_end_dt = FormatDateTime("12/31/2049", DateFormat.ShortDate)
        End If


        l_prov = drpdwn_prov.SelectedItem.Value

        If isEmpty(l_co_grp_cd) Then
            lbl_msg.Text = "Please select a value from dropdown co_grp_cd"
            Exit Sub
        ElseIf isEmpty(l_prov) Then
            lbl_msg.Text = "Please select a value from dropdown Province"
            Exit Sub
            ' ElseIf isEmpty(l_store_grp_cd) Then
            '  lbl_warning.Text = "Please select a value from dropdown Store_Group/Store"
            ' Exit Sub
        ElseIf isEmpty(l_style) Then
            lbl_msg.Text = "Please enter a style"
            Exit Sub
        ElseIf isEmpty(l_match_sku) Then
            lbl_msg.Text = "Please enter a match_sku"
            Exit Sub
        End If

        Dim strYes As String = "N"

        strYes = check_dup_record(l_rowid, l_prov, l_co_grp_cd, l_style, l_match_sku, l_eff_dt, l_end_dt)

        If strYes = "Y" Then
            insert_to_table(l_co_grp_cd, l_prov, l_style, l_match_sku, l_eco_fee, l_eff_dt, l_end_dt)


            reload()
        End If
    End Sub


    Protected Sub update_record()



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String


        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        lbl_msg.Text = Nothing
        lbl_warning.Text = Nothing

        Dim dgItem As DataGridItem
        Dim drpdwn As DropDownList


        Dim l_txt_prov As TextBox
        Dim l_txt_style As TextBox
        Dim l_txt_match_sku As TextBox
        Dim l_txt_co_grp_cd As TextBox
        Dim l_txt_eco_fee As TextBox
        Dim l_txt_eff_date As TextBox
        Dim l_txt_end_date As TextBox
        Dim l_txt_des As TextBox

        Dim txtStore As String
        Dim txtRowId As String
        Dim txtStyle As String
        Dim txtProv As String

        Dim txtMatch_sku As String
        Dim txtCo_grp_cd As String
        Dim txtEco_fee As String
        Dim txtEff_date As String
        Dim txtEnd_date As String
        Dim txtDes As String

        Dim dblEco_fee As Double
        Dim dtEff_date As Date
        Dim dtEnd_date As Date

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()


            'iterate through all the rows  
            For i As Integer = 0 To GV_SRR.Items.Count - 1
                dgItem = GV_SRR.Items(i)

                txtRowId = dgItem.Cells(0).Text
                ' drpdwn = CType(GV_SRR.Items(i).FindControl("drpdwn_pv"), DropDownList)
                ' txtProv = drpdwn.Text

                txtCo_grp_cd = dgItem.Cells(2).Text
                txtProv = dgItem.Cells(1).Text
                l_txt_style = CType(GV_SRR.Items(i).FindControl("txt_style"), TextBox)
                l_txt_match_sku = CType(GV_SRR.Items(i).FindControl("txt_match_sku"), TextBox)
                l_txt_des = CType(GV_SRR.Items(i).FindControl("txt_des"), TextBox)

                l_txt_eco_fee = CType(GV_SRR.Items(i).FindControl("txt_eco_fee"), TextBox)
                l_txt_eff_date = CType(GV_SRR.Items(i).FindControl("txt_eff_date"), TextBox)
                l_txt_end_date = CType(GV_SRR.Items(i).FindControl("txt_end_date"), TextBox)


                '  txtCo_grp_cd = l_txt_co_grp_cd.Text
                txtStyle = l_txt_style.Text
                txtMatch_sku = l_txt_match_sku.Text
                txtEco_fee = l_txt_eco_fee.Text
                txtEff_date = l_txt_eff_date.Text
                txtEnd_date = l_txt_end_date.Text


                If IsNumeric(txtEco_fee) And txtEco_fee < 999 Then

                    dblEco_fee = txtEco_fee
                Else
                    lbl_warning.Text = txtEco_fee + " is invalid number"

                    Exit Sub
                End If

                If IsDate(txtEff_date) Then
                    l_txt_eff_date.Text = FormatDateTime(txtEff_date, DateFormat.ShortDate)
                    dtEff_date = l_txt_eff_date.Text
                Else
                    lbl_warning.Text = txtEff_date + " is invalid date"
                    Exit Sub
                End If
                If dtEff_date = DateTime.MinValue Then
                    dtEff_date = FormatDateTime(Today, DateFormat.ShortDate)
                End If

                If IsDate(txtEnd_date) Then
                    l_txt_end_date.Text = FormatDateTime(txtEnd_date, DateFormat.ShortDate)
                    dtEnd_date = l_txt_end_date.Text
                Else
                    lbl_warning.Text = txtEnd_date + " is invalid date"
                    Exit Sub
                End If
                If dtEnd_date = DateTime.MinValue Then
                    dtEnd_date = FormatDateTime("12/31/2049", DateFormat.ShortDate)
                End If


                Dim strYes As String = "N"
                Dim strStyle As String = "N"
                Dim strMatch As String = "N"
                strStyle = check_style(txtStyle)
                If strStyle = "Y" Then
                    strMatch = check_match_sku(txtMatch_sku)
                    If strMatch = "Y" Then
                        strYes = check_dup_record(txtRowId, txtProv, txtCo_grp_cd, txtStyle, txtMatch_sku, dtEff_date, dtEnd_date)
                        If strYes = "Y" Then
                            update_each_record(txtRowId, txtStyle, txtMatch_sku, dblEco_fee, dtEff_date, dtEnd_date)
                        End If
                    Else
                        lbl_warning.Text = txtMatch_sku + " is invalid match sku"
                        Exit Sub
                        
                    End If
                Else
                    lbl_warning.Text = txtStyle + " is invalid style"
                    Exit Sub
                End If
            Next

            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try


    End Sub


    Protected Sub update_each_record(ByRef p_rowid As String,
                                     ByRef p_style As String, ByRef p_match_sku As String, ByRef p_eco_fee As Double, ByRef p_eff_date As Date,
                                     ByRef p_end_date As Date)



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String


        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        lbl_msg.Text = Nothing


        Dim dgItem As DataGridItem
        Dim drpdwn As DropDownList
        Dim txtRowId As String
        Dim txtProv As String
        Dim txtCo_grp_cd As String
        Dim txtStyle As String
        Dim txtMatch_sku As String
        Dim txEco_fee As String
        Dim txtEff_date As Date
        Dim txtEnd_date As Date

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()



            sql = "update electron_recycle_master "
            sql = sql & " set style=:txtStyle, match_sku=:txtMatch_sku,eco_fee=:txtEco_fee, eff_dt=:txtEff_date, end_dt=:txtEnd_date"
            sql = sql & " where rowid =:txtRowId"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":txtRowId", OracleType.VarChar)
            objsql.Parameters(":txtRowId").Value = p_rowid


            objsql.Parameters.Add(":txtMatch_sku", OracleType.VarChar)
            objsql.Parameters(":txtMatch_sku").Value = p_match_sku
            objsql.Parameters.Add(":txtStyle", OracleType.VarChar)
            objsql.Parameters(":txtStyle").Value = p_style
            objsql.Parameters.Add(":txtEco_fee", OracleType.Double)
            objsql.Parameters(":txtEco_fee").Value = p_eco_fee
            objsql.Parameters.Add(":txtEff_date", OracleType.DateTime)
            objsql.Parameters(":txtEff_date").Value = p_eff_date
            objsql.Parameters.Add(":txtEnd_date", OracleType.DateTime)
            objsql.Parameters(":txtEnd_date").Value = p_end_date
            'Execute DataReader 

            objsql.ExecuteNonQuery()


            lbl_warning.Text = "Record updated"

            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try


    End Sub
    Public Sub click_btn_update(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim l_style As String
        Dim l_match_sku As String



        lbl_warning.Text = Nothing

        l_style = text_style.Text
        l_match_sku = text_match_sku.Text


        update_record()

        ' reload()

    End Sub




    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GV_SRR.DeleteCommand

        Dim txtRowid As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String


        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim oldRowid As String
        lbl_msg.Text = Nothing


        txtRowid = GV_SRR.DataKeys(e.Item.ItemIndex)

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()



            sql = "delete from electron_recycle_master "
            sql = sql & " where rowid =:txtRowId"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":txtRowId", OracleType.VarChar)
            objsql.Parameters(":txtRowId").Value = txtRowid


            'Execute DataReader 

            objsql.ExecuteNonQuery()


            lbl_msg.Text = "one record deleted, please click search again to view it"

            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try

        GV_SRR.Enabled = False
        GV_SRR.Visible = False

        btn_update.Visible = False

        ' reload()      'for some reason, reload will cause another delete


    End Sub






    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : To Clear Grid, search criteria and remove sessions
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_clr_screen_Click(sender As Object, e As EventArgs)
        Dim l_id As String



        GV_SRR.Enabled = False
        GV_SRR.Visible = False

        btn_update.Visible = False


        '  drpdwn_store.SelectedIndex = 0
        drpdwn_prov.SelectedIndex = 0

        text_match_sku.Text = Nothing

        text_style.Text = Nothing

        can_eff_date.SelectedDate = DateTime.MinValue
        can_end_date.SelectedDate = DateTime.MinValue

        lbl_warning.Text = ""
        lbl_msg.Text = ""
    End Sub

    Protected Sub txt_eff_date_TextChanged(sender As Object, e As EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim l_txt_eff_date As TextBox



        Dim txtEff_date As String


        l_txt_eff_date = CType(dgItem.FindControl("txt_eff_date"), TextBox)

        txtEff_date = l_txt_eff_date.Text
        lbl_warning.Text = Nothing
        If Not (isEmpty(txtEff_date)) Then
            If Not (IsDate(txtEff_date)) Then

                lbl_warning.Text = "Invalid date"
                btn_update.Enabled = False
            Else
                btn_update.Enabled = True
            End If
        Else
            btn_update.Enabled = True
        End If

    End Sub

    Protected Sub txt_end_date_TextChanged(sender As Object, e As EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim l_txt_end_date As TextBox



        Dim txtEnd_date As String


        l_txt_end_date = CType(dgItem.FindControl("txt_end_date"), TextBox)

        txtEnd_date = l_txt_end_date.Text
        lbl_warning.Text = Nothing

        If Not (isEmpty(txtEnd_date)) Then
            If Not (IsDate(txtEnd_date)) Then

                lbl_warning.Text = "Invalid date"
                btn_update.Enabled = False
            Else
                btn_update.Enabled = True
            End If
        Else
            btn_update.Enabled = True
        End If


    End Sub




    Protected Sub txt_eco_fee_TextChanged(sender As Object, e As EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim l_txt_eco_fee As TextBox



        Dim txtEco_fee As String


        l_txt_eco_fee = CType(dgItem.FindControl("txt_eco_fee"), TextBox)

        txtEco_fee = l_txt_eco_fee.Text

        lbl_warning.Text = Nothing
        If (Not isEmpty(txtEco_fee)) Then
            If Not (IsNumeric(txtEco_fee)) Then

                lbl_warning.Text = txtEco_fee + "is invalid number"

            ElseIf txtEco_fee > 999 Then
                lbl_warning.Text = txtEco_fee + "is invalid number"

            Else
                btn_update.Enabled = True
            End If
        Else
            btn_update.Enabled = True
        End If

    End Sub
    Public Function check_style(ByRef p_style As String)




        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader

        Dim txtStyle As String

        Dim strY As String = "N"


        txtStyle = p_style


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()


        sql = "select style_cd from inv_mnr_style where style_cd='" & UCase(txtStyle) & "'  "


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then

                strY = "Y"

            Else

                lbl_warning.Text = txtStyle + "is invalid style"

                strY = "N"

            End If

            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try


        Return strY

    End Function

    Public Function check_match_sku(ByRef p_match_sku As String)



        Dim txtMatch_sku As String

        Dim txtDes As String
        Dim strY As String
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader





        txtMatch_sku = p_match_sku


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()


        sql = "select des from itm where itm_cd='" & UCase(txtMatch_sku) & "'  "


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                strY = "Y"

            Else

                lbl_warning.Text = txtMatch_sku + " is invalid match sku"

                strY = "N"

            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try
        Return strY
    End Function


    Protected Sub txt_style_TextChanged(sender As Object, e As EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim l_txt_style As TextBox


        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader

        Dim txtStyle As String


        l_txt_style = CType(dgItem.FindControl("txt_style"), TextBox)

        txtStyle = l_txt_style.Text


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()


        sql = "select style_cd from inv_mnr_style where style_cd='" & UCase(txtStyle) & "'  "


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If Not MyDataReader.Read() Then





                lbl_msg.Text = txtStyle + " is invalid style"



            End If

            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try




    End Sub
    Protected Sub txt_match_sku_TextChanged(sender As Object, e As EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim l_txt_match_sku As TextBox

        Dim l_txt_des As TextBox

        Dim txtMatch_sku As String

        Dim txtDes As String

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        l_txt_match_sku = CType(dgItem.FindControl("txt_match_sku"), TextBox)


        txtMatch_sku = l_txt_match_sku.Text


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()


        sql = "select des from itm where itm_cd='" & UCase(txtMatch_sku) & "'  "


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                txtDes = MyDataReader.Item("des")


                l_txt_des = CType(dgItem.FindControl("txt_des"), TextBox)


                l_txt_des.Text = txtDes


            Else
                lbl_msg.Text = txtMatch_sku + " is invalid match sku"

            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try
    End Sub


End Class