<%@ Page Language="VB" AutoEventWireup="false" CodeFile="User_Defined_Fields.aspx.vb"
    Inherits="User_Defined_Fields" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DataGrid ID="MyDataGrid" runat="server" Width="461px" BorderColor="Black" CellPadding="3"
                AutoGenerateColumns="False" DataKeyField="FLD_NAME" Height="16px" PageSize="7"
                AlternatingItemStyle-BackColor="Beige" CssClass="style5" ShowHeader="False">
                <Columns>
                    <asp:BoundColumn HeaderText="FLD_NAME" DataField="FLD_NAME" Visible="False" />
                    <asp:BoundColumn HeaderText="FLD_LENGTH" DataField="FLD_LENGTH" Visible="False" />
                    <asp:BoundColumn DataField="FLD_TITLE" />
                    <asp:TemplateColumn>
                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:TextBox ID="txt_fld_value" runat="server" Width="300" CssClass="style5" Visible="False" AutoPostBack="true" OnTextChanged="UDF_Update" ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                <HeaderStyle Font-Bold="True" ForeColor="Black" />
                <EditItemStyle CssClass="style5" />
                <ItemStyle CssClass="style5" />
            </asp:DataGrid>
        </div>
    </form>
</body>
</html>
