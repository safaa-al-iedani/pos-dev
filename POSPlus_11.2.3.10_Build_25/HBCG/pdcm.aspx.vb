﻿Imports System.Collections.Generic
Imports System.Data.OracleClient
Imports DevExpress.Web.ASPxGridView

Partial Class Default2
    Inherits POSBasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("lucy_cmnt") = ""
        'Daniela french
        Dim cult As String = UICulture
        If InStr(cult.ToLower, "fr") >= 1 Then
            cult = "FR"
        End If
        bindgrid(cult)
    End Sub


    Public Sub bindgrid(ByVal cult As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim SoLnGItm As DataGridItem

        ds = New DataSet
        GridView1.DataSource = ""
        'CustTable.Visible = False

        GridView1.Visible = True
        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        'Daniela July 14
        sql = "SELECT seq,decode('" + cult + "', 'FR', cmnt_fr, cmnt) cmnt FROM lucy_pdcm order by seq"


        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count
        Dim str_seq As String
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then


                GridView1.DataSource = ds
                GridView1.DataBind()
            Else
                GridView1.Visible = False
            End If

            Dim i As Integer

            For i = 0 To numrows - 1 'lucy
                Dim val As Object
                ' val = GridView1.GetR' val = GridViowValues(2, (TryCast(GridView1.Columns(3), GridViewDataTextColumn)).FieldName)
                '  str_seq = GridView1.Columns(0).ToString
                ' str_seq = GridView1.Columns(1).ToString
                ' str_seq = GridView1.Columns(2).ToString

            Next


            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub


     
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim i As Integer
        Dim itm As List(Of Object) = GridView1.GetSelectedFieldValues("CMNT")
        ' Dim itm As List(Of Object) = GridView1.GetSelectedFieldValues("SEQ")
        Dim itm_list As String = ""
        Dim str_cmnt As String

        For i = 0 To itm.Count - 1
            ' itm_list = itm_list & itm(i).ToString() & ","                'lucy this will be combined
            itm_list = itm_list & Trim(itm(i).ToString()) & vbCrLf

            ' itm_list = itm(i).ToString()
            str_cmnt = itm_list
            Session("lucy_cmnt") = str_cmnt
        Next i

        Session("scomments") = Session("scomments") & Session("lucy_cmnt")
        Session("lucy_cmnt") = ""
        If Len(Session("del_doc_num")) > 8 Then
            Session("from_lucy_cmnt") = "YES"

            Response.Redirect("salesordermaintenance.aspx?del_doc_num=" & Session("del_doc_num") & "&query_returned=Y")
        Else
            Response.Redirect("comments.aspx")
        End If

    End Sub
End Class
