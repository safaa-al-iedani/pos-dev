<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Relationship_Lookup.aspx.vb"
    Inherits="Relationship_Lookup" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server">
        <dx:ASPxGridView ID="GridView3" runat="server" Width="98%" AutoGenerateColumns="False"
            KeyFieldName="REL_NO">
            <SettingsPager Visible="False">
            </SettingsPager>
            <SettingsBehavior AllowSort="False" />
            <Columns>
                <dx:GridViewDataTextColumn Caption="Relationship" FieldName="REL_NO" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Written Date" FieldName="WR_DT" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Status" FieldName="REL_STATUS" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Customer" FieldName="CORP_NAME" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Last Name" FieldName="LNAME" Visible="False"
                    VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="City" FieldName="CITY" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label229 %>" FieldName="HPHONE" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Total" FieldName="PROSPECT_TOTAL" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Store" FieldName="STORE_CD" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="7">
                    <DataItemTemplate>
                        <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                            <Items>
                                <dx:MenuItem Text="Select" NavigateUrl="logout.aspx">
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <table id="nav_table" runat="server" width="100%">
            <tr>
                <td align="center">
                    <table width="65%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="40%" align="center">
                                <dx:ASPxButton ID="btn_search_again" runat="server" Text="<%$ Resources:LibResources, Label527 %>">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="Div1">
        <div class="global_page_normaltext" style="width: 300px; padding-top: 4px;">
            <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
        </div>
    </div>
</asp:Content>
