<%@ Page Language="VB" AutoEventWireup="false" CodeFile="warranties.aspx.vb" Inherits="warranties" %>

<%@ Register assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Additional Warranty Details</title>
    <link href="report_styles.css" rel="stylesheet" type="text/css" />
</head>
<body leftmargin="20px">
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="VSN" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="lbl_curr_warr" runat="server" Text=""></asp:Label>
            <asp:Label ID="lbl_ext_warr" runat="server" Text="Your purchase is also eligible for the following extended warranties:"></asp:Label>
            <br />
            <asp:RadioButtonList ID="ADD_WARR" runat="server" AutoPostBack="true" >
            </asp:RadioButtonList>
            <br />
            <asp:Label ID="lbl_ext_warr2" runat="server" Text="Or you may choose to warranty this item with one of these previously selected extended warranties:"></asp:Label>
            <br />
             <asp:RadioButtonList ID="PrevWarrList" runat="server" AutoPostBack="true" >
            </asp:RadioButtonList>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Add Selected Items" />
            <asp:Button ID="Button2" runat="server" Text="<%$ Resources:LibResources, Label350 %>" OnClientClick="window.close();" />
            <br />
            <dx:ASPxLabel ID="lbl_error" runat="server" Width="100%">
            </dx:ASPxLabel>
        </div>
    </form>
</body>
</html>
