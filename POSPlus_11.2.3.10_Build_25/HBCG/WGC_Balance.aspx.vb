Imports System.Xml
Imports World_Gift_Utils
Imports HBCG_Utils
Imports System.Data.OracleClient
Imports SD_Utils
Imports Renci.SshNet.Common


Partial Class WGC_Balance
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    'sabrina add
    Public Shared g_filename As String = ""
    Public Shared g_cmd As String = ""
    Public Shared g_card_bal As String = ""
    Public Shared g_card_num As String = ""
    Public Shared g_sel_printer As String = ""

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        theSystemBiz.SaveAuditLogComment("GC_BALANCE", TextBox1.Text & " checked account balance")
        'sabrina add
        Dim v_card_num As String = ""

        'TextBox1.Focus()

        Dim merchant_id As String = ""
        merchant_id = Lookup_Merchant("GC", Session("home_store_cd"), "GC")
        Dim myipadd As String = Session("clientip")
        Dim sessionid As String = Session.SessionID.ToString.Trim

        'sabrina comment out  check with Donna if merchant_id validation is required
        'If String.IsNullOrEmpty(merchant_id) Then
        '    lbl_response.Text = "No Gift Card Merchant ID found for your home store code"
        '    Exit Sub
        'End If
        '
        '    Dim xml_response As XmlDocument = WGC_SendRequestAndGetResponse(merchant_id, "balance", TextBox1.Text)
        '    lbl_response.Text = "Response Received: " & FormatDateTime(Date.Now, DateFormat.LongTime) & vbCrLf
        '    If IsNumeric(xml_response.InnerText) Then
        '        lbl_response.Text = lbl_response.Text & "Card Balance: $" & FormatNumber(CDbl(xml_response.InnerText), 2) & vbCrLf
        '        theSystemBiz.SaveAuditLogComment("GC_BALANCE_RESPONSE", TextBox1.Text & " Card Balance: $" & FormatNumber(CDbl(xml_response.InnerText), 2))
        '    Else
        '        lbl_response.Text = lbl_response.Text & xml_response.InnerText
        '        theSystemBiz.SaveAuditLogComment("GC_BALANCE_RESPONSE", TextBox1.Text & " " & xml_response.InnerText)
        '    End If
        'Catch
        '    lbl_response.Text = Err.Description
        'End Try

        Dim myret_string As String = ""
        Dim v_term_id As String = ""

        'Dim v_term_id = LeonsBiz.get_term_id(Session("clientip"))

        ' sabrina feb26
        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If
        theSystemBiz.SaveAuditLogComment("GC_BALANCE", TextBox1.Text & " checked account balance")

        Try
            If isEmpty(v_term_id) Or v_term_id = "empty" Then
                lbl_response.Text = "Invalid Terminal ID " & v_term_id
                Exit Sub
            End If
        Catch
            lbl_response.Text = Err.Description
        End Try

        Dim mybal As Integer = 0
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        sycmd.Connection = conn
        sycmd.CommandText = "std_brk_pos1.sendsubTp4"
        sycmd.CommandType = CommandType.StoredProcedure

        'subtp4
        sycmd.Parameters.Add("p_req_tp", OracleType.VarChar).Value = "D3"  'Balance Inq.
        sycmd.Parameters.Add("p_crdnum", OracleType.VarChar).Value = "" 'sabrina (brick requirement) TextBox1.Text
        sycmd.Parameters.Add("p_crdtoken", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_pswd", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_amt", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_tran_no", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_term_id", OracleType.VarChar).Value = v_term_id
        sycmd.Parameters.Add("p_currency", OracleType.VarChar).Value = "124"
        sycmd.Parameters.Add("p_crdnum2", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_opid", OracleType.VarChar).Value = ""
        'end of subtp4

        sycmd.Parameters.Add("p_sessionid", OracleType.VarChar).Value = sessionid
        sycmd.Parameters.Add("p_ackind", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_authno", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_transtp", OracleType.VarChar, 100).Direction = ParameterDirection.Output

        sycmd.Parameters.Add("p_aut", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crn", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_exp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_res", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_rcp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        'gift card
        sycmd.Parameters.Add("p_nba", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bnb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bpb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crf", OracleType.VarChar, 150).Direction = ParameterDirection.Output

        sycmd.Parameters.Add("p_outfile", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

        Try
            syda.Fill(syds)

        Catch ex As Exception
            lbl_response.Text = ex.Message.ToString()
            conn.Close()
            Throw
        End Try

        myret_string = sycmd.Parameters("p_res").Value.ToString
        lbl_response.Text = "Balance: " & sycmd.Parameters("p_nba").Value.ToString & "  (" & sycmd.Parameters("p_rcp").Value.ToString & "-" & sycmd.Parameters("p_dsp").Value.ToString & ")"

        If IsDBNull(sycmd.Parameters("p_nba").Value) = False Then
            'If sycmd.Parameters("p_nba").Value.ToString > 0 Then
            TextBox1.Text = sycmd.Parameters("p_crn").Value
            'g_filename = sycmd.Parameters("p_outfile").Value
            'g_cmd = sycmd.Parameters("p_cmd").Value
            g_card_bal = sycmd.Parameters("p_nba").Value.ToString
            g_card_num = sycmd.Parameters("p_crn").Value
            btnprint_rcpt.Enabled = True
            btnprint_rcpt.Visible = True
            printer_drp.Enabled = True
            printer_drp.Visible = True
            sy_printer_drp_Populate()
            'End If
        End If

    End Sub

    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

        If InStr(TextBox1.Text, "?") > 0 Then
            Dim sCC As Array
            sCC = Split(TextBox1.Text.ToString, "?")
            TextBox1.Text = Right(sCC(0).Trim, Len(sCC(0)) - 1)
            'TextBox1.Enabled = False
            'txt_swiped.Text = "Y"
            'Else
            ' txt_swiped.Text = "N"
        End If

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        TextBox1.Text = ""
        'txt_swiped.Text = "N" sabrina commented out
        'TextBox1.Enabled = True sabrina  (brick requirment for GC)
        lbl_response.Text = ""
        TextBox1.Focus()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'sabrina add
        btnprint_rcpt.Enabled = False
        btnprint_rcpt.Visible = False
        printer_drp.Enabled = False
        printer_drp.Visible = False
        TextBox1.Focus()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
    Protected Sub print_receipt(ByVal p_cmd As String)

        ' lblInfo.Text = "in testprint1 "

        Dim str_ip As String
        Dim str_user As String
        Dim str_pswd As String

        str_ip = get_database_ip()
        str_user = get_user()
        str_pswd = get_pswd()

        Dim syclient As New Renci.SshNet.SshClient(str_ip, str_user, str_pswd)
        Dim sycmd As Renci.SshNet.SshCommand

        Try
            syclient.Connect()
        Catch str_excep As Exception
            Throw
            lbl_response.Text = str_excep.ToString
        End Try

        sycmd = syclient.RunCommand(p_cmd)
        Try
        Catch syex3 As SshException
            Throw
            lbl_response.Text = syex3.ToString
        End Try

        sycmd.Dispose()
        syclient.Disconnect()

    End Sub

    Private Function get_database_ip() As String

        Dim connString As String
        Dim syConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim db_ip As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        syConnection = DisposablesManager.BuildOracleConnection(connString)
        syConnection.Open()

        Dim syCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        syCMD.Connection = syConnection
        syCMD.CommandText = "std_tenderretail_util.getSshIP"
        syCMD.CommandType = CommandType.StoredProcedure

        syCMD.Parameters.Add("p_ip", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        Try
            syCMD.ExecuteNonQuery()
            db_ip = syCMD.Parameters("p_ip").Value
        Catch x
            syConnection.Close()
        End Try

        Return db_ip

    End Function

    Private Function get_user() As String

        Dim connString As String
        Dim syConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim str_user As String = ""
        Dim p_user As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        syConnection = DisposablesManager.BuildOracleConnection(connString)
        syConnection.Open()

        Dim syCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        syCMD.Connection = syConnection
        syCMD.CommandText = "std_tenderretail_util.getSshUsr"
        syCMD.CommandType = CommandType.StoredProcedure

        syCMD.Parameters.Add("p_user", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        Try
            syCMD.ExecuteNonQuery()
            str_user = syCMD.Parameters("p_user").Value
        Catch x
            syConnection.Close()
        End Try

        Return str_user

    End Function
    Public Function get_pswd() As String

        Dim connString As String
        Dim syConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim p_pswd As String = ""
        Dim str_pswd As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        syConnection = DisposablesManager.BuildOracleConnection(connString)
        syConnection.Open()

        Dim syCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        syCMD.Connection = syConnection
        syCMD.CommandText = "std_tenderretail_util.getSshPwd"
        syCMD.CommandType = CommandType.StoredProcedure

        syCMD.Parameters.Add("p_pswd", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        Try
            syCMD.ExecuteNonQuery()
            str_pswd = syCMD.Parameters("p_pswd").Value
        Catch x
            syConnection.Close()
        End Try

        Return str_pswd

    End Function

    Protected Sub btnprint_rcpt_Click(sender As Object, e As EventArgs) Handles btnprint_rcpt.Click

        Dim v_cmd As String = ""

        'sabrina add next if
        If printer_drp.SelectedIndex = 0 Then
            g_sel_printer = Session("default_prt")
        Else
            g_sel_printer = Session("printer_drp_id")
        End If

        If IsDBNull(g_sel_printer) = True Or g_sel_printer Is Nothing Or g_sel_printer = "Printers:" Then
            lbl_response.Text = "<font color=red>Please select a printer to proceed </font>"
            Exit Sub
        End If

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        'myCMD.CommandText = "std_tenderretail_util.build_receipt"   'sabrina new
        myCMD.CommandText = "std_tenderretail_util.build_receipt"   'sabrina new

        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_emp_cd", OracleType.Char)).Value = Session("EMP_CD")
        myCMD.Parameters.Add(New OracleParameter("p_str", OracleType.Char)).Value = Session("HOME_STORE_CD")
        myCMD.Parameters.Add(New OracleParameter("p_card", OracleType.Char)).Value = g_card_num
        myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.Char)).Value = g_card_bal
        myCMD.Parameters.Add(New OracleParameter("p_printer", OracleType.Char)).Value = g_sel_printer

        myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 100).Direction = ParameterDirection.ReturnValue

        Try
            myCMD.ExecuteNonQuery()
            v_cmd = myCMD.Parameters("p_cmd").Value
        Catch x
            objConnection.Close()
            v_cmd = ""
        End Try

        If IsDBNull(v_cmd) = True Or v_cmd Is Nothing Then
            lbl_response.Text = "<font color=red> ** Error Detected when printing receipt - please try again </font>"
            Exit Sub
        Else
            print_receipt(v_cmd)
        End If

    End Sub
    Public Sub sy_printer_drp_Populate()

        'sabrina new sub added
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet
        Dim printer_found As Boolean = False
        Dim v_user_init As String = sy_get_emp_init(Session("EMP_CD"))
        Dim v_user_cd As String = Session("EMP_CD")
        Dim v_default_printer As String = sy_get_default_printer_id(v_user_cd) 'sabrina emp_cd to get default printer
        'Dim default_printer As String = ""
        If IsDBNull(v_default_printer) = True Or v_default_printer Is Nothing Or v_default_printer = "" Then
            v_default_printer = "Printers:"
        End If

        Session("default_prt") = v_default_printer

        sql = "SELECT PRINT_Q FROM misc.PRINT_QUEUE a WHERE PRINT_Q IS NOT NULL "
        sql = sql & " AND (STD_CHK_PRINTER.IS_VALID_STR_PRINTER(PRINT_Q,STD_CHK_PRINTER.GET_HOME_STR("
        sql = sql & "'" & v_user_init & "')) = 'Y'"
        sql = sql & " OR STD_CHK_PRINTER.IS_SUPR_USR('" & v_user_init & "') = 'Y') "
        sql = sql & " and exists (select 'x' from misc.console c where c.print_q = a.print_q) "
        sql = sql & " and print_q <> '" & v_default_printer & "'"
        sql = sql & " ORDER BY PRINT_Q "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        printer_drp.Items.Clear()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                printer_found = True
                printer_drp.DataSource = ds
                printer_drp.ValueField = "PRINT_Q"
                printer_drp.TextField = "PRINT_Q"
                printer_drp.DataBind()
            End If

            printer_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(v_default_printer))
            printer_drp.Items.FindByText(v_default_printer).Selected = True
            printer_drp.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception

            MyDataReader.Close()
            conn.Close()
            Throw

        End Try

    End Sub

    Private Function sy_get_default_printer_id(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select c.print_q as PRINT_Q from console c, emp e "
        sql = sql & "where e.print_grp_con_cd = c.con_cd And e.emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("PRINT_Q").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function
    Private Function sy_get_emp_init(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select emp_init as EMP_INIT from emp where emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("EMP_INIT").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function
End Class

