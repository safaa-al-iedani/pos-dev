<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Remove_Customer_Verification.aspx.vb"
    Inherits="Remove_Customer_Verification" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reselect Customer?</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
        <table>
            <tr>
                <td valign="middle" align="center">
                    <img src="images/icons/Symbol-Stop.png" />
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td valign="middle">
                    <asp:Label ID="lbl_warning" runat="server" Font-Bold="True" Font-Size="Small"></asp:Label>
                    <br />
                    <br />
                    <asp:Button ID="btn_Proceed" runat="server" Text="Yes, Reselect Customer" Width="175px" />
                    <asp:Button ID="btn_Cancel" runat="server" Text="Keep the Current Customer" Width="175px" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
