<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InventoryLookup.aspx.vb"
    Inherits="InventoryLookup" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main_body" runat="server">
        <table bgcolor="white" width="100%">
            <tr>
                <td valign="top" align="center">
                    <asp:Image ID="img_picture" runat="server" ImageUrl="~/images/image_not_found.jpg"
                        ></asp:Image><br />
                    <br />
                    &nbsp;
                    <dx:ASPxButton ID="btn_refresh" runat="server" OnClick="btn_refresh_Click" Text="<%$ Resources:LibResources, Label274 %>"></dx:ASPxButton>
                </td>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="2" align="center" valign="top">
                                <table width="90%">
                                    <tr>
                                        <td>
                                            <img src="images/left_arrow.gif" />
                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="lbl_availability" runat="server" Text="" Font-Size="Medium">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <img src="images/right_arrow.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <dx:ASPxLabel ID="lbl_vsn" runat="server" Text="" Width="100%">
                                </dx:ASPxLabel>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <table width="95%">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label550 %> ">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="lbl_sku" runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label640 %> ">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="lbl_vendor" runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label113 %>">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="txt_cost" runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <%--' Daniela hide retail price--%>
                                            <dx:ASPxLabel ID="lblRetailPrice" runat="server" Text="Retail Price: " Visible="False">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <%--' Daniela hide retail price--%>
                                            <dx:ASPxLabel ID="lblRetailPriceValue" runat="server" Text="" Visible="False">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="middle" align="center">
                                <font size="5">
                                    <dx:ASPxLabel ID="lbl_retail" runat="server" Text="" Font-Size="X-Large">
                                    </dx:ASPxLabel>
                                </font>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <dx:ASPxLabel ID="Label1" runat="server" Text="<%$ Resources:LibResources, Label654%>">
                    </dx:ASPxLabel>
                    <dx:ASPxGridView ID="GridView4" runat="server" AutoGenerateColumns="False" Width="98%">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label572 %>" VisibleIndex="0">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hpl_store_reserve" Visible="False" runat="server" Text="">
                                    </dx:ASPxHyperLink>
                                    <dx:ASPxLabel ID="lbl_store_cd" runat="server" Text="" Visible="False">
                                    </dx:ASPxLabel>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="STORE_CD" VisibleIndex="1" Visible="False">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label272 %>" FieldName="LOC_CD" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="QTY" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowSort="False" />
                        <SettingsPager Visible="False" Mode="ShowAllRecords">
                        </SettingsPager>
                        <Settings ShowGroupButtons="False" ShowHeaderFilterBlankItems="False" />
                    </dx:ASPxGridView>
                    <dx:ASPxLabel ID="lbl_total_whs" runat="server" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <br />
                    <dx:ASPxLabel ID="Label_vendor" runat="server" Text="<%$ Resources:LibResources, Label1003%>">
                    </dx:ASPxLabel>
                    <dx:ASPxGridView ID="ASPxGridView_vendor" runat="server" AutoGenerateColumns="False" Width="98%">
                        <Columns>
                           <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label1004 %>" FieldName="RDC" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label1005 %>" FieldName="AS_OF_DT" VisibleIndex="1">
                                <PropertiesTextEdit DisplayFormatString="MM/dd/yyyy"  DisplayFormatInEditMode="True" ></PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label1006 %>" FieldName="QTY" VisibleIndex="2" CellStyle-HorizontalAlign ="Left" >
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowSort="False" />
                        <SettingsPager Visible="False" Mode="ShowAllRecords">
                        </SettingsPager>
                        <Settings ShowGroupButtons="False" ShowHeaderFilterBlankItems="False" />
                    </dx:ASPxGridView>

                    <br />
                    <br />
                    <dx:ASPxLabel ID="Label2" runat="server" Text="<%$ Resources:LibResources, Label443 %>">
                    </dx:ASPxLabel>
                    <br />
                    <dx:ASPxGridView ID="GridView2" runat="server" AutoGenerateColumns="False" Width="98%">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label419 %>" VisibleIndex="0">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hpl_po_reserve" Visible="False" runat="server" Text="">
                                    </dx:ASPxHyperLink>
                                    <dx:ASPxLabel ID="lbl_po_cd" runat="server" Text="" Visible="False">
                                    </dx:ASPxLabel>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label419 %>" FieldName="PO_CD" Visible="False" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="TOTAL_INCOMING" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label159 %>" FieldName="DEST_STORE_CD" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label37 %>" FieldName="ARRIVAL_DT" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label1026 %>" FieldName="BOOKING_QTY" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label571 %>" FieldName="BOOKING_STATUS" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowSort="False" />
                        <SettingsPager Visible="False" Mode="ShowAllRecords">
                        </SettingsPager>
                    </dx:ASPxGridView>
                    <dx:ASPxLabel ID="lbl_total_po" runat="server" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <br />
                    <dx:ASPxLabel ID="Label3" runat="server" Text="<%$ Resources:LibResources, Label511 %>">
                    </dx:ASPxLabel>
                    <br />
                    <dx:ASPxGridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="98%">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="0" Caption="<%$ Resources:LibResources, Label511 %>">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hpl_del_doc" Visible="False" runat="server" Text="">
                                    </dx:ASPxHyperLink>
                                    <dx:ASPxLabel ID="lbl_del_doc" runat="server" Text="" Visible="False">
                                    </dx:ASPxLabel>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="QTY" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label568 %>" FieldName="STORE_CD" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label271 %>" FieldName="LOC_CD" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label394 %>" FieldName="PU_DEL_DT" VisibleIndex="4">
                                <PropertiesTextEdit>
                                    <MaskSettings Mask="MM/dd/yyyy" />
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label263 %>" FieldName="SHIP_TO_L_NAME" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="DEL_DOC_NUM" VisibleIndex="1"
                                Visible="False">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowSort="False" />
                        <SettingsPager Visible="False" Mode="ShowAllRecords">
                        </SettingsPager>
                    </dx:ASPxGridView>
                    <dx:ASPxLabel ID="lbl_total_so" runat="server" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <br />
                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label241 %>">
                    </dx:ASPxLabel>
                    <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" Width="98%">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label251 %>" FieldName="DOC_NUM" Visible="True" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label266 %>" FieldName="LN#" Visible="True" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="QTY" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label160 %>" FieldName="DEST_STORE_CD" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label628 %>" FieldName="TRANSFER_DT" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label94 %>" FieldName="CRPT_ID_NUM" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowSort="False" />
                        <SettingsPager Visible="False" Mode="ShowAllRecords">
                        </SettingsPager>
                    </dx:ASPxGridView>
                    <dx:ASPxLabel ID="lbl_total_ist" runat="server" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <br />
                    <dx:ASPxLabel ID="Label4" runat="server" Text="<%$ Resources:LibResources, Label574 %>">
                    </dx:ASPxLabel>
                    <dx:ASPxGridView ID="GridView3" runat="server" AutoGenerateColumns="False" Width="98%">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label572 %>" VisibleIndex="0">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hpl_store_reserve" Visible="False" runat="server" Text="">
                                    </dx:ASPxHyperLink>
                                    <dx:ASPxLabel ID="lbl_store_cd" runat="server" Text="" Visible="False">
                                    </dx:ASPxLabel>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label572 %>" FieldName="STORE_CD" VisibleIndex="1"
                                Visible="False">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label272 %>" FieldName="LOC_CD" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="QTY" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowSort="False" />
                        <SettingsPager Visible="False" Mode="ShowAllRecords">
                        </SettingsPager>
                    </dx:ASPxGridView>
                    <dx:ASPxLabel ID="lbl_total_store" runat="server" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <br />
                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label375 %>">
                    </dx:ASPxLabel>
                    <dx:ASPxGridView ID="ASPxGridView3" runat="server" AutoGenerateColumns="False" Width="98%">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label571 %>" FieldName="OUT_CD" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label374 %>" VisibleIndex="0">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hpl_outlet_reserve" Visible="False" runat="server" Text="">
                                    </dx:ASPxHyperLink>
                                    <dx:ASPxLabel ID="lbl_outlet_id" runat="server" Text="" Visible="False">
                                    </dx:ASPxLabel>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label572 %>" FieldName="STORE_CD" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label272 %>" FieldName="LOC_CD" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="QTY" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="SPIFF" VisibleIndex="3" Visible="false">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label447 %>" FieldName="COMM_CD" VisibleIndex="3" Visible="false">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowSort="False" />
                        <SettingsPager Visible="False" Mode="ShowAllRecords">
                        </SettingsPager>
                    </dx:ASPxGridView>
                    <dx:ASPxLabel ID="lbl_out" runat="server" Text="">
                    </dx:ASPxLabel>
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl7" runat="server" CloseAction="None" HeaderText="Select Serial Number"
        Height="179px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="370px" ShowCloseButton="False">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <dx:ASPxLabel ID="lbl_ser_store_cd" runat="server" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_ser_loc_cd" runat="server" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_ser_row_id" runat="server" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_ser_itm_cd" runat="server" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_ser_tp" runat="server" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxComboBox ID="ASPxGridView1" runat="server" ValueType="System.String" Width="355px"
                    IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Use Selected Serial #" OnClick="ASPxButton2_Click">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click" Text="Exit Without Selecting Serial #">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <dx:ASPxLabel ID="lbl_ser_err" runat="server" Width="100%">
                </dx:ASPxLabel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <div id="div_lookup_form" runat="server">
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btn_view" BorderStyle="None">
            <dx:ASPxLabel ID="lbl_script" runat="server" Text="Label" Visible="False"></dx:ASPxLabel><br />
            <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxTextBox ID="txt_SKU" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btn_view" runat="server" Text="<%$ Resources:LibResources, Label645 %>" OnClick="btn_view_Click">
                        </dx:ASPxButton>
                    </td>
                    <td>
                        <asp:HyperLink ID="hpl_find_merch" runat="server" NavigateUrl="order.aspx?referrer=InventoryLookup.aspx">
        <img src="images/icons/find.gif" style="width: 15px; height: 15px" border="0" alt="Merchandise Lookup" /></asp:HyperLink>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <dx:ASPxLabel ID="lbl_warnings" runat="server" Text="">
            </dx:ASPxLabel>
        </asp:Panel>
    </div>
</asp:Content>
