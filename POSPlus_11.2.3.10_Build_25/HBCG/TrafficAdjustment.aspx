﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TrafficAdjustment.aspx.vb"
    MasterPageFile="MasterPages/NoWizard2.master"  Inherits="TrafficAdjustment" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" BorderStyle="none" runat="Server">
        <div runat="server">
        <%-- SCREEN HEADING --%>
            <table  width="100%" class="style5">
                <tr>
                    <td align="center" > 
                    <asp:Label ID="ASPxLabel1" runat="server" Text="Traffic Count Adjustment Screen" Font-Bold="True" ></asp:Label>
                    </td>
                </tr>
       
            </table>
            <br />  
            <asp:Panel ID="Panel1" BorderStyle="Groove" runat="server">
             <%-- MESSAGES Width="900px" CssClass="style5" font-size="Small"   --%>          
            <table>
                  <tr>
                    <td >
                        <asp:TextBox ID="lbl_msg" BorderColor="Brown" Width="900px" forecolor="Blue" BackColor="WhiteSmoke" runat="server" AutoPostBack="true"  Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" ></asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
          <%-- STORE, DATE  --%> 
     
           <table width="100%">
           <tr>
             <td> 
                 <asp:Label ID="lbl_str" runat="server"    font-size="XX-Small" Text="Store"></asp:Label>  
                  <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" width="260px" autopostback="true" OnSelectedIndexChanged="STRSelected" ID="str_drp_list">                   
                  </asp:dropdownlist> 
               &nbsp&nbsp&nbsp&nbsp 
              <asp:Label ID="Label1" runat="server" font-size="XX-Small" Text="Traffic Date " ></asp:Label>
               <asp:TextBox ID="traffic_dt_txt" readonly="true" Width="100px" runat="server"></asp:TextBox>
                <asp:Calendar ImageUrl="~/Images/Calendar_scheduleHS.png" ID="clndr_traffic_dt" DisplayDateFormat="MM/dd/yyyy" 
                   ForeColor="Black" OnSelectionChanged="traffic_dt_changed" runat="server">
               <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
               </asp:Calendar>
               <asp:ImageButton ID="traffic_dt_sel_btn" ImageUrl="~/images/icons/Calendar16px.gif" runat="server" Text="Date" OnClick="traffic_dt_sel_btn_clicked" /> 
               </td>
            </tr>
          </table>
                <br />
                <br />
     
        <%-- BUTTONS  --%> 
           
          <table width="100%">
          <tr> 
            <td> <asp:Button ID="hrly_btn" width="170px" OnClick="hrly_btn_click" Font-Bold="true" runat="server" Text="Traffic Detail" /> </td>
            <td> <asp:Button ID="summ_btn" width="170px" onclick="summ_btn_click" Font-Bold="true" runat="server" Text="Traffic Summary" /></td>
            <td> <asp:Button ID="clear_btn" width="150px" onclick="clear_btn_click" Font-Bold="true" runat="server" Text="Clear" /></td>
          </tr> 
        </table>
                </asp:Panel>
            <br />

    <%-- ===============POPUP - edit =========HOURLY DETAIL =========================================   --%>  
    <dxpc:ASPxPopupControl ID="ASPxPopupHourly" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Hourly Traffic Adjustment" Font-Bold="true" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="40%"
        CssClass="style5">
         <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" DefaultButton="btnSubmit" runat="server">
        
            <br />
        <asp:TextBox ID="lbl_msg2" BorderColor="Brown" Width="900px"  BackColor="WhiteSmoke" forecolor="Blue" runat="server" AutoPostBack="true"  Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" ></asp:TextBox>
            <br />
            <br />
        <asp:Button ID="saveAvg_btn" width="180px" onclick="saveAvg_btn_click" font-bold="true" ForeColor="Red"  runat="server" Text="Apply Avg." />  
            <br />
            <br />
        <asp:Label ID="lbl1_store_cd" Enabled="false" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="lbl1_dt" Enabled="false" runat="server" Text="Label"></asp:Label>
        <%-- traffic hours OnRowUpdating="ASPxGridView1_RowUpdating" --%> 
        <dx:ASPxGridView ID="GridView1" runat="server" keyfieldname="HR" AutoGenerateColumns="False"
           Width="60%" ClientInstanceName="GridView1" oncelleditorinitialize="GridView1_CellEditorInitialize"   
            OnBatchUpdate="GridView1_BatchUpdate" OnRowValidating="rowvalidation"  >
        <SettingsEditing mode="Batch"/>
        
        <Columns>
             <dx:GridViewDataTextColumn Caption="HR" FieldName="HR" ReadOnly="true" Visible="True" VisibleIndex="1">
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Curr Cnt." FieldName="CURR_CNT" CellStyle-foreColor="gray" ReadOnly="true" Visible="True" VisibleIndex="2">
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="AVG Cnt." FieldName="AVG_CNT" CellStyle-foreColor="gray" readOnly="true" Visible="True" VisibleIndex="3">
                <PropertiesTextEdit  MaxLength="3">
                 </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="New / Logged Cnt." FieldName="NEW_CNT" Visible="True" VisibleIndex="4">
                <EditFormCaptionStyle >
                </EditFormCaptionStyle>
                 <PropertiesTextEdit  MaxLength="3">
                    <ValidationSettings>
                    <RegularExpression  ValidationExpression="^[0-9]{0,3}$" ErrorText="Invalid Value"  />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
                                               
     
        </Columns>
        
        <SettingsBehavior  AllowSort="False" />
        
        <SettingsPager Position="Bottom" Visible="true" Mode="ShowAllRecords"  /> 
         
        
    </dx:ASPxGridView>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>

    <%-- ===============POPUP - BROWSE =========HOURLY DETAIL Height="80px" Width="600px" =========================================   --%>  
    <dxpc:ASPxPopupControl ID="ASPxPopupHourlyBrowse" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Hourly Traffic" Font-Bold="true"  PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="60%" 
        CssClass="style5">
         <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl9" DefaultButton="btnSubmit" runat="server">
        <%-- traffic hours  --%> 
            <br />
            <br />
        <asp:Label ID="lbl2_store_cd" Enabled="false" runat="server" Text=""></asp:Label>
        <asp:Label ID="lbl2_dt" Enabled="false" runat="server" Text=""></asp:Label>
        <dx:ASPxGridView ID="GridView9" runat="server" keyfieldname="HR" AutoGenerateColumns="False" 
           Width="60%" ClientInstanceName="GridView9" >
        <Columns>
             <dx:GridViewDataTextColumn Caption="HR" FieldName="HR" CellStyle-backColor="WhiteSmoke" CellStyle-Font-Bold="true" ReadOnly="true" Visible="True" VisibleIndex="1">
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Curr Cnt." FieldName="CURR_CNT" CellStyle-backColor="WhiteSmoke" ReadOnly="true" Visible="True" VisibleIndex="2">
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="AVG Cnt." FieldName="AVG_CNT" CellStyle-backColor="WhiteSmoke" ReadOnly="true" Visible="True" VisibleIndex="3">
                <PropertiesTextEdit  MaxLength="3">
                 </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Curr. Adj." FieldName="NEW_CNT" CellStyle-backColor="WhiteSmoke" ReadOnly="true" Visible="True" VisibleIndex="4">
                <PropertiesTextEdit  MaxLength="3">
                    
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior  AllowSort="False" />
        <SettingsPager Position="Bottom" Visible="true" Mode="ShowAllRecords"  /> 
         
        
    </dx:ASPxGridView>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
  
  <%-- ===============POPUP===========Summary ========================================= --%>  
  <dxpc:ASPxPopupControl ID="ASPxPopupSummary" runat="server" AllowDragging="True" 
        CloseAction="CloseButton"  HeaderText="Traffic Summary Adjustment" Font-Bold="true" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="60%"  
        CssClass="style5">
         <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" DefaultButton="btnSubmit" runat="server">
<br />
            <br />
             <asp:TextBox ID="lbl3_msg" BorderColor="Brown" BackColor="WhiteSmoke" forecolor="Blue" runat="server" AutoPostBack="true"  Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" ></asp:TextBox>
         <br />
            <br />
            <table>
             <tr>
                 <td><asp:Label ID="Label4" Enabled="false" runat="server" Text="Store:"></asp:Label> </td>
                 <td><asp:TextBox ID="summ_str" ReadOnly="true" runat="server"></asp:TextBox></td>
                 <td><asp:Label ID="Label41" Enabled="false" runat="server" Text="Date:"></asp:Label> </td>
                 <td><asp:TextBox ID="summ_dt" ReadOnly="true" runat="server"></asp:TextBox></td>
             </tr>
         </table>
        <asp:Panel ID="Panel2" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
            <br />
            <br />
            <br />
            <table>
              <tr>
               <td> <asp:Label ID="Label2" runat="server" Text="Current Total Traffic"></asp:Label> </td>
               <td> <asp:TextBox ID="summ_curr_count" readonly="true" runat="server"></asp:TextBox> </td>
              </tr>
              <tr>
              <td> <asp:Label ID="Label3" runat="server" Text="New Total Traffic"></asp:Label> </td>
              <td> <asp:TextBox ID="summ_new_count" runat="server"></asp:TextBox></td>
              </tr>
            </table>
            <br />
            <br />
           
            <asp:Button ID="summ_upd_btn" runat="server" Text="UPDATE" />
            <asp:Button ID="summ_clear_btn" runat="server" Text="CLEAR" /> <br />
            <br />
            <br />         

     </asp:Panel>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>

   </div>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
            
    
