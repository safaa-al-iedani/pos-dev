Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Relationship_Copy
    Inherits POSBasePage

    Private theCustomerBiz As CustomerBiz = New CustomerBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lbl_rel_no.Text = Request("del_doc_num")
            txt_cust_cd.Text = Request("cust_cd")
            PopulateCustomer()
            PopulateStores()
        End If

    End Sub

    Protected Sub txt_cust_cd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cust_cd.TextChanged

        PopulateCustomer()

    End Sub

    Public Sub PopulateCustomer()

        Dim ccCustomer As CustomerDtc = theCustomerBiz.GetCustomer(txt_cust_cd.Text)
        If (ccCustomer.custCd.isNotEmpty) Then
            lbl_cust_cd.Text = ccCustomer.fname & " " & ccCustomer.lname
            btn_copy.Enabled = True
        Else
            lbl_cust_cd.Text = "Customer Not Found"
            btn_copy.Enabled = False
        End If

    End Sub

    Public Sub PopulateStores()

        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)
            With store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

        store_cd.SelectedValue = Session("store_cd")

    End Sub

    Protected Sub btn_copy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_copy.Click

        Dim REL_NO As String = ""
        REL_NO = Create_Relationship_No(store_cd.SelectedValue)

        txt_status.Text = "Relationship " & REL_NO & " was created....Success!" & vbCrLf
        lbl_new_rel_no.Text = REL_NO
        btn_open_new.Enabled = True

        Dim sql As String
        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim Change_Customer As Boolean = False
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        conn2.Open()

        sql = "INSERT INTO RELATIONSHIP ("
        sql = sql & "REL_NO, EMP_CD, STORE_CD, SLSP1, WR_DT, P_D, TAX_CHG, FOLLOW_UP_DT, ZONE_CD, CUST_CD, DEL_CHG, "
        sql = sql & "CONVERSION_DT, DISCOUNTS, PROSPECT_TOTAL, TAX_CD, TAX_RATE, APPT_TIME, APPT_TYPE, COMMENTS, "
        sql = sql & "FNAME, LNAME, HPHONE, BPHONE, ADDR1, ADDR2, CITY, ST, ZIP, EMAIL_ADDR, REL_STATUS, REL_STATUS_DT, "
        sql = sql & "CORP_NAME, DISC_CD) "

        sql = sql & "SELECT :NEW_DEL_DOC_NUM, :EMP_CD, :STORE_CD, :EMP_CD, :WR_DT, P_D, TAX_CHG, :WR_DT, ZONE_CD, CUST_CD, DEL_CHG, "
        sql = sql & "NULL, DISCOUNTS, PROSPECT_TOTAL, TAX_CD, TAX_RATE, APPT_TIME, APPT_TYPE, COMMENTS, "
        sql = sql & "FNAME, LNAME, HPHONE, BPHONE, ADDR1, ADDR2, CITY, ST, ZIP, EMAIL_ADDR, 'O', :WR_DT, "
        sql = sql & "CORP_NAME, DISC_CD "
        sql = sql & "FROM RELATIONSHIP WHERE REL_NO=:REL_NO"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
        objsql.Parameters.Add(":REL_NO", OracleType.VarChar)
        objsql.Parameters.Add(":STORE_CD", OracleType.VarChar)
        objsql.Parameters.Add(":EMP_CD", OracleType.VarChar)
        objsql.Parameters.Add(":WR_DT", OracleType.DateTime)

        objsql.Parameters(":NEW_DEL_DOC_NUM").Value = REL_NO
        objsql.Parameters(":REL_NO").Value = lbl_rel_no.Text
        objsql.Parameters(":STORE_CD").Value = store_cd.SelectedValue
        objsql.Parameters(":EMP_CD").Value = Session("emp_cd")
        objsql.Parameters(":WR_DT").Value = DateTime.Now

        objSql.ExecuteNonQuery()

        txt_status.Text = txt_status.Text & "Relationship data copied....Success!" & vbCrLf

        sql = "SELECT CUST_CD FROM RELATIONSHIP WHERE REL_NO='" & REL_NO & "'"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            If (MyDataReader.Read()) Then
                If MyDataReader.Item("CUST_CD").ToString <> txt_cust_cd.Text Then
                    Change_Customer = True
                    txt_status.Text = txt_status.Text & "Different customer code found....Success!" & vbCrLf
                End If
            End If
        Catch
            conn.Close()
            conn2.Close()
            Throw
        End Try

        If Change_Customer = True Then
            sql = "SELECT * FROM CUST WHERE CUST_CD='" & txt_cust_cd.Text & "'"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                If (MyDataReader.Read()) Then
                    objsql.Parameters.Clear()

                    sql = "UPDATE RELATIONSHIP SET CUST_CD=:CUST_CD, FNAME=:FNAME, LNAME=:LNAME, ADDR1=:ADDR1, ADDR2=:ADDR2, "
                    sql = sql & "CITY=:CITY, ST=:ST, ZIP=:ZIP, EMAIL_ADDR=:EMAIL_ADDR, CORP_NAME=:CORP_NAME "
                    sql = sql & "WHERE REL_NO=:REL_NO"

                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                    objsql.Parameters.Add(":REL_NO", OracleType.VarChar)
                    objsql.Parameters.Add(":CUST_CD", OracleType.VarChar)
                    objsql.Parameters.Add(":FNAME", OracleType.VarChar)
                    objsql.Parameters.Add(":LNAME", OracleType.VarChar)
                    objsql.Parameters.Add(":ADDR1", OracleType.VarChar)
                    objsql.Parameters.Add(":ADDR2", OracleType.VarChar)
                    objsql.Parameters.Add(":CITY", OracleType.VarChar)
                    objsql.Parameters.Add(":ST", OracleType.VarChar)
                    objsql.Parameters.Add(":ZIP", OracleType.VarChar)
                    objsql.Parameters.Add(":EMAIL_ADDR", OracleType.VarChar)
                    objsql.Parameters.Add(":CORP_NAME", OracleType.VarChar)

                    objsql.Parameters(":REL_NO").Value = REL_NO
                    objsql.Parameters(":CUST_CD").Value = MyDataReader.Item("CUST_CD").ToString
                    objsql.Parameters(":FNAME").Value = MyDataReader.Item("FNAME").ToString
                    objsql.Parameters(":LNAME").Value = MyDataReader.Item("LNAME").ToString
                    objsql.Parameters(":ADDR1").Value = MyDataReader.Item("ADDR1").ToString
                    objsql.Parameters(":ADDR2").Value = MyDataReader.Item("ADDR2").ToString
                    objsql.Parameters(":CITY").Value = MyDataReader.Item("CITY").ToString
                    objsql.Parameters(":ST").Value = MyDataReader.Item("ST_CD").ToString
                    objsql.Parameters(":ZIP").Value = MyDataReader.Item("ZIP_CD").ToString
                    objsql.Parameters(":EMAIL_ADDR").Value = MyDataReader.Item("EMAIL_ADDR").ToString
                    objsql.Parameters(":CORP_NAME").Value = MyDataReader.Item("CORP_NAME").ToString

                    objsql.ExecuteNonQuery()
                    txt_status.Text = txt_status.Text & "Customer data updating....Success!" & vbCrLf
                End If
            Catch
                conn.Close()
                conn2.Close()
                Throw
            End Try
        End If

        objsql.Parameters.Clear()
        conn2.Close()

        sql = "INSERT INTO RELATIONSHIP_LINES ("
        sql = sql & "REL_NO, LINE, ITM_CD, QTY, RET_PRC, TREATED, VE_CD, VSN, DES, ITM_TP_CD, TREATABLE, warr_itm_link, warr_row_link) "

        sql = sql & "SELECT :NEW_DEL_DOC_NUM, LINE, ITM_CD, QTY, RET_PRC, TREATED, VE_CD, VSN, DES, ITM_TP_CD, TREATABLE, warr_itm_link, warr_row_link "
        sql = sql & "FROM RELATIONSHIP_LINES WHERE REL_NO=:REL_NO"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        objsql.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
        objsql.Parameters.Add(":REL_NO", OracleType.VarChar)

        objsql.Parameters(":NEW_DEL_DOC_NUM").Value = REL_NO
        objsql.Parameters(":REL_NO").Value = lbl_rel_no.Text

        objsql.ExecuteNonQuery()
        txt_status.Text = txt_status.Text & "Relationship line data copied....Success!" & vbCrLf
        objsql.Parameters.Clear()

        If chk_line_comments.Checked = True Then
            sql = "INSERT INTO RELATIONSHIP_LINES_DESC ("
            sql = sql & "REL_NO, LINE, DESCRIPTION) "

            sql = sql & "SELECT :NEW_DEL_DOC_NUM, LINE, DESCRIPTION "
            sql = sql & "FROM RELATIONSHIP_LINES_DESC WHERE REL_NO=:REL_NO"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
            objsql.Parameters.Add(":REL_NO", OracleType.VarChar)

            objsql.Parameters(":NEW_DEL_DOC_NUM").Value = REL_NO
            objsql.Parameters(":REL_NO").Value = lbl_rel_no.Text

            objsql.ExecuteNonQuery()
            txt_status.Text = txt_status.Text & "Relationship line description data copied....Success!" & vbCrLf
            objsql.Parameters.Clear()
        End If

        If chk_crm_comments.Checked = True Then
            sql = "INSERT INTO RELATIONSHIP_COMMENTS ("
            sql = sql & "REL_NO, SUBMIT_DT, FOLLOW_UP_DT, COMMENTS) "

            sql = sql & "SELECT :NEW_DEL_DOC_NUM, SUBMIT_DT, FOLLOW_UP_DT, COMMENTS "
            sql = sql & "FROM RELATIONSHIP_COMMENTS WHERE REL_NO=:REL_NO"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql.Parameters.Add(":NEW_DEL_DOC_NUM", OracleType.VarChar)
            objsql.Parameters.Add(":REL_NO", OracleType.VarChar)

            objsql.Parameters(":NEW_DEL_DOC_NUM").Value = REL_NO
            objsql.Parameters(":REL_NO").Value = lbl_rel_no.Text

            objsql.ExecuteNonQuery()
            txt_status.Text = txt_status.Text & "Relationship comment data copied....Success!" & vbCrLf

            objsql.Parameters.Clear()
        Else
            objsql.Parameters.Clear()

            sql = "UPDATE RELATIONSHIP SET COMMENTS=NULL "
            sql = sql & "WHERE REL_NO=:REL_NO"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":REL_NO", OracleType.VarChar)
            objsql.Parameters(":REL_NO").Value = REL_NO

            objsql.ExecuteNonQuery()
        End If

        conn.Close()

    End Sub

    'Fires in response to the user clicking the 'search button' to look for a new customer
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup2();")
        Response.Write("parentWindow.location.href='customer.aspx?referrer=Relationship_Maintenance.aspx&cust_returned=Y&query_returned=Y&del_doc_num=" & lbl_rel_no.Text & "';")
        Response.Write("</script>")

    End Sub

    Protected Sub btn_open_new_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_open_new.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup2();")
        Response.Write("parentWindow.location.href='Relationship_Maintenance.aspx?del_doc_num=" & lbl_new_rel_no.Text & "&query_returned=Y';")
        Response.Write("</script>")

    End Sub
End Class
