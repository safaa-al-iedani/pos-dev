<%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
<%--<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="LCOMCommissionLookup.aspx.vb" Inherits="LCOMCommissionLookup" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>--%>
<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="LCOMCommissionLookup.aspx.vb" Inherits="LCOMCommissionLookup" meta:resourcekey="PageResource1" 
    UICulture="auto"  %>
<%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<%--    <br /> --%>
    <table border="0" style="position: relative; width: 1000px; left: 0px; top: 0px;">
        <tr>
            <td class="class_tdlbl" colspan="7" align="center">
                <dx:ASPxLabel ID="lbl_title_commission_lookup" runat="server" meta:resourcekey="lbl_title_commission_lookup" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_store_cd" runat="server" meta:resourcekey="lbl_store_cd" Visible="False">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdddlb" colspan="3">
                     <asp:DropDownList ID="txt_store_cd" runat="server" Style="position: relative" AppendDataBoundItems="true" Visible="False" AutoPostBack="True">
                     </asp:DropDownList>
            </td>
            <td class="class_tdddlb" colspan="3">
            &nbsp;
            </td>
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <%--Code Modified  by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:ASPxLabel ID="lbl_salesperson_code" runat="server" meta:resourcekey="lbl_salesperson_code">--%>
                <dx:ASPxLabel ID="lbl_salesperson_code" runat="server" meta:resourcekey="lbl_salesperson_code" Width="170px">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:DropDownList ID="txt_salesperson_code" runat="server" Style="position: relative" AppendDataBoundItems="false" >
                </asp:DropDownList>
            </td>
            <td class="class_tdtxt" colspan="4">
            &nbsp;
            </td>
        </tr>


        <tr>
            <%--Code Modified  by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%--<td class="class_tdlbl" style="width: 150px" align="right">- --%>
            <td class="class_tdlbl"  align="right">
            <%--Code Modified  by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
<%--                <dx:ASPxLabel ID="lbl_salesperson_code" runat="server" meta:resourcekey="lbl_salesperson_code">
                </dx:ASPxLabel>
--%>
                <dx:ASPxLabel ID="lbl_del_doc_num" runat="server" meta:resourcekey="lbl_del_doc_num">
                </dx:ASPxLabel>
            </td>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%--<td class="class_tdtxt" style="width: 180px"> --%>
            <td class="class_tdtxt" style="width: 130px">
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
<%--               <asp:TextBox ID="txt_salesperson_code" runat="server" Style="position: relative" Width="120px" MaxLength="9"></asp:TextBox>
--%>
<%--                <asp:DropDownList ID="txt_salesperson_code" runat="server" Style="position: relative" AppendDataBoundItems="false" >
                </asp:DropDownList>
--%>
                <asp:TextBox ID="txt_del_doc_num" runat="server" Style="position: relative" Width="120px" MaxLength="15"></asp:TextBox>
            </td>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%--<td class="class_tdlbl" style="width: 100px" align="right"> --%>
            <td class="class_tdlbl" style="width: 150px" align="right">
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                <dx:ASPxLabel ID="lbl_written_date" runat="server" meta:resourcekey="lbl_written_date">
                </dx:ASPxLabel>
            </td>
             <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%--<td class="class_tdtxt" style="width: 180px">
                <dx:ASPxDateEdit ID="dateEdit_written_date_start" runat="server" Width="170" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" 
                    AutoPostBack="True" NullValue="01-01-0001" NullText="" ShowNullText="True">
                </dx:ASPxDateEdit>
            </td> --%>
            <td class="class_tdtxt">
                <dx:ASPxDateEdit ID="dateEdit_written_date_start" runat="server" Width="140" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" 
                    AutoPostBack="True"   NullText="" >
                </dx:ASPxDateEdit>
            </td>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
            <td class="class_tdtxt" style="width: 50px" align="center">
                <dx:ASPxLabel ID="lbl_TO_written" runat="server" meta:resourcekey="lbl_TO">
                </dx:ASPxLabel>
            </td>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
           <%-- <td class="class_tdtxt" style="width: 180px">
                <dx:ASPxDateEdit ID="dateEdit_written_date_end" runat="server" Width="170" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" 
                    AutoPostBack="True">
                </dx:ASPxDateEdit>
            </td>
            <td class="class_tdlbl" style="width: 160px">
               &nbsp;
            </td> --%>
            <td class="class_tdtxt">
                <dx:ASPxDateEdit ID="dateEdit_written_date_end" runat="server" Width="140" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" 
                    AutoPostBack="True">
                </dx:ASPxDateEdit>
            </td>
            
            <td class="class_tdlbl" style="width: 200px">
               &nbsp;
            </td>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
        </tr>

        <tr>
            <td class="class_tdlbl" colspan="1" align="center">
<%--            <dx:ASPxLabel ID="lbl_OR_deldoc" runat="server" meta:resourcekey="lbl_OR" ForeColor="DarkRed" Font-Bold="true">
                </dx:ASPxLabel>
--%>
                &nbsp;
            </td>
            <td class="class_tdlbl" colspan="1" align="center">
                &nbsp;
            </td>
            <td class="class_tdlbl" colspan="1" align="center">
                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <%--Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015 --%>
                <dx:ASPxLabel ID="lbl_OR_date" runat="server" meta:resourcekey="lbl_OR" ForeColor="DarkRed" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdlbl" colspan="4" align="center">
                &nbsp;
            </td>
        </tr>

        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
<%--                <dx:ASPxLabel ID="lbl_del_doc_num" runat="server" meta:resourcekey="lbl_del_doc_num">
                </dx:ASPxLabel>
--%>
                &nbsp;
            </td>
            <td class="class_tdtxt" colspan="1">
<%--                <asp:TextBox ID="txt_del_doc_num" runat="server" Style="position: relative" Width="120px" MaxLength="15"></asp:TextBox>
--%>
                &nbsp;
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_finalized_date" runat="server" meta:resourcekey="lbl_finalized_date">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:ASPxDateEdit ID="dateEdit_finalized_date_start" runat="server" Width="170" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"  --%>
                <dx:ASPxDateEdit ID="dateEdit_finalized_date_start" runat="server" Width="140" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" 
                    AutoPostBack="True">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                </dx:ASPxDateEdit>
            </td>
            <td class="class_tdtxt" colspan="1" align="center">
                <dx:ASPxLabel ID="lbl_TO_finalized" runat="server" meta:resourcekey="lbl_TO">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:ASPxDateEdit ID="dateEdit_finalized_date_end" runat="server" Width="170" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" 
                    AutoPostBack="True"> --%>
                <dx:ASPxDateEdit ID="dateEdit_finalized_date_end" runat="server" Width="140" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" 
                    AutoPostBack="True">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                </dx:ASPxDateEdit>
            </td>
            <td class="class_tdbtn" colspan="1" align="right">
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:ASPxButton ID="btn_Search" runat="server" meta:resourcekey="lbl_btn_search">
                </dx:ASPxButton>
                &nbsp;
                <dx:ASPxButton ID="btn_Clear" runat="server" meta:resourcekey="lbl_btn_clear">
                </dx:ASPxButton> --%>
                <dx:ASPxButton ID="btn_Search" runat="server" meta:resourcekey="lbl_btn_search" Width="90px">
                </dx:ASPxButton>
                &nbsp;
                <dx:ASPxButton ID="btn_Clear" runat="server" meta:resourcekey="lbl_btn_clear" Width="90px">
                </dx:ASPxButton>
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
            </td>
        </tr>

<%--
            <td class="class_tdddlb" colspan="3">
                <asp:DropDownList ID="txt_store_cd" runat="server" Style="position: relative" AppendDataBoundItems="true">
                </asp:DropDownList>
            </td>
--%>
   
        <tr><td colspan="7">
        <hr />
        </td></tr>

        <tr>
            <td colspan="7" align="center">
                <dx:ASPxLabel ID="lbl_resultsInfo" runat="server" Text="" Visible="false" Font-Size="Small" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>

    </table>


    <div runat="server" id="div_GridViews">

    <dx:ASPxGridView ID="GridViewCommissionSummary" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="DEL_DOC_NUM"
        ClientInstanceName="GridViewCommissionSummary" OnDataBinding="GridViewCommissionSummary_DataBinding">
<%-- --%>       
<%-- --%>
<%-- --%>
        <SettingsPager Position="TopAndBottom" >
        <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
        </SettingsPager>
<%-- --%>
        <Columns>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%--<dx:GridViewDataTextColumn Caption="Document #" FieldName="DEL_DOC_NUM" VisibleIndex="1" >
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Status" FieldName="STAT_CD" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Finalized Date" FieldName="FINALIZED_DATE" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Order Type" FieldName="ORD_TP_CD" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Sales $" FieldName="SALES$" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Commission $" FieldName="COMMISSION$" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SPIFF $" FieldName="SPIFF$" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Total Earnings" FieldName="TOTAL$" VisibleIndex="8">
            </dx:GridViewDataTextColumn> --%>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_document"  FieldName="DEL_DOC_NUM" VisibleIndex="1" >
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_status"  FieldName="STAT_CD" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_finalized_date"  FieldName="FINALIZED_DATE" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_order_type"  FieldName="ORD_TP_CD" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_sales"  FieldName="SALES$" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_commission"  FieldName="COMMISSION$" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_spiff"  FieldName="SPIFF$" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_total_earnings"  FieldName="TOTAL$" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
<%--
            <dx:GridViewDataHyperLinkColumn Caption="JAMTEST" FieldName="JAMTEST"  VisibleIndex="9" PropertiesHyperLinkEdit-NavigateUrlFormatString="LCOMCommissionDetails.aspx?DEL_DOC_NUM=test">
            </dx:GridViewDataHyperLinkColumn>
--%>
            <dx:GridViewDataTextColumn VisibleIndex="15" Settings-AllowSort="True" Width="50px">
            <Settings AllowSort="True"></Settings>
                    <DataItemTemplate>
                        <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                            <Items>
                                <dx:MenuItem Text="" NavigateUrl="LCOMCommissionDetails.aspx?DEL_DOC_NUM=test" Image-IconID="zoom_zoom_16x16">
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

        </Columns>
        <Settings ShowFooter="True" />
       <TotalSummary >
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%-- <dx:ASPxSummaryItem FieldName="SALES$" ShowInColumn="Sales $" SummaryType="Sum" DisplayFormat="{0:n2}" />
            <dx:ASPxSummaryItem FieldName="COMMISSION$" ShowInColumn="Commission $" SummaryType="Sum" DisplayFormat="{0:n2}" />
            <dx:ASPxSummaryItem FieldName="SPIFF$" ShowInColumn="SPIFF $" SummaryType="Sum" DisplayFormat="{0:n2}" />
            <dx:ASPxSummaryItem FieldName="TOTAL$" ShowInColumn="Total Earnings" SummaryType="Sum" DisplayFormat="{0:n2}" /> --%>
            <dx:ASPxSummaryItem FieldName="SALES$"   SummaryType="Sum" DisplayFormat="{0:n2}" />
            <dx:ASPxSummaryItem FieldName="COMMISSION$"  SummaryType="Sum" DisplayFormat="{0:n2}" />
            <dx:ASPxSummaryItem FieldName="SPIFF$"  SummaryType="Sum" DisplayFormat="{0:n2}" />
            <dx:ASPxSummaryItem FieldName="TOTAL$"  SummaryType="Sum" DisplayFormat="{0:n2}" />
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
        </TotalSummary>

        
        
        <SettingsBehavior AllowSort="False" />
    </dx:ASPxGridView>

    </div>

    <table border="0" style="position: relative; width: 1000px; left: 0px; top: 0px;">
        <tr>
            <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_footer_step_commission" runat="server" meta:resourcekey="lbl_footer_step_commission" Visible="true" Font-Size="Small" Font-Bold="True">
            </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="center">
            <dx:ASPxLabel ID="lbl_footer_disclaimer" runat="server" meta:resourcekey="lbl_footer_disclaimer" Visible="true" Font-Size="Small" Font-Bold="True">
            </dx:ASPxLabel>
            </td>
        </tr>
    </table>


    <br />
    <br />
    
    <div runat="server" id="div_version">
    <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
    </dx:ASPxLabel>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
