Imports System.Data.OracleClient
Imports System.IO
Imports HBCG_Utils

Partial Class admin_rights
    Inherits POSBasePage

    Public Sub bindgrid()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As oracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataadapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds = New DataSet
        Gridview1.DataSource = ""
        'CustTable.Visible = False

        Gridview1.Visible = True
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        sql = "SELECT * FROM USER_ACCESS order by EMP_CD"


        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "<br /><br />We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator.<br /><br /><br />"
            cbo_new_users.Visible = False
            btn_add_user.Visible = False
            Exit Sub
        Else
            cbo_new_users.Visible = True
            btn_add_user.Visible = True
        End If
        If Not IsPostBack Then
            Populate_New_Adds()
            bindgrid()
        End If

    End Sub

    Public Sub Populate_New_Adds()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet

        'conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT EMP_CD, LNAME || ', ' || FNAME AS FULL_DESC FROM EMP WHERE TERMDATE IS NULL ORDER BY LNAME, FNAME"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        With cbo_new_users
            .DataSource = ds
            .DataValueField = "EMP_CD"
            .DataTextField = "FULL_DESC"
            .DataBind()
        End With
        conn.Close()

    End Sub

    Public Function PopulateAdminList() As DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT admin_page FROM USER_ACCESS group by admin_page"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        conn.Close()

        Return ds

    End Function

    Public Function PopulateCCInfoList() As DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataadapter

        ds = New DataSet

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT admin_page FROM USER_ACCESS group by admin_page"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        conn.Close()
        Return ds

    End Function

    Public Function PopulateModHlp() As DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataadapter

        ds = New DataSet

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT admin_page FROM USER_ACCESS group by admin_page"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        conn.Close()
        Return ds

    End Function

    Public Function PopulateChgRt() As DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        ds = New DataSet

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT admin_page FROM USER_ACCESS group by admin_page"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        conn.Close()
        Return ds

    End Function

    Public Function PopulateUpPics() As DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        ds = New DataSet

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT admin_page FROM USER_ACCESS group by admin_page"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        conn.Close()
        Return ds

    End Function

    Protected Sub DoItemSave(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim AdminList As DropDownList
        Dim CCMnt As DropDownList
        Dim ModHlp As DropDownList
        Dim ChgRts As DropDownList
        Dim UpPics As DropDownList
        Dim TrnLeads As DropDownList
        Dim ModInv As DropDownList
        Dim AdminValue As String
        Dim CCMntValue As String
        Dim ModHlpValue As String
        Dim ChgRtsValue As String
        Dim UpPicsValue As String
        Dim TrnLeadsValue As String
        Dim ModInvValue As String
        Dim drppaymentsetup As DropDownList
        Dim drp_terminalsetup As DropDownList
        Dim drpterminalstatus As DropDownList
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdUpdate As OracleCommand = DisposablesManager.BuildOracleCommand



        AdminList = e.Item.FindControl("drpAdmin")
        CCMnt = e.Item.FindControl("drpCCMnt")
        ModHlp = e.Item.FindControl("drpModHlp")
        ChgRts = e.Item.FindControl("drpChgRts")
        UpPics = e.Item.FindControl("drpUpPics")
        TrnLeads = e.Item.FindControl("drpTrfLds")
        ModInv = e.Item.FindControl("drpModInv")
        drppaymentsetup = e.Item.FindControl("drppaymentsetup")
        drp_terminalsetup = e.Item.FindControl("drp_terminalsetup")
        drpterminalstatus = e.Item.FindControl("drpterminalstatus")
        AdminValue = AdminList.SelectedItem.Value
        CCMntValue = CCMnt.SelectedItem.Value
        ModHlpValue = ModHlp.SelectedItem.Value
        ChgRtsValue = ChgRts.SelectedItem.Value
        UpPicsValue = UpPics.SelectedItem.Value
        TrnLeadsValue = TrnLeads.SelectedItem.Value
        ModInvValue = ModInv.SelectedItem.Value
        ' Update TempValue to DB

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        With cmdUpdate
            .Connection = conn
            .CommandText = "UPDATE user_access SET admin_page= :ADMIN, PAYMENT_XREF = :PAYMENT, TERMINAL_SETUP = :TERMINAL, TERMINAL_STATUS = :TERMINAL_STATUS, credit_card_info = :CCVALUE,help_files = :HELP,change_rights = :CHGRTS,upload_pics = :UPPICS,transfer_leads = :TRNLEADS,invoice_admin =:MODINV  WHERE emp_cd =:EMP_CD"
            .Parameters.Add(":ADMIN", OracleType.VarChar)
            .Parameters(":ADMIN").Value = AdminValue
            .Parameters.Add(":PAYMENT", OracleType.VarChar)
            .Parameters(":PAYMENT").Value = drppaymentsetup.SelectedValue
            .Parameters.Add(":TERMINAL", OracleType.VarChar)
            .Parameters(":TERMINAL").Value = drp_terminalsetup.SelectedValue
            .Parameters.Add(":TERMINAL_STATUS", OracleType.VarChar)
            .Parameters(":TERMINAL_STATUS").Value = drpterminalstatus.SelectedValue
            .Parameters.Add(":CCVALUE", OracleType.VarChar)
            .Parameters(":CCVALUE").Value = CCMntValue
            .Parameters.Add(":HELP", OracleType.VarChar)
            .Parameters(":HELP").Value = ModHlpValue
            .Parameters.Add(":CHGRTS", OracleType.VarChar)
            .Parameters(":CHGRTS").Value = ChgRtsValue
            .Parameters.Add(":UPPICS", OracleType.VarChar)
            .Parameters(":UPPICS").Value = UpPicsValue
            .Parameters.Add(":TRNLEADS", OracleType.VarChar)
            .Parameters(":TRNLEADS").Value = TrnLeadsValue
            .Parameters.Add(":MODINV", OracleType.VarChar)
            .Parameters(":MODINV").Value = ModInvValue
            .Parameters.Add(":EMP_CD", OracleType.VarChar)
            .Parameters(":EMP_CD").Value = e.Item.Cells(0).Text



        End With
        cmdUpdate.ExecuteNonQuery()

        cmdUpdate.Dispose()
        conn.Close()

    End Sub

    Protected Sub btn_add_user_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add_user.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDatareader As OracleDataReader
        Dim Proceed As Boolean = False

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        objsql2.CommandText = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD=:EMP_CD"
        objsql2.Connection = conn
        objsql2.Parameters.Add(":EMP_CD", OracleType.VarChar)
        objsql2.Parameters(":EMP_CD").Value = cbo_new_users.SelectedValue

        Try

            MyDatareader = DisposablesManager.BuildOracleDataReader(objsql2)

            If (MyDatareader.Read()) Then
                'if the emp code is populated, it implies user exists, do not add again
                If Not String.IsNullOrEmpty(MyDatareader.Item("EMP_CD").ToString) Then
                    Proceed = False
                End If
            Else
                Proceed = True
            End If
            MyDatareader.Close()
            If Proceed = True Then
                objsql2 = DisposablesManager.BuildOracleCommand

                objsql2.CommandText = "INSERT INTO USER_ACCESS (EMP_CD) VALUES(:EMP_CD)"
                objsql2.Connection = conn
                objsql2.Parameters.Add(":EMP_CD", OracleType.VarChar)
                objsql2.Parameters(":EMP_CD").Value = cbo_new_users.SelectedValue
                objsql2.ExecuteNonQuery()
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Proceed = False Then
            lbl_errors.Text = "User already exists"
        Else
            lbl_errors.Text = "User " & cbo_new_users.SelectedValue & " was successfully added."
        End If
        bindgrid()
        conn.Close()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
