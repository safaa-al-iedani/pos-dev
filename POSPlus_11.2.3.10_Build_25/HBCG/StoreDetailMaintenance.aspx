﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="StoreDetailMaintenance.aspx.vb" Inherits="StoreDetailMaintenance"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
    

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
        function OnInit(s, e) {
            document.getElementById("tabFrame").src = s.GetActiveTab().GetNavigateUrl();
            alert("I want here the id of expanded row of grid");

        }
    </script>

    <table>
              <tr>
            <td class="class_tdlbl" colspan="1" align="left">
                <dx:ASPxLabel ID="lbl_title_commission_details" runat="server" Text="Store Detail Maintenance" Font-Bold="true">
                </dx:ASPxLabel>
                 <br />
                <br />
            </td>
                 
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                            <td style="width: 109px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Store Code">
                </dx:ASPxLabel>
            </td>
            <td style="width: 338px">
                     <asp:DropDownList ID="txt_store_cd" runat="server" Style="position: relative" AppendDataBoundItems="true">
                     </asp:DropDownList>
              <%-- <asp:TextBox ID="txt_store_cd" runat="server" Style="position: relative" Width="79px"></asp:TextBox>&nbsp; --%>
            </td>
       
          <%--    <td style="width: 338px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Store Code">
                </dx:ASPxLabel>
            </td>  --%>
                    <%--      </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_store_cd" runat="server" Width="150PX" MaxLength="3"></dx:ASPxTextBox>
                        </td> --%>
                        <td>
                            <dx:ASPxButton ID="btn_search" runat="server" Text="Search"></dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="GV_store_detail" runat="server" AutoGenerateColumns="False" KeyFieldName="ROWID" EnableRowsCache="true"
                                OnRowUpdating="GV_row_update" OnRowInserting="GV_row_insert" OnRowDeleting="GV_row_delete" OnDataBinding="GV_Store_Detail_DataBind"
                                OnCellEditorInitialize="GVCellEditorInitialize" Settings-ShowFooter="true" Enabled="true" 
                                SettingsBehavior-ColumnResizeMode="Control" Width="7000px" >                                       
                                <Styles>
                                    <Cell Wrap="False"></Cell>
                                </Styles>                   
                                <Settings ShowFooter="true" />
                                <SettingsBehavior ConfirmDelete="true" />
                                <SettingsPager Position="Bottom">
                                    <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                                </SettingsPager>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowEditButton="true" ShowNewButtonInHeader="true" ShowDeleteButton="true"></dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="" FieldName="ROWID" Visible="False" ReadOnly="True" VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="store_cd" FieldName="STORE_CD" Visible="True" ReadOnly="False" VisibleIndex="1" PropertiesTextEdit-MaxLength="2" Width="70">
                                          <PropertiesTextEdit MaxLength="2">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Store Code"/>
                                        </ValidationSettings>
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="store_name" FieldName="STORE_NAME" Visible="True" VisibleIndex="2" PropertiesTextEdit-MaxLength="60"  Width="260">
                                       <PropertiesTextEdit MaxLength="60">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Store Name"/>
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="address_1" FieldName="ADDRESS_1" Visible="True" VisibleIndex="3" PropertiesTextEdit-MaxLength="60"  Width="170">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="address_2" FieldName="ADDRESS_2" Visible="True" VisibleIndex="4" PropertiesTextEdit-MaxLength="60">
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn meta:resourcekey="CITY" FieldName="CITY" Visible="True" VisibleIndex="5" PropertiesTextEdit-MaxLength="20">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="PROV" FieldName="PROV" Visible="True" VisibleIndex="6" PropertiesTextEdit-MaxLength="2" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="PC" FieldName="PC" Visible="True" VisibleIndex="7" PropertiesTextEdit-MaxLength="6" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="EMAIL" FieldName="EMAIL" Visible="True" VisibleIndex="8" PropertiesTextEdit-MaxLength="60" Width="150" >
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="PHONE" FieldName="PHONE" Visible="True" VisibleIndex="9" PropertiesTextEdit-MaxLength="10">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="CO_GRP" FieldName="CO_GRP" Visible="True" VisibleIndex="10" PropertiesTextEdit-MaxLength="3" Width="55">
                                             <PropertiesTextEdit MaxLength="3">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Company Group"/>
                                        </ValidationSettings>
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn meta:resourcekey="CO_NAME" FieldName="CO_NAME" Visible="True" VisibleIndex="11" PropertiesTextEdit-MaxLength="30" Width="150">
                                          <PropertiesTextEdit MaxLength="30">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Company Name"/>
                                        </ValidationSettings>
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                               <dx:GridViewDataTextColumn meta:resourcekey="CO_CD" FieldName="CO_CD" Visible="True" VisibleIndex="12" PropertiesTextEdit-MaxLength="3" Width="50">
                                    <PropertiesTextEdit MaxLength="3">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Company Code"/>
                                        </ValidationSettings>
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="CO_TYPE" FieldName="CO_TYPE" Visible="True" VisibleIndex="13" PropertiesTextEdit-MaxLength="60">
                                       <PropertiesTextEdit MaxLength="60">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Company Type"/>
                                        </ValidationSettings>
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="LOC_TYPE" FieldName="LOC_TYPE" Visible="True" VisibleIndex="14" PropertiesTextEdit-MaxLength="3">
      <PropertiesTextEdit MaxLength="3">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Location Type"/>
                                        </ValidationSettings>
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="BANNER" FieldName="BANNER" Visible="True" VisibleIndex="15" PropertiesTextEdit-MaxLength="20">
      <PropertiesTextEdit MaxLength="20">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Banner"/>
                                        </ValidationSettings>
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="REGION" FieldName="REGION" Visible="True" VisibleIndex="16" PropertiesTextEdit-MaxLength="1"  Width="50">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="METRO_STORE" FieldName="METRO_STORE" Visible="True" VisibleIndex="17" PropertiesTextEdit-MaxLength="1">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="METRO_AREA" FieldName="METRO_AREA" Visible="True" VisibleIndex="18" PropertiesTextEdit-MaxLength="3">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="METRO_AREA_DESC" FieldName="METRO_AREA_DESC" Visible="True" VisibleIndex="19" PropertiesTextEdit-MaxLength="100">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="PHYSICAL_LOC" FieldName="PHYSICAL_LOC" Visible="True" VisibleIndex="20" PropertiesTextEdit-MaxLength="1">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataDateColumn meta:resourcekey="OPEN_DATE" FieldName="OPEN_DATE" Visible="True" VisibleIndex="21">
      <PropertiesDateEdit DisplayFormatString="d/MM/yyyy" EditFormatString="d/MM/yyyy">
            </PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
 <dx:GridViewDataDateColumn meta:resourcekey="CLOSE_DATE" FieldName="CLOSE_DATE" Visible="True" VisibleIndex="22">
      <PropertiesDateEdit DisplayFormatString="d/MM/yyyy" EditFormatString="d/MM/yyyy">
            </PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="SAME_STORE" FieldName="SAME_STORE" Visible="True" VisibleIndex="23" PropertiesTextEdit-MaxLength="1">  
       <PropertiesTextEdit MaxLength="1">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Same Store"/>
                                        </ValidationSettings>
                                        </PropertiesTextEdit>  
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_STATUS" FieldName="STORE_STATUS" Visible="True" VisibleIndex="24" PropertiesTextEdit-MaxLength="6">
      <PropertiesTextEdit MaxLength="6">
                                        <ValidationSettings>
                                            <RequiredField IsRequired="True" ErrorText="Please enter the Store Status"/>
                                        </ValidationSettings>
                                        </PropertiesTextEdit>  
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="FIN_CO_CD" FieldName="FIN_CO_CD" Visible="True" VisibleIndex="25" PropertiesTextEdit-MaxLength="2">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="FIN_BANNER_CD" FieldName="FIN_BANNER_CD" Visible="True" VisibleIndex="26" PropertiesTextEdit-MaxLength="2">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="FIN_REGION_CD" FieldName="FIN_REGION_CD" Visible="True" VisibleIndex="27" PropertiesTextEdit-MaxLength="2">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="FIN_LOCATION_CD" FieldName="FIN_LOCATION_CD" Visible="True" VisibleIndex="28" PropertiesTextEdit-MaxLength="4">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="CREDIT_STORE" FieldName="CREDIT_STORE" Visible="True" VisibleIndex="29" PropertiesTextEdit-MaxLength="3">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="PARENT_CO" FieldName="PARENT_CO" Visible="True" VisibleIndex="30" PropertiesTextEdit-MaxLength="100">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="SUPPLY_POINT" FieldName="SUPPLY_POINT" Visible="True" VisibleIndex="31" PropertiesTextEdit-MaxLength="2">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="LINEHAUL" FieldName="LINEHAUL" Visible="True" VisibleIndex="32" PropertiesTextEdit-MaxLength="1">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="FLEET_STORE_NAME" FieldName="FLEET_STORE_NAME" Visible="True" VisibleIndex="33" PropertiesTextEdit-MaxLength="30">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="FLEET_STORE_NAME" FieldName="FLEET_STORE_NAME" Visible="True" VisibleIndex="34" PropertiesTextEdit-MaxLength="30">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="FLEET_STORE_MANAGER" FieldName="FLEET_STORE_MANAGER" Visible="True" VisibleIndex="35" PropertiesTextEdit-MaxLength="30">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="FLEET_STORE_EMAIL" FieldName="FLEET_STORE_EMAIL" Visible="True" VisibleIndex="36" PropertiesTextEdit-MaxLength="30">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="FLEET_STORE_REGION" FieldName="FLEET_STORE_REGION" Visible="True" VisibleIndex="37" PropertiesTextEdit-MaxLength="20">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="AM_NAME" FieldName="AM_NAME" Visible="True" VisibleIndex="38" PropertiesTextEdit-MaxLength="30">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="AM_SORT_CD" FieldName="AM_SORT_CD" Visible="True" VisibleIndex="39" PropertiesTextEdit-MaxLength="9">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="AM_EMAIL" FieldName="AM_EMAIL" Visible="True" VisibleIndex="40" PropertiesTextEdit-MaxLength="30">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="AM_GRP_EMAIL" FieldName="AM_GRP_EMAIL" Visible="True" VisibleIndex="41" PropertiesTextEdit-MaxLength="30">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_LEVEL" FieldName="STORE_LEVEL" Visible="True" VisibleIndex="42" PropertiesTextEdit-MaxLength="20">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_SQFEET" FieldName="STORE_SQFEET" Visible="True" VisibleIndex="43" PropertiesTextEdit-MaxLength="9">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR1" FieldName="STORE_ATTR1" Visible="True" VisibleIndex="44" PropertiesTextEdit-MaxLength="60">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR2" FieldName="STORE_ATTR2" Visible="True" VisibleIndex="45" PropertiesTextEdit-MaxLength="60">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR3" FieldName="STORE_ATTR3" Visible="True" VisibleIndex="46" PropertiesTextEdit-MaxLength="60">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR4" FieldName="STORE_ATTR4" Visible="True" VisibleIndex="47" PropertiesTextEdit-MaxLength="60">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR5" FieldName="STORE_ATTR5" Visible="True" VisibleIndex="48" PropertiesTextEdit-MaxLength="60">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR6" FieldName="STORE_ATTR6" Visible="True" VisibleIndex="49" PropertiesTextEdit-MaxLength="9">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR7" FieldName="STORE_ATTR7" Visible="True" VisibleIndex="50" PropertiesTextEdit-MaxLength="16">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR8" FieldName="STORE_ATTR8" Visible="True" VisibleIndex="51" PropertiesTextEdit-MaxLength="9">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR9" FieldName="STORE_ATTR9" Visible="True" VisibleIndex="52" PropertiesTextEdit-MaxLength="16">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="STORE_ATTR10" FieldName="STORE_ATTR10" Visible="True" VisibleIndex="53" PropertiesTextEdit-MaxLength="16">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="TRANSIT_NO" FieldName="TRANSIT_NO" Visible="True" VisibleIndex="54" PropertiesTextEdit-MaxLength="50">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="ACCT_NO" FieldName="ACCT_NO" Visible="True" VisibleIndex="55" PropertiesTextEdit-MaxLength="50">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="MARKET" FieldName="MARKET" Visible="True" VisibleIndex="56" PropertiesTextEdit-MaxLength="50">
                                    </dx:GridViewDataTextColumn>
 <dx:GridViewDataTextColumn meta:resourcekey="MERCHANT_NO" FieldName="MERCHANT_NO" Visible="True" VisibleIndex="57" PropertiesTextEdit-MaxLength="50">
                                    </dx:GridViewDataTextColumn>

                                </Columns>
                                <SettingsEditing Mode="Inline"></SettingsEditing>
                                <SettingsDataSecurity AllowDelete="True" />
                                <Templates>
                                    <FooterRow>
                                        <dx:ASPxLabel ID="GV_Footer_label" runat="server" ClientInstanceName="GV_Footer_label" Text="TEST" OnInit="GV_Footer_label_init" ForeColor="Red"></dx:ASPxLabel>
                                    </FooterRow>
                                </Templates>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="lbl_message" runat="server" Width="100%" ForeColor="Red" EncodeHtml="false"></dx:ASPxLabel>
            </td>
        </tr>
    </table>

      <div runat="server" id="div_version">
        <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
        </dx:ASPxLabel>
    </div>
</asp:Content>








