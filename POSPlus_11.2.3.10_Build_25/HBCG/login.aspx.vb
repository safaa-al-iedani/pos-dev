Imports System.Data.OracleClient
Imports HBCG_Utils
Imports System.Globalization

Partial Class Login_Page
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Daniela get ip and computer name
        'sabrina xxxxxxxxxxxxxxx
        If Not IsPostBack Then
            If Not Request("clientip") Is Nothing Then
                If Request("clientip").ToString.isNotEmpty Then
                    ' Create cookie only when login from goglobal
                    Dim appCookie As New HttpCookie("IpCookie")
                    appCookie.Value = Request("clientip")
                    appCookie.Expires = Now.AddDays(1)
                    appCookie.Path = "posleons.com"
                    Response.Cookies.Add(appCookie)
                    Session("clientip") = Request("clientip")
                End If
            End If
            ' Login after logout
            If Request("clientip") Is Nothing Then
                ' Check for cookie  first
                Dim ipcookie As String = Nothing
                If Not Request.Cookies("IpCookie") Is Nothing Then
                    Dim aCookie As HttpCookie = Request.Cookies("IpCookie")
                    ipcookie = Server.HtmlEncode(aCookie.Value)
                End If
                ' If not gglobal then default to remote address
                If ipcookie Is Nothing Then
                    Session("clientip") = Request.ServerVariables("REMOTE_ADDR") ' Not goglobal used
                Else
                    If ipcookie.ToString.isNotEmpty Then
                        Session("clientip") = ipcookie ' use cookie to get IP address
                    End If
                End If
            End If

            If Not Request("cname") Is Nothing Then ' not needed for now 
                If Session("clientip") Is Nothing Then
                    Session("cname") = Request("cname")
                End If
            End If
        End If


        'Daniela end

        If Not IsPostBack Then
            txtLoginUser.Focus()
            Dim login As String = String.Empty
            Dim pwd As String = String.Empty
            'sabrina
            'PopulateCompanies()

            If txtLoginUser.Text.isNotEmpty Then
                login = txtLoginUser.Text
                pwd = txt_password.Text
            Else
                If Request("login") & "" <> "" Then
                    login = Request("login")
                    pwd = Request("password")
                    txtLoginUser.Text = login
                    txt_password.Text = pwd
                End If
            End If

            Dim Version_String As String = "RedPrairie MM POS Version " & Get_Version("/HandleCode/HighlandsPOS", "Version")
            lbl_version.Text = Version_String
            'Dim MMHFLink As String = LeonsBiz.GetFtpIpAdress("MMHF_LNK")
            'If Not String.IsNullOrEmpty(MMHFLink) Then
            '    lit_mmhfLink.Text = "URL to MMHF " & "<span style='font-weight:bold; font-style:italic'>must be opened In Internet Explorer</span> " & " - " & MMHFLink
            'End If

        End If

    End Sub

    ''' <summary>
    ''' Populates the company drop-down
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateCompanies()
        Try
            Dim ds As DataSet = theSystemBiz.GetCompanies()
            With cboxCompanies
                .DataSource = ds
                .ValueField = "co_cd"
                .TextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub

    '    Protected Sub btnChangePwd_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnChangePwd.Click

    '

    '    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As System.EventArgs) Handles btnLogin.Click

        Dim userInit As String = ""
        Dim userPwd As String = ""

        If txtLoginUser.Text.isNotEmpty Then
            userInit = txtLoginUser.Text
            userPwd = txt_password.Text
        Else
            If Request("login") & "" <> "" Then
                userInit = Request("login")
                userPwd = Request("password")
                txtLoginUser.Text = userInit
                txt_password.Text = userPwd
            End If
        End If

        'sabrina
        '        If txt_co_password.Visible = True AndAlso txt_co_password.Text.isEmpty Then
        '            lblInfo.Text = "You must enter a company password."
        '
        '        End If

        Dim versionText As String = "RedPrairie MM POS Version " & Get_Version("/HandleCode/HighlandsPOS", "Version")
        lbl_version.Text = versionText

        If userInit.isNotEmpty Then
            If Request("Msg") & "" <> "" Then
                lblInfo.Text = Request("Msg")
            End If

	   'Feb 11 check connection for every environment
            If Not ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            'If ConfigurationManager.AppSettings("system_mode") = "LIVE" Then
                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim dsn_text As String = ""
                Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                Dim x As Integer
                For x = LBound(conStr) To UBound(conStr) - 1
                    If LCase(Left(conStr(x), 11)) = "data source" Then
                        dsn_text = conStr(x)
                    End If
                Next
                conn.ConnectionString = dsn_text & ";uid=" & userInit & ";pwd=" & userPwd
                Try
                    conn.Open()
                    conn.Close()
                Catch ex As Exception
                    lblInfo.Text = Err.Description
                    theSystemBiz.SaveAuditLogComment("LOGIN_FAILURE", UCase(userInit) & " - " & Err.Description, UCase(userInit))
                    conn.Close()
                    Exit Sub  'since the user is NOT valid, stops validations at this point
                End Try
            End If

            Dim loginResponse As LoginResponseDtc = theSystemBiz.DoLogin(cboxCompanies.Value, txt_co_password.Text, userInit)
            If loginResponse.message.isEmpty Then

                'login succeeded- so set all the sesssion variables
                InitializeSessionData(loginResponse)

                ' Daniela Oct 28, 2014
                ' Check if pilot store commented since return Y all the time
                'If LeonsBiz.GetStorePilot(Session("HOME_STORE_CD")) = "N" Then
                '    theSystemBiz.SaveAuditLogComment("LOGIN_FAILURE", UCase(userInit) & " - " & "Not A PILOT STORE", UCase(userInit))
                '    lblInfo.Text = "Invalid Home Store."
                '    Exit Sub
                'End If
                theSystemBiz.SaveAuditLogComment("LOGIN_SUCCESS", loginResponse.emp.cd & " logged In successfully")
                If ConfigurationManager.AppSettings("use_generic") = "Y" Then
                    Response.Redirect("Select_Salesperson.aspx")
                Else
                    If InStr(Request.ServerVariables("HTTP_HOST"), "payment") > 0 Then
                        Response.Redirect("IndependentPaymentProcessing.aspx", False)
                    Else
                        Response.Redirect("newmain.aspx", False)


                    End If
                End If
                lblInfo.Text = String.Empty

            Else
                'display the login error
                lblInfo.Text = loginResponse.message
                theSystemBiz.SaveAuditLogComment("LOGIN_FAILURE", "Login failed: " & loginResponse.message, UCase(userInit))

            End If
        Else
            Session.Abandon()
        End If

    End Sub

    ' Daniela Nov 7, 2014 comment Protected Sub btnChangePwd_Click(sender As Object, e As System.EventArgs) Handles btnChangePwd.Click

    '    lbl_change_label.Text = ""

    '    Dim login As String = txt_change_username.Text
    '    Dim pwd As String = txt_change_curr_password.Text

    '    If txt_change_new_password.Text <> txt_change_new_password2.Text Then
    '        lbl_change_label.Text = "Your new passwords do not match."
    '        Exit Sub
    '    End If

    '    If login & "" <> "" Then
    '        Dim conn As New OracleConnection
    '        Dim sql As String
    '        Dim objSql As OracleCommand
    '        Dim CatchMe As Boolean = False
    '        If Request("Msg") & "" <> "" Then
    '            lblInfo.Text = Request("Msg")
    '        End If
    '        If txt_change_username.Text & "" = "" Then
    '            CatchMe = True
    '        End If
    '        If CatchMe = False Then
    '            Try

    '                Dim dsn_text As String = ""
    '                Dim Connection_String = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
    '                Dim x As Integer

    '                For x = LBound(Connection_String) To UBound(Connection_String) - 1
    '                    If LCase(Left(Connection_String(x), 11)) = "data source" Then
    '                        dsn_text = Connection_String(x)
    '                    End If
    '                Next    'x

    '                conn.ConnectionString = dsn_text & ";uid=" & login & ";pwd=" & pwd
    '                conn.Open()

    '                sql = "ALTER USER " & UCase(txt_change_username.Text) & " IDENTIFIED BY " & Replace(UCase(txt_change_new_password.Text), "'", "''") & " REPLACE " & Replace(UCase(txt_change_curr_password.Text), "'", "''")

    '                'Set SQL OBJECT 
    '                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

    '                'objSql.Parameters.Add(":UPDATE_USER", OracleType.VarChar)
    '                'objSql.Parameters.Add(":UPDATE_PASSWORD", OracleType.VarChar)

    '                'objSql.Parameters(":UPDATE_USER").Value = UCase(txt_change_username.Text)
    '                'objSql.Parameters(":UPDATE_PASSWORD").Value = UCase(txt_change_new_password.Text)

    '                objSql.ExecuteNonQuery()
    '                objSql.Parameters.Clear()

    '                lbl_change_label.Text = "Password successfully changed"
    '                txt_change_username.Text = ""
    '                txt_change_curr_password.Text = ""
    '                txt_change_new_password.Text = ""
    '                txt_change_new_password2.Text = ""

    '                theSystemBiz.SaveAuditLogComment("PASSWORD_CHANGE_SUCCESS", UCase(login) & " - Successfully changed password", UCase(login))
    '                conn.Close()
    '            Catch ex As Exception
    '                'Throw
    '                lbl_change_label.Text = Err.Description
    '                theSystemBiz.SaveAuditLogComment("PASSWORD_CHANGE_FAIL", UCase(login) & " - " & Err.Description, UCase(login))
    '                conn.Close()
    '            End Try
    '        End If
    '    Else
    '        If Not IsPostBack Then
    '            lbl_change_label.Text = "Please enter a valid user name"
    '        End If
    '    End If

    'End Sub

    Protected Sub txtLoginUser_TextChanged(sender As Object, e As System.EventArgs) Handles txtLoginUser.TextChanged

        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim sql As String
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        If txtLoginUser.Text.isNotEmpty Then

            lblInfo.Text = String.Empty
            sql = "SELECT CO.CO_CD, CO.PASSWORD " &
                  "FROM STORE, EMP, CO " &
                  "WHERE EMP.HOME_STORE_CD=STORE.STORE_CD AND EMP.EMP_INIT=:EMP_INIT AND STORE.CO_CD=CO.CO_CD GROUP BY CO.CO_CD, CO.PASSWORD"

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Add(":EMP_INIT", OracleType.VarChar)
            cmd.Parameters(":EMP_INIT").Value = UCase(txtLoginUser.Text)

            Try
                reader = DisposablesManager.BuildOracleDataReader(cmd)


                If reader.Read() Then
                    cboxCompanies.Value = reader.Item("CO_CD").ToString
                    'sabrina comment out
                    'If reader.Item("PASSWORD").ToString & "" = "" Then
                    '    txt_co_password.Visible = False
                    '    ASPxLabel9.Visible = False
                    '    txt_co_password.Text = ""
                    'Else
                    '    txt_co_password.Visible = True
                    '    ASPxLabel9.Visible = True
                    '    txt_co_password.Text = ""
                    'End If
                    txt_password.Focus()
                Else
                    lblInfo.Text = Resources.POSErrors.ERR0001
                    txtLoginUser.Focus()
                End If

                reader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If
        conn.Close()


    End Sub

    Protected Sub cboxCompanies_ValueChanged(sender As Object, e As System.EventArgs) Handles cboxCompanies.ValueChanged

        Dim coPwd As String = theSystemBiz.GetCompanyPwd((cboxCompanies.Value))

        If coPwd.isEmpty Then
            txt_co_password.Visible = False
            ASPxLabel9.Visible = False
            txt_co_password.Text = ""
        Else
            txt_co_password.Visible = True
            ASPxLabel9.Visible = True
            txt_co_password.Text = ""
        End If

    End Sub

    ''' <summary>
    ''' Initializes and sets the session values with those set at login.
    ''' </summary>
    ''' <param name="loginResponse">the login response object with the details</param>
    Private Sub InitializeSessionData(ByVal loginResponse As LoginResponseDtc)

        Dim tranData As New TransactionData()
        tranData.emp.cd = loginResponse.emp.cd
        tranData.emp.init = loginResponse.emp.init
        tranData.emp.fName = loginResponse.emp.fName
        tranData.emp.lName = loginResponse.emp.lName
        tranData.emp.homeStore = loginResponse.emp.homeStore
        tranData.emp.skipDataSecurity = loginResponse.emp.skipDataSecurity
        tranData.emp.skipSecurity = loginResponse.emp.skipSecurity
        tranData.coCd = cboxCompanies.Value
         'Daniela comment not needed
        'Session("TRAN_DATA") = tranData

        'TODO:AXK - need to remove
        '********** REMOVE SETTING OF ANY SESSION VBLS ONCE ALL OTHER REFERENCES ARE ELIMINATED **********
        ' Daniela add toUpper 
        Session("EMP_INIT") = loginResponse.emp.init.ToUpper
        Session("EMP_CD") = loginResponse.emp.cd
        Session("EMP_FNAME") = loginResponse.emp.fName
        Session("EMP_LNAME") = loginResponse.emp.lName
        Session("HOME_STORE_CD") = loginResponse.emp.homeStore
        Session("CO_CD") = cboxCompanies.Value


    End Sub


End Class