Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports AppUtils
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxTreeList
Partial Class AeroplanMPC
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Dim g_custmpc_id As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim v_name As String = ""
        lbl_msg.Text = ""
        lbl_msg2.Text = ""
        Session("aeroMPCmain") = "Y"

        If isAuthToUpd() = "Y" Then
            btn_new.Visible = True
            CreateCustomer()
        Else
            btn_new.Visible = False
        End If

        If Session("aero_cust_cd") & "" <> "" Then
            v_name = get_cust_name(Session("aero_cust_cd"))
            srch_cust_cd.Text = Session("aero_cust_cd")
            srch_cust_name.Text = v_name
        End If

        If IsPostBack Then
            If ASPxPopupCustMPC1.ShowOnPageLoad = True Or
               ASPxPopupCustMPC2.ShowOnPageLoad = True Then
                If search_criteria_exists() = "Y" Then
                    populate_gridview()
                End If
            End If
        Else
            Populate_str()
            Populate_mpc_code()
            btn_search.Visible = True
        End If

    End Sub
    Public Function isAuthToUpd() As String

        Dim v_ind As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoupdate('" & Session("emp_cd") & "','AeroplanMPC.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                v_ind = MyDatareader2.Item("V_RES").ToString
            Else
                v_ind = "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

        If v_ind & "" = "" Then
            Return "N"
        ElseIf v_ind <> "Y" And v_ind <> "N" Then
            Return "N"
        Else
            Return v_ind
        End If

    End Function
    Protected Sub clear_the_screen()

        srch_cust_cd.Text = ""
        srch_cust_name.Text = ""

        If srch_str_cd.SelectedIndex > 0 Then
            srch_str_cd.SelectedIndex = 0
        End If
        
        srch_aeroplan_crd_num.Text = ""
        srch_contract.Text = ""
        srch_cre_by.Text = ""
        srch_cre_name.Text = ""

        If srch_mpc_code.SelectedIndex > 0 Then
            srch_mpc_code.SelectedIndex = 0
        End If

        srch_cre_by.Text = ""

        srch_process_dt_from.Text = ""
        srch_process_dt_to.Text = ""
        srch_purch_dt_from.Text = ""
        srch_purch_dt_to.Text = ""

        clndr_purch_dt_from.Visible = False
        clndr_purch_dt_to.Visible = False
        clndr_process_dt_from.Visible = False
        clndr_process_dt_to.Visible = False

        clndr_process_dt_from.SelectedDate = DateTime.MinValue
        clndr_process_dt_to.SelectedDate = DateTime.MinValue
        clndr_purch_dt_from.SelectedDate = DateTime.MinValue
        clndr_purch_dt_to.SelectedDate = DateTime.MinValue

        ASPxPopupCustMPC1.ShowOnPageLoad = False
        ASPxPopupCustMPC2.ShowOnPageLoad = False

        GridView1.DataSource = ""
        GridView1.DataBind()
        GridView1.PageIndex = 0

        GridView9.DataSource = ""
        GridView9.DataBind()
        GridView9.PageIndex = 0

    End Sub
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        Session("aero_cust_cd") = ""
        clear_the_screen()

    End Sub
    Protected Sub btn_new_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_new.Click

        clear_the_screen()
        ASPxPopupCustMPC1.ShowOnPageLoad() = True


    End Sub
    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click

        Dim v_msg As String = ""

        If search_criteria_exists() <> "Y" Then
            lbl_msg.Text = "Please enter search criteria to continue"
            Exit Sub
        Else
            v_msg = validate_search_fields()
            If v_msg & "" <> "" Then
                lbl_msg.Text = v_msg
                Exit Sub
            End If
        End If

        GridView1.PageIndex = 0
        GridView9.PageIndex = 0
        populate_gridview()

        If isAuthToUpd() = "Y" Then
            ASPxPopupCustMPC1.ShowOnPageLoad() = True
            If GridView1.VisibleRowCount < 1 Then
                lbl_msg2.Text = "No Data to display"
            End If
        Else
            ASPxPopupCustMPC2.ShowOnPageLoad() = True
            If GridView9.VisibleRowCount < 1 Then
                lbl_msg2.Text = "No Data to display"
            End If
        End If


    End Sub

    Function search_criteria_exists() As String

        If (srch_str_cd.SelectedIndex < 1) And
     (srch_aeroplan_crd_num.Text & "" = "" Or isEmpty(srch_aeroplan_crd_num.Text)) And
     (srch_cust_cd.Text & "" = "" Or isEmpty(srch_cust_cd.Text)) And
     (srch_contract.Text & "" = "" Or isEmpty(srch_contract.Text)) And
     (srch_cre_by.Text & "" = "" Or isEmpty(srch_cre_by.Text)) And
     (srch_mpc_code.SelectedIndex < 1) And
     (srch_purch_dt_from.Text & "" = "" Or isEmpty(srch_purch_dt_from.Text)) And
     (srch_purch_dt_to.Text & "" = "" Or isEmpty(srch_purch_dt_to.Text)) And
     (srch_process_dt_from.Text & "" = "" Or isEmpty(srch_process_dt_from.Text)) And
     (srch_process_dt_to.Text & "" = "" Or isEmpty(srch_process_dt_to.Text)) Then
            Return "N"
        Else
            Return "Y"
        End If


    End Function
    Function validate_search_fields() As String

        If (srch_purch_dt_from.Text & "" = "" Or isEmpty(srch_purch_dt_from.Text)) And
            (srch_purch_dt_to.Text & "" <> "") Then
            dt1_btn.Focus()
            Return "Must enter a value for Enter Date FROM to continue "
        End If

        If (srch_purch_dt_to.Text & "" = "" Or isEmpty(srch_purch_dt_to.Text)) And
            (srch_purch_dt_from.Text & "" <> "") Then
            dt2_btn.Focus()
            Return "Must enter a value for Enter Date TO to continue "
        End If

        If (srch_process_dt_from.Text & "" = "" Or isEmpty(srch_process_dt_from.Text)) And
            (srch_process_dt_to.Text & "" <> "") Then
            dt3_btn.Focus()
            Return "Must enter a value for Process Date FROM to continue"
        End If

        If (srch_process_dt_to.Text & "" = "" Or isEmpty(srch_process_dt_to.Text)) And
            (srch_process_dt_from.Text & "" <> "") Then
            dt4_btn.Focus()
            Return "Must enter a value for Process Date TO to continue"
        End If

        Return ""

    End Function
    Protected Sub populate_gridview()



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim v_upd_usr As String = isAuthToUpd()

        Dim v_str_sql As String = ""
        Dim v_aeroplan_sql As String = ""
        Dim v_cust_sql As String = ""
        Dim v_contract_sql As String = ""
        Dim v_cre_by_sql As String = ""
        Dim v_mpc_code_sql As String = ""
        Dim v_purch_dt_sql As String = ""
        Dim v_process_dt_sql As String = ""

        If srch_str_cd.SelectedIndex < 1 Then
            v_str_sql = " and std_multi_co2.isvalidstr7('" & Session("emp_init") & "', nvl(a.store_cd,'x')) = 'Y' "
        Else
            v_str_sql = " and a.store_cd = '" & srch_str_cd.SelectedItem.Value.ToString() & "' " &
                        " and std_multi_co2.isvalidstr7('" & Session("emp_init") & "',nvl(a.store_cd,'x')) = 'Y' "
        End If

        If srch_aeroplan_crd_num.Text & "" = "" Or isEmpty(srch_aeroplan_crd_num.Text) Then
            v_aeroplan_sql = ""
        Else
            v_aeroplan_sql = " and a.aeroplan_crd_num = nvl('" & srch_aeroplan_crd_num.Text & "', a.aeroplan_crd_num) "
        End If

        If srch_cust_cd.Text & "" = "" Or isEmpty(srch_cust_cd.Text) Then
            v_cust_sql = ""
        Else
            v_cust_sql = " and nvl(a.cust_cd,'x') = nvl('" & UCase(srch_cust_cd.Text) & "', a.cust_cd) "
        End If

        If srch_contract.Text & "" = "" Or isEmpty(srch_contract.Text) Then
            v_contract_sql = ""
        Else
            v_contract_sql = " and nvl(d.contract,'!') = nvl('" & srch_contract.Text & "', d.contract) "
        End If

        If srch_cre_by.Text & "" = "" Then
            v_cre_by_sql = ""
        Else
            v_cre_by_sql = " and nvl(a.cre_by,'x') = upper('" & srch_cre_by.Text & "') "
        End If

        If srch_mpc_code.SelectedIndex < 1 Then
            v_mpc_code_sql = ""
        Else
            v_mpc_code_sql = " and d.mpc_code = '" & srch_mpc_code.SelectedItem.Value.ToString() & "' "
        End If

        If srch_purch_dt_from.Text & "" <> "" And srch_purch_dt_to.Text & "" <> "" Then
            v_purch_dt_sql = " and trunc(a.aeroplan_purch_dt) between " &
                             " to_date('" & FormatDateTime(srch_purch_dt_from.Text, DateFormat.ShortDate) & "','mm/dd/RRRR')" &
                             " and " &
                             " to_date('" & FormatDateTime(srch_purch_dt_to.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        Else
            v_purch_dt_sql = ""
        End If

        If srch_process_dt_from.Text & "" <> "" And srch_process_dt_to.Text & "" <> "" Then
            v_process_dt_sql = " and trunc(d.processing_dt) between " &
                             " to_date('" & FormatDateTime(srch_process_dt_from.Text, DateFormat.ShortDate) & "','mm/dd/RRRR')" &
                             " and " &
                             " to_date('" & FormatDateTime(srch_process_dt_to.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        Else
            v_process_dt_sql = ""
        End If


        GridView1.DataSource = ""
        GridView9.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        objSql.CommandText = " select a.cust_cd CUST_CD " &
                             " ,c.fname||' '||c.lname CUST_NAME " &
                             " ,decode('" & v_upd_usr & "','Y',a.aeroplan_crd_num,substr(a.aeroplan_crd_num,1,3)||'***'||substr(a.aeroplan_crd_num,7,3)) AEROPLAN_CRD_NUM " &
                             " ,a.store_cd STORE_CD " &
                             " ,trunc(a.aeroplan_purch_dt) AEROPLAN_PURCH_DT " &
                             " ,a.cre_by CRE_BY " &
                             " ,d.mpc_code MPC_CODE " &
                             " ,d.qualify_pts QUALIFY_PTS " &
                             " ,d.contract CONTRACT " &
                             " ,d.stat_cd STAT_CD " &
                             " ,d.cmnt CMNT " &
                             " ,d.processing_dt PROCESSING_DT " &
                             " ,d.custmpc_id CUSTMPC_ID " &
                             " from so_aeroplan a, so_aeroplan_custmpc_dtl d, cust c" &
                             "  where nvl(a.cust_cd,'x') = c.cust_cd " &
                             "    and a.custmpc_id = d.custmpc_id " &
                             v_cust_sql &
                             v_str_sql &
                             v_cre_by_sql &
                             v_mpc_code_sql &
                             v_aeroplan_sql &
                             v_contract_sql &
                             v_purch_dt_sql &
                             v_process_dt_sql &
                             " order by a.store_cd,a.cust_cd,a.aeroplan_crd_num,a.aeroplan_purch_dt,d.mpc_code,d.qualify_pts"

        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView9.DataSource = ds
                GridView9.DataBind()
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView9.DataSource = ds
                GridView9.DataBind()
                lbl_msg.Text = "No data to display"
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Sub
    Function getStat(ByVal p_cust_cd As String, ByVal p_custmpc_id As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT min(stat_cd) stat "
        sql = sql & " from so_aeroplan_custmpc_dtl "
        sql = sql & " where cust_cd = '" & p_cust_cd & "' "
        sql = sql & "   and custmpc_id = '" & p_custmpc_id & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("stat")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function getCustMpcStr(ByVal p_cust_cd As String, ByVal p_id As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT store_cd str "
        sql = sql & " from so_aeroplan "
        sql = sql & " where cust_cd = '" & p_cust_cd & "' "
        sql = sql & "   and custmpc_id = '" & p_id & "' "
        sql = sql & "   and del_doc_num = '" & p_id & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("str")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function getStartDt(ByVal p_mpc As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim v_mpc As String = ""

        Dim sql As String = "SELECT start_dt dt "
        sql = sql & " from aimia_mpc_hdr "
        sql = sql & " where mpc_code = '" & p_mpc & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("dt")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function getMaxMiles(ByVal p_mpc As String) As String


        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim v_mpc As String = ""

        Dim sql As String = "SELECT nvl(mileage_max,0) miles "
        sql = sql & " from aimia_mpc_hdr "
        sql = sql & " where mpc_code = '" & p_mpc & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("miles")
            Else
                v_value = "0"
            End If
        Catch
            v_value = "0"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If isEmpty(v_value) Then
            Return "0"
        Else
            Return v_value
        End If

    End Function
    Function getCustMPC() As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim v_mpc As String = ""

        Dim sql As String = "SELECT std_aeroplan.getCustmpcID id from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("id")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function isValidCardNum(ByVal p_card_num As String) As String

        'for now only allow 9 digit card#
        'If Len(p_card_num) = 16 Then  
        '    If Left(p_card_num, 5) <> "627421" Then       'bin#
        '        Return "N"
        '    End If
        'End If

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT std_aeroplan.validate_crdnum('" & p_card_num & "') ind "
        sql = sql & " from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("ind")
            Else
                v_value = "N"
            End If
        Catch
            v_value = "N"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        Else
            Return v_value
        End If

    End Function
    Function isValidCustomer(ByVal p_cust_cd As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from cust " &
                            " where cust_cd = '" & UCase(p_cust_cd) & "'"


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Function isValidStore(ByVal p_str As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt " &
                            " from store s " &
                            " where s.store_cd = '" & p_str & "' and " &
                            " std_multi_co2.isvalidstr2('" & Session("emp_init") & "', s.store_cd) = 'Y' "


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Function IsValidPurchaseDt(ByVal p_dt As String, ByVal p_mpc_cd As String) As String

        'Req2778 (new function)
        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        If Not IsDate(p_dt) Then
            Return "N"
        End If

        Dim sql As String = "SELECT count(*) cnt from aimia_mpc_hdr "
        sql = sql & " where mpc_code = '" & p_mpc_cd & "' "
        sql = sql & "  and to_date ('" & FormatDateTime(p_dt.ToString(), 2) & "','mm/dd/RRRR') between start_dt and end_dt"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value < 1 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Function OrderAlreadyExists(ByVal p_doc As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from so_aeroplan " &
                            " where CUST_NAME = '" & UCase(p_doc) & "'"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 1
            End If
        Catch
            v_value = 1
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "Y"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Public Function getMPC() As DataSet

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        sql = " select mpc_code cd, mpc_code||' - '||mpc_name nm from aimia_mpc_hdr "
        sql = sql & " where cust_mpc_ind = 'Y' "
        sql = sql & " order by 1 "

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)
            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

        Return ds

    End Function
    Public Function getStores() As DataSet

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        sql = " select store_cd str_cd, store_cd||' - '||store_name str_nm from store "
        sql = sql & " where std_multi_co2.isvalidstr2('" & Session("emp_init") & "',store_cd) = 'Y' "
        sql = sql & " order by 1 "

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)
            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

        Return ds

    End Function

    Protected Sub srch_dt1_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt1_btn.Click
        clndr_purch_dt_from.Visible = True
        clndr_purch_dt_from.Focus()
    End Sub
    Protected Sub srch_dt1x_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt1x_btn.Click
        clndr_purch_dt_from.Visible = False
        srch_purch_dt_from.Text = ""
    End Sub
    Protected Sub srch_dt2_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt2_btn.Click
        clndr_purch_dt_to.Visible = True
        clndr_purch_dt_to.Focus()
    End Sub
    Protected Sub srch_dt2x_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt2x_btn.Click
        clndr_purch_dt_to.Visible = False
        srch_purch_dt_to.Text = ""
    End Sub
    Protected Sub srch_dt3_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt3_btn.Click
        clndr_process_dt_from.Visible = True
        clndr_process_dt_from.Focus()
    End Sub
    Protected Sub srch_dt3x_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt3x_btn.Click
        clndr_process_dt_from.Visible = False
        srch_process_dt_from.Text = ""
    End Sub
    Protected Sub srch_dt4_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt4_btn.Click
        clndr_process_dt_to.Visible = True
        clndr_process_dt_to.Focus()
    End Sub
    Protected Sub srch_dt4x_btn(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt4x_btn.Click
        clndr_process_dt_to.Visible = False
        srch_process_dt_to.Text = ""
    End Sub
    Protected Sub srch_dt1_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        srch_purch_dt_from.Text = clndr_purch_dt_from.SelectedDate
        clndr_purch_dt_from.Visible = False
    End Sub
    Protected Sub srch_dt2_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        srch_purch_dt_to.Text = clndr_purch_dt_to.SelectedDate
        clndr_purch_dt_to.Visible = False
    End Sub
    Protected Sub srch_dt3_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        srch_process_dt_from.Text = clndr_process_dt_from.SelectedDate
        clndr_process_dt_from.Visible = False
    End Sub
    Protected Sub srch_dt4_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        srch_process_dt_to.Text = clndr_process_dt_to.SelectedDate
        clndr_process_dt_to.Visible = False
    End Sub
    Private Sub CreateCustomer()

        If Session("aero_cust_cd") = "NEW" And isAuthToUpd() = "Y" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim hphone As String = ""
            Dim bphone As String = ""
            Dim cust_tp_cd As String = ""
            Dim email As String = ""

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("aero_cust_cd") & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Try
                dbConnection.Open()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    fname = dbReader.Item("BILL_FNAME").ToString.Trim
                    If fname & "" = "" Then fname = dbReader.Item("FNAME").ToString.Trim
                    lname = dbReader.Item("BILL_LNAME").ToString.Trim
                    If lname & "" = "" Then lname = dbReader.Item("LNAME").ToString.Trim
                    cust_tp_cd = dbReader.Item("CUST_TP").ToString.Trim
                    addr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                    If addr1 & "" = "" Then addr1 = dbReader.Item("ADDR1").ToString.Trim
                    addr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                    If addr2 & "" = "" Then addr2 = dbReader.Item("ADDR2").ToString.Trim
                    city = dbReader.Item("BILL_CITY").ToString.Trim
                    If city & "" = "" Then city = dbReader.Item("CITY").ToString.Trim
                    state = dbReader.Item("BILL_ST").ToString.Trim
                    If state & "" = "" Then state = dbReader.Item("ST").ToString.Trim
                    zip = dbReader.Item("BILL_ZIP").ToString.Trim
                    If zip & "" = "" Then zip = dbReader.Item("ZIP").ToString.Trim
                    hphone = StringUtils.FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                    bphone = StringUtils.FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)
                    email = dbReader.Item("EMAIL").ToString.Trim
                End If
                dbReader.Close()
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
            ' a single transaction to be able to rollback all changes if something fails
            Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")

            Session("aero_cust_cd") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"),
                                                                 UCase(addr1),
                                                                 Left(UCase(new_cust_lname), 20),
                                                                 Left(UCase(new_cust_fname), 15))
            Try
                dbAtomicConnection.Open()
                dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction()

                ' --- Save Customer to the E1 Database
                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
                sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_CD").Value = Session("aero_cust_cd").ToString.Trim
                dbAtomicCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
                dbAtomicCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
                dbAtomicCommand.Parameters.Add(":FNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":FNAME").Value = UCase(fname)
                dbAtomicCommand.Parameters.Add(":LNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":LNAME").Value = UCase(lname)
                dbAtomicCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_TP").Value = cust_tp_cd
                dbAtomicCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR1").Value = UCase(addr1)
                dbAtomicCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR2").Value = UCase(addr2)
                dbAtomicCommand.Parameters.Add(":CITY", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CITY").Value = UCase(city)
                dbAtomicCommand.Parameters.Add(":ST", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ST").Value = UCase(state)
                dbAtomicCommand.Parameters.Add(":ZIP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ZIP").Value = zip
                dbAtomicCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":HPHONE").Value = hphone
                dbAtomicCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":BPHONE").Value = bphone
                dbAtomicCommand.Parameters.Add(":EMAIL", OracleType.VarChar)
                dbAtomicCommand.Parameters(":EMAIL").Value = UCase(email)
                dbAtomicCommand.ExecuteNonQuery()
                dbAtomicCommand.Parameters.Clear()  'to get it ready for the next statement

                ' --- Update newly created Customer Code to the table in the CRM schema 
                sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("aero_cust_cd") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='NEW'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves all the pending changes.
                dbAtomicCommand.Transaction.Commit()

            Catch ex As Exception
                dbAtomicCommand.Transaction.Rollback()  'reverts any changes made so far
                Throw
            Finally
                dbAtomicCommand.Dispose()
                dbAtomicConnection.Close()
            End Try
        End If
    End Sub
    Function get_cust_name(ByVal p_cust As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader


        Dim sql As String = "SELECT fname||' '||lname nm "
        sql = sql & " from cust "
        sql = sql & " where cust_cd = '" & p_cust & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("nm")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Public Sub Populate_str()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_str_cd.Items.Clear()

        sql = " select store_cd str_cd, store_cd||' - '||store_name name from store "
        sql = sql & " where std_multi_co2.isvalidstr7('" & Session("emp_init") & "',store_cd) = 'Y' "
        sql = sql & " order by 1 "

        srch_str_cd.Items.Insert(0, "-")
        srch_str_cd.Items.FindByText("-").Value = ""
        srch_str_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_str_cd
                .DataSource = ds
                .DataValueField = "str_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Public Sub Populate_mpc_code()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_mpc_code.Items.Clear()

        sql = " select mpc_code cd, mpc_code||' - '||mpc_name name from aimia_mpc_hdr "
        sql = sql & " where cust_mpc_ind = 'Y' "
        sql = sql & " order by 1 "

        srch_mpc_code.Items.Insert(0, "-")
        srch_mpc_code.Items.FindByText("-").Value = ""
        srch_mpc_code.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_mpc_code
                .DataSource = ds
                .DataValueField = "cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Protected Sub UserChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If srch_cre_by.Text & "" <> "" Then
            srch_cre_name.Text = get_user_name(UCase(srch_cre_by.Text))
        End If
    End Sub
    Protected Sub CustChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If srch_cust_cd.Text & "" <> "" Then
            srch_cust_name.Text = get_cust_name(UCase(srch_cust_cd.Text))
        End If
    End Sub
    Function get_store_name(ByVal p_str As String) As String

        Dim v_value As String = ""
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT store_name nm "
        sql = sql & " from store "
        sql = sql & " where store_cd = '" & p_str & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("nm")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function get_mpc_name(ByVal p_mpc As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        If srch_mpc_code.SelectedIndex < 1 Then
            Return ""
        End If

        Dim sql As String = "SELECT mpc_name nm "
        sql = sql & " from aimia_mpc_hdr "
        sql = sql & " where mpc_code = '" & p_mpc & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("nm")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function get_user_name(ByVal p_user As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        If srch_mpc_code.SelectedIndex < 1 Then
            Return ""
        End If

        Dim sql As String = "SELECT fname||' '||lname nm "
        sql = sql & " from emp "
        sql = sql & " where emp_init = '" & p_user & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("nm")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function IfAlreadyExists(ByVal p_e As ASPxDataValidationEventArgs) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        If srch_mpc_code.SelectedIndex < 1 Then
            Return ""
        End If

        Dim sql As String = "SELECT count(*) cnt from so_aeroplan_custmpc_dtl "
        sql = sql & " where cust_cd = '" & p_e.NewValues("CUST_CD") & "' "
        sql = sql & "   and mpc_cd = '" & p_e.NewValues("MPC_CODE") & "'"
        sql = sql & "   and nvl(qualify_pts,0) = nvl(" & p_e.NewValues("QUALIFY_PTS") & " ,0) " &
        sql = sql & "   and nvl(stat_cd,'x') = 'O' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Then
            Return "N"
        ElseIf v_value < 1 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Protected Sub GridView1_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)


        If e.Column.FieldName = "CUST_CD" Then
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        ElseIf e.Column.FieldName = "AEROPLAN_CRD_NUM" Then
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        ElseIf e.Column.FieldName = "STORE_CD" Then
            Dim ds_str As New DataSet 'this is to build a dropdownlist
            ds_str = getStores()
            Dim cb_str As ASPxComboBox = DirectCast(e.Editor, ASPxComboBox)
            cb_str.DataSource = ds_str
            cb_str.ValueField = "str_cd"
            cb_str.TextField = "str_nm"
            cb_str.DataBind()
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        ElseIf e.Column.FieldName = "MPC_CODE" Then
            Dim ds_mpc As New DataSet        'this is to build a dropdownlist
            ds_mpc = getMPC()
            Dim cb_mpc As ASPxComboBox = DirectCast(e.Editor, ASPxComboBox)
            cb_mpc.DataSource = ds_mpc
            cb_mpc.ValueField = "cd"
            cb_mpc.TextField = "nm"
            cb_mpc.DataBind()
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        ElseIf e.Column.FieldName = "QUALIFY_PTS" Then
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        ElseIf e.Column.FieldName = "CMNT" Then
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
            'Req2778
        ElseIf e.Column.FieldName = "AEROPLAN_PURCH_DT" Then
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
            'Req2778 end
        Else
            e.Editor.ReadOnly = True
            e.Editor.ClientEnabled = False
        End If

    End Sub
    Protected Sub GridView1_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)

        If Session("aero_cust_cd") & "" <> "" Then
            e.NewValues("CUST_CD") = Session("aero_cust_cd")
            e.NewValues("CUST_NAME") = get_cust_name(Session("aero_cust_cd"))
        Else
            e.NewValues("CUST_CD") = ""
            e.NewValues("CUST_NAME") = ""
        End If

        e.NewValues("AEROPLAN_CRD_NUM") = ""
        e.NewValues("STORE_CD") = ""
        e.NewValues("AEROPLAN_PURCH_DT") = DateTime.Now
        e.NewValues("CRE_BY") = Session("EMP_INIT")
        e.NewValues("MPC_CODE") = ""
        e.NewValues("QUALIFY_PTS") = ""
        e.NewValues("CONTRACT") = ""
        e.NewValues("STAT_CD") = "O"
        e.NewValues("PROCESSING_DT") = DateTime.Now
        e.NewValues("CUSTMPC_ID") = ""
        e.NewValues("CMNT") = ""


    End Sub
    Protected Sub ASPxGridView1_RowValidating(ByVal sender As Object, ByVal e As ASPxDataValidationEventArgs)

        e.RowError = ""
        Dim v_stat As String = ""

        If Not e.IsNewRow Then
            v_stat = getStat(e.OldValues("CUST_CD"), e.Keys.Item("CUSTMPC_ID"))
        End If

        If v_stat & "" = "" Or isEmpty(v_stat) Then
            'continue
        ElseIf v_stat <> "R" And v_stat <> "O" And v_stat <> "C" Then
            e.RowError = "No updates allowed - status is " & v_stat
            Exit Sub
        End If

        If e.NewValues("CUST_CD") & "" = "" Or isEmpty(e.NewValues("CUST_CD")) Then
            e.RowError = "Please enter a Customer code"
            Exit Sub
        ElseIf isValidCustomer(UCase(e.NewValues("CUST_CD"))) <> "Y" Then
            e.RowError = "Invalid Customer code - please try again"
            Exit Sub
        Else
            e.NewValues("CUST_NAME") = get_cust_name(e.NewValues("CUST_CD"))
        End If

        If e.NewValues("AEROPLAN_CRD_NUM") & "" = "" Or isEmpty(e.NewValues("AEROPLAN_CRD_NUM")) Then
            e.RowError = "Please enter a valid Aeroplan No. "
            Exit Sub
        ElseIf Not IsNumeric(e.NewValues("AEROPLAN_CRD_NUM")) Then
            e.RowError = "Invalid Aeroplan No. - please try again"
            Exit Sub
        ElseIf Len(e.NewValues("AEROPLAN_CRD_NUM")) <> 9 Then
            e.RowError = "invalid Aeroplan No. - please try again"
            Exit Sub
        ElseIf isValidCardNum(e.NewValues("AEROPLAN_CRD_NUM")) <> "Y" Then
            e.RowError = "Invalid Aeroplan No. - please try again"
            Exit Sub
        End If

        'Req2778
        If e.NewValues("AEROPLAN_PURCH_DT") & "" = "" Then
            e.RowError = "Please enter Purchase Date"
            Exit Sub
        ElseIf IsValidPurchaseDt(e.NewValues("AEROPLAN_PURCH_DT"), e.NewValues("MPC_CODE")) <> "Y" Then
            e.RowError = "Invalid Purchase Date - Please try again"
            Exit Sub
        End If
        'Req2778 end

        If e.NewValues("MPC_CODE") & "" = "" Or e.NewValues("MPC_CODE") & "" = "-" Then
            e.RowError = "Please select MPC CODE to continue"
            Exit Sub
        End If

        If e.NewValues("STORE_CD") & "" = "" Or e.NewValues("STORE_CD") & "" = "-" Then
            e.RowError = "Please select a STORE to continue"
            Exit Sub
        End If

        Dim v_max_miles As OracleNumber = getMaxMiles(Left(e.NewValues("MPC_CODE"), 4))
        If e.NewValues("QUALIFY_PTS") & "" = "" Or isEmpty(e.NewValues("QUALIFY_PTS")) Then
            e.RowError = "Please enter a value for MILES - must be equal to or less than " & ToString(v_max_miles)
            Exit Sub
        ElseIf Not IsNumeric(e.NewValues("QUALIFY_PTS")) Then
            e.RowError = "MILES is invalid - please try again"
            Exit Sub
        ElseIf CDec(e.NewValues("QUALIFY_PTS")) > CDec(v_max_miles) Then
            e.RowError = "MILES must be equal to or less than " & v_max_miles.ToString()
            Exit Sub
        End If

        If e.NewValues("CMNT") & "" = "" Or isEmpty(e.NewValues("CMNT")) Then
            e.RowError = "Please enter a Comment"
            Exit Sub
        End If

        Session("aero_cust_cd") = ""
        srch_cust_cd.Text = e.NewValues("CUST_CD").ToString()
        srch_cust_name.Text = e.NewValues("CUST_NAME").ToString()
        srch_mpc_code.SelectedValue = Left(e.NewValues("MPC_CODE"), 4)
        srch_aeroplan_crd_num.Text = e.NewValues("AEROPLAN_CRD_NUM")
        srch_str_cd.SelectedValue = Left(e.NewValues("STORE_CD"), 2)

        e.RowError = ""
        e.Keys.Clear()

    End Sub
    Protected Sub ASPxGridView1_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)

        Dim hasError As Boolean = False
        Dim errorMsg As String = ""
        Dim v_upd_ind As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        '--------------------------------------------------------------------
        ' update aeroplan# on so_aeroplan
        '--------------------------------------------------------------------
        conn.Open()

        sql = "UPDATE SO_AEROPLAN SET "
        sql = sql & " aeroplan_crd_num = '" & e.NewValues("AEROPLAN_CRD_NUM") & "' "
        sql = sql & " ,aeroplan_purch_dt = to_date ('" & FormatDateTime(e.NewValues("AEROPLAN_PURCH_DT"), 2) & "','mm/dd/RRRR') "
        sql = sql & " ,upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString(), 2) & "','mm/dd/RRRR') "
        sql = sql & " ,upd_by = '" & Session("emp_init") & "' "
        sql = sql & " ,store_cd = '" & Left(e.NewValues("STORE_CD"), 2) & "' "
        sql = sql & " where del_doc_num = '" & e.Keys.Item("CUSTMPC_ID") & "' "
        sql = sql & "   and cust_cd     = '" & e.OldValues("CUST_CD") & "' "
        sql = sql & "   and custmpc_id  = '" & e.Keys.Item("CUSTMPC_ID") & "' "

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()
        v_upd_ind = "Y"

        '-----------------------------------------------------------------------------------------
        'ADD DETAIL
        '-----------------------------------------------------------------------------------------

        Dim connstring As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim v_dt As String = getStartDt(Left(e.OldValues("MPC_CODE"), 4))

        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_aeroplan.populateDetail"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_id", OracleType.VarChar)).Value = e.Keys.Item("CUSTMPC_ID")
            myCMD.Parameters.Add(New OracleParameter("p_cus", OracleType.VarChar)).Value = UCase(e.NewValues("CUST_CD"))
            myCMD.Parameters.Add(New OracleParameter("p_mpc", OracleType.VarChar)).Value = Left(e.NewValues("MPC_CODE"), 4)
            myCMD.Parameters.Add(New OracleParameter("p_mpc_strt_dt", OracleType.DateTime)).Value = FormatDateTime(v_dt, DateFormat.ShortDate)
            myCMD.Parameters.Add(New OracleParameter("p_pts", OracleType.Number)).Value = CInt(e.NewValues("QUALIFY_PTS"))
            myCMD.Parameters.Add(New OracleParameter("p_cmnt", OracleType.VarChar)).Value = UCase(e.NewValues("CMNT"))

            myCMD.ExecuteNonQuery()

            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Error in creating Aeroplan Detail ")
        End Try

        '-----------------------------------------------------------------------------------------

        If search_criteria_exists() <> "Y" Then
            srch_cust_cd.Text = e.NewValues("CUST_CD")
        End If
        GridView1.CancelEdit()
        populate_gridview()
        GridView1.DataBind()
        e.Cancel = True

        If v_upd_ind = "Y" Then
            GridView1.Settings.ShowTitlePanel = True
            GridView1.SettingsText.Title = "** CUSTOMER RECORD IS UPDATED"
        End If

    End Sub
    Protected Sub ASPxGridView1_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)

        Dim hasError As Boolean = False
        Dim errorMsg As String = ""
        Dim v_upd_ind As String = ""

        '---------------------------------------------------------------------------------------------
        '---------------------------------------- insert new SO_AEROPLAN ----------------------------
        '---------------------------------------------------------------------------------------------

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""

        g_custmpc_id = getCustMPC()
        Dim v_dummy_doc As String = g_custmpc_id.ToString()

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'Req2778 -purch_dt change from trunc(sysdate)
        sql = "Insert into so_aeroplan " &
              "(del_doc_num,aeroplan_crd_num,aeroplan_purch_dt,cust_cd,cre_dt,cre_by,store_cd, custmpc_id) " &
              " values ('" & v_dummy_doc & "','" & e.NewValues("AEROPLAN_CRD_NUM") & "' " &
              ",to_date ('" & FormatDateTime(e.NewValues("AEROPLAN_PURCH_DT"), 2) & "','mm/dd/RRRR'), '" & UCase(e.NewValues("CUST_CD")) & "'" &
              " , trunc(sysdate), '" & Session("emp_init") & "','" & Left(e.NewValues("STORE_CD"), 2) & "'" &
              " , '" & g_custmpc_id & "' )"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()

        '-----------------------------------------------------------------------------------------
        'ADD DETAIL   - 
        '-----------------------------------------------------------------------------------------
        Dim connstring As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim v_dt As String = getStartDt(Left(e.NewValues("MPC_CODE"), 4))

        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_aeroplan.populateDetail"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_id", OracleType.VarChar)).Value = g_custmpc_id
            myCMD.Parameters.Add(New OracleParameter("p_cus", OracleType.VarChar)).Value = UCase(e.NewValues("CUST_CD"))
            myCMD.Parameters.Add(New OracleParameter("p_mpc", OracleType.VarChar)).Value = Left(e.NewValues("MPC_CODE"), 4)
            myCMD.Parameters.Add(New OracleParameter("p_mpc_strt_dt", OracleType.DateTime)).Value = FormatDateTime(v_dt, DateFormat.ShortDate)
            myCMD.Parameters.Add(New OracleParameter("p_pts", OracleType.Number)).Value = e.NewValues("QUALIFY_PTS")
            myCMD.Parameters.Add(New OracleParameter("p_cmnt", OracleType.VarChar)).Value = UCase(e.NewValues("CMNT"))

            myCMD.ExecuteNonQuery()

            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Error in creating Aeroplan Detail ")
        End Try
        '-----------------------------------------------------------------------------------------

        e.Cancel = True
        If search_criteria_exists() <> "Y" Then
            srch_cust_cd.Text = e.NewValues("CUST_CD")
        End If
        populate_gridview()
        GridView1.CancelEdit()
        GridView1.DataBind()

        GridView1.Settings.ShowTitlePanel = True
        GridView1.SettingsText.Title = "*** CUSTOMER RECORD IS ADDED"

    End Sub


    Protected Sub ASPxGridView1_Rowdeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "delete so_aeroplan " &
              " where del_doc_num = '" & e.Keys.Item("CUSTMPC_ID") & "' " &
              "   and cust_cd = '" & e.Keys.Item("CUST_CD") & "' " &
              "   AND custmpc_id = '" & e.Keys.Item("CUSTMPC_ID") & "' "

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw New Exception("Error deleting Header record. Please try again.")
        End Try
        conn.Close()

        '------------ delete detail

        conn.Open()

        sql = "delete so_aeroplan_custmpc_dtl " &
              " where cust_cd = '" & e.Keys.Item("CUST_CD") & "' " &
              "   AND custmpc_id = '" & e.Keys.Item("CUSTMPC_ID") & "' "

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw New Exception("Error deleting Detail record. Please try again.")
        End Try

        conn.Close()

        If search_criteria_exists() <> "Y" Then
            srch_cust_cd.Text = e.Keys.Item("CUST_CD")
        End If
        e.Cancel = True
        GridView1.CancelEdit()
        populate_gridview()
        GridView1.DataBind()

        GridView1.Settings.ShowTitlePanel = True
        GridView1.SettingsText.Title = "* CUSTOMER RECORD IS DELETED"

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    Protected Sub GridView1_CommandButtonInitialize(sender As Object, e As ASPxGridViewCommandButtonEventArgs)

        If e.VisibleIndex < 0 Then
            Exit Sub
        End If

        Dim v_cust As String = GridView1.GetRowValues(e.VisibleIndex, "CUST_CD")
        Dim v_custmpc_id As Integer = GridView1.GetRowValues(e.VisibleIndex, "CUSTMPC_ID")

        Dim V_STAT As String = getStat(v_cust, v_custmpc_id)
        Dim v_str As String = getCustMpcStr(v_cust, v_custmpc_id)

        If V_STAT & "" = "O" Or V_STAT & "" = "R" Or V_STAT & "" = "" Then
            If isValidStore(v_str) = "Y" Then
                If e.ButtonType = ColumnCommandButtonType.Edit Or ColumnCommandButtonType.Delete Then
                    e.Visible = True
                End If
            Else
                If e.ButtonType = ColumnCommandButtonType.Edit Or ColumnCommandButtonType.Delete Then
                    e.Visible = False
                End If
            End If
        Else
            If e.ButtonType = ColumnCommandButtonType.Edit Or ColumnCommandButtonType.Delete Then
                e.Visible = False
            End If
        End If


    End Sub
    Function getErr(ByVal p_cd As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT des err "
        sql = sql & " from aeroplan_errcode "
        sql = sql & " where err_code = '" & p_cd & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("err")
            Else
                v_value = "Error Code Not Found"
            End If
        Catch
            v_value = "Error Code Not Found"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "Error Code Not Found"
        Else
            Return v_value
        End If

    End Function
    Function getProcessCd(ByVal p_custmpc_id As Integer) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT processing_cd cd "
        sql = sql & " from so_aeroplan_custmpc_dtl "
        sql = sql & " where custmpc_id  = '" & p_custmpc_id & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cd")
            Else
                v_value = "x"
            End If
        Catch
            v_value = "x"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "x"
        Else
            Return v_value
        End If

    End Function
    Protected Sub ASPxGridView1_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As ASPxGridViewTableDataCellEventArgs) Handles GridView1.HtmlDataCellPrepared
        Dim v_err As String = ""

        If e.VisibleIndex < 0 Then
            Exit Sub
        End If

        If e.DataColumn.FieldName = "STAT_CD" Then
            If e.CellValue & "" = "R" Then
                e.Cell.BackColor = System.Drawing.Color.LightCyan
                Dim v_custmpc_id As Integer = GridView1.GetRowValues(e.VisibleIndex, "CUSTMPC_ID")
                Dim v_process_cd As String = getProcessCd(v_custmpc_id)
                If v_process_cd & "" <> "" Then
                    v_err = getErr(v_process_cd)
                    e.Cell.Attributes.Add("onmouseover", "alert('" & v_err & "')")
                End If
            End If

        End If
    End Sub
    Protected Sub ASPxGridView9_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As ASPxGridViewTableDataCellEventArgs) Handles GridView9.HtmlDataCellPrepared
        Dim v_err As String = ""

        If e.VisibleIndex < 0 Then
            Exit Sub
        End If

        If e.DataColumn.FieldName = "STAT_CD" Then
            If e.CellValue & "" = "R" Then
                e.Cell.BackColor = System.Drawing.Color.LightCyan
                Dim v_custmpc_id As Integer = GridView9.GetRowValues(e.VisibleIndex, "CUSTMPC_ID")
                Dim v_process_cd As String = getProcessCd(v_custmpc_id)
                If v_process_cd & "" <> "" Then
                    v_err = getErr(v_process_cd)
                    e.Cell.Attributes.Add("onmouseover", "alert('" & v_err & "')")
                End If
            End If

        End If
    End Sub



End Class
