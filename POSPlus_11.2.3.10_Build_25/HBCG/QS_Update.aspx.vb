Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic

Partial Class QS_Update
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


    End Sub

    Private Sub bindgrid()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim MyTable As DataTable
        Dim numrows As Integer
        ds = New DataSet
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        GridView1.DataSource = ""

        conn2.Open()

        sql = "SELECT EMP_CD, STORE_CD, QS_STATUS, CUST_CD, TRAN_DT FROM QS_AUDIT WHERE TRAN_DT BETWEEN TO_DATE('" & txt_sdate.Text & " 00:00','mm/dd/yyyy hh24:mi') AND "
        sql = sql & "TO_DATE('" & txt_edate.Text & " 23:59','mm/dd/yyyy hh24:mi') AND QS_STATUS='N' AND CUST_CD IS NOT NULL"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn2)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read) Then
                GridView1.Visible = True
                GridView1.DataSource = dv
                GridView1.DataBind()
            Else
                GridView1.Visible = False
            End If
            'Close Connection 
            MyDataReader.Close()
            conn2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Request("LEAD") = "TRUE" Then
            Page.MasterPageFile = "~/Regular.Master"
        End If
        If Session("MP") & "" <> "" Then
            Me.MasterPageFile = "~/MasterPages/NoWizard2.Master"
        End If

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If IsDate(txt_sdate.Text) And IsDate(txt_edate.Text) Then
            bindgrid()
        End If

    End Sub

    Protected Sub GridView1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView1.ItemDataBound

        If Not IsNothing(e.Item.Cells(1).Text) And e.Item.Cells(1).Text <> "Customer Code" Then
            Dim txt_qty As TextBox = CType(e.Item.FindControl("txt_ge_acct"), TextBox)
            Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim sql As String
            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn2.Open()

            'Use the provider code setup in GP Parms to get the acct info from cust_asp table instead of hardcoding it here
            Dim providerCd As String = GetQuickCreditProviderCode()
            sql = "SELECT ACCT_CD FROM CUST_ASP" &
                        " WHERE AS_CD = '" & providerCd &
                        "' AND CUST_CD='" & e.Item.Cells(1).Text & "'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read) Then
                    If Not IsNothing(txt_qty) Then
                        Dim txt_update As Button = CType(e.Item.FindControl("txt_update"), Button)
                        txt_update.Enabled = True
                        txt_qty.Text = "Account Found"
                    End If
                Else
                    If Not IsNothing(txt_qty) Then
                        txt_qty.Text = ""
                    End If
                End If
                'Close Connection 
                MyDataReader.Close()
                conn2.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try

        End If

    End Sub


    Private Function GetQuickCreditProviderCode() As String
        'Returns the provider that has been setup in  gp_parms to do a GE_QUICK_CREDIT. This code
        ' must match the entry in  the ASP table for that provider.

        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim providerCd As String = ""
        Dim sql As String = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        Try
            ' bt_misc_wrapper.get_gp_parm('PTS_POS+',  'GE_QUICK_CREDIT');
            sql = "bt_misc_wrapper.get_gp_parm"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql.CommandType = CommandType.StoredProcedure

            objsql.Parameters.Add("returnValue", OracleType.VarChar).Direction = ParameterDirection.ReturnValue
            objsql.Parameters.Add("key_i", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("parm_i", OracleType.VarChar).Direction = ParameterDirection.Input

            objsql.Parameters("key_i").Value = "PTS_POS+"
            objsql.Parameters("parm_i").Value = "GE_QUICK_CREDIT"

            objsql.ExecuteNonQuery()

            'get the asp-cd setup for GE quick credit from the API call
            providerCd = objsql.Parameters(0).Value ' to use this by number, starts at zero and is by add list sequence above

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        Return providerCd.Trim
    End Function

    Protected Sub Update_Approved(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim textdata As Button = CType(sender, Button)
        Dim dgItem As DataGridItem = CType(textdata.NamingContainer, DataGridItem)

        If textdata.Text & "" <> "" Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update qs_audit set qs_status='A', last_update=SYSDATE where cust_cd='" & dgItem.Cells(1).Text & "'"
            End With
            cmdDeleteItems.ExecuteNonQuery()
            bindgrid()
        End If
        conn.Close()

    End Sub

End Class
