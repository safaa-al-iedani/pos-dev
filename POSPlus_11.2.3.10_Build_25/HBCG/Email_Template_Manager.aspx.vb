Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Email_Template_Manager
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            admin_form.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If

        lbl_error.Text = ""

        If Not IsPostBack Then
            Update_Templates()
        End If

    End Sub

    Public Sub Update_Templates(Optional ByVal row_id As String = "")

        cbo_template.ClearSelection()
        Reset_Form()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String

        conn.Open()
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader
        Dim ds2 As DataSet
        Dim oAdp2 As OracleDataAdapter
        Dim dv2 As DataView
        ds2 = New DataSet

        sql = "SELECT * FROM EMAIL_TEMPLATES WHERE EMAIL_PERM='Y' "
        If row_id & "" <> "" Then
            sql = sql & "AND EMAIL_ID=" & row_id & " "
        End If
        sql = sql & "ORDER BY EMAIL_DESC"
        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp2 = DisposablesManager.BuildOracleDataAdapter(objSql2)
        oAdp2.Fill(ds2)

        dv2 = ds2.Tables(0).DefaultView
        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                If row_id & "" = "" Then
                    cbo_template.DataSource = dv2
                    cbo_template.DataValueField = "EMAIL_ID"
                    cbo_template.DataTextField = "EMAIL_DESC"
                    cbo_template.DataBind()
                End If
                Dim i As Integer
                Dim email_from As String = MyDataReader2.Item("EMAIL_FROM").ToString
                For i = 0 To chk_list_from.Items.Count - 1
                    If InStr(email_from, "<SLSP1_EMAIL>") > 0 Then
                        email_from = Replace(email_from, "<SLSP1_EMAIL>", "")
                        chk_list_from.Items(0).Selected = True
                    End If
                    If InStr(email_from, "<SLSP2_EMAIL>") > 0 Then
                        email_from = Replace(email_from, "<SLSP2_EMAIL>", "")
                        chk_list_from.Items(1).Selected = True
                    End If
                    If email_from.ToString.Trim & "" <> "" Then
                        txt_from.Text = email_from.ToString.Trim
                        chk_list_from.Items(5).Selected = True
                    End If
                Next
                Dim email_to As String = MyDataReader2.Item("EMAIL_TO").ToString
                For i = 0 To chk_list_to.Items.Count - 1
                    If InStr(email_to, "<SLSP1_EMAIL>") > 0 Then
                        email_to = Replace(email_to, "<SLSP1_EMAIL>", "")
                        chk_list_to.Items(0).Selected = True
                    End If
                    If InStr(email_to, "<SLSP2_EMAIL>") > 0 Then
                        email_to = Replace(email_to, "<SLSP2_EMAIL>", "")
                        chk_list_to.Items(1).Selected = True
                    End If
                    If InStr(email_to, "<CUST_EMAIL>") > 0 Then
                        email_to = Replace(email_to, "<CUST_EMAIL>", "")
                        chk_list_to.Items(2).Selected = True
                    End If
                    If InStr(email_to, "<CUST_SHIP_EMAIL>") > 0 Then
                        email_to = Replace(email_to, "<CUST_SHIP_EMAIL>", "")
                        chk_list_to.Items(3).Selected = True
                    End If
                    If InStr(email_to, "<ALLOW_FREEFORM>") > 0 Then
                        email_to = Replace(email_to, "<ALLOW_FREEFORM>", "")
                        chk_list_to.Items(4).Selected = True
                    End If
                    If InStr(email_to, "<DONT_ALLOW>") > 0 Then
                        email_to = Replace(email_to, "<DONT_ALLOW>", "")
                        chk_list_to.Items(5).Selected = True
                        chk_list_to.Items(0).Selected = False
                        chk_list_to.Items(1).Selected = False
                        chk_list_to.Items(2).Selected = False
                        chk_list_to.Items(3).Selected = False
                        chk_list_to.Items(4).Selected = False
                    End If
                    If email_to.ToString.Trim & "" <> "" Then
                        txt_to.Text = email_to.ToString.Trim
                        chk_list_to.Items(5).Selected = True
                    End If
                Next
                Dim email_cc As String = MyDataReader2.Item("EMAIL_CC").ToString
                For i = 0 To chk_list_cc.Items.Count - 1
                    If InStr(email_cc, "<SLSP1_EMAIL>") > 0 Then
                        email_cc = Replace(email_cc, "<SLSP1_EMAIL>", "")
                        chk_list_cc.Items(0).Selected = True
                    End If
                    If InStr(email_cc, "<SLSP2_EMAIL>") > 0 Then
                        email_cc = Replace(email_cc, "<SLSP2_EMAIL>", "")
                        chk_list_cc.Items(1).Selected = True
                    End If
                    If InStr(email_cc, "<CUST_EMAIL>") > 0 Then
                        email_cc = Replace(email_cc, "<CUST_EMAIL>", "")
                        chk_list_cc.Items(2).Selected = True
                    End If
                    If InStr(email_cc, "<CUST_SHIP_EMAIL>") > 0 Then
                        email_cc = Replace(email_cc, "<CUST_SHIP_EMAIL>", "")
                        chk_list_cc.Items(3).Selected = True
                    End If
                    If InStr(email_cc, "<ALLOW_FREEFORM>") > 0 Then
                        email_cc = Replace(email_cc, "<ALLOW_FREEFORM>", "")
                        chk_list_cc.Items(4).Selected = True
                    End If
                    If InStr(email_cc, "<DONT_ALLOW>") > 0 Then
                        email_cc = Replace(email_cc, "<DONT_ALLOW>", "")
                        chk_list_cc.Items(5).Selected = True
                        chk_list_cc.Items(0).Selected = False
                        chk_list_cc.Items(1).Selected = False
                        chk_list_cc.Items(2).Selected = False
                        chk_list_cc.Items(3).Selected = False
                        chk_list_cc.Items(4).Selected = False
                    End If
                    If email_cc.ToString.Trim & "" <> "" Then
                        txt_cc.Text = email_cc.ToString.Trim
                        chk_list_cc.Items(5).Selected = True
                    End If
                Next
                Dim email_bcc As String = MyDataReader2.Item("EMAIL_BCC").ToString
                For i = 0 To chk_list_bcc.Items.Count - 1
                    If InStr(email_bcc, "<SLSP1_EMAIL>") > 0 Then
                        email_bcc = Replace(email_bcc, "<SLSP1_EMAIL>", "")
                        chk_list_bcc.Items(0).Selected = True
                    End If
                    If InStr(email_bcc, "<SLSP2_EMAIL>") > 0 Then
                        email_bcc = Replace(email_bcc, "<SLSP2_EMAIL>", "")
                        chk_list_bcc.Items(1).Selected = True
                    End If
                    If InStr(email_bcc, "<CUST_EMAIL>") > 0 Then
                        email_bcc = Replace(email_bcc, "<CUST_EMAIL>", "")
                        chk_list_bcc.Items(2).Selected = True
                    End If
                    If InStr(email_bcc, "<CUST_SHIP_EMAIL>") > 0 Then
                        email_bcc = Replace(email_bcc, "<CUST_SHIP_EMAIL>", "")
                        chk_list_bcc.Items(3).Selected = True
                    End If
                    If InStr(email_bcc, "<ALLOW_FREEFORM>") > 0 Then
                        email_bcc = Replace(email_bcc, "<ALLOW_FREEFORM>", "")
                        chk_list_bcc.Items(4).Selected = True
                    End If
                    If InStr(email_bcc, "<DONT_ALLOW>") > 0 Then
                        email_bcc = Replace(email_bcc, "<DONT_ALLOW>", "")
                        chk_list_bcc.Items(5).Selected = True
                        chk_list_bcc.Items(0).Selected = False
                        chk_list_bcc.Items(1).Selected = False
                        chk_list_bcc.Items(2).Selected = False
                        chk_list_bcc.Items(3).Selected = False
                        chk_list_bcc.Items(4).Selected = False
                    End If
                    If email_bcc.ToString.Trim & "" <> "" Then
                        txt_bcc.Text = email_bcc.ToString.Trim
                        chk_list_bcc.Items(5).Selected = True
                    End If
                Next
                Dim subject As String = MyDataReader2.Item("EMAIL_SUBJECT").ToString
                Dim body As String = MyDataReader2.Item("EMAIL_BODY").ToString
                If InStr(subject, "<ALLOW_FREEFORM>") > 0 Then
                    subject = Replace(subject, "<ALLOW_FREEFORM>", "")
                    chk_subject_allow_ff.Checked = True
                End If
                If InStr(body, "<ALLOW_FREEFORM>") > 0 Then
                    body = Replace(body, "<ALLOW_FREEFORM>", "")
                    chk_body_allow_ff.Checked = True
                End If
                txt_subject.Text = subject
                body_editor.Html = body
            Else
                Reset_Form()
            End If
            'Close Connection 
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        If row_id & "" <> "" Then cbo_template.SelectedValue = row_id
        conn.Close()

    End Sub

    Public Sub Reset_Form()

        Dim i As Integer
        For i = 0 To chk_list_to.Items.Count - 1
            chk_list_to.Items(i).Selected = False
        Next
        For i = 0 To chk_list_from.Items.Count - 1
            chk_list_from.Items(i).Selected = False
        Next
        For i = 0 To chk_list_cc.Items.Count - 1
            chk_list_cc.Items(i).Selected = False
        Next
        For i = 0 To chk_list_bcc.Items.Count - 1
            chk_list_bcc.Items(i).Selected = False
        Next
        chk_subject_allow_ff.Checked = False
        chk_body_allow_ff.Checked = False
        txt_subject.Text = ""
        body_editor.Html = ""
        txt_from.Text = ""
        txt_to.Text = ""
        txt_cc.Text = ""
        txt_bcc.Text = ""

    End Sub

    Protected Sub chk_list_bcc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If chk_list_bcc.Items(5).Selected = True Then
            Dim values As String = String.Empty
            Dim i As Integer
            For i = 0 To chk_list_bcc.Items.Count - 1
                If chk_list_bcc.Items(i).Selected And chk_list_bcc.Items(i).Value <> "DONT_ALLOW" Then
                    chk_list_bcc.Items(i).Selected = False
                End If
            Next
        End If

    End Sub

    Protected Sub chk_list_cc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If chk_list_bcc.Items(5).Selected = True Then
            Dim values As String = String.Empty
            Dim i As Integer
            For i = 0 To chk_list_bcc.Items.Count - 1
                If chk_list_bcc.Items(i).Selected And chk_list_bcc.Items(i).Value <> "DONT_ALLOW" Then
                    chk_list_bcc.Items(i).Selected = False
                End If
            Next
        End If

    End Sub

    Protected Sub btn_add_new_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        txt_template_name.Text = ""
        ASPxPopupControl1.ShowOnPageLoad = True

    End Sub

    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        txt_template_name.Text = ""
        ASPxPopupControl1.ShowOnPageLoad = False

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        conn.Open()
        Dim objSql2 As OracleCommand

        Dim email_from As String = String.Empty
        Dim i As Integer
        For i = 0 To chk_list_from.Items.Count - 1
            If chk_list_from.Items(i).Selected Then
                email_from = email_from + "<" + chk_list_from.Items(i).Value + ">"
            End If
        Next
        email_from = Replace(email_from, "<OTHER>", txt_from.Text)

        Dim email_cc As String = String.Empty
        For i = 0 To chk_list_cc.Items.Count - 1
            If chk_list_cc.Items(i).Selected Then
                email_cc = email_cc + "<" + chk_list_cc.Items(i).Value + ">"
            End If
        Next
        email_cc = Replace(email_cc, "<OTHER>", txt_cc.Text)

        Dim email_to As String = String.Empty
        For i = 0 To chk_list_to.Items.Count - 1
            If chk_list_to.Items(i).Selected Then
                email_to = email_to + "<" + chk_list_to.Items(i).Value + ">"
            End If
        Next
        email_to = Replace(email_to, "<OTHER>", txt_to.Text)

        Dim email_bcc As String = String.Empty
        For i = 0 To chk_list_bcc.Items.Count - 1
            If chk_list_bcc.Items(i).Selected Then
                email_bcc = email_bcc + "<" + chk_list_bcc.Items(i).Value + ">"
            End If
        Next
        email_bcc = Replace(email_bcc, "<OTHER>", txt_bcc.Text)

        Dim email_subject As String = String.Empty
        email_subject = txt_subject.Text
        If chk_subject_allow_ff.Checked = True Then
            email_subject = email_subject & "<ALLOW_FREEFORM>"
        End If

        Dim email_body As String = String.Empty
        email_body = body_editor.Html
        If chk_body_allow_ff.Checked = True Then
            email_body = email_body & "<ALLOW_FREEFORM>"
        End If

        If email_body & "" = "" Then
            lbl_error.Text = "You must type text in the body or allow freeform entry"
            Exit Sub
        ElseIf email_from & "" = "" Then
            lbl_error.Text = "You must select at least one address in the From field or allow freeform entry"
            Exit Sub
        ElseIf email_to & "" = "" Then
            lbl_error.Text = "You must select at least one address in the To field or allow freeform entry"
            Exit Sub
        ElseIf email_cc & "" = "" Then
            lbl_error.Text = "You must select at least one address in the CC field or allow freeform entry"
            Exit Sub
        ElseIf email_bcc & "" = "" Then
            lbl_error.Text = "You must select at least one address in the BCC field or allow freeform entry"
            Exit Sub
        ElseIf email_subject & "" = "" Then
            lbl_error.Text = "You must enter something in the Subject field or allow freeform entry"
            Exit Sub
        ElseIf cbo_template.SelectedValue & "" = "" Then
            lbl_error.Text = "You must select a template to save your changes to"
            Exit Sub
        End If

        Try
            sql = "UPDATE EMAIL_TEMPLATES SET EMAIL_FROM=:EMAIL_FROM, EMAIL_TO=:EMAIL_TO, EMAIL_CC=:EMAIL_CC, EMAIL_BCC=:EMAIL_BCC, "
            sql = sql & "EMAIL_SUBJECT=:EMAIL_SUBJECT, EMAIL_BODY=:EMAIL_BODY, EMAIL_PERM='Y' WHERE EMAIL_ID=" & cbo_template.SelectedValue

            objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql2.Parameters.Add(":EMAIL_FROM", OracleType.VarChar)
            objSql2.Parameters(":EMAIL_FROM").Value = email_from
            objSql2.Parameters.Add(":EMAIL_TO", OracleType.VarChar)
            objSql2.Parameters(":EMAIL_TO").Value = email_to
            objSql2.Parameters.Add(":EMAIL_CC", OracleType.VarChar)
            objSql2.Parameters(":EMAIL_CC").Value = email_cc
            objSql2.Parameters.Add(":EMAIL_BCC", OracleType.VarChar)
            objSql2.Parameters(":EMAIL_BCC").Value = email_bcc
            objSql2.Parameters.Add(":EMAIL_SUBJECT", OracleType.VarChar)
            objSql2.Parameters(":EMAIL_SUBJECT").Value = email_subject
            objSql2.Parameters.Add(":EMAIL_BODY", OracleType.VarChar)
            objSql2.Parameters(":EMAIL_BODY").Value = email_body

            objSql2.ExecuteNonQuery()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        lbl_error.Text = "Changes Saved"

        conn.Close()

    End Sub

    Protected Sub btn_save_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        conn.Open()
        Dim objSql2 As OracleCommand

        sql = "INSERT INTO EMAIL_TEMPLATES (EMAIL_DESC, EMAIL_PERM) VALUES(:EMAIL_DESC,'Y')"
        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql2.Parameters.Add(":EMAIL_DESC", OracleType.VarChar)
        objSql2.Parameters(":EMAIL_DESC").Value = txt_template_name.Text
        objSql2.ExecuteNonQuery()
        conn.Close()
        ASPxPopupControl1.ShowOnPageLoad = False
        Update_Templates()
        Update_Templates(cbo_template.Items.FindByText(txt_template_name.Text).Value)

    End Sub

    Protected Sub cbo_template_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_template.SelectedIndexChanged

        Update_Templates(cbo_template.SelectedValue)

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class
