Imports System.Data.OracleClient
Imports System.IO
Imports HBCG_Utils

Partial Class ASP_Promo_Admin
    Inherits POSBasePage

    Public Sub bindgrid()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds = New DataSet

        Gridview1.DataSource = ""

        Gridview1.Visible = True
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT AS_CD, PROMO_CD, INVOICE_NAME, INVOICE_NAME_WDR, AS_NAME, PROMO_DES FROM PROMO_ADDENDUMS ORDER BY AS_NAME, PROMO_DES"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If

        If Not IsPostBack Then
            'Locate promotions added to the system and automatically add them to the the table
            Find_New_Promos()
            bindgrid()
        End If

    End Sub

    Public Sub Find_New_Promos()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim sql As String
        Dim promo_listing As String = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader

        conn2.Open()

        sql = "select as_cd, promo_cd from PROMO_ADDENDUMS"

        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                promo_listing = promo_listing & MyDataReader2.Item("AS_CD").ToString & "|" & MyDataReader2.Item("PROMO_CD").ToString & vbCrLf
            Loop
            MyDataReader2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

        conn.Open()
        sql = "SELECT a.AS_CD, a.PROMO_CD, b.NAME, a.DES FROM ASP_PROMO a, ASP b WHERE a.AS_CD=b.AS_CD ORDER BY AS_CD, PROMO_CD"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read
                If InStr(UCase(promo_listing), UCase(MyDataReader.Item("AS_CD").ToString & "|" & MyDataReader.Item("PROMO_CD").ToString)) < 1 Then
                    sql = "INSERT INTO PROMO_ADDENDUMS (AS_CD, PROMO_CD, AS_NAME, PROMO_DES) VALUES('" & MyDataReader.Item("AS_CD").ToString & "','" & MyDataReader.Item("PROMO_CD").ToString & "','" & MyDataReader.Item("NAME").ToString & "','" & MyDataReader.Item("DES").ToString & "')"
                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                    objSql2.ExecuteNonQuery()
                    lbl_msg.Visible = True
                    lbl_msg.Text = lbl_msg.Text & MyDataReader.Item("NAME").ToString & " | " & MyDataReader.Item("DES").ToString & "<br />"
                End If
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try
        conn.Close()
        conn2.Close()

    End Sub

    'Public Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

    '    Dim conn As New OracleConnection
    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As OracleDataReader

    '    conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '    conn.Open()

    '    sql = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

    '    'Set SQL OBJECT 
    '    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

    '    Try
    '        MyDataReader = objSql.ExecuteReader
    '        If (MyDataReader.Read()) Then
    '            If MyDataReader.Item("EMP_CD") & "" <> "" Then
    '                Authorize_User = True
    '            Else
    '                Authorize_User = False
    '            End If
    '        Else
    '            Authorize_User = False
    '        End If
    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try

    'End Function

    Protected Sub PopulateFiles(ByVal sender As Object, ByVal e As DataGridItemEventArgs)

        Dim myGrid As DropDownList = e.Item.Cells(1).FindControl("drpAddendum")
        Dim myGrid2 As DropDownList = e.Item.Cells(1).FindControl("drp_WDR_Addendum")
        Dim invoice_name As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As New DataSet
        Dim MyDataReader As OracleDataReader
        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        'Set SQL OBJECT 

        If Not myGrid Is Nothing Then
            invoice_name = e.Item.Cells(4).Text
            sql = "SELECT USER_FORMAT_CD, DES FROM USER_FORMAT WHERE ORIGIN_CD='IPRT' ORDER BY DES"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            With myGrid
                .DataSource = ds
                .DataValueField = "USER_FORMAT_CD"
                .DataTextField = "DES"
                .DataBind()
            End With
            myGrid.Items.Insert(0, "")
            myGrid.Items.FindByText("").Value = ""
            myGrid.SelectedValue = e.Item.Cells(6).Text
            With myGrid2
                .DataSource = ds
                .DataValueField = "USER_FORMAT_CD"
                .DataTextField = "DES"
                .DataBind()
            End With
            myGrid2.Items.Insert(0, "")
            myGrid2.Items.FindByText("").Value = ""
            myGrid2.SelectedValue = e.Item.Cells(7).Text
        End If
        conn.Close()

    End Sub
    Protected Sub DoItemSave(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim drpfile As DropDownList
        Dim strFileName As String
        Dim drpfile_wdr As DropDownList
        Dim strFileName_wdr As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdUpdate As OracleCommand = DisposablesManager.BuildOracleCommand


        drpfile = e.Item.FindControl("drpAddendum")
        strFileName = drpfile.SelectedItem.Value
        drpfile_wdr = e.Item.FindControl("drp_wdr_Addendum")
        strFileName_wdr = drpfile_wdr.SelectedItem.Value

        ' Update TempValue to DB

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        With cmdUpdate
            .Connection = conn
            .CommandText = "UPDATE PROMO_ADDENDUMS SET INVOICE_NAME_WDR = :FileName, INVOICE_NAME = :InvFileName  WHERE AS_CD = AS_CD AND PROMO_CD=:PROMO_CD"
            .Parameters.Add(":FileName", OracleType.VarChar)
            .Parameters(":FileName").Value = strFileName_wdr
            .Parameters.Add(":InvFileName", OracleType.VarChar)
            .Parameters(":InvFileName").Value = strFileName
            .Parameters.Add(":AS_CD", OracleType.VarChar)
            .Parameters(":AS_CD").Value = e.Item.Cells(1).Text
            .Parameters.Add(":PROMO_CD", OracleType.VarChar)
            .Parameters(":PROMO_CD").Value = e.Item.Cells(2).Text
            .ExecuteNonQuery()
        End With

        cmdUpdate.Dispose()
        conn.Close()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
