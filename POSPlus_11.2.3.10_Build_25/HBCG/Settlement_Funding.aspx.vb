Imports System.Data
Imports System.Data.OracleClient

Partial Class Settlement_Funding
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim trn_tp_cd As String = ""
        Dim x As Integer = 1

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "UPDATE ASP_TRN SET STAT_CD='F' WHERE STAT_CD='P' AND MEDIUM_TP_CD='GDR'"
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql2.ExecuteNonQuery()

        sql = "select * from asp_trn where medium_tp_cd<>'GDR' and stat_cd='P'"
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        txt_results.Text = ""

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            Do While MyDataReader.Read()
                Select Case MyDataReader.Item("MEDIUM_TP_CD").ToString
                    Case "SAL"
                        trn_tp_cd = "PMT"
                    Case "MDB"
                        trn_tp_cd = "PMT"
                    Case "MCR"
                        trn_tp_cd = "R"
                    Case "CRM"
                        trn_tp_cd = "R"
                End Select

                sql = "insert into ar_trn "
                sql = sql & "(co_cd, cust_cd, ivc_cd, origin_store, pmt_store "
                sql = sql & ",trn_tp_cd, amt, post_dt, adj_ivc_cd "
                sql = sql & ",stat_cd, ar_tp, emp_cd_cshr, emp_cd_op, mop_cd "
                sql = sql & ",origin_cd, csh_dwr_cd, create_dt) "
                sql = sql & "values "
                sql = sql & "('LEV', :CUST_CD, :IVC_CD, :ORIGIN_STORE, :ORIGIN_STORE "
                sql = sql & ",:TRN_TP_CD, abs(:AMT), trunc(SYSDATE), :ADJ_IVC_CD "
                sql = sql & ",'T', 'O', 'SYS', 'SYS' "
                sql = sql & ",'GE', 'HBSET', '01', trunc(sysdate)) "
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)

                objSql2.Parameters.Add(":CUST_CD", OracleType.VarChar)
                objSql2.Parameters.Add(":IVC_CD", OracleType.VarChar)
                objSql2.Parameters.Add(":ORIGIN_STORE", OracleType.VarChar)
                objSql2.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
                objSql2.Parameters.Add(":AMT", OracleType.Number)
                objSql2.Parameters.Add(":ADJ_IVC_CD", OracleType.VarChar)

                objSql2.Parameters(":CUST_CD").Value = MyDataReader.Item("CUST_CD").ToString
                If MyDataReader.Item("MEDIUM_TP_CD").ToString = "SAL" Then
                    objSql2.Parameters(":IVC_CD").Value = MyDataReader.Item("DEL_DOC_NUM").ToString
                Else
                    objSql2.Parameters(":IVC_CD").Value = System.DBNull.Value
                End If
                objSql2.Parameters(":ORIGIN_STORE").Value = MyDataReader.Item("STORE_CD").ToString
                objSql2.Parameters(":TRN_TP_CD").Value = trn_tp_cd
                objSql2.Parameters(":AMT").Value = MyDataReader.Item("AMT").ToString
                If MyDataReader.Item("MEDIUM_TP_CD").ToString <> "SAL" Then
                    objSql2.Parameters(":ADJ_IVC_CD").Value = MyDataReader.Item("DEL_DOC_NUM").ToString
                Else
                    objSql2.Parameters(":ADJ_IVC_CD").Value = System.DBNull.Value
                End If
                objSql2.ExecuteNonQuery()

                sql = "UPDATE ASP_TRN SET STAT_CD='F' WHERE STAT_CD='P' AND MEDIUM_TP_CD='" & MyDataReader.Item("MEDIUM_TP_CD").ToString & "' "
                sql = sql & "AND DEL_DOC_NUM='" & MyDataReader.Item("DEL_DOC_NUM").ToString & "'"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                objSql2.ExecuteNonQuery()
                txt_results.Text = txt_results.Text & MyDataReader.Item("DEL_DOC_NUM").ToString & " funded." & vbCrLf
            Loop
            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
