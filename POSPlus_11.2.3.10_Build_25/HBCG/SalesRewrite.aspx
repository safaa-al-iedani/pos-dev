<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="SalesRewrite.aspx.vb" Inherits="SalesRewrite" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register TagPrefix="ucZone" Src="~/Usercontrols/ZoneCode.ascx" TagName="ZonePopup" %>
<%@ Register TagPrefix="ucDeliveryDate" Src="~/Usercontrols/DeliveryDateDisplay.ascx" TagName="DeliveryDatePopup" %>
<%@ Register TagPrefix="ucTax" Src="~/Usercontrols/TaxUpdate.ascx" TagName="TaxUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table width="100%">
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txt_del_doc_num" runat="server" Width="148px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btn_submit" runat="server" Text="<%$ Resources:LibResources, Label222 %>" OnClick="btn_submit_Click"
                                            Width="36px">
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnSOM" runat="server" Text="SOM" Visible="False" Width="56px">
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btn_Save" runat="server" Text="<%$ Resources:LibResources, Label521 %>" Visible="False" Width="129px">
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label82 %>" Visible="False" Width="75px">
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="lbl_cust_cd" runat="server" Text="">
                                        </dx:ASPxLabel>
                                        &nbsp;&nbsp;
                                        <dx:ASPxLabel ID="lbl_fname" runat="server" Text="">
                                        </dx:ASPxLabel>
                                        &nbsp;&nbsp;
                                        <dx:ASPxLabel ID="lbl_lname" runat="server" Text="">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:DropDownList ID="cbo_related_docs" runat="server" AutoPostBack="True" Visible="False"
                                Width="154px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <dx:ASPxLabel ID="lbl_error" runat="server" ForeColor="Red" Text="Label" Width="654px">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ccff66">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="6">
                            <dx:ASPxLabel ID="ASPxLabel3" Font-Bold="true" runat="server" Text="<%$ Resources:LibResources, Label776 %>">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label511 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label667 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label378 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text="<%$ Resources:LibResources, Label379 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label394 %>">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:TextBox ID="txt_del_doc_existing" runat="server" Width="95px" Enabled="False"></asp:TextBox>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="txt_store_cd" runat="server" Width="27px"></asp:TextBox>
                            <asp:HiddenField ID="hidWrStARS" runat="server" />
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="cbo_PD" runat="server" Width="34px" AutoPostBack="True" Enabled="false">
                                <asp:ListItem Value="P">P</asp:ListItem>
                                <asp:ListItem Value="D">D</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="cbo_pd_store_cd" runat="server" Width="236px" AutoPostBack="True" 
                                Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxDateEdit ID="cbo_pd_date" runat="server" EnableViewState="true" AutoPostBack="false">
                                        </dx:ASPxDateEdit>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="btn_orig_lookup" runat="server" Height="20px" ImageUrl="~/images/icons/delivery.gif"
                                            OnClick="btn_orig_lookup_Click" Width="20px" />
                                        <asp:TextBox ID="txt_setup_tax" runat="server" Enabled="False" Height="1px" Visible="False"
                                            Width="1px"></asp:TextBox>
                                        <asp:TextBox ID="txt_del_tax" runat="server" Enabled="False" Height="1px" Visible="False"
                                            Width="1px"></asp:TextBox>
                                        <asp:TextBox ID="txt_total_finance" runat="server" Enabled="False" Height="1px" Visible="False"
                                            Width="1px"></asp:TextBox>
                                        <asp:TextBox ID="txt_tax_cd" runat="server" Enabled="False" Height="1px" Visible="False"
                                            Width="1px"></asp:TextBox>
                                        <asp:HiddenField ID="hidOrigTaxCd" runat="server" />
                                        <asp:TextBox ID="txt_zip_cd" runat="server" Enabled="False" Height="1px" Visible="False"
                                            Width="1px"></asp:TextBox>
                                        <asp:TextBox ID="txt_zone_cd" runat="server" Enabled="False" Height="1px" Visible="False"
                                            Width="1px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:LibResources, Label149 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td colspan="2">
                            <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="<%$ Resources:LibResources, Label542 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="<%$ Resources:LibResources, Label586 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label203 %>">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:TextBox ID="txt_del_chg" runat="server" Width="66px" AutoPostBack="True" OnTextChanged="txt_del_chg_TextChanged">0.00</asp:TextBox>
                        </td>
                        <td valign="top" colspan="2">
                            <asp:TextBox ID="txt_setup" runat="server" Width="62px" AutoPostBack="True" OnTextChanged="txt_setup_TextChanged">0.00</asp:TextBox>
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="lnkModifyTax" runat="server" ToolTip="Click to modify the tax code." CssClass="dxeBase_Office2010Blue">
                            </asp:LinkButton>&nbsp;
                            <asp:TextBox ID="txt_tax" runat="server" Enabled="False" Width="45px">0.00</asp:TextBox>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="txt_finance" runat="server" Width="81px" AutoPostBack="True">0.00</asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ccff66">
                <asp:DataGrid ID="grd_lines" runat="server" Width="100%" AutoGenerateColumns="False"
                    Height="50px" PageSize="1">
                    <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                        Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                    <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                        Font-Bold="True" ForeColor="Black"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="DEL_DOC_LN_NUM" HeaderText="<%$ Resources:LibResources, Label266 %>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="QTY" HeaderText="<%$ Resources:LibResources, Label477 %>" ReadOnly="True" DataFormatString="{0:0}" />
                        <asp:BoundColumn DataField="STORE_CD" HeaderText="<%$ Resources:LibResources, Label568 %>" ReadOnly="True" />
                        <asp:BoundColumn DataField="LOC_CD" HeaderText="<%$ Resources:LibResources, Label271 %>" ReadOnly="True" />
                        <asp:BoundColumn DataField="ITM_CD" HeaderText="<%$ Resources:LibResources, Label550 %>" ReadOnly="True" />
                        <asp:BoundColumn DataField="UNIT_PRC" HeaderText="<%$ Resources:LibResources, Label577 %>" ReadOnly="True" DataFormatString="{0:c}" />
                        <asp:TemplateColumn HeaderText="Move Qty">
                            <ItemTemplate>
                                <asp:TextBox ID="txtQty" DataField="QTY"  Text="1"  runat="server" Width="45"
                                     ></asp:TextBox>
                              <asp:TextBox ID="LblAllowFraction" DataField="AllowFraction"  Text='<%# DataBinder.Eval(Container, "DataItem.AllowFraction") %>' runat="server" Visible="false"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:ImageButton ID="Image1" CommandName="move_inventory_to_new" runat="server" ImageUrl="images/icons/arrow_right.png"
                                    Height="20" Width="20" />
                            </ItemTemplate>
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                            <EditItemTemplate>
                                <asp:ImageButton ID="LinkButton2" CommandName="move_inventory_to_new" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td style="text-align: center" bgcolor="#ccff66">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="<%$ Resources:LibResources, Label355 %>" Font-Bold="true">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:DataGrid ID="grd_ar" runat="server" Width="100%" AutoGenerateColumns="False"
                                Height="50px" PageSize="1">
                                <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                                    Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                                <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                                    Font-Bold="True" ForeColor="Black"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="AR_TRN_PK" HeaderText="<%$ Resources:LibResources, Label568 %>" ReadOnly="True" Visible="False" />
                                    <asp:BoundColumn DataField="ORIGIN_STORE" HeaderText="<%$ Resources:LibResources, Label568 %>" ReadOnly="True" />
                                    <asp:BoundColumn DataField="CSH_DWR_CD" HeaderText="<%$ Resources:LibResources, Label173 %>" ReadOnly="True" />
                                    <asp:BoundColumn DataField="AMT" HeaderText="<%$ Resources:LibResources, Label28 %>" ReadOnly="True" DataFormatString="{0:c}" />
                                    <asp:BoundColumn DataField="MOP_CD" HeaderText="<%$ Resources:LibResources, Label322 %>" ReadOnly="True" />
                                    <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label323 %>">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="txt_move_amt" runat="server" Text="" Width="45" AutoPostBack="False"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Image3" runat="server" CommandName="move_deposit_to_new" ImageUrl="images/icons/arrow_right.png"
                                                Height="20" Width="20" />
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="LinkButton3" CommandName="move_deposit_to_new" runat="server" />
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#ccff66">
                <asp:TextBox ID="txt_original_total" runat="server" Enabled="False" Width="84px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td align="center" colspan="5">
                            <dx:ASPxLabel ID="ASPxLabel18" Font-Bold="true" runat="server" Text="<%$ Resources:LibResources, Label777 %>">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="<%$ Resources:LibResources, Label511 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="<%$ Resources:LibResources, Label667 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="<%$ Resources:LibResources, Label378 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel21" runat="server" Text="<%$ Resources:LibResources, Label379 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="<%$ Resources:LibResources, Label394 %>">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:TextBox ID="txt_del_doc_new" runat="server" Width="95px" Enabled="False"></asp:TextBox>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="txt_store_cd_new" runat="server" Width="27px"></asp:TextBox>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="cbo_PD_new" runat="server" Width="34px" AutoPostBack="True">
                                <asp:ListItem Value="P">P</asp:ListItem>
                                <asp:ListItem Value="D">D</asp:ListItem>
                            </asp:DropDownList>

                            <asp:HiddenField ID="hidCustomerZipCode" runat="server" />
                            <asp:HiddenField ID="hidStoreZipCode" runat="server" />
                            <asp:HiddenField ID="hidChangedZoneCode" runat="server" />

                        </td>
                        <td style="width: 75px" align="right" valign="top">
                            <asp:DropDownList ID="cbo_new_pd_store_cd" runat="server" Width="236px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxDateEdit ID="cbo_pd_date_new" runat="server" EnableViewState="true" AutoPostBack="false">
                                        </dx:ASPxDateEdit>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="btn_new_lookup" runat="server" Height="20px" ImageUrl="~/images/icons/delivery.gif"
                                            Width="20px" />
                                        <asp:TextBox ID="txt_setup_tax_new" runat="server" Enabled="False" Height="1px" Visible="False"
                                            Width="1px"></asp:TextBox><asp:TextBox ID="txt_del_tax_new" runat="server" Enabled="False"
                                                Height="1px" Visible="False" Width="1px"></asp:TextBox>
                                        <asp:TextBox ID="txt_tax_cd_new" runat="server" Enabled="False" Height="1px" Visible="False"
                                            Width="1px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:LibResources, Label149 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td colspan="2">
                            <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="<%$ Resources:LibResources, Label542 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="<%$ Resources:LibResources, Label586 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="<%$ Resources:LibResources, Label203 %>">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:TextBox ID="txt_del_chg_new" runat="server" Width="66px" AutoPostBack="True"
                                OnTextChanged="txt_del_chg_new_TextChanged">0.00</asp:TextBox>
                        </td>
                        <td valign="top" colspan="2">
                            <asp:TextBox ID="txt_setup_new" runat="server" Width="60px" AutoPostBack="True" OnTextChanged="txt_setup_new_TextChanged">0.00</asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkModifyTaxNew" runat="server" ToolTip="Click to modify the tax code." CssClass="dxeBase_Office2010Blue">
                            </asp:LinkButton>&nbsp;
                            <asp:TextBox ID="txt_tax_new" runat="server" Enabled="False" Width="45px">0.00</asp:TextBox>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="txt_finance_new" runat="server" Width="81px" AutoPostBack="True"
                                OnTextChanged="txt_finance_new_TextChanged">0.00</asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid ID="grd_lines_new" runat="server" Width="100%" AutoGenerateColumns="False"
                    Height="50px" PageSize="1">
                    <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                        Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                    <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                        Font-Bold="True" ForeColor="Black"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="DEL_DOC_LN_NUM" HeaderText="<%$ Resources:LibResources, Label266 %>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="QTY" HeaderText="<%$ Resources:LibResources, Label447 %>" ReadOnly="True" DataFormatString="{0:0}" />
                        <asp:BoundColumn DataField="STORE_CD" HeaderText="<%$ Resources:LibResources, Label568 %>" ReadOnly="True" />
                        <asp:BoundColumn DataField="LOC_CD" HeaderText="<%$ Resources:LibResources, Label271 %>" ReadOnly="True" />
                        <asp:BoundColumn DataField="ITM_CD" HeaderText="<%$ Resources:LibResources, Label550 %>" ReadOnly="True" />
                        <asp:BoundColumn DataField="UNIT_PRC" HeaderText="<%$ Resources:LibResources, Label477 %>" ReadOnly="True" DataFormatString="{0:c}" />
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:ImageButton ID="Image2" runat="server" CommandName="move_inventory_to_old" ImageUrl="images/icons/symbol-delete.png"
                                    Height="20" Width="20" />
                            </ItemTemplate>
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                            <EditItemTemplate>
                                <asp:ImageButton ID="LinkButton3" CommandName="move_inventory_to_old" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="<%$ Resources:LibResources, Label335 %>" Font-Bold="true">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DataGrid ID="grd_ar_new" runat="server" Width="100%" AutoGenerateColumns="False"
                                Height="50px" PageSize="1">
                                <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                                    Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                                <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                                    Font-Bold="True" ForeColor="Black"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="AR_TRN_PK" HeaderText="<%$ Resources:LibResources, Label568 %>" ReadOnly="True" Visible="False" />
                                    <asp:BoundColumn DataField="ORIGIN_STORE" HeaderText="<%$ Resources:LibResources, Label568 %>" ReadOnly="True" />
                                    <asp:BoundColumn DataField="CSH_DWR_CD" HeaderText="<%$ Resources:LibResources, Label173 %>" ReadOnly="True" />
                                    <asp:BoundColumn DataField="AMT" HeaderText="<%$ Resources:LibResources, Label28 %>" ReadOnly="True" DataFormatString="{0:c}" />
                                    <asp:BoundColumn DataField="MOP_CD" HeaderText="<%$ Resources:LibResources, Label322 %>" ReadOnly="True" />
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Image4" runat="server" CommandName="remove_deposit" ImageUrl="images/icons/symbol-delete.png"
                                                Height="20" Width="20" />
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="LinkButton4" CommandName="remove_deposit" runat="server" />
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblZoneChangeNotification" runat="server" ForeColor="Red" Width="654px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:TextBox ID="txt_new_total" runat="server" Enabled="False" Width="84px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btn_original" runat="server" Enabled="False" Text="<%$ Resources:LibResources, Label648 %>" />
                <asp:Button ID="btn_new" runat="server" Enabled="False" Text="<%$ Resources:LibResources, Label778 %>" />
            </td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <asp:Label ID="lbl_old_or_new" runat="server" Visible="False"></asp:Label>

    <asp:HiddenField ID="hidIsNewSO" runat="server" />

    <dxpc:ASPxPopupControl ID="PopupTax" runat="server" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label592 %>" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="420px" Width="670px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <div style="width: 100%; height: 420px; overflow-y: scroll;">
                    <ucTax:TaxUpdate runat="server" ID="ucTaxUpdate" />
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <div>
        <ucMsg:MsgPopup runat="server" ID="ucMsgPopup" />
    </div>
    <dxpc:ASPxPopupControl ID="PopupZone" ClientInstanceName="PopupZone" runat="server" ShowOnPageLoad="false" CloseAction="None" ShowCloseButton="false"
        HeaderText="Zone Update" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter" OnLoad="PopupZone_Load"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="350px" Width="500px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                <ucZone:ZonePopup runat="server" ID="UcZonePopup" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <asp:HiddenField ID="hidZoneCode" runat="server" />
    <asp:HiddenField ID="hideZipCode" runat="server" />
    <asp:HiddenField ID="IsZoneSelected" runat="server" Value="False" />
    <dxpc:ASPxPopupControl ID="PopupDeliveryDate" runat="server" CloseAction="CloseButton" ShowOnPageLoad="false" ClientInstanceName="PopupDeliveryDate"
        HeaderText="<%$ Resources:SalesExchange, Label70 %>" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="400px" Width="500px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="DeliveryDatePopupContentControl" runat="server">
                <ucDeliveryDate:DeliveryDatePopup runat="server" ID="UcDeliveryDatePopup" />
                <div style="padding-top: 10px; padding-bottom: 10px;">
                    <dx:ASPxHyperLink ID="linkZone" runat="server" Text="Zone Selection"
                        ClientSideEvents-Click="function(s,e){if (typeof PopupDeliveryDate !=='undefined' && typeof PopupZone !=='undefined'){PopupDeliveryDate.Hide();PopupZone.Show();}}">
                    </dx:ASPxHyperLink>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
</asp:Content>
