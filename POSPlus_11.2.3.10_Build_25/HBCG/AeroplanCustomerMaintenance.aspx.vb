Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports AppUtils

Partial Class AeroplanCustomerMaintenance
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Dim g_old_crd_num As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_msg.Text = ""

        If isAuthToUpd() = "Y" Then
            btn_add_order.Visible = True
            GridView1.Visible = True
            GridView9.Visible = False
        Else
            btn_add_order.Visible = False
            GridView1.Visible = False
            GridView9.Visible = True
        End If

        If IsPostBack Then
            If ASPxPopupAddOrder.ShowOnPageLoad = False Then
                populate_gridview()
                GridView1.DataBind()
                GridView9.DataBind()
            End If
        Else
            btn_search.Visible = True
        End If

    End Sub
    Public Function isAuthToUpd() As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoupdate('" & Session("emp_cd") & "','AeroplanCustomerMaintenance.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_RES").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Function
    Private Sub populateOrders(ByVal p_cust As String)

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        new_del_doc_drp_list.Items.Clear()

        Dim SQL = "SELECT distinct s.so_doc_num del_doc_num " &
                            " from itm i, so_ln sl, so s " &
                            " where s.cust_cd = '" & UCase(p_cust) & "'" &
                            "    and s.del_doc_num = sl.del_doc_num " &
                            "    and sl.itm_cd     = i.itm_cd " &
                            "    and nvl(sl.void_flag,'x') <> 'Y' " &
                            "    and nvl(s.stat_cd,'x') in ('O','F') " &
                            "    and nvl(s.ord_tp_cd,'x') = 'SAL' " &
                            "    and std_multi_co2.isvalidstr6('" & Session("emp_init") & "', nvl(s.so_store_cd,'x')) = 'Y' " &
                            "    and not exists (select 'x' from so_aeroplan s2 " &
                            "                    where s2.del_doc_num = s.so_doc_num ) "

        new_del_doc_drp_list.Items.Insert(0, "Orders")
        new_del_doc_drp_list.Items.FindByText("Orders").Value = ""
        new_del_doc_drp_list.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With new_del_doc_drp_list
                .DataSource = ds
                .DataValueField = "del_doc_num"
                .DataTextField = "del_doc_num"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try


    End Sub
    Protected Sub mnrcdChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub clear_the_screen()

        srch_aeroplan_crd_num.Text = ""
        srch_cust_cd.Text = ""
        srch_order_no.Text = ""
        srch_contract.Text = ""


        GridView1.DataSource = ""
        GridView1.DataBind()
        GridView1.PageIndex = 0

        GridView9.DataSource = ""
        GridView9.DataBind()
        GridView9.PageIndex = 0

        btn_search.Visible = True


    End Sub
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        clear_the_screen()

    End Sub
    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click

        GridView1.PageIndex = 0
        populate_gridview()

    End Sub
    Protected Sub btn_add_order_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add_order.Click

        new_del_doc_drp_list.Visible = False
        btn2_search.Visible = False
        lbl_msg.Text = ""
        chkbox_srch.Checked = False

        GridView1.DataSource = ""
        GridView1.DataBind()
        GridView1.PageIndex = 0
        btn_search.Visible = True

        ASPxPopupAddOrder.ShowOnPageLoad = True


    End Sub
    Protected Sub refresh_miles(ByVal p_sql As String)

        'Req2627
        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim sql As String = p_sql

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count
            connErp.Close()
        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

        Dim v_so_doc_num As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                v_so_doc_num = getSoDocNum(mytable.Rows(0).Item("DEL_DOC_NUM").ToString)
                AddAeroplanDetail(v_so_doc_num)
            Next
        End If



    End Sub
    Protected Sub populate_gridview()

        If (srch_order_no.Text & "" = "" Or isEmpty(srch_order_no.Text)) And
        (srch_aeroplan_crd_num.Text & "" = "" Or isEmpty(srch_aeroplan_crd_num.Text)) And
        (srch_cust_cd.Text & "" = "" Or isEmpty(srch_cust_cd.Text)) And
        (srch_contract.Text & "" = "" Or isEmpty(srch_contract.Text)) Then
            lbl_msg.Text = "Please enter a Search criteria to continue"
            Exit Sub
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim v_aeroplan_sql As String = ""
        Dim v_del_doc_num As String = ""
        Dim v_cust_sql As String = ""
        Dim v_contract_sql As String = ""
        Dim v_upd_usr As String = isAuthToUpd()

        'Req2627
        Dim v_refresh_sql As String = ""

        If srch_aeroplan_crd_num.Text & "" = "" Or isEmpty(srch_aeroplan_crd_num.Text) Then
            v_aeroplan_sql = " and  (a.aeroplan_crd_num is null or a.aeroplan_crd_num = nvl('" & srch_aeroplan_crd_num.Text & "', a.aeroplan_crd_num)) "
        Else
            v_aeroplan_sql = " and a.aeroplan_crd_num = nvl('" & srch_aeroplan_crd_num.Text & "', a.aeroplan_crd_num) "
        End If

        If srch_cust_cd.Text & "" = "" Or isEmpty(srch_cust_cd.Text) Then
            v_cust_sql = " and (a.cust_cd is null or a.cust_cd = nvl('" & UCase(srch_cust_cd.Text) & "', a.cust_cd)) "
        Else
            v_cust_sql = " and a.cust_cd = nvl('" & UCase(srch_cust_cd.Text) & "', a.cust_cd) "
        End If

        If srch_contract.Text & "" = "" Or isEmpty(srch_contract.Text) Then
            v_contract_sql = " and (d.contract is null or d.contract = nvl('" & srch_contract.Text & "', d.contract)) "
        Else
            v_contract_sql = " and d.contract = nvl('" & srch_contract.Text & "', d.contract) "
        End If

        'Req2627
        v_refresh_sql = "select distinct d.del_doc_num DEL_DOC_NUM " &
                         " from so_aeroplan a, so_aeroplan_dtl d, SO s" &
                         "  where a.del_doc_num = d.so_doc_num " &
                         " and s.del_doc_num = nvl(d.so_doc_num,'x') " &
                         " and d.so_doc_num = nvl('" & UCase(srch_order_no.Text) & "',d.so_doc_num) " &
                         " and nvl(d.stat_cd,'x') = 'O' " &
                         v_cust_sql &
                         v_aeroplan_sql &
                         v_contract_sql

        refresh_miles(v_refresh_sql)

        GridView1.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet
        'sabrina oct,2016 R2660
        objSql.CommandText = " select a.cust_cd CUST_CD,d.del_doc_num DEL_DOC_NUM, nvl(d.contract,' ') CONTRACT, " &
                             " decode('" & v_upd_usr & "','Y',a.aeroplan_crd_num,substr(a.aeroplan_crd_num,1,3)||'***'||substr(a.aeroplan_crd_num,7,3)) AEROPLAN_CRD_NUM, " &
                             " trunc(a.aeroplan_purch_dt) SO_WR_DT, " &
                             " d.itm_cd ITM_CD,nvl(d.qualify_amt,0) SLS_AMT, d.mpc_code MPC_CODE, " &
                             " nvl(std_aeroplan.calcpts(d.mpc_code,d.qualify_amt,d.qty,a.aeroplan_purch_dt),0) MILES, " &
                             " trunc(s.final_dt) FINAL_DT,d.stat_cd STAT_CD, 0 ROW_ID" &
                             " from so_aeroplan a, so_aeroplan_dtl d, SO s" &
                             "  where a.del_doc_num = d.so_doc_num " &
                             " and s.del_doc_num = nvl(d.so_doc_num,'x') " &
                             " and d.so_doc_num = nvl('" & UCase(srch_order_no.Text) & "',d.so_doc_num)" &
                             v_cust_sql &
                             v_aeroplan_sql &
                             v_contract_sql &
                             " order by 1,2,3,4,6,8,12 "

        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        'Req2670 (sabrina)
        If numrows > 0 Then
            Dim i As Integer = 0
            For Each row As DataRow In ds.Tables(0).Rows
                i = i + 1
                row.Item("ROW_ID") = i
            Next
        End If

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView9.DataSource = ds
                GridView9.DataBind()
                btn_search.Visible = False
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                GridView9.DataSource = ds
                GridView9.DataBind()
                lbl_msg.Text = "No data to display"
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Sub
    Function getStat(ByVal p_del_doc_num As String, ByVal p_itm As String, ByVal p_mpc As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim v_mpc As String = ""

        If p_mpc & "" = "" Then
            v_mpc = " and mpc_code is null "
        ElseIf isEmpty(p_mpc) Then
            v_mpc = " and mpc_code is null "
        Else
            v_mpc = " and nvl(mpc_code,'x') = '" & p_mpc & "' "
        End If

        Dim sql As String = "SELECT min(stat_cd) stat "
        sql = sql & " from so_aeroplan_dtl "
        sql = sql & " where del_doc_num = '" & p_del_doc_num & "' "
        sql = sql & "   and itm_cd = '" & p_itm & "' "
        sql = sql & v_mpc


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("stat")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function getSoWrDt(ByVal p_del_doc_num As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim v_mpc As String = ""

        Dim sql As String = "SELECT min(so_wr_dt) dt "
        sql = sql & " from so "
        sql = sql & " where so_doc_num = '" & p_del_doc_num & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("dt")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function getSoDocNum(ByVal p_del_doc_num As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim v_mpc As String = ""

        Dim sql As String = "SELECT so_doc_num doc "
        sql = sql & " from so_aeroplan_dtl "
        sql = sql & " where del_doc_num = '" & p_del_doc_num & "' "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("doc")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Function isValidCardNum(ByVal p_card_num As String) As String

        'for now only allow 9 digit card#
        'If Len(p_card_num) = 16 Then  
        '    If Left(p_card_num, 5) <> "627421" Then       'bin#
        '        Return "N"
        '    End If
        'End If

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT std_aeroplan.validate_crdnum('" & p_card_num & "') ind "
        sql = sql & " from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("ind")
            Else
                v_value = "N"
            End If
        Catch
            v_value = "N"
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        Else
            Return v_value
        End If

    End Function
    Function isValidCustomer(ByVal p_cust_cd As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from cust " &
                            " where cust_cd = '" & UCase(p_cust_cd) & "'"


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
            End If
        Catch
            v_value = 0
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Function isValidOrder(ByVal p_doc As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt " &
                            " from itm i, so_ln sl, so s " &
                            " where s.so_doc_num = '" & UCase(p_doc) & "'" &
                            "    and s.del_doc_num = sl.del_doc_num " &
                            "    and sl.itm_cd     = i.itm_cd " &
                            "    and nvl(sl.void_flag,'x') <> 'Y' " &
                            "    and nvl(s.stat_cd,'x') in ('O','F') " &
                            "    and nvl(s.ord_tp_cd,'x') = 'SAL' " &
                            "    and std_multi_co2.isvalidstr6('" & Session("emp_init") & "', nvl(s.so_store_cd,'x')) = 'Y' "


        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 0
                End If
        Catch
            v_value = 0
                'do nothing
            End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "N"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
            End If

    End Function
    Function OrderAlreadyExists(ByVal p_doc As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from so_aeroplan " &
                            " where del_doc_num = '" & UCase(p_doc) & "'"  
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 1
            End If
        Catch
            v_value = 1
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "Y"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function
    Protected Sub GridView1_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)

        If e.Column.FieldName = "AEROPLAN_CRD_NUM" Then
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        ElseIf e.Column.FieldName = "STAT_CD" Then
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        ElseIf e.Column.FieldName = "SO_WR_DT" Then
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        Else
            e.Editor.ReadOnly = True
            e.Editor.ClientEnabled = False
        End If

    End Sub
    Protected Sub GridView1_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)

        e.NewValues("CUST_CD") = ""
        e.NewValues("DEL_DOC_NUM") = ""
        e.NewValues("CONTRACT") = ""
        e.NewValues("AEROPLAN_CRD_NUM") = ""
        e.NewValues("SO_WR_DT") = ""
        e.NewValues("ITM_CD") = ""
        e.NewValues("MPC_CODE") = ""
        e.NewValues("SLS_AMT") = ""
        e.NewValues("MILES") = ""
        e.NewValues("FINAL_DT") = ""
        e.NewValues("STAT_CD") = ""

    End Sub
    Protected Sub rowvalidation(ByVal sender As Object, ByVal e As ASPxDataValidationEventArgs)

        Dim v_mpc As String = ""
        If e.OldValues("MPC_CODE") & "" = "" Then
            v_mpc = ""
        Else
            v_mpc = e.OldValues("MPC_CODE")
        End If
        Dim v_stat As String = getStat(e.OldValues("DEL_DOC_NUM"), e.OldValues("ITM_CD"), v_mpc)

        If v_stat & "" = "" Or isEmpty(v_stat) Then
            'continue
        Else
            If v_stat <> "R" And v_stat <> "O" And v_stat <> "C" Then
                e.RowError = "No updates allowed - status is " & v_stat
                Exit Sub
            End If
        End If

        If e.NewValues("STAT_CD") & "" = "" Or isEmpty(e.NewValues("STAT_CD")) Then
            e.RowError = "Please enter Status 'O'pen or 'C'ancel"
            Exit Sub
        ElseIf UCase(e.NewValues("STAT_CD").ToString()) <> "O" And UCase(e.NewValues("STAT_CD").ToString()) <> "C" Then
            e.RowError = "Invalid Status - Please enter Status 'O'pen or 'C'ancel"
            Exit Sub
        End If

        If e.NewValues("AEROPLAN_CRD_NUM") & "" = "" Or isEmpty(e.NewValues("AEROPLAN_CRD_NUM")) Then
            e.RowError = "Please enter a valid Aeroplan No. "
            Exit Sub
        ElseIf Not IsNumeric(e.NewValues("AEROPLAN_CRD_NUM")) Then
            e.RowError = "Invalid Aeroplan No. - please try again"
            Exit Sub
        ElseIf Len(e.NewValues("AEROPLAN_CRD_NUM")) <> 9 Then
            e.RowError = "invalid Aeroplan No. - please try again"
            Exit Sub
        ElseIf isValidCardNum(e.NewValues("AEROPLAN_CRD_NUM")) <> "Y" Then
            e.RowError = "Invalid Aeroplan No. - please try again"
            Exit Sub
        End If

        If e.NewValues("SO_WR_DT") & "" = "" Or isEmpty(e.NewValues("SO_WR_DT")) Then
            e.RowError = "Please select a Date "
            Exit Sub
        ElseIf e.NewValues("SO_WR_DT") > Today.Date Then
            e.RowError = "Future date is not allowed "
            Exit Sub
        End If

        e.RowError = ""
        e.Keys.Clear()

    End Sub
    Protected Sub ASPxGridView1_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)

        Dim hasError As Boolean = False
        Dim errorMsg As String = ""
        Dim v_upd_ind As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim v_so_doc_num As String = getSoDocNum(e.OldValues("DEL_DOC_NUM").ToString())

        Dim v_cust_sql As String = ""

        If e.OldValues("CUST_CD") & "" = "" Then
            v_cust_sql = " and cust_cd is null "
        ElseIf isEmpty(e.OldValues("CUST_CD")) Then
            v_cust_sql = " and cust_cd is null "
        Else
            v_cust_sql = " and cust_cd = '" & UCase(e.OldValues("CUST_CD").ToString()) & "' "
        End If

        '**** note ******* on so_aeroplan the del-doc-num is actually so-doc-num 
        '--------------------------------------------------------------------
        ' update aeroplan# on so_aeroplan
        '--------------------------------------------------------------------
        If (e.NewValues("AEROPLAN_CRD_NUM").ToString() <> e.OldValues("AEROPLAN_CRD_NUM").ToString()) Or
            (e.NewValues("SO_WR_DT").ToString() <> e.OldValues("SO_WR_DT").ToString()) Then

            conn.Open()
            sql = "UPDATE SO_AEROPLAN SET "
            sql = sql & " aeroplan_crd_num = '" & e.NewValues("AEROPLAN_CRD_NUM").ToString() & "' "
            sql = sql & " ,aeroplan_purch_dt = to_date ('" & FormatDateTime(e.NewValues("SO_WR_DT"), 2) & "','mm/dd/RRRR') "
            sql = sql & " ,upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR') "
            sql = sql & " ,upd_by = '" & Session("emp_init") & "' "
            sql = sql & " where del_doc_num = '" & v_so_doc_num & "' "
            sql = sql & v_cust_sql


            Dim v_val As String = ""

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                Mydatareader.Close()
            Catch
                Throw New Exception("Error updating the record. Please try again.")
                conn.Close()
            End Try
            conn.Close()
            v_upd_ind = "Y"

        End If

        '--------------------------------------------------------------------
        ' update points if purchase date is changed
        '--------------------------------------------------------------------
        'july 25,2016
        If (e.NewValues("SO_WR_DT").ToString() <> e.OldValues("SO_WR_DT").ToString()) Then
            AddAeroplanDetail(v_so_doc_num)
        End If

        '--------------------------------------------------------------------
        ' update status on so_aeroplan_dtl
        '--------------------------------------------------------------------
        If UCase(e.NewValues("STAT_CD").ToString()) <> UCase(e.OldValues("STAT_CD").ToString()) Then

            Dim v_where_sql As String = ""

            If e.OldValues("STAT_CD").ToString() = "R" Then 'if old status was R then all items with status R will change to new status 
                v_where_sql = "   and stat_cd = 'R' "
            Else
                v_where_sql = "   and itm_cd = '" & e.OldValues("ITM_CD").ToString() & "' " &
                              "   and del_doc_num = '" & e.OldValues("DEL_DOC_NUM").ToString & "' "
                If e.OldValues("MPC_CODE") & "" = "" Then
                    v_where_sql = v_where_sql & " and mpc_code is null "
                ElseIf isEmpty(e.OldValues("MPC_CODE")) Then
                    v_where_sql = v_where_sql & " and mpc_code is null "
                Else
                    v_where_sql = v_where_sql & " and nvl(mpc_code,'x') = '" & e.OldValues("MPC_CODE").ToString() & "' "
                End If

            End If

           
            conn.Open()

            sql = "UPDATE SO_AEROPLAN_DTL SET "
            sql = sql & " stat_cd = '" & UCase(e.NewValues("STAT_CD").ToString()) & "' "
            sql = sql & " ,upd_dt = to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR') "
            sql = sql & " ,upd_by = '" & Session("emp_init") & "' "
            sql = sql & " where so_doc_num = '" & v_so_doc_num & "' "
            sql = sql & v_where_sql


            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                Mydatareader.Close()
            Catch
                Throw New Exception("Error updating the record. Please try again.")
                conn.Close()
            End Try
            conn.Close()
            v_upd_ind = "Y"
        End If

        GridView1.DataBind()
        e.Cancel = True
        GridView1.CancelEdit()

        If v_upd_ind = "Y" Then
            Throw New Exception("Update is complete")
        End If

    End Sub
    Protected Sub btn2_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn2_clear.Click

        chkbox_srch.Checked = False
        btn2_search.Visible = False
        btn2_add.Visible = True

        new_del_doc_drp_list.Items.Clear()
        new_del_doc_drp_list.Visible = False

        new_del_doc_num.Visible = True
        new_del_doc_num.Text = ""
        new_cust_cd.Text = ""
        new_aeroplan_crd_num.Text = ""


        new_cust_cd.Text = ""
        lbl_msg2.Text = ""

    End Sub
    Protected Sub btn2_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn2_search.Click

        If new_cust_cd.Text & "" = "" Or isEmpty(new_cust_cd.Text) Then
            lbl_msg2.Text = "Please enter a Customer Code to search for Orders"
            Exit Sub
        Else
            populateOrders(UCase(new_cust_cd.Text))
        End If

    End Sub
    Protected Sub btn2_add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn2_add.Click

        If new_cust_cd.Text & "" = "" Or isEmpty(new_cust_cd.Text) Then
            lbl_msg2.Text = "Please enter a Customer Code to continue"
            new_cust_cd.Focus()
            Exit Sub
        ElseIf isValidCustomer(UCase(new_cust_cd.Text)) <> "Y" Then
            lbl_msg2.Text = "Invalid Customer Code - please try again"
            new_cust_cd.Focus()
            Exit Sub
        End If

        If new_aeroplan_crd_num.Text & "" = "" Or isEmpty(new_aeroplan_crd_num.Text) Then
            lbl_msg2.Text = "Please enter a valid Aeroplan No."
            new_aeroplan_crd_num.Focus()
            Exit Sub
        ElseIf Not IsNumeric(UCase(new_aeroplan_crd_num.Text)) Then
            lbl_msg2.Text = "Invalid Aeroplan No. - please try again"
            new_aeroplan_crd_num.Focus()
            Exit Sub
        ElseIf Len(new_aeroplan_crd_num.Text) <> 9 Then
            lbl_msg2.Text = "Invalid Aeroplan No. - please try again"
            new_aeroplan_crd_num.Focus()
            Exit Sub
        ElseIf isValidCardNum(UCase(new_aeroplan_crd_num.Text)) <> "Y" Then
            lbl_msg2.Text = "Invalid Aeroplan No. - please try again"
            new_aeroplan_crd_num.Focus()
            Exit Sub
        End If

        If new_del_doc_num.Text & "" = "" Or isEmpty(new_del_doc_num.Text) Then
            If new_del_doc_drp_list.SelectedIndex < 1 Then
                lbl_msg2.Text = "Please Enter or Select an Order to continue"
                Exit Sub
            Else
                If isValidOrder(new_del_doc_drp_list.SelectedItem.Value.ToString()) <> "Y" Then
                    lbl_msg2.Text = "Invalid Order - please try again"
                    Exit Sub
                ElseIf OrderAlreadyExists(new_del_doc_drp_list.SelectedItem.Value.ToString()) = "Y" Then
                    lbl_msg2.Text = "This Order already exists"
                    Exit Sub
                Else
                    AddNewOrder(new_del_doc_drp_list.SelectedItem.Value.ToString())
                End If
            End If
        Else
            If isValidOrder(UCase(new_del_doc_num.Text)) <> "Y" Then
                lbl_msg2.Text = "Invalid Order - please try again"
                Exit Sub
            Else
                If OrderAlreadyExists(UCase(new_del_doc_num.Text)) = "Y" Then
                    lbl_msg2.Text = "This Order already exists"
                    Exit Sub
                Else
                    AddNewOrder(UCase(new_del_doc_num.Text))
                End If
            End If
        End If

    End Sub
    Protected Sub doSearchOrder(ByVal sender As Object, ByVal e As EventArgs)

        If chkbox_srch.Checked = True Then
            btn2_search.Visible = True
            new_del_doc_drp_list.Visible = True
            new_del_doc_num.Visible = False
            new_del_doc_num.Text = ""
            lbl_msg2.Text = "Enter Customer code and press Search"
        Else
            btn2_search.Visible = False
            new_del_doc_drp_list.Visible = False
            new_del_doc_num.Visible = True
            new_del_doc_num.Text = ""
            lbl_msg2.Text = ""
        End If

    End Sub
    Protected Sub AddNewOrder(ByVal p_order As String)

        '---------------------------------------------------------------------------------------------
        '---------------------------------------- insert new order -------------------------------------
        '---------------------------------------------------------------------------------------------

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim sql As String = ""

        Dim v_so_wr_dt As String = getSoWrDt(UCase(p_order))

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = "Insert into so_aeroplan " &
              "(del_doc_num,aeroplan_crd_num,aeroplan_purch_dt,cust_cd,cre_dt,cre_by) " &
              " values ('" & UCase(p_order) & "'" &
              " ,'" & new_aeroplan_crd_num.Text & "'" &
              " ,to_date ('" & FormatDateTime(v_so_wr_dt, 2) & "','mm/dd/RRRR') " &
              " ,'" & UCase(new_cust_cd.Text) & "'" &
              " , to_date ('" & FormatDateTime(System.DateTime.Today.ToString, 2) & "','mm/dd/RRRR') " &
              " ,'" & Session("emp_init") & "' )"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try

        conn.Close()
        AddAeroplanDetail(p_order)

        btn2_add.Visible = False
        btn2_search.Visible = False

        ' populate_gridview()
        ' GridView1.DataBind()

        lbl_msg2.Text = "Order is added"


    End Sub
    Protected Sub AddAeroplanDetail(ByVal p_order As String)
        Dim connstring As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection
         

        Dim sql As String = "SELECT std_aeroplan.populateDetail('" & UCase(p_order) & "') from dual "
        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_aeroplan.populateDetail"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.VarChar)).Value = UCase(p_order)

            myCMD.ExecuteNonQuery()

            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Error in creating Aeroplan Detail ")
        End Try
           

    End Sub

End Class
