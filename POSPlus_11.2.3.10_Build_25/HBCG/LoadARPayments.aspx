﻿<%@ Page Language="VB"  MasterPageFile="~/MasterPages/NoWizard2.master"  AutoEventWireup="false" CodeFile="LoadARPayments.aspx.vb" Inherits="LoadARPayments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:Label ID="txt_title" runat="server" Style="position: relative;color:black" CssClass="style5">Load AR Payments File</asp:Label>
    <br />
    <div>
        <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
        <asp:FileUpload ID="FileUpload1" runat="server" ClientIDMode="Static"/><br />
            </ContentTemplate>
            <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" 
         Text="Upload AR Payment File" />&nbsp;<br />
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label></div>

    <br />
     <asp:TextBox ID="lbl_msg" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" Font-Size="small" ForeColor="Red"></asp:TextBox>
     
    <br />
    <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server" 
         AutoGenerateColumns="false" CellPadding="2" DataKeyField="ivc_cd"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="co_cd" headertext="Company Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ar_tp" headertext="A/R Type" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="pmt_store" headertext="Payment Store" ReadOnly="True" Visible="True">                
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="csh_dwr_cd" headertext="Cash Drawer Code" ReadOnly="True" Visible="True">
                    </asp:BoundColumn>
                <asp:BoundColumn DataField="mop_cd" headertext="Method of Payment Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="cust_cd" headertext="Customer Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="cntr_cd" headertext="Contract Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ivc_cd" headertext="Invoice Code" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
            <asp:BoundColumn DataField="origin_store" headertext="Origin Store" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
            <asp:BoundColumn DataField="amt" headertext="Amount" ReadOnly="True" Visible="True">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="err_msg" HeaderText="<font color=red>Error Message</font>" ReadOnly="True" Visible="True" ItemStyle-Wrap="False">
                <ItemStyle ForeColor="Red"/>
            </asp:BoundColumn>
    </columns> 
    <itemstyle verticalalign="Top" />
    </asp:DataGrid>
    <br />
     <asp:Button ID="btn_validate" runat="server"  Text="Validate Items" Width="200px" Visible="false" OnClick="btn_validate_Click">
     </asp:Button>

     <asp:Button ID="btn_process" runat="server" Text="Process Items" Width="200px" Visible="false" OnClick="btn_process_Click">
     </asp:Button>

     <asp:Button ID="btn_clear" runat="server" Text="Clear" Width="200px" Visible="false" OnClick="btn_clear_Click">
     </asp:Button>
    <br />
</asp:Content>
