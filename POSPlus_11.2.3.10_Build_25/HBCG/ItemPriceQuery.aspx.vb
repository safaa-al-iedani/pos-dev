Imports HBCG_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization

'JAM added 20Oct2014
Imports DevExpress.Web.ASPxEditors
Imports System.Data

'JAM import SalesDac
Imports SalesDac


Partial Class ItemPriceQuery
    Inherits POSBasePage

    Public Const gs_version As String = "Version 3.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen. Basic item data query.
    '
    '   2.0:    Added new price group gridview.
    '   (1) Store drop-down list filtered based on user�s company code.
    '   (2) Message box issue � message now changed to a label on screen.
    '   (3) Bug fix as noted by Dave L.
    '
    '   2.0.1:  Go live issues:
    '   (1) modified security violation error message to include employee code.
    '   (2) confirmed upper-case on all data values being used to query item data.
    '
    '   2.1:
    '   (1) combined category and description into one field
    '   (2) moved category description from main item query to a separate procedure as some items were not being retrieved as expected (those without a category)
    '   (3) combined style and description into one field
    '   (4) added item dimensions
    '   (5) added "SPIFF calendar" data
    '   (6) changed "as of date" calendar to allow selection of a date up to 7 years in the past and 30 days in the future
    '   (7) changed isValidItem to use new PL/SQL block which bypasses the STD_MULTI_CO.isValidItm logic to determine if 'security violation' reports would be fixed.
    '       NOTE:  a) does not include " Super user: bypass validation" as per Jacquie S. request
    '              b) calls everything else as programmed in "STD_MULTI_CO.isValidItm"
    '
    '   2.1.1:
    '   (1) SPIFF data not being cleared upon a new query
    '   (2) modified "isValidItem" check and "isValidItemSecurityCheck" to properly execute in logical order - renamed functions and addressed the order of execution.
    '
    '   2.2:
    '   (1) added new logic for packages
    '   (2) added "split method" field to form - displays only for packages
    '   (3) new gridview for package components
    '
    '   2.2.1:
    '   (1) fixed retail price for individual package component items - Jacquie discovers price being sent back by "SalesDacLibraryClass.GetPkgCmpntsRetPrc()" is not the one we want.
    '   Modified to use "bt_get_price.retail_clndr_price()" instead.
    '
    '   2.3:
    '   (1) added landed cost - Brick stores and Franchise
    '   (2) visibility of both based on user security level
    '   (3) PENDING - staff pricing -- on hold until staff price business logic is completed
    '
    '   2.3.1:
    '   (1) removed error message when vendor upcharge is not found, default to zero
    '   (2) removed error message when freight factor is not found, default to zero -- added warning message that freight factor is not found/zero.
    '
    '   2.3.2:
    '   (1) fixed issue where freight factor not found message was displaying most of the time
    '   3.0:
    '   (1) French implementation
    '------------------------------------------------------------------------------------------------------------------------------------

    'JAM v2.1: changed calendar to allow selection of a date up to 7 years in the past and 30 days in the future
    Public Const gn_minimumCalendarDaysLimit As Integer = 365 * 7   '~7 years
    Public Const gn_maximumCalendarDaysLimit As Integer = 30        '30 days
    'JAM v2.1: changed calendar to allow selection of a date up to 7 years in the past and 30 days in the future

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           ResetAllFields()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Resets/clears all fields.
    '   Called by:      btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub ResetAllFields()

        lbl_resultsInfo.Visible = False
        lbl_resultsInfo.Text = ""

        txt_des.Text = ""
        txt_ve_name.Text = ""
        txt_vsn.Text = ""
        txt_major_des.Text = ""
        txt_minor_des.Text = ""
        txt_itm_tp_cd.Text = ""
        txt_drop_cd.Text = ""
        txt_cat_cd.Text = ""
        txt_cat_des.Text = ""
        txt_style_cd.Text = ""
        txt_style_des.Text = ""
        txt_comm_cd.Text = ""
        txt_related_itm_cd.Text = ""
        txt_warrantable.Text = ""
        txt_treatable.Text = ""
        txt_pos_price.Text = ""
        txt_promo_cd.Text = ""
        txt_promo_des.Text = ""
        lst_iic_desc.Items.Clear()

        'JAM v2.1: added dimensions
        txt_width.Text = ""
        txt_depth.Text = ""
        txt_height.Text = ""
        txt_weight.Text = ""
        'JAM v2.1: added dimensions

        'JAM v2.1: added SPIFF data
        txt_spiff_cd.Text = ""
        txt_spiff_des.Text = ""
        txt_spiff_eff_date.Text = ""
        txt_spiff_end_date.Text = ""
        txt_spiff_amount.Text = ""
        txt_spiff_min_retail.Text = ""
        'JAM v2.1: added SPIFF data

        'JAM v2.2: added new package split method
        txt_pkg_split_method.Text = ""
        'JAM v2.2 (END)

        GridViewPrices.DataSource = ""
        GridViewPrices.DataBind()

        'JAM v2.2: added new package components gridview
        GridViewPackageComponents.DataSource = ""
        GridViewPackageComponents.DataBind()
        'JAM v2.2 (END)

        txt_landed_cost.Text = ""
        txt_fran_cost.Text = ""
        txt_comm_cost.Text = ""  'Alice added for request 387



    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           IsValidItemSecurityCheck_LEONS()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which validates the item is accessible for the given user.
    '   Called by:      btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function IsValidItemSecurityCheck_LEONS(ByVal p_itemCode As String) As String

        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim SQL As String = ""

        Dim s_employeeCode = Session("EMP_CD")
        'MsgBox("employee: " + s_employeeCode)

        'TO TEST empty employee value
        's_employeeCode = ""

        If (String.IsNullOrEmpty(Trim(s_employeeCode.ToString()))) Then

            Dim s_errorMessage As String = ""

            's_errorMessage = "ERROR: Current user employee code has no value, cannot proceed to confirm employee-item security."
            s_errorMessage = Resources.CustomMessages.MSG0001
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            IsValidItemSecurityCheck_LEONS = "NO_EMP"

        Else
            'Dim s_errorMessage As String = ""
            's_errorMessage = "INFO: Current user employee code is [" + s_employeeCode + "]"
            'DisplayMessage(s_errorMessage, "LABEL", "INFO")

            If (Not (p_itemCode Is Nothing) AndAlso p_itemCode.isNotEmpty) Then

                conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                'Open Connection 
                conn.Open()

                'we use isValidItm2() so we can pass in the EMP_CD which determines the EMP_INIT which is required for isValidItm()
                SQL = "STD_MULTI_CO.isValidItm2"
                objsql = DisposablesManager.BuildOracleCommand(SQL, conn)
                objsql.CommandType = CommandType.StoredProcedure

                'objsql.Parameters.Add("p_empinit", OracleType.VarChar).Direction = ParameterDirection.Input
                objsql.Parameters.Add("p_empCd", OracleType.VarChar).Direction = ParameterDirection.Input
                objsql.Parameters.Add("p_itm", OracleType.VarChar).Direction = ParameterDirection.Input
                objsql.Parameters.Add("v_returnValue", OracleType.VarChar, 10).Direction = ParameterDirection.ReturnValue

                'objsql.Parameters("p_empinit").Value = s_employeeCode
                objsql.Parameters("p_empCd").Value = s_employeeCode
                objsql.Parameters("p_itm").Value = p_itemCode

                objsql.ExecuteNonQuery()
                IsValidItemSecurityCheck_LEONS = objsql.Parameters("v_returnValue").Value

            End If

        End If

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           IsValidItemSecurityCheck()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which validates the item is accessible for the given user.
    '   Called by:      btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function IsValidItemSecurityCheck(ByVal p_itemCode As String) As String

        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim Mydatareader As OracleDataReader
        Dim SQL As String = ""

        Dim s_result As String = ""

        Dim s_itemCode As String = p_itemCode.ToUpper
        Dim s_employeeCode = Session("EMP_CD")
        'MsgBox("employee: " + s_employeeCode)

        'TO TEST empty employee value
        's_employeeCode = ""

        If (String.IsNullOrEmpty(Trim(s_employeeCode.ToString()))) Then

            Dim s_errorMessage As String = ""

            's_errorMessage = "ERROR: Current user employee code has no value, cannot proceed to confirm employee-item security."
            s_errorMessage = Resources.CustomMessages.MSG0001
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            IsValidItemSecurityCheck = "NO_EMP"

        Else
            'Dim s_errorMessage As String = ""
            's_errorMessage = "INFO: Current user employee code is [" + s_employeeCode + "]"
            'DisplayMessage(s_errorMessage, "LABEL", "INFO")

            If Not (String.IsNullOrEmpty(Trim(s_itemCode.ToString()))) Then
                'If (Not (p_itemCode Is Nothing) AndAlso p_itemCode.isNotEmpty) Then

                conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                'Open Connection 
                conn.Open()

                SQL = "DECLARE "
                SQL = SQL & "s_emp_cd            EMP.emp_cd%TYPE := :x_employeeCode; " ''SQLTEST'; "
                SQL = SQL & "s_emp_init          EMP.emp_init%TYPE; "
                SQL = SQL & "s_company_group_cd  CO_GRP.co_grp_cd%TYPE; "
                SQL = SQL & "s_itm_cd            ITM.itm_cd%TYPE := :x_itemCode; " ''5998273GN'; "
                SQL = SQL & "s_result            VARCHAR2(1) := 'N'; "

                SQL = SQL & "CURSOR c_emp_init IS "
                SQL = SQL & "SELECT emp_init "
                SQL = SQL & "FROM emp "
                SQL = SQL & "WHERE emp_cd = s_emp_cd; "
                SQL = SQL & "CURSOR c_company_group_cd IS "
                SQL = SQL & "SELECT co_grp_cd "
                SQL = SQL & "FROM co_grp "
                SQL = SQL & "WHERE co_cd = ( SELECT co_cd FROM store WHERE store_cd = "
                SQL = SQL & " ( SELECT home_store_cd FROM emp WHERE emp_cd = s_emp_cd) "
                SQL = SQL & "); "
                SQL = SQL & "BEGIN "
                SQL = SQL & "  OPEN c_emp_init; "
                SQL = SQL & "  FETCH c_emp_init INTO s_emp_init; "
                SQL = SQL & "  CLOSE c_emp_init; "
                SQL = SQL & "  OPEN c_company_group_cd; "
                SQL = SQL & "  FETCH c_company_group_cd INTO s_company_group_cd; "
                SQL = SQL & "  CLOSE c_company_group_cd; "
                '    -- ---------------------------------
                '    -- Brick user logic
                '    -- ---------------------------------
                SQL = SQL & "  IF STD_MULTI_CO.isBrickCoGrp(s_company_group_cd) = 'Y' THEN "
                SQL = SQL & "    IF STD_MULTI_CO.isBrickItm(s_itm_cd) = 'Y' THEN "
                'DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is a Brick item - return value = Y');
                '--RETURN 'Y';
                SQL = SQL & "      s_result := 'Y'; "
                SQL = SQL & "    END IF; "
                SQL = SQL & "  END IF; "


                '    -- ---------------------------------
                '   -- Leon user logic
                '  -- ---------------------------------
                SQL = SQL & "  IF STD_MULTI_CO.isLeonCoGrp(s_company_group_cd) = 'Y' THEN "
                SQL = SQL & "    IF STD_MULTI_CO.isLeonsItm(s_itm_cd) = 'Y' THEN "
                '-- corporate itm"
                'DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is a Leons item - return value = Y');
                SQL = SQL & "      IF STD_ROLE.isLeonsCorpStoreUsr(s_emp_init) = 'Y' "
                SQL = SQL & "         OR STD_ROLE.isLeonsHomeOfficeUsr(s_emp_init) = 'Y' "
                SQL = SQL & "         OR STD_ROLE.isLeonsFranchiseStrUsr(s_emp_init) = 'Y' THEN "
                'DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is a Leons(2) item - return value = Y');
                '  --RETURN 'Y';
                SQL = SQL & "          s_result := 'Y'; "
                SQL = SQL & "      END IF; "
                SQL = SQL & "  END IF;"
                SQL = SQL & "  IF STD_MULTI_CO.isLeonsFrItm(s_itm_cd) = 'Y' THEN "
                '  -- franchise itm
                '-- Note: Item maintenance screen is not release to
                '--       franchise user in general. only restricted user.
                SQL = SQL & "      IF STD_ROLE.isLeonsFrItmMaintUsr(s_emp_init) = 'Y' "
                SQL = SQL & "         OR STD_ROLE.isLeonsFranchiseStrUsr(s_emp_init) = 'Y' THEN "
                '          DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is a Leons Franchise item - return value = Y');
                '             --RETURN 'Y';
                SQL = SQL & "          s_result := 'Y'; "
                SQL = SQL & "      END IF; "
                SQL = SQL & "    END IF; "
                SQL = SQL & "  END IF; "

                '  --DBMS_OUTPUT.put_line('Item [' || s_itm_cd || '] is not a valid company item - return value = N');
                ' --RETURN 'N';
                '--end isValidItm;
                SQL = SQL & "  :x_result := s_result; "
                SQL = SQL & "END;"

                objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

                objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
                objsql.Parameters(":x_employeeCode").Value = s_employeeCode
                'Dim s_paramEmployeeCode As OracleParameter = New OracleParameter(":x_employeeCode", OracleType.VarChar, 255)
                's_paramEmployeeCode.Direction = ParameterDirection.Input
                'objsql.Parameters.Add(s_paramEmployeeCode)
                'objsql.Parameters(":x_employeeCode").Value = s_employeeCode

                objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
                objsql.Parameters(":x_itemCode").Value = s_itemCode
                'Dim s_paramItemCode As OracleParameter = New OracleParameter(":x_itemCode", OracleType.VarChar, 255)
                's_paramItemCode.Direction = ParameterDirection.Input
                'objsql.Parameters.Add(s_paramItemCode)
                'objsql.Parameters(":x_itemCode").Value = s_itemCode

                'objsql.Parameters.Add(":x_result", OracleType.VarChar)
                'objsql.Parameters(":x_result").Value = s_result
                Dim s_resultParameter As OracleParameter = New OracleParameter(":x_result", OracleType.VarChar, 255)
                s_resultParameter.Direction = ParameterDirection.Output
                objsql.Parameters.Add(s_resultParameter)

                objsql.ExecuteNonQuery()

                s_result = s_resultParameter.Value

            End If

        End If

        IsValidItemSecurityCheck = s_result

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           isValidItem()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Confirms if item is a valid item - it exists!
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function isValidItem(p_itemCode As String) As Boolean
        'Daniela return if item is null - Dec 29, 2016
        If isEmpty(p_itemCode) Then
            Dim s_errorMessage As String = ""
            Dim s_employeeCode = Session("EMP_CD")

            s_errorMessage = String.Format(Resources.CustomMessages.MSG0002, p_itemCode)
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return False
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim s_itemCode As String = p_itemCode.ToUpper
        Dim s_styleCode As String = ""
        Dim s_categoryCode As String = ""
        Dim s_minorCode As String = ""

        Dim b_Continue As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT i.des "
        SQL = SQL & "FROM itm i "
        SQL = SQL & "WHERE i.itm_cd = UPPER(:x_itemCode) "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = s_itemCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

                'txt_des.Text = Mydatareader.Item("DES").ToString

            Else

                Dim s_errorMessage As String = ""
                Dim s_employeeCode = Session("EMP_CD")

                's_errorMessage = "ERROR: Item '" + s_itemCode + "' does not exist"
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0002, s_itemCode)
                'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_Continue

    End Function

    'JAM v2.2: *NEW*
    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           isValidPackageItem()
    '   Created by:     John Melenko
    '   Date:           Mar2015
    '   Description:    Confirms if the passed in package item is a valid package - all components are valid.
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function isValidPackageItem(p_packageItemCode As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim s_packageItemCode As String = p_packageItemCode.ToUpper

        Dim b_Continue As Boolean = True

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT p.pkg_itm_cd, p.cmpnt_itm_cd "
        SQL = SQL & "FROM package p "
        SQL = SQL & "WHERE p.pkg_itm_cd = UPPER(:x_packageItemCode) "
        SQL = SQL & "AND NOT EXISTS( "
        SQL = SQL & "  SELECT 'x' FROM itm i "
        SQL = SQL & "  WHERE i.itm_cd = p.cmpnt_itm_cd)"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_packageItemCode", OracleType.VarChar)
        objsql.Parameters(":x_packageItemCode").Value = s_packageItemCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then
                If (Mydatareader.HasRows) Then

                    b_Continue = False
                    Dim s_errorMessage As String = ""

                    's_errorMessage = "ERROR: Package item '" + s_packageItemCode + "' contains invalid components. Contact Purchasing."
                    s_errorMessage = String.Format(Resources.CustomMessages.MSG0003, s_packageItemCode)
                    'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                    'lbl_resultsInfo.Text = s_errorMessage
                    'lbl_resultsInfo.ForeColor = Color.Red
                    DisplayMessage(s_errorMessage, "LABEL", "ERROR")
                Else
                    b_Continue = True
                End If
            End If

            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_Continue

    End Function
    'Alice added on Aug 22, 2018. 
    Private Function isValidComponentPackageItem(p_ComponentPackageItemCode As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String



        Dim b_Continue As Boolean = True

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT p.pkg_itm_cd, p.cmpnt_itm_cd "
        SQL = SQL & "FROM package p, itm i "
            SQL = SQL & "WHERE p.cmpnt_itm_cd = UPPER(:x_componentPackageItemCode) "
            SQL = SQL & "AND p.pkg_itm_cd = i.itm_cd And i.itm_tp_cd = 'PKG' "


            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_componentPackageItemCode", OracleType.VarChar)
        objsql.Parameters(":x_componentPackageItemCode").Value = p_ComponentPackageItemCode.ToUpper

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then
                If (Not Mydatareader.HasRows) Then

                    b_Continue = False
                    Dim s_errorMessage As String = ""

                    's_errorMessage = "ERROR: Package item '" + s_packageItemCode + "' contains invalid components. Contact Purchasing."
                    s_errorMessage = String.Format(Resources.CustomMessages.MSG0003, p_ComponentPackageItemCode.ToUpper)
                    'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                    'lbl_resultsInfo.Text = s_errorMessage
                    'lbl_resultsInfo.ForeColor = Color.Red
                    DisplayMessage(s_errorMessage, "LABEL", "ERROR")
                Else
                    b_Continue = True
                End If
            End If

            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return b_Continue

    End Function


    'JAM v2.2: *NEW* (END)

    'JAM v2.3: *NEW*
    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           belongsToBrickCompanyGroup()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Checks to see if user belongs to the Brick company group
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function belongsToBrickCompanyGroup() As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim b_Continue As Boolean = False

        Dim s_companyCode As String = Session("CO_CD")
        Dim s_employeeCode As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        If (Not (s_companyCode Is Nothing)) AndAlso s_companyCode.isNotEmpty Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            SQL = "SELECT std_multi_co.isBrickCoGrp(cg.co_grp_cd) as brick_company_group "
            SQL = SQL & "FROM co_grp cg "
            SQL = SQL & "WHERE cg.co_cd = UPPER(:x_companyCode) "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_companyCode", OracleType.VarChar)
            objsql.Parameters(":x_companyCode").Value = s_companyCode

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then

                    If (Mydatareader.Item("brick_company_group").ToString.Equals("Y")) Then
                        b_Continue = True
                    End If
                    Mydatareader.Close()

                End If

            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

        End If

        'If (Not b_Continue) Then
        'Dim s_errorMessage As String = ""
        's_errorMessage = "ERROR: Employee does not belong to Brick company group - Employee [" + s_employeeCode + "]"
        'DisplayMessage(s_errorMessage, "LABEL", "ERROR")
        'End If

        Return b_Continue

    End Function
    
    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           hasAccess()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Checks to see if user has access
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function hasAccess(p_securityCode As String) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim b_Continue As Boolean = False
        Dim s_result As String = "N"
        Dim b_hasAccess As Boolean = False

        If (String.IsNullOrEmpty(Trim(p_securityCode.ToString()))) Then

            Dim s_errorMessage As String = ""

            's_errorMessage = "ERROR: Security code must have a value to determine access."
            s_errorMessage = Resources.CustomMessages.MSG0004
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return False
        End If

        Dim s_employeeCode As String = Session("EMP_CD")
        'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

        If (Not (s_employeeCode Is Nothing)) AndAlso s_employeeCode.isNotEmpty Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            SQL = "DECLARE "
            SQL = SQL & "b_result BOOLEAN := FALSE; "
            SQL = SQL & "s_result VARCHAR(1) := 'N'; "
            SQL = SQL & "BEGIN "
            SQL = SQL & "  b_result := bt_security.has_security( :x_employeeCode, :x_securityCode ); "
            SQL = SQL & "  IF b_result THEN "
            SQL = SQL & "      s_result := 'Y'; "
            SQL = SQL & "  END IF; "
            SQL = SQL & "  :x_result := s_result; "
            SQL = SQL & "END;"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
            objsql.Parameters(":x_employeeCode").Value = s_employeeCode

            objsql.Parameters.Add(":x_securityCode", OracleType.VarChar)
            objsql.Parameters(":x_securityCode").Value = p_securityCode

            Dim s_resultParameter As OracleParameter = New OracleParameter(":x_result", OracleType.VarChar, 255)
            s_resultParameter.Direction = ParameterDirection.Output
            objsql.Parameters.Add(s_resultParameter)

            objsql.ExecuteNonQuery()

            s_result = s_resultParameter.Value

            conn.Close()

        End If

        If (s_result.Equals("Y")) Then
            b_hasAccess = True
        End If
        hasAccess = b_hasAccess

    End Function
   

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           canViewLandedCost()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Checks to see if user has access to view landed cost
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function canViewLandedCost() As Boolean

        Dim b_Continue As Boolean = False
        'Daniela Dec 1  allow Leon's to see landed cost
        'If (belongsToBrickCompanyGroup()) Then

        If (hasAccess("IPQPOSBRC") OrElse hasAccess("IPQPOSFRC")) Then
            b_Continue = True
        End If

        'Else
        'Dim s_errorMessage As String = ""
        's_errorMessage = "ERROR: Employee not allowed to see landed cost - Employee [" + s_employeeCode + "]"
        'DisplayMessage(s_errorMessage, "LABEL", "ERROR")
        'End If

        Return b_Continue

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           canViewStaffPrice()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Checks to see if user has access to view staff price
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function canViewStaffPrice() As Boolean

        Dim b_Continue As Boolean = False
        'Daniela Dec 1  allow Leon's to see StaffPrice
        'If (belongsToBrickCompanyGroup()) Then

        If (hasAccess("IPQPOSSTA")) Then
            b_Continue = True
        End If

        'Else
        'Dim s_errorMessage As String = ""
        's_errorMessage = "ERROR: Employee not allowed to see staff price - Employee [" + s_employeeCode + "]"
        'DisplayMessage(s_errorMessage, "LABEL", "ERROR")
        'End If

        Return b_Continue

    End Function
    Function GetWhseStr(ByVal p_str As String) As String

        'sabrina new function - Req2857 
        Dim v_str As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT whse_store_cd str from frtbl " &
                            " where store_cd = '" & p_str & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_str = dbReader.Item("str")
            Else
                v_str = ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_str & "" = "" Then
            Return ""
        Else
            Return v_str
        End If

    End Function
    Function GetMarkupPct() As Decimal

        'sabrina new function - Req2857 
        Dim v_pct As Decimal = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = " select nvl(p.fran_mrkup_pcnt,0) pct " &
                            " from prod_grp p " &
                            " ,itm i, inv_mnr im, inv_mjr ij " &
                            " where i.itm_cd = '" & txt_sku.Text & "' " &
                            " and i.mnr_cd = im.mnr_cd " &
                            " and im.mjr_cd = ij.mjr_cd " &
                            " and p.prod_grp_cd = nvl(ij.prod_grp_cd,'x') "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_pct = dbReader.Item("pct")
            Else
                v_pct = ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If IsDBNull(v_pct) Then
            Return 0
        Else
            Return v_pct
        End If

    End Function
    'sabrina commented out function - Req2857
    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           calculateLandedCost()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Calculates landed cost
    '   Called by:      
    ''------------------------------------------------------------------------------------------------------------------------------------
    'Private Function calculateLandedCost(ByVal p_str As String) As Decimal

    '    Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

    '    Dim SQL As String

    '    Dim b_Continue As Boolean = True
    '    Dim n_landedCost As Decimal
    '    Dim n_bareCost As Decimal = 0
    '    Dim n_vendorUpcharge As Decimal = 0
    '    Dim n_freightFactor As Decimal = 0


    '    n_bareCost = retrieveBareCost(p_str, txt_sku.Text)
    '    n_vendorUpcharge = retrieveVendorUpcharge(txt_sku.Text)
    '    n_freightFactor = retrieveFreightFactor(p_str, txt_sku.Text)

    '    If (n_freightFactor < 0) Then
    '        'ERROR
    '        n_landedCost = -1

    '        Dim s_errorMessage As String = ""
    '        's_errorMessage = "Landed cost could not be calculated, freight factor negative?"
    '        s_errorMessage = Resources.CustomMessages.MSG0005
    '        'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
    '        'lbl_resultsInfo.Text = s_errorMessage
    '        'lbl_resultsInfo.ForeColor = Color.Red
    '        DisplayMessage(s_errorMessage, "LABEL", "ERROR")

    '    Else
    '        'n_landedCost = calculateBareCost(txt_store_cd.Text, txt_sku.Text) * (1 + calculateVendorUpcharge(txt_sku.Text) / 100) * (1 + calculateFreightFactor(txt_store_cd.Text, txt_sku.Text) / 100)
    '        n_landedCost = n_bareCost * (1 + n_vendorUpcharge / 100) * (1 + n_freightFactor / 100)
    '    End If

    '    Return n_landedCost

    'End Function
   



    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           retrieveBareCost()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Retrieves bare cost for an item in a given store
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function retrieveBareCost(p_storeCode As String, p_itemCode As String) As Decimal

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim b_Continue As Boolean = True
        Dim n_bareCost As Decimal = 0

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        SQL = "SELECT i.itm_cd, i."
        SQL = SQL & Resources.CustomMessages.MSG0080
        SQL = SQL & ", i.ve_cd, i.mnr_cd, sgrc1.store_grp_cd, NVL(sgrc1.repl_cst, 0) as bare_cost, sgrc1.dt "
        SQL = SQL & "FROM itm i, store_grp_repl_clndr sgrc1, store_grp$store sgs1, store_grp sg1, "
        SQL = SQL & "  (SELECT i.itm_cd, sgrc2.store_grp_cd, MAX(sgrc2.dt) as max_date "
        SQL = SQL & "  FROM itm i, store_grp_repl_clndr sgrc2, store_grp$store sgs2, store_grp sg2 "
        SQL = SQL & "  WHERE i.itm_cd = sgrc2.itm_cd "
        'SQL = SQL & "  AND sgrc2.store_grp_cd = 'CRE' "
        SQL = SQL & "  AND sgrc2.store_grp_cd = sg2.store_grp_cd "
        SQL = SQL & "  AND sgs2.store_grp_cd = sg2.store_grp_cd "
        SQL = SQL & "  AND sgs2.store_cd = :x_storeCode "
        SQL = SQL & "  AND NVL(sg2.pricing_store_grp,'N') = 'Y' "
        SQL = SQL & "  AND sgrc2.dt <= TRUNC(SYSDATE) "
        SQL = SQL & "  AND i.itm_cd = :x_itemCode "
        SQL = SQL & "  GROUP BY i.itm_cd, sgrc2.store_grp_cd) maxrec "
        SQL = SQL & "WHERE i.itm_cd = sgrc1.itm_cd "
        'SQL = SQL & "AND sgrc1.store_grp_cd = 'CRE' "
        SQL = SQL & "  AND sgrc1.store_grp_cd = sg1.store_grp_cd "
        SQL = SQL & "  AND sgs1.store_grp_cd = sg1.store_grp_cd "
        SQL = SQL & "  AND sgs1.store_cd = :x_storeCode "
        SQL = SQL & "  AND NVL(sg1.pricing_store_grp,'N') = 'Y' "
        SQL = SQL & "AND i.itm_cd = :x_itemCode "
        SQL = SQL & "AND maxrec.max_date = sgrc1.dt "
        SQL = SQL & "AND maxrec.store_grp_cd = sgrc1.store_grp_cd "
        SQL = SQL & "AND maxrec.itm_cd = sgrc1.itm_cd "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = p_itemCode

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

                n_bareCost = Mydatareader.Item("bare_cost").ToString

                'Else

                '    Dim s_errorMessage As String = ""
                '    Dim s_employeeCode = Session("EMP_CD")

                '    s_errorMessage = "ERROR: Bare cost cannot be found for item [" + p_itemCode + "] and store [" + p_storeCode + "] - Employee [" + s_employeeCode + "]"
                '    'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                '    'lbl_resultsInfo.Text = s_errorMessage
                '    'lbl_resultsInfo.ForeColor = Color.Red
                '    DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return n_bareCost

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           retrieveVendorUpcharge()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Retrieves vendor upcharge for an item
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function retrieveVendorUpcharge(p_itemCode As String) As Decimal

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_Continue As Boolean = True
        Dim n_vendorUpcharge As Decimal = 0


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand


        SQL = "SELECT i.itm_cd, i.des, i.ve_cd, i.mnr_cd, NVL(v.up_chg, 0) as vendor_upcharge "
        SQL = SQL & "FROM itm i, ve2up_chg v "
        SQL = SQL & "WHERE i.ve_cd = v.ve_cd(+) "
        SQL = SQL & "AND i.itm_cd = :x_itemCode "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = p_itemCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

                n_vendorUpcharge = Mydatareader.Item("vendor_upcharge").ToString

            Else
                'do nothing, vendor upcharge remains at zero

                'Dim s_errorMessage As String = ""
                'Dim s_employeeCode = Session("EMP_CD")

                's_errorMessage = "ERROR: Vendor upcharge cannot be found for item [" + p_itemCode + "] - Employee [" + s_employeeCode + "]"
                'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                'DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return n_vendorUpcharge

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           retrieveFreightFactor()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Retrieves freight factor for an item in a given store
    '   Called by:      
    '   NOTES:
    '   If no freight factor can be found for the given store, a warning message is displayed.
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function retrieveFreightFactor(p_storeCode As String, p_itemCode As String) As Decimal

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_Continue As Boolean = True
        Dim n_freightFactor As Decimal = 0

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        Dim s_freightFactorTable As String
        Dim s_companyCode As String = Session("CO_CD")

        'sabrina Req2857
        s_freightFactorTable = "frt2store"
        'If (s_companyCode.Equals("BW1")) Then
        '    s_freightFactorTable = "frt2store"
        'Else
        '    s_freightFactorTable = "book_frt2store"
        'End If
        'sabrina Req2857 end

        SQL = "SELECT i.itm_cd, i.des, i.ve_cd, i.mnr_cd, f.store_cd, NVL(f.frt_fac, 0) as freight_factor, f.dt "
        SQL = SQL & "FROM itm i, " & s_freightFactorTable & " f "
        SQL = SQL & "WHERE i.mnr_cd = f.mnr_cd(+) "
        SQL = SQL & "AND f.store_cd(+) = :x_storeCode "
        SQL = SQL & "AND i.itm_cd = :x_itemCode "

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = p_itemCode

        Dim s_errorMessage As String = ""
        Dim s_employeeCode = Session("EMP_CD")

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

                n_freightFactor = Mydatareader.Item("freight_factor").ToString

                If (String.IsNullOrEmpty(Trim(Mydatareader.Item("store_cd").ToString()))) Then

                    'No freight factor exists for the store, display error message and landed cost cannot be displayed

                    's_errorMessage = "Freight factor was not found for item [" + p_itemCode + "] and store [" + p_storeCode + "] - Employee [" + s_employeeCode + "]"
                    's_errorMessage = "Freight factor was not found for item [" + p_itemCode + "] and store [" + p_storeCode + "]"
                    s_errorMessage = String.Format(Resources.CustomMessages.MSG0006, p_itemCode, p_storeCode)
                    'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                    'lbl_resultsInfo.Text = s_errorMessage
                    'lbl_resultsInfo.ForeColor = Color.Red
                    DisplayMessage(s_errorMessage, "LABEL", "WARNING")

                    n_freightFactor = 0

                End If

            Else

                's_errorMessage = "ERROR: (2) Freight factor cannot be found for item [" + p_itemCode + "] and store [" + p_storeCode + "] - Employee [" + s_employeeCode + "]"
                's_errorMessage = "Freight factor does not exist for item [" + p_itemCode + "] and store [" + p_storeCode + "]"
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0007, p_itemCode, p_storeCode)
                'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                DisplayMessage(s_errorMessage, "LABEL", "WARNING")
                n_freightFactor = 0
            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return n_freightFactor

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           calculateStaffPrice()
    '   Created by:     John Melenko
    '   Date:           Apr2015
    '   Description:    Calculates staff price - remains as WIP
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function calculateStaffPrice() As Decimal

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_Continue As Boolean = True
        Dim n_staffPrice As Decimal = 0

        Return n_staffPrice

    End Function
    Sub CalculateCost(ByVal p_str As String, ByVal p_markup_ind As String)

        'sabrina new sub - Req2857
        Dim n_landedCost As Decimal
        Dim n_bareCost As Decimal = 0
        Dim n_vendorUpcharge As Decimal = 0
        Dim n_freightFactor As Decimal = 0
        Dim n_markup_pct As Decimal = 0

        'txt_fran_cost.Text = ""
        'txt_landed_cost.Text = ""

        n_bareCost = retrieveBareCost(p_str, txt_sku.Text)
        n_vendorUpcharge = retrieveVendorUpcharge(txt_sku.Text)
        n_freightFactor = retrieveFreightFactor(p_str, txt_sku.Text)

        If (n_freightFactor < 0) Then
            n_landedCost = -1
        Else
            n_landedCost = n_bareCost * (1 + n_vendorUpcharge / 100) * (1 + n_freightFactor / 100)
        End If

        If LeonsBiz.isBrickCorpStr(p_str) = "Y" Or LeonsBiz.isLeonCorpStr(p_str) = "Y" Then
            If n_landedCost < 0 Then
                txt_landed_cost.Text = "ERROR"
            Else
                If p_markup_ind = "Y" Then
                    If LeonsBiz.isLeonCorpStr(p_str) = "Y" Then
                        n_markup_pct = GetMarkupPct()
                        n_landedCost = n_landedCost + (n_landedCost * n_markup_pct) / 100
                    End If
                End If
                txt_landed_cost.Text = FormatNumber(n_landedCost.ToString, 2)
            End If
        ElseIf LeonsBiz.isBrickFranStr(p_str) = "Y" Or LeonsBiz.isLeonFranStr(p_str) = "Y" Then
            If n_landedCost < 0 Then
                txt_fran_cost.Text = "ERROR"
            Else
                txt_fran_cost.Text = FormatNumber(n_landedCost.ToString, 2)
            End If
        Else
            txt_fran_cost.Text = ""
            txt_landed_cost.Text = ""
        End If

    End Sub

#Region "Calculate commercial cost"
    'Alice added for request 387
    'show Commercial Cost when user has security and the item's vendor is setup in the Commercial Vendor Percentages for user at either their co_cd level (if no record) then check co_grp_cd level.  Both have to exist before showing this cost

    Sub setupCommercialCost()
        Dim v_corp_str As String = ""
        Dim s_errorMessage As String = ""

        If txt_store_cd.Text & "" = "" Or txt_sku.Text & "" = "" Then
            's_errorMessage = "Item code, store code are required to retrieve Cost information."
            s_errorMessage = Resources.CustomMessages.MSG0116
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Exit Sub
        End If
        If hasAccess("IPQCOMMCST") And VendorExistInCommVendorPct(txt_sku.Text.Trim()) Then
            Dim bareCost As Decimal = 0
            Dim commCost As Decimal = 0
            Dim down_pct As Decimal = 0
            bareCost = retrieveBareCost(txt_store_cd.Text, txt_sku.Text)
            down_pct = retrieveMarkdown_PCT(txt_sku.Text, txt_store_cd.Text, dateEdit.Text)
            If down_pct <> 0 Then
                commCost = bareCost - (bareCost * (down_pct / 100))
                txt_comm_cost.Text = FormatNumber(commCost.ToString, 2)
            End If

        End If

    End Sub

    Public Function VendorExistInCommVendorPct(ByVal sku As String) As Boolean

        Dim existsInDB As Boolean = False
        Dim dbSql As String = "select * from ITM where ITM_CD=:ITM_CD and VE_CD in (select VE_CD from COMMERCIAL_VENDOR_MARKDOWN_PCT)"
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbSql, conn)
        Dim dbReader As OracleDataReader

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()
            dbCommand.Parameters.Add(":ITM_CD", OracleType.VarChar)
            dbCommand.Parameters(":ITM_CD").Value = sku

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                existsInDB = True
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
        Return existsInDB

    End Function
    Private Function retrieveMarkdown_PCT(p_itemCode As String, p_storeCode As String, p_date As String) As Decimal

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim b_Continue As Boolean = True
        Dim n_markdown_PCT As Decimal = 0


        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand


        SQL = "select cvmp.down_pct from COMMERCIAL_VENDOR_MARKDOWN_PCT cvmp, store_detail sd "
        SQL = SQL & "where cvmp.co_grp_cd=sd.co_grp and (cvmp.co_cd=sd.co_cd or trim(cvmp.co_cd) is null) and sd.store_cd=:x_storeCode "
        SQL = SQL & "and cvmp.ve_cd in (select ve_cd from itm where itm_cd=:x_itemCode) "
        SQL = SQL & "and :x_date between cvmp.start_dt and cvmp.end_dt and rownum = 1"


        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = p_itemCode

        objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
        objsql.Parameters(":x_storeCode").Value = p_storeCode

        objsql.Parameters.Add(":x_date", OracleType.DateTime)
        objsql.Parameters(":x_date").Value = p_date

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            If Mydatareader.Read() Then
                n_markdown_PCT = Mydatareader.Item("down_pct").ToString
            End If

            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return n_markdown_PCT

    End Function
#End Region

    Sub setupLandedCost()
        'sabrina new sub
        Dim v_corp_str As String = ""
        Dim s_errorMessage As String = ""

        If txt_store_cd.Text & "" = "" Or txt_sku.Text & "" = "" Then
            's_errorMessage = "Item code, store code are required to retrieve Cost information."
            s_errorMessage = Resources.CustomMessages.MSG0116
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Exit Sub
        End If
        If hasAccess("IPQPOSBRC") Then
            If LeonsBiz.isBrickCorpStr(txt_store_cd.Text) = "Y" Or
               LeonsBiz.isLeonCorpStr(txt_store_cd.Text) = "Y" Then
               
                CalculateCost(txt_store_cd.Text, "N")

            ElseIf LeonsBiz.isBrickFranStr(txt_store_cd.Text) = "Y" Or
                   LeonsBiz.isLeonFranStr(txt_store_cd.Text) = "Y" Then
                
                v_corp_str = GetWhseStr(txt_store_cd.Text)
                If IsDBNull(v_corp_str) Then
                    txt_landed_cost.Text = ""
                    s_errorMessage = String.Format(Resources.CustomMessages.MSG0117)
                    DisplayMessage(s_errorMessage, "LABEL", "WARNING")
                Else
                     
                    CalculateCost(v_corp_str, "Y") 'using whsestore also calc. markup for leons str
                End If

                CalculateCost(txt_store_cd.Text, "N") 'using fran. store
            End If
        ElseIf hasAccess("IPQPOSFRC") Then
             

            If LeonsBiz.isLeonFranStr(txt_store_cd.Text) = "Y" Or
               LeonsBiz.isBrickFranStr(txt_store_cd.Text) = "Y" Then
               
                CalculateCost(txt_store_cd.Text, "N")
            Else
                'do nothing
            End If
        Else
            'do nothing
        End If


    End Sub
    'JAM v2.3: *NEW* (END)


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateItem()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Main procedure which populates the item information based on query parameters entered by the user.
    '   Called by:      "Lookup Prices" button - btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateItem(p_itemCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim s_itemCode As String = p_itemCode.ToUpper
        Dim s_styleCode As String = ""
        Dim s_categoryCode As String = ""
        Dim s_minorCode As String = ""

        Dim b_Continue As Boolean = False

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        SQL = "SELECT i."
        SQL = SQL & Resources.CustomMessages.MSG0080
        SQL = SQL & " des, i.vsn, i.itm_tp_cd, i.drop_cd, i.cat_cd, i.style_cd, i.comm_cd, i.related_itm_cd, i.warrantable, i.treatable, i.ret_prc, "
        'JAM v2.1: added dimensions
        SQL = SQL & "i.siz_id as width, i.cover_id as depth, i.grade_id as height, i.weight as weight, "
        'JAM v2.1: added dimensions
        'JAM v2.2: added package split method
        SQL = SQL & "i.pkg_split_method, "
        'JAM v2.2: added package split method
        SQL = SQL & "v.ve_name, mjr.mjr_cd || ' - ' || mjr.des as major_des, mnr.mnr_cd as minor_code, mnr.mnr_cd || ' - ' || mnr.des as minor_des "
        SQL = SQL & "FROM itm i, ve v, inv_mnr mnr, inv_mjr mjr "   ' , inv_mnr_cat cat "
        SQL = SQL & "WHERE i.ve_cd = v.ve_cd "
        SQL = SQL & "AND i.mnr_cd = mnr.mnr_cd "
        SQL = SQL & "AND mnr.mjr_cd = mjr.mjr_cd "
        'JAM v2.2: removed UPPER on itm_cd
        'SQL = SQL & "AND UPPER(i.itm_cd) = UPPER(:x_itemCode) "
        SQL = SQL & "AND i.itm_cd = UPPER(:x_itemCode) "
        'JAM v2.2 (END)

        'SQL = SQL & "AND i.cat_cd = cat.cat_cd "
        'SQL = SQL & "AND mnr.mnr_cd = cat.mnr_cd"

        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
        objsql.Parameters(":x_itemCode").Value = s_itemCode

        Try
            'Execute DataReader 
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            'Store Values in String Variables 
            If Mydatareader.Read() Then

                b_Continue = True

                txt_sku.Text = txt_sku.Text.ToUpper 'force uppercase on what user entered

                txt_des.Text = Mydatareader.Item("DES").ToString
                txt_vsn.Text = Mydatareader.Item("VSN").ToString
                txt_itm_tp_cd.Text = Mydatareader.Item("ITM_TP_CD").ToString
                txt_drop_cd.Text = Mydatareader.Item("DROP_CD").ToString
                txt_cat_cd.Text = Mydatareader.Item("CAT_CD").ToString
                txt_style_cd.Text = Mydatareader.Item("STYLE_CD").ToString
                txt_comm_cd.Text = Mydatareader.Item("COMM_CD").ToString
                txt_related_itm_cd.Text = Mydatareader.Item("RELATED_ITM_CD").ToString
                txt_warrantable.Text = Mydatareader.Item("WARRANTABLE").ToString
                txt_treatable.Text = Mydatareader.Item("TREATABLE").ToString
                txt_ret_prc.Text = Mydatareader.Item("RET_PRC").ToString

                txt_ve_name.Text = Mydatareader.Item("VE_NAME").ToString
                txt_major_des.Text = Mydatareader.Item("MAJOR_DES").ToString
                txt_minor_des.Text = Mydatareader.Item("MINOR_DES").ToString
                'JAM v2.1: combine category code and description -- removed category from select and retrieved separately
                'txt_cat_des.Text = Mydatareader.Item("CAT_DES").ToString
                'txt_cat_des.Text = txt_cat_cd.Text + " - " + Mydatareader.Item("CAT_DES").ToString
                s_categoryCode = txt_cat_cd.Text

                'txt_style_des.Text = Mydatareader.Item("STYLE_DES").ToString
                s_minorCode = Mydatareader.Item("MINOR_CODE").ToString
                s_styleCode = txt_style_cd.Text

                'JAM v2.1: added dimensions
                txt_width.Text = Mydatareader.Item("WIDTH").ToString
                txt_depth.Text = Mydatareader.Item("DEPTH").ToString
                txt_height.Text = Mydatareader.Item("HEIGHT").ToString
                txt_weight.Text = Mydatareader.Item("WEIGHT").ToString

                'JAM v2.2: added package split method
                txt_pkg_split_method.Text = Mydatareader.Item("PKG_SPLIT_METHOD").ToString
                'JAM v2.2 (END)

            Else

                'MsgBox("Invalid Item [" + s_itemCode + "]", Title:="Error", MsgBoxStyle.Exclamation)
                'Response.Write("<script type=""text/javascript"">alert(""Invalid Item [" + s_itemCode + "]"");</script")

                Dim s_errorMessage As String = ""
                Dim s_employeeCode = Session("EMP_CD")

                's_errorMessage = "ERROR: Invalid Item [" + s_itemCode + "] - Employee [" + s_employeeCode + "]"
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0008, s_itemCode, s_employeeCode)
                'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            End If
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        If b_Continue Then

            ' populate the style description
            PopulateStyleDescription(s_styleCode, s_minorCode)

            ' populate the category description
            PopulateCategoryDescription(s_categoryCode, s_minorCode)

            ' populate the SKU sort codes
            PopulateSKUSortCodes(s_itemCode)

            PopulateSPIFFCalendar(s_itemCode, txt_store_cd.Text, dateEdit.Text)

            ' populate the item price -- also sets the item's Promo code
            'TURNED OFF - PopulatePriceLeonsProc(txt_store_cd.Text, s_itemCode, dateEdit.Text)
            PopulatePriceJDAProc(txt_store_cd.Text, s_itemCode, dateEdit.Text)

            ' populate the promo code's description
            '**NOT DONE HERE** see PopulatePricingGrid() - PopulatePromoDescription(txt_promo_cd.Text, dateEdit.Text)

            ' populate pricing grid
            PopulatePricingGrid(txt_store_cd.Text, s_itemCode, dateEdit.Text)

            'JAM v2.2: populate package component gridviews
            ' Dec 29, 2016 prevent yellow triangle if store nullm - Daniela
            If isNotEmpty(txt_store_cd.Text) Then
                Try
                    If txt_itm_tp_cd.Text = "PKG" Then
                        PopulatePackageComponentsGridview()
                    End If

                    If txt_itm_tp_cd.Text = "INV" Then
                        PopulatePackageGridview()
                    End If
                Catch 'Daniela prevent system error
                End Try
            End If
            'JAM v2.2 (END)

            'JAM v2.3: added landed cost and staff price

            'sabrina Req2857
            txt_fran_cost.Text = ""
            txt_landed_cost.Text = ""
            setupLandedCost()
            setupCommercialCost()

            'JAM Apr2015: turned off until business logic is finalized
            'If (canViewStaffPrice()) Then
            'txt_staff_price.Text = calculateStaffPrice.ToString
            'lbl_staff_price.Visible = True
            'txt_staff_price.Visible = True
            'End If
            'JAM v2.3 (END)

        End If

    End Sub

    Private Sub RetrieveIdentificationInfo(p_itemCode As String, p_storeCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        Dim s_itemCode As String = p_itemCode.ToUpper
        Dim s_storeCode As String = p_storeCode.ToUpper
        Dim s_styleCode As String = ""
        Dim s_categoryCode As String = ""
        Dim s_minorCode As String = ""

        Dim b_Continue As Boolean = False

        If (String.IsNullOrEmpty(Trim(p_itemCode.ToString()))) Then

            Dim s_errorMessage As String = ""
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Item code required"
            s_errorMessage = Resources.CustomMessages.MSG0058
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        ElseIf (String.IsNullOrEmpty(Trim(p_storeCode.ToString()))) Then

            Dim s_errorMessage As String = ""
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            's_errorMessage = "ERROR: Store code required."
            s_errorMessage = Resources.CustomMessages.MSG0056
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand
            SQL = "select ii_cd from iicst where iicst.itm_cd = '" & p_itemCode & "' And iicst.st_cd = '" + txt_store_cd.Text + "'"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                While Mydatareader.Read()
                    lst_iic_desc.Items.Add(Mydatareader.Item("ii_cd").ToString)
                End While
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()
        End If
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulatePromoDescription()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which populates the promo description based on the provided promo code and date.
    '   Called by:      **NOT USED AT THIS TIME** - kept just in case need to re-use in future
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulatePromoDescription(p_promoCode As String, p_date As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        ''If (String.IsNullOrEmpty(Trim(p_itemCode.ToString())) OrElse String.IsNullOrEmpty(Trim(p_storeCode.ToString())) OrElse String.IsNullOrEmpty(Trim(p_date.ToString()))) Then
        If (Not (p_promoCode Is Nothing OrElse p_date Is Nothing)) AndAlso p_promoCode.isNotEmpty AndAlso p_date.isNotEmpty Then
            'TODO - change to this -- If Not (String.IsNullOrEmpty(Trim(p_categoryCode.ToString())) OrElse String.IsNullOrEmpty(Trim(p_minorCode.ToString()))) Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            SQL = "SELECT des "
            SQL = SQL & "FROM prom_prc_tp "
            SQL = SQL & "WHERE prc_cd = UPPER(:x_promoCode) "
            SQL = SQL & "AND :x_date BETWEEN eff_dt AND end_dt "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_promoCode", OracleType.VarChar)
            objsql.Parameters(":x_promoCode").Value = p_promoCode

            objsql.Parameters.Add(":x_date", OracleType.DateTime)
            objsql.Parameters(":x_date").Value = p_date

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then
                    txt_promo_des.Text = Mydatareader.Item("DES").ToString
                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

            'MsgBox("promo [" + txt_promo_cd.Text + "] des [" + txt_promo_des.Text + "]")
            If (txt_promo_des.Text Is Nothing OrElse txt_promo_des.Text.isEmpty) Then
                txt_promo_cd.Text = ""
            End If

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateStyleDescription()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which populates the style description based on the provided style code and minor code.
    '   Called by:      PopulateItem()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateStyleDescription(p_styleCode As String, p_minorCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        If (Not (p_styleCode Is Nothing OrElse p_minorCode Is Nothing)) AndAlso p_styleCode.isNotEmpty AndAlso p_minorCode.isNotEmpty Then
            'TODO - change to this -- If Not (String.IsNullOrEmpty(Trim(p_categoryCode.ToString())) OrElse String.IsNullOrEmpty(Trim(p_minorCode.ToString()))) Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            SQL = "SELECT des "
            SQL = SQL & "FROM inv_mnr_style "
            SQL = SQL & "WHERE style_cd = UPPER(:x_styleCode) "
            SQL = SQL & "AND mnr_cd = UPPER(:x_minorCode) "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_styleCode", OracleType.VarChar)
            objsql.Parameters(":x_styleCode").Value = p_styleCode
            objsql.Parameters.Add(":x_minorCode", OracleType.VarChar)
            objsql.Parameters(":x_minorCode").Value = p_minorCode

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then

                    'JAM v2.1: combine style code and description
                    'txt_style_des.Text = Mydatareader.Item("DES").ToString
                    txt_style_des.Text = p_styleCode + " - " + Mydatareader.Item("DES").ToString

                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateCategoryDescription()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which populates the category description based on the provided category code and minor code.
    '   Called by:      PopulateItem()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateCategoryDescription(p_categoryCode As String, p_minorCode As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        'If (Not (p_categoryCode Is Nothing OrElse p_minorCode Is Nothing)) AndAlso p_categoryCode.isNotEmpty AndAlso p_minorCode.isNotEmpty Then
        If Not (String.IsNullOrEmpty(Trim(p_categoryCode.ToString())) OrElse String.IsNullOrEmpty(Trim(p_minorCode.ToString()))) Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            SQL = "SELECT des "
            SQL = SQL & "FROM inv_mnr_cat "
            SQL = SQL & "WHERE cat_cd = UPPER(:x_categoryCode) "
            SQL = SQL & "AND mnr_cd = UPPER(:x_minorCode) "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_categoryCode", OracleType.VarChar)
            objsql.Parameters(":x_categoryCode").Value = p_categoryCode
            objsql.Parameters.Add(":x_minorCode", OracleType.VarChar)
            objsql.Parameters(":x_minorCode").Value = p_minorCode

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then

                    'JAM v2.1: combine category code and description
                    'txt_cat_des.Text = Mydatareader.Item("DES").ToString
                    txt_cat_des.Text = p_categoryCode + " - " + Mydatareader.Item("DES").ToString

                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateStoreCodes()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which populates the store code drop-down list.
    '   Called by:      Page_Load()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim SQL As String = ""

        'Dim s_co_cd As String = Session("CO_CD")
        'Dim s_employee_code As String = Session("EMP_CD")

        'Dim v_co_cd_sql As String = ""
        
        Dim userType As Double = -1

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd As String = Session("CO_CD")

        '----------------------------------
                'MsgBox("company code [" + s_co_cd + "] for employee code [" + s_employee_code + "]")

            If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
                SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc FROM store "
            End If

            If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then

            SQL = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_store_desc " +
                   "from MCCL_STORE_GROUP a, store s, store_grp$store b " +
                   "where a.user_id = :userType and " +
                   "s.store_cd = b.store_cd and " +
                   "a.store_grp_cd = b.store_grp_cd "
                ' Leons Franchise exception
                If isNotEmpty(userType) And userType = 5 Then
                    SQL = SQL & " and '" & coCd & "' = s.co_cd "
                End If
            End If

            SQL = SQL & " order by store_cd"

            'Set SQL OBJECT 
            cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, connErp)

            If (isNotEmpty(userType) And userType > 0) Then
                cmdGetCodes.Parameters.Add(":userType", OracleType.VarChar)
                cmdGetCodes.Parameters(":userType").Value = userType
            End If

            txt_store_cd.Items.Insert(0, GetLocalResourceObject("combo_select_store_code.Text").ToString())
            txt_store_cd.Items.FindByText(GetLocalResourceObject("combo_select_store_code.Text").ToString()).Value = ""
            txt_store_cd.SelectedIndex = 0

            Try
                With cmdGetCodes
                    .Connection = connErp
                    .CommandText = SQL
                End With

                connErp.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
                oAdp.Fill(ds)

                With txt_store_cd
                    .DataSource = ds
                    .DataValueField = "store_cd"
                    .DataTextField = "full_store_desc"
                    .DataBind()
                End With

                connErp.Close()

            Catch ex As Exception
                connErp.Close()
                Throw
            End Try

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateSKUSortCodes()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which populates the SKU sort codes drop-down list.
    '   Called by:      PopulateItem()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateSKUSortCodes(p_itemCode As String)

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim SQL As String

        If (Not (p_itemCode Is Nothing)) AndAlso p_itemCode.isNotEmpty Then

            SQL = "SELECT DISTINCT itm_srt_cd "
            SQL = SQL & "FROM itm$itm_srt "
            'JAM v2.2: removed UPPER on itm_cd
            'SQL = SQL & "WHERE UPPER(itm_cd) = UPPER(:x_itemCode) "
            SQL = SQL & "WHERE itm_cd = UPPER(:x_itemCode) "
            'JAM v2.2 (END)
            SQL = SQL & "ORDER BY itm_srt_cd "

            ' bind variables
            cmdGetCodes.Parameters.Add(":x_itemCode", OracleType.VarChar)
            cmdGetCodes.Parameters(":x_itemCode").Value = p_itemCode

            Try
                With cmdGetCodes
                    .Connection = connErp
                    .CommandText = SQL
                End With

                connErp.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
                oAdp.Fill(ds)

                With txt_itm_srt_cd
                    .DataSource = ds
                    .DataValueField = "itm_srt_cd"
                    .DataTextField = "itm_srt_cd"
                    .DataBind()
                End With

                connErp.Close()

            Catch ex As Exception
                connErp.Close()
                Throw
            End Try
        End If
    End Sub

    'JAM v2.1: new SPIFF addition

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateSPIFFCalendar()
    '   Created by:     John Melenko
    '   Date:           Feb2015
    '   Description:    Procedure which populates the SPIFF data
    '   Called by:      PopulateItem()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateSPIFFCalendar(p_itemCode As String, p_storeCode As String, p_date As String)

        'Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        If (String.IsNullOrEmpty(Trim(p_itemCode.ToString())) OrElse String.IsNullOrEmpty(Trim(p_storeCode.ToString())) OrElse String.IsNullOrEmpty(Trim(p_date.ToString()))) Then
            'SPIFF cannot be populated with an item code, store and date
            Dim s_errorMessage As String = ""

            's_errorMessage = "Item code, store code and date are required to retrieve SPIFF information."
            s_errorMessage = Resources.CustomMessages.MSG0009
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        Else

            SQL = "SELECT isc.spiff_cd, isc.spiff_cd || ' - ' || sc.des as spiff_des, LTRIM(TO_CHAR(isc.min_retail,'999999.99')) as spiff_min_retail, TO_CHAR(isc.eff_dt,'DD-MON-YYYY') as spiff_eff_date, TO_CHAR(isc.end_dt,'DD-MON-YYYY') as spiff_end_date, LTRIM(TO_CHAR(isc.spiff_amt,'999999.99')) as spiff_amount "
            SQL = SQL & "FROM itm$spiff_clndr isc, spiff_cd sc "
            SQL = SQL & "WHERE isc.spiff_cd = sc.spiff_cd "
            SQL = SQL & "AND isc.itm_cd = UPPER(:x_itemCode) "
            SQL = SQL & "AND isc.store_cd = :x_storeCode "
            SQL = SQL & "AND :x_date between isc.eff_dt and isc.end_dt "
            SQL = SQL & "ORDER BY isc.spiff_cd "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            ' bind variables
            objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
            objsql.Parameters(":x_itemCode").Value = p_itemCode.ToUpper

            objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
            objsql.Parameters(":x_storeCode").Value = p_storeCode

            objsql.Parameters.Add(":x_date", OracleType.DateTime)
            objsql.Parameters(":x_date").Value = p_date

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then

                    txt_spiff_cd.Text = Mydatareader.Item("SPIFF_CD").ToString
                    txt_spiff_des.Text = Mydatareader.Item("SPIFF_DES").ToString
                    txt_spiff_min_retail.Text = Mydatareader.Item("SPIFF_MIN_RETAIL").ToString
                    txt_spiff_eff_date.Text = Mydatareader.Item("SPIFF_EFF_DATE").ToString
                    txt_spiff_end_date.Text = Mydatareader.Item("SPIFF_END_DATE").ToString
                    txt_spiff_amount.Text = Mydatareader.Item("SPIFF_AMOUNT").ToString

                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

        End If
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulatePriceLeonsProc()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which retrieves the price of an item using the Leon's "STD_PRC.getPrc" database procedure.
    '   Called by:      PopulateItem() -- **NOT USED AT THE MOMENT** - retained for possible changes in the future
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulatePriceLeonsProc(p_storeCode As String, p_itemCode As String, p_date As String)

        Dim conn As OracleConnection
        Dim objsql As OracleCommand
        Dim sql As String = ""
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

        'Dim itemPriceRec As ItemPriceRecord

        If (Not (p_storeCode Is Nothing OrElse p_itemCode Is Nothing OrElse p_date Is Nothing)) AndAlso p_storeCode.isNotEmpty AndAlso p_itemCode.isNotEmpty AndAlso p_date.isNotEmpty AndAlso IsDate(p_date) Then

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            'Open Connection 
            conn.Open()

            sql = "STD_PRC.getPrc"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql.CommandType = CommandType.StoredProcedure

            objsql.Parameters.Add("p_str", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("p_itm", OracleType.VarChar).Direction = ParameterDirection.Input
            objsql.Parameters.Add("p_dt", OracleType.DateTime).Direction = ParameterDirection.Input
            objsql.Parameters.Add("p_prc", OracleType.Number).Direction = ParameterDirection.InputOutput
            objsql.Parameters.Add("p_prc_tp", OracleType.VarChar, 30).Direction = ParameterDirection.InputOutput

            objsql.Parameters("p_str").Value = p_storeCode
            objsql.Parameters("p_itm").Value = p_itemCode
            objsql.Parameters("p_dt").Value = p_date
            objsql.Parameters("p_prc").Value = 0
            objsql.Parameters("p_prc_tp").Value = ""
            objsql.ExecuteNonQuery()

            objsql.Cancel()
            objsql.Dispose()
            conn.Close()
            conn.Dispose()

            Dim s_itemPrice As String = ""
            Dim s_pricePromoCode As String = ""

            txt_pos_price.Text = ""
            txt_promo_cd.Text = ""

            If (Not IsDBNull(objsql.Parameters("p_prc").Value)) Then
                s_itemPrice = objsql.Parameters("p_prc").Value
                'txt_pos_price.Text = FormatCurrency(s_itemPrice, 2)
                txt_pos_price.Text = FormatNumber(s_itemPrice, 2)
            End If

            If (Not IsDBNull(objsql.Parameters("p_prc_tp").Value)) Then
                txt_promo_cd.Text = objsql.Parameters("p_prc_tp").Value
            End If

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulatePriceJDAProc()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which retrieves the price of an item using the JDA's "Get_Curr_Pricing()" procedure in
    '                   "./App_Code/Utils/HBCG_Utils.lib"
    '   Called by:      PopulateItem()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulatePriceJDAProc(p_storeCode As String, p_itemCode As String, p_date As String)

        If (Not (p_storeCode Is Nothing OrElse p_itemCode Is Nothing OrElse p_date Is Nothing)) AndAlso p_storeCode.isNotEmpty AndAlso p_itemCode.isNotEmpty AndAlso p_date.isNotEmpty AndAlso IsDate(p_date) Then

            Dim s_itemPrice As String = ""
            Dim s_pricePromoCode As String = ""
            Dim n_current_price As Double

            If (txt_ret_prc.Text.isEmpty) Then
                n_current_price = 0
            Else
                n_current_price = txt_ret_prc.Text
            End If

            txt_pos_price.Text = ""
            txt_promo_cd.Text = ""

            Dim currentPrice As Double
            currentPrice = Get_Curr_Pricing(p_itemCode, p_storeCode, p_date, n_current_price, s_pricePromoCode)

            If currentPrice = n_current_price Then
                'txt_pos_price.Text = FormatCurrency(n_current_price, 2)
                'txt_pos_price.Text = FormatNumber(n_current_price, 2)
                txt_pos_price.Text = n_current_price.ToString(".00", CultureInfo.InvariantCulture)
            Else
                'txt_pos_price.Text = FormatCurrency(currentPrice, 2)
                'txt_pos_price.Text = FormatNumber(currentPrice, 2)
                txt_pos_price.Text = currentPrice.ToString(".00", CultureInfo.InvariantCulture)
            End If

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulatePricingGrid()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which retrieves all Promo and regular retail pricing of an item for a given store and date. Populates
    '                   the pricing gridview. Lastly, populates the promo code and promo description when applicable.
    '   Called by:      PopulateItem()
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub PopulatePricingGrid(p_storeCode As String, p_itemCode As String, p_asOfDate As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim SQL As String
        Dim objsql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If (Not (p_storeCode Is Nothing OrElse p_itemCode Is Nothing OrElse p_asOfDate Is Nothing)) AndAlso p_storeCode.isNotEmpty AndAlso p_itemCode.isNotEmpty AndAlso p_asOfDate.isNotEmpty AndAlso IsDate(p_asOfDate) Then

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            '--------------------------------------------------------------------------
            '--  Promo and Retail pricing retrieval
            '--------------------------------------------------------------------------
            '--  Given an item code, store code and a date
            '--
            '--  PROMO: one data row for PROMO, EITHER:
            '--    A) store promo - price group is the store (query Promo-A) OR
            '--    B) store group promo - price group is the store-group (query Promo-B) OR
            '--    C) store promo calendar price (query Promo-C) 
            '--
            '--  RETAIL: multiple data rows for RETAIL:
            '--    A) store level retail price - price group is the store (query Retail-A) AND
            '--    B) store group level retail price - price group is the store-group (query Retail-B) AND
            '--    C) item level pricing (query Retail-C)

            '----------------------------
            '--(query Promo-A)
            SQL = "SELECT '1' as priority, s.store_cd as price_group, 'PROMO' as price_type, TO_CHAR(p.eff_dt,'DD-MON-YYYY') as eff_date, TO_CHAR(p.end_dt,'DD-MON-YYYY') as term_date, LTRIM(TO_CHAR(s.prc,'999,999.99')) as price "
            SQL = SQL & ", NVL(p.prc_cd,'') as prc_cd, NVL(p.des,'') as des, s.itm_cd "
            SQL = SQL & "FROM prom_prc_tp p, store_prom_prc s "
            SQL = SQL & "WHERE p.prc_cd = s.prc_cd "
            'JAM v2.2: removed UPPER on itm_cd amd store_cd
            'SQL = SQL & "AND UPPER(s.itm_cd) = UPPER(:x_itemCode) "
            'SQL = SQL & "AND UPPER(s.store_cd) = UPPER(:x_storeCode) "
            SQL = SQL & "AND s.itm_cd = UPPER(:x_itemCode) "
            SQL = SQL & "AND s.store_cd = UPPER(:x_storeCode) "
            'JAM v2.2 (END)

            SQL = SQL & "AND :x_asOfDate BETWEEN p.eff_dt AND p.end_dt "

            SQL = SQL & "UNION "

            '----------------------------
            '--(query Promo-B)
            SQL = SQL & "SELECT '2' as priority, sgs.store_grp_cd as price_group, 'PROMO' as price_type, TO_CHAR(p.eff_dt,'DD-MON-YYYY') as eff_date, TO_CHAR(p.end_dt,'DD-MON-YYYY') as term_date, LTRIM(TO_CHAR(sgpp.prc,'999,999.99')) as price "
            SQL = SQL & ",NVL(p.prc_cd,'') as prc_cd, NVL(p.des,'') as des, sgpp.itm_cd "
            SQL = SQL & "FROM store_grp$store sgs, store_grp sg, store_grp_prom_prc sgpp, prom_prc_tp p "
            SQL = SQL & "WHERE p.prc_cd = sgpp.prc_cd "
            'JAM v2.2: removed UPPER on itm_cd amd store_cd
            'SQL = SQL & "AND UPPER(sgpp.itm_cd) = UPPER(:x_itemCode) "
            'SQL = SQL & "AND UPPER(sgs.store_cd) = UPPER(:x_storeCode) "
            SQL = SQL & "AND sgpp.itm_cd = UPPER(:x_itemCode) "
            SQL = SQL & "AND sgs.store_cd = UPPER(:x_storeCode) "
            'JAM v2.2 (END)

            SQL = SQL & "AND :x_asOfDate BETWEEN p.eff_dt AND p.end_dt "
            SQL = SQL & "AND sgpp.store_grp_cd = sg.store_grp_cd "
            SQL = SQL & "AND sg.store_grp_cd = sgs.store_grp_cd "
            SQL = SQL & "AND sg.pricing_store_grp = 'Y' "

            SQL = SQL & "UNION "

            '----------------------------
            '--(query Promo-C)
            SQL = SQL & "SELECT '3' as priority, s.store_cd as price_group, 'PROMO' as price_type, TO_CHAR(s.eff_dt,'DD-MON-YYYY') as eff_date, TO_CHAR(s.end_dt,'DD-MON-YYYY') as term_date, LTRIM(TO_CHAR(i.prc,'999,999.99')) as price "
            SQL = SQL & ", NVL(i.prc_cd,'') as prc_cd, 'NO DESC' as des, i.itm_cd "
            SQL = SQL & "FROM itm$prom_prc_tp i, store$prom_clndr s "
            SQL = SQL & "WHERE s.prc_cd = i.prc_cd "

            'JAM v2.2: removed UPPER on itm_cd amd store_cd
            'SQL = SQL & "AND UPPER(i.itm_cd) = UPPER(:x_itemCode) "
            'SQL = SQL & "AND UPPER(s.store_cd) = UPPER(:x_storeCode) "
            SQL = SQL & "AND i.itm_cd = UPPER(:x_itemCode) "
            SQL = SQL & "AND s.store_cd = UPPER(:x_storeCode) "
            'JAM v2.2 (END)

            SQL = SQL & "AND :x_asOfDate BETWEEN s.eff_dt AND s.end_dt "

            SQL = SQL & "UNION "

            '----------------------------
            '--(query Retail-A)
            SQL = SQL & "SELECT '4' as priority, i.store_cd as price_group, 'RETAIL' as price_type, TO_CHAR(i.eff_dt,'DD-MON-YYYY') as eff_date, TO_CHAR(i.end_dt,'DD-MON-YYYY') as term_date, LTRIM(TO_CHAR(i.prc,'999,999.99')) as price "
            SQL = SQL & ", 'NONE' as prc_cd, 'NO DESC' as des, i.itm_cd "
            SQL = SQL & "FROM itm$ret_clndr i "

            'JAM v2.2: removed UPPER on itm_cd amd store_cd
            'SQL = SQL & "WHERE UPPER(i.itm_cd) = UPPER(:x_itemCode) "
            'SQL = SQL & "AND UPPER(i.store_cd) = UPPER(:x_storeCode) "
            SQL = SQL & "WHERE i.itm_cd = UPPER(:x_itemCode) "
            SQL = SQL & "AND i.store_cd = UPPER(:x_storeCode) "
            'JAM v2.2 (END)

            SQL = SQL & "AND :x_asOfDate BETWEEN i.eff_dt AND i.end_dt "
            SQL = SQL & "AND ROWNUM = 1 "

            SQL = SQL & "UNION "

            '----------------------------
            '--(query Retail-B)
            SQL = SQL & "SELECT '5' as priority, sgrc.store_grp_cd as price_group, 'RETAIL' as price_type, TO_CHAR(sgrc.eff_dt,'DD-MON-YYYY') as eff_date, TO_CHAR(sgrc.end_dt,'DD-MON-YYYY') as term_date, LTRIM(TO_CHAR(sgrc.prc,'999,999.99')) as price "
            SQL = SQL & ", 'NONE' as prc_cd, NVL(sg.des,'') as des, sgrc.itm_cd "
            SQL = SQL & "FROM store_grp$store sgs, store_grp sg, store_grp_ret_clndr sgrc "

            'JAM v2.2: removed UPPER on itm_cd amd store_cd
            'SQL = SQL & "WHERE UPPER(sgrc.itm_cd) = UPPER(:x_itemCode) "
            'SQL = SQL & "AND UPPER(sgs.store_cd) = UPPER(:x_storeCode) "
            SQL = SQL & "WHERE sgrc.itm_cd = UPPER(:x_itemCode) "
            SQL = SQL & "AND sgs.store_cd = UPPER(:x_storeCode) "
            'JAM v2.2 (END)

            SQL = SQL & "AND :x_asOfDate BETWEEN sgrc.eff_dt AND sgrc.end_dt "
            SQL = SQL & "AND sgrc.store_grp_cd = sg.store_grp_cd "
            SQL = SQL & "AND sg.store_grp_cd = sgs.store_grp_cd "
            SQL = SQL & "AND sg.pricing_store_grp = 'Y' "
            SQL = SQL & "AND ROWNUM = 1 "

            SQL = SQL & "UNION "

            '----------------------------
            '--(query Retail-C)
            SQL = SQL & "SELECT '6' as priority, NULL as price_group, 'RETAIL' as price_type, NULL as eff_date, NULL as term_date, LTRIM(TO_CHAR(i.ret_prc,'999,999.99')) as price "
            SQL = SQL & ", 'NONE' as prc_cd, 'NO DESC' as des, i.itm_cd "
            SQL = SQL & "FROM itm i "

            'JAM v2.2: removed UPPER on itm_cd amd store_cd
            'SQL = SQL & "WHERE UPPER(i.itm_cd) = UPPER(:x_itemCode) "
            SQL = SQL & "WHERE i.itm_cd = UPPER(:x_itemCode) "
            'JAM v2.2 (END)

            SQL = SQL & "ORDER BY 1 "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            ' bind variables
            objsql.Parameters.Add(":x_asOfDate", OracleType.DateTime)
            objsql.Parameters(":x_asOfDate").Value = p_asOfDate.ToUpper
            objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
            objsql.Parameters(":x_storeCode").Value = p_storeCode.ToUpper
            objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
            objsql.Parameters(":x_itemCode").Value = p_itemCode.ToUpper

            Dim ds_GridViewPrices As New DataSet
            Dim oAdp As OracleDataAdapter

            oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
            oAdp.Fill(ds_GridViewPrices)

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)
                'Store Values in String Variables 
                If MyDataReader.Read Then
                    Try
                        'txt_store_cd.Items.Insert(0, Request("store_cd"))
                        'txt_store_cd.SelectedIndex = 0
                    Catch ex As Exception
                        conn.Close()
                    End Try

                    MyDataReader.Close()
                End If

                'Close Connection 
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            GridViewPrices.DataSource = ds_GridViewPrices
            GridViewPrices.DataBind()

            'Determine Promo Code and Description from grid - possible nothing will populate if promo does not exist
            Dim dv As DataView = New DataView(ds_GridViewPrices.Tables(0))
            Dim s_promo_code As String = ""
            Dim s_promo_des As String = ""

            dv.RowFilter = "price_type = 'PROMO'"

            For Each drv As DataRowView In dv
                'MsgBox(" {0} " + drv("priority") + "; prc_cd: " + drv("prc_cd") + "; des: " + drv("des"))

                s_promo_code = drv("prc_cd").ToString
                s_promo_des = drv("des").ToString

                If (Not (s_promo_code Is Nothing) AndAlso s_promo_code.isNotEmpty) Then
                    Exit For
                End If
            Next

            txt_promo_cd.Text = s_promo_code
            txt_promo_des.Text = s_promo_des

        End If

    End Sub

    'Private Sub PrintRows(ByVal dataSet As DataSet)
    '    Dim table As DataTable
    '    Dim row As DataRow
    '    Dim column As DataColumn
    '    ' For each table in the DataSet, print the row values. 
    '    For Each table In dataSet.Tables
    '        For Each row In table.Rows
    '            For Each column In table.Columns
    '                Console.WriteLine(row(column))
    '            Next column
    '        Next row
    '    Next table
    'End Sub

    'JAM v2.2: **NEW**

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulatePackageComponentsGridview()
    '   Created by:     John Melenko
    '   Date:           Mar2015
    '   Description:    
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulatePackageComponentsGridview()

        lbl_pkg_split_method.Visible = False
        txt_pkg_split_method.Visible = False

        btn_pkg_components.Visible = False
        btn_pkg_pricing.Visible = False
        btn_pkg.Visible = False

        If (txt_itm_tp_cd.Text = "PKG") Then

            lbl_pkg_split_method.Visible = True
            txt_pkg_split_method.Visible = True

            btn_pkg_components.Visible = True

            Dim ds_packageComponentsPromoPrice As New DataSet
            Dim ds_packageComponentsRetailPrice As New DataSet

            Dim ds_packageComponentsPOSPrice As New DataSet
            Dim ds_packageComponentsRETPrice As New DataSet
            Dim ds_temp As New DataSet


            '(1) Confirm package is a valid package first - ie. it contains valid components
            If (isValidPackageItem(txt_sku.Text)) Then

                '(2) Get package component(s) data for RETAIL and PROMO prices from prices gridview
                'Dim ds_GridViewPrices = GridViewPrices.DataSource
                Dim ds_GridViewPrices = GridViewPrices.DataSource
                Dim dv_GridViewPrices As DataView = New DataView(ds_GridViewPrices.Tables(0))

                '(2a) RETAIL prices
                dv_GridViewPrices.RowFilter = "price_type = 'RETAIL'"

                Dim s_retail_price As String = ""
                Dim n_top_priority As Integer = -1

                For Each drv As DataRowView In dv_GridViewPrices
                    'MsgBox("priority: " + drv("priority") + "; prc_cd: " + drv("prc_cd") + "; des: " + drv("des") + "; price: " + drv("price"))

                    If (n_top_priority < 0) Then
                        n_top_priority = drv("priority")
                        s_retail_price = drv("price")
                        'MsgBox("(INIT)top priority: " + n_top_priority.ToString + "; price: " + s_retail_price)
                    End If

                    If (drv("priority") < n_top_priority) Then
                        n_top_priority = drv("priority")
                        s_retail_price = drv("price")
                        'MsgBox("top priority: " + n_top_priority.ToString + "; price: " + s_retail_price)
                    End If
                Next

                If Not (String.IsNullOrEmpty(Trim(s_retail_price.ToString()))) Then
                    ds_packageComponentsRetailPrice = GetPackageComponentsInfo(txt_sku.Text, txt_pkg_split_method.Text, s_retail_price)
                End If
                'GridViewPackageComponentsRETPrice.DataSource = ds_packageComponentsRetailPrice
                'GridViewPackageComponentsRETPrice.DataBind()

                'Dim dv As DataView = New DataView(ds_packageComponentsRETPrice.Tables(0))
                'For Each drv As DataRowView In dv
                'MsgBox(" {0} " + drv("itm_cd") + "; price: " + drv("curr_prc").ToString + ";")
                'Next

                '(2b) PROMO prices
                dv_GridViewPrices.RowFilter = "price_type = 'PROMO'"

                Dim s_promo_price As String = ""
                n_top_priority = -1

                For Each drv As DataRowView In dv_GridViewPrices
                    'MsgBox("priority: " + drv("priority") + "; prc_cd: " + drv("prc_cd") + "; des: " + drv("des") + "; price: " + drv("price"))

                    If (n_top_priority < 0) Then
                        n_top_priority = drv("priority")
                        s_promo_price = drv("price")
                        'MsgBox("(INIT)top priority: " + n_top_priority.ToString + "; price: " + s_retail_price)
                    End If

                    If (drv("priority") < n_top_priority) Then
                        n_top_priority = drv("priority")
                        s_promo_price = drv("price")
                        'MsgBox("top priority: " + n_top_priority.ToString + "; price: " + s_retail_price)
                    End If
                Next

                If Not (String.IsNullOrEmpty(Trim(s_promo_price.ToString()))) Then
                    ds_packageComponentsPromoPrice = GetPackageComponentsInfo(txt_sku.Text, txt_pkg_split_method.Text, s_promo_price)
                Else
                    ds_packageComponentsPromoPrice = New DataSet
                    Dim dt_temp As DataTable = ds_packageComponentsPromoPrice.Tables.Add("TEMP")

                End If
                'GridViewPackageComponentsPromoPrice.DataSource = ds_packageComponentsPromoPrice
                'GridViewPackageComponentsPromoPrice.DataBind()

                'If Not (String.IsNullOrEmpty(Trim(txt_pos_price.Text.ToString()))) Then
                'ds_packageComponentsPOSPrice = GetPackageComponentsInfo(txt_sku.Text, txt_pkg_split_method.Text, txt_pos_price.Text)
                'End If
                'GridViewPackageComponentsPOSPrice.DataSource = ds_packageComponentsPOSPrice
                'GridViewPackageComponentsPOSPrice.DataBind()

                '(3) Now build a new data table based on the above data
                Dim dt_packageComponents As DataTable = New DataTable

                dt_packageComponents.Columns.Add("ITM_CD", Type.GetType("System.String"))
                dt_packageComponents.Columns.Add("QTY", Type.GetType("System.String"))
                dt_packageComponents.Columns.Add("VSN", Type.GetType("System.String"))
                dt_packageComponents.Columns.Add("DES", Type.GetType("System.String"))
                dt_packageComponents.Columns.Add("POS_PRICE", Type.GetType("System.String"))
                dt_packageComponents.Columns.Add("RET_PRICE", Type.GetType("System.String"))
                dt_packageComponents.Columns.Add("PKG_PRC_PROMO", Type.GetType("System.String"))
                dt_packageComponents.Columns.Add("PKG_PRC_RETAIL", Type.GetType("System.String"))

                Dim dv_packageComponentsRetailPrice As DataView = New DataView(ds_packageComponentsRetailPrice.Tables(0))
                Dim dv_packageComponentsPromoPrice As DataView = New DataView(ds_packageComponentsPromoPrice.Tables(0))

                '(3a) use the RETAIL components dataview to populate main data; use the PROMO components dataview to grab the Promo price for each item
                For Each drv_packageComponents As DataRowView In dv_packageComponentsRetailPrice

                    Dim dr_newDataRow As DataRow = dt_packageComponents.NewRow()
                    dr_newDataRow("ITM_CD") = drv_packageComponents("ITM_CD")
                    dr_newDataRow("QTY") = drv_packageComponents("ALT_ID_QTY")
                    dr_newDataRow("VSN") = drv_packageComponents("VSN")
                    dr_newDataRow("DES") = drv_packageComponents(Resources.CustomMessages.MSG0080)

                    Dim n_itemPrice As Double
                    n_itemPrice = Get_Curr_Pricing(drv_packageComponents("ITM_CD"), txt_store_cd.Text, dateEdit.Text, 0, "")

                    'dr_newDataRow("POS_PRICE") = "---"
                    'dr_newDataRow("POS_PRICE") = FormatCurrency(n_itemPrice, 2)
                    'dr_newDataRow("POS_PRICE") = n_itemPrice.ToString
                    dr_newDataRow("POS_PRICE") = FormatNumber(n_itemPrice, 2)

                    'dr_newDataRow("RET_PRICE") = FormatNumber(drv_packageComponents("RET_PRC"), 2)
                    'dr_newDataRow("RET_PRICE") = FormatNumber(drv_packageComponents("RET_PRC"), 2)

                    Dim n_component_retail_price As Double = 0
                    Dim s_component_retail_price As String = "N/A"

                    n_component_retail_price = GetRetailPriceForPackageComponents(drv_packageComponents("ITM_CD"), txt_store_cd.Text, dateEdit.Text)
                    If (n_component_retail_price > 0) Then
                        s_component_retail_price = FormatNumber(n_component_retail_price, 2)
                    End If
                    dr_newDataRow("RET_PRICE") = s_component_retail_price

                    Dim s_pkg_price_promo As String = "N/A"
                    If (dv_packageComponentsPromoPrice.Count > 0) Then
                        dv_packageComponentsPromoPrice.RowFilter = "itm_cd = '" & drv_packageComponents("ITM_CD") & "'"
                        For Each drv_item As DataRowView In dv_packageComponentsPromoPrice
                            s_pkg_price_promo = FormatNumber(drv_item("CURR_PRC"), 2)
                        Next
                    End If
                    dr_newDataRow("PKG_PRC_PROMO") = s_pkg_price_promo

                    dr_newDataRow("PKG_PRC_RETAIL") = FormatNumber(drv_packageComponents("CURR_PRC"), 2)
                    dt_packageComponents.Rows.Add(dr_newDataRow)

                Next

                GridViewPackageComponents.DataSource = dt_packageComponents
                GridViewPackageComponents.DataBind()

                GridViewPackageComponents.Width = 1100

            Else 'package is an invalid package due to invalid components
                btn_pkg_components.Visible = False
            End If
        End If
    End Sub

    'Alice added on Aug 21, 2018 populate the component packages where item is contained, item type is PKG, queried component package item type is INV
    Private Sub PopulatePackageGridview()

        lbl_pkg_split_method.Visible = False
        txt_pkg_split_method.Visible = False

        btn_pkg_components.Visible = False
        btn_pkg_pricing.Visible = False
        btn_pkg.Visible = False

        Try


            '(1) Confirm it is a valid component package first 
            If (isValidComponentPackageItem(txt_sku.Text)) Then

                btn_pkg.Visible = True



                Dim dt_packages As New DataTable
                dt_packages = GetComponentPackages(txt_sku.Text).Tables(0)

                For i As Integer = 0 To dt_packages.Rows.Count - 1


                    Dim packageItemCode As String = dt_packages.Rows(i)("PKG_ITM_CD")
                    Dim n_current_price As Double

                    If String.IsNullOrEmpty(dt_packages.Rows(i)("RET_PRICE")) Then
                        n_current_price = 0
                    Else
                        n_current_price = Double.Parse(dt_packages.Rows(i)("RET_PRICE"))
                    End If

                    Dim currentPrice As Double
                    currentPrice = Get_Curr_Pricing(packageItemCode, txt_store_cd.Text, dateEdit.Text, n_current_price, "")

                    dt_packages.Rows(i)("POS_PRICE") = currentPrice.ToString(".00", CultureInfo.InvariantCulture)

                    Dim ret_price As String = GetRetailPriceforPackage(txt_store_cd.Text, packageItemCode, dateEdit.Text)


                    If ret_price = "" Then
                        dt_packages.Rows(i)("RET_PRICE") = "N/A"
                    Else
                        dt_packages.Rows(i)("RET_PRICE") = Double.Parse(ret_price).ToString(".00", CultureInfo.InvariantCulture)
                    End If
                Next


                GridViewPackages.DataSource = dt_packages
                GridViewPackages.DataBind()

            Else 'package is an invalid package due to invalid components
                btn_pkg.Visible = False
            End If

        Catch ex As Exception

            Throw
        End Try



    End Sub

    Private Function GetComponentPackages(ByVal p_componentPackageItemCode As String) As DataSet
        Dim componentPackageDataSet As New DataSet

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        '  sql = "SELECT admin_page FROM USER_ACCESS group by admin_page"
        sql = "select * from "
        sql = sql & "(SELECT p.cmpnt_itm_cd, p.pkg_itm_cd, p.pkg_itm_cd||decode(i.drop_cd,null,null,' ('||i.drop_cd||')') pkg_itm_cd_new, i.vsn, i."
        sql = sql & Resources.CustomMessages.MSG0080
        sql = sql & " as des, '0' as pos_price, i.ret_prc as ret_price, "
        sql = sql & "(select count(*) from package k where k.pkg_itm_cd=p.pkg_itm_cd) as count "
        sql = sql & "FROM package p, itm i "
        sql = sql & "WHERE p.cmpnt_itm_cd = UPPER(:x_componentPackageItemCode) "
        sql = sql & "AND p.pkg_itm_cd = i.itm_cd And i.itm_tp_cd = 'PKG') a "
        sql = sql & "where a.count>1"



        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.Parameters.Add(":x_componentPackageItemCode", OracleType.VarChar)
        objSql.Parameters(":x_componentPackageItemCode").Value = p_componentPackageItemCode.ToUpper
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(componentPackageDataSet)
        conn.Close()

        Return componentPackageDataSet

    End Function

    Protected Function GetRetailPriceforPackage(p_storeCode As String, p_itemCode As String, p_asOfDate As String) As String

        '	First check if retail price setup for just the selected store

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim SQL As String
        Dim objsql As OracleCommand
        Dim MyDataReader As OracleDataReader

        Dim ret_price As String = String.Empty

        If (Not (p_storeCode Is Nothing OrElse p_itemCode Is Nothing OrElse p_asOfDate Is Nothing)) AndAlso p_storeCode.isNotEmpty AndAlso p_itemCode.isNotEmpty AndAlso p_asOfDate.isNotEmpty AndAlso IsDate(p_asOfDate) Then

            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                conn.Open()

            SQL = "select PRC from itm$ret_clndr i "
            SQL = SQL & "Where i.eff_dt = (Select max(i2.eff_dt) From itm$ret_clndr i2 Where i2.eff_dt <=:x_asOfDate And i2.end_dt >=:x_asOfDate And i2.itm_cd = i.itm_cd And i2.store_cd = i.store_cd) "
            SQL = SQL & "And i.itm_cd = :x_itemCode And i.store_cd  =:x_storeCode"

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            ' bind variables
            objsql.Parameters.Add(":x_asOfDate", OracleType.DateTime)
            objsql.Parameters(":x_asOfDate").Value = p_asOfDate.ToUpper
            objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
            objsql.Parameters(":x_storeCode").Value = p_storeCode.ToUpper
            objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
            objsql.Parameters(":x_itemCode").Value = p_itemCode.ToUpper

            Try

                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)
                'Store Values in String Variables 
                If MyDataReader.Read Then
                    Try
                        ret_price = MyDataReader.Item("PRC").ToString
                    Catch ex As Exception
                        conn.Close()
                    End Try

                    MyDataReader.Close()
                End If

                'Close Connection 
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            If ret_price = String.Empty Then

                ret_price = GetRetailPricefromStoreGroupforPackage(p_storeCode, p_itemCode, p_asOfDate)

            End If

        End If
        Return ret_price

    End Function

    Protected Function GetRetailPricefromStoreGroupforPackage(p_storeCode As String, p_itemCode As String, p_asOfDate As String) As String

        'If no retail price setup for individual store then check if setup for a stroe group

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim SQL As String
        Dim objsql As OracleCommand
        Dim MyDataReader As OracleDataReader

        Dim ret_price As String = String.Empty

        If (Not (p_storeCode Is Nothing OrElse p_itemCode Is Nothing OrElse p_asOfDate Is Nothing)) AndAlso p_storeCode.isNotEmpty AndAlso p_itemCode.isNotEmpty AndAlso p_asOfDate.isNotEmpty AndAlso IsDate(p_asOfDate) Then

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If
            SQL = "select PRC from store_grp_ret_clndr i "
            SQL = SQL & "Where i.eff_dt = (Select max(i2.eff_dt) From store_grp_ret_clndr i2 Where i2.eff_dt <=:x_asOfDate And i2.end_dt >=:x_asOfDate And i2.itm_cd = i.itm_cd And i2.store_grp_cd  = i.store_grp_cd ) "
            SQL = SQL & "And i.itm_cd = :x_itemCode And  i.store_grp_cd  in ("
            SQL = SQL & "Select sg.store_grp_cd  From store_grp sg, store_grp$store sgs where sg.pricing_store_grp = 'Y' and sgs.store_grp_cd = sg.store_grp_cd and  sgs.store_cd =:x_storeCode)"


            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            ' bind variables
            objsql.Parameters.Add(":x_asOfDate", OracleType.DateTime)
            objsql.Parameters(":x_asOfDate").Value = p_asOfDate.ToUpper
            objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
            objsql.Parameters(":x_storeCode").Value = p_storeCode.ToUpper
            objsql.Parameters.Add(":x_itemCode", OracleType.VarChar)
            objsql.Parameters(":x_itemCode").Value = p_itemCode.ToUpper

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)
                'Store Values in String Variables 
                If MyDataReader.Read Then
                    Try
                        ret_price = MyDataReader.Item("PRC").ToString
                    Catch ex As Exception
                        conn.Close()
                    End Try

                    MyDataReader.Close()
                End If

                'Close Connection 
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If
        Return ret_price

    End Function



    'JAM v2.2 **NEW** (END)

    'JAM v2.2: **NEW**

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           GetPackageComponentsInfo()
    '   Created by:     John Melenko
    '   Date:           Mar2015
    '   Description:    TODO
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function GetPackageComponentsInfo(ByVal p_packageItemCode As String, ByVal p_packageSplitMethod As String, ByVal p_packagePrice As String) As DataSet


        Dim s_packageItemCode As String = p_packageItemCode.ToUpper
        Dim s_packageSplitMethod As String = p_packageSplitMethod.ToUpper
        Dim componentPricesDataSet As New DataSet

        If (String.IsNullOrEmpty(Trim(s_packageItemCode.ToString())) OrElse String.IsNullOrEmpty(Trim(p_packagePrice.ToString()))) Then

            Dim s_errorMessage As String = ""

            's_errorMessage = "ERROR: Package item code and/or price are required."
            s_errorMessage = Resources.CustomMessages.MSG0010
            'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
            'lbl_resultsInfo.Text = s_errorMessage
            'lbl_resultsInfo.ForeColor = Color.Red
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

        Else

            Dim dbConn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand(dbConn)

            Dim SalesDacLibraryClass As New SalesDac

            'MsgBox("Calling (SalesDacLibraryClass.GetPkgCmpntsRetPrc) - Item: " + s_packageItemCode + "; Method: " + s_packageSplitMethod + "; Price: " + p_packagePrice)
            'Daniela Dec 29, 2016 - prevent system error
            Try
                ' SalesDacLibraryClass.GetPkgComponentData(s_packageItemCode, dbConn, txt_store_cd.Text, "Error")

                componentPricesDataSet = SalesDacLibraryClass.GetPkgCmpntsRetPrc(s_packageItemCode, s_packageSplitMethod, Convert.ToDouble(p_packagePrice, CultureInfo.InvariantCulture), dbCmd)

                Dim aa As Integer = componentPricesDataSet.Tables(0).Rows.Count

            Catch
                Dim s_errorMessage As String = ""
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0003, s_packageItemCode)
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            End Try



        End If

        Return componentPricesDataSet

    End Function

    Public Function GetRetailPriceForPackageComponents(ByVal p_itemCode As String, ByVal p_storeCode As String, ByVal p_effectiveDate As String) As String

        Dim dbConn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand(dbConn)
        Dim dbAdapter As OracleDataAdapter
        Dim sql As StringBuilder = New StringBuilder()

        Dim n_result As Double = 0

        dbAdapter = DisposablesManager.BuildOracleDataAdapter(dbCmd)

        Try
            'Open Connection 
            dbConn.Open()

            'Calling API here
            dbCmd.Parameters.Clear()
            dbCmd.CommandText = "bt_get_price.retail_clndr_price"
            dbCmd.CommandType = CommandType.StoredProcedure

            dbCmd.Parameters.Add("itm_cd_i", OracleType.NVarChar).Direction = ParameterDirection.Input
            dbCmd.Parameters.Add("store_cd_i", OracleType.NVarChar).Direction = ParameterDirection.Input
            dbCmd.Parameters.Add("eff_date_i", OracleType.DateTime).Direction = ParameterDirection.Input
            dbCmd.Parameters.Add("err_msg_io", OracleType.VarChar, 250).Direction = ParameterDirection.InputOutput
            dbCmd.Parameters.Add("return_type_i", OracleType.VarChar).Direction = ParameterDirection.Input
            dbCmd.Parameters.Add("v_price", OracleType.Number).Direction = ParameterDirection.ReturnValue

            dbCmd.Parameters("itm_cd_i").Value = p_itemCode.ToUpper
            dbCmd.Parameters("store_cd_i").Value = p_storeCode
            dbCmd.Parameters("eff_date_i").Value = p_effectiveDate
            dbCmd.Parameters("err_msg_io").Value = DBNull.Value
            dbCmd.Parameters("return_type_i").Value = DBNull.Value

            dbCmd.ExecuteNonQuery()

            'JAM_GetPkgCmpntsRetPrc = dbCmd.Parameters("v_price").Value
            n_result = dbCmd.Parameters("v_price").Value

            'Close Connection 
            dbConn.Close()
        Catch
            Throw

            'Close Connection 
            dbConn.Close()
        Finally
            dbAdapter.Dispose()
        End Try

        Return n_result

    End Function


    'JAM v2.2 **NEW** (END)

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PrintDataView()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Debugging procedure for dataviews
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    'Private Shared Sub PrintDataView(dv As DataView)
    '    ' Printing first DataRowView to demo that the row in the first index of the DataView changes depending on sort and filters
    '    'Console.WriteLine("First DataRowView value is '{0}'", dv(0)("priority"))
    '    MsgBox("First DataRowView value is '{0}' " + dv(0)("priority") + "; prc_cd: " + dv(0)("prc_cd") + "; des: " + dv(0)("des"))

    '    ' Printing all DataRowViews 
    '    For Each drv As DataRowView In dv

    '        'Console.WriteLine(vbTab & " {0}", drv("priority"))
    '        'MsgBox(" {0}" + drv("priority"))
    '        MsgBox(" {0} " + drv("priority") + "; prc_cd: " + drv("prc_cd") + "; des: " + drv("des"))

    '    Next
    'End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AlertMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display alert messages
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub AlertMessage(p_messageText)
        MsgBox(p_messageText, MsgBoxStyle.Exclamation, Title:="Error")
        'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           DisplayMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display messages - can toggle between a label or message box
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub DisplayMessage(p_messageText, p_messageType, p_msgBoxStyle)

        Dim color_fontColor As Color
        Dim msgBoxStyle As MsgBoxStyle

        If (p_msgBoxStyle = "ERROR") Then
            color_fontColor = Color.Red
            msgBoxStyle = msgBoxStyle.Critical
        ElseIf (p_msgBoxStyle = "WARNING") Then
            color_fontColor = Color.Orange
            msgBoxStyle = msgBoxStyle.Exclamation
        ElseIf (p_msgBoxStyle = "INFO") Then
            color_fontColor = Color.Green
            msgBoxStyle = msgBoxStyle.Information
        End If

        If (p_messageType = "LABEL") Then
            lbl_resultsInfo.Text = p_messageText
            lbl_resultsInfo.ForeColor = color_fontColor
            lbl_resultsInfo.Visible = True
        ElseIf (p_messageType = "MSGBOX") Then
            MsgBox(p_messageText, msgBoxStyle)
            'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID, "javascript:alert('(2) date changed');", True)
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_Load()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form - minor changes
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_resultsInfo.Visible = False

        If Not IsPostBack Then
            '    If Find_Security("ISTM", Session("emp_cd")) = "N" Then
            'btn_Lookup.Enabled = False
            'btn_Clear.Enabled = False
            'lbl_Cust_Info.Text = "You don't have the appropriate securities to view Franchise-Store percentages."
            'End If
            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then

                btn_Lookup.Enabled = False

            Else

                'JAM: populate the store codes drop-down list
                PopulateStoreCodes()

                dateEdit.Date = Today.Date


                'JAM v2.1: changed calendar to allow selection of a date up to 7 years in the past and 30 days in the future
                Dim d_mindate As Date = Date.Today
                Dim d_maxdate As Date = Date.Today

                'd_mindate = d_mindate.AddDays(-90)
                d_mindate = d_mindate.AddDays(-gn_minimumCalendarDaysLimit)
                d_maxdate = d_maxdate.AddDays(+gn_maximumCalendarDaysLimit)
                dateEdit.MinDate = d_mindate
                dateEdit.MaxDate = d_maxdate
                'JAM v2.1: changed calendar to allow selection of a date up to 7 years in the past and 30 days in the future

                dateEdit.TimeSectionProperties.Visible = False
                'chkShowTimeSection.Checked()
                dateEdit.UseMaskBehavior = True
                'chkMaskBehavior.Checked()
                dateEdit.EditFormatString = "dd MMM yyyy"
                'GetFormatString(cbDateEditFormatString.Value)
                dateEdit.DisplayFormatString = "dd MMM yyyy"
                'GetFormatString(cbDateDisplayFormatString.Value)

                dateEdit.ClientEnabled = True
                'dateEdit.AllowUserInput = True
            End If
        End If

        'lbl_Cust_Info.Visible = True

        'txt_store_cd.SelectedValue = ConfigurationManager.AppSettings("default_store_code").ToString

        If (Not GridViewPrices.DataSource Is Nothing) Then
            GridViewPrices.DataBind()
        End If

        'JAM v2.2: added new GridViewPackageComponents and changed name of GridView2 to GridViewPrices
        GridViewPrices.Visible = True
        GridViewPackageComponents.Visible = False
        'GridViewPackageComponentsPOSPrice.Visible = False
        'GridViewPackageComponentsRETPrice.Visible = False
        'GridViewPackageComponentsPromoPrice.Visible = False
        'JAM v2.2 (END)

        lbl_versionInfo.Text = gs_version

        'JAM added 20Oct2014
        'txt_des.Enabled = False
        'txt_ve_name.Enabled = False
        'txt_vsn.Enabled = False
        'txt_major_des.Enabled = False
        'txt_minor_des.Enabled = False
        'txt_itm_tp_cd.Enabled = False
        'txt_drop_cd.Enabled = False
        'txt_cat_cd.Enabled = False
        'txt_cat_des.Enabled = False
        'txt_style_cd.Enabled = False
        'txt_style_des.Enabled = False
        'txt_comm_cd.Enabled = False
        'txt_related_itm_cd.Enabled = False
        'txt_warrantable.Enabled = False
        'txt_treatable.Enabled = False
        'txt_pos_price.Enabled = False
        'txt_promo_cd.Enabled = False
        'txt_promo_des.Enabled = False

        txt_des.ReadOnly = True
        txt_ve_name.ReadOnly = True
        txt_vsn.ReadOnly = True
        txt_major_des.ReadOnly = True
        txt_minor_des.ReadOnly = True
        txt_itm_tp_cd.ReadOnly = True
        txt_drop_cd.ReadOnly = True
        txt_cat_cd.ReadOnly = True
        txt_cat_des.ReadOnly = True
        txt_style_cd.ReadOnly = True
        txt_style_des.ReadOnly = True
        txt_comm_cd.ReadOnly = True
        txt_related_itm_cd.ReadOnly = True
        txt_warrantable.ReadOnly = True
        txt_treatable.ReadOnly = True
        txt_pos_price.ReadOnly = True
        txt_promo_cd.ReadOnly = True
        txt_promo_des.ReadOnly = True
        'JAM v2.1: added dimensions
        txt_width.ReadOnly = True
        txt_depth.ReadOnly = True
        txt_height.ReadOnly = True
        txt_weight.ReadOnly = True
        'JAM v2.1: added dimensions

        'JAM v2.1: added SPIFF data
        txt_spiff_cd.ReadOnly = True
        txt_spiff_des.ReadOnly = True
        txt_spiff_eff_date.ReadOnly = True
        txt_spiff_end_date.ReadOnly = True
        txt_spiff_amount.ReadOnly = True
        txt_spiff_min_retail.ReadOnly = True
        'JAM v2.1: added SPIFF data

        'JAM v2.2: added new package split method and buttons - default to NOT visible on page load
        'lbl_pkg_split_method.Visible = False
        txt_pkg_split_method.ReadOnly = True
        'txt_pkg_split_method.Visible = False

        btn_pkg_components.Visible = False
        btn_pkg_pricing.Visible = False
        btn_pkg.Visible = False
        'JAM v2.2 (END)

        ''JAM v2.2: added new feature to toggle package components gridview
        'txt_pos_price.ClientIDMode = UI.ClientIDMode.Static

        ''"javascript:var n_price = document.getElementById('ctl00_arpMain_ContentPlaceHolder1_txt_pos_price').value; " & _
        'txt_pos_price.Attributes.Add("ondblclick", _
        '"javascript:var n_price = document.getElementById('txt_pos_price').value; alert('price: '+ n_price ); " & _
        '";")

        ''"alert('price2: ' + n_price2 );")
        ''JAM v2.2 (END)

        'JAM v2.3: added landed cost and staff price
        txt_landed_cost.ReadOnly = True
        txt_fran_cost.ReadOnly = True

        lbl_staff_price.Visible = False
        txt_staff_price.Visible = False
        txt_staff_price.ReadOnly = True
        'JAM v2.3 (END)

    End Sub

    'JAM added 20Oct2014
    'Protected Function GetFormatString(ByVal value As Object) As String
    '    If value Is Nothing Then
    '        Return String.Empty
    '    Else
    '        Return value.ToString()
    '    End If
    'End Function
    'JAM added 20Oct2014

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Clear_Click()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_store_cd.Enabled = True

        'lbl_Cust_Info.Visible = True
        'lbl_Cust_Info.Text = "Clear"
        Response.Redirect("ItemPriceQuery.aspx")

    End Sub


    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           
    '   Created by:     
    '   Date:           Mar2015
    '   Description:    
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_pkg_components_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_pkg_components.Click

        GridViewPrices.Visible = False
        GridViewPackageComponents.Visible = True
        GridViewPackages.Visible = False
        '        GridViewPackageComponentsPOSPrice.Visible = False
        '       GridViewPackageComponentsRETPrice.Visible = False
        '      GridViewPackageComponentsPromoPrice.Visible = False

        btn_pkg_components.Visible = False
        btn_pkg_pricing.Visible = True
        btn_pkg.Visible = False


    End Sub

    Protected Sub btn_pkg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_pkg.Click

        GridViewPrices.Visible = False
        GridViewPackageComponents.Visible = False
        GridViewPackages.Visible = True

        btn_pkg_components.Visible = False
        btn_pkg_pricing.Visible = True
        btn_pkg.Visible = False


    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           
    '   Created by:     
    '   Date:           Mar2015
    '   Description:    
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_pkg_pricing_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_pkg_pricing.Click

        GridViewPrices.Visible = True
        GridViewPackageComponents.Visible = False
        GridViewPackages.Visible = False

        If txt_itm_tp_cd.Text = "PKG" Then
            btn_pkg_components.Visible = True
            btn_pkg.Visible = False
        End If

        If txt_itm_tp_cd.Text = "INV" Then
            btn_pkg.Visible = True
            btn_pkg_components.Visible = False
        End If

        btn_pkg_pricing.Visible = False

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Lookup_Click()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click


        GridViewPackageComponents.Visible = False
        GridViewPackages.Visible = False

        ResetAllFields()
        Session("IPQ_SEARCH") = "ON"

        If (isValidItem(txt_sku.Text)) Then

            Dim strGroup As String = Session("str_co_grp_cd")
            Dim supeUser As String = Session("str_sup_user_flag")

            Dim s_isValidItemSecurityCheck As String = "N"

            If strGroup = "LFS" Or supeUser = "Y" Then
                s_isValidItemSecurityCheck = "Y"
            Else
                s_isValidItemSecurityCheck = IsValidItemSecurityCheck(txt_sku.Text)
            End If

            If (s_isValidItemSecurityCheck = "Y") Then

                PopulateItem(txt_sku.Text)

                RetrieveIdentificationInfo(txt_sku.Text, txt_store_cd.Text)

            ElseIf (s_isValidItemSecurityCheck = "N") Then

                Dim s_errorMessage As String = ""
                Dim s_employeeCode = Session("EMP_CD")

                'MsgBox("Security Violation for item [" + txt_sku.Text + "]", MsgBoxStyle.Exclamation, Title:="Error")
                's_errorMessage = "Security Violation for item '" + txt_sku.Text.ToUpper + "' and employee '" + s_employeeCode + "'"
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0011, txt_sku.Text.ToUpper, s_employeeCode)
                'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Else

                Dim s_errorMessage As String = ""
                Dim s_employeeCode = Session("EMP_CD")

                'MsgBox("Security Violation for item [" + txt_sku.Text + "]", MsgBoxStyle.Exclamation, Title:="Error")
                's_errorMessage = "Cannot determine security information for item '" + txt_sku.Text.ToUpper + "' and employee '" + s_employeeCode + "'"
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0012, txt_sku.Text.ToUpper, s_employeeCode)
                'MsgBox(s_errorMessage, MsgBoxStyle.Critical)
                'lbl_resultsInfo.Text = s_errorMessage
                'lbl_resultsInfo.ForeColor = Color.Red
                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            End If

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_PreInit()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AddError()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub AddError(ByVal errors As Dictionary(Of GridViewColumn, String), ByVal column As GridViewColumn, ByVal errorText As String)
        If errors.ContainsKey(column) Then
            Return
        End If
        errors(column) = errorText
    End Sub

End Class
