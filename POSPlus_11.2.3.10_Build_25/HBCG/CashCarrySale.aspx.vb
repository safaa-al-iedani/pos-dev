Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class CashCarrySale
    Inherits POSBasePage

    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private theCustBiz As CustomerBiz = New CustomerBiz()


    Private Function ValStoreAndDt() As String
        ' validate SHU has not been run for the date (today) and also the store (emp home store) is not closed for the date (today)
        Dim ShuDt As String = SessVar.shuDt
        Dim todayDate As String = FormatDateTime(Today.Date, DateFormat.ShortDate)

        If IsDate(ShuDt) AndAlso DateDiff("d", todayDate, ShuDt) >= 0 Then
            ValStoreAndDt = "Sales History (SHU) has already been run for today.  You cannot submit orders or payments at this time."
        Else
            ValStoreAndDt = theSalesBiz.ValidateStoreClosedDate(Session("HOME_STORE_CD"), todayDate)
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not IsPostBack) Then
            Label1.Text = String.Empty
            Dim lockTerminal = ("Y" = ConfigurationManager.AppSettings("term_lock_down").ToString)
            Dim lockTerminalVal = Terminal_Locked(Request.ServerVariables("REMOTE_ADDR"))

            If lockTerminal AndAlso "C" = lockTerminalVal Then
                Label1.Text = "This terminal has been closed.  You cannot submit orders or payments at this time."
            Else
                Label1.Text = ValStoreAndDt()
            End If
            If Label1.Text.isNotEmpty Then Exit Sub

            Dim empCd As String = Session("EMP_CD")
            Dim fname As String = Session("EMP_FNAME")
            Dim lname As String = Session("EMP_LNAME")
            Dim storeCd As String = Session("HOME_STORE_CD")
            Dim companyCd As String = Session("CO_CD")
            Dim IPAD As String = Session("IPAD")

            ' Daniela save clientip
            Dim client_IP As String = Session("clientip")
            Dim empInit As String = Session("EMP_INIT")
            Dim supUser As String = Session("str_sup_user_flag")
            Dim coGrpCd As String = Session("str_co_grp_cd")
            Dim userType As String = Session("USER_TYPE")
            Session.Clear()
            Session("clientip") = client_IP
            Session("str_sup_user_flag") = supUser ' Daniela
            Session("str_co_grp_cd") = coGrpCd ' Daniela
            Session("EMP_INIT") = empInit
            Session("USER_TYPE") = userType ' Daniela
            Session("EMP_CD") = empCd
            Session("EMP_FNAME") = fname
            Session("EMP_LNAME") = lname
            Session("HOME_STORE_CD") = storeCd
            Session("CO_CD") = companyCd
            Session("IPAD") = IPAD
            Session("cash_carry") = "TRUE"
            Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL
            Session("PD") = "P"
            Session("DEL_DT") = FormatDateTime(Today.Date, 2)
            Session("TRAN_DT") = FormatDateTime(Today.Date, 2)
            Session("STORE_CD") = Session("HOME_STORE_CD")
            Session("SLSP1") = UCase(Session("EMP_CD"))
            Session("PCT_1") = "100"
            Session("COMMENTS") = "Y"

            Session("lucy_been_to_rf_scan") = "N" 'lucy





            ActivateCashier(empCd)

            If ConfigurationManager.AppSettings("default_sort_code").ToString & "" <> "" Then
                Session("ORD_SRT") = ConfigurationManager.AppSettings("default_sort_code").ToString
            End If

            If Request("SWATCH") = "TRUE" Then
                Session("ORD_SRT") = "FAB"
            End If

            Dim strActivatedStoreCd As String = If(IsNothing(Session("STORE_CD")), storeCd, Session("STORE_CD"))

            ' --------- retrieves CASH DRAWER details 
            Dim dbSql As String
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmd As OracleCommand
            Dim rdr As OracleDataReader

            Try
                conn.Open()
                If lockTerminal AndAlso "O" = lockTerminalVal AndAlso Request("LEAD") <> "TRUE" Then
                    dbSql = UCase("SELECT STORE_CD, CASH_DWR_CD FROM TERMINAL_SETUP WHERE TERMINAL_IP=:REMOTE_ADDR")
                    cmd = DisposablesManager.BuildOracleCommand(dbSql, conn)
                    cmd.Parameters.Add(":REMOTE_ADDR", OracleType.VarChar)
                    cmd.Parameters(":REMOTE_ADDR").Value = Request.ServerVariables("REMOTE_ADDR")

                    rdr = DisposablesManager.BuildOracleDataReader(cmd)

                    If rdr.Read Then
                        Session("STORE_CD") = rdr.Item("STORE_CD").ToString
                        Session("CSH_DWR_CD") = rdr.Item("CASH_DWR_CD").ToString
                    End If
                    rdr.Close()
                    cmd.Dispose()
                Else
                    'nothing to be done here, the Session("CSH_DWR_CD"), gets set above
                    'by calling the method: ActivateCashier(empCd)
                End If

            Catch ex As Exception
                Throw ex
            Finally
                conn.Close()
            End Try

            ' --------- retrieves STORE data 
            Dim storeInfo As StoreData = theSalesBiz.GetStoreInfo(UCase(strActivatedStoreCd))
            Session("PD_STORE_CD") = storeInfo.puStoreCd
            Session("CO_CD") = storeInfo.coCd

            ' --------- Sets CUSTOMER data 
            Session("CUST_CD") = ConfigurationManager.AppSettings("cash_carry").ToString
            GetCustomerInfo(Session("CUST_CD"))

            ' --------- Sets TAX info, if NO prompting for tax is required then the Swatch SKU has
            ' to be inserted at this point, otherwise, the Order_Detail page takes care of it.
            Dim swatchSKU As String = ConfigurationManager.AppSettings("SWATCH_SKU").ToString
            Dim taxHasBeenSet As Boolean = DetermineTax(storeInfo.addr.postalCd)

            If (taxHasBeenSet) Then
                If Request("swatch") = "TRUE" Then
                    InsertRecords(swatchSKU)
                    'the code expects a value in the itemId session variable in order to show the shopping bag icon
                    'as "checked", since a SKU was added above, this is done to mark the shopping bag as complete
                    Session("itemid") = "NONEENTERED"
                    Session("swatch") = "TRUE"
                    Response.Redirect("Comments.aspx?swatch=TRUE")
                Else
                    Response.Redirect("Order_Detail.aspx?referrer=CC")
                End If
            Else
                ' if here means that it needs to prompt, after which it goes to the order_detail page
                ' SKU gets inserted there
                If Request("swatch") = "TRUE" Then
                    Session("itemid") = swatchSKU
                End If
            End If
        End If

    End Sub

    ''' <summary>
    ''' Retrieves the CUSTOMER details for the Cash and Carry customer and creates a record in
    ''' the CRM.CUST_INFO table for it.
    ''' </summary>
    ''' <param name="custCd">the CC customer that should be used</param>
    Private Sub GetCustomerInfo(ByVal custCd As String)

        Dim custInfo As CustomerDtc = theCustBiz.GetCustomer(custCd)
        If (custInfo.custCd.isNotEmpty()) Then

            theCustBiz.InsertCustomer(Session.SessionID.ToString.Trim, custInfo)
            lbl_Zip.Text = custInfo.zip
        End If
    End Sub

    ''' <summary>
    ''' Determines the tax details that will be used for this transaction
    ''' </summary>
    ''' <returns>TRUE if only one tax-code was found; FALSE if user has to be prompted to select a tax-code</returns>
    Private Function DetermineTax(ByVal storeZip As String) As Boolean

        Dim rtnVal As Boolean = False
        Dim dsTaxCodes As DataSet = theSalesBiz.GetTaxCodesByZip(storeZip)
        Dim dtTaxCodes As DataTable = dsTaxCodes.Tables(0)
        Dim taxRows As Integer = dtTaxCodes.Rows.Count()

        ucTaxUpdate.Visible = taxRows <> 1

        If (taxRows = 1) Then
            Session("TAX_RATE") = dtTaxCodes.Rows(0).Item("SumofPCT").ToString
            Session("TAX_CD") = dtTaxCodes.Rows(0).Item("TAX_CD").ToString

            '** The 'manual' tax cd is the tax, before the user consciously modifies it( via the tax icon button). 
            '** Since the system is auto assigning the tax, maunal code should be set to nothing.
            Session("MANUAL_TAX_CD") = Nothing
            rtnVal = True
        Else
            Dim _hidZip As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
            _hidZip.Value = storeZip
            ucTaxUpdate.LoadTaxData()
        End If

        Return rtnVal
    End Function

    Private Sub InsertRecords(ByVal theItmCd As String)

        ' If only one tax code on the store AND a swatch SKU checkout, then this logic is used
        Dim auto_add As Boolean = False

        If (theItmCd.isNotEmpty AndAlso theItmCd <> "NONEENTERED") Then

            Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim itemcd As String = ""
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim MyTable As DataTable
            Dim sql As String
            Dim sAdp As OracleDataAdapter
            Dim ds As DataSet
            Dim numrows As Integer
            Dim loop1 As Integer
            Dim sku As String = ""
            Dim VeCd As String = ""
            Dim mnrcd As String = ""
            Dim catcd As String = ""
            Dim point_size As String = ""
            Dim retprc, pass_ret_prc, adv_prc As Double
            Dim prom_prc As String = ""
            Dim cost As Double
            Dim vsn As String = ""
            Dim comm_cd As String = ""
            Dim spiff As String = ""
            Dim des As String = ""
            Dim siz As String = ""
            Dim finish As String = ""
            Dim cover As String = ""
            Dim grade As String = ""
            Dim stylecd As String = ""
            Dim itm_tp_cd As String = ""
            Dim treatable As String = ""
            Dim thistransaction As OracleTransaction
            Dim dropped_SKUS As String = ""
            Dim dropped_dialog As String = ""
            Dim User_Questions As String = ""
            Dim tax_cd As String = ""
            Dim tax_pct As String = ""
            Dim taxable_amt As String = ""
            Dim row_id As String = ""
            User_Questions = ""
            dropped_SKUS = ""
            dropped_dialog = ""

            objConnection.Open()

            conn.Open()
            itemcd = theItmCd
            itemcd = "'" + itemcd.Replace(",", "','") + "'"
            itemcd = "(" + itemcd + ")"

            sql = "SELECT ITM_CD, VE_CD, MNR_CD, CAT_CD, RET_PRC, ADV_PRC, VSN, DES, SIZ, FINISH, COVER, GRADE, STYLE_CD, ITM_TP_CD,TREATABLE, FRAME_TP_ITM, DROP_CD, DROP_DT, USER_QUESTIONS, POINT_SIZE, REPL_CST, COMM_CD, SPIFF from itm where itm_cd in " & itemcd & ""

            sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)


            ds = New DataSet
            sAdp.Fill(ds)
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            If numrows = 1 And Request("auto_add") = "TRUE" Then
                auto_add = True
            End If

            If numrows = 0 Then
                Exit Sub
            End If

            Dim taxability As String = ""
            Dim PKG_SKUS As String
            PKG_SKUS = ""
            Dim CUSTOM_SKUS As String
            CUSTOM_SKUS = ""
            Dim objSql As OracleCommand
            Dim objSql2 As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim MyDataReader2 As OracleDataReader

            For loop1 = 0 To numrows - 1
                sku = MyTable.Rows(loop1).Item("ITM_CD").ToString
                If MyTable.Rows(loop1).Item("USER_QUESTIONS").ToString & "" <> "" Then
                    User_Questions = User_Questions & MyTable.Rows(loop1).Item("USER_QUESTIONS").ToString & "<br />"
                End If
                If IsDate(MyTable.Rows(loop1).Item("DROP_DT").ToString) Then
                    dropped_SKUS = FormatDateTime(MyTable.Rows(loop1).Item("DROP_DT").ToString, DateFormat.ShortDate)
                Else
                    dropped_SKUS = ""
                End If
                If dropped_SKUS & "" <> "" Then
                    If FormatDateTime(dropped_SKUS, DateFormat.ShortDate) < Now() Then
                        dropped_dialog = dropped_dialog & "SKU " & sku & "(" & MyTable.Rows(loop1).Item("VSN").ToString & ") was dropped on " & dropped_SKUS & "<br />"
                    End If
                End If
                If MyTable.Rows(loop1).Item("ITM_TP_CD").ToString = "PKG" Then
                    PKG_SKUS = PKG_SKUS & "'" & sku & "',"
                End If
                If MyTable.Rows(loop1).Item("FRAME_TP_ITM").ToString = "Y" Then
                    CUSTOM_SKUS = CUSTOM_SKUS & sku & ","
                End If
                VeCd = MyTable.Rows(loop1).Item("VE_CD").ToString
                mnrcd = MyTable.Rows(loop1).Item("MNR_CD").ToString
                If IsDBNull(MyTable.Rows(loop1).Item("CAT_CD")) = False Then
                    catcd = MyTable.Rows(loop1).Item("CAT_CD").ToString
                Else
                    catcd = ""
                End If
                If IsDBNull(MyTable.Rows(loop1).Item("COMM_CD")) = False Then
                    comm_cd = MyTable.Rows(loop1).Item("COMM_CD").ToString
                Else
                    comm_cd = ""
                End If
                If ConfigurationManager.AppSettings("package_breakout").ToString = "Y" And MyTable.Rows(loop1).Item("ITM_TP_CD") = "PKG" Then
                    pass_ret_prc = CDbl(MyTable.Rows(loop1).Item("RET_PRC"))
                    retprc = 0
                Else
                    retprc = CDbl(MyTable.Rows(loop1).Item("RET_PRC"))
                    pass_ret_prc = CDbl(MyTable.Rows(loop1).Item("RET_PRC"))
                End If
                If IsNumeric(MyTable.Rows(loop1).Item("ADV_PRC")) Then
                    adv_prc = CDbl(MyTable.Rows(loop1).Item("ADV_PRC"))
                Else
                    adv_prc = 0
                End If
                'If adv_prc < retprc And adv_prc > 0 Then retprc = adv_prc
                If MyTable.Rows(loop1).Item("REPL_CST") & "" <> "" Then
                    cost = CDbl(MyTable.Rows(loop1).Item("REPL_CST").ToString)
                Else
                    cost = 0
                End If
                vsn = MyTable.Rows(loop1).Item("VSN").ToString
                If MyTable.Rows(loop1).Item("DES").ToString & "" <> "" Then
                    des = MyTable.Rows(loop1).Item("DES").ToString
                End If
                If MyTable.Rows(loop1).Item("POINT_SIZE").ToString & "" <> "" Then
                    point_size = MyTable.Rows(loop1).Item("POINT_SIZE").ToString
                End If
                If MyTable.Rows(loop1).Item("SPIFF").ToString & "" <> "" Then
                    If IsNumeric(MyTable.Rows(loop1).Item("SPIFF").ToString) Then
                        spiff = MyTable.Rows(loop1).Item("SPIFF").ToString
                    Else
                        spiff = 0
                    End If
                Else
                    spiff = 0
                End If
                If point_size & "" = "" Then point_size = "0"
                itm_tp_cd = MyTable.Rows(loop1).Item("ITM_TP_CD").ToString
                treatable = MyTable.Rows(loop1).Item("TREATABLE").ToString
                thistransaction = objConnection.BeginTransaction

                If itm_tp_cd = "INV" Then
                    Check_Warranty(sku)
                End If
                prom_prc = Find_Promotions(sku)
                If IsNumeric(prom_prc) Then
                    If CDbl(prom_prc) < retprc Then
                        retprc = CDbl(prom_prc)
                        pass_ret_prc = CDbl(prom_prc)
                    End If
                End If

                'Determine if the select item should be taxed.
                If (Session("TAX_CD") & "" <> "") Then
                    taxability = TaxUtils.Determine_Taxability(itm_tp_cd, Date.Today, Session("TAX_CD"))
                End If
                If taxability = "Y" And String.IsNullOrEmpty(Session("tet_cd")) Then

                    tax_cd = Session("TAX_CD")
                    tax_pct = Session("TAX_RATE")
                    taxable_amt = retprc
                Else
                    tax_cd = 0
                    tax_pct = 0
                    taxable_amt = 0
                End If
                If Not IsNumeric(tax_pct) Then tax_pct = 0
                If Not IsNumeric(retprc) Then retprc = 0
                If Not IsNumeric(point_size) Then point_size = 0
                If Not IsNumeric(cost) Then cost = 0
                If Not IsNumeric(tax_pct) Then tax_pct = 0
                If Not IsNumeric(taxable_amt) Then taxable_amt = 0
                Dim Calc_Ret As Double
                Calc_Ret = taxable_amt * (tax_pct / 100)

                sql = "insert into temp_itm (session_id,ITM_CD,VE_CD,MNR_CD,CAT_CD,RET_PRC,ORIG_PRC,MANUAL_PRC,VSN,DES,SIZ,FINISH,COVER,GRADE, " &
                                            "STYLE_CD,ITM_tp_CD,TREATABLE,DEL_PTS, COST, COMM_CD, tax_cd, tax_pct, taxable_amt, SPIFF, TAX_AMT) values (" &
                                            ":sessId, :SKU, :VECD, :MNRCD, :CATCD, :RETPRC, :ORIGPRC, :MANUALPRC, :VSN, :DES, :SIZ, :FINISH, :COVER, :GRADE, " &
                                            ":STYLECD, :itmTp, :TREAT, :PT_SIZE, :COST, :COMM_CD, :TAX_CD, :TAX_PCT, :tax_basis, :SPIFF, ROUND(:tax_amt, :tax_prec)) "

                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = sql
                End With

                cmdInsertItems.Parameters.Add(":sessId", OracleType.VarChar)
                cmdInsertItems.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                cmdInsertItems.Parameters.Add(":SKU", OracleType.VarChar)
                cmdInsertItems.Parameters(":SKU").Value = sku
                cmdInsertItems.Parameters.Add(":VECD", OracleType.VarChar)
                cmdInsertItems.Parameters(":VECD").Value = VeCd
                cmdInsertItems.Parameters.Add(":MNRCD", OracleType.VarChar)
                cmdInsertItems.Parameters(":MNRCD").Value = mnrcd
                cmdInsertItems.Parameters.Add(":CATCD", OracleType.VarChar)
                cmdInsertItems.Parameters(":CATCD").Value = catcd
                cmdInsertItems.Parameters.Add(":RETPRC", OracleType.Number)
                cmdInsertItems.Parameters(":RETPRC").Value = retprc
                cmdInsertItems.Parameters.Add(":ORIGPRC", OracleType.Number)
                cmdInsertItems.Parameters(":ORIGPRC").Value = retprc
                cmdInsertItems.Parameters.Add(":MANUALPRC", OracleType.Number)
                cmdInsertItems.Parameters(":MANUALPRC").Value = retprc
                cmdInsertItems.Parameters.Add(":VSN", OracleType.VarChar)
                cmdInsertItems.Parameters(":VSN").Value = Replace(vsn, "'", "''") + ""
                cmdInsertItems.Parameters.Add(":DES", OracleType.VarChar)
                cmdInsertItems.Parameters(":DES").Value = Replace(des, "'", "''") + ""
                cmdInsertItems.Parameters.Add(":SIZ", OracleType.VarChar)
                cmdInsertItems.Parameters(":SIZ").Value = Replace(siz, "'", "''") + ""
                cmdInsertItems.Parameters.Add(":FINISH", OracleType.VarChar)
                cmdInsertItems.Parameters(":FINISH").Value = Replace(finish, "'", "''") + ""
                cmdInsertItems.Parameters.Add(":COVER", OracleType.VarChar)
                cmdInsertItems.Parameters(":COVER").Value = Replace(cover, "'", "''") + ""
                cmdInsertItems.Parameters.Add(":GRADE", OracleType.VarChar)
                cmdInsertItems.Parameters(":GRADE").Value = Replace(grade, "'", "''") + ""
                cmdInsertItems.Parameters.Add(":STYLECD", OracleType.VarChar)
                cmdInsertItems.Parameters(":STYLECD").Value = stylecd + ""
                cmdInsertItems.Parameters.Add(":itmTp", OracleType.VarChar)
                cmdInsertItems.Parameters(":itmTp").Value = itm_tp_cd
                cmdInsertItems.Parameters.Add(":TREAT", OracleType.VarChar)
                cmdInsertItems.Parameters(":TREAT").Value = treatable
                cmdInsertItems.Parameters.Add(":PT_SIZE", OracleType.Number)
                If (Not point_size Is Nothing) AndAlso IsNumeric(point_size) Then
                    cmdInsertItems.Parameters(":PT_SIZE").Value = point_size
                Else
                    cmdInsertItems.Parameters(":PT_SIZE").Value = System.DBNull.Value
                End If
                cmdInsertItems.Parameters.Add(":COST", OracleType.Number)
                cmdInsertItems.Parameters(":COST").Value = cost
                cmdInsertItems.Parameters.Add(":COMM_CD", OracleType.VarChar)
                cmdInsertItems.Parameters(":COMM_CD").Value = comm_cd
                cmdInsertItems.Parameters.Add(":TAX_CD", OracleType.VarChar)
                cmdInsertItems.Parameters(":TAX_CD").Value = tax_cd + ""
                cmdInsertItems.Parameters.Add(":TAX_PCT", OracleType.Number)
                If (Not tax_pct Is Nothing) AndAlso IsNumeric(tax_pct) Then
                    cmdInsertItems.Parameters(":TAX_PCT").Value = tax_pct
                Else
                    cmdInsertItems.Parameters(":TAX_PCT").Value = System.DBNull.Value
                End If
                cmdInsertItems.Parameters.Add(":tax_basis", OracleType.Number)
                If (Not taxable_amt Is Nothing) AndAlso IsNumeric(taxable_amt) Then
                    cmdInsertItems.Parameters(":tax_basis").Value = taxable_amt
                Else
                    cmdInsertItems.Parameters(":tax_basis").Value = System.DBNull.Value
                End If
                cmdInsertItems.Parameters.Add(":SPIFF", OracleType.Number)
                If (Not spiff Is Nothing) AndAlso IsNumeric(spiff) Then
                    cmdInsertItems.Parameters(":SPIFF").Value = spiff
                Else
                    cmdInsertItems.Parameters(":SPIFF").Value = System.DBNull.Value
                End If
                cmdInsertItems.Parameters.Add(":tax_amt", OracleType.Number)
                cmdInsertItems.Parameters(":tax_amt").Value = Calc_Ret
                cmdInsertItems.Parameters.Add(":tax_prec", OracleType.Number)
                cmdInsertItems.Parameters(":tax_prec").Value = TaxUtils.Tax_Constants.taxPrecision

                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                point_size = "0"

                'Check the INV_MNR_CAT table for fab protection to auto-insert, if system parameters allow it
                If ConfigurationManager.AppSettings("fab_populate").ToString = "Y" And treatable = "Y" Then

                    sql = "SELECT ROW_ID FROM TEMP_ITM WHERE SESSION_ID=:SESSIONID AND ITM_CD = :SKU ORDER BY ROW_ID DESC"
                    objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                    objSql2.Parameters.Add(":SESSIONID", OracleType.VarChar)
                    objSql2.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
                    objSql2.Parameters.Add(":SKU", OracleType.VarChar)
                    objSql2.Parameters(":SKU").Value = sku

                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    Try
                        If (MyDataReader2.Read()) Then
                            row_id = MyDataReader2.Item("ROW_ID")
                        End If
                        MyDataReader2.Close()
                    Catch ex As Exception
                        objConnection.Close()
                        conn.Close()
                        Throw
                    End Try

                    sql = "SELECT ITM_CD, VE_CD, MNR_CD, ADV_PRC, CAT_CD, RET_PRC, VSN, DES, SIZ, FINISH, COVER, GRADE, STYLE_CD, ITM_TP_CD,TREATABLE, FRAME_TP_ITM, DROP_CD, DROP_DT, USER_QUESTIONS, POINT_SIZE, REPL_CST, COMM_CD, SPIFF from itm where itm_cd in "
                    sql = sql & "(SELECT TREATMENT_ITM_CD FROM INV_MNR_CAT WHERE MNR_CD = :MNRCD AND CAT_CD = :CATCD)"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.Parameters.Add(":MNRCD", OracleType.VarChar)
                    objSql.Parameters(":MNRCD").Value = mnrcd
                    objSql.Parameters.Add(":CATCD", OracleType.VarChar)
                    objSql.Parameters(":CATCD").Value = catcd

                    Try
                        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                        If (MyDataReader.Read()) Then
                            sku = MyDataReader.Item("ITM_CD")
                            If MyDataReader.Item("USER_QUESTIONS") & "" <> "" Then
                                User_Questions = User_Questions & MyDataReader.Item("USER_QUESTIONS").ToString & "<br />"
                            End If
                            dropped_SKUS = MyDataReader.Item("DROP_DT").ToString
                            If dropped_SKUS & "" <> "" Then
                                If FormatDateTime(dropped_SKUS, DateFormat.ShortDate) < Now() Then
                                    dropped_dialog = dropped_dialog & "SKU " & sku & "(" & MyDataReader.Item("VSN") & ") was dropped on " & dropped_SKUS & "<br />"
                                End If
                            End If
                            If MyDataReader.Item("ITM_TP_CD") = "PKG" Then
                                PKG_SKUS = PKG_SKUS & "'" & sku & "',"
                            End If
                            If MyDataReader.Item("FRAME_TP_ITM") = "Y" Then
                                CUSTOM_SKUS = CUSTOM_SKUS & sku & ","
                            End If
                            VeCd = MyDataReader.Item("VE_CD").ToString
                            mnrcd = MyDataReader.Item("MNR_CD").ToString
                            If IsDBNull(MyDataReader.Item("CAT_CD")) = False Then
                                catcd = MyDataReader.Item("CAT_CD").ToString
                            Else
                                catcd = ""
                            End If
                            retprc = CDbl(MyDataReader.Item("RET_PRC"))
                            If IsNumeric(MyDataReader.Item("ADV_PRC")) Then
                                adv_prc = CDbl(MyDataReader.Item("ADV_PRC"))
                            Else
                                adv_prc = 0
                            End If
                            'If adv_prc < retprc And adv_prc > 0 Then retprc = adv_prc
                            vsn = MyDataReader.Item("VSN").ToString
                            If MyDataReader.Item("DES") & "" <> "" Then
                                des = MyDataReader.Item("DES").ToString
                            End If
                            If IsDBNull(MyDataReader.Item("COMM_CD")) = False Then
                                comm_cd = MyDataReader.Item("COMM_CD").ToString
                            Else
                                comm_cd = ""
                            End If
                            If MyDataReader.Item("REPL_CST") & "" <> "" Then
                                cost = CDbl(MyDataReader.Item("REPL_CST").ToString)
                            Else
                                cost = 0
                            End If
                            If MyDataReader.Item("POINT_SIZE").ToString & "" <> "" Then
                                point_size = MyDataReader.Item("POINT_SIZE").ToString
                            End If
                            If MyTable.Rows(loop1).Item("SPIFF").ToString & "" <> "" Then
                                If IsNumeric(MyTable.Rows(loop1).Item("SPIFF").ToString) Then
                                    spiff = MyTable.Rows(loop1).Item("SPIFF").ToString
                                Else
                                    spiff = 0
                                End If
                            Else
                                spiff = 0
                            End If
                            If point_size & "" = "" Then point_size = "0"
                            itm_tp_cd = MyDataReader.Item("ITM_TP_CD").ToString
                            treatable = MyDataReader.Item("TREATABLE").ToString
                            thistransaction = objConnection.BeginTransaction

                            If itm_tp_cd = "INV" Then
                                Check_Warranty(sku)
                            End If

                            prom_prc = Find_Promotions(sku)
                            If IsNumeric(prom_prc) Then
                                If CDbl(prom_prc) < retprc Then
                                    pass_ret_prc = CDbl(prom_prc)
                                    retprc = CDbl(prom_prc)
                                End If
                            End If

                            'Determine if the select item should be taxed.
                            tax_cd = 0
                            tax_pct = 0
                            taxable_amt = 0

                            If Not IsNumeric(tax_pct) Then tax_pct = 0
                            sql = "insert into temp_itm (session_id,ITM_CD,VE_CD,MNR_CD,CAT_CD,RET_PRC,ORIG_PRC,MANUAL_PRC,VSN,DES,SIZ,FINISH,COVER,GRADE,STYLE_CD," &
                                                        "ITM_tp_CD,TREATABLE,DEL_PTS, COST, COMM_CD, tax_cd, tax_pct, taxable_amt, SPIFF) values ('" &
                                                        Session.SessionID.ToString.Trim & "','" & sku & "','" & VeCd & "','" & mnrcd & "','" & catcd & "'," & retprc & retprc & retprc & ",'" &
                                                        Replace(vsn, "'", "''") & "','" & Replace(des, "'", "''") & "','" & Replace(siz, "'", "''") & "','" & Replace(finish, "'", "''") & "','" &
                                                        Replace(cover, "'", "''") & "','" & Replace(grade, "'", "''") & "','" & stylecd & "','" & itm_tp_cd & "','" & treatable & "'," & point_size & "," &
                                                        cost & ",'" & comm_cd & "','" & tax_cd & "'," & tax_pct & "," & taxable_amt & "," & spiff & ")"
                            With cmdInsertItems
                                .Transaction = thistransaction
                                .Connection = objConnection
                                .CommandText = sql
                            End With
                            cmdInsertItems.ExecuteNonQuery()
                            thistransaction.Commit()
                            thistransaction = objConnection.BeginTransaction
                            With cmdInsertItems
                                .Transaction = thistransaction
                                .Connection = objConnection
                                .CommandText = "UPDATE TEMP_ITM SET TREATED_BY='" & CDbl(row_id) + 1 & "', TREATED='Y' WHERE ROW_ID=" & CDbl(row_id)
                            End With
                            cmdInsertItems.ExecuteNonQuery()
                            thistransaction.Commit()
                        End If
                        MyDataReader.Close()

                    Catch ex As Exception
                        objConnection.Close()
                        conn.Close()
                        Throw
                    End Try
                End If
                If PKG_SKUS & "" <> "" Then
                    Add_Packages(PKG_SKUS)
                    PKG_SKUS = ""
                End If
            Next

            point_size = "0"

            Dim strScript As String = ""
            objConnection.Close()
            conn.Close()
        End If
    End Sub

    Public Sub Check_Warranty(ByVal itm_cd As String)

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim strScript As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        'Search for related SKUS on the order
        Dim warranty_skus As Boolean = False

        sql = "SELECT WAR_ITM_CD, WAR_RET_PRC, ITM.VSN  || ' (' || ITM.WARR_DAYS || ' DAYS) ' AS VSN FROM ITM, ITM2ITM_WARR "
        sql = sql & "WHERE(ITM2ITM_WARR.WAR_ITM_CD = ITM.ITM_CD) AND ITM2ITM_WARR.ITM_CD IN('" & itm_cd & "') ORDER BY ITM.WARR_DAYS"
        sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
        ds = New DataSet
        sAdp.Fill(ds)
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        For loop1 = 0 To numrows - 1
            If MyTable.Rows(loop1).Item("war_itm_cd").ToString & "" <> "" Then
                warranty_skus = True
                Exit For
            End If
        Next

        If warranty_skus = True Then
            strScript = "<script type='text/javascript'>window.open('Warranties.aspx?itm_cd=" & itm_cd & "','frmTest" & itm_cd & "','height=390px,width=560px,top=50,left=50,status=no,toolbar=no,menubar=no,scrollbars=yes');</script>"
            ClientScript.RegisterStartupScript(Me.GetType(), "AvisoScript" & itm_cd, strScript)
        End If
        conn.Close()

    End Sub

    Public Function Find_Promotions(ByVal sku As String)

        Dim conn6 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim MyDatareader6 As OracleDataReader
        Dim objsql6 As OracleCommand

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn6.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        'Open Connection 
        conn6.Open()

        sql = "SELECT PRC FROM PROM_PRC_TP, STORE_PROM_PRC WHERE PROM_PRC_TP.PRC_CD=STORE_PROM_PRC.PRC_CD "

        'jkl
        'sql = sql & "AND ITM_CD='" & sku & "' AND STORE_CD='" & Session("store_cd") & "' AND EFF_DT<=TO_DATE('"
        sql = sql & "AND ITM_CD=:SKU AND STORE_CD=:STORE_CD AND EFF_DT<=TO_DATE('"

        sql = sql & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') AND END_DT>=TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') "
        sql = sql & "ORDER BY PRC"

        'Set SQL OBJECT 
        objsql6 = DisposablesManager.BuildOracleCommand(sql, conn6)
        'jkl
        objsql6.Parameters.Add(":SKU", OracleType.VarChar)
        objsql6.Parameters(":SKU").Value = sku
        objsql6.Parameters.Add(":STORE_CD", OracleType.VarChar)
        objsql6.Parameters(":STORE_CD").Value = Session("store_cd")

        Try
            'Execute DataReader 
            MyDatareader6 = DisposablesManager.BuildOracleDataReader(objsql6)


            If MyDatareader6.Read Then
                Find_Promotions = MyDatareader6.Item("PRC").ToString
                conn6.Close()
                Exit Function
            End If
            MyDatareader6.Close()
        Catch
            conn6.Close()
            Throw
        End Try

        sql = "SELECT PRC FROM PROM_PRC_TP, STORE_GRP_PROM_PRC  WHERE PROM_PRC_TP.PRC_CD=STORE_GRP_PROM_PRC .PRC_CD "
        sql = sql & "AND ITM_CD=:SKU AND STORE_GRP_CD IN (SELECT STORE_GRP_CD FROM STORE_GRP$STORE WHERE STORE_CD=:STORE_CD) AND EFF_DT<=TO_DATE('"
        sql = sql & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') AND END_DT>=TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') "
        sql = sql & "ORDER BY PRC"

        'Set SQL OBJECT 
        objsql6 = DisposablesManager.BuildOracleCommand(sql, conn6)
        'jkl
        objsql6.Parameters.Add(":SKU", OracleType.VarChar)
        objsql6.Parameters(":SKU").Value = sku
        objsql6.Parameters.Add(":STORE_CD", OracleType.VarChar)
        objsql6.Parameters(":STORE_CD").Value = Session("store_cd")
        Try
            'Execute DataReader 
            MyDatareader6 = DisposablesManager.BuildOracleDataReader(objsql6)


            If MyDatareader6.Read Then
                Find_Promotions = MyDatareader6.Item("PRC").ToString
                conn6.Close()
                Exit Function
            End If
            MyDatareader6.Close()
        Catch
            conn6.Close()
            Throw
        End Try

        conn6.Close()

        Find_Promotions = ""

    End Function

    Public Sub Add_Packages(ByVal PKG_SKUS As String)

        ' DSA - appears that CC uses the order_detail logic and that this is not used at all 
        '   that is good because then it isn't duplicated problems
        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim sku As String = ""
        Dim VeCd As String = ""
        Dim mnrcd As String = ""
        Dim catcd As String = ""
        Dim spiff As String = ""
        Dim point_size As String = ""
        Dim adv_prc As Double
        Dim retprc As Double
        Dim cost As Double
        Dim qty As Double
        Dim vsn As String = ""
        Dim comm_cd As String = ""
        Dim des As String = ""
        Dim siz As String = ""
        Dim finish As String = ""
        Dim cover As String = ""
        Dim grade As String = ""
        Dim prom_prc As String = ""
        Dim stylecd As String = ""
        Dim itm_tp_cd As String = ""
        Dim treatable As String = ""
        Dim tax_cd As String = ""
        Dim tax_pct As String = ""
        Dim taxable_amt As String = ""
        Dim original_retail As Double = 0
        Dim pkg_items As String

        Dim thistransaction As OracleTransaction
        Dim dropped_SKUS As String
        Dim dropped_dialog As String
        Dim User_Questions As String
        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        User_Questions = ""
        dropped_SKUS = ""
        dropped_dialog = ""

        objConnection.Open()

        conn.Open()
        'Populate the table for any package SKUS that were created
        Dim PKG_GO As Boolean
        PKG_GO = False
        If PKG_SKUS & "" <> "" Then

            If Right(PKG_SKUS, 1) = "," Then PKG_SKUS = Left(PKG_SKUS, Len(PKG_SKUS) - 1)

            'If we are to populate the retail amounts, we need to calculate based on cost percentages
            sql = "SELECT ITM_CD, REPL_CST, PKG_ITM_CD from itm, package where pkg_itm_cd in (:PKG_SKUS) "
            sql = sql & "and package.cmpnt_itm_cd=itm.itm_cd order by pkg_itm_cd, seq"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.Parameters.Add(":PKG_SKUS", OracleType.VarChar)
            objSql.Parameters(":PKG_SKUS").Value = PKG_SKUS

            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read()
                pkg_items = pkg_items & ",'" & MyDataReader.Item("ITM_CD").ToString & "'"
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    'jkl not sure if this should be parameterized
                    .CommandText = "insert into PACKAGE_BREAKOUT (session_id,PKG_SKU,COMPONENT_SKU,COST) values ('" & Session.SessionID.ToString.Trim & "','" & MyDataReader.Item("PKG_ITM_CD").ToString & "','" & MyDataReader.Item("ITM_CD").ToString & "'," & MyDataReader.Item("REPL_CST").ToString & ")"
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()
            sql = "SELECT ITM_CD, RET_PRC, ADV_PRC from itm where itm_cd in (:PKG_SKUS) "
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.Parameters.Add(":PKG_SKUS", OracleType.VarChar)
            objSql.Parameters(":PKG_SKUS").Value = PKG_SKUS

            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read()
                If IsNumeric(MyDataReader.Item("RET_PRC")) Then
                    retprc = CDbl(MyDataReader.Item("RET_PRC"))
                Else
                    retprc = 0
                End If
                If IsNumeric(MyDataReader.Item("ADV_PRC")) Then
                    adv_prc = CDbl(MyDataReader.Item("ADV_PRC"))
                Else
                    adv_prc = 0
                End If

                original_retail = retprc
                retprc = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                     SessVar.wrStoreCd,
                                                     SessVar.custTpPrcCd,
                                                     Session("tran_dt"),
                                                     MyDataReader.Item("ITM_CD"),
                                                     retprc)

                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_RETAIL=" & retprc & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & MyDataReader.Item("ITM_CD") & "'"
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()
            sql = "SELECT pkg_ITM_CD, SUM(REPL_CST) As TOTAL_COST from itm, package where pkg_itm_cd in (:PKG_SKUS) and package.cmpnt_itm_cd=itm.itm_cd GROUP BY pkg_ITM_CD"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.Parameters.Add(":PKG_SKUS", OracleType.VarChar)
            objSql.Parameters(":PKG_SKUS").Value = PKG_SKUS

            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Calculate the total cost
            Do While MyDataReader.Read()
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_COST=" & MyDataReader.Item("TOTAL_COST") & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & MyDataReader.Item("PKG_ITM_CD") & "'"
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()

            If PKG_GO = True Then
                Dim PERCENT_OF_TOTAL As Double
                Dim NEW_RETAIL As Double
                Dim TOTAL_NEW_RETAIL As Double
                Dim TOTAL_RETAIL As Double
                Dim LAST_SKU As String
                Dim LAST_PKG_SKU As String

                'Calculate the percentage based on cost/total cost
                sql = "SELECT * FROM PACKAGE_BREAKOUT WHERE SESSION_ID = :SESSIONID and PKG_SKU in (:PKG_SKUS) ORDER BY PKG_SKU"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                objSql2.Parameters.Add(":SESSIONID", OracleType.VarChar)
                objSql2.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
                objSql2.Parameters.Add(":PKG_SKUS", OracleType.VarChar)
                objSql2.Parameters(":PKG_SKUS").Value = PKG_SKUS

                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read()
                    PERCENT_OF_TOTAL = CDbl(MyDataReader2.Item("COST").ToString) / CDbl(MyDataReader2.Item("TOTAL_COST").ToString)
                    NEW_RETAIL = PERCENT_OF_TOTAL * CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                    TOTAL_NEW_RETAIL = TOTAL_NEW_RETAIL + NEW_RETAIL
                    TOTAL_RETAIL = CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                    LAST_SKU = MyDataReader2.Item("COMPONENT_SKU").ToString
                    LAST_PKG_SKU = MyDataReader2.Item("PKG_SKU").ToString
                    If PERCENT_OF_TOTAL.ToString = "NaN" Then PERCENT_OF_TOTAL = 0
                    thistransaction = objConnection.BeginTransaction
                    With cmdInsertItems
                        .Transaction = thistransaction
                        .Connection = objConnection
                        .CommandText = "UPDATE PACKAGE_BREAKOUT SET PERCENT_OF_TOTAL = " & PERCENT_OF_TOTAL & ", NEW_RETAIL=" & Math.Round(NEW_RETAIL, 2, MidpointRounding.AwayFromZero) & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & MyDataReader2.Item("PKG_SKU").ToString & "' AND COMPONENT_SKU='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "'"
                    End With
                    cmdInsertItems.ExecuteNonQuery()
                    thistransaction.Commit()
                Loop

                MyDataReader2.Close()
                objSql2.Dispose()
            End If

            ' End If
            sql = "SELECT ITM_CD, VE_CD, MNR_CD, CAT_CD, package.PKG_ITM_CD, package.RET_PRC, PACKAGE.QTY, VSN, DES, SIZ, FINISH, COVER, GRADE, STYLE_CD, ITM_TP_CD, TREATABLE, ITM.DROP_CD, ITM.DROP_DT, ITM.USER_QUESTIONS, POINT_SIZE, REPL_CST, COMM_CD, SPIFF from itm, package where pkg_itm_cd in (" & PKG_SKUS & ") "
            sql = sql & "and package.cmpnt_itm_cd=itm.itm_cd order by pkg_itm_cd, seq"
            sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
            ds = New DataSet
            sAdp.Fill(ds)
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            For loop1 = 0 To numrows - 1
                sku = MyTable.Rows(loop1).Item("ITM_CD").ToString
                dropped_SKUS = MyTable.Rows(loop1).Item("DROP_DT").ToString
                If MyTable.Rows(loop1).Item("USER_QUESTIONS") & "" <> "" Then
                    User_Questions = User_Questions & MyTable.Rows(loop1).Item("USER_QUESTIONS").ToString & "<br />"
                End If
                If dropped_SKUS & "" <> "" Then
                    If FormatDateTime(dropped_SKUS, DateFormat.ShortDate) < Now() Then
                        dropped_dialog = dropped_dialog & "SKU " & sku & "(" & MyTable.Rows(loop1).Item("VSN").ToString & ") was dropped on " & dropped_SKUS & "<br />"
                    End If
                End If
                VeCd = MyTable.Rows(loop1).Item("VE_CD").ToString
                mnrcd = MyTable.Rows(loop1).Item("MNR_CD").ToString
                catcd = MyTable.Rows(loop1).Item("CAT_CD").ToString
                itm_tp_cd = MyTable.Rows(loop1).Item("ITM_TP_CD").ToString
                treatable = MyTable.Rows(loop1).Item("TREATABLE").ToString
                If MyTable.Rows(loop1).Item("REPL_CST") & "" <> "" Then
                    cost = CDbl(MyTable.Rows(loop1).Item("REPL_CST").ToString)
                Else
                    cost = 0
                End If
                If IsNumeric(MyTable.Rows(loop1).Item("QTY").ToString) Then
                    qty = MyTable.Rows(loop1).Item("QTY").ToString
                Else
                    qty = 1
                End If
                sql = "SELECT NEW_RETAIL FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND COMPONENT_SKU='" & sku & "' AND PKG_SKU='" & MyTable.Rows(loop1).Item("PKG_ITM_CD").ToString & "'"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                If ConfigurationManager.AppSettings("package_breakout").ToString = "Y" Then
                    If MyDataReader2.Read() Then
                        retprc = MyDataReader2.Item("NEW_RETAIL")
                        taxable_amt = MyDataReader2.Item("NEW_RETAIL")
                    Else
                        retprc = 0
                        taxable_amt = 0
                    End If
                Else
                    If MyDataReader2.Read() Then
                        taxable_amt = MyDataReader2.Item("NEW_RETAIL")
                    Else
                        taxable_amt = 0
                    End If
                    If IsNumeric(MyTable.Rows(loop1).Item("RET_PRC")) Then
                        retprc = CDbl(MyTable.Rows(loop1).Item("RET_PRC"))
                    Else
                        retprc = 0
                    End If
                End If
                If IsNumeric(retprc) And IsNumeric(qty) Then
                    retprc = FormatNumber((retprc / qty), 2)
                End If
                If IsNumeric(taxable_amt) And IsNumeric(qty) Then
                    taxable_amt = FormatNumber((taxable_amt / qty), 2)
                End If
                MyDataReader2.Close()
                objSql2.Dispose()
                vsn = MyTable.Rows(loop1).Item("VSN").ToString
                If MyTable.Rows(loop1).Item("COMM_CD") & "" <> "" Then
                    comm_cd = MyTable.Rows(loop1).Item("COMM_CD").ToString
                Else
                    comm_cd = ""
                End If
                If MyTable.Rows(loop1).Item("DES").ToString & "" <> "" Then
                    des = MyTable.Rows(loop1).Item("DES").ToString
                End If
                If MyTable.Rows(loop1).Item("POINT_SIZE").ToString & "" <> "" Then
                    point_size = MyTable.Rows(loop1).Item("POINT_SIZE").ToString
                End If
                If MyTable.Rows(loop1).Item("SPIFF").ToString & "" <> "" Then
                    If IsNumeric(MyTable.Rows(loop1).Item("SPIFF").ToString) Then
                        spiff = MyTable.Rows(loop1).Item("SPIFF").ToString
                    Else
                        spiff = 0
                    End If
                Else
                    spiff = 0
                End If
                If point_size & "" = "" Then point_size = "0"

                thistransaction = objConnection.BeginTransaction

                If itm_tp_cd = "INV" Then
                    Check_Warranty(sku)
                End If

                prom_prc = Find_Promotions(sku)
                If IsNumeric(prom_prc) Then
                    If CDbl(prom_prc) < retprc Then
                        retprc = CDbl(prom_prc)
                    End If
                End If
                'Determine if the select item should be taxed.
                tax_cd = 0
                tax_pct = 0
                taxable_amt = 0

                If Not IsNumeric(tax_pct) Then tax_pct = 0
                sql = "insert into temp_itm (session_id,ITM_CD,VE_CD,MNR_CD,CAT_CD,RET_PRC,ORIG_PRC,MANUAL_PRC,VSN,DES,SIZ,FINISH,COVER,GRADE, " &
                    "STYLE_CD,ITM_tp_CD,TREATABLE,DEL_PTS, COST, COMM_CD, tax_cd, tax_pct, taxable_amt, SPIFF, QTY) values ('" &
                    Session.SessionID.ToString.Trim & "','" & sku & "','" & VeCd & "','" & mnrcd & "','" & catcd & "'," & retprc & retprc & retprc & ",'" & Replace(vsn, "'", "''") & "','" &
                    Replace(des, "'", "''") & "','" & Replace(siz, "'", "''") & "','" & Replace(finish, "'", "''") & "','" & Replace(cover, "'", "''") & "','" &
                    Replace(grade, "'", "''") & "','" & stylecd & "','" & itm_tp_cd & "','" & treatable & "'," & point_size & "," & cost & ",'" & comm_cd & "','" &
                    tax_cd & "'," & tax_pct & "," & CDbl(taxable_amt) & "," & spiff & "," & qty & ")"

                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = sql
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()

                'Check the INV_MNR_CAT table for fab protection to auto-insert, if system parameters allow it
                If ConfigurationManager.AppSettings("fab_populate").ToString = "Y" And treatable = "Y" Then

                    Dim row_id As String = ""
                    Dim pass_ret_prc As Double
                    sql = "SELECT ROW_ID FROM TEMP_ITM WHERE SESSION_ID=:SESSIONID AND ITM_CD = :SKU ORDER BY ROW_ID DESC"
                    objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                    objSql2.Parameters.Add(":SESSIONID", OracleType.VarChar)
                    objSql2.Parameters(":SESSIONID").Value = Session.SessionID.ToString.Trim
                    objSql2.Parameters.Add(":SKU", OracleType.VarChar)
                    objSql2.Parameters(":SKU").Value = sku

                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    Try
                        If (MyDataReader2.Read()) Then
                            row_id = MyDataReader2.Item("ROW_ID")
                        End If
                        MyDataReader2.Close()
                    Catch ex As Exception
                        conn.Close()
                        objConnection.Close()
                        Throw
                    End Try

                    sql = "SELECT ITM_CD, VE_CD, MNR_CD, ADV_PRC, CAT_CD, RET_PRC, VSN, DES, SIZ, FINISH, COVER, GRADE, STYLE_CD, ITM_TP_CD,TREATABLE, FRAME_TP_ITM, DROP_CD, DROP_DT, USER_QUESTIONS, POINT_SIZE, REPL_CST, COMM_CD, SPIFF from itm where itm_cd in "
                    sql = sql & "(SELECT TREATMENT_ITM_CD FROM INV_MNR_CAT WHERE MNR_CD = :MNRCD AND CAT_CD = :CATCD)"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.Parameters.Add(":MNRCD", OracleType.VarChar)
                    objSql.Parameters(":CATCD").Value = catcd

                    Try
                        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                        If (MyDataReader.Read()) Then
                            sku = MyDataReader.Item("ITM_CD")
                            If MyDataReader.Item("USER_QUESTIONS") & "" <> "" Then
                                User_Questions = User_Questions & MyDataReader.Item("USER_QUESTIONS").ToString & "<br />"
                            End If
                            dropped_SKUS = MyDataReader.Item("DROP_DT").ToString
                            If dropped_SKUS & "" <> "" Then
                                If FormatDateTime(dropped_SKUS, DateFormat.ShortDate) < Now() Then
                                    dropped_dialog = dropped_dialog & "SKU " & sku & "(" & MyDataReader.Item("VSN") & ") was dropped on " & dropped_SKUS & "<br />"
                                End If
                            End If
                            If MyDataReader.Item("ITM_TP_CD") = "PKG" Then
                                PKG_SKUS = PKG_SKUS & "'" & sku & "',"
                            End If
                            VeCd = MyDataReader.Item("VE_CD").ToString
                            mnrcd = MyDataReader.Item("MNR_CD").ToString
                            If IsDBNull(MyDataReader.Item("CAT_CD")) = False Then
                                catcd = MyDataReader.Item("CAT_CD").ToString
                            Else
                                catcd = ""
                            End If
                            retprc = CDbl(MyDataReader.Item("RET_PRC"))
                            If IsNumeric(MyDataReader.Item("ADV_PRC")) Then
                                adv_prc = CDbl(MyDataReader.Item("ADV_PRC"))
                            Else
                                adv_prc = 0
                            End If
                            'If adv_prc < retprc And adv_prc > 0 Then retprc = adv_prc
                            vsn = MyDataReader.Item("VSN").ToString
                            If MyDataReader.Item("DES") & "" <> "" Then
                                des = MyDataReader.Item("DES").ToString
                            End If
                            If IsDBNull(MyDataReader.Item("COMM_CD")) = False Then
                                comm_cd = MyDataReader.Item("COMM_CD").ToString
                            Else
                                comm_cd = ""
                            End If
                            If MyDataReader.Item("REPL_CST") & "" <> "" Then
                                cost = CDbl(MyDataReader.Item("REPL_CST").ToString)
                            Else
                                cost = 0
                            End If
                            If MyDataReader.Item("POINT_SIZE").ToString & "" <> "" Then
                                point_size = MyDataReader.Item("POINT_SIZE").ToString
                            End If
                            If MyTable.Rows(loop1).Item("SPIFF").ToString & "" <> "" Then
                                If IsNumeric(MyTable.Rows(loop1).Item("SPIFF").ToString) Then
                                    spiff = MyTable.Rows(loop1).Item("SPIFF").ToString
                                Else
                                    spiff = 0
                                End If
                            Else
                                spiff = 0
                            End If
                            If point_size & "" = "" Then point_size = "0"
                            itm_tp_cd = MyDataReader.Item("ITM_TP_CD").ToString
                            treatable = MyDataReader.Item("TREATABLE").ToString
                            thistransaction = objConnection.BeginTransaction

                            If itm_tp_cd = "INV" Then
                                Check_Warranty(sku)
                            End If

                            prom_prc = Find_Promotions(sku)
                            If IsNumeric(prom_prc) Then
                                If CDbl(prom_prc) < retprc Then
                                    pass_ret_prc = CDbl(prom_prc)
                                    retprc = CDbl(prom_prc)
                                End If
                            End If

                            'Determine if the select item should be taxed.
                            tax_cd = 0
                            tax_pct = 0
                            taxable_amt = 0

                            If Not IsNumeric(tax_pct) Then tax_pct = 0
                            sql = "insert into temp_itm (session_id,ITM_CD,VE_CD,MNR_CD,CAT_CD,RET_PRC,ORIG_PRC,MANUAL_PRC,VSN,DES,SIZ,FINISH,COVER,GRADE," &
                                                    "STYLE_CD,ITM_tp_CD,TREATABLE,DEL_PTS, COST, COMM_CD, tax_cd, tax_pct, taxable_amt, SPIFF) values ('" &
                                                    Session.SessionID.ToString.Trim & "','" & sku & "','" & VeCd & "','" & mnrcd & "','" & catcd & "'," & retprc & retprc & retprc & ",'" &
                                                    Replace(vsn, "'", "''") & "','" & Replace(des, "'", "''") & "','" & Replace(siz, "'", "''") & "','" & Replace(finish, "'", "''") & "','" &
                                                    Replace(cover, "'", "''") & "','" & Replace(grade, "'", "''") & "','" & stylecd & "','" & itm_tp_cd & "','" & treatable & "'," &
                                                    point_size & "," & cost & ",'" & comm_cd & "','" & tax_cd & "'," & tax_pct & "," & taxable_amt & "," & spiff & ")"
                            With cmdInsertItems
                                .Transaction = thistransaction
                                .Connection = objConnection
                                .CommandText = sql
                            End With
                            cmdInsertItems.ExecuteNonQuery()
                            thistransaction.Commit()
                            thistransaction = objConnection.BeginTransaction
                            With cmdInsertItems
                                .Transaction = thistransaction
                                .Connection = objConnection
                                .CommandText = "UPDATE TEMP_ITM SET TREATED_BY='" & CDbl(row_id) + 1 & "', TREATED='Y' WHERE ROW_ID=" & CDbl(row_id)
                            End With
                            cmdInsertItems.ExecuteNonQuery()
                            thistransaction.Commit()
                        End If
                        MyDataReader.Close()

                    Catch ex As Exception
                        objConnection.Close()
                        conn.Close()
                        Throw
                    End Try
                End If
            Next

            'We're done, delete out all of the records to avoid duplication
            thistransaction = objConnection.BeginTransaction

            sql = "DELETE FROM PACKAGE_BREAKOUT WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "'"
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()

        End If

        conn.Close()
        objConnection.Close()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub ucTaxUpdate_TaxUpdated(sender As Object, e As EventArgs) Handles ucTaxUpdate.TaxUpdated
        Response.Redirect("order_detail.aspx?CUSTID=" & Session("CUST_CD") & "&referrer=CC")
        ucTaxUpdate.Visible = False
    End Sub
End Class
