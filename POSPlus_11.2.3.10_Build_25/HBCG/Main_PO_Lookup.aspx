<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Main_PO_Lookup.aspx.vb"
    Inherits="Main_PO_Lookup" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server">
        <dx:ASPxGridView ID="GridView1" runat="server" Width="98%" AutoGenerateColumns="False"
            KeyFieldName="PO_CD">
            <SettingsPager Visible="False">
            </SettingsPager>
            <Columns>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label418 %>" FieldName="PO_CD" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label573 %>" FieldName="STORE_CD" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label560 %>" FieldName="PO_SRT_CD" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label668 %>" FieldName="WR_DT" VisibleIndex="3">
                    <PropertiesTextEdit DisplayFormatString="MM/dd/yyyy">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Stat Code" FieldName="STAT_CD" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="8">
                    <DataItemTemplate>
                        <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                            <Items>
                                <dx:MenuItem Text="<%$ Resources:LibResources, Label528 %>" NavigateUrl="logout.aspx">
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <table width="98%">
            <tr>
                <td align="center">
                    <table width="75%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label210 %>"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label425 %>"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label337 %>"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label261 %>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="40%" align="center">
                                <dx:ASPxButton ID="btn_search_again" runat="server" Text="<%$ Resources:LibResources, Label527 %>">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="Div1">
        <dx:ASPxLabel ID="Label1" runat="server" Text="" Visible="false"></dx:ASPxLabel>
    </div>
</asp:Content>
