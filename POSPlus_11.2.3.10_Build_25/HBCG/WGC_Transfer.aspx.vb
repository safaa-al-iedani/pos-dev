﻿Imports System.Collections.Generic
Imports System.Data.OracleClient
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports Renci.SshNet
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp


Partial Class WGC_Transfer
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'sabrina R1922
        CreateCustomer()

        Session("gc_transfer") = "Y"
        If old_gc.Text <> "" Then
            Session("gct_old_gc") = old_gc.Text
        End If

        If Session("gct_old_gc") & "" <> "" Then
            old_gc.Text = Session("gct_old_gc")
        End If

        If Session("gct_old_gc_bal") & "" <> "" Then
            gc_bal.Text = Session("gct_old_gc_bal")
        End If

        If Session("gct_xfer_comment") & "" <> "" Then
            xfer_comment.Text = Session("gct_xfer_comment")
        End If

        If cust_cd.Text & "" <> "" Then
            Session("gct_cust_cd") = UCase(cust_cd.Text)
            Session("cust_cd") = UCase(cust_cd.Text)
            get_cust_info(UCase(Session("gct_cust_cd")))
        Else
            If Session("cust_cd") & "" <> "" Then
                Session("gct_cust_cd") = Session("cust_cd")
                cust_cd.Text = Session("cust_cd")
                get_cust_info(UCase(Session("gct_cust_cd")))
            End If
        End If

        If IsPostBack Then
            If cust_cd.Text = "" Then
                Session("gct_cust_cd") = ""
                Session("cust_cd") = ""
            Else
                get_cust_info(UCase(cust_cd.Text))
            End If
        End If


    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles swipe_btn1.Click

        xfer_msg.Text = ""

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim Ds As New DataSet()
        Dim l_crn As String
        Dim l_dsp As String

        Dim v_term_id As String = ""

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Daniela Oct 21
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            xfer_msg.Text = "Invalid Terminal ID " & v_term_id
            Exit Sub
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_brk_pos1.sendsubTp7_swipe"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_trantp", OracleType.VarChar)).Value = "SW"
            myCMD.Parameters.Add(New OracleParameter("p_subtp", OracleType.VarChar)).Value = "7"
            myCMD.Parameters.Add(New OracleParameter("p_displn1", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_displn2", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_rsvd", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_term_id", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cst", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 500).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            l_dsp = myCMD.Parameters("p_dsp").Value

            If InStr(UCase(myCMD.Parameters("p_dsp").Value), "SUCCESSFUL") > 0 Then

                l_crn = myCMD.Parameters("p_crn").Value

            Else   'transation not done 
                l_crn = ""
                xfer_msg.Text = "Click to Swipe Again"
            End If
            old_gc.Text = l_crn
            Session("gct_old_gc") = l_crn

            objConnection.Close()
        Catch
            objConnection.Close()
            Throw
        End Try


    End Sub

    Protected Sub swipe_btn2_Click(sender As Object, e As EventArgs) Handles swipe_btn2.Click

        xfer_msg.Text = ""

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim Ds As New DataSet()
        Dim l_crn As String
        Dim l_dsp As String

        Dim v_term_id As String = ""

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Daniela Oct 21
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            xfer_msg.Text = "Invalid Terminal ID " & v_term_id
            Exit Sub
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_brk_pos1.sendsubTp7_swipe"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_trantp", OracleType.VarChar)).Value = "SW"
            myCMD.Parameters.Add(New OracleParameter("p_subtp", OracleType.VarChar)).Value = "7"
            myCMD.Parameters.Add(New OracleParameter("p_displn1", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_displn2", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_rsvd", OracleType.VarChar)).Value = " "
            myCMD.Parameters.Add(New OracleParameter("p_term_id", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_rt2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr1", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cst", OracleType.VarChar, 500).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 500).Direction = ParameterDirection.Output

            myCMD.ExecuteNonQuery()

            l_dsp = myCMD.Parameters("p_dsp").Value

            If InStr(UCase(myCMD.Parameters("p_dsp").Value), "SUCCESSFUL") > 0 Then

                l_crn = myCMD.Parameters("p_crn").Value

            Else   'transation not done 
                l_crn = ""
                xfer_msg.Text = "Click to Swipe Again"
            End If

            new_gc.Text = l_crn
            Session("gct_new_gc") = l_crn

            objConnection.Close()
        Catch
            objConnection.Close()
            Throw
        End Try
    End Sub
    Function chk_security() As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoInsert('" & Session("emp_cd") & "','WGC_Transfer.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_RES").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Function

    Protected Sub bal_btn_Click(sender As Object, e As EventArgs) Handles bal_btn.Click

        Dim v_card_num As String = old_gc.Text

        If IsDBNull(old_gc.Text) = True Or isEmpty(old_gc.Text) = True Or old_gc.Text = "" Then
            xfer_msg.Text = "Please enter Old Gift Card number to proceed"
            Exit Sub
        End If

        Dim merchant_id As String = ""
        merchant_id = Lookup_Merchant("GC", Session("home_store_cd"), "GC")
        Dim myipadd As String = Session("clientip")
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim myret_string As String = ""
        Dim v_term_id As String = ""

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        Try
            If IsDBNull(v_term_id) Or isEmpty(v_term_id) Or v_term_id = "empty" Then
                xfer_msg.Text = "Invalid Termianl ID " & v_term_id
                Exit Sub
            End If
        Catch
            xfer_msg.Text = Err.Description
        End Try

        Dim mybal As Integer = 0
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        sycmd.Connection = conn
        sycmd.CommandText = "std_brk_pos1.sendsubTp4"
        sycmd.CommandType = CommandType.StoredProcedure

        'subtp4
        sycmd.Parameters.Add("p_req_tp", OracleType.VarChar).Value = "D3"  'Balance Inq.
        sycmd.Parameters.Add("p_crdnum", OracleType.VarChar).Value = v_card_num
        sycmd.Parameters.Add("p_crdtoken", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_pswd", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_amt", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_tran_no", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_term_id", OracleType.VarChar).Value = v_term_id
        sycmd.Parameters.Add("p_currency", OracleType.VarChar).Value = "124"
        sycmd.Parameters.Add("p_crdnum2", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_opid", OracleType.VarChar).Value = ""
        'end of subtp4

        sycmd.Parameters.Add("p_sessionid", OracleType.VarChar).Value = sessionid
        sycmd.Parameters.Add("p_ackind", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_authno", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_transtp", OracleType.VarChar, 100).Direction = ParameterDirection.Output

        sycmd.Parameters.Add("p_aut", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crn", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_exp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_res", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_rcp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        'gift card
        sycmd.Parameters.Add("p_nba", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bnb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bpb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crf", OracleType.VarChar, 150).Direction = ParameterDirection.Output

        sycmd.Parameters.Add("p_outfile", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

        Try
            syda.Fill(syds)

        Catch ex As Exception
            xfer_msg.Text = ex.Message.ToString()
            conn.Close()
            Throw
        End Try

        myret_string = sycmd.Parameters("p_res").Value.ToString
        xfer_msg.Text = "Available Balance to transfer is " & sycmd.Parameters("p_nba").Value.ToString & "  (" & sycmd.Parameters("p_rcp").Value.ToString & "-" & sycmd.Parameters("p_dsp").Value.ToString & ")"

        If IsDBNull(sycmd.Parameters("p_nba").Value) = False Then
            gc_bal.Text = sycmd.Parameters("p_nba").Value.ToString
        Else
            gc_bal.Text = "0"
        End If
        Session("gct_old_gc_bal") = gc_bal.Text

    End Sub

    Public Sub get_cust_info(ByVal p_cust_cd As String)

        Session("valid_cust_cd") = "N"

        If IsDBNull(cust_cd.Text) = True Or IsNothing(cust_cd.Text) = True Then
            xfer_msg.Text = "Enter Customer Code"
            Exit Sub
        End If

        Dim sql As String = ""
        Dim cmd As OracleCommand
        Dim reader As OracleDataReader
        Dim connLocal As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        connLocal.Open()

        sql = "select c.cust_cd cust_cd, c.fname||' '||c.lname cust_name,c.addr1||' '||c.addr2||' '||c.zip_cd cust_addr "
        sql = sql & " from cust c "
        sql = sql & " where c.cust_cd = '" & p_cust_cd & "'"

        cmd = DisposablesManager.BuildOracleCommand(sql, connLocal)
        Try
            'Execute DataReader 
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            reader.Read()
            cust_cd.Text = reader.Item("cust_cd").ToString
            cust_name.Text = reader.Item("cust_name").ToString
            cust_addr.Text = reader.Item("cust_addr").ToString

            Session("gct_cust_name") = reader.Item("cust_name").ToString
            Session("gct_cust_addr") = reader.Item("cust_addr").ToString

            Session("valid_cust_cd") = "Y"
        Catch ex As Exception
            connLocal.Close()
            'Throw
            xfer_msg.Text = "Invalid Cutomer Code"
        End Try
        connLocal.Close()


    End Sub

    Protected Sub commit_btn_Click(sender As Object, e As EventArgs) Handles commit_btn.Click

        If chk_security() & "" <> "Y" Then
            xfer_msg.Text = "You are NOT AUTHORIZED to use this function - please exit the screen"
            Exit Sub
        End If

        If IsNothing(old_gc.Text) = True Or old_gc.Text = "" Then
            xfer_msg.Text = "Enter Old Gift Card Number to proceed"
            Exit Sub
        End If

        If cust_cd.Text = "" Or IsNothing(cust_cd.Text) = True Then
            xfer_msg.Text = "Enter a valid Customer Code to proceed"
            Exit Sub
        End If

        If new_gc.Text = "" Or IsNothing(new_gc.Text) = True Then
            xfer_msg.Text = "Enter New Gift Card Number to Proceed"
            Exit Sub
        End If

        If gc_bal.Text = "" Or IsNothing(gc_bal.Text) = True Then
            xfer_msg.Text = "There is no Balance to transfer - Get Balance first"
            Exit Sub
        End If

        If IsNothing(xfer_comment.Text) = True Or xfer_comment.Text = "" Then
            xfer_msg.Text = "Enter Comments to proceed"
            Exit Sub
        End If

        Dim v_card_num As String = old_gc.Text

        If IsDBNull(old_gc.Text) = True Or isEmpty(old_gc.Text) = True Or old_gc.Text = "" Then
            xfer_msg.Text = "Please enter Old Gift Card number to proceed"
            Exit Sub
        End If

        Dim merchant_id As String = ""
        Session("gct_app_cd") = ""
        merchant_id = Lookup_Merchant("GC", Session("home_store_cd"), "GC")

        Dim myipadd As String = Session("clientip")
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim myret_string As String = ""
        Dim v_term_id As String = ""

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        Try
            If IsDBNull(v_term_id) Or isEmpty(v_term_id) Or v_term_id = "empty" Then
                xfer_msg.Text = "Invalid Terminal ID " & v_term_id
                Exit Sub
            End If
        Catch

        End Try


        Dim v_pmt_str As String
        Dim v_doc As String

        Dim pmtStore As String
        If Not Session("clientip") Is Nothing Then
            pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else ' if not go global then default to remote ip
            pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If

        If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
        Dim soKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(pmtStore, Session("tran_dt"))

        If Not (soKeyInfo Is Nothing OrElse soKeyInfo.soDocNum Is Nothing OrElse soKeyInfo.soDocNum.delDocNum Is Nothing) Then
            If Len(soKeyInfo.soDocNum.delDocNum) <> 11 Then
                xfer_msg.Text = "<font color=red>Could not create order, please try again</font>"
                Exit Sub
            End If
        End If

        v_doc = soKeyInfo.soDocNum.delDocNum

        Try
            If IsDBNull(v_term_id) Or isEmpty(v_term_id) Or v_term_id = "empty" Then
                xfer_msg.Text = "Invalid Termianl ID " & v_term_id
                Exit Sub
            End If
        Catch
            xfer_msg.Text = Err.Description
        End Try

        Dim mybal As Integer = 0
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        conn.Open()

        Dim sycmd As OracleCommand = DisposablesManager.BuildOracleCommand()
        Dim syda As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sycmd)
        Dim syds As New DataSet()

        sycmd.Connection = conn
        sycmd.CommandText = "std_brk_pos1.sendsubTp4"
        sycmd.CommandType = CommandType.StoredProcedure

        'subtp4
        sycmd.Parameters.Add("p_req_tp", OracleType.VarChar).Value = "E3"  'Balance transfer.
        sycmd.Parameters.Add("p_crdnum", OracleType.VarChar).Value = new_gc.Text
        sycmd.Parameters.Add("p_crdtoken", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_pswd", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_amt", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_tran_no", OracleType.VarChar).Value = ""
        sycmd.Parameters.Add("p_term_id", OracleType.VarChar).Value = v_term_id
        sycmd.Parameters.Add("p_currency", OracleType.VarChar).Value = "124"
        sycmd.Parameters.Add("p_crdnum2", OracleType.VarChar).Value = old_gc.Text
        sycmd.Parameters.Add("p_opid", OracleType.VarChar).Value = ""
        'end of subtp4

        sycmd.Parameters.Add("p_sessionid", OracleType.VarChar).Value = sessionid
        sycmd.Parameters.Add("p_cust_cd", OracleType.VarChar).Value = cust_cd.Text
        sycmd.Parameters.Add("p_doc_num", OracleType.VarChar).Value = v_doc

        sycmd.Parameters.Add("p_ackind", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_authno", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_transtp", OracleType.VarChar, 100).Direction = ParameterDirection.Output

        sycmd.Parameters.Add("p_aut", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crn", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_exp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_res", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_tr2", OracleType.VarChar, 500).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_rcp", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        'gift card
        sycmd.Parameters.Add("p_nba", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bnb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_bpb", OracleType.VarChar, 150).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_crf", OracleType.VarChar, 150).Direction = ParameterDirection.Output

        sycmd.Parameters.Add("p_outfile", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        sycmd.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

        Try
            syda.Fill(syds)

        Catch ex As Exception
            xfer_msg.Text = ex.Message.ToString()
            conn.Close()
            Throw
        End Try

        myret_string = sycmd.Parameters("p_res").Value.ToString

        'SABRINA r2019 check for REFU
        If InStr(UCase(sycmd.Parameters("p_rcp").Value.ToString), "DECLINE") > 0 Or InStr(UCase(sycmd.Parameters("p_rcp").Value.ToString), "REFU") > 0 Or sycmd.Parameters("p_aut").Value.ToString & "" = "" Or InStr(sycmd.Parameters("p_aut").Value.ToString, "?") > 0 Then
            xfer_msg.Text = "Balance transfer Failed - " & "  (" & sycmd.Parameters("p_rcp").Value.ToString & "-" & sycmd.Parameters("p_dsp").Value.ToString & ")"
            Exit Sub
        Else
            Session("gct_app_cd") = sycmd.Parameters("p_aut").Value.ToString
            Dim v_trn_tp_cd As String
            Dim v_mop_cd As String
            Dim v_des As String

            v_trn_tp_cd = "PMT"
            v_mop_cd = "GC"
            v_pmt_str = pmtStore
            v_des = ""
            gct_ar_trn(old_gc.Text, v_pmt_str, v_doc, v_trn_tp_cd, v_mop_cd, v_des)

            v_trn_tp_cd = "GCS"
            v_mop_cd = ""
            v_des = "Gift Card"
            gct_ar_trn(new_gc.Text, v_pmt_str, v_doc, v_trn_tp_cd, v_mop_cd, v_des)

            gct_print_rcpt(v_doc)

            xfer_msg.Text = "Balance transfer: " & sycmd.Parameters("p_nba").Value.ToString & "  (" & sycmd.Parameters("p_rcp").Value.ToString & "-" & sycmd.Parameters("p_dsp").Value.ToString & ")"
        End If


    End Sub

    Public Sub gct_ar_trn(ByVal p_gc_no As String, ByVal p_pmt_str As String, ByVal p_doc As String, ByVal p_trn_tp As String, ByVal p_mop_cd As String, ByVal p_des As String)
        Dim merchant_id As String = ""
        Dim sql As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdAddRecords As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim v_amt As OracleNumber
        Dim v_emp_init As String = ""
        Dim v_csh_dwr As String = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)

        conn.Open()

        Dim v_store As StoreData = theSalesBiz.GetStoreInfo(UCase(Session("HOME_STORE_CD").ToString))
        Session("CO_CD") = v_store.coCd

        v_emp_init = LeonsBiz.GetEmpInit(Session("emp_cd"))
        v_csh_dwr = LeonsBiz.get_csh_dwr(v_emp_init, Session("CO_CD"))

        sql = "insert into ar_trn (CO_CD,CUST_CD,MOP_CD,EMP_CD_CSHR,EMP_CD_OP,ORIGIN_STORE,CSH_DWR_CD,TRN_TP_CD,IVC_CD,"
        sql = sql & "BNK_CRD_NUM,AMT,POST_DT,STAT_CD,AR_TP,APP_CD,DES,PMT_STORE,ORIGIN_CD,CREATE_DT,AR_TRN_PK) "
        sql = sql & " values (:CO_CD,:CUST_CD,:MOP_CD,:EMP_CD_CSHR,:EMP_CD_OP,:ORIGIN_STORE,:CSH_DWR_CD,:TRN_TP_CD,:IVC_CD,"
        sql = sql & ":BNK_CRD_NUM,:AMT,TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'),"
        sql = sql & ":STAT_CD,:AR_TP,:APP_CD,:DES,:PMT_STORE,:ORIGIN_CD,TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'),"
        sql = sql & " seq_ar_trn.nextval)"

        cmdAddRecords.Parameters.Clear()

        Try
            If Not IsNothing(Session.SessionID.ToString.Trim) Then
                With cmdAddRecords
                    .Connection = conn
                    .CommandText = sql.ToString

                    cmdAddRecords.Parameters.Add(":CO_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CO_CD").Value = v_store.coCd

                    cmdAddRecords.Parameters.Add(":CUST_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CUST_CD").Value = cust_cd.Text

                    cmdAddRecords.Parameters.Add(":MOP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":MOP_CD").Value = p_mop_cd

                    cmdAddRecords.Parameters.Add(":EMP_CD_CSHR", OracleType.VarChar)
                    cmdAddRecords.Parameters(":EMP_CD_CSHR").Value = Session("emp_cd").ToString.Trim

                    cmdAddRecords.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)
                    cmdAddRecords.Parameters(":EMP_CD_OP").Value = Session("emp_cd").ToString.Trim

                    cmdAddRecords.Parameters.Add(":ORIGIN_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters(":ORIGIN_STORE").Value = Session("HOME_STORE_CD").ToString.Trim

                    cmdAddRecords.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CSH_DWR_CD").Value = v_csh_dwr

                    cmdAddRecords.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":TRN_TP_CD").Value = p_trn_tp

                    cmdAddRecords.Parameters.Add(":IVC_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":IVC_CD").Value = p_doc

                    cmdAddRecords.Parameters.Add(":BNK_CRD_NUM", OracleType.VarChar)
                    cmdAddRecords.Parameters(":BNK_CRD_NUM").Value = p_gc_no


                    If IsNumeric(gc_bal.Text) Then
                        v_amt = CDbl(gc_bal.Text)
                    Else
                        v_amt = 0
                    End If

                    cmdAddRecords.Parameters.Add(":AMT", OracleType.Number)
                    cmdAddRecords.Parameters(":AMT").Value = v_amt

                    cmdAddRecords.Parameters.Add(":STAT_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":STAT_CD").Value = "T"

                    cmdAddRecords.Parameters.Add(":AR_TP", OracleType.VarChar)
                    cmdAddRecords.Parameters(":AR_TP").Value = "O"

                    cmdAddRecords.Parameters.Add(":APP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":APP_CD").Value = Session("gct_app_cd")

                    cmdAddRecords.Parameters.Add(":DES", OracleType.VarChar)
                    If xfer_comment.Text = "" Then
                        cmdAddRecords.Parameters(":DES").Value = p_des
                    Else
                        cmdAddRecords.Parameters(":DES").Value = xfer_comment.Text
                    End If

                    cmdAddRecords.Parameters.Add(":PMT_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters(":PMT_STORE").Value = p_pmt_str

                    cmdAddRecords.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":ORIGIN_CD").Value = "PGC"

                End With
                cmdAddRecords.ExecuteNonQuery()

            End If
            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw ex
        Finally
            conn.Close()
        End Try




    End Sub

    Protected Sub xfer_comment_TextChanged(sender As Object, e As EventArgs) Handles xfer_comment.TextChanged
        Session("gct_xfer_comment") = xfer_comment.Text
    End Sub
    Protected Sub old_gc_TextChanged(sender As Object, e As EventArgs) Handles old_gc.TextChanged
        Session("gct_old_gc") = old_gc.Text
    End Sub

    Protected Sub cust_cd_TextChanged(sender As Object, e As EventArgs) Handles cust_cd.TextChanged
        Session("gct_cust_cd") = cust_cd.Text
        get_cust_info(cust_cd.Text)

    End Sub
    Public Sub gct_print_rcpt(ByVal p_doc As String)

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim v_ufm As String = "LPO"
        'Daniela french UFM
        Dim cult As String = UICulture
        If InStr(cult.ToLower, "fr") >= 1 Then
            v_ufm = "PRF"
        End If ' End Daniela
        Dim v_seq_num As String = ""

        Dim co_cd As String = Session("CO_CD")
        Dim grpCd As String = ""
        If isNotEmpty(co_cd) Then
            grpCd = LeonsBiz.GetGroupCode(co_cd)
            If grpCd = "BRK" Then
                If InStr(cult.ToLower, "fr") >= 1 Then
                    v_ufm = "BFC"
                Else
                    v_ufm = "BRC"
                End If
            End If
        End If

        v_seq_num = Right(p_doc, 4)

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "STD_TENDERRETAIL_UTIL.RECEIPT_PRINT"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_doc
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = Session("emp_cd")
        myCMD.Parameters.Add(New OracleParameter("p_doc_seq_num", OracleType.Char)).Value = v_seq_num
        myCMD.Parameters.Add(New OracleParameter("p_ufm", OracleType.Char)).Value = v_ufm
        Try
            myCMD.ExecuteNonQuery()

        Catch ex As Exception
            objConnection.Close()
            Throw ex
        End Try
    End Sub

    Private Sub CreateCustomer()

        'sabrina R1922 (new sub)
        If Session("cust_cd") = "NEW" Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String
            Dim fname As String = ""
            Dim lname As String = ""
            Dim addr1 As String = ""
            Dim addr2 As String = ""
            Dim city As String = ""
            Dim state As String = ""
            Dim zip As String = ""
            Dim hphone As String = ""
            Dim bphone As String = ""
            Dim cust_tp_cd As String = ""
            Dim email As String = ""

            sql = "SELECT * FROM CUST_INFO WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='" & Session("cust_cd") & "'"
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            Try
                dbConnection.Open()
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If (dbReader.Read()) Then
                    fname = dbReader.Item("BILL_FNAME").ToString.Trim
                    If fname & "" = "" Then fname = dbReader.Item("FNAME").ToString.Trim
                    lname = dbReader.Item("BILL_LNAME").ToString.Trim
                    If lname & "" = "" Then lname = dbReader.Item("LNAME").ToString.Trim
                    cust_tp_cd = dbReader.Item("CUST_TP").ToString.Trim
                    addr1 = dbReader.Item("BILL_ADDR1").ToString.Trim
                    If addr1 & "" = "" Then addr1 = dbReader.Item("ADDR1").ToString.Trim
                    addr2 = dbReader.Item("BILL_ADDR2").ToString.Trim
                    If addr2 & "" = "" Then addr2 = dbReader.Item("ADDR2").ToString.Trim
                    city = dbReader.Item("BILL_CITY").ToString.Trim
                    If city & "" = "" Then city = dbReader.Item("CITY").ToString.Trim
                    state = dbReader.Item("BILL_ST").ToString.Trim
                    If state & "" = "" Then state = dbReader.Item("ST").ToString.Trim
                    zip = dbReader.Item("BILL_ZIP").ToString.Trim
                    If zip & "" = "" Then zip = dbReader.Item("ZIP").ToString.Trim
                    hphone = StringUtils.FormatPhoneNumber(dbReader.Item("HPHONE").ToString.Trim)
                    bphone = StringUtils.FormatPhoneNumber(dbReader.Item("BPHONE").ToString.Trim)
                    email = dbReader.Item("EMAIL").ToString.Trim
                End If
                dbReader.Close()
                dbConnection.Close()
            Catch ex As Exception
                dbConnection.Close()
                Throw
            End Try

            ' All update/insert/delete operations made in the "dbAtomicCommand" object will be part of 
            ' a single transaction to be able to rollback all changes if something fails
            Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

            Dim new_cust_fname As String = fname
            new_cust_fname = Replace(new_cust_fname, "'", "")
            new_cust_fname = Replace(new_cust_fname, """", "")
            new_cust_fname = Replace(new_cust_fname, "%", "")
            Dim new_cust_lname As String = lname
            new_cust_lname = Replace(new_cust_lname, "'", "")
            new_cust_lname = Replace(new_cust_lname, """", "")
            new_cust_lname = Replace(new_cust_lname, "%", "")

            Session("CUST_CD") = CustomerUtils.Create_CUSTOMER_CODE("", Session("store_cd"),
                                                                 UCase(addr1),
                                                                 Left(UCase(new_cust_lname), 20),
                                                                 Left(UCase(new_cust_fname), 15))
            Try
                dbAtomicConnection.Open()
                dbAtomicCommand.Transaction = dbAtomicConnection.BeginTransaction()

                ' --- Save Customer to the E1 Database
                sql = "INSERT INTO CUST (CUST_CD, ACCT_OPN_DT, FNAME, LNAME, CUST_TP_CD, ADDR1, ADDR2, CITY, ST_CD, ZIP_CD, HOME_PHONE, BUS_PHONE, EMAIL_ADDR) "
                sql = sql & "VALUES(:CUST_CD, :ACCT_OPN_DT, :FNAME, :LNAME, :CUST_TP, :ADDR1, :ADDR2, :CITY, :ST, :ZIP, :HPHONE, :BPHONE, :EMAIL) "

                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.Parameters.Add(":CUST_CD", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_CD").Value = Session("CUST_CD").ToString.Trim
                dbAtomicCommand.Parameters.Add(":ACCT_OPN_DT", OracleType.DateTime)
                dbAtomicCommand.Parameters(":ACCT_OPN_DT").Value = FormatDateTime(Now, DateFormat.ShortDate)
                dbAtomicCommand.Parameters.Add(":FNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":FNAME").Value = UCase(fname)
                dbAtomicCommand.Parameters.Add(":LNAME", OracleType.VarChar)
                dbAtomicCommand.Parameters(":LNAME").Value = UCase(lname)
                dbAtomicCommand.Parameters.Add(":CUST_TP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CUST_TP").Value = cust_tp_cd
                dbAtomicCommand.Parameters.Add(":ADDR1", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR1").Value = UCase(addr1)
                dbAtomicCommand.Parameters.Add(":ADDR2", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ADDR2").Value = UCase(addr2)
                dbAtomicCommand.Parameters.Add(":CITY", OracleType.VarChar)
                dbAtomicCommand.Parameters(":CITY").Value = UCase(city)
                dbAtomicCommand.Parameters.Add(":ST", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ST").Value = UCase(state)
                dbAtomicCommand.Parameters.Add(":ZIP", OracleType.VarChar)
                dbAtomicCommand.Parameters(":ZIP").Value = zip
                dbAtomicCommand.Parameters.Add(":HPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":HPHONE").Value = hphone
                dbAtomicCommand.Parameters.Add(":BPHONE", OracleType.VarChar)
                dbAtomicCommand.Parameters(":BPHONE").Value = bphone
                dbAtomicCommand.Parameters.Add(":EMAIL", OracleType.VarChar)
                dbAtomicCommand.Parameters(":EMAIL").Value = UCase(email)
                dbAtomicCommand.ExecuteNonQuery()
                dbAtomicCommand.Parameters.Clear()  'to get it ready for the next statement

                ' --- Update newly created Customer Code to the table in the CRM schema 
                sql = "UPDATE CUST_INFO SET CUST_CD='" & Session("CUST_CD") & "' WHERE Session_ID='" & Session.SessionID.ToString.Trim & "' AND CUST_CD='NEW'"
                dbAtomicCommand.CommandText = sql
                dbAtomicCommand.ExecuteNonQuery()

                'saves all the pending changes.
                dbAtomicCommand.Transaction.Commit()

            Catch ex As Exception
                dbAtomicCommand.Transaction.Rollback()  'reverts any changes made so far
                Throw
            Finally
                dbAtomicCommand.Dispose()
                dbAtomicConnection.Close()
            End Try
        End If
    End Sub

End Class

