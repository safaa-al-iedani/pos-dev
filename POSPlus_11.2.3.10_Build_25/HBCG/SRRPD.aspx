﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SRRPD.aspx.vb"
    MasterPageFile="MasterPages/NoWizard2.master"  Inherits="SRRPD" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" BorderStyle="none" runat="Server">
        <div runat="server">
        
            <br />
        
            <br /> 
           <table width="100%">
            <tr>
              <asp:TextBox ID="lbl_msg" BorderColor="Brown"  BackColor="WhiteSmoke" forecolor="Blue" font-size="Small" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True" ></asp:TextBox>
            </tr>
           </table>
             
             <%-- ===========CREDIT TP, VCM sort, store, status ========OnSelectedIndexChanged="STRSelected"===============================   --%> 
            <table width="100%">
            <tr>
               <td align="right"> <asp:Label ID="lbl_str" runat="server"    font-size="XX-Small" Text="Store Group Code "></asp:Label> </td>
                         <td>
                    <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" 
                    width="260px" autopostback="true"  ID="srch_store_grp_list">                   
                    </asp:dropdownlist> 
                </td>  
                </tr>
                 <tr> </tr>
                <tr> </tr>
                   
               <td align="right"> <asp:Label ID="Label3" runat="server"    font-size="XX-Small" Text="Sales Type "></asp:Label> </td>
                         <td>
                    <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" 
                    width="260px" autopostback="true"  ID="srch_sls_tp">                   
                    </asp:dropdownlist> 
                </td>  
                </tr>
                <tr> </tr>
                <tr> </tr>
                     <tr>
                          
                <td align="right">                                               
                <asp:Label ID="Label2" runat="server" font-size="XX-Small" Text="Written/Delivered Date "></asp:Label>  </td>
                         <td>
                <asp:TextBox ID="srch_dt" ReadOnly="true" Width="100px" runat="server"></asp:TextBox>
                <asp:Button ID="dt1_btn" runat="server" Text="Date" OnClick="srch_dt1_btn" />
                <asp:Button ID="dt1x_btn" runat="server" Text="Clear" OnClick="srch_dt1x_btn" />
                <asp:Calendar Visible="false" font-size="Xx-Small" hieght="1px" Width="1px" ID="clndr_dt" 
                 DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server" OnSelectionChanged="srch_dt1_changed" >
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                 <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
              </asp:Calendar>
            </td>              
            </tr>
            </table>
            <br />
            <br />
            <br />
                    <br />
        <%-- ========================== BUTTONS OnClick="search_btn_click" =============================   --%> 
           
          <table width="100%">
            <tr> 
            <td align="center"> <asp:Button ID="summary_btn" Width="150" font-bold="true" ForeColor="Red" runat="server" Text="Search" />
               &nbsp&nbsp&nbsp&nbsp
                 <asp:Button ID="clear_btn" font-bold="true" Width="150" ForeColor="Red" runat="server" Text="Clear" /></td>
            
            </tr> 
                
           </table>
     <%-- ==================== TOTAL =====Height="80px"= Width="750px"=======================   --%> 
          <dxpc:ASPxPopupControl ID="ASPxPopupSummary" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="SALES SUMMARY" Height="90px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="95%"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl" DefaultButton="btnSubmit" runat="server">
             <%--  --%> 
          <%--<asp:Label ID="Label9" runat="server" Text="Store Group "></asp:Label>
          <asp:TextBox ID="popup_store_grp_cd" Enabled="false" ReadOnly="true" runat="server"></asp:TextBox>
          <asp:Label ID="Label1" runat="server" Text="Sales Type "></asp:Label>
          <asp:TextBox ID="popup_sls_tp" Enabled="false" ReadOnly="true" runat="server"></asp:TextBox>
          <asp:Label ID="Label4" runat="server" Text="Sales Date "></asp:Label>
          <asp:TextBox ID="popup_dt" Enabled="false" ReadOnly="true" runat="server"></asp:TextBox> --%>
          <br />
          <br />
          <asp:Button ID="detail_btn" runat="server" Font-Bold="true" ForeColor="red" Text="Display Detail" />
          <br />
          <br />
          <dx:ASPxGridView ID="GridView1" runat="server" keyfieldname="STORE_CD" AutoGenerateColumns="False" 
          Width="98%" ClientInstanceName="GridView1">
                  <SettingsCommandButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewDataTextColumn Caption="Store" FieldName="STORE_CD" ReadOnly="true" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  Width="100" MaxLength="10">
                      </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Total Sales" FieldName="TOT_SLS" ReadOnly="true" CellStyle-BackColor="WhiteSmoke" VisibleIndex="2">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" Width="300" MaxLength="30">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Furniture" FieldName="FURN_SLS" ReadOnly="true" VisibleIndex="3">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Appliances" FieldName="APPL_SLS" ReadOnly="true" VisibleIndex="4">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Electronics" FieldName="ELEC_SLS" ReadOnly="true" VisibleIndex="5">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Mattress" FieldName="MATT_SLS" ReadOnly="true" VisibleIndex="6">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Misc." FieldName="MISC_SLS" ReadOnly="true" VisibleIndex="7">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

        </Columns>

        <SettingsBehavior  AllowSort="False" />
        <SettingsPager mode="ShowAllRecords" />
        <%--<SettingsPager Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />--%>
        <%--<SettingsBehavior ConfirmDelete="true"    />
        <SettingsText  ConfirmDelete="Please press 'UPDATE' to confirm and Delete this record" />--%>
    </dx:ASPxGridView>
    <%--  --%>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
        
  <%-- =================== DETAIL ===============================   --%> 
          <dxpc:ASPxPopupControl ID="ASPxPopupDetail" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="SALES DETAIL BY STORE" Height="90px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="95%"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" DefaultButton="btnSubmit" runat="server">
          <%--  --%> 
         
          <br />
          <br />
          <dx:ASPxGridView ID="GridView2" runat="server" keyfieldname="STORE_CD" AutoGenerateColumns="False" 
          Width="98%" ClientInstanceName="GridView2">
                  <SettingsCommandButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewDataTextColumn Caption="Store" FieldName="STORE_CD" ReadOnly="true" Visible="True" VisibleIndex="1">
                <PropertiesTextEdit  Width="100" MaxLength="10">
                      </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Total Sales" EditFormCaptionStyle-Font-Bold="true" FieldName="TOT_SLS" ReadOnly="true" CellStyle-BackColor="WhiteSmoke" VisibleIndex="2">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" Width="300" MaxLength="30">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Furniture" FieldName="FURN_SLS" ReadOnly="true" VisibleIndex="3">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Appliances" FieldName="APPL_SLS" ReadOnly="true" VisibleIndex="4">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Electronics" FieldName="ELEC_SLS" ReadOnly="true" VisibleIndex="5">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Mattress" FieldName="MATT_SLS" ReadOnly="true" VisibleIndex="6">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Misc." FieldName="MISC_SLS" ReadOnly="true" VisibleIndex="7">
                <PropertiesTextEdit ReadOnlyStyle-BackColor="Gray" MaxLength="28">
                                   </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

        </Columns>

        <SettingsBehavior  AllowSort="False" />
              <SettingsPager mode="ShowAllRecords" />
        <%--<SettingsPager Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />--%>
        <%--<SettingsBehavior ConfirmDelete="true"    />
        <SettingsText  ConfirmDelete="Please press 'UPDATE' to confirm and Delete this record" />--%>
    </dx:ASPxGridView>
    <%--  --%>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
    
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
            
    
