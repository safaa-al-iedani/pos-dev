<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" 
    CodeFile="AeroplanMPC.aspx.vb" Inherits="AeroplanMPC" meta:resourcekey="PageResource1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div id="Div1" runat="server">   
     <%-- =============== HEADING ==========================================   --%> 
    <table  width="100%" class="style5">
      <tr>
      <td align="center" > 
      <asp:Label ID="Label1" runat="server" Text="Aeroplan Customer MPC Maintenance" Font-Bold="True" ></asp:Label>
      </td>
      </tr>
     </table>
       <br />
      <%-- ===============MESSAGES =========lbl_msg ==========================================   --%> 
     <table>
         <tr>
             <td>
     <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" Font-Size="small" ForeColor="Blue" BackColor="White" runat="server"
        Text="" Width="100%"> 
     </dx:ASPxLabel>
            </td>
            </tr>
     </table>
    <br />
     <%-- ============================================   --%> 
    <asp:Panel ID="Panel5" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >
        <table cellspacing="2" width="100%">
          <%-- CUST_CD --%>
             <tr>
                <td> <dx:ASPxLabel ID="cust_cd_lbl" text="Customer Code" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
                <td> <asp:TextBox ID="srch_cust_cd" style='text-transform:uppercase' runat="server" OnTextChanged="CustChanged" AutoPostBack="true"></asp:TextBox> 
                <asp:TextBox ID="srch_cust_name" enabled="false" borderstyle="None" runat="server" OnTextChanged="CustChanged" AutoPostBack="true"></asp:TextBox> </td>
                <td> <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Customer.aspx"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:LibResources, Label88 %>" /></asp:HyperLink> </td>
             </tr>
          <%-- store  --%>
             <tr>
             <td> <dx:ASPxLabel ID="ASPxLabel2" Text="Store Code" Font-Bold="true" runat="server" > </dx:ASPxLabel> </td>
             <td> 
             <asp:dropdownlist runat="server" AppendDataBoundItems="true" width="400px" EnableViewState="true" ID="srch_str_cd">                   
             </asp:dropdownlist> 
             </td>
             <%--<td> <asp:TextBox ID="srch_str_name" Font-Size="XX-Small" Enabled="false" BorderStyle="None" width="300px" runat="server"></asp:TextBox> </td>--%>
          </tr>
      </table>
      <table>
        <%-- AEROPLAN# --%>
          <tr>
             <td> <dx:ASPxLabel ID="ASPxLabel1" Text="Aeroplan Number" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
             <td> <asp:TextBox ID="srch_aeroplan_crd_num" runat="server"></asp:TextBox> </td>
            <%-- CRE_by --%> 
              <td style="text-align:right"> <dx:ASPxLabel ID="ASPxLabel4" Text="Entered By"  Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
              <td> <asp:TextBox ID="srch_cre_by" Width="100px" runat="server" style='text-transform:uppercase' OnTextChanged="UserChanged" AutoPostBack="true" ></asp:TextBox> 
             <asp:TextBox ID="srch_cre_name" Font-Size="XX-Small"  BorderStyle="None" enabled="false" runat="server"></asp:TextBox> </td>
          </tr>
          <%-- contract no.  --%>
          <tr>
           <td> <dx:ASPxLabel ID="ASPxLabel3" Text="Contract No" Font-Bold="true" runat="server" > </dx:ASPxLabel> </td>
           <td> <asp:TextBox ID="srch_contract" runat="server"></asp:TextBox> </td>
           <td style="text-align:right"> <dx:ASPxLabel ID="ASPxLabel5" Text="MPC Code" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
           <td> <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" 
                Width  ="300px" ID="srch_mpc_code">                   
                </asp:dropdownlist>   </td>
          <%-- <td> <asp:TextBox ID="srch_mpc_name" Font-Size="XX-Small" BorderStyle="None" enabled="false" runat="server"></asp:TextBox> </td>--%>
        </tr>
      
        <tr>
           <td>
                <dx:ASPxLabel ID="ASPxLabel6" Text="Enter DT From" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
               </td>
            <td>
              <asp:TextBox ID="srch_purch_dt_from" Enabled="false" Width="100px" runat="server"></asp:TextBox>
              <asp:Button ID="dt1_btn" runat="server" Text="Date" OnClick="srch_dt1_btn" />
               <asp:Button ID="dt1x_btn" runat="server" Text="Clear" OnClick="srch_dt1x_btn" />
                <asp:Calendar Visible="false" font-size="Xx-Small" hieght="1px" Width="1px" ID="clndr_purch_dt_from" 
                 DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server" OnSelectionChanged="srch_dt1_changed" >
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                 <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
              </asp:Calendar>
            </td>
            <%-- Entered dt to  --%>
             <td style="text-align:right">
             <dx:ASPxLabel ID="ASPxLabel7" Text=" TO " Font-Bold="true" runat="server" meta:resourcekey="Label1">
             </dx:ASPxLabel>
             </td>
             <td>
             <asp:TextBox ID="srch_purch_dt_to" Enabled="false" Width="100px" runat="server"></asp:TextBox>
                 <asp:Button ID="dt2_btn" runat="server" Text="Date" OnClick="srch_dt2_btn" />
                 <asp:Button ID="dt2x_btn" runat="server" Text="Clear" OnClick="srch_dt2x_btn" />
              <asp:Calendar visible="false" font-size="Xx-Small" hieght="1px" Width="1px" ID="clndr_purch_dt_to"
                 DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server" OnSelectionChanged="srch_dt2_changed" >
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                 <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
              </asp:Calendar>
             </td>
        </tr>
        <tr>
           <td>
             <%-- Process dt from  --%>
                <dx:ASPxLabel ID="ASPxLabel8" Text="Process DT From" Font-Bold="true" runat="server" >
                </dx:ASPxLabel>
             </td>
              <td>
              <asp:TextBox ID="srch_process_dt_from" enabled="false" Width="100px" runat="server"></asp:TextBox>
                  <asp:Button ID="dt3_btn" runat="server" Text="Date" OnClick="srch_dt3_btn" />
                   <asp:Button ID="dt3x_btn" runat="server" Text="Clear" OnClick="srch_dt3x_btn" />
              <asp:Calendar visible="false" font-size="Xx-Small" hieght="1px" Width="1px" ID="clndr_process_dt_from"
                 DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server" OnSelectionChanged="srch_dt3_changed" >
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                 <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
              </asp:Calendar>
            </td>
            <%-- process dt to  --%>
             <td style="text-align:right">
             <dx:ASPxLabel ID="ASPxLabel9" Text=" TO " Font-Bold="true" runat="server" meta:resourcekey="Label1">
             </dx:ASPxLabel>
             </td>
             <td>
             <asp:TextBox ID="srch_process_dt_to" enabled="false" Width="100px" runat="server"></asp:TextBox>
                 <asp:Button ID="dt4_btn" runat="server" Text="Date" OnClick="srch_dt4_btn" />
                  <asp:Button ID="dt4x_btn" runat="server" Text="Clear" OnClick="srch_dt4x_btn" />
              <asp:Calendar visible="false" font-size="Xx-Small" hieght="1px" Width="1px" ID="clndr_process_dt_to"
                 DisplayDateFormat="MM/dd/yyyy" ForeColor="Black" runat="server" OnSelectionChanged="srch_dt4_changed" >
                 <SelectedDayStyle ForeColor="Red" BackColor="White"></SelectedDayStyle>
                 <OtherMonthDayStyle ForeColor="GrayText"></OtherMonthDayStyle>
              </asp:Calendar>
             </td>
        </tr>
            <%-- SEARCH BUTTON --%>
            <tr>
                <td style="width: 60px;">
                <dx:ASPxButton ID="btn_search" runat="server" Font-Bold="true" Text="Search">
                </dx:ASPxButton>
           </td>     
         
         <%-- CLEAR BUTTON --%>
           <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Font-Bold="true" Text="<%$ Resources:LibResources, Label888 %>">
                </dx:ASPxButton>
           </td> 
        <%-- add BUTTON --%>
                            <td style="width: 30px;">
                <dx:ASPxButton ID="btn_new" runat="server" Font-Bold="true" Text="New">
                </dx:ASPxButton>
           </td>       
        </tr>
       </table>
       </asp:Panel>
    <br />
    <br />
    <%-- -------------------------------------------------- ----------------------------------------------------- --%>
    <%-- -------------------------------------------------- EDIT grid-------------------------------------------- --%>
    <%-- -------------------------------------------------- ----------------------------------------------------- --%>
    
     <%-- ===========================================================================   --%>  
          <dxpc:ASPxPopupControl ID="ASPxPopupCustMPC1" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Customer MPC Detail" Height="80px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="750px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" DefaultButton="btnSubmit" runat="server">
        
          <asp:TextBox ID="lbl_msg2" BorderColor="Brown"  BackColor="WhiteSmoke" forecolor="Blue" font-size="Small" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True"></asp:TextBox>
          <br />
          <br />
    <%-- ===== --%>
    <asp:Panel ID="Panel3" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
   
    <%-- gridview --%>
       <dx:ASPxGridView ID="GridView1" Settings-ShowFooter="true" runat="server" KeyFieldName="CUST_CD;AEROPLAN_CRD_NUM;STORE_CD;AEROPLAN_PURCH_DT;MPC_CODE;QUALIFY_PTS;CUSTMPC_ID" 
           AutoGenerateColumns="False" OnCommandButtonInitialize="GridView1_CommandButtonInitialize" Width="98%" ClientInstanceName="GridView1" OnInitNewRow="GridView1_InitNewRow" oncelleditorinitialize="GridView1_CellEditorInitialize" OnRowInserting="ASPxGridView1_RowInserting" OnRowDeleting="ASPxGridView1_RowDeleting" OnRowUpdating="ASPxGridView1_RowUpdating" 
           OnRowValidating="ASPxGridView1_RowValidating">
        <SettingsEditing mode="EditForm" />
        <SettingsCommandButton> 
         <UpdateButton Text="<%$ Resources:LibResources, Label885 %>"></UpdateButton>
         <CancelButton Text="<%$ Resources:LibResources, Label886 %>"></CancelButton>
        </SettingsCommandButton>
          
       <%-- ---------------- COLUMNS ------------------ --%>
        <Columns>
          <dx:GridViewDataTextColumn Caption="Customer" FieldName="CUST_CD" VisibleIndex="2">
                <PropertiesTextEdit Width="100" MaxLength="10">
                    <ValidationSettings>
                      <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Name" FieldName="CUST_NAME" VisibleIndex="3">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" MaxLength="30">
                </PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Aeroplan#" FieldName="AEROPLAN_CRD_NUM" VisibleIndex="4">
                <PropertiesTextEdit Width="90"  MaxLength="9">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right" >
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ValidationExpression="\d+" ErrorText="Invalid Value" />
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Store CD" FieldName ="STORE_CD" VisibleIndex="5">
               <PropertiesComboBox Width="360" DropDownStyle="DropDown" MaxLength="46">
                   <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right">
                    </ValidationSettings>
               </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
         <dx:GridViewDatadateColumn Caption="Purchase DT" FieldName="AEROPLAN_PURCH_DT" VisibleIndex="6">
              <PropertiesdateEdit DisplayFormatString="MM/dd/yyyy" ReadOnlyStyle-BackColor="Snow">
              </PropertiesdateEdit> 
         </dx:GridViewDatadateColumn>
         <dx:GridViewDataTextColumn Caption="Enter By" FieldName="CRE_BY" VisibleIndex="7">
               <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" MaxLength="12">
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn> 
        <dx:GridViewDataComboBoxColumn Caption="MPC CD" FieldName ="MPC_CODE" VisibleIndex="8">
               <PropertiesComboBox Width="360" DropDownStyle="DropDown" MaxLength="46">
                   <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right">
                    </ValidationSettings>
               </PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataTextColumn Caption="Miles" FieldName="QUALIFY_PTS" VisibleIndex="9">
            <PropertiesTextEdit Width="60" MaxLength="6">
              <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right" >
                  <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                  <RegularExpression ValidationExpression="^-?\d*\.{0,1}\d+$" ErrorText="Invalid Value" />
                   <ErrorFrameStyle ForeColor="red"/>
              </ValidationSettings>
           </PropertiesTextEdit> 
         </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Contract" FieldName="CONTRACT" VisibleIndex="10">
               <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" MaxLength="12">
                </PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Status" FieldName="STAT_CD" VisibleIndex="11">
             <PropertiesTextEdit Width="20"  MaxLength="1">
                <ValidationSettings>
                <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                </ValidationSettings>
             </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
            <%-- <dx:GridViewDataTextColumn Caption="Process CD" FieldName="PROCESSING_CD" VisibleIndex="12">
           <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" MaxLength="13">
           </PropertiesTextEdit>
     </dx:GridViewDataTextColumn>--%>
            <dx:GridViewDataTextColumn Caption="Comment" FieldName="CMNT" VisibleIndex="12">
             <PropertiesTextEdit Width="500"  MaxLength="60">
                <ValidationSettings>
                <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                </ValidationSettings>
             </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Process DT" FieldName="PROCESSING_DT" VisibleIndex="13">
           <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" DisplayFormatString="MM/dd/yyyy"  MaxLength="13">
           </PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="" FieldName="CUSTMPC_ID" ReadOnly="true" Visible="false" VisibleIndex="14">
             <PropertiesTextEdit MaxLength="10"></PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         <dx:GridViewCommandColumn ShowEditButton="true" ShowNewButtonInHeader="true" ShowDeleteButton="true" VisibleIndex="19"/>
         </Columns>
        
        <SettingsBehavior AllowSort="False"/>
        <SettingsPager Position="Bottom" Visible="true" Mode="ShowPager" PageSize="10"/>
           
        <SettingsBehavior ConfirmDelete="true"   />
        <SettingsText  ConfirmDelete="Please press 'OK' to confirm and Delete this record" />
    
    </dx:ASPxGridView>
   
    </asp:Panel>

    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
<%-- -------------------------------------------------- ----------------------------------------------------- --%>
<%-- -------------------------------------  BROWSE grid ----------------------------------------------------- --%>
<%-- -------------------------------------------------- ----------------------------------------------------- --%>
    <%-- ===========================================================================   --%>  
 <dxpc:ASPxPopupControl ID="ASPxPopupCustMPC2" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Customer MPC Detail" Height="80px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="750px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" DefaultButton="btnSubmit" runat="server">
        
         <br />
         <br />
     <%-- gridview --%>
<%-- ===== --%>
    <asp:Panel ID="Panel1" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
         <dx:ASPxGridView ID="GridView9" Settings-ShowFooter="true" runat="server" 
             KeyFieldName="CUST_CD;AEROPLAN_CRD_NUM;STORE_CD;AEROPLAN_PURCH_DT;MPC_CODE;QUALIFY_PTS;CUSTMPC_ID" AutoGenerateColumns="False" 
            Width="98%"  ClientInstanceName="GridView9">
         <Columns>
          <dx:GridViewDataTextColumn Caption="Customer" FieldName="CUST_CD" VisibleIndex="2">
                <PropertiesTextEdit Width="100" ReadOnlyStyle-BackColor="Snow" MaxLength="10"></PropertiesTextEdit>
           <CellStyle Wrap="False" />
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Name" FieldName="CUST_NAME" VisibleIndex="3">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow"  MaxLength="30"></PropertiesTextEdit>
                <CellStyle Wrap="False" />
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Aeroplan#" FieldName="AEROPLAN_CRD_NUM" VisibleIndex="4">
                <PropertiesTextEdit Width="90" ReadOnlyStyle-BackColor="Snow"  MaxLength="9"> </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Store CD" FieldName="STORE_CD" VisibleIndex="5">
                <PropertiesTextEdit Width="60" ReadOnlyStyle-BackColor="Snow" MaxLength="6"> </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDatadateColumn Caption="Purchase DT" FieldName="AEROPLAN_PURCH_DT" VisibleIndex="6">
              <PropertiesdateEdit DisplayFormatString="MM/dd/yyyy" ReadOnlyStyle-BackColor="Snow"> </PropertiesdateEdit> 
         </dx:GridViewDatadateColumn>
         <dx:GridViewDataTextColumn Caption="Enter By"  FieldName="CRE_BY" VisibleIndex="7">
               <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" MaxLength="12"> </PropertiesTextEdit>
          </dx:GridViewDataTextColumn> 
          <dx:GridViewDataTextColumn Caption="MPC CD" FieldName="MPC_CODE" VisibleIndex="8">
           <PropertiesTextEdit Width="60" ReadOnlyStyle-BackColor="Snow"  MaxLength="6"> </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Miles" FieldName="QUALIFY_PTS" VisibleIndex="9">
            <PropertiesTextEdit Width="60" ReadOnlyStyle-BackColor="Snow"  MaxLength="6"> </PropertiesTextEdit> 
         </dx:GridViewDataTextColumn>
          <dx:GridViewDataTextColumn Caption="Contract"  FieldName="CONTRACT" VisibleIndex="10">
               <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" MaxLength="12">
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Status" FieldName="STAT_CD" VisibleIndex="11">
                <PropertiesTextEdit Width="20" ReadOnlyStyle-BackColor="Snow" MaxLength="1"> </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn Caption="Comment" FieldName="CMNT" VisibleIndex="12">
                <PropertiesTextEdit Width="20" ReadOnlyStyle-BackColor="Snow" MaxLength="1"> </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="Process DT" FieldName="PROCESSING_DT" VisibleIndex="13">
              <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" DisplayFormatString="MM/dd/yyyy"  MaxLength="13"> </PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="" FieldName="CUSTMPC_ID" ReadOnly="true" Visible="false" VisibleIndex="14">
                       </dx:GridViewDataTextColumn>
        <%-- <dx:GridViewCommandColumn ShowEditButton="FALSE"  VisibleIndex="19"/>--%>
        </Columns>
        
        <SettingsBehavior  AllowSort="False" />
        <SettingsPager  Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />
      
    </dx:ASPxGridView>
    </asp:Panel>

</dxpc:PopupControlContentControl>
</ContentCollection>
</dxpc:ASPxPopupControl>

 
    <br />
   </div>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
