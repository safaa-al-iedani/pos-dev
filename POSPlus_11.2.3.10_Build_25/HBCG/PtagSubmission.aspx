<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="PtagSubmission.aspx.vb" Inherits="PtagSubmission" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function multiitempopupclose() {
            popup_multi_item.Hide();
        }
        function clear_label() {
            document.getElementById("<%= lbl_ps_message.ClientID()%>").innerHTML = "";
        }
    </script>

    <table>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_report_type" runat="server" meta:resourcekey="lbl_report_type"></dx:ASPxLabel>
            </td>
            <td colspan="3">
                <dx:ASPxComboBox ID="cbo_report_type" runat="server" ValueType="System.String" DropDownStyle="DropDownList" Width="90px">
                    <Items>
                        <dx:ListEditItem Text="ALL" Value="" Selected="true" />
                        <dx:ListEditItem Text="Update RFL" Value="RFL" />
                        <dx:ListEditItem Text="Update HPT" Value="HPT" />
                        <dx:ListEditItem Text="Update TRK" Value="TRK" />
                        <dx:ListEditItem Text="Update SMT" Value="SMT" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_fm_loc" runat="server" meta:resourcekey="lbl_fm_loc"></dx:ASPxLabel>
            </td>
            <td colspan="3" align="left">
                <dx:ASPxCheckBox ID="cbx_fm_loc" runat="server"></dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_promo_cd" runat="server" meta:resourcekey="lbl_promo_cd"></dx:ASPxLabel>
            </td>
            <td colspan="3" align="left">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <dx:ASPxTextBox ID="txt_promo_cd" runat="server" Width="200" MaxLength="3" ClientIDMode="static">
                                <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                            </dx:ASPxTextBox>
                        </td>
                        <td align="center" style="width: 110px">
                            <dx:ASPxLabel ID="lbl_promo_end_dt" runat="server" meta:resourcekey="lbl_promo_end_dt"></dx:ASPxLabel>
                        </td>
                        <td></td>
                        <td align="left">
                            <dx:ASPxDateEdit ID="dt_promo_end_dt" runat="server" Width="100px" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"
                                AutoPostBack="True">
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_prod_grp_range" runat="server" meta:resourcekey="lbl_prod_grp_range"></dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="txt_prod_grp_from" runat="server" Width="200">
                    <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                </dx:ASPxTextBox>
            </td>
            <td align="center">
                <dx:ASPxLabel ID="lbl_to2" runat="server" meta:resourcekey="lbl_to"></dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="txt_prod_grp_to" runat="server" Width="200">
                    <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_mjr_code_range" runat="server" meta:resourcekey="lbl_mjr_code_range"></dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="txt_mjr_code_from" runat="server" Width="200">
                    <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                </dx:ASPxTextBox>
            </td>
            <td align="center">
                <dx:ASPxLabel ID="lbl_to3" runat="server" meta:resourcekey="lbl_to"></dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="txt_mjr_code_to" runat="server" Width="200">
                    <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_mnr_code_range" runat="server" meta:resourcekey="lbl_mnr_code_range"></dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="txt_mnr_code_from" runat="server" Width="200">
                    <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                </dx:ASPxTextBox>
            </td>
            <td align="center">
                <dx:ASPxLabel ID="lbl_to4" runat="server" meta:resourcekey="lbl_to"></dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="txt_mnr_code_to" runat="server" Width="200">
                    <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_multi_itm_cd" runat="server" meta:resourcekey="lbl_multi_itm_cd"></dx:ASPxLabel>
            </td>
            <td colspan="2">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <dx:ASPxCheckBox ID="cbx_multi_itm_cd" runat="server" CheckState="Unchecked" AutoPostBack="true"></dx:ASPxCheckBox>
                        </td>
                        <td colspan="2">
                            <dx:ASPxButton ID="btn_multi_item" runat="server" meta:resourcekey="btn_multiple_item" AutoPostBack="true"></dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
            <td colspan="1">
                 <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="lbl_location_cd" runat="server" meta:resourcekey="lbl_location_cd" Width="100"> </dx:ASPxLabel>
                        </td>
                         <td>  
                            <dx:ASPxTextBox ID="txt_location_cd" runat="server" Width="100" >
                                  <ClientSideEvents KeyUp="function(s,e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_itm_cd" runat="server" meta:resourcekey="lbl_itm_cd"></dx:ASPxLabel>
            </td>
            <td colspan="3">
                <dx:ASPxTextBox ID="txt_itm_cd" runat="server" Width="200">
                    <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td align="right"  >
                <dx:ASPxLabel ID="lbl_print_for_head" runat="server" meta:resourcekey="lbl_print_for_head"></dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_store" runat="server" meta:resourcekey="lbl_store"></dx:ASPxLabel>
            </td>

            <td colspan="3">
                <dx:ASPxComboBox ID="cbo_store_cd" runat="server"
                    Width="350px" IncrementalFilteringMode="Contains" ClientInstanceName="cbo_store_cd">
                    <ClientSideEvents GotFocus="function(s, e) {cbo_store_cd.SelectAll();}" />
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_eff_date" runat="server" meta:resourcekey="lbl_eff_date"></dx:ASPxLabel>
            </td>

            <td colspan="3">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <dx:ASPxDateEdit ID="dt_eff_date" runat="server" Width="100px" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"
                                AutoPostBack="True">
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 190px;">
                <dx:ASPxLabel ID="lbl_print_drop_tags" runat="server" meta:resourcekey="lbl_print_drop_tags"></dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="cbx_print_drop_flags" runat="server"></dx:ASPxCheckBox>
            </td>
        </tr>

        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_st_upd_dt_range" runat="server" meta:resourcekey="lbl_st_upd_dt_range"></dx:ASPxLabel>
            </td>
            <td colspan="3">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <dx:ASPxDateEdit ID="dt_update_from" runat="server" Width="100px" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"
                                AutoPostBack="True">
                            </dx:ASPxDateEdit>
                        </td>
                        <td align="center" style ="width :20px">
                            <dx:ASPxLabel ID="lbl_to" runat="server" meta:resourcekey="lbl_to"></dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dt_update_to" runat="server" Width="100px" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy"
                                AutoPostBack="True" MinDate="">
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <table>
                    <tr>
                        <td style="width: 100px">
                            <dx:ASPxButton ID="btn_generate_tags" runat="server" meta:resourcekey="btn_generate_tags" ></dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_clear" runat="server" meta:resourcekey="btn_clear" Width="100"></dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxComboBox ID="printer_drp" runat="server" Width="100px" DropDownRows="5">
                            </dx:ASPxComboBox>
                        </td>
                        <td style ="width :100px">
                            <dx:ASPxButton ID="btn_print_summary" runat="server"  meta:resourcekey="btn_print_summary"></dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <dx:ASPxGridView ID="GV_Pricetags" runat="server" AutoGenerateColumns="False" Width="400px" KeyFieldName="price_tags">
                    <ClientSideEvents EndCallback="clear_label" />
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="price_tags" Visible="True" VisibleIndex="0" Width="180px" meta:resourcekey="lbl_price_tags">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Settings ShowFooter="true" />
                    <Templates>
                        <FooterRow>
                            <dx:ASPxLabel ID="GV_Footer_label" runat="server" ClientInstanceName="GV_Footer_label" Text=" " ForeColor="WindowText"></dx:ASPxLabel>
                        </FooterRow>
                    </Templates>
                </dx:ASPxGridView>
            </td>
        </tr>
    </table>
    <dxpc:ASPxPopupControl ID="popup_multi_item" runat="server" ClientInstanceName="popup_multi_item" CloseAction="CloseButton"
        Height="300px" Modal="True" PopupAction="None" meta:resourcekey="lbl_multi_items"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="500px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dx:ASPxLabel ID="lbl_warning" runat="server" Width="100%" EncodeHtml="false" ForeColor="Red"></dx:ASPxLabel>
    <dx:ASPxLabel ID="lbl_ps_message" runat="server" Width="100%" ForeColor="Black"></dx:ASPxLabel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
