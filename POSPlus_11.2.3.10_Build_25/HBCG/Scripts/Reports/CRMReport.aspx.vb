Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Reports_SalesReport
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack = True Then
            ' populate the store drop down
            store_cd_populate()
            txt_start_dt.SelectedDate = Today.Date.AddDays(-7)
            txt_end_dt.SelectedDate = Today.Date
            txt_del_start.SelectedDate = Today.Date.AddDays(-7)
            txt_del_end.SelectedDate = Today.Date
        End If

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        If ConfigurationManager.AppSettings("store_based") = "Y" And Skip_Data_Security() = "N" Then
            SQL = "SELECT store.store_cd, store.store_name, store.store_cd || ' - ' || store.store_name as full_desc FROM store "
            SQL = SQL & "WHERE STORE_CD IN(select store_grp$store.store_cd "
            SQL = SQL & "from store_grp$store, emp2store_grp "
            SQL = SQL & "where emp2store_grp.store_grp_cd=store_grp$store.store_grp_cd "
            SQL = SQL & "and emp2store_grp.emp_cd='" & Session("EMP_CD") & "') "
            SQL = SQL & " order by store_cd"
        Else
            SQL = "SELECT store_grp_cd, des, store_grp_cd || ' - ' || des as full_desc FROM store_grp order by store_grp_cd"
        End If

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store_cd
                .DataSource = ds
                .DataValueField = "store_grp_cd"
                .DataTextField = "full_desc"
                .DataBind()
                .SelectedValue = "ALL"
            End With
            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_exit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_exit.Click

        Response.Redirect("../newmain.aspx")

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        Dim del_chg As Double = 0
        Dim tax_chg As Double = 0
        Dim setup_chg As Double = 0
        Dim subtotal As Double = 0
        Dim sql As String
        Dim HTML As String = ""

        If Not IsDate(txt_start_dt.SelectedDate) And Not IsDate(txt_end_dt.SelectedDate) Then
            lbl_errors.Text = "You must enter a start and end written date."
        End If

        conn.Open()

        sql = "Select * from RELATIONSHIP WHERE WR_DT BETWEEN TO_DATE('" & txt_start_dt.SelectedDate
        sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_end_dt.SelectedDate & "','mm/dd/RRRR') "
        sql = sql & "AND STORE_CD IN(" & Retrieve_Store_Group_Stores(cbo_store_cd.SelectedValue) & ") "
        If cbo_ord_stat.SelectedValue <> "B" Then
            sql = sql & "AND REL_STATUS = '" & cbo_ord_stat.SelectedValue & "' "
        End If
        If IsDate(txt_del_start.SelectedDate) And IsDate(txt_del_end.SelectedDate) Then
            sql = sql & "AND FOLLOW_UP_DT BETWEEN TO_DATE('" & txt_del_start.SelectedDate
            sql = sql & "','mm/dd/RRRR') AND TO_DATE('" & txt_del_end.SelectedDate & "','mm/dd/RRRR') "
        End If
        sql = sql & "ORDER BY " & RadioButtonList1.SelectedValue

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then
                HTML = "<div align=center>"
                HTML = HTML & "<table border=0 width=""97%"" bgcolor=""white"" cellpadding=0 cellspacing=0 style=""font:10pt Tahoma"" border=0>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 bgcolor=lightgrey align=center><b>CUSTOMER RELATIONSHIP REPORT</b></td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=2 bgcolor=lightgrey align=left>Run By: " & Session("EMP_CD") & "</td>"
                HTML = HTML & "<td colspan=3 bgcolor=lightgrey align=right>" & Now() & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5><hr></td>"
                HTML = HTML & "</tr>"
            Else
                HTML = "<br />No records returned.<br /><br /><br />"
            End If
            Do While MyDataReader.Read
                subtotal = 0
                If MyDataReader.Item("DEL_CHG") & "" <> "" Then
                    If IsNumeric(MyDataReader.Item("DEL_CHG")) Then
                        del_chg = CDbl(MyDataReader.Item("DEL_CHG").ToString)
                    Else
                        del_chg = 0
                    End If
                Else
                    del_chg = 0
                End If
                If MyDataReader.Item("TAX_CHG") & "" <> "" Then
                    If IsNumeric(MyDataReader.Item("TAX_CHG")) Then
                        tax_chg = CDbl(MyDataReader.Item("TAX_CHG").ToString)
                    Else
                        tax_chg = 0
                    End If
                Else
                    tax_chg = 0
                End If
                HTML = HTML & "<tr>"
                HTML = HTML & "<td align=left>" & MyDataReader.Item("FNAME").ToString & " " & MyDataReader.Item("LNAME").ToString & "</td>"
                HTML = HTML & "<td align=left>"
                HTML = HTML & "<a href=""../Relationship_Maintenance.aspx?query_returned=Y&del_doc_num=" & MyDataReader.Item("REL_NO") & """>" & MyDataReader.Item("REL_NO") & "</a></td>"
                HTML = HTML & "<td align=left>Customer:&nbsp;<a href=""../Main_Customer.aspx?query_returned=Y&cust_cd=" & MyDataReader.Item("CUST_CD") & """>" & MyDataReader.Item("CUST_CD") & "</a></td>"
                'Pickup/Delivery Information
                HTML = HTML & "<td colspan=2 rowspan=4 align=right valign=top>"
                HTML = HTML & "<table border=0 width=""80%"" cellpadding=0 cellspacing=0>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td align=left>&nbsp;&nbsp;"
                Select Case MyDataReader.Item("P_D").ToString
                    Case "P"
                        HTML = HTML & "Pickup - "
                    Case "D"
                        HTML = HTML & "Delivery - "
                End Select

                HTML = HTML & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "</table>"
                HTML = HTML & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td align=left>" & MyDataReader.Item("ADDR1").ToString & "</td>"
                HTML = HTML & "<td align=left>Relationship:&nbsp;&nbsp;(" & MyDataReader.Item("REL_STATUS").ToString & ")"
                Select Case MyDataReader.Item("REL_STATUS").ToString
                    Case "O"
                        HTML = HTML & "pen"
                    Case "F"
                        HTML = HTML & "inalized"
                    Case "C"
                        HTML = HTML & "onverted"
                    Case "V"
                        HTML = HTML & "oided"
                End Select
                HTML = HTML & "</td>"
                HTML = HTML & "<td>&nbsp;</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=3 align=left>" & MyDataReader.Item("CITY").ToString & "&nbsp;&nbsp;&nbsp; "
                HTML = HTML & MyDataReader.Item("ST").ToString & "&nbsp;&nbsp;&nbsp;&nbsp;" & MyDataReader.Item("ZIP") & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=3 align=left>" & MyDataReader.Item("HPHONE").ToString & "&nbsp;&nbsp;&nbsp; "
                HTML = HTML & MyDataReader.Item("BPHONE").ToString & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td>&nbsp;</td>"
                'Populate the Sales Order Line information
                HTML = HTML & "<td colspan=4 align=left>"
                sql = "SELECT * FROM RELATIONSHIP_LINES "
                sql = sql & "WHERE REL_NO = '" & MyDataReader.Item("REL_NO").ToString & "' ORDER BY LINE"

                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    Dim FoundME As Boolean = False
                    If MyDataReader2.Read Then
                        HTML = HTML & "<table width=""100%"" cellpadding=0 cellspacing=0>"
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td width=15>" & MyDataReader2.Item("LINE").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("ITM_CD").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("VE_CD").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDataReader2.Item("VSN").ToString & "</td>"
                        HTML = HTML & "<td width=10>" & MyDataReader2.Item("QTY").ToString & "</td>"
                        subtotal = subtotal + CDbl(MyDataReader2.Item("QTY").ToString * MyDataReader2.Item("RET_PRC").ToString)
                        HTML = HTML & "<td align=right width=100>" & FormatCurrency(MyDataReader2.Item("RET_PRC").ToString, 2) & "</td>"
                        HTML = HTML & "</tr>"
                        FoundME = True
                    End If
                    Do While MyDataReader2.Read
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td width=15>" & MyDataReader2.Item("LINE").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("ITM_CD").ToString & "</td>"
                        HTML = HTML & "<td width=100>" & MyDataReader2.Item("VE_CD").ToString & "</td>"
                        HTML = HTML & "<td>" & MyDataReader2.Item("VSN").ToString & "</td>"
                        HTML = HTML & "<td width=10>" & MyDataReader2.Item("QTY").ToString & "</td>"
                        subtotal = subtotal + CDbl(MyDataReader2.Item("QTY").ToString * MyDataReader2.Item("RET_PRC").ToString)
                        HTML = HTML & "<td align=right width=100>" & FormatCurrency(MyDataReader2.Item("RET_PRC").ToString, 2) & "</td>"
                        HTML = HTML & "</tr>"
                    Loop
                    If FoundME = True Then
                        HTML = HTML & "</table>"
                    End If
                    MyDataReader2.Close()
                Catch
                    Throw
                End Try
                HTML = HTML & "</td>"
                HTML = HTML & "</tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 align=left><b><u><a href=../SalesOrderComments.aspx?del_doc_num=" & MyDataReader.Item("REL_NO").ToString
                HTML = HTML & "&cust_cd=" & MyDataReader.Item("CUST_CD").ToString & ">Comments:</a></u></b></td>"
                HTML = HTML & "</tr>"

                'Add Sales Order Comments
                sql = "Select * FROM RELATIONSHIP_COMMENTS WHERE REL_NO = '" & MyDataReader.Item("REL_NO").ToString
                sql = sql & "' ORDER BY SUBMIT_DT"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                Dim Repeat_Me As Boolean = True
                Dim comment_data As String = ""
                Dim term_comm As Boolean = False
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If Not MyDataReader2.Read Then
                        comment_data = comment_data & "<tr>"
                        comment_data = comment_data & "<td colspan=3>&nbsp;</td>"
                    Else
                        comment_data = comment_data & "<tr><td colspan=3 align=left valign=top>"
                        comment_data = comment_data & "<table width=""100%"" celpadding=0 cellspacing=0>"
                        term_comm = True
                    End If
                    Do While MyDataReader2.Read
                        comment_data = comment_data & "<tr>"
                        comment_data = comment_data & "<td align=left width=70>" & FormatDateTime(MyDataReader2.Item("DT").ToString, DateFormat.ShortDate) & ")</td>"
                        comment_data = comment_data & "<td colspan=2 align=left>" & MyDataReader2.Item("COMMENTS").ToString & "</td>"
                        comment_data = comment_data & "</tr>"
                    Loop
                    MyDataReader2.Close()
                Catch
                    Throw
                End Try
                If term_comm = True Then
                    comment_data = comment_data & "</table></td><td colspan=2 valign=top align=right>"
                Else
                    comment_data = comment_data & "<td colspan=2 valign=top align=right>"
                End If
                'Add the balance and payment information
                Dim payment_data As String

                payment_data = "<table cellpadding=0 cellspacing=0 border=0>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>SubTotal:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(subtotal, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Delivery:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(del_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Setup:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(setup_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Taxes:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(tax_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "<tr>"
                payment_data = payment_data & "<td align=right>Grand Total:</td>"
                payment_data = payment_data & "<td>&nbsp;&nbsp;</td>"
                payment_data = payment_data & "<td align=left>" & FormatCurrency(subtotal + del_chg + setup_chg + tax_chg, 2, , TriState.True, ) & "</td>"
                payment_data = payment_data & "</tr>"
                payment_data = payment_data & "</table>"
                HTML = HTML & comment_data & payment_data & "</td></tr>"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td colspan=5 bgcolor=lightgrey>&nbsp;</td>"
                HTML = HTML & "</tr>"
            Loop
            HTML = HTML & "</table>"
            MyDataReader.Close()
        Catch ex As Exception
            Throw
        End Try
        If InStr(HTML, "No records returned") > 0 Then HTML = Replace(HTML, "</table>", "")
        main_body.InnerHtml = HTML
        btn_create_new.Visible = True

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub btn_create_new_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_create_new.Click

        Response.Redirect("CRMReport.aspx")

    End Sub
End Class
