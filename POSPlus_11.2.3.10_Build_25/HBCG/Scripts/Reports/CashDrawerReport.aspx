<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CashDrawerReport.aspx.vb"
    Inherits="Reports_CashDrawerReport" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="mainbody" runat="server">
        <br />
        <table width="100%">
            <tr>
                <td width="100%" valign="top">
                    <div>
                        <table>
                            <tr>
                                <td style="text-align: right">
                                    Company Code:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cbo_company" runat="server" Width="285px" CssClass="style5">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    Store Code:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cbo_store_cd" runat="server" Width="285px" CssClass="style5">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    Transaction Date:
                                </td>
                                <td nowrap>
                                    <table>
                                        <tr>
                                            <td>
                                                <dx:ASPxDateEdit ID="txt_start_dt" runat="server">
                                                </dx:ASPxDateEdit>
                                            </td>
                                            <td>
                                                &nbsp;through&nbsp;
                                            </td>
                                            <td>
                                                <dx:ASPxDateEdit ID="txt_end_dt" runat="server">
                                                </dx:ASPxDateEdit>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Button ID="btn_submit" runat="server" Text="Run Report" Width="120px" CssClass="style5" />
                                    &nbsp;
                                    <input id="Reset1" style="width: 120px" type="reset" value="Reset Options" onclick="return Reset1_onclick()"
                                        class="style5" />
                                    &nbsp;
                                    <asp:Button ID="btn_exit" runat="server" Text="Exit Report" Width="120px" CssClass="style5" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <br />
    </div>
</asp:Content>
