<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Audit_logs.aspx.vb" Inherits="Reports_Audit_logs" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="">
        </dx:ASPxLabel>
        <table>
            <tr>
                <td>
                    <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" AutoPostBack="True">
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    &nbsp;<dx:ASPxLabel ID="ASPxLabel1" runat="server" Text=" through ">
                    </dx:ASPxLabel>
                    &nbsp;
                </td>
                <td>
                    <dx:ASPxDateEdit ID="ASPxDateEdit2" runat="server" AutoPostBack="true">
                    </dx:ASPxDateEdit>
                </td>
            </tr>
        </table>
        <br />
        &nbsp;<br />
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" Width="786px" DataSourceID="SqlDataSource1"
            AutoGenerateColumns="False" KeyFieldName="AUDIT_ID">
            <SettingsPager PageSize="25">
            </SettingsPager>
            <Columns>
                <dx:GridViewCommandColumn VisibleIndex="0">
                    <ClearFilterButton Visible="True">
                    </ClearFilterButton>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="AUDIT_ID" ReadOnly="True" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="AUDIT_COLUMN_ID" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="AUDIT_COLUMN_DESC" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn FieldName="AUDIT_DT" VisibleIndex="4">
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn FieldName="AUDIT_USER" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
            </Columns>
            <Settings ShowFilterRow="True" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ERP %>"
            ProviderName="<%$ ConnectionStrings:ERP.ProviderName %>" SelectCommand='SELECT "AUDIT_ID", "AUDIT_COLUMN_ID", "AUDIT_COLUMN_DESC", "AUDIT_DT", "AUDIT_USER" FROM "AUDIT_LOG" WHERE (("AUDIT_DT" >= :AUDIT_DT) AND ("AUDIT_DT" <= :AUDIT_DT2)) ORDER BY "AUDIT_DT"'>
            <SelectParameters>
                <asp:ControlParameter ControlID="ASPxDateEdit1" Name="AUDIT_DT" PropertyName="Value"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="ASPxDateEdit2" Name="AUDIT_DT2" PropertyName="Value"
                    Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
