Imports System.Data.OracleClient
Imports DevExpress.XtraReports.UI

Partial Class Reports_CashReceipts
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            frm_submit.Visible = True
            store_cd_populate()
            Store_Update()
            cashier1_populate()
        End If

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand
        Dim stored_store_cd
        Dim SQL As String

        If Session("store_cd") & "" <> "" Then
            stored_store_cd = Session("store_Cd")
        Else
            stored_store_cd = Session("HOME_STORE_CD")
        End If
        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        ' SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Dim p_co_cd As String = Session("CO_CD") 'lucy

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store "
        SQL = SQL & "  where co_cd in(select co_cd from co_grp where co_grp_cd in (select co_grp_cd from co_grp where co_cd= '" & p_co_cd & "'))"
        SQL = SQL & " order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL

            End With


            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)
            Dim foundrow As DataRow
            If stored_store_cd & "" <> "" Then
                Dim pkColumn(1) As DataColumn
                pkColumn(0) = ds.Tables(0).Columns("STORE_CD")
                'set the primary key to the CustomerID column
                ds.Tables(0).PrimaryKey = pkColumn
                foundrow = ds.Tables(0).Rows.Find(stored_store_cd)
            End If

            With store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                If Not (foundrow Is Nothing) Then
                    .SelectedValue = stored_store_cd
                End If
                .DataBind()
            End With
        Catch ex As Exception
            Throw
        End Try

    End Sub

    Public Sub Store_Update()

        If store_cd.SelectedValue <> "" Then

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
            Dim sql As String
            Dim cmdGetStores As OracleCommand
            cmdGetStores = DisposablesManager.BuildOracleCommand

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT csh_dwr_cd, des, csh_dwr_cd || ' - ' || des as full_desc FROM csh_dwr WHERE STORE_CD='" & store_cd.SelectedValue.ToString & "' order by csh_dwr_cd"

            Dim ds2 As New DataSet

            Try
                With cmdGetStores
                    .Connection = conn
                    .CommandText = sql

                End With

                Dim oAdp2 = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
                oAdp2.Fill(ds2)

                With cbo_csh_drw
                    .DataSource = ds2
                    .DataValueField = "csh_dwr_cd"
                    .DataTextField = "full_desc"
                    .DataBind()
                End With

                cbo_csh_drw.Items.Insert(0, "Please Select A Cash Drawer")
                cbo_csh_drw.Items.FindByText("Please Select A Cash Drawer").Value = ""
                cbo_csh_drw.SelectedIndex = 0

            Catch ex As Exception
                Throw
            End Try

        End If
    End Sub

    Protected Sub store_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles store_cd.SelectedIndexChanged

        Store_Update()

    End Sub

    Public Sub cashier1_populate()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim ds As New DataSet
        Dim cmdGetCashiers As OracleCommand
        cmdGetCashiers = DisposablesManager.BuildOracleCommand
        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If


        Dim p_co_cd As String = Session("CO_CD") 'lucy
        SQL = "SELECT EMP.LNAME || ', ' || EMP.FNAME AS FullNAME, EMP.HOME_STORE_CD, EMP.EMP_CD FROM EMP, EMP_SLSP, EMP$EMP_TP "
        SQL = SQL & "WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD and EMP.TERMDATE IS NULL AND EMP_TP_CD='SLS' AND EMP.EMP_CD=EMP$EMP_TP.EMP_CD "
        SQL = SQL & "AND EMP$EMP_TP.EFF_DT <= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        '  SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') ORDER BY FullNAME" 'lucy
        SQL = SQL & "AND EMP$EMP_TP.END_DT >= TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        SQL = SQL & "AND EMP.home_store_cd in(select store_cd from store where co_cd in(select co_cd from co_grp where co_grp_cd in (select co_grp_cd from co_grp where co_cd ='" & p_co_cd & "' )))"
        SQL = SQL & " ORDER BY FullNAME"

        Try
            With cmdGetCashiers
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCashiers)
            oAdp.Fill(ds)

            With cbo_slsp
                .DataSource = ds
                .DataValueField = "EMP_CD"
                .DataTextField = "FullNAME"
                .DataBind()
            End With

            cbo_slsp.Items.Insert(0, "Please Select A Salesperson")
            cbo_slsp.Items.FindByText("Please Select A Salesperson").Value = ""
            cbo_slsp.SelectedIndex = 0

            conn.Close()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Dim UpPanel As UpdatePanel
        UpPanel = Master.FindControl("UpdatePanel1")

        ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike2", "window.open('CashReceiptsReport.aspx?tran_dt=" & txttrandt.Text & "&checkbox1=" & CheckBox1.Checked & "&slsp=" & cbo_slsp.SelectedValue.ToString & "&csh_drw=" & cbo_csh_drw.SelectedValue.ToString & "&store_cd=" & store_cd.SelectedValue.ToString & "','frmFinance2" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

    End Sub

    Protected Sub cbo_slsp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_slsp.SelectedIndexChanged

        If Not String.IsNullOrEmpty(cbo_slsp.SelectedValue.ToString) Then
            CheckBox1.Enabled = True
        End If

    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

        If CheckBox1.Checked = True Then
            store_cd.Enabled = False
            cbo_csh_drw.Enabled = False
        Else
            store_cd.Enabled = True
            cbo_csh_drw.Enabled = True
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class

