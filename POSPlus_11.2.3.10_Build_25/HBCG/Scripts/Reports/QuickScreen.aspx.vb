Imports System.Data.OracleClient
Imports DevExpress.XtraReports.UI

Partial Class Reports_CashReceiptsReport
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            store_cd_populate()
        End If

    End Sub

    Public Function CreateBoundLabel(ByVal PropertyName As String, ByVal DataSource As DataSet, ByVal DataMember As String)
        ' Create a label.
        Dim Label As New XRLabel()

        ' Bind a label to the specified data field.
        Label.DataBindings.Add(PropertyName, DataSource, DataMember)

        Return Label
    End Function

    Public Function CreateGroupField(ByVal GroupFieldName As String) As GroupField
        ' Create a GroupField object.
        Dim groupField As New GroupField()

        ' Set its field name.
        groupField.FieldName = GroupFieldName

        Return groupField
    End Function

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With cbo_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        cbo_store_cd.Items.Insert(0, "All Store Codes")
        cbo_store_cd.Items.FindByText("All Store Codes").Value = "ALL"
        cbo_store_cd.SelectedIndex = 0


    End Sub

    Public Sub Publish_Report()

        ReportToolbar1.Enabled = True
        ReportToolbar1.Visible = True
        ReportViewer1.Visible = True
        frm_submit.Visible = False
        ' Create a report.
        Dim Report As New QuickScreen

        ' Create a connection.
        Dim Connection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        Dim sql As String
        sql = "SELECT EMP_CD, QS_STATUS, COUNT(QS_STATUS) AS QS_COUNT, '" & txt_start_dt.Text & "' AS ST_DT, '" & txt_end_dt.Text & "' AS END_DT, '" & cbo_store_cd.SelectedValue & "' AS REP_ST_CD FROM QS_AUDIT WHERE LAST_UPDATE BETWEEN TO_DATE('" & txt_start_dt.Text & " 00:00','mm/dd/yyyy hh24:mi') AND "
        sql = sql & "TO_DATE('" & txt_end_dt.Text & " 23:59','mm/dd/yyyy hh24:mi') "
        If cbo_store_cd.SelectedValue <> "ALL" Then
            sql = sql & "AND STORE_CD='" & cbo_store_cd.SelectedValue & "' "
        End If
        sql = sql & "AND CUST_CD IS NOT NULL "
        sql = sql & "GROUP BY EMP_CD, QS_STATUS "


        ' Create a data adapter and a dataset.
        Dim Adapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, Connection)
        Dim DataSet1 As New DataSet()

        ' Specify the data adapter and the data source for the report.
        ' Note that you must fill the datasource with data because it is not bound directly to the specified data adapter.
        Report.DataAdapter = Adapter
        Adapter.Fill(DataSet1, "AR_TRN")
        If DataSet1.Tables(0).Rows.Count > 0 Then
            Report.DataSource = DataSet1
            Report.DataMember = "AR_TRN"
            Report.BindSOVariables()

            ' Create a group header band and add it to the report.
            Dim GhBand As New GroupHeaderBand
            GhBand.Height = 10
            GhBand.GroupFields.Add(CreateGroupField("EMP_CD"))
            Report.Bands.Add(GhBand)

            ReportViewer1.Report = Report
            ReportViewer1.WritePdfTo(Page.Response)
        Else
            ReportToolbar1.Enabled = False
            ReportToolbar1.Visible = False
            ReportViewer1.Visible = False
            frm_submit.Visible = True
            frm_submit.InnerHtml = "No records found"
        End If

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Publish_Report()

    End Sub
End Class

