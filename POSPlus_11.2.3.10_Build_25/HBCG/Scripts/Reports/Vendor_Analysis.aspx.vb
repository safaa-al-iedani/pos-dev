Imports System.Data.OracleClient
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraCharts

Partial Class Reports_Vendor_Analysis
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim dp_string As String = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            sql = "SELECT ITM.VE_CD from ITM_SLS_HST_DAY, ITM WHERE DT > TO_DATE('" & Today.AddDays(-180) & "','mm/dd/RRRR') AND ITM.ITM_CD=ITM_SLS_HST_DAY.ITM_CD GROUP BY VE_CD"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                Do While MyDataReader.Read()
                    dp_string = dp_string & "'" & MyDataReader.Item("VE_CD").ToString & "',"
                Loop
                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            If dp_string & "" = "" Then
                btn_submit.Enabled = False
                lbl_warning.Text = "No vendors found"
            Else
                dp_string = Left(dp_string, Len(dp_string) - 1)
                conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
                conn.Open()
                Dim objSql2 As OracleCommand
                Dim MyDataReader2 As OracleDataReader
                Dim ds2 As DataSet
                Dim oAdp2 As OracleDataAdapter
                Dim dv2 As DataView
                ds2 = New DataSet

                sql = "SELECT VE_CD, VE_NAME from VE WHERE VE_CD IN(" & dp_string & ") ORDER BY VE_NAME"
                'Set SQL OBJECT 
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                oAdp2 = DisposablesManager.BuildOracleDataAdapter(objSql2)
                oAdp2.Fill(ds2)

                dv2 = ds2.Tables(0).DefaultView
                Try
                    'Execute DataReader 
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    'Store Values in String Variables 
                    If MyDataReader2.Read() Then
                        cbo_vendor.DataSource = dv2
                        cbo_vendor.DataValueField = "VE_CD"
                        cbo_vendor.DataTextField = "VE_NAME"
                        cbo_vendor.DataBind()
                    End If
                    'Close Connection 
                    MyDataReader2.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
                conn.Close()
            End If

        End If

    End Sub

    Public Function CreateBoundLabel(ByVal PropertyName As String, ByVal DataSource As DataSet, ByVal DataMember As String)
        ' Create a label.
        Dim Label As New XRLabel()

        ' Bind a label to the specified data field.
        Label.DataBindings.Add(PropertyName, DataSource, DataMember)

        Return Label
    End Function

    Public Function CreateGroupField(ByVal GroupFieldName As String) As GroupField
        ' Create a GroupField object.
        Dim groupField As New GroupField()

        ' Set its field name.
        groupField.FieldName = GroupFieldName

        Return groupField
    End Function

    Public Sub Publish_Report()

        'ReportToolbar1.Enabled = True
        'ReportToolbar1.Visible = True
        'ReportViewer1.Visible = True
        'Dim Report As New Vendor_Analysis

        'Report.BindSOVariables()
        'ReportViewer1.Report = Report

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader

        conn2 = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn2.Open()

        sql = "SELECT VE_CD, VE_NAME, TERMS_PCT, TERMS_DAYS, LST_PUR_DT FROM VE WHERE VE_CD='" & cbo_vendor.SelectedValue & "'"
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            'Store Values in String Variables 
            If MyDataReader2.Read() Then
                lbl_vendor_info.Text = "<br /><u>Vendor Code:</u>&nbsp;&nbsp;" & MyDataReader2.Item("VE_CD").ToString
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<br /><table><tr><td>Vendor:</td><td>" & MyDataReader2.Item("VE_NAME").ToString & "</td></tr>"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<tr><td>Percent Terms:</td><td>" & MyDataReader2.Item("TERMS_PCT").ToString & "</td></tr>"
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<tr><td>Terms (Days):</td><td>" & MyDataReader2.Item("TERMS_DAYS").ToString & "</td></tr>"
                If IsDate(MyDataReader2.Item("LST_PUR_DT").ToString) Then
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<tr><td>Last Purchase Date:&nbsp;&nbsp;&nbsp;</td><td>" & FormatDateTime(MyDataReader2.Item("LST_PUR_DT").ToString, DateFormat.ShortDate) & "</td></tr>"
                Else
                    lbl_vendor_info.Text = lbl_vendor_info.Text & "<tr><td>Last Purchase Date:&nbsp;&nbsp;&nbsp;</td><td></td></tr>"
                End If
                lbl_vendor_info.Text = lbl_vendor_info.Text & "<br />"
            End If
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try
        conn2.Close()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim ds2 As DataSet
        Dim oAdp As OracleDataAdapter
        Dim start_timestamp As Date
        start_timestamp = Now.AddDays(-DropDownList1.SelectedValue)

        ds = New DataSet

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "SELECT TO_CHAR(ishd.DT,'MM/dd') AS WR_DT, SUM(ishd.QTY) AS ORIG_COUNT, SUM(ishd.QTY*ABS(ishd.RET)) AS SALE_COUNT, ishd.STORE_CD "
        sql = sql & "FROM ITM_SLS_HST_DAY ishd, ITM "
        sql = sql & "WHERE ishd.DT > TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        sql = sql & "AND ITM.VE_CD='" & cbo_vendor.SelectedValue & "' "
        sql = sql & "AND ITM.ITM_CD=ishd.ITM_CD AND ishd.sls_wr_cd = 'D' "   ' only extract delivered business
        sql = sql & "GROUP BY TO_CHAR(ishd.DT,'MM/dd'), ishd.STORE_CD "
        sql = sql & "ORDER BY 1 ASC"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        Dim dt As DataTable = ds.Tables(0)

        Dim view As New DataView(dt)
        view.Sort = "WR_DT ASC"

        ' Generate a data table and bind the series to it.
        WebChartControl1.DataSource = view

        ' Specify data members to bind the series.
        WebChartControl1.Titles.Clear()
        Dim headTitle As New ChartTitle
        headTitle.Text = "Sales Count by Store for Vendor " & cbo_vendor.Text
        headTitle.Font = New System.Drawing.Font("Tahoma", 16)
        WebChartControl1.Titles.Add(headTitle)

        WebChartControl1.SeriesDataMember = "STORE_CD"
        WebChartControl1.SeriesTemplate.ArgumentDataMember = "WR_DT"
        WebChartControl1.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("ORIG_COUNT").ColumnName.ToString))

        WebChartControl1.SeriesTemplate.View = New SplineSeriesView

        WebChartControl1.DataBind()

        'ds2 = New DataSet

        'sql = "SELECT TO_CHAR(WR_DT,'MM/dd') AS WR_DT, SUM(QTY*UNIT_PRC) AS ORIG_COUNT, STORE_CD "
        'sql = sql & "FROM SALES_REPORT_ARCHIVE "
        'sql = sql & "WHERE WR_DT > TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        'sql = sql & "AND VE_CD='" & cbo_vendor.SelectedValue & "' "
        'sql = sql & "GROUP BY TO_CHAR(WR_DT,'MM/dd'), STORE_CD"

        'objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        'oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        'oAdp.Fill(ds2)

        'Dim dt2 As DataTable = ds2.Tables(0)
        'Dim view2 As New DataView(dt2)
        'view2.Sort = "WR_DT ASC"

        ' Generate a data table and bind the series to it.
        WebChartControl2.DataSource = view

        WebChartControl2.Titles.Clear()
        Dim headTitle2 As New ChartTitle
        headTitle2.Text = "Sales Revenue by Store for Vendor " & cbo_vendor.Text
        headTitle2.Font = New System.Drawing.Font("Tahoma", 16)
        WebChartControl2.Titles.Add(headTitle2)

        ' Specify data members to bind the series.
        WebChartControl2.SeriesDataMember = "STORE_CD"
        WebChartControl2.SeriesTemplate.ArgumentDataMember = "WR_DT"
        WebChartControl2.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("SALE_COUNT").ColumnName.ToString))

        WebChartControl2.SeriesTemplate.View = New SplineSeriesView

        WebChartControl2.DataBind()

        Dim ds3 As New DataSet

        sql = "SELECT SUM(ishd.QTY*ABS(ishd.RET)) AS ORIG_COUNT, ITM.MNR_CD, 'Values' as VAL "
        sql = sql & "FROM ITM_SLS_HST_DAY ishd, ITM "
        sql = sql & "WHERE ishd.DT > TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        sql = sql & "AND ITM.VE_CD='" & cbo_vendor.SelectedValue & "' "
        sql = sql & "AND ITM.ITM_CD=ishd.ITM_CD AND ishd.sls_wr_cd = 'D' "   ' only extract delivered business
        sql = sql & "GROUP BY ITM.MNR_CD"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds3)

        Dim dt3 As DataTable = ds3.Tables(0)
        Dim view3 As New DataView(dt3)

        ' Generate a data table and bind the series to it.
        WebChartControl3.DataSource = view3

        WebChartControl3.Titles.Clear()
        Dim headTitle3 As New ChartTitle
        headTitle3.Text = "Revenue by Minor for Vendor " & cbo_vendor.Text
        headTitle3.Font = New System.Drawing.Font("Tahoma", 16)
        WebChartControl3.Titles.Add(headTitle3)

        ' Specify data members to bind the series.
        WebChartControl3.SeriesDataMember = "MNR_CD"
        WebChartControl3.SeriesTemplate.ArgumentDataMember = "MNR_CD"
        WebChartControl3.SeriesTemplate.ValueDataMembers.AddRange(New String(dt.Columns("ORIG_COUNT").ColumnName.ToString))

        WebChartControl3.SeriesTemplate.View = New SideBySideBarSeriesView

        WebChartControl3.DataBind()

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Publish_Report()

    End Sub
End Class

