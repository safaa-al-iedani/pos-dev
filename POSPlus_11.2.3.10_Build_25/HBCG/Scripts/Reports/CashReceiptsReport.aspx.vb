Imports System.Data.OracleClient
Imports DevExpress.XtraReports.UI

Partial Class Reports_CashReceiptsReport
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Publish_Report()

    End Sub

    Public Function CreateBoundLabel(ByVal PropertyName As String, ByVal DataSource As DataSet, ByVal DataMember As String)
        ' Create a label.
        Dim Label As New XRLabel()

        ' Bind a label to the specified data field.
        Label.DataBindings.Add(PropertyName, DataSource, DataMember)

        Return Label
    End Function

    Public Function CreateGroupField(ByVal GroupFieldName As String) As GroupField
        ' Create a GroupField object.
        Dim groupField As New GroupField()

        ' Set its field name.
        groupField.FieldName = GroupFieldName

        Return groupField
    End Function

    Public Sub Publish_Report()

        ReportToolbar1.Enabled = True
        ReportToolbar1.Visible = True
        ReportViewer1.Visible = True
        frm_submit.Visible = False
        ' Create a report.
        Dim Report As New CashReceipts

        ' Create a connection.
        Dim Connection As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)

        Dim sql As String
        sql = "SELECT a.CUST_CD, a.MOP_CD, a.EMP_CD_CSHR, a.BNK_ACCT_CD, DECODE(a.TRN_TP_CD,'PMT',A.AMT,'DEP',A.AMT,'R',-A.AMT,A.AMT) AS AMT, a.CHK_NUM, "
        sql = sql & "a.APP_CD, a.POST_DT, a.PMT_STORE, a.CSH_DWR_CD, a.EXP_DT, b.FNAME, b.LNAME, b.HOME_STORE_CD, a.IVC_CD, "
        sql = sql & "substr(a.BNK_CRD_NUM, length(a.BNK_CRD_NUM) - 3) AS BNK_CRD_NUM "
        sql = sql & "FROM AR_TRN a, EMP b WHERE "
        sql = sql & "a.POST_DT=TO_DATE('" & FormatDateTime(Request("tran_dt").ToString, DateFormat.ShortDate) & "','mm/dd/RRRR') "
        sql = sql & "AND a.TRN_TP_CD IN('PMT','DEP','R') "
        sql = sql & "AND a.EMP_CD_CSHR = b.EMP_CD (+) "
        If Request("checkbox1") = True And Not String.IsNullOrEmpty(Request("slsp")) Then
            sql = sql & "AND a.EMP_CD_CSHR = '" & Request("slsp") & "' "
        Else
            If Not String.IsNullOrEmpty(Request("slsp")) Then
                sql = sql & "AND a.EMP_CD_CSHR = '" & Request("slsp") & "' "
            End If
            If Not String.IsNullOrEmpty(Request("csh_drw")) Then
                sql = sql & "AND a.CSH_DWR_CD='" & Request("csh_drw") & "' "
            End If
            sql = sql & "AND a.PMT_STORE='" & Request("store_cd") & "' "
        End If
        sql = sql & "ORDER BY a.PMT_STORE, a.CSH_DWR_CD, a.MOP_CD, a.IVC_CD"

        ' Create a data adapter and a dataset.
        Dim Adapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, Connection)
        Dim DataSet1 As New DataSet()

        ' Specify the data adapter and the data source for the report.
        ' Note that you must fill the datasource with data because it is not bound directly to the specified data adapter.
        Report.DataAdapter = Adapter
        Adapter.Fill(DataSet1, "AR_TRN")
        If DataSet1.Tables(0).Rows.Count > 0 Then
            Report.DataSource = DataSet1
            Report.DataMember = "AR_TRN"
            Report.BindSOVariables()

            ' Create a group header band and add it to the report.
            Dim GhBand As New GroupHeaderBand
            GhBand.Height = 10
            GhBand.PageBreak = PageBreak.BeforeBand
            GhBand.GroupFields.Add(CreateGroupField("CSH_DWR_CD"))
            Report.Bands.Add(GhBand)

            ReportViewer1.Report = Report
            ReportViewer1.WritePdfTo(Page.Response)
        Else
            ReportToolbar1.Enabled = False
            ReportToolbar1.Visible = False
            ReportViewer1.Visible = False
            frm_submit.Visible = True
            frm_submit.InnerHtml = "No records found"
        End If

    End Sub

End Class

