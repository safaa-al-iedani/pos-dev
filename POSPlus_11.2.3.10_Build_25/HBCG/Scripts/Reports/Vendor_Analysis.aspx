<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Vendor_Analysis.aspx.vb"
    Inherits="Reports_Vendor_Analysis" MasterPageFile="~/MasterPages/NoWizard2.master" %>

<%@ Register Assembly="DevExpress.XtraCharts.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.XtraReports.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frm_submit" runat="server">
        <table width="100%">
            <tr>
                <td>
                    &nbsp;&nbsp;<asp:Label ID="Label1" runat="server" CssClass="style5" Text="Vendor:"
                        Width="52px"></asp:Label>
                    <asp:DropDownList ID="cbo_vendor" runat="server" CssClass="style5" Width="427px">
                    </asp:DropDownList><br />
                    <br />
                    &nbsp;
                    <asp:Label ID="Label2" runat="server" CssClass="style5" Text="Time frame:" Width="86px"></asp:Label>&nbsp;<asp:DropDownList
                        ID="DropDownList1" runat="server" CssClass="style5" Width="89px">
                        <asp:ListItem Value="30">30 days</asp:ListItem>
                        <asp:ListItem Value="60">60 days</asp:ListItem>
                        <asp:ListItem Value="90">90 days</asp:ListItem>
                        <asp:ListItem Value="120">120 days</asp:ListItem>
                        <asp:ListItem Value="180">180 days</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lbl_warning" runat="server" Width="60%"></asp:Label></td>
                <td valign="middle" align="center">
                    <asp:Button ID="btn_submit" runat="server" Text="Generate Report" CssClass="style5" />
                </td>
            </tr>
        </table>
        <asp:Label ID="lbl_vendor_info" runat="server" Width="100%"></asp:Label><br />
        <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" Height="210px" Width="683px">
            <seriestemplate><ViewSerializable>
<cc1:SideBySideBarSeriesView></cc1:SideBySideBarSeriesView>
</ViewSerializable>
<LabelSerializable>
<cc1:SideBySideBarSeriesLabel LineVisible="True">
<FillStyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</FillStyle>
</cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
<PointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</PointOptionsSerializable>
<LegendPointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</LegendPointOptionsSerializable>
</seriestemplate>
            <fillstyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</fillstyle>
        </dxchartsui:WebChartControl>
        <br />
        <br />
        <dxchartsui:WebChartControl ID="WebChartControl2" runat="server" Height="210px" Width="683px">
            <seriestemplate><ViewSerializable>
<cc1:SideBySideBarSeriesView></cc1:SideBySideBarSeriesView>
</ViewSerializable>
<LabelSerializable>
<cc1:SideBySideBarSeriesLabel LineVisible="True">
<FillStyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</FillStyle>
</cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
<PointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</PointOptionsSerializable>
<LegendPointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</LegendPointOptionsSerializable>
</seriestemplate>
            <fillstyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</fillstyle>
        </dxchartsui:WebChartControl>
        <br />
        <br />
        <dxchartsui:WebChartControl ID="WebChartControl3" runat="server" Height="210px" Width="683px">
            <seriestemplate><ViewSerializable>
<cc1:SideBySideBarSeriesView></cc1:SideBySideBarSeriesView>
</ViewSerializable>
<LabelSerializable>
<cc1:SideBySideBarSeriesLabel LineVisible="True">
<FillStyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</FillStyle>
</cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
<PointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</PointOptionsSerializable>
<LegendPointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</LegendPointOptionsSerializable>
</seriestemplate>
            <fillstyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</fillstyle>
        </dxchartsui:WebChartControl>
    </div>
    <dxxr:ReportToolbar ID="ReportToolbar1" runat='server' ShowDefaultButtons='False'
        ReportViewer="<%# ReportViewer1 %>" Visible="False">
        <Items>
            <dxxr:ReportToolbarButton ItemKind='Search' ToolTip='Display the search window' />
            <dxxr:ReportToolbarSeparator />
            <dxxr:ReportToolbarButton ItemKind='PrintReport' ToolTip='Print the report' />
            <dxxr:ReportToolbarButton ItemKind='PrintPage' ToolTip='Print the current page' />
            <dxxr:ReportToolbarSeparator />
            <dxxr:ReportToolbarButton Enabled='False' ItemKind='FirstPage' ToolTip='First Page' />
            <dxxr:ReportToolbarButton Enabled='False' ItemKind='PreviousPage' ToolTip='Previous Page' />
            <dxxr:ReportToolbarLabel Text='Page' />
            <dxxr:ReportToolbarComboBox ItemKind='PageNumber' Width='65px'>
            </dxxr:ReportToolbarComboBox>
            <dxxr:ReportToolbarLabel Text='of' />
            <dxxr:ReportToolbarTextBox IsReadOnly='True' ItemKind='PageCount' />
            <dxxr:ReportToolbarButton ItemKind='NextPage' ToolTip='Next Page' />
            <dxxr:ReportToolbarButton ItemKind='LastPage' ToolTip='Last Page' />
            <dxxr:ReportToolbarSeparator />
            <dxxr:ReportToolbarButton ItemKind='SaveToDisk' ToolTip='Export a report and save it to the disk' />
            <dxxr:ReportToolbarButton ItemKind='SaveToWindow' ToolTip='Export a report and show it in a new window' />
            <dxxr:ReportToolbarComboBox ItemKind='SaveFormat' Width='70px'>
                <Elements>
                    <dxxr:ListElement Text='PDF' Value='pdf' />
                    <dxxr:ListElement Text='XLS' Value='xls' />
                    <dxxr:ListElement Text='RTF' Value='rtf' />
                    <dxxr:ListElement Text='MHT' Value='mht' />
                    <dxxr:ListElement Text='TXT' Value='txt' />
                    <dxxr:ListElement Text='CSV' Value='csv' />
                    <dxxr:ListElement Text='IMG' Value='png' />
                </Elements>
            </dxxr:ReportToolbarComboBox>
        </Items>
        <Styles>
            <LabelStyle>
                <Margins MarginLeft='3px' MarginRight='3px' />
            </LabelStyle>
        </Styles>
    </dxxr:ReportToolbar>
    <dxxr:ReportViewer ID="ReportViewer1" runat="server" Height="567px" Width="683px"
        Visible="False" AutoSize="False" Report="<%# New Vendor_Analysis() %>" ReportName="Vendor_Analysis">
    </dxxr:ReportViewer>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
