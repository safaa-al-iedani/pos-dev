﻿
$('body').on('keydown', 'input, select, textarea', function (e) {
    var self = $(this)
      , form = self.parents('form:eq(0)')
      , focusable
      , next
    ;
    if (e.keyCode == 13) {
        focusable = form.find('input,select,button,textarea').filter(':visible');

        var thisIdx = focusable.index(this);

        if (focusable[thisIdx].type == "button") {
            focusable[thisIdx].click();
        }
        else {
            for (var a = 1; a < focusable.length - 1; a++) {
                next = focusable.eq(thisIdx + a);

                if ($(next).attr('disabled') != 'disabled') {
                    if ($(next).attr('id') != 'txtTransactionDate') {
                        break;
                    }
                }
            }

            if (next.length) {
                next.focus();
            }
            //else {
            //    form.submit();
            //}
        }
        return false;
    }
});

var cdbAccess = null;
var storeList = null;
var cashDrawerList = null;
var cashierList = null;
var isKeyInStore = false;
var storeTransData = null;

$(document).ready(function () {
    $('input, :input').attr('autocomplete', 'off');

    LoadPage();

    $('#txtStore').keyup(function (e) {
        isKeyInStore = true;
        $(this).val(($(this).val()).toUpperCase());
        $("#txtStoreDesc").val('');
        $("#txtCashDrawer").val('');
        $("#txtCashDrawer").prop('disabled', true);
        $("#txtCashDrawerDesc").val('');
        $('#btnClear').prop('disabled', false);
    });

    $("#txtStore").autocomplete({
        source: function (request, response) {

            if (storeList == null) {
                GetStoreList('');
            }
            if (storeList != null) {
                var arr = $.grep(storeList, function (n, i) {
                    if (n.storeCd.indexOf(request.term) >= 0) {
                        return n;
                    }
                });

                response($.map(arr, function (item) {
                    return item.storeCd;
                }))
            }
        },
        minLength: 1,
        select: function (event, ui) {
            $('#lblErrMsg').text('');
            $('#txtStore').val(ui.item.label);
            $('#txtStore').trigger('click');

            GetStoreDetails(ui.item.label);
        }
    });

    $("#txtStore").blur(function () {
        if (isKeyInStore)
            GetStoreDetails($(this).val());
    });

    $("#txtCashDrawer").autocomplete({
        source: function (request, response) {

            var arr = $.grep(cashDrawerList, function (n, i) {
                if (n.cashDrawerCd.indexOf(request.term) >= 0) {
                    return n;
                }
            });

            response($.map(arr, function (item) {
                return item.cashDrawerCd;
            }))
        },
        minLength: 1,
        select: function (event, ui) {
            $('#lblErrMsg').text('');
            $('#txtCashDrawer').val(ui.item.label);
            $('#txtCashDrawer').trigger('click');

            GetCashDrawerDetails(ui.item.label);
        }
    });

    $("#txtCashDrawer").blur(function () {
        GetCashDrawerDetails($(this).val());
    });

    $('#btnGo').click(function () {
        $('.ajax-content').show();

        $('#lblErrMsg').text('');
        $('#divCdb').find('input[type=text]').removeClass('InValidTextBox');

        $('.currency').text($.trim($("#hidCurrSymbol").val()));
        $('.currCode').text($.trim($("#ddlCurr option:selected").text()));

        $('#txtTransCsTotal').val('');
        $('#txtCashOvrShrt').val('');
        $('#txtTransCkTotal').val('');
        $('#txtCheckOverShrt').val('');

        //$('#btnSave').prop('disabled', true);

        if (ValidateCDBQuery()) {
            GetCashDrawerData();
        }
        else
            $('.ajax-content').hide();
    });

    $('#addNewChk').click(function () {
        AddNewCheckRow();
    });

    $('#btnSave').click(function () {
        SaveCDB();
    });

    $('#btnClear').click(function () {
        if ($('#hidIsDataRetrived').val() == 'Yes' && $('#hidIsDataEntered').val() == 'Yes') {
            $('#divPopupConfirm').fadeIn("slow");
            $("body").append('<div class="modalOverlay">');
        }
        else {
            LoadPage();
        }
    });

    $('#btnBalance').click(function () {
        $('.ajax-content').show();

        $('#lblErrMsg').text('');

        GetTranscationBalance(storeTransData, true);

        $('.ajax-content').hide();
    });

    $('#btnPrintDepSlip').click(function () {

        PrintDepositSlip();
    });

    $('#btnClose').click(function () {
        $('.ajax-content').show();
        CloseDrawer();
    });

    $('#btnUpdateTrn').click(function () {

        $('.ajax-content').show();

        var transList = new Array();
        var indx = 1;
        $.each($('#divTransDetail').find('div'), function (idx, ctrl) {
            if (ctrl.id.indexOf('divTrans') >= 0) {

                var trans = new Object();

                //trans.tranPK = GetStoreValue('tranPK');
                trans.companyCd = GetStoreValue('companyCd');
                trans.storeCd = $('#txtStore').val();
                trans.cashDrawerCd = $('#txtCashDrawer').val();
                trans.postDate = $.trim($('#txtTransactionDate').val());

                $.each($(this).find('input[type=text], select,input[type=hidden]'), function (Index, value) {

                    if (value.id.indexOf('transPk' + indx) >= 0) {
                        trans.tranPk = $(this).val();
                    }

                    if (value.id.indexOf('txtTransMop' + indx) >= 0) {
                        trans.mopCd = $(this).val();
                    }

                    if (value.id.indexOf('txtTransInvoice' + indx) >= 0) {
                        trans.invoice = $(this).val();
                    }

                    if (value.id.indexOf('txtTransTrn' + indx) >= 0) {
                        trans.tranTpCd = $(this).val();
                    }

                    if (value.id.indexOf('txtTransDc' + indx) >= 0) {
                        trans.amount = $(this).val();
                    }

                    if (value.id.indexOf('txtTranAmt' + indx) >= 0) {
                        trans.amount = parseFloat($(this).val());
                    }

                });
                indx++;
                transList.push(trans);
            }

        });

        $.ajax({
            type: 'POST',
            url: 'CashDrawerBalancing.aspx/UpdateTransactionDetail',
            data: JSON.stringify({ objTransList: transList, empCd: GetSessionValue('EmpCd') }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {

                setColorForSuccess('#lblTransErr');
                $('#lblTransErr').text('Data saved successfully.');

                LoadTransDetails($('#hidMopTrans').val());
                GetCashDrawerData();

                $('.ajax-content').hide();
            }, failure: function (error) {
                setColorForFailure('#lblTransErr');
                $('#lblTransErr').text(jQuery.parseJSON(error.responseText).Message);

                $('.ajax-content').hide();
            }
        }).fail(function (error) {
            var r = jQuery.parseJSON(error.responseText);
            setColorForFailure('#lblTransErr');
            $('#lblTransErr').text(r.Message);

            $('.ajax-content').hide();
        });
        GetTranscationBalance(storeTransData, false);
    });

    $('#btnResetTrn').click(function () {
        LoadTransDetails($('#hidMopTrans').val());
    });

    $('#imgCloseConfirm').click(function () {
        $('#divPopupConfirm').fadeOut("slow");

        $(".modalOverlay").remove();
    });

    $('#btnConfirmYes').click(function () {
        LoadPage();

        $('#divPopupConfirm').fadeOut("slow");

        $(".modalOverlay").remove();
    });

    $('#btnConfirmCancel').click(function () {
        //$('.ajax-content').show();

        //if ($.trim($('#txtCashTotal').val()) != '' && parseInt($('#txtCashTotal').val(), 10) > 0) {
        //    SaveCdbCash();
        //}

        //SaveCdbCheck();

        $('#divPopupConfirm').fadeOut("slow");

        $(".modalOverlay").remove();
    });

    $(window).on('beforeunload', function (e) {
        if ($('#hidIsDataRetrived').val() == 'Yes' && $('#hidIsDataEntered').val() == 'Yes') {
            return 'You are about to clear entered/updated data?';
        }
    });

    $('#divDepositSlip').mCustomScrollbar({
        scrollButtons: {
            enable: true
        },
        theme: "dark-thick",
        advanced: {
            updateOnContentResize: true
        }
    });

});

function LoadPage() {
    $('.ajax-content').show();

    $('#lblErrMsg').text('');

    $('#hidIsDataEntered').val('');

    $('#divCdb').not(':button, :submit, :reset').val('');
    $('#divCdb').find('input[type=text]').val('');
    $('#divCdb').find('input[type=text]').removeClass('InValidTextBox');
    $('#divCdb').find('select').html('');

    $('#txtCashDrawer').prop('disabled', true);
    $('#txtTransactionDate').prop('disabled', true);
    $('#txtTransactionDate').datepicker({ clickInput: true, changeMonth: true, changeYear: true, yearRange: '1900:2050', maxDate: '1', dateFormat: 'mm/dd/yy', onSelect: function () { $('#lblErrMsg').text(''); } });
    $('#txtTransactionDate').datepicker("setDate", new Date());

    $('#ddlCashier').prop('disabled', true);
    $('#ddlCashier').html('');

    $('#ddlCurr').prop('disabled', true);
    $('#ddlCurr').html('');

    $('#lblIsPrnt').text('No');
    $('#lblPrintSlipCnt').text('0');

    $('.onlynumber').keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

    $('.numeric').numeric();

    $('.disable').prop('disabled', true);

    $('.checkLabel').text(GetGpParmLabel('CDB_CHECK_LABEL'));

    $('#divCdbDetail').find('input, a, button, select').prop('disabled', true);
    $('#divCdbDetail').find('a').attr('disabled', 'disabled');

    $('#divAddCheck').empty();

    $('#divAddOther').empty();

    $('#divCash').find('input[type=text]').keyup(function () {
        CashTotal();
    });

    $('#divCash').find('input[type=text]').change(function () {
        CashTotal();
    });

    $('#divSubBtns').find('input[type=button]').prop('disabled', true);

    $('#divCdbDetail').find('input').keydown(function () {
        $('#btnSave').prop('disabled', false);
        $('#btnClear').prop('disabled', false);
        $('#btnBalance').prop('disabled', true);
        $('#btnClose').prop('disabled', true);
        $('#hidIsDataEntered').val('Yes');
    });

    $('#txtCashDrawer').removeClass("InValidTextBox");
    $('#txtStore').removeClass("InValidTextBox");

    cdbAccess = new Object();
    cdbAccess.empCd = GetSessionValue('EmpCd');

    $.ajax({
        url: 'CashDrawerBalancing.aspx/LoadCDB',
        data: JSON.stringify({ empCd: GetSessionValue('EmpCd') }),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {

            if (null != data) {
                cdbAccess = data.d.cdbAccess;
                storeList = data.d.storeList;
            }

            if (null != cdbAccess) {
                $('#btnUpdateTrn').prop('disabled', !cdbAccess.isUpdateAccess);
            }

            $('#txtStore').focus();

            $('.ajax-content').hide();
        },
        error: function (XMLHttpRequest, callStatus, errorThrown) {
            storeList = null;

            $('.ajax-content').hide();
        }
    });
}

function GetCDBAccess() {

    cdbAccess = new Object();
    cdbAccess.empCd = GetSessionValue('EmpCd');

    $.ajax({
        type: 'POST',
        url: 'CashDrawerBalancing.aspx/GetSecurityAccess',
        data: JSON.stringify({ cdbAccess: cdbAccess }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {

            cdbAccess = msg.d;

        }, failure: function (error) {
            setColorForFailure('#lblErrMsg');
            $('#lblErrMsg').text(jQuery.parseJSON(error.responseText).Message);
        }
    }).fail(function (error) {
        var r = jQuery.parseJSON(error.responseText);
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text(r.Message);
    });
}

function HasSecurityAccess(secCd, ctrl, mop) {

    var flag = false;

    $.ajax({
        type: 'POST',
        url: 'CashDrawerBalancing.aspx/HasSecurityAccess',
        data: JSON.stringify({ secCd: secCd, empCd: GetSessionValue('EmpCd') }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {

            if (msg.d) {
                flag = true;

                if ($.trim(ctrl) != '') {
                    $(ctrl).prop('disabled', false);

                    if ($.trim(mop) != '') {
                        $(ctrl).click(function () {
                            LoadDetailPopup(mop);
                        });
                    }
                }
            }
            else {
                if ($.trim(ctrl) != '') {
                    $(ctrl).prop('disabled', true);
                }
                flag = false;
            }

        }, failure: function (error) {
            setColorForFailure('#lblErrMsg');
            $('#lblErrMsg').text(jQuery.parseJSON(error.responseText).Message);
        }
    }).fail(function (error) {
        var r = jQuery.parseJSON(error.responseText);
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text(r.Message);
    });

    return flag;
}

function GetStoreList(e) {
    $.ajax({
        url: 'CashDrawerBalancing.aspx/GetStoreList',
        data: JSON.stringify({ empCd: GetSessionValue('EmpCd'), filterText: e }),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            storeList = data.d;
        },
        error: function (XMLHttpRequest, callStatus, errorThrown) {
            storeList = null;
        }
    });
}

function GetStoreDetails(e) {
    $('.ajax-content').show();

    var isValid = false;

    if (storeList == null) {
        GetStoreList('');
    }

    $.each(storeList, function (key, value) {
        if (value.storeCd == e) {
            isValid = true;

            $('#txtStoreDesc').val(value.storeName);

            LoadStoreData(value);

            AddStoreValue('companyCd', value.companyCd);
            AddStoreValue('multiCurrency', value.multiCurrency);
            AddStoreValue('defCurrCd', value.defCurrCd);
            AddStoreValue('transRoute', value.transRoute);
            AddStoreValue('bankCd', value.bankCd);
            AddStoreValue('bankAccCd', value.bankAccCd);
            AddStoreValue('bankAccNo', value.bankAccNo);
        }
    });

    if (isValid) {

        if ($('#lblErrMsg').text().indexOf('cash drawer') < 0)
            $('#lblErrMsg').text('');

        $('#txtStore').removeClass("InValidTextBox");

        $('#txtCashDrawer').prop('disabled', false);
    }
    else {
        $('#lblErrMsg').text("Please key in a valid store.");
        setColorForFailure('#lblErrMsg');

        $('#txtStoreDesc').val('');
        $('#txtCashDrawer').prop('disabled', true);
        $('#txtStore').addClass("InValidTextBox");
        $('#txtStore').select();

        AddStoreValue('companyCd', '');
        AddStoreValue('multiCurrency', '');
        AddStoreValue('defCurrCd', '');
        AddStoreValue('transRoute', '');
        AddStoreValue('bankCd', '');
        AddStoreValue('bankAccCd', '');
        AddStoreValue('bankAccNo', '');

        $('.ajax-content').hide();
    }
}

function GetCashDrawerDetails(e) {
    $('.ajax-content').show();
    var isValid = false;

    $.each(cashDrawerList, function (key, value) {
        if (value.cashDrawerCd == e) {
            isValid = true;

            $('#txtCashDrawerDesc').val(value.cashDrawerDesc);
        }
    });

    if (isValid) {
        $('#lblErrMsg').text('');
        $('#txtCashDrawer').removeClass("InValidTextBox");

        $('#txtTransactionDate').prop('disabled', false);
        //$('#txtTransactionDate').focus();

        $('#txtTransactionDate').datepicker({ clickInput: true, changeMonth: true, changeYear: true, yearRange: '1900:2050', maxDate: '1', dateFormat: 'mm/dd/yy' });
        $('#txtTransactionDate').datepicker("setDate", new Date());
    }
    else {
        if (/chrome/.test(navigator.userAgent.toLowerCase())) {
            ValidateCDBQuery()
        }
        else {
            if (!(($.trim($('#txtCashDrawer').val()) === '') && ($("#txtStore").is(":focus")))) {
                $('#txtCashDrawerDesc').val('');

                setColorForFailure('#lblErrMsg');

                $('#txtTransactionDate').prop('disabled', true);
                $('#lblErrMsg').text("Please key in a valid cash drawer.");
                $('#txtCashDrawer').addClass("InValidTextBox");
                $('#txtCashDrawer').select();
            }
        }
    }

    $('.ajax-content').hide();
}

function LoadStoreData(objStore) {

    $.ajax({
        url: 'CashDrawerBalancing.aspx/LoadStoreData',
        data: JSON.stringify({ empCd: GetSessionValue('EmpCd'), store: objStore }),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            cashDrawerList = data.d.CashDrawerList;

            cashierList = data.d.CashierList;
            BindCashier(data.d.CashierList);

            BindCurrency(data.d.CurrencyList, data.d.defCurrCd);

            if (data.d.multiCurrency == "" || data.d.multiCurrency == null || data.d.multiCurrency == "N")
                $('#ddlCurr').prop('disabled', true);

            $('#txtCashDrawer').focus();
            $('.ajax-content').hide();
        },
        error: function (XMLHttpRequest, callStatus, errorThrown) {
            cashDrawerList = null;

            $('.ajax-content').hide();
        }
    });
}

function AddStoreValue(e, sessionVal) {
    var sessionData = $('#hidStoreDtls').val();
    sessionVal = $.trim(sessionVal) != '' ? sessionVal : '';
    if ($.trim(sessionData) != '') {
        if (sessionData.indexOf(e + ':') != -1) {
            var items = sessionData.slice(0, -1).split(';');
            $.each(items, function (Index, value) {
                if (value.indexOf(e + ':') == 0) {
                    sessionData = sessionData.replace(value, e + ':' + sessionVal);
                }
            });

            $('#hidStoreDtls').val(sessionData);
        }
        else {
            $('#hidStoreDtls').val(sessionData + e + ':' + sessionVal + ';');
        }
    }
    else {
        $('#hidStoreDtls').val(e + ':' + sessionVal + ';');
    }
}

function ValidateCDBQuery() {
    var isCDBValid = false;

    if ($.trim($('#txtStore').val()) == '') {
        $('#txtStore').addClass("InValidTextBox");
        $('#txtCashDrawer').removeClass("InValidTextBox");
        $('#txtTransactionDate').removeClass("InValidTextBox");
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text('Please key in a valid store.');
    }
    else if ($.trim($('#txtCashDrawer').val()) == '') {
        $('#txtStore').removeClass("InValidTextBox");
        $('#txtCashDrawer').addClass("InValidTextBox");
        $('#txtTransactionDate').removeClass("InValidTextBox");
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text('Please key in a valid cash drawer.');
    }
    else if ($.trim($('#txtTransactionDate').val()) == '') {
        $('#txtStore').removeClass("InValidTextBox");
        $('#txtCashDrawer').removeClass("InValidTextBox");
        $('#txtTransactionDate').addClass("InValidTextBox");
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text('Please key in a valid transaction date.');
    }
    else {
        isCDBValid = true;

        $('#txtStore').removeClass("InValidTextBox");
        $('#txtCashDrawer').removeClass("InValidTextBox");
        $('#txtTransactionDate').removeClass("InValidTextBox");
    }

    return isCDBValid;
}

function GetCashDrawerData() {

    var objcashDrawer = new Object();
    objcashDrawer.companyCd = GetStoreValue('companyCd');
    objcashDrawer.empCd = $('#hidCashierCd').val();
    objcashDrawer.empInit = $('#ddlCashier option:selected').text();
    objcashDrawer.loggedEmpCd = GetSessionValue('EmpCd');
    objcashDrawer.storeCd = $('#txtStore').val();
    objcashDrawer.cashDrawerCd = $('#txtCashDrawer').val();
    objcashDrawer.transactionDate = $.trim($('#txtTransactionDate').val());
    objcashDrawer.currencyCd = $.trim($("#ddlCurr option:selected").text()) != '' ? $("#ddlCurr option:selected").text() : GetStoreValue('defCurrCd');

    $.ajax({
        url: 'CashDrawerBalancing.aspx/GetCashDrawerData',
        data: JSON.stringify({ cashDrawer: objcashDrawer }),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {

            if ($.trim(msg.d) != '') {

                $('#hidIsDrawerClosed').val(msg.d.closed);
                $('#hidIsDrawerOpen').val(msg.d.isOpen);
                $('#hidCloseBalance').val(msg.d.closeBalance);
                $('#txtOpenBal').val(parseFloat(msg.d.openBalance).toFixed(2));

                // These totals are to be used in GetTranscationBalance, in case where the cash drawer is balanced and closed.
                $('#txtTransCsTotal').val(msg.d.csTransTotalAmt.toFixed(2));
                $('#txtTransCkTotal').val(msg.d.ckTransTotalAmt.toFixed(2));

                $('#hidIsDataRetrived').val('Yes');

                BindCashData(msg.d);

                BindCheckData(msg.d);

                BindOtherData(msg.d.depositOther);

                storeTransData = msg.d.transactionList;
                GetTranscationBalance(msg.d.transactionList, false);

                GetDepositSlipPrintCount(msg.d.printCount);

                EnableControls(msg.d);
            }

            $('.ajax-content').hide();
        },
        error: function (XMLHttpRequest, callStatus, errorThrown) {

            $('.ajax-content').hide();
        }
    });
}

function EnableControls(cdb) {

    var isEnable = false;
    var isClose = false;

    if ($.trim(cdb.closed) != '' && (cdb.closed.toUpperCase() == 'F' || cdb.closed.toUpperCase() == 'Y' || cdb.closed.toUpperCase() == 'S')) {
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text('Cash Drawer has already been closed for transaction date ' + $('#txtTransactionDate').val() + '.');
        isClose = true;
    }
    else if ($.trim(cdb.isOpen) != '' && !cdb.isOpen) {
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text('Cashier not activated for this store ' + cdb.storeCd + ', cash drawer ' + cdb.cashDrawerCd + ' and date ' + $('#txtTransactionDate').val() + '.');
        isClose = true;
    }
    else {
        $('#lblErrMsg').text('');

        if (cdb.printCount < 1) {
            isEnable = true;
            if (null != cdbAccess) {
                cdbAccess.isRePrtAccess = true;
            }
        }
        else {
            if (null != cdbAccess) {
                isEnable = cdbAccess.isDwrUpdAccess;
            }
        }
    }

    $('#txtOpenBal').prop('disabled', !isEnable);
    $('#divCash').find('input[type=text], a, input[type=button], select').prop('disabled', !isEnable);
    $('#chkAddRow').find('input[type=text], a').prop('disabled', !isEnable);
    $('#divAddCheck').find('input[type=text], a').prop('disabled', !isEnable);
    $('#txtDepBag').prop('disabled', !isEnable);
    $('#divCdbDetail').find('a').attr('disabled', !isEnable);
    $('#divCdbDetail').find('a').css('color', isEnable ? '#5a9ddb' : '#c6c6c6');
    $("a[id^='rmvChk']").css('color', isEnable ? 'Red' : '#c6c6c6');
    $('#divSubBtns').find('input[type=button]').prop('disabled', true);
    $('#btnClear').prop('disabled', false);

    if (null != cdbAccess) {
        if (isClose) {
            $('.ShowDetail').attr('disabled', true);
            $('.ShowDetail').css('color', '#c6c6c6');
        } else {
            $('.ShowDetail').attr('disabled', !cdbAccess.isDetailAccess);
            $('.ShowDetail').css('color', cdbAccess.isDetailAccess ? '#5a9ddb' : '#c6c6c6');
        }

        var cashdiff = parseFloat($('#txtTransCsTotal').val()) - parseFloat($('#txtCashTotal').val());
        var chkdiff = parseFloat($('#txtTransCkTotal').val()) - parseFloat($('#txtChkTotal').val());

        if (isClose) {
            $('#btnClose').prop('disabled', true);
        }
        else if (cashdiff == 0 && chkdiff == 0) {
            $('#btnClose').prop('disabled', false);
        }
        else if (null != cdbAccess) {
            $('#btnClose').prop('disabled', !cdbAccess.isCloseAccess);
        }
        else {
            $('#btnClose').prop('disabled', true);
        }

        $('#btnPrintDepSlip').prop('disabled', !cdbAccess.isRePrtAccess);
    }
}

function BindCashier(CashierList) {

    //var loggedEmp = GetSessionValue('EmpCd');
    //var empInit = GetSessionValue('EmpInit');

    $('#ddlCashier').prop('disabled', false);
    $('#ddlCashier').html('');

    $.each(CashierList, function (key, value) {
        $('#ddlCashier').append($("<option></option>").val(value.fullName).html(value.empInit));
    });

    $('#ddlCashier' + ' option').each(function () {
        if ($(this).text() == GetSessionValue('EmpInit')) {
            $(this).attr('selected', 'selected');
        }
    });

    $('#txtCashier').val($('#ddlCashier').val());

    $('#ddlCashier').each(function () {
        $('#txtCashier').val($('#ddlCashier').val());

        if (CashierList != null) {
            var selCashier = $.grep(CashierList, function (n, i) {
                if ($.trim(n.empInit) == $.trim($('#ddlCashier option:selected').text())) {
                    return n;
                }
            });

            $('#hidCashierCd').val(selCashier[0].empCd);
        }
    });

    $('#ddlCashier').change(function () {
        $('#txtCashier').val($('#ddlCashier').val());

        if (cashierList != null) {
            selCashier = $.grep(cashierList, function (n, i) {
                if ($.trim(n.empInit) == $.trim($('#ddlCashier option:selected').text())) {
                    return n;
                }
            });

            $('#hidCashierCd').val(selCashier[0].empCd);
        }
    });
}

function BindCurrency(CurrencyList, defCurrCd) {
    $('#ddlCurr').prop('disabled', false);
    $('#ddlCurr').html('');

    $.each(CurrencyList, function (key, value) {
        $('#ddlCurr').append($("<option></option>").val(value.currencyDesc).html(value.currencyCd));
    });

    if ($.trim(defCurrCd) != '')
        $('#ddlCurr' + ' option').each(function () {
            if ($(this).text() == defCurrCd) {
                $(this).attr('selected', 'selected');
            }
        });

    $('#txtCurrDesc').val($('#ddlCurr').val());

    $('#ddlCurr').each(function () {
        $('#txtCurrDesc').val($('#ddlCurr').val());

        if (CurrencyList && CurrencyList.length > 0) {
            $.each(CurrencyList, function (key, value) {
                if (value.currencyCd == $('#ddlCurr option:selected').text()) {
                    //if ($.trim(value.currencySymbol) != '')
                    //$('.currency').text(value.currencySymbol);
                    $("#hidCurrSymbol").val(value.currencySymbol);
                }
            });
        }
    });

    $('#ddlCurr').change(function () {
        $('#txtCurrDesc').val($('#ddlCurr').val());

        if (CurrencyList && CurrencyList.length > 0) {
            $.each(CurrencyList, function (key, value) {
                if (value.currencyCd == $('#ddlCurr option:selected').text()) {
                    $("#hidCurrSymbol").val(value.currencySymbol);
                }
            });
        }
    });
}

function GetGpParmLabel(e) {
    var lblVal = '';

    $.ajax({
        type: "POST",
        async: false,
        contentType: "application/json; charset=utf-8",
        url: 'CashDrawerBalancing.aspx/GetGpParmLabel',
        data: JSON.stringify({ parm: e }),
        dataType: "json",
        success: function (data) {
            lblVal = data.d;
        }, error: function (result) {
            setColorForFailure('#lblErrMsg');
        }, failure: function (error) {
            setColorForFailure('#lblErrMsg');
            $('#lblErrMsg').text(jQuery.parseJSON(error.responseText).Message);
        }
    });

    return lblVal;
}

function GetSessionValue(e) {
    var sessionData = $('#hiddenSessions').val();
    var sessionValue = '';

    if (sessionData.length > 0) {
        var items = sessionData.slice(0, -1).split(';');
        $.each(items, function (Index, value) {
            if (value.indexOf(e + ':') == 0) {
                sessionValue = value.split(':')[1];
            }
        });
    }
    return sessionValue;
}

function GetStoreValue(e) {
    var sessionData = $('#hidStoreDtls').val();
    var sessionValue = '';

    if ($.trim(sessionData) != '') {
        var items = sessionData.slice(0, -1).split(';');
        $.each(items, function (Index, value) {
            if (value.indexOf(e + ':') == 0) {
                sessionValue = value.split(':')[1];
            }
        });
    }
    return sessionValue;
}

function GetDepositSlip(e) {

    $('#hidDepositSlipNo').val(e);

    if (e < 1)
        e++;

    var dt = new Date($('#txtTransactionDate').val());

    var depositSlip = (dt.getMonth() < 10 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1) + '') +
        (dt.getDate() < 10 ? '0' + dt.getDate() : dt.getDate()) + (dt.getFullYear() + '').charAt(3) +
        $('#txtStore').val() + $('#txtCashDrawer').val() + ($.trim(e).length < 2 ? '0' + e : e) + '-' +
        ($.trim($("#ddlCurr option:selected").text()) != '' ? $("#ddlCurr option:selected").text() : GetStoreValue('defCurrCd'));

    return depositSlip;
}

function BindCashData(cdb) {

    var cash = cdb.depositCash;

    $('#divCash').find('input[type=text]').val('');

    $('#txtCashTotal').val('0.00');

    $('#txtDepSlip').val('');
    $('#txtDepBag').val('');

    $('#hidIsDataRetrived').val('No');
    $('#hidIsInsert').val('Yes');

    var totalCash = 0;

    if (null != cash) {

        //if (!($('#hidIsUpdateEnabled').val() == "" || $('#hidIsUpdateEnabled').val() == "false") && $('#hidIsDrawerOpen').val() == "true") {
        //    $('#divCash').find('input[type=text],a').prop('disabled', false);
        //    $('#txtOpenBal').prop('disabled', false);
        //    $('#txtDepBag').prop('disabled', false);
        //}

        //if ($('#hidIsDrawerOpen').val() == "true")
        //    HasSecurityAccess('CDB_DETAIL', '#cashTransDtl', 'CS');

        //$('#hidIsDataRetrived').val('Yes');

        $('#txtCash1').val($.trim(cash.oneCount) != '0' ? cash.oneCount : '');
        $('#txtCash2').val($.trim(cash.twoCount) != '0' ? cash.twoCount : '');
        $('#txtCash5').val($.trim(cash.fiveCount) != '0' ? cash.fiveCount : '');
        $('#txtCash10').val($.trim(cash.tenCount) != '0' ? cash.tenCount : '');
        $('#txtCash20').val($.trim(cash.twentyCount) != '0' ? cash.twentyCount : '');
        $('#txtCash50').val($.trim(cash.fiftyCount) != '0' ? cash.fiftyCount : '');
        $('#txtCash100').val($.trim(cash.oneHundredCount) != '0' ? cash.oneHundredCount : '');
        $('#txtCash1000').val($.trim(cash.oneThousandCount) != '0' ? cash.oneThousandCount : '');
        $('#txtPennies').val($.trim(cash.penniesCount) != '0' ? cash.penniesCount : '');
        $('#txtNickels').val($.trim(cash.nickelsCount) != '0' ? cash.nickelsCount : '');
        $('#txtDimes').val($.trim(cash.dimesCount) != '0' ? cash.dimesCount : '');
        $('#txtQuarters').val($.trim(cash.quartersCount) != '0' ? cash.quartersCount : '');
        $('#txtHalfDlr').val($.trim(cash.halfCount) != '0' ? cash.halfCount : '');
        $('#txtDlrCoin').val($.trim(cash.oneCoinCount) != '0' ? cash.oneCoinCount : '');

        if ($.trim($(cash.currencyCd).val()) != '') {
            $('#ddlCurr' + ' option').each(function () {
                if ($(this).text() == cash.currencyCd) {
                    $(this).attr('selected', 'selected');
                }
            });

            $('#txtCurrDesc').val($('#ddlCurr').val());
        }

        $('#txtDepSlip').val(GetDepositSlip(cash.depositSlip));
        $('#txtDepBag').val(cash.depositBag);

        totalCash = parseFloat(cash.oneCount) * 1 + parseFloat(cash.twoCount) * 2 + parseFloat(cash.fiveCount) * 5 + parseFloat(cash.tenCount) * 10;
        totalCash += parseFloat(cash.twentyCount) * 20 + parseFloat(cash.fiftyCount) * 50 + parseFloat(cash.oneHundredCount) * 100 + parseFloat(cash.oneThousandCount) * 1000;
        totalCash += parseFloat(cash.penniesCount) * 0.01 + parseFloat(cash.nickelsCount) * 0.05 + parseFloat(cash.dimesCount) * 0.1;
        totalCash += parseFloat(cash.quartersCount) * 0.25 + parseFloat(cash.halfCount) * 0.5 + parseFloat(cash.oneCoinCount) * 1;

        $('#txtCashTotal').val(parseFloat(totalCash).toFixed(2));

        $('#hidIsInsert').val('No');
    }

    if (parseFloat($('#txtCashTotal').val()) == parseFloat($('#txtTransCsTotal').val())) {

        cdb.balanced = true;
        if (null != cdbAccess) {
            $('#btnBalance').prop('disabled', !cdbAccess.isBalAccess);
        }
        else {
            $('#btnBalance').prop('disabled', false);
        }
    }
    else {
        cdb.balanced = false;
    }

    if (null != cdbAccess) {
        $('#cashTransDtl').prop('disabled', !cdbAccess.isDetailAccess);
    }

    $('#cashTransDtl').click(function () {

        $('#divTransDetail').empty();
        LoadDetailPopup('CS');
    });

}

function BindCheckData(cdb) {

    var check = cdb.depositCk;

    $('#divAddCheck').empty();

    $('#scrollChkData').mCustomScrollbar("disable", true);

    $('#txtChkAmt').val('');
    $('#txtChkName').val('');

    $('#hidCheckData').val('');

    $('#txtChkCnt').val('0');
    $('#txtChkTotal').val('0.00');

    //if (($('#hidIsUpdateEnabled').val() == "" || $('#hidIsUpdateEnabled').val() == "false") && $('#hidIsDrawerOpen').val() == "true") {
    //    $('#chkAddRow').find('input[type=text],a').prop('disabled', false);
    //    $('#divAddCheck').find('input[type=text]').prop('disabled', false);
    //}
    //else {
    //    $('#chkAddRow').find('input[type=text],a').prop('disabled', true);
    //    $('#divAddCheck').find('input[type=text]').prop('disabled', true);
    //}

    //if ($('#hidIsDrawerOpen').val() == "true")
    //    HasSecurityAccess('CDB_DETAIL', '#checkTransDtl', 'CK');

    if (null != check) {
        var idx = 0;
        var amtTotal = 0;
        var htmltag = '';
        var chkHiddenData = '';

        $.each(check, function (key, value) {
            idx++;

            amtTotal += parseFloat(value.checkAmount);

            var checkName = $.trim(value.checkName) != '' ? $.trim(value.checkName) != 'null' ? value.checkName : '' : '';

            htmltag += "<div id='divAddChk" + value.seqenceNo + "' style='display: table;  width: 94%;'>";
            htmltag += "<div style='display: table-cell; width: 6px;'>";
            htmltag += "<a id='rmvChk" + value.seqenceNo + "' style='color: red;' title='Remove this Check'>X</a>";
            htmltag += "</div><div style='display: table-cell;'>";
            htmltag += "<input type='text' id='txtChkName" + value.seqenceNo + "' value='" + checkName + "' maxlength='30' />";  //MM-11836
            htmltag += "</div><div style='display: table-cell;'><input type='text' id='txtChkAmt" + value.seqenceNo + "' class='numeric' ";
            htmltag += "style='text-align: right;' value='" + parseFloat(value.checkAmount).toFixed(2) + "' ";
            htmltag += "onkeyup='CheckTotal();' onchange='CheckTotal();' onblur='ValidateEmptyText(this);' />";
            htmltag += "</div></div>";

            chkHiddenData = value.seqenceNo + ':' + value.checkName + ':' + parseFloat(value.checkAmount).toFixed(2) + ';' + chkHiddenData;
        });

        $('#divAddCheck').prepend($(htmltag));

        $.each($('#divAddCheck a'), function (idx, ctrl) {
            var a = ctrl.id.replace('rmvChk', '');
            $(ctrl).click(function () {
                RemoveCheck(a);
            });
        });

        $('.numeric').numeric();

        $('#txtChkCnt').val(idx);
        $('#txtChkTotal').val(parseFloat(amtTotal).toFixed(2));

        $('#hidCheckData').val(chkHiddenData);

        $('#scrollChkData').mCustomScrollbar({
            scrollButtons: {
                enable: true
            },
            theme: "dark-thick",
            advanced: {
                updateOnContentResize: true
            }
        });

        $("#chkData :text").keyup(function () {
            CheckTotal();
        });

        $("#chkData :text").change(function () {
            CheckTotal();
        });
    }

    if (null != cdbAccess) {
        $('#checkTransDtl').prop('disabled', !cdbAccess.isDetailAccess);
    }

    $('#checkTransDtl').click(function () {
        $('#divTransDetail').empty();
        LoadDetailPopup('CK');
    });
}

function BindOtherData(other) {

    $('#divAddOther').empty();

    $('#othorMopScroll').mCustomScrollbar("disable", true);

    $('#txtOtherCount').val('0');
    $('#txtOthrTotal').val('0.00');

    var idx = 0;
    var amtTotal = 0;
    var htmltag = '';

    if (null != other) {
        $.each(other, function (key, value) {
            idx++;
            amtTotal += parseFloat(value.amount);

            htmltag += "<div id='divAddOthr" + idx + "' style='display: table;  width: 94%;'>";
            htmltag += "<div style='display: table-cell; width: 24%;'>";
            htmltag += "<a id='mop" + idx + "#" + value.mopCd + "' title='" + value.mopCd + "'>" + value.mopCd + "</a>";
            htmltag += "</div><div style='display: table-cell; width: 40%;'>";
            htmltag += "<a id='mopDesc" + idx + "#" + value.mopCd + "' title='" + value.mopDesc + "'>" + value.mopDesc + "</a>";
            htmltag += "</div><div style='display: table-cell; width: 30%; text-align: right;'>";
            htmltag += "<a id='mopAmt" + idx + "#" + value.mopCd + "' title='" + parseFloat(value.amount).toFixed(2) + "'>" + parseFloat(value.amount).toFixed(2) + "</a>";
            htmltag += "</div></div>";
        });

        $('#divAddOther').prepend($(htmltag));

        $.each($('#divAddOther').find('div'), function (idx, ctrl) {

            if (ctrl.id.indexOf('divAddOthr') >= 0) {

                //$('#' + ctrl.id + ' a').each(function (index, obj) {
                $(ctrl).find('a').each(function (index, obj) {
                    if (obj.id.indexOf('mop') >= 0) {
                        //if ($('#hidIsDrawerOpen').val() == "true") {

                        if (null != cdbAccess) {
                            $('#' + obj.id).prop('disabled', !cdbAccess.isDetailAccess);
                        }

                        $(obj).click(function () {
                            var selectedMop = obj.id.substr(obj.id.indexOf('#') + 1);

                            LoadDetailPopup(selectedMop);

                            GetTranscationBalance(storeTransData, false);
                        });
                        //}
                    }
                });
            }
        });

        //if ($('#hidIsDrawerOpen').val() == "true")
        //    HasSecurityAccess('CDB_DETAIL', '#checkOther', '');

        $('#txtOtherCount').val(idx);
        $('#txtOthrTotal').val(parseFloat(amtTotal).toFixed(2));

        $('#othorMopScroll').mCustomScrollbar({
            scrollButtons: {
                enable: true
            },
            theme: "dark-thick",
            advanced: {
                updateOnContentResize: true
            }
        });
    }

    if (null != cdbAccess) {
        $('#checkOther').prop('disabled', !cdbAccess.isDetailAccess);
    }

    $('#checkOther').click(function () {
        $('#divTransDetail').empty();
        LoadDetailPopup('');
    });
}

function GetTranscationBalance(transcationData, isBalance) {

    if (null != transcationData) {

        var totalTarnsCash = 0;
        var totalTarnsCk = 0;

        if (transcationData.length > 0) {
            // In case where the Cash drawer is not closed
            $.each(transcationData, function (key, value) {

                if (value.mopTp != null) {
                    if (value.mopTp == 'CS') {
                        totalTarnsCash = parseFloat($.trim(value.mopAmount) != '' ? value.mopAmount : 0);
                    }

                    if (value.mopTp == 'CK') {
                        totalTarnsCk = parseFloat($.trim(value.mopAmount) != '' ? value.mopAmount : 0);
                    }
                }
            });
        }
        else {
            // In case the cash drawer is balanced and closed
            totalTarnsCash = $.trim($('#txtTransCsTotal').val()) != '' ? parseFloat($('#txtTransCsTotal').val()) : 0;
            totalTarnsCk = $.trim($('#txtTransCkTotal').val()) != '' ? parseFloat($('#txtTransCkTotal').val()) : 0;
        }

        var openBal = $.trim($('#txtOpenBal').val()) != '' ? parseFloat($('#txtOpenBal').val()) : 0;

        var totalCash = $.trim($('#txtCashTotal').val()) != '' ? parseFloat($('#txtCashTotal').val()) : 0;
        var totalCheck = $.trim($('#txtChkTotal').val()) != '' ? parseFloat($('#txtChkTotal').val()) : 0;

        var cashOverShort = (totalCash - openBal - totalTarnsCash).toFixed(2);
        var checkOverShort = (totalCheck - totalTarnsCk).toFixed(2);

        $('#hidTransCsTotal').val(totalTarnsCash.toFixed(2));
        $('#hidTransCkTotal').val(totalTarnsCk.toFixed(2));

        $('#hidCashOvrShrt').val(cashOverShort);
        $('#hidCheckOverShrt').val(checkOverShort);

        $('#txtCashOvrShrt').val(cashOverShort);
        $('#txtCheckOverShrt').val(checkOverShort);

        if (isBalance == true) {
            if (cashOverShort == 0 && checkOverShort == 0) {
                setColorForSuccess('#lblErrMsg');

                $('#lblErrMsg').text('Cash drawer balanced successfully.');

                $(this).prop('disabled', true);

                $('#btnClose').prop('disabled', false);
            }
            else {

                setColorForFailure('#lblErrMsg');
                $('#lblErrMsg').text('Cash drawer is not balanced due to mismatch in totals.');

                $(this).prop('disabled', false);

                //$('#hidIsDwrCloseAccess').val(data.d[0].isDwrCloseAccess);
                if (null != cdbAccess) {
                    $('#btnClose').prop('disabled', !cdbAccess.isCloseAccess);
                }
            }
        }
    }
}

function GetDepositSlipPrintCount(printCount) {

    if (parseInt(printCount, 10) > 0)
        $('#lblIsPrnt').text('Yes');
    else
        $('#lblIsPrnt').text('No');

    $('#lblPrintSlipCnt').text(printCount);
}

function LoadDetailPopup(e) {

    $('#popTransDetail').bPopup({
        modalClose: false
    },
        function () {
            $('#hidMopTrans').val(e);

            LoadTransDetails(e);

            $('#aCloseCashDetail').click(function () {
                $('#popTransDetail').bPopup().close();
            });

            $('#btnReturnTrn').click(function () {
                $('#popTransDetail').bPopup().close();
            });
        });
}


function LoadTransDetails(e) {

    $('.ajax-content').show();

    switch (e) {
        case 'CS':
            $('#divTransDtlHeader').html('Detail Cash Transactions');
            break;
        case 'CK':
            $('#divTransDtlHeader').html('Detail ' + GetGpParmLabel('CDB_CHECK_LABEL') + ' Transactions');
            break;
        default:
            $('#divTransDtlHeader').html('Detail Other Transactions');
            break;
    }

    var rowCnt = 0;
    var chkTrTotal = 0;

    $('#divTransDetail').empty();
    $('#divCsTransDtl').mCustomScrollbar("disable", true);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: 'CashDrawerBalancing.aspx/GetTransactionDetail',
        data: JSON.stringify({ coCd: GetStoreValue('companyCd'), storeCd: $('#txtStore').val(), CashDrawerCd: $('#txtCashDrawer').val(), transDate: $.trim($('#txtTransactionDate').val()), mop: e }),
        dataType: "json",
        success: function (data) {
            var idx = 0;
            var htmltag = "";
            $.each(data.d, function (key, value) {
                idx++;
                var dt = new Date(parseInt(value.postDate.substr(6)));
                dt = (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear();

                htmltag += "<table style='width: 94%;'>";
                htmltag += "<div id='divTrans" + idx + "' style='display: table;'>";
                htmltag += "<div style='display: table-cell; width: 50px;'>";
                htmltag += "<input type='hidden' id='transPk" + idx + "' value='" + value.tranPK + "' />";
                htmltag += "<input type='text' id='txtTransMop" + idx + "' value='" + value.mopCd + "' class='disable' />";
                htmltag += "</div><div style='display: table-cell; width: 100px; padding-left: 3px;'>";
                htmltag += "<input type='text' id='txtTransInvoice" + idx + "' value='" + value.invoice + "' class='disable' />";
                htmltag += "</div><div style='display: table-cell; width: 100px;'>";
                htmltag += "<input type='text' id='txtTransPostDt" + idx + "' value='" + dt + "' class='disable' />";
                htmltag += "</div><div style='display: table-cell; width: 50px;'>";
                htmltag += "<input type='text' id='txtTransTrn" + idx + "' style='width: 49px;' value='" + value.tranTpCd + "' class='disable' />";
                htmltag += "</div><div style='display: table-cell; width: 50px;'>";
                htmltag += "<input type='text' id='txtTransDc" + idx + "' style='width: 49px;' value='" + value.dcCd + "' class='disable' />";
                htmltag += "</div><div style='display: table-cell; width: 100px;'>";
                htmltag += "<input type='text' id='txtTranAmt" + idx + "' style='text-align: right;' ";
                htmltag += "value='" + parseFloat(value.amount).toFixed(2) + "' class='numeric' onkeyup='TransTotal();' />";
                htmltag += "</div></div></table>";

                if (value.dcCd == 'C')
                    chkTrTotal += parseFloat(value.amount);
                else
                    chkTrTotal = chkTrTotal - parseFloat(value.amount);
            });

            $('#txtTransCount').val(idx);

            $('#divTransDetail').prepend($(htmltag));

            $('.numeric').numeric();
            $('#txtTransTotal').val(chkTrTotal.toFixed(2));

            $('.disable').prop('disabled', true);

            if (null != cdbAccess) {
                $("input[id^='txtTranAmt']").prop('disabled', !cdbAccess.isUpdateAccess);
            }

            $('#divCsTransDtl').mCustomScrollbar({
                scrollButtons: {
                    enable: true
                },
                theme: "dark-thick",
                advanced: {
                    updateOnContentResize: true
                }
            });

            $('.ajax-content').hide();

        }, error: function (result) {
            $('.ajax-content').hide();
        }, failure: function (error) {
            setColorForFailure('#lblErrMsg');
            $('#lblErrMsg').text(jQuery.parseJSON(error.responseText).Message);
            $('.ajax-content').hide();
        }
    });
}

function SaveCDB() {
    $('.ajax-content').show();

    //SaveOpenBalance();
    var objCashDrawer = new Object();
    objCashDrawer.companyCd = GetStoreValue('companyCd');
    objCashDrawer.empCd = $('#hidCashierCd').val();
    objCashDrawer.empInit = $('#ddlCashier option:selected').text();
    objCashDrawer.loggedEmpCd = GetSessionValue('EmpCd');
    objCashDrawer.storeCd = $('#txtStore').val();
    objCashDrawer.cashDrawerCd = $('#txtCashDrawer').val();
    objCashDrawer.transactionDate = $.trim($('#txtTransactionDate').val());
    objCashDrawer.currencyCd = $.trim($("#ddlCurr option:selected").text()) != '' ? $("#ddlCurr option:selected").text() : GetStoreValue('defCurrCd');
    objCashDrawer.openBalance = parseFloat($.trim($('#txtOpenBal').val()) != '' ? $('#txtOpenBal').val() : 0);

    if (parseFloat($('#txtCashOvrShrt').val()) == 0 && parseFloat($('#txtCheckOverShrt').val()) == 0) {
        objCashDrawer.balanced = 'Y';
        objCashDrawer.closed = 'Y';
    }
    else {
        objCashDrawer.balanced = 'N';
        objCashDrawer.closed = 'F';
    }

    objCashDrawer.closeBalance = parseFloat($.trim($('#hidCloseBalance').val()) != '' ? $('#hidCloseBalance').val() : 0);
    objCashDrawer.transBal = 0;
    objCashDrawer.mopCd = '';
    objCashDrawer.mopAmount = 0;
    objCashDrawer.overShortAmt = 0;
    objCashDrawer.isOpen = $('#hidIsDrawerOpen').val();

    var objCash = new Object();

    objCash.oneCount = parseInt($.trim($('#txtCash1').val()) != '' ? $('#txtCash1').val() : 0, 10);
    objCash.twoCount = $.trim($('#txtCash2').val()) != '' ? parseInt($('#txtCash2').val(), 10) : 0;
    objCash.fiveCount = $.trim($('#txtCash5').val()) != '' ? parseInt($('#txtCash5').val(), 10) : 0;
    objCash.tenCount = $.trim($('#txtCash10').val()) != '' ? parseInt($('#txtCash10').val(), 10) : 0;
    objCash.twentyCount = $.trim($('#txtCash20').val()) != '' ? parseInt($('#txtCash20').val(), 10) : 0;
    objCash.fiftyCount = $.trim($('#txtCash50').val()) != '' ? parseInt($('#txtCash50').val(), 10) : 0;
    objCash.oneHundredCount = $.trim($('#txtCash100').val()) != '' ? parseInt($('#txtCash100').val(), 10) : 0;
    objCash.oneThousandCount = $.trim($('#txtCash1000').val()) != '' ? parseInt($('#txtCash1000').val(), 10) : 0;
    objCash.penniesCount = $.trim($('#txtPennies').val()) != '' ? parseInt($('#txtPennies').val(), 10) : 0;
    objCash.nickelsCount = $.trim($('#txtNickels').val()) != '' ? parseInt($('#txtNickels').val(), 10) : 0;
    objCash.dimesCount = $.trim($('#txtDimes').val()) != '' ? parseInt($('#txtDimes').val(), 10) : 0;
    objCash.quartersCount = $.trim($('#txtQuarters').val()) != '' ? parseInt($('#txtQuarters').val(), 10) : 0;
    objCash.halfCount = $.trim($('#txtHalfDlr').val()) != '' ? parseInt($('#txtHalfDlr').val(), 10) : 0;
    objCash.oneCoinCount = $.trim($('#txtDlrCoin').val()) != '' ? parseInt($('#txtDlrCoin').val(), 10) : 0;
    //objCash.depositSlip = $('#txtDepSlip').val();
    objCash.depositBag = $('#txtDepBag').val();

    objCashDrawer.depositCash = objCash;

    GetCashDrawerCheckData(objCashDrawer);

    $.ajax({
        type: 'POST',
        url: 'CashDrawerBalancing.aspx/SaveCDB',
        data: JSON.stringify({ objCdbBal: objCashDrawer }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {

            $('#btnSave').prop('disabled', true);
            $('#txtDepSlip').val(GetDepositSlip(data.d.depositCash.depositSlip));

            if (null != cdbAccess) {
                $('#btnBalance').prop('disabled', !cdbAccess.isBalAccess);
            }
            else {
                $('#btnBalance').prop('disabled', false);
            }

            var cashdiff = parseFloat($('#txtTransCsTotal').val()) - parseFloat($('#txtCashTotal').val());
            var chkdiff = parseFloat($('#txtTransCkTotal').val()) - parseFloat($('#txtChkTotal').val());

            if (cashdiff == 0 && chkdiff == 0) {
                $('#btnClose').prop('disabled', false);
            }
            else if (null != cdbAccess) {
                $('#btnClose').prop('disabled', !cdbAccess.isCloseAccess);
            }
            else {
                $('#btnClose').prop('disabled', true);
            }

            var bankCd = GetStoreValue('bankCd');
            var bankAccCd = GetStoreValue('bankAccCd');

            if (bankCd == "" && bankAccCd == "")
                $('#btnPrintDepSlip').prop('disabled', true);
            else {
                $('#btnPrintDepSlip').prop('disabled', false);
            }

            $('#txtChkAmt').val('');
            $('#txtChkName').val('');


            setColorForSuccess('#lblErrMsg');
            $('#lblErrMsg').text('Data saved successfully.');

            $('.ajax-content').hide();

        }, failure: function (error) {
            setColorForFailure('#lblErrMsg');
            $('#lblErrMsg').text(jQuery.parseJSON(error.responseText).Message);

            $('.ajax-content').hide();
        }
    }).fail(function (error) {
        var r = jQuery.parseJSON(error.responseText);
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text(r.Message);

        $('.ajax-content').hide();
    });

    //SaveCdbCash();

    //SaveCdbCheck();

}

function GetCashDrawerCheckData(objCashDrawer) {

    objCashDrawer.depositCk = new Array();

    $.each($('#divAddCheck').find('div'), function (idx, ctrl) {
        var isItemFound = false;

        if (ctrl.id.indexOf('divAddChk') >= 0) {

            objCdbCk = new Object();

            $('#' + ctrl.id + ' input[type="text"]').each(function (index, obj) {
                isItemFound = true;

                objCdbCk.seqenceNo = ctrl.id.substr('divAddChk'.length, ctrl.id.length - 1);
                if (obj.id.indexOf('txtChkAmt') >= 0) {
                    objCdbCk.checkAmount = parseFloat($(this).val());
                }

                if (obj.id.indexOf('txtChkName') >= 0) {
                    objCdbCk.checkName = $(this).val();
                }

            });
            if (isItemFound == true)
                objCashDrawer.depositCk.push(objCdbCk);
        }
    });

    if (objCashDrawer.depositCk.length == 0) {
        objCdbCk = new Object();
        objCdbCk.seqenceNo = -1;
        objCashDrawer.depositCk.push(objCdbCk);
    }
}

function CashTotal() {
    var totalCash = 0;

    $("#divCash :text").each(function () {
        var a = $(this).attr('id').match(/[\d]+$/);

        switch ($(this).attr('id')) {
            case 'txtPennies':
                totalCash += $(this).val().length > 0 ? parseFloat($(this).val() * 0.01) : 0;
                break;
            case 'txtNickels':
                totalCash += $(this).val().length > 0 ? parseFloat($(this).val() * 0.05) : 0;
                break;
            case 'txtDimes':
                totalCash += $(this).val().length > 0 ? parseFloat($(this).val() * 0.1) : 0;
                break;
            case 'txtQuarters':
                totalCash += $(this).val().length > 0 ? parseFloat($(this).val() * 0.25) : 0;
                break;
            case 'txtHalfDlr':
                totalCash += $(this).val().length > 0 ? parseFloat($(this).val() * 0.5) : 0;
                break;
            case 'txtDlrCoin':
                totalCash += $(this).val().length > 0 ? parseFloat($(this).val()) : 0;
                break;
            default:
                totalCash += $(this).val().length > 0 ? parseFloat($(this).val()) * (a != null ? a : 0) : 0;
                break;
        }
    });

    $('#txtCashTotal').val(parseFloat(totalCash).toFixed(2));
}

function CheckTotal() {
    var totalChkAmt = 0;
    var chkHiddenData = $('#hidCheckData').val();
    $('#btnSave').prop('disabled', false);
    $('#btnBalance').prop('disabled', true);
    $('#btnClose').prop('disabled', true);

    $("#chkData :text").each(function () {
        if ($(this).attr('id').indexOf('txtChkAmt') == 0) {
            totalChkAmt += $.trim($(this).val()) != '' ? parseFloat($(this).val()) : 0;

            if ($(this).attr('id').match(/[\d]+$/)) {
                var idx = $(this).attr('id').match(/[\d]+$/);
                if ($.trim(chkHiddenData) != '') {
                    var rowdata = chkHiddenData.slice(0, -1).split(';');

                    var checkName = $('#txtChkName' + idx).val();
                    var checkAmt = $('#txtChkAmt' + idx).val()

                    $.each(rowdata, function (Index, value) {
                        if (value.indexOf(idx + ':') == 0) {
                            chkHiddenData = chkHiddenData.replace(value, idx + ':' + checkName + ':' + checkAmt);
                        }
                    });
                }
            }
        }
    });

    $('#txtChkTotal').val(parseFloat(totalChkAmt).toFixed(2));

    $('#hidCheckData').val(chkHiddenData);
}

function AddNewCheckRow() {
    if ($.trim($('#txtChkAmt').val()) == '') {
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text('Please key in a valid check amount.');
        $('#txtChkAmt').addClass('InValidTextBox');
        $('#txtChkAmt').focus();
    }
    else {
        $('#lblErrMsg').text('');
        $('#txtChkAmt').removeClass('InValidTextBox');
        $('#btnSave').prop('disabled', false);
        $('#btnBalance').prop('disabled', true);
        $('#btnClose').prop('disabled', true);

        var chkHiddenData = $('#hidCheckData').val();
        var idx = ($.trim(chkHiddenData) != '' ? parseInt(chkHiddenData.split(';')[0].split(':')[0], 10) + 1 : 0);

        var htmltag = "<div id='divAddChk" + idx + "' style='display: table;  width: 94%;'>";
        htmltag += "<div style='display: table-cell; width: 6px;'>";
        htmltag += "<a id='rmvChk" + idx + "' style='color: red;' title='Remove this Check'>X</a>";
        htmltag += "</div><div style='display: table-cell;'>";
        htmltag += "<input type='text' id='txtChkName" + idx + "' value='" + $('#txtChkName').val() + "' maxlength='30' />";
        htmltag += "</div><div style='display: table-cell;'><input type='text' id='txtChkAmt" + idx + "' class='numeric' ";
        htmltag += "style='text-align: right;' value='" + parseFloat($('#txtChkAmt').val()).toFixed(2) + "'";
        htmltag += "onkeyup='CheckTotal();' onchange='CheckTotal();' onblur='ValidateEmptyText(this);' />";
        htmltag += "</div></div>";

        //$('#divAddCheck').empty();

        $('#divAddCheck').prepend($(htmltag));

        $.each($('#divAddCheck a'), function (idx, ctrl) {
            var a = ctrl.id.replace('rmvChk', '');
            $(ctrl).click(function () {
                RemoveCheck(a);
            });
        });

        $('.numeric').numeric();

        $('#txtChkCnt').val(parseInt($('#txtChkCnt').val().length > 0 ? $('#txtChkCnt').val() : 0, 10) + 1);

        chkHiddenData = idx + ':' + $('#txtChkName').val() + ':' + parseFloat($('#txtChkAmt').val()).toFixed(2) + ';' + chkHiddenData;
        $('#hidCheckData').val(chkHiddenData);

        $('#txtChkName').val('');
        $('#txtChkAmt').val('');

        $('#txtChkName').focus();
    }
}

function RemoveCheck(idx) {
    //if (($('#hidIsUpdateEnabled').val() == "" || $('#hidIsUpdateEnabled').val() == "false") && $('#hidIsDrawerOpen').val() == "true") {
    var divRowAddChk = '#divAddChk' + idx;
    $(divRowAddChk).empty();
    $('#btnSave').prop('disabled', false);
    $('#btnBalance').prop('disabled', true);
    $('#btnClose').prop('disabled', true);

    $('#txtChkCnt').val(parseInt($('#txtChkCnt').val().length > 0 ? $('#txtChkCnt').val() : 0, 10) - 1);

    var totalChk = parseFloat($('#txtChkTotal').val());
    var chkHiddenData = $('#hidCheckData').val();

    if ($.trim(chkHiddenData) != '') {
        var rowdata = chkHiddenData.slice(0, -1).split(';');
        $.each(rowdata, function (Index, value) {
            if (value.indexOf(idx + ':') == 0) {
                totalChk -= value.split(':')[2].length > 0 ? parseFloat(value.split(':')[2]) : 0;
                chkHiddenData = chkHiddenData.replace(value + ';', '');
            }
        });
    }

    $('#txtChkTotal').val(parseFloat(totalChk).toFixed(2));
    $('#hidCheckData').val(chkHiddenData);
    $(divRowAddChk).remove();  //MM-11836
    //}
}

function TransTotal() {
    var totalAmt = 0;

    $.each($('#divTransDetail').find('input[type=text]'), function (Index, value) {

        if (value.id.indexOf('txtTranAmt') >= 0) {
            if ($('#' + value.id.replace('txtTranAmt', 'txtTransDc')).val() == 'C')
                totalAmt += parseFloat($(this).val());
            else
                totalAmt = totalAmt - parseFloat($(this).val());
        }
    });

    $('#txtTransTotal').val(totalAmt.toFixed(2));
}

function CloseDrawer() {

    var isDrawerClose = $('#hidIsDrawerClosed').val();

    if (isDrawerClose.toUpperCase() == 'F' || isDrawerClose.toUpperCase() == 'Y' || isDrawerClose.toUpperCase() == 'S') {
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text('Cash Drawer has already been closed for transaction date ' + $('#txtTransactionDate').val() + '.');
        $('.ajax-content').hide();
    }
    else {

        var objCdb = new Object();

        objCdb.companyCd = GetStoreValue('companyCd');
        objCdb.storeCd = $('#txtStore').val();
        objCdb.cashDrawerCd = $('#txtCashDrawer').val();
        objCdb.transactionDate = $.trim($('#txtTransactionDate').val());
        objCdb.empCd = GetSessionValue('EmpCd');

        if (parseFloat($('#txtCashOvrShrt').val()) == 0 && parseFloat($('#txtCheckOverShrt').val()) == 0) {
            objCdb.closed = 'Y';
            objCdb.balanced = 'Y';
        }
        else {
            objCdb.closed = 'F';
            objCdb.balanced = 'N';
        }

        var cashTotal = parseFloat($('#txtCashTotal').val());
        var cashOvrSht = parseFloat($('#txtCashOvrShrt').val());

        var checkTotal = parseFloat($('#txtChkTotal').val());
        var checkOvrSht = parseFloat($('#txtCheckOverShrt').val());

        $.ajax({
            type: 'POST',
            url: 'CashDrawerBalancing.aspx/CloseCashDrawer',
            data: JSON.stringify({ objCashDrawerBal: objCdb, cashTotal: cashTotal, cashOvrSht: cashOvrSht, checkTotal: checkTotal, checkOvrSht: checkOvrSht }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                $('#hidIsDrawerClosed').val('Y');
                $('#btnClose').prop('disabled', true);
                setColorForSuccess('#lblErrMsg');
                $('#lblErrMsg').text('The cash drawer closed successfully.');
                $('.ajax-content').hide();
            }, failure: function (error) {
                setColorForFailure('#lblErrMsg');
                $('#lblErrMsg').text(jQuery.parseJSON(error.responseText).Message);
                $('.ajax-content').hide();
            }
        }).fail(function (error) {
            var r = jQuery.parseJSON(error.responseText);
            setColorForFailure('#lblErrMsg');
            $('#lblErrMsg').text(r.Message);
            $('.ajax-content').hide();
        });
    }
}

function PrintDepositSlip() {

    $('.ajax-content').show();

    $('#popDepositSlip').bPopup({
        modalClose: false
    },
        function () {
            $('.ajax-content').show();

            ClearAllDepositSlipData();

            BindPrintDepositSlipData();

            $('#imgDepositSlipClose').click(function () {
                $('#popDepositSlip').bPopup().close();
            });

            $('#btnPrintDepositSlip').unbind('click');

            $('#btnPrintDepositSlip').click(function () {

                SavePrintData();

                var isEnable = false;

                if (null != cdbAccess) {
                    isEnable = cdbAccess.isDwrUpdAccess;
                }

                $('#txtOpenBal').prop('disabled', !isEnable);
                $('#divCash').find('input[type=text], a, input[type=button], select').prop('disabled', !isEnable);
                $('#chkAddRow').find('input[type=text], a').prop('disabled', !isEnable);
                $('#divAddCheck').find('input[type=text], a').prop('disabled', !isEnable);
                $('#txtDepBag').prop('disabled', !isEnable);
                $('#divCdbDetail').find('a').attr('disabled', !isEnable);
                $('#divCdbDetail').find('a').css('color', isEnable ? '#5a9ddb' : '#c6c6c6');
                $("a[id^='rmvChk']").css('color', isEnable ? 'Red' : '#c6c6c6');
                $('#divSubBtns').find('input[type=button]').prop('disabled', true);
                $('#btnClear').prop('disabled', false);

                if (null != cdbAccess) {
                    $('#btnPrintDepSlip').prop('disabled', !cdbAccess.isRePrtAccess);
                }

                $("#divPrintDepositSlip").printThis({
                    debug: false,              // show the iframe for debugging
                    importCSS: true,           // import page CSS
                    printContainer: true,      // grab outer container as well as the contents of the selector
                    loadCSS: "Styles/POSStyleSheet.css", // path to additional css file
                    pageTitle: "Deposit Slip",             // add title to print page
                    removeInline: false,       // remove all inline styles from print elements
                    printDelay: 333,           // variable print delay S. Vance
                    header: null               // prefix to html
                });

                $('#popDepositSlip').bPopup().close();
                return false;
            });
        });
}

function ValidateEmptyText(e) {
    if ($.trim($(e).val()) == '') {
        $(e).addClass("InValidTextBox");
        $(e).focus();
        return false;
    }
    else {
        $(e).removeClass("InValidTextBox");
        return true;
    }
}

function setColorForSuccess(id) {
    $(id).removeClass("failureMsg");
    $(id).addClass("successMsg");
}

function setColorForFailure(id) {
    $(id).removeClass("successMsg");
    $(id).addClass("failureMsg");
}

function ClearAllDepositSlipData() {

    $('#lblDepositSlipTitle').text('');

    $('.divDpSlipTemp').empty();

    $('#lblPdCkDate').text('');
    $('#lblPdCsDate').text('');

    $('#lblPdCkTrNo').text('');
    $('#lblPdCsTrNo').text('');

    $('#lblPdCkAcNo').text('');
    $('#lblPdCsAcNo').text('');

    $('#lblPdCkAcName').text('');
    $('#lblPdCsAcName').text('');

    $.each($('#tblPdCk').find('label'), function (Index, value) {
        if (value.className != 'currCode')
            $(this).text('');
    });

    $.each($('#divPdCs').find('label'), function (idx, ctrl) {
        //if (!(ctrl.id.indexOf('pdCsCurr') == 0 || ctrl.id.indexOf('lblCsChkCount') == 0 ||
        //    ctrl.id.indexOf('lblPdCurrCkTotal') == 0 || ctrl.id.indexOf('lblPdCurrCkTotalP') == 0))
        if (ctrl.className != 'currCode') {
            $(this).text('');
        }
    });

    $('#divPrintDepositSlip').find('.divDpSlipCopyTemp').remove();

    $('#divPrintDepositSlip').find('[class^="divDpSlipTemp"]').remove();

    $('#lblCsChkCount').text('');
    $('#lblPdCurrCkTotal').text('');
    $('#lblPdCurrCkTotalP').text('');

    $('.dsDepositSlip').text('');
    $('.pdDsCopyName').text('');
}

function BindPrintDepositSlipData_old() {

    var months = ["January", "February", "March", "April", "May", "June",
               "July", "August", "September", "October", "November", "December"];

    var dt = new Date($('#txtTransactionDate').val());

    $('#lblDepositSlipTitle').text($('#txtStoreDesc').val());

    $('#lblPdCkDate').text(months[dt.getMonth()] + ' ' + dt.getDate() + ', ' + dt.getFullYear());
    $('#lblPdCsDate').text(months[dt.getMonth()] + ' ' + dt.getDate() + ', ' + dt.getFullYear());

    var trNo = GetStoreValue('transRoute');
    $('#lblPdCkTrNo').text(trNo);
    $('#lblPdCsTrNo').text(trNo);

    var accNo = GetStoreValue('bankAccNo');
    $('#lblPdCkAcNo').text(accNo);
    $('#lblPdCsAcNo').text(accNo);

    $('#lblPdCkAcName').text($('#txtStoreDesc').val() + ' #' + $('#txtStore').val());
    $('#lblPdCsAcName').text($('#txtStoreDesc').val() + ' #' + $('#txtStore').val());

    //$('.pdDsCopyName').text(GetGpParm('CDB_DEP_SLIP_COPY_NAME%'));
    $('.checkLabel').text(GetGpParmLabel('CDB_CHECK_LABEL'));

    $('#lblDepositSlip').text($('#txtDepSlip').val());

    var csDwrDep = new Object();

    csDwrDep.companyCd = GetStoreValue('companyCd');
    csDwrDep.storeCd = $('#txtStore').val();
    csDwrDep.cashDrawerCd = $('#txtCashDrawer').val();
    csDwrDep.transactionDate = $.trim($('#txtTransactionDate').val());
    csDwrDep.empCdCashier = GetSessionValue('EmpCd');
    csDwrDep.currencyCd = $.trim($("#ddlCurr option:selected").text()) != '' ? $("#ddlCurr option:selected").text() : GetStoreValue('defCurrCd');

    $.ajax({
        type: 'POST',
        url: 'CashDrawerBalancing.aspx/GetCdbCash',
        data: JSON.stringify({ objCashDrawerDepositDtc: csDwrDep }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {

            PrintCheckDeposit();

            //$.each($('#divPdCs').find('label'), function (idx, ctrl) {
            //    if (!(ctrl.id.indexOf('pdCsCurr') == 0 || ctrl.id.indexOf('lblCsChkCount') == 0 ||
            //        ctrl.id.indexOf('lblPdCurrCkTotal') == 0 || ctrl.id.indexOf('lblPdCurrCkTotalP') == 0)) {
            //        $(this).text('');
            //    }
            //});

            if ($.trim(msg.d) != '') {

                $('#lblPdDepositBagNo').text($.trim(msg.d.depositBag) != '' ? msg.d.depositBag : '');

                $('#lblPdCurr1').text(parseFloat(msg.d.oneCount) > 0 ? msg.d.oneCount : '');
                $('#lblPdCurr2').text(parseFloat(msg.d.twoCount) > 0 ? msg.d.twoCount : '');
                $('#lblPdCurr5').text(parseFloat(msg.d.fiveCount) > 0 ? msg.d.fiveCount : '');
                $('#lblPdCurr10').text(parseFloat(msg.d.tenCount) > 0 ? msg.d.tenCount : '');
                $('#lblPdCurr20').text(parseFloat(msg.d.twentyCount) > 0 ? msg.d.twentyCount : '');
                $('#lblPdCurr50').text(parseFloat(msg.d.fiftyCount) > 0 ? msg.d.fiftyCount : '');
                $('#lblPdCurr100').text(parseFloat(msg.d.oneHundredCount) > 0 ? msg.d.oneHundredCount : '');
                $('#lblPdCurr1000').text(parseFloat(msg.d.oneThousandCount) > 0 ? msg.d.oneThousandCount : '');

                $('#lblPdCurr1Total').text($('#lblPdCurr1').text());
                $('#lblPdCurr1TotalP').text($.trim($('#lblPdCurr1').text()) != '' ? '00' : '');
                $('#lblPdCurr2Total').text($.trim($('#lblPdCurr2').text()) != '' ? parseFloat($('#lblPdCurr2').text()) * 2 : '');
                $('#lblPdCurr2TotalP').text($.trim($('#lblPdCurr2').text()) != '' ? '00' : '');
                $('#lblPdCurr5Total').text($.trim($('#lblPdCurr5').text()) != '' ? parseFloat($('#lblPdCurr5').text()) * 5 : '');
                $('#lblPdCurr5TotalP').text($.trim($('#lblPdCurr5').text()) != '' ? '00' : '');
                $('#lblPdCurr10Total').text($.trim($('#lblPdCurr10').text()) != '' ? parseFloat($('#lblPdCurr10').text()) * 10 : '');
                $('#lblPdCurr10TotalP').text($.trim($('#lblPdCurr10').text()) != '' ? '00' : '');
                $('#lblPdCurr20Total').text($.trim($('#lblPdCurr20').text()) != '' ? parseFloat($('#lblPdCurr20').text()) * 20 : '');
                $('#lblPdCurr20TotalP').text($.trim($('#lblPdCurr20').text()) != '' ? '00' : '');
                $('#lblPdCurr50Total').text($.trim($('#lblPdCurr50').text()) != '' ? parseFloat($('#lblPdCurr50').text()) * 50 : '');
                $('#lblPdCurr50TotalP').text($.trim($('#lblPdCurr50').text()) != '' ? '00' : '');
                $('#lblPdCurr100Total').text($.trim($('#lblPdCurr100').text()) != '' ? parseFloat($('#lblPdCurr100').text()) * 100 : '');
                $('#lblPdCurr100TotalP').text($.trim($('#lblPdCurr100').text()) != '' ? '00' : '');
                $('#lblPdCurr1000Total').text($.trim($('#lblPdCurr1000').text()) != '' ? parseFloat($('#lblPdCurr1000').text()) * 1000 : '');
                $('#lblPdCurr1000TotalP').text($.trim($('#lblPdCurr1000').text()) != '' ? '00' : '');

                var currTotal = parseFloat($.trim($('#lblPdCurr1Total').text()) != '' ? $('#lblPdCurr1Total').text() : '00') +
                    parseFloat($.trim($('#lblPdCurr2Total').text()) != '' ? $('#lblPdCurr2Total').text() : '00') +
                    parseFloat($.trim($('#lblPdCurr5Total').text()) != '' ? $('#lblPdCurr5Total').text() : '00') +
                    parseFloat($.trim($('#lblPdCurr10Total').text()) != '' ? $('#lblPdCurr10Total').text() : '00') +
                    parseFloat($.trim($('#lblPdCurr20Total').text()) != '' ? $('#lblPdCurr20Total').text() : '00') +
                    parseFloat($.trim($('#lblPdCurr50Total').text()) != '' ? $('#lblPdCurr50Total').text() : '00') +
                    parseFloat($.trim($('#lblPdCurr100Total').text()) != '' ? $('#lblPdCurr100Total').text() : '00') +
                    parseFloat($.trim($('#lblPdCurr1000Total').text()) != '' ? $('#lblPdCurr1000Total').text() : '00');

                $('#lblPdCurrTotal').text((currTotal + '').split('.')[0]);
                $('#lblPdCurrTotalP').text((currTotal.toFixed(2) + '').split('.')[1]);

                var totalCoin = parseFloat(msg.d.penniesCount) * 0.01 + parseFloat(msg.d.nickelsCount) * 0.05 + parseFloat(msg.d.dimesCount) * 0.1 +
                parseFloat(msg.d.quartersCount) * 0.25 + parseFloat(msg.d.halfCount) * 0.5 + parseFloat(msg.d.oneCoinCount) * 1;

                $('#lblPdCurrCoinTotal').text((totalCoin + '').split('.')[0]);
                $('#lblPdCurrCoinTotalP').text((totalCoin.toFixed(2) + '').split('.')[1]);

                var chkTotal = $.trim($('#lblPdCurrCkTotal').text()) != '' ? parseFloat($('#lblPdCurrCkTotal').text() + '.' + $('#lblPdCurrCkTotalP').text()) : 0;

                var total = currTotal + totalCoin + chkTotal;

                $('#lblTotalPd').text((total + '').split('.')[0]);
                $('#lblTotalPdP').text((total.toFixed(2) + '').split('.')[1]);
            }

            AddDepositSlipCopyNames();

            $('.ajax-content').hide();
        }, failure: function (error) {
            setColorForFailure('#lblErrMsg');
            $('#lblErrMsg').text(jQuery.parseJSON(error.responseText).Message);
            $('.ajax-content').hide();
        }
    }).fail(function (error) {
        var r = jQuery.parseJSON(error.responseText);
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text(r.Message);
        $('.ajax-content').hide();
    });

    //var depositSlip = (dt.getMonth() < 10 ? '0' + dt.getMonth() : (dt.getMonth() + 1) + '') +
    //   (dt.getDate() < 10 ? '0' + dt.getDate() : dt.getDate()) + (dt.getFullYear() + '').charAt(3) +
    //   $('#txtStore').val() + $('#txtCashDrawer').val() + '01';

    //$('#lblPdDepositSlip').text(depositSlip);
    $('#lblPdDepositSlip').text(GetDepositSlip(1));
}

function BindPrintDepositSlipData() {

    $('.divDpSlipCopyTemp').empty();
    $('[class^="divDpSlipTemp"]').empty();

    var objcashDrawer = new Object();
    objcashDrawer.companyCd = GetStoreValue('companyCd');
    objcashDrawer.empCd = $('#hidCashierCd').val();
    objcashDrawer.empInit = $('#ddlCashier option:selected').text();
    objcashDrawer.loggedEmpCd = GetSessionValue('EmpCd');
    objcashDrawer.storeCd = $('#txtStore').val();
    objcashDrawer.cashDrawerCd = $('#txtCashDrawer').val();
    objcashDrawer.transactionDate = $.trim($('#txtTransactionDate').val());
    objcashDrawer.currencyCd = $.trim($("#ddlCurr option:selected").text()) != '' ? $("#ddlCurr option:selected").text() : GetStoreValue('defCurrCd');

    $.ajax({
        url: 'CashDrawerBalancing.aspx/PrintCashDrawerData',
        data: JSON.stringify({ cashDrawer: objcashDrawer }),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {

            if ($.trim(msg.d) != '') {

                if (null != msg.d.depositCash)
                    PrintCashData(msg.d.depositCash);

                if (null != msg.d.depositCk)
                    PrintCheckData(msg.d.depositCk);

                PrintCommonData(msg.d);

                AddDepositSlipCopyNames();

                $('.checkLabel').text(GetGpParmLabel('CDB_CHECK_LABEL'));

                //GetDepositSlipPrintCount(msg.d.printCount);
            }

            $('.ajax-content').hide();
        },
        error: function (XMLHttpRequest, callStatus, errorThrown) {

            $('.ajax-content').hide();
        }
    });
}

function PrintCashData(cash) {

    $('#lblPdCurr1').text(parseFloat(cash.oneCount) > 0 ? cash.oneCount : '');
    $('#lblPdCurr2').text(parseFloat(cash.twoCount) > 0 ? cash.twoCount : '');
    $('#lblPdCurr5').text(parseFloat(cash.fiveCount) > 0 ? cash.fiveCount : '');
    $('#lblPdCurr10').text(parseFloat(cash.tenCount) > 0 ? cash.tenCount : '');
    $('#lblPdCurr20').text(parseFloat(cash.twentyCount) > 0 ? cash.twentyCount : '');
    $('#lblPdCurr50').text(parseFloat(cash.fiftyCount) > 0 ? cash.fiftyCount : '');
    $('#lblPdCurr100').text(parseFloat(cash.oneHundredCount) > 0 ? cash.oneHundredCount : '');
    $('#lblPdCurr1000').text(parseFloat(cash.oneThousandCount) > 0 ? cash.oneThousandCount : '');

    $('#lblPdCurr1Total').text($('#lblPdCurr1').text());
    $('#lblPdCurr1TotalP').text($.trim($('#lblPdCurr1').text()) != '' ? '00' : '');
    $('#lblPdCurr2Total').text($.trim($('#lblPdCurr2').text()) != '' ? parseFloat($('#lblPdCurr2').text()) * 2 : '');
    $('#lblPdCurr2TotalP').text($.trim($('#lblPdCurr2').text()) != '' ? '00' : '');
    $('#lblPdCurr5Total').text($.trim($('#lblPdCurr5').text()) != '' ? parseFloat($('#lblPdCurr5').text()) * 5 : '');
    $('#lblPdCurr5TotalP').text($.trim($('#lblPdCurr5').text()) != '' ? '00' : '');
    $('#lblPdCurr10Total').text($.trim($('#lblPdCurr10').text()) != '' ? parseFloat($('#lblPdCurr10').text()) * 10 : '');
    $('#lblPdCurr10TotalP').text($.trim($('#lblPdCurr10').text()) != '' ? '00' : '');
    $('#lblPdCurr20Total').text($.trim($('#lblPdCurr20').text()) != '' ? parseFloat($('#lblPdCurr20').text()) * 20 : '');
    $('#lblPdCurr20TotalP').text($.trim($('#lblPdCurr20').text()) != '' ? '00' : '');
    $('#lblPdCurr50Total').text($.trim($('#lblPdCurr50').text()) != '' ? parseFloat($('#lblPdCurr50').text()) * 50 : '');
    $('#lblPdCurr50TotalP').text($.trim($('#lblPdCurr50').text()) != '' ? '00' : '');
    $('#lblPdCurr100Total').text($.trim($('#lblPdCurr100').text()) != '' ? parseFloat($('#lblPdCurr100').text()) * 100 : '');
    $('#lblPdCurr100TotalP').text($.trim($('#lblPdCurr100').text()) != '' ? '00' : '');
    $('#lblPdCurr1000Total').text($.trim($('#lblPdCurr1000').text()) != '' ? parseFloat($('#lblPdCurr1000').text()) * 1000 : '');
    $('#lblPdCurr1000TotalP').text($.trim($('#lblPdCurr1000').text()) != '' ? '00' : '');

    var currTotal = parseFloat($.trim($('#lblPdCurr1Total').text()) != '' ? $('#lblPdCurr1Total').text() : '00') +
        parseFloat($.trim($('#lblPdCurr2Total').text()) != '' ? $('#lblPdCurr2Total').text() : '00') +
        parseFloat($.trim($('#lblPdCurr5Total').text()) != '' ? $('#lblPdCurr5Total').text() : '00') +
        parseFloat($.trim($('#lblPdCurr10Total').text()) != '' ? $('#lblPdCurr10Total').text() : '00') +
        parseFloat($.trim($('#lblPdCurr20Total').text()) != '' ? $('#lblPdCurr20Total').text() : '00') +
        parseFloat($.trim($('#lblPdCurr50Total').text()) != '' ? $('#lblPdCurr50Total').text() : '00') +
        parseFloat($.trim($('#lblPdCurr100Total').text()) != '' ? $('#lblPdCurr100Total').text() : '00') +
        parseFloat($.trim($('#lblPdCurr1000Total').text()) != '' ? $('#lblPdCurr1000Total').text() : '00');

    $('#lblPdCurrTotal').text((currTotal + '').split('.')[0]);
    $('#lblPdCurrTotalP').text((currTotal.toFixed(2) + '').split('.')[1]);

    var totalCoin = parseFloat(cash.penniesCount) * 0.01 + parseFloat(cash.nickelsCount) * 0.05 + parseFloat(cash.dimesCount) * 0.1 +
    parseFloat(cash.quartersCount) * 0.25 + parseFloat(cash.halfCount) * 0.5 + parseFloat(cash.oneCoinCount) * 1;

    $('#lblPdCurrCoinTotal').text((totalCoin + '').split('.')[0]);
    $('#lblPdCurrCoinTotalP').text((totalCoin.toFixed(2) + '').split('.')[1]);
}

function PrintCheckData(check) {

    $('#firstDepSlipData').find('[class^="lblPdCkName"]').text('');
    $('#firstDepSlipData').find('[class^="lblPdCkAmt"]').text('');
    $('#firstDepSlipData').find('[class^="lblPdCkAmtP"]').text('');

    $('[class^="divDpSlipTemp"]').find('[class^="lblPdCkName"]').text('');
    $('[class^="divDpSlipTemp"]').find('[class^="lblPdCkAmt"]').text('');
    $('[class^="divDpSlipTemp"]').find('[class^="lblPdCkAmtP"]').text('');

    var idx = 1;
    var ckAmtTotal = 0;

    $('#lblEmpinit').text(GetSessionValue('EmpInit'));

    if (check.length < 14) {

        $('#firstDepSlipData').find('.dsPageNo').text('Page 1 of 1');

        $.each(check.reverse(), function (key, value) {

            $('#firstDepSlipData').find('.lblPdCkName' + idx).text($.trim(value.checkName) != '' ? $.trim(value.checkName) != 'null' ? value.checkName : '' : '');
            $('#firstDepSlipData').find('.lblPdCkAmt' + idx).text((parseFloat(value.checkAmount) + '').split('.')[0]);
            $('#firstDepSlipData').find('.lblPdCkAmtP' + idx).text((parseFloat(value.checkAmount).toFixed(2) + '').split('.')[1]);

            idx++;

            ckAmtTotal += parseFloat(value.checkAmount);
        });
    }
    else {

        var maxCkRow = 60;

        var totalPage = (check.length / maxCkRow) + 1;

        if (parseInt(totalPage, 10) < totalPage)
            totalPage = parseInt(totalPage + 1, 10);

        $('#firstDepSlipData').find('.dsPageNo').text('Page 1 of ' + totalPage);

        $('#firstDepSlipData').find('.lblPdCkName1').text('See Attached ' + GetGpParmLabel('CDB_CHECK_LABEL') + ' List');

        var tempCkRowCnt = 1;
        var pageIdx = 1;

        AddNewDsCheck(pageIdx);

        $.each(check.reverse(), function (key, value) {

            if (tempCkRowCnt > maxCkRow) {

                tempCkRowCnt = 1;

                pageIdx++;

                AddNewDsCheck(pageIdx);
            }

            $('.divDpSlipTemp' + pageIdx).find('.lblPdCkName' + tempCkRowCnt).text($.trim(value.checkName) != '' ? $.trim(value.checkName) != 'null' ? value.checkName : '' : '');
            $('.divDpSlipTemp' + pageIdx).find('.lblPdCkAmt' + tempCkRowCnt).text((parseFloat(value.checkAmount) + '').split('.')[0]);
            $('.divDpSlipTemp' + pageIdx).find('.lblPdCkAmtP' + tempCkRowCnt).text((parseFloat(value.checkAmount).toFixed(2) + '').split('.')[1]);

            if ((pageIdx + 1) != totalPage) {
                $('.divDpSlipTemp' + pageIdx).find('.divTotalCk').empty();
            }

            $('.divDpSlipTemp' + pageIdx).find('.dsPageNo').text('Page ' + (pageIdx + 1) + ' of ' + totalPage);

            tempCkRowCnt++;

            idx++;

            ckAmtTotal += parseFloat(value.checkAmount);
        });
    }

    $('.lblTotalChkAmt').text((ckAmtTotal + '').split('.')[0]);
    $('.lblTotalChkAmtP').text((ckAmtTotal.toFixed(2) + '').split('.')[1]);
    $('.lblTotalNoChk').text(idx - 1);

    var currTotal = $.trim($('#lblPdCurrTotal').text()) != '' ? $('#lblPdCurrTotal').text() : 0;
    currTotal += '.' + ($.trim($('#lblPdCurrTotalP').text()) != '' ? $('#lblPdCurrTotalP').text() : 0);

    var totalCoin = $.trim($('#lblPdCurrCoinTotal').text()) != '' ? $('#lblPdCurrCoinTotal').text() : 0;
    totalCoin += '.' + ($.trim($('#lblPdCurrCoinTotalP').text()) != '' ? $('#lblPdCurrCoinTotalP').text() : 0);

    var total = parseFloat(currTotal) + parseFloat(totalCoin) + parseFloat(ckAmtTotal);

    $('#lblTotalPd').text((total + '').split('.')[0]);
    $('#lblTotalPdP').text((total.toFixed(2) + '').split('.')[1]);
}

function PrintCommonData(cdb) {
    var months = ["January", "February", "March", "April", "May", "June",
               "July", "August", "September", "October", "November", "December"];

    var dt = new Date($('#txtTransactionDate').val());

    $('#lblDepositSlipTitle').text($('#txtStoreDesc').val());

    $('.dsCkDate').text(months[dt.getMonth()] + ' ' + dt.getDate() + ', ' + dt.getFullYear());

    if (null != cdb.depositCash && $.trim(cdb.depositCash.depositBag) != '')
        $('.dsDepositBagNo').text(cdb.depositCash.depositBag);

    $('.dsTransitNo').text(GetStoreValue('transRoute'));

    $('.dsAccountNo').text(GetStoreValue('bankAccNo'));

    $('.dsAccountName').text($('#txtStoreDesc').val() + ' #' + $('#txtStore').val());

    //$('.pdDsCopyName').text(GetGpParm('CDB_DEP_SLIP_COPY_NAME%'));
    $('.checkLabel').text(GetGpParmLabel('CDB_CHECK_LABEL'));

    $('.dsDepositSlip').text($('#txtDepSlip').val());

}

function SavePrintData() {

    $('.ajax-content').show();

    var objCashDrawer = new Object();
    objCashDrawer.companyCd = GetStoreValue('companyCd');
    objCashDrawer.empCd = GetSessionValue('EmpCd');
    objCashDrawer.storeCd = $('#txtStore').val();
    objCashDrawer.cashDrawerCd = $('#txtCashDrawer').val();
    objCashDrawer.transactionDate = $.trim($('#txtTransactionDate').val());
    objCashDrawer.currencyCd = $.trim($("#ddlCurr option:selected").text()) != '' ? $("#ddlCurr option:selected").text() : GetStoreValue('defCurrCd');

    var objCash = new Object();
    objCash.depositSlip = $('#hidDepositSlipNo').val();
    objCashDrawer.depositCash = objCash;

    $.ajax({
        url: 'CashDrawerBalancing.aspx/SavePrintData',
        data: JSON.stringify({ cashDrawer: objCashDrawer }),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {
            GetDepositSlipPrintCount(msg.d.printCount);
            $('.ajax-content').hide();
        },
        error: function (XMLHttpRequest, callStatus, errorThrown) {

            $('.ajax-content').hide();
        }
    });
}

function AddNewDsCheck(a) {

    var htmltag = "<div class='divDpSlipTemp" + a + "' style='display: table-row; width: 100%;'>";
    htmltag += $('#divNextDpSlipData').html();
    htmltag += "</div>";

    $('#depSlipData').append(htmltag);
}

function AddDepositSlipCopyNames() {

    var DepositSlipCopyNames = GetGpParmDepositSlipCopyName('CDB_DEP_SLIP_COPY_NAME%');

    for (var a = 1; a < DepositSlipCopyNames.d.length; a++) {

        //var htmltag = "<br style='page-break-before:always' />";
        var htmltag = "<div class='divDpSlipCopyTemp' id='divDpSlipCopyT" + a + "' ";
        htmltag += "style='display: table; position: relative; width: 100%;'>";
        htmltag += "<br style='page-break-before:always' />";
        htmltag += $('#depSlipData').html();
        htmltag += "</div>";

        $('#divPrintDepositSlip').append(htmltag);

        var tempDiv = '#divDpSlipCopyT' + a;

        $(tempDiv).find('.pdDsCopyName').each(function (i, obj) {
            obj.innerHTML = DepositSlipCopyNames.d[a];
        });
    }

    $('#depSlipData').find('.pdDsCopyName').each(function (i, obj) {
        obj.innerHTML = DepositSlipCopyNames.d[0];
    });
}

function GetGpParmDepositSlipCopyName(e) {
    var DepositSlipCopyNames;
    $.ajax({
        type: 'POST',
        async: false,
        url: 'CashDrawerBalancing.aspx/GetGpParmDepositSlipCopyName',
        data: JSON.stringify({ parm: e }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            DepositSlipCopyNames = msg;
        }, failure: function (error) {
            setColorForFailure('#lblErrMsg');
            $('#lblErrMsg').text(jQuery.parseJSON(error.responseText).Message);
            $('.ajax-content').hide();
        }
    }).fail(function (error) {
        var r = jQuery.parseJSON(error.responseText);
        setColorForFailure('#lblErrMsg');
        $('#lblErrMsg').text(r.Message);
        $('.ajax-content').hide();
    });
    return DepositSlipCopyNames;
}
