<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Relationship_Maintenance.aspx.vb" Inherits="Relationship_Maintenance" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Src="~/Usercontrols/MultiWarranties.ascx" TagPrefix="ucWar" TagName="Warranties" %>
<%@ Register Src="~/Usercontrols/RelatedSKU.ascx" TagPrefix="ucRelatedSku" TagName="RelatedSKUs" %>
<%@ Register Src="~/Usercontrols/ManagerOverride.ascx" TagPrefix="ucMgr" TagName="ManagerOverride" %>
<%@ Register Src="~/Usercontrols/SummaryPriceChangeApproval.ascx" TagPrefix="ucPC" TagName="SummaryPriceChangeApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">
        function SelectAndClosePopup1() {
            ASPxPopupControl1.Hide();
        }
        function SelectAndClosePopup5() {
            ctl00_ContentPlaceHolder1_ASPxPopupControl5.Hide();
        }
        function SelectAndClosePopup2() {
            ASPxPopupControl2.Hide();
        }
        function SelectAndClosePopupEmailTemplate() {
            ASPxPopupControl1.Hide();
        }
        function SelectAndClosePopup9() {
            ASPxPopupControlWarr.Hide();
        }
        function ClosePopupTax() {
            PopupControlTax.Hide();
        }
        function pageLoad(sender, args) {
            $('input[type=submit], a, button').click(function (event) {

                if ($('#hidUpdLnNos').val() != null && $.trim($('#hidUpdLnNos').val()) != '' && $(this).attr('href') != null) {

                    if ($.trim($(this).attr('href')) != '' && $.trim($(this).attr('href')).indexOf('javascript') < 0) {

                        $('#hidDestUrl').val($.trim($(this).attr('href')));

                        __doPostBack('SummaryPriceChangeApproval', "");

                        event.preventDefault();
                    }
                }
            });
        }
    </script>
    <asp:HiddenField ID="hidDestUrl" runat="server" ClientIDMode="Static" />

    <table style="position: relative; width: 95%;">
        <tr>
            <td style="width: 95px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label571 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 139px">
                <dx:ASPxComboBox ID="cbo_rel_status" runat="server" ValueType="System.String" Enabled="False"
                    Width="106px" IncrementalFilteringMode="StartsWith">
                    <Items>
                        <dx:ListEditItem Text="<%$ Resources:LibResources, Label359 %>" Value="O" Selected="True" />
                        <dx:ListEditItem Text="<%$ Resources:LibResources, Label769 %>" Value="C" />
                        <dx:ListEditItem Text="<%$ Resources:LibResources, Label771 %>" Value="F" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
            <td colspan="3">
                <dx:ASPxLabel ID="lbl_status_dt" runat="server" Text="">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td style="width: 95px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label459 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 139px">
                <asp:TextBox ID="txt_del_doc_num" runat="server" Style="position: relative" Enabled="False"
                    CssClass="style5"></asp:TextBox>
            </td>
            <td style="width: 116px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label668 %>">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:TextBox ID="txt_wr_dt" runat="server" Style="position: relative" Width="88px"
                    CssClass="style5"></asp:TextBox>
            </td>
            <td style="text-align: right; width: 77px;">
                <asp:TextBox ID="txt_store" runat="server" Width="24px" CssClass="style5"></asp:TextBox>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 95px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label130 %>">
                </dx:ASPxLabel>
            </td>
            <td style="width: 139px">
                <asp:TextBox ID="txt_Cust_cd" runat="server" CssClass="style5"></asp:TextBox>
            </td>
            <td valign="top" style="width: 116px">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="<%$ Resources:LibResources, Label212 %>">
                </dx:ASPxLabel>
            </td>
            <td colspan="2">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:TextBox ID="txt_follow_dt" runat="server" Width="88px" CssClass="style5" Wrap="False"></asp:TextBox>
                            <asp:TextBox ID="txt_appt_time" runat="server" Width="35px" CssClass="style5"></asp:TextBox>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_am_or_pm" AutoPostBack="True" runat="server" ValueType="System.String"
                                Width="45px" IncrementalFilteringMode="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="AM" Value="AM" />
                                    <dx:ListEditItem Text="PM" Value="PM" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td valign="top">
                            <dx:ASPxComboBox ID="cbo_call_type" AutoPostBack="True" runat="server" ValueType="System.String"
                                Width="120px" IncrementalFilteringMode="StartsWith">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="<%$ Resources:LibResources, Label52 %>" Value="PHONE" />
                                    <dx:ListEditItem Text="<%$ Resources:LibResources, Label238 %>" Value="STORE" />
                                    <dx:ListEditItem Text="<%$ Resources:LibResources, Label237 %>" Value="HOME" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="99%"
        OnActiveTabChanged="ASPxPageControl1_ActiveTabChanged" AutoPostBack="True">
        <TabPages>
            <dx:TabPage Text="<%$ Resources:LibResources, Label364 %>">
                <ActiveTabStyle CssClass="style5">
                </ActiveTabStyle>
                <TabStyle CssClass="style5">
                </TabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table style="position: relative; width: 99%;" class="style5">
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="<%$ Resources:LibResources, Label110 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td colspan="4">
                                    <asp:TextBox ID="txt_corp_name" runat="server" Width="324px" CssClass="style5" MaxLength="30"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="<%$ Resources:LibResources, Label196 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txt_f_name" runat="server" Width="86px" CssClass="style5"></asp:TextBox>
                                    <asp:TextBox ID="txt_l_name" runat="server" Width="86px" CssClass="style5"></asp:TextBox>
                                </td>
                                <td colspan="1" align="right">
                                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="<%$ Resources:LibResources, Label225 %> &nbsp;">
                                    </dx:ASPxLabel>
                                    <asp:TextBox ID="txt_h_phone" runat="server" Width="86px" CssClass="style5"></asp:TextBox>
                                </td>
                                <td style="text-align: center; width: 77px;" rowspan="2">
                                    <asp:Image ID="img_delivery" runat="server" ImageUrl="images/icons/delivery.gif" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="<%$ Resources:LibResources, Label20 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txt_addr1" runat="server" Width="233px" CssClass="style5"></asp:TextBox>
                                </td>
                                <td colspan="1" align="right">
                                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:LibResources, Labe1000 %> &nbsp;">
                                    </dx:ASPxLabel>
                                    <asp:TextBox ID="txt_b_phone" runat="server" Width="86px" CssClass="style5"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="<%$ Resources:LibResources, Label21 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txt_addr2" runat="server" Width="233px" CssClass="style5"></asp:TextBox>
                                </td>
                                <td colspan="1" align="right">
                                    <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="<%$ Resources:LibResources, Label553 %>">
                                    </dx:ASPxLabel>
                                    <asp:TextBox ID="txt_slsp1_init" runat="server" Width="60px" CssClass="style5"></asp:TextBox>
                                </td>
                                <td style="text-align: center; width: 77px;" rowspan="2">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="<%$ Resources:LibResources, Label81 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txt_city" runat="server" Width="136px" CssClass="style5"></asp:TextBox>&nbsp;
                                     <asp:DropDownList ID="cbo_State" Width="100px" runat="server" CssClass="style5">
                                     </asp:DropDownList>&nbsp;
                                    <asp:TextBox ID="txt_zip" runat="server" Width="43px" CssClass="style5" AutoPostBack="True"
                                        OnTextChanged="txt_zip_TextChanged"></asp:TextBox>
                                </td>
                                <td colspan="1" align="right">&nbsp;
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="<%$ Resources:LibResources, Label178 %>">
                                    </dx:ASPxLabel>
                                </td>
                                <td colspan="4">
                                    <asp:TextBox ID="txt_email" runat="server" Width="322px" CssClass="style5"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lbl_slsp1_cd" runat="server" Visible="false"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top">
                                    <br />
                                    <asp:Image ID="img_warn" runat="server" Height="20px" ImageUrl="images/warning.jpg"
                                        Width="26px" Visible="False" />&nbsp;
                                    <dx:ASPxLabel ID="lbl_cust_warn" runat="server" Text="" Visible="false">
                                    </dx:ASPxLabel>
                                    <br />
                                    <br />
                                </td>
                                <td colspan="3" align="right" valign="top">
                                    <table>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="lblTaxCd" runat="server" Text="<%$ Resources:LibResources, Label587 %>">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtTaxCd" runat="server" Enabled="false" Width="180px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>

                                    <dx:ASPxLabel ID="AspxLabelExempt" runat="server" Text="<%$ Resources:LibResources, Label588 %>">
                                    </dx:ASPxLabel>
                                    <table>
                                        <tr>
                                            <td>
                                                <dx:ASPxComboBox ID="cbo_tax_exempt" runat="server" ValueType="System.String"
                                                    Width="305px" AutoPostBack="True" IncrementalFilteringMode="StartsWith">
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txt_exempt_id" runat="server" Width="103px" MaxLength="20">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                </td>
                            </tr>
                        </table>
                        <%--<div style="width: 100%; height: 500px; overflow: auto;">--%>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="<%$ Resources:LibResources, Label268 %>">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <asp:DataGrid ID="Gridview1" runat="server" AutoGenerateColumns="False" CellPadding="2"
                            DataKeyField="LINE" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" Width="99%" ShowFooter="True" Font-Bold="False">
                            <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                                Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                            <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                                Font-Bold="True" ForeColor="Black"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn DataField="LINE" ReadOnly="True" ItemStyle-CssClass="style5"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ITM_CD" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ve_cd" HeaderText="Vendor" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label653 %>">
                                    <ItemTemplate>
                                        <table width="100%" class="style5">
                                            <tr>
                                                <td align="left" valign="top" width="150">

                                                    <%--<asp:Image ID="img_picture" runat="server" Width="133px" Height="100px" ImageUrl="~/images/image_not_found.jpg" ToolTip='<%# If(DataBinder.Eval(Container, "DataItem.sortdescription").ToString().ToUpper().Replace(",", "," + Environment.NewLine) = "-", "", DataBinder.Eval(Container, "DataItem.sortdescription").ToString().ToUpper().Replace(",", "," + Environment.NewLine))%> ' />--%>
                                                    <asp:Image ID="img_picture" runat="server" Width="133px" Height="100px" ImageUrl="~/images/image_not_found.jpg" />
                                                    <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label561 %>" />:<br />
                                                    <div runat="server" id="divSortCodes" style="overflow: auto; height: 40px; text-align: left; width: 95%; padding-top: 5px;">
                                                        <%--<label><%# Replace(If(DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper().Replace(",", "," + Environment.NewLine) = "-", "", DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper()),",",", ")%></label>--%>
                                                        <label><%# Replace(StringUtils.EliminateCommas(If(DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper().Replace(",", "," + Environment.NewLine) = "-", "", DataBinder.Eval(Container, "DataItem.sortcodes").ToString().ToUpper())),",",", ")%></label>
                                                    </div>
                                                    <br />
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="Label1" Font-Bold="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vsn") %>'></asp:Label>
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Label2" Font-Bold="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.des") %>'></asp:Label>
                                                    <br />
                                                    <asp:Label ID="Label43" Font-Bold="false" runat="server" Text="<%$ Resources:LibResources, Label550 %>"></asp:Label>
                                                    <asp:Label ID="lbl_SKU" Font-Bold="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itm_cd") %>'></asp:Label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Label ID="Label33" Font-Bold="false" runat="server" Text="<%$ Resources:LibResources, Label640 %>"></asp:Label>
                                                    <asp:Label ID="lbl_ve_cd" Font-Bold="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ve_cd") %>'></asp:Label>
                                                    <br />
                                                    <asp:Label ID="lbl_warr" runat="server" Font-Bold="false" Text='Warr SKU:'></asp:Label>
                                                    <asp:Label ID="lbl_warr_sku" runat="server" Font-Bold="false" Text='<%# DataBinder.Eval(Container, "DataItem.warr_itm_link")%>'></asp:Label>
                                                    <br />
                                                    <asp:Label ID="lbl_carton" Font-Bold="false" runat="server" Text="<%$ Resources:LibResources, Label264 %>"></asp:Label>
                                                    <asp:CheckBox ID="chk_carton" Font-Bold="false" AutoPostBack="true" OnCheckChanged="Carton_Update"
                                                        runat="server" />
                                                    <br />
                                                    <asp:Label ID="lblAvailResults" runat="server" Font-Bold="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txt_line_desc" runat="server" Height="50px" TextMode="MultiLine"
                                                        CssClass="style5" Width="98%" AutoPostBack="true" OnTextChanged="Line_Update"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txt_new_itm_cd" runat="server" Text="" AutoPostBack="False" CssClass="style5" Width="170px"></asp:TextBox>
                                        <asp:HyperLink ID="hpl_find_merch" runat="server"
                                            NavigateUrl="order.aspx?referrer=Relationship_Maintenance.aspx">
                                                <img src="images/icons/find.gif" alt="Find" style="width: 15px; height: 15px" border="0" />
                                        </asp:HyperLink>
                                    </FooterTemplate>
                                    <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" VerticalAlign="Top" />
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="des" HeaderText="Description" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label427 %>">
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="unit_prc" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.ret_prc"),2,-1,0,0) %>'
                                            Width="45" AutoPostBack="True" OnTextChanged="Price_Update" CssClass="style5"></asp:TextBox>
                                        <asp:HiddenField id="hidMaxDisc" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.MAX_DISC_PCNT")%>'></asp:HiddenField>
                                        <br />
                                        <asp:HiddenField ID="hidRetPrc" runat="server" Value='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.ret_prc"), 2)%>'></asp:HiddenField>
                                        <asp:HiddenField ID="hidOrigPrc" runat="server" Value='<%# FormatNumber(If(IsDBNull(DataBinder.Eval(Container, "DataItem.orig_prc")) ,0.00,DataBinder.Eval(Container, "DataItem.orig_prc") ) , 2)%>'></asp:HiddenField>
                                        <asp:ImageButton ID="img_gm" runat="server" ImageUrl="images/icons/money.gif" Height="20"
                                            Width="20" CommandName="find_gm" Visible="True" />
                                        <asp:ImageButton ID="Image2" runat="server" ImageUrl="images/icons/compare_inventory.gif"
                                            Height="20" Width="20" CommandName="find_inventory" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txt_new_unit_prc" runat="server" Text='' Width="45" AutoPostBack="False"
                                            CssClass="style5"></asp:TextBox>
                                    </FooterTemplate>
                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:LibResources, Label447 %>">
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="qty" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.qty"),0) %>'
                                            Width="23" AutoPostBack="True" OnTextChanged="Qty_Update" CssClass="style5"></asp:TextBox>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txt_new_qty" runat="server" Text='' Width="23" AutoPostBack="False"
                                            CssClass="style5"></asp:TextBox>
                                    </FooterTemplate>
                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <font color="white"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label197 %>" /></font>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="SelectThis" runat="server" CssClass="style5" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <b><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label655 %>" /></b>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkBoxWarr" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="ChkBoxWarr_CheckedChanged"
                                            CssClass="style5" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chk_warr" runat="server" Enabled="false" CssClass="style5" />
                                    </FooterTemplate>
                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Center" />
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" VerticalAlign="Top" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <b>Ext. Price</b>
                                    </HeaderTemplate>
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="ext_prc" runat="server" Text='<%# FormatNumber(CDbl(DataBinder.Eval(Container, "DataItem.qty"))*CDbl(DataBinder.Eval(Container, "DataItem.ret_prc")),2,-1,0,0) %>'
                                            Width="53" AutoPostBack="True" Enabled="false" ReadOnly="true"></asp:TextBox>
                                        <br />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button runat="server" ID="btn_add_item" Text="<%$ Resources:LibResources, Label10 %>" Width="50px" Height="21px"
                                            OnClick="Add_SKU_to_Order" CssClass="style5"></asp:Button>
                                    </FooterTemplate>
                                    <FooterStyle HorizontalAlign="Center" />
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="LINE" HeaderText="LINE" Visible="False"></asp:BoundColumn>
                                <asp:EditCommandColumn Visible="False" CancelText="&lt;img src='images/icons/Undo24.gif' border='0'&gt;"
                                    UpdateText="&lt;img src='images/icons/Save24.gif' border='0'&gt;" EditText="&lt;img src='images/icons/Edit24.gif' border='0'&gt;"></asp:EditCommandColumn>
                                <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="treated" HeaderText="Treated" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="treatable" HeaderText="Treatable" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="itm_tp_cd" HeaderText="Itm_tp_cd" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="warr_itm_link" HeaderText="WARR_itm_link" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="warr_row_link" HeaderText="WARR_B" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="warrantable" HeaderText="warrantable" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="warr_days" HeaderText="WARR_DAYS" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="inventory" HeaderText="WARR_BY_LN" Visible="False"></asp:BoundColumn>
                            </Columns>
                            <FooterStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" VerticalAlign="Top" />
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" VerticalAlign="Top" />
                        </asp:DataGrid>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>         
                 <dx:TabPage Text="<%$ Resources:LibResources, Label164 %>">
                            <ContentCollection>
                                <dx:ContentControl ID="ccDiscounts" runat="server">
                                    <ucDisc:Discount runat="server" id="ucDiscount" />
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>            
            <dx:TabPage Text="<%$ Resources:LibResources, Label441 %>">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div class="style5" width="100%">
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label6 %>" />:&nbsp; &nbsp;<asp:TextBox ID="txt_acct_balance" runat="server" Enabled="false" ReadOnly="True"
                                CssClass="style5" Width="102px" Text="$0.00"></asp:TextBox>
                            <br />
                            <br />
                            <asp:DataGrid ID="DataGrid1" runat="server" AllowSorting="True" AlternatingItemStyle-BackColor="Beige"
                                AutoGenerateColumns="False" CellPadding="3" DataKeyField="ar_trn_pk" Height="16px"
                                PagerStyle-HorizontalAlign="Right" PagerStyle-Mode="NumericPages" Width="99%" CssClass="style5">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label249 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="style5"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="post_dt" HeaderText="<%$ Resources:LibResources, Label143 %>" SortExpression="post_dt asc"
                                        DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TRN_TP_CD" HeaderText="<%$ Resources:LibResources, Label624 %>" SortExpression="trn_tp_cd asc"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="STAT_CD" HeaderText="<%$ Resources:LibResources, Label571 %>" SortExpression="stat_cd asc"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mop_cd" HeaderText="<%$ Resources:LibResources, Label322 %>" SortExpression="mop_cd asc"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="amt" DataFormatString="{0:N2}" HeaderText="<%$ Resources:LibResources, Label28 %>" SortExpression="amt asc">
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label44 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_balance" runat="server" CssClass="style5" Text="0"></asp:Label><br />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Right" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ivc_cd" HeaderText="" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Mode="NumericPages" />
                                <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" ForeColor="Black" CssClass="style5" />
                            </asp:DataGrid>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="<%$ Resources:LibResources, Label448 %>">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div class="style5" width="100%">
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label449 %>" />:&nbsp; &nbsp;<asp:TextBox ID="txt_quote_summary" runat="server" Enabled="false" ReadOnly="true"
                                CssClass="style5" Width="102px" Text="$0.00"></asp:TextBox>
                            <br />
                            <br />
                            <asp:DataGrid ID="gridQuoteHistory" runat="server" AllowSorting="True" AlternatingItemStyle-BackColor="Beige"
                                AutoGenerateColumns="False" CellPadding="3"
                                DataKeyField="REL_NO" Height="16px" Width="99%" CssClass="style5">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label459 %>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="style5"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="WR_DT" DataFormatString="{0:MM/dd/yyyy}" HeaderText="<%$ Resources:LibResources, Label668 %>"
                                        SortExpression="WR_DT asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="FOLLOW_UP_DT" DataFormatString="{0:MM/dd/yyyy}" HeaderText="<%$ Resources:LibResources, Label212 %>"
                                        SortExpression="FOLLOW_UP_DT asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PROSPECT_TOTAL" DataFormatString="{0:c}" HeaderText="Total"
                                        SortExpression="PROSPECT_TOTAL asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="STORE_CD" HeaderText="<%$ Resources:LibResources, Label572 %>" SortExpression="STORE_CD asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="REL_STATUS" HeaderText="<%$ Resources:LibResources, Label571 %>" SortExpression="REL_STATUS asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="REL_NO" HeaderText="<%$ Resources:LibResources, Label459 %>" SortExpression="REL_NO asc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundColumn>
                                </Columns>
                                <HeaderStyle Font-Bold="True" ForeColor="Black" />
                            </asp:DataGrid>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="<%$ Resources:LibResources, Label95 %>">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table width="100%" height="100%" class="style5">
                            <tr>
                                <td><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label212 %>" />:&nbsp;&nbsp;<asp:TextBox ID="txt_follow_up_dt" runat="server" Width="70"
                                    CssClass="style5"></asp:TextBox>&nbsp;&nbsp;
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <strong><span style="text-decoration: underline"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label240 %>" />:</span></strong><br />
                                    <asp:TextBox ID="txt_comments" runat="server" Rows="3" TextMode="MultiLine" Height="69px"
                                        Width="624px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <asp:TextBox ID="txt_past_Comments" runat="server" Rows="3" TextMode="MultiLine"
                                        Height="75px" Width="624px" ReadOnly="True" Enabled="false"></asp:TextBox>&nbsp;<br />
                                    <br />
                                    <strong><span style="text-decoration: underline"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label743 %>" />:<br />
                                    </span></strong>
                                    <asp:TextBox ID="txt_cust_comments" runat="server" Height="69px" Rows="3" TextMode="MultiLine"
                                        Width="624px"></asp:TextBox><br />
                                    <br />
                                    <asp:TextBox ID="txt_cust_past" runat="server" Height="75px" Rows="3" TextMode="MultiLine"
                                        Width="624px" ReadOnly="True" Enabled="false"></asp:TextBox>
                                    <br />
                                    <br />
                                    <asp:Button ID="btn_save_comments" runat="server" Text="<%$ Resources:LibResources, Label521 %>" Width="119px"
                                        CssClass="style5" />
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
    <br />
    <asp:Label ID="lbl_warning" runat="server" CssClass="style5" ForeColor="Red" Width="100%"></asp:Label><br />
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btn_Lookup" runat="server" Text="<%$ Resources:LibResources, Label280 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_Save" runat="server" Text="<%$ Resources:LibResources, Label521 %>" Enabled="False">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label83 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_convert" runat="server" Text="<%$ Resources:LibResources, Label107 %>">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_quote" runat="server" Text="View Quote" Visible="False">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_total_margins" runat="server" Text="<%$ Resources:LibResources, Label545 %>" Visible="False"
                    OnClick="btn_total_margins_Click">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btn_copy" runat="server" Text="Copy Relationship" Visible="False">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <asp:Label ID="lbl_tax_rate" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lbl_tax_cd" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lbl_PD" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lbl_cust_tp" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lbl_tet_cd" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lbl_exempt_id" runat="server" Visible="False"></asp:Label><br />
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btnConfigureEmail" runat="server" Text="<%$ Resources:LibResources, Label102 %>" Visible="False">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxCheckBox ID="chk_email" runat="server" Text="<%$ Resources:LibResources, Label182 %>" AutoPostBack="true" OnCheckedChanged="chk_email_CheckedChanged"
                    Visible="False">
                </dx:ASPxCheckBox>
            </td>
        </tr>
    </table>
    <dx:ASPxLabel ID="lbl_email_msg" runat="server" Text="<%$ Resources:LibResources, Label613 %>" Visible="False">
    </dx:ASPxLabel>
     <dx:ASPxLabel ID="lbl_desc" runat="server" ForeColor="Red" Width="684px" EncodeHtml="false">
    </dx:ASPxLabel>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="ASPxPopupControl1" AllowDragging="True"
        AllowResize="True" ContentUrl="~/Email_Templates.aspx" HeaderText="Update Email Template"
        Height="345px" Width="546px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl3" runat="server" AllowDragging="True"
        AllowResize="True" HeaderText="Inventory Availability" Height="345px" Width="546px"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl6" runat="server" CloseAction="CloseButton"
        HeaderText="Gross Margin" Height="104px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="317px">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                <asp:Label ID="lbl_gm" runat="server"></asp:Label>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" ClientInstanceName="ASPxPopupControl2" AllowDragging="True"
        AllowResize="True" ContentUrl="~/Relationship_Copy.aspx" HeaderText="Copy Existing Relationship"
        Height="500px" Width="646px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="PopupControlTax" runat="server" ClientInstanceName="PopupControlTax" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label592 %>" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="420px" Width="670px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <div style="width: 100%; height: 420px; overflow-y: scroll;">
                    <ucTax:TaxUpdate runat="server" ID="ucTaxUpdate" />
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

    <dxpc:ASPxPopupControl ID="popupMgrOverride" runat="server" ClientInstanceName="popupMgrOverride"
        HeaderText="Manager Approval" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Modal="true"
        AllowDragging="true" ShowCloseButton="false" ShowPageScrollbarWhenModal="true" Width="400px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="pcccMgrOverride" runat="server">
                <ucMgr:ManagerOverride runat="server" ID="ucMgrOverride" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

    <asp:HiddenField ID="hidUpdLnNos" runat="server" ClientIDMode="Static" />
    <ucPC:SummaryPriceChangeApproval runat="server" ID="ucSummaryPrcChange" ClientIDMode="Static" />
    <ucWar:Warranties runat="server" ID="ucWarranties" ClientIDMode="Static" />
    <ucRelatedSku:RelatedSKUs runat="server" ID="ucRelatedSku" ClientIDMode="Static" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div align="right" class="style5" style="padding-right: 12px;">
        <table style="margin-right: 0px;" border="0">
            <tr>
                <td align="right">
                    <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label578 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lblSubtotal" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; white-space: nowrap; vertical-align: middle" valign="top">
                    <table cellpadding="0" cellspacing="0" style="float: right;">
                        <tr>
                            <td>
                                <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label585%>">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <table id="tblTaxCd" runat="server" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel runat="server" Text="(">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkModifyTax" runat="server" ToolTip="Click to modify the tax code." CssClass="dxeBase_Office2010Blue">
                                            </asp:LinkButton></td>
                                        <td>
                                            <dx:ASPxLabel runat="server" Text=")">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <dx:ASPxLabel runat="server" Text=":">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lblTax" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label151 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lblDelivery" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td colspan="2" nowrap="nowrap">
                    <hr />
                </td>
            </tr>
            <tr>
                <td align="right" style="white-space: nowrap">
                    <dx:ASPxLabel runat="server" Text="<%$ Resources:LibResources, Label224 %>">
                    </dx:ASPxLabel>
                </td>
                <td align="right" style="width: 41px">
                    <dx:ASPxLabel ID="lbltotal" runat="server" Text="$0.00">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>

    <ucMsg:MsgPopup runat="server" ID="ucMsgPopup" />

</asp:Content>
