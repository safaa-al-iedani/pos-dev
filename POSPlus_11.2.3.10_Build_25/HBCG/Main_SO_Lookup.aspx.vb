Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Web.UI
Imports System.Configuration
Imports System.Collections.Generic
Imports HBCG_Utils

Partial Class Main_SO_Lookup
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack = True Then
            GridView1_Binddata()
        End If
    End Sub

    Protected Sub cbo_records_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_records.SelectedIndexChanged
        Session("SOCACHED") = Nothing
        GridView2.PageIndex = 0
        'GridView2.PageSize = cbo_records.SelectedValue
        GridView2.SettingsPager.PageSize = cbo_records.SelectedValue
        GridView1_Binddata()

    End Sub

    Private Sub GridView1_Binddata()

        Dim datSet As DataSet
        Dim dv As DataView

        GridView2.DataSource = ""
        GridView2.Visible = True
        'Dim reqTemp As New OrderUtils.OrderQueryRequest
        'if the request is from IndependentPaymentProcessing.aspx, then consider the Query string, otherwise use the object stored insession "orderRequest"
        Dim req As New OrderRequestDtc
        If Request("referrer") = "IndependentPaymentProcessing.aspx" Then
            req.docNum = Request("del_doc_num")
            req.empCdOp = Session("EMP_CD")
            req.wrDt = Request("so_wr_dt")
            req.ordTp = Request("ord_tp_cd")
            req.custCd = Request("cust_cd")
            req.statCd = Request("stat_cd")
            req.shipFName = Request("f_name")
            req.shipLName = Request("l_name")
            req.shipHPhone = Request("h_phone")
            req.shipBPhone = Request("bphone")
            req.shipAddr1 = Request("addr1")
            req.shipAddr2 = Request("addr2")
            req.shipcity = Request("city")
            req.shipState = Request("state")
            req.shipZip = Request("zip")
            req.puDel = Request("pd")
            req.puDelDt = Request("pd_dt")
            req.soStore = Request("store_cd")
            req.corpName = Request("corp_name")
            req.slspInit1 = Request("slsp1")
            req.slspInit2 = Request("slsp2")
            req.allForSlspCd = ""
            req.custTpCd = ""
            req.shipToName = True
            req.shipToHPhone = True
            req.confStatusCd = Request("confStatusCd")
        Else
            Try
                req = CType(Session("OrderRequest"), OrderRequestDtc)
            Catch ex As Exception
            End Try

        End If

        'datSet = OrderUtils.LookupOrders(reqTemp)
        If Session("SOCACHED") Is Nothing Then
            Dim ObjSalesBiz As SalesBiz = New SalesBiz()
            datSet = ObjSalesBiz.LookupOrders(req)
            Session.Add("SOCACHED", datSet)
        Else
            datSet = DirectCast(Session("SOCACHED"), DataSet)
        End If
        If SystemUtils.dataSetHasRows(datSet) Then

            Dim MyTable As DataTable = New DataTable
            Dim numrows As Integer
            dv = datSet.Tables(0).DefaultView
            MyTable = datSet.Tables(0)
            numrows = MyTable.Rows.Count
            Dim datRow As DataRow = MyTable.Rows(0)

            If SystemUtils.dataRowIsNotEmpty(datRow) Then

                If numrows = 1 Then

                    If Request("referrer") = "IndependentPaymentProcessing.aspx" Then
                        Dim query_string As String = ""
                        If Request("ip_csh_pass") & "" <> "" Then
                            query_string = query_string & "ip_csh_pass=" & Request("ip_csh_pass") & "&"
                        End If
                        If Request("ip_store_cd") & "" <> "" Then
                            query_string = query_string & "ip_store_cd=" & Request("ip_store_cd") & "&"
                        End If
                        If Request("ip_csh_drw") & "" <> "" Then
                            query_string = query_string & "ip_csh_drw=" & Request("ip_csh_drw") & "&"
                        End If
                        If Request("pmt_tp_cd") & "" <> "" Then
                            query_string = query_string & "pmt_tp_cd=" & Request("pmt_tp_cd") & "&"
                        End If
                        If Request("ip_cust_cd") & "" <> "" Then
                            query_string = query_string & "ip_cust_cd=" & Request("ip_cust_cd") & "&"
                        End If
                        query_string = query_string & "search=true"
                        Response.Redirect("IndependentPaymentProcessing.aspx?ip_ivc_cd=" & datRow.Item("DEL_DOC_NUM") & "&" & query_string)

                    Else
                        Response.Redirect("SalesOrderMaintenance.aspx?del_doc_num=" & datRow.Item("DEL_DOC_NUM") & "&query_returned=Y")
                    End If
                Else
                    GridView2.DataSource = dv
                    GridView2.DataBind()
                End If
            Else
                GridView2.Visible = False
            End If

        Else
            Label1.Text = "No results were found using your search criteria."
            Label1.Visible = True
        End If
        ' Daniela French
        'lbl_pageinfo.Text = "Page " + CStr(GridView2.PageIndex + 1) + " of " + CStr(GridView2.PageCount)
        lbl_pageinfo.Text = "Page " + CStr(GridView2.PageIndex + 1) + " " + Resources.LibResources.Label686 + " " + CStr(GridView2.PageCount)
        ' make all the buttons visible if page count is more than 0
        If GridView2.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True


        ' then turn off buttons that don't make sense
        If GridView2.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If GridView2.PageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If GridView2.PageIndex = (GridView2.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        If GridView2.PageCount = 0 Then
            Label1.Text = "Sorry, no sales order records found.  Please, <a href=""SalesORderMaintenance.aspx"">search again</a>"
            Label1.Visible = True
            'cmd_add_new.Visible = True
        End If

    End Sub

    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If GridView2.PageIndex < (GridView2.PageCount - 1) Then
                    GridView2.PageIndex += 1
                End If
            Case "Prev"
                If GridView2.PageIndex > 0 Then
                    GridView2.PageIndex -= 1
                End If
            Case "Last"
                GridView2.PageIndex = GridView2.PageCount - 1
            Case Else
                GridView2.PageIndex = 0
        End Select
        GridView1_Binddata()

    End Sub

    Protected Sub btn_search_again_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search_again.Click

        If Request("referrer") = "IndependentPaymentProcessing.aspx" Then
            Dim query_string As String = ""
            If Request("ip_csh_pass") & "" <> "" Then
                query_string = query_string & "ip_csh_pass=" & Request("ip_csh_pass") & "&"
            End If
            If Request("ip_store_cd") & "" <> "" Then
                query_string = query_string & "ip_store_cd=" & Request("ip_store_cd") & "&"
            End If
            If Request("ip_csh_drw") & "" <> "" Then
                query_string = query_string & "ip_csh_drw=" & Request("ip_csh_drw") & "&"
            End If
            If Request("pmt_tp_cd") & "" <> "" Then
                query_string = query_string & "pmt_tp_cd=" & Request("pmt_tp_cd") & "&"
            End If
            If Request("ip_cust_cd") & "" <> "" Then
                query_string = query_string & "ip_cust_cd=" & Request("ip_cust_cd") & "&"
            End If
            query_string = query_string & "search=true"
            Response.Redirect("IndependentPaymentProcessing.aspx?" & query_string)
        Else
            Response.Redirect("SalesOrderMaintenance.aspx")
        End If
        Session("SOCACHED") = Nothing
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        HBCG_Utils.Update_Theme()
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If
    End Sub

    Protected Sub GridView1_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView2.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        'If Not IsNothing(e.GetValue("DEL_DOC_NUM").ToString()) Then 'Daniela fix
        If e.GetValue("DEL_DOC_NUM") IsNot Nothing Then
            Dim ASPxMenu2 As DevExpress.Web.ASPxMenu.ASPxMenu = TryCast(GridView2.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "ASPxMenu2"), DevExpress.Web.ASPxMenu.ASPxMenu)
            If Not IsNothing(ASPxMenu2) Then
                If Request("referrer") = "IndependentPaymentProcessing.aspx" Then
                    Dim query_string As String = ""
                    If Request("ip_csh_pass") & "" <> "" Then
                        query_string = query_string & "ip_csh_pass=" & Request("ip_csh_pass") & "&"
                    End If
                    If Request("ip_store_cd") & "" <> "" Then
                        query_string = query_string & "ip_store_cd=" & Request("ip_store_cd") & "&"
                    End If
                    If Request("ip_csh_drw") & "" <> "" Then
                        query_string = query_string & "ip_csh_drw=" & Request("ip_csh_drw") & "&"
                    End If
                    If Request("pmt_tp_cd") & "" <> "" Then
                        query_string = query_string & "pmt_tp_cd=" & Request("pmt_tp_cd") & "&"
                    End If
                    If Request("ip_cust_cd") & "" <> "" Then
                        query_string = query_string & "ip_cust_cd=" & Request("ip_cust_cd") & "&"
                    End If
                    'to indicate to the independentPaymentProcessing page that a refresh is needed since a CC pmt got added
                    query_string = query_string & "search=true"
                    ASPxMenu2.RootItem.Items(0).NavigateUrl = "IndependentPaymentProcessing.aspx?ip_ivc_cd=" & e.GetValue("DEL_DOC_NUM").ToString() & "&" & query_string
                Else
                    ASPxMenu2.RootItem.Items(0).NavigateUrl = "SalesOrderMaintenance.aspx?del_doc_num=" & e.GetValue("DEL_DOC_NUM").ToString() & "&query_returned=Y"
                End If
            End If
        End If

    End Sub
    Protected Sub grid_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
        Dim dv As DataTable = New DataTable()
        Dim dView As DataView
        If Session("SOCACHED") IsNot Nothing Then
            dv = DirectCast(Session("SOCACHED"), DataSet).Tables(0)
            dView = dv.DefaultView
        Else

        End If
        
        GridView2.DataSource = dView
    End Sub

End Class
