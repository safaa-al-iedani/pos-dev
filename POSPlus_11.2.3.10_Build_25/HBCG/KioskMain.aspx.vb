Imports System.Data.OracleClient

Partial Class KioskMain
    Inherits POSBasePage

    Public Sub bindgrid()


        Dim sql As String
        Dim ds As DataSet
        Dim start_timestamp As Date
        Dim timestamp As Date
        timestamp = FormatDateTime(Now.Date, DateFormat.ShortDate)
        start_timestamp = timestamp.AddDays(-30)

        ds = New DataSet
        sql = "SELECT COUNT(KIOSK_SERIAL_ID) AS ORIG_COUNT, STORE_CD "
        sql = sql & "FROM CUSTOM_CUST_KIOSK_AUDIT "
        sql = sql & "WHERE DT_TIME BETWEEN TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') AND TO_DATE('" & timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
        sql = sql & "GROUP BY STORE_CD"

        Using conn As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, False), _
            objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn, False), oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objSql, False)

            conn.Open()

            oAdp.Fill(ds)
            Dim dt As DataTable = ds.Tables(0)

            Dim view As New DataView(dt)
            view.Sort = "ORIG_COUNT DESC"

            Gridview1.DataSource = view
            Gridview1.DataBind()

        End Using


    End Sub

    Protected Sub Gridview1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

        Dim sql As String = ""
        Dim start_timestamp As Date
        Dim timestamp As Date
        timestamp = FormatDateTime(Now.Date, DateFormat.ShortDate)
        start_timestamp = timestamp.AddDays(-30)

        If IsNumeric(e.Item.Cells(1).Text) Then
            Dim lbl_kiosk_info As Label = CType(e.Item.FindControl("lbl_kiosk_info"), Label)
            Dim hpl_store_details As HyperLink = CType(e.Item.FindControl("hpl_store_details"), HyperLink)
            hpl_store_details.NavigateUrl = "KioskAnalysis.aspx?store_cd=" & e.Item.Cells(0).Text
            lbl_kiosk_info.Text = "Applications received: " & e.Item.Cells(1).Text
            Dim lbl_store_info As Label = CType(e.Item.FindControl("lbl_store_info"), Label)
            lbl_store_info.Text = "Store Code: " & e.Item.Cells(0).Text & "<br />"
            sql = "SELECT STORE.STORE_NAME, 'Tel. ' || STORE_PHONE.PHONE AS STORE_PHONE, STORE.ADDR1 || ', ' || STORE.CITY || ', ' || STORE.ST  || ' ' || STORE.ZIP_CD AS STORE_ADDRESS "
            sql = sql & "FROM STORE, STORE_PHONE WHERE STORE_PHONE.PHONE_TP_CD(+)='PHN' "
            sql = sql & "AND STORE.STORE_CD=STORE_PHONE.STORE_CD(+) "
            sql = sql & "AND STORE.STORE_CD ='" & e.Item.Cells(0).Text & "'"

            Using Connection As OracleConnection = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, False)

                Connection.Open()

                Using objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, Connection, False), _
                    MyDataReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(objSql, False)
                    'Store Values in String Variables 
                    If MyDataReader.Read() Then
                        lbl_store_info.Text = lbl_store_info.Text & MyDataReader.Item("STORE_NAME").ToString & "<br />"
                        lbl_store_info.Text = lbl_store_info.Text & MyDataReader.Item("STORE_ADDRESS").ToString & "<br />"
                        lbl_store_info.Text = lbl_store_info.Text & MyDataReader.Item("STORE_PHONE").ToString & "<br />"
                    End If
                End Using

                sql = "SELECT CUST_APPROVAL_CODE, COUNT(CUST_APPROVAL_CODE) AS ORIG_COUNT, SUM(CUST_CREDIT_LIMIT) AS CREDIT_LIMIT "
                sql = sql & "FROM CUSTOM_CUST_KIOSK_AUDIT "
                sql = sql & "WHERE DT_TIME BETWEEN TO_DATE('" & start_timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') AND TO_DATE('" & timestamp.ToString("MM/dd/yyyy") & "','mm/dd/RRRR') "
                sql = sql & "AND STORE_CD='" & e.Item.Cells(0).Text & "' "
                sql = sql & "GROUP BY CUST_APPROVAL_CODE "

                Using objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, Connection, False), _
                    MyDataReader As OracleDataReader = DisposablesManager.BuildOracleDataReader(objSql, False)
                    Dim Curr_dt As String = ""
                    Dim approvals As Integer = 0
                    Dim declines As Integer = 0
                    Dim credit_limit As Integer = 0
                    Dim html As String = ""

                    html = "<br /><table width=""100%"">"

                    Do While MyDataReader.Read
                        If MyDataReader.Item("CUST_APPROVAL_CODE").ToString = "APPROVED" Then
                            approvals = approvals + MyDataReader.Item("ORIG_COUNT").ToString
                            credit_limit = credit_limit + MyDataReader.Item("CREDIT_LIMIT").ToString
                        Else
                            declines = declines + MyDataReader.Item("ORIG_COUNT").ToString
                        End If
                    Loop
                    html = html & "<tr>"
                    html = html & "<td valign=""top"">Approvals: <br />" & approvals & "</td>"
                    html = html & "<td valign=""top"">Declines: <br />" & declines & "</td>"
                    html = html & "<td valign=""top"">Average Credit Limit: <br />" & FormatCurrency(credit_limit / approvals, 0) & "</td>"
                    html = html & "<td valign=""top"">Approval %: <br />" & FormatPercent(approvals / (approvals + declines), 2) & "</td>"
                    html = html & "</tr>"
                    html = html & "</table><br />"
                    lbl_kiosk_info.Text = lbl_kiosk_info.Text & html
                    MyDataReader.Close()

                End Using
            End Using

        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        bindgrid()

    End Sub
End Class
