<%--Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="true"
    CodeFile="PaymentServerQueryGridResults.aspx.vb" Inherits="PaymentServerQueryGridResults" uiculture="auto" %>
<%--Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server">
        <dx:ASPxGridView ID="GridView" runat="server" Width="100%" AutoGenerateColumns="False"
            KeyFieldName="PET_PS_INVOICE" EnableCallBacks="False" OnDataBinding="grid_DataBinding">
            <SettingsBehavior AllowSort="true" />
           <%-- <Settings ShowFilterRow="true" />--%>
            <SettingsPager Visible="False">
            </SettingsPager>
            <SettingsBehavior AllowSort="False" />
            <Columns>
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                <%--<dx:GridViewDataTextColumn Caption="Row#" FieldName="COLUMN_ROWID" VisibleIndex="0" Width="25px">
                <DataItemTemplate>
                <dx:ASPxLabel ID="LABEL_ROWID" runat="server" Text='<%# Container.ItemIndex + 1 %>'></dx:ASPxLabel>
                </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="DBRowNum" FieldName="ROW_NUM" VisibleIndex="1" Settings-AllowSort="False" Width="0px" Visible="false">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Transaction Date" FieldName="PET_DATE" VisibleIndex="2" Settings-AllowSort="True" Width="90px">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Terminal ID" FieldName="TERMINAL_ID" VisibleIndex="3" Settings-AllowSort="True" Width="70px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Store Code" FieldName="STORE_CD" VisibleIndex="4" Settings-AllowSort="True" Width="75px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Sales Order" FieldName="PET_PS_INVOICE" VisibleIndex="5" Settings-AllowSort="True" Width="95px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="PET_CUST_CD" VisibleIndex="6" Settings-AllowSort="True" Width="90px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Transaction Type" FieldName="PET_TRANS_TYPE" VisibleIndex="7" Settings-AllowSort="True" Width="180px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Card Type" FieldName="PET_CARD_TYPE" VisibleIndex="8" Settings-AllowSort="True" Width="140px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Amount" FieldName="PET_AMOUNT" VisibleIndex="9" Settings-AllowSort="True" Width="60px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Auth#" FieldName="PET_AUTH_NUM" VisibleIndex="10" Settings-AllowSort="True" Width="80px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Transaction Msg" FieldName="PET_DSP_TRANS_MSG" VisibleIndex="11" Settings-AllowSort="True" Width="175px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Card#" FieldName="PET_CARD_NUMBER_MASKED" VisibleIndex="12" Settings-AllowSort="True" Width="120px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Entry Type" FieldName="PET_ENTRY_TYPE" VisibleIndex="13" Settings-AllowSort="True" Width="80px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Plan#" FieldName="PET_PLAN_NUM" VisibleIndex="14" Settings-AllowSort="True" Width="70px">
                </dx:GridViewDataTextColumn> --%>
                
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_row"  FieldName="COLUMN_ROWID" VisibleIndex="0" Width="25px">
                <DataItemTemplate>
                <dx:ASPxLabel ID="LABEL_ROWID" runat="server" Text='<%# Container.ItemIndex + 1 %>'></dx:ASPxLabel>
                </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn  meta:resourcekey="gv_col_dbrownum"  FieldName="ROW_NUM" VisibleIndex="1" Settings-AllowSort="False" Width="0px" Visible="false">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_transaction_date"  FieldName="PET_DATE" VisibleIndex="2" Settings-AllowSort="True" Width="90px">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_terminal_id"  FieldName="TERMINAL_ID" VisibleIndex="3" Settings-AllowSort="True" Width="70px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_store_code"  FieldName="STORE_CD" VisibleIndex="4" Settings-AllowSort="True" Width="75px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_sales_order"  FieldName="PET_PS_INVOICE" VisibleIndex="5" Settings-AllowSort="True" Width="95px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_customer_id"  FieldName="PET_CUST_CD" VisibleIndex="6" Settings-AllowSort="True" Width="90px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_tran_type"  FieldName="PET_TRANS_TYPE" VisibleIndex="7" Settings-AllowSort="True" Width="180px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_card_type" FieldName="PET_CARD_TYPE" VisibleIndex="8" Settings-AllowSort="True" Width="140px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_amount" FieldName="PET_AMOUNT" VisibleIndex="9" Settings-AllowSort="True" Width="60px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_auth" FieldName="PET_AUTH_NUM" VisibleIndex="10" Settings-AllowSort="True" Width="80px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_tran_msg" FieldName="PET_DSP_TRANS_MSG" VisibleIndex="11" Settings-AllowSort="True" Width="175px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_card" FieldName="PET_CARD_NUMBER_MASKED" VisibleIndex="12" Settings-AllowSort="True" Width="120px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_entry_type" FieldName="PET_ENTRY_TYPE" VisibleIndex="13" Settings-AllowSort="True" Width="80px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn meta:resourcekey="gv_col_plan" FieldName="PET_PLAN_NUM" VisibleIndex="14" Settings-AllowSort="True" Width="70px">
                </dx:GridViewDataTextColumn>
                <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                <dx:GridViewDataTextColumn VisibleIndex="15" Settings-AllowSort="True" Width="50px">
                    <DataItemTemplate>
                        <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                            <Items>
                                <dx:MenuItem Text="" NavigateUrl="logout.aspx" Image-IconID="zoom_zoom_16x16">
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <table width="100%">
            <tr>
                <td align="center">
                    <table width="75%">
                        <tr>
                            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
                            <%--<td width="25%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td> --%>
                            

                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First" 
                                    CommandArgument="First" Visible="false" meta:resourcekey="btn_first">
                                </dx:ASPxButton>
                            </td>
                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev" 
                                     CommandArgument="Prev" Visible="false" meta:resourcekey="btn_prev">
                                </dx:ASPxButton>
                            </td>
                            <td width="25%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                     CommandArgument="Next" Visible="False" meta:resourcekey="btn_next">
                                </dx:ASPxButton>
                            </td>
                            <td width="25%" align="center">
                               <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                    CommandArgument="Last" Visible="false" meta:resourcekey="btn_last">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td></tr>
        </table>
    </div>
    <div runat="server" id="Div1">
        <div class="global_page_normaltext" style="width: 300px; padding-top: 4px;">
            <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts--%>
            <%-- <asp:Label ID="Label2" runat="server" Text="Label" Visible="false"></asp:Label>
            &nbsp;Records to Show:&nbsp;<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True"
                CssClass="style5" Style="position: relative">--%>
           <asp:Label meta:resourcekey="lbl_records_to_show" runat="server" Text="Label" Visible="True"></asp:Label>
            &nbsp;&nbsp;<asp:DropDownList ID="cbo_records" runat="server" AutoPostBack="True"
                CssClass="style5" Style="position: relative">
            <%--Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends--%>
                <asp:ListItem Selected="True" Value="10">10</asp:ListItem>
                <asp:ListItem Value="25">25</asp:ListItem>
                <asp:ListItem Value="50">50</asp:ListItem>
                <asp:ListItem Value="100">100</asp:ListItem>
                <asp:ListItem Value="9999">ALL</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <br />
    <br />
    
    <div runat="server" id="div_version">
    <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
    </dx:ASPxLabel>
    </div>

</asp:Content>
