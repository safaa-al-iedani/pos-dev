Imports System.Data.OracleClient
Imports SalesUtils

Partial Class customorderentry
    Inherits POSBasePage

    Public Sub bindgrid()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim SKUS As String
        SKUS = Request("itm_cd")
        SKUS = Replace(SKUS, ",", "','")
        GridView1.DataSource = ""

        GridView1.Visible = True
        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT SPO_OPT_ITM.ITM_CD, SPO_OPT_ITM.SEQ, SPO_OPTION.DES, SPO_OPT_ITM.OPTION_CD, SPO_OPT_ITM.OPTION_VE_CD, SPO_OPTION.OPTION_TP "
        sql = sql & "FROM SPO_OPT_ITM, SPO_OPTION "
        sql = sql & "WHERE SPO_OPTION.VE_CD = SPO_OPT_ITM.OPTION_VE_CD "
        sql = sql & "AND SPO_OPT_ITM.OPTION_CD = SPO_OPTION.OPTION_CD "
        sql = sql & "AND SPO_OPT_ITM.ITM_CD='" & SKUS & "' "
        sql = sql & "ORDER BY SPO_OPT_ITM.SEQ"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        ds = New DataSet
        oAdp.Fill(ds)
        GridView1.DataSource = ds
        GridView1.DataBind()

        Dim MyDataReader As OracleDataReader
        'Close the existing connection to pull back SKU data
        oAdp.Dispose()
        ds.Dispose()
        objSql.Dispose()

        sql = "SELECT ITM_CD, DES, VSN FROM ITM WHERE ITM_CD='" & SKUS & "'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read) Then
                lbl_SKU.Text = "Custom Order Entry Selection for SKU " & MyDataReader.Item(0).ToString
                lbl_DESC.Text = MyDataReader.Item(1).ToString
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_error.Text = ""
        If Not IsPostBack Then
            bindgrid()
        End If

    End Sub

    Protected Sub GridView1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView1.ItemDataBound

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        Dim opt_ve_cd As String
        Dim option_cd As String
        Dim option_type As String

        option_type = e.Item.Cells(2).Text.ToString()
        opt_ve_cd = e.Item.Cells(1).Text.ToString()
        option_cd = e.Item.Cells(0).Text.ToString()

        If opt_ve_cd = "&nbsp;" Or opt_ve_cd & "" = "" Then
            'Do nothing
        Else
            sql = "SELECT SPO_OPT_VAL.DES, SPO_OPT_VAL.OPTION_VAL "
            sql = sql & "FROM SPO_OPT_VAL_CP_ITM, SPO_OPT_VAL "
            sql = sql & "WHERE SPO_OPT_VAL_CP_ITM.GRADE = SPO_OPT_VAL.GRADE "
            sql = sql & "AND SPO_OPT_VAL_CP_ITM.OPTION_VE_CD = SPO_OPT_VAL.VE_CD "
            sql = sql & "AND SPO_OPT_VAL_CP_ITM.ITM_CD='" & Request("itm_cd") & "' "
            sql = sql & "AND SPO_OPT_VAL_CP_ITM.OPTION_VE_CD='" & opt_ve_cd & "' "
            sql = sql & "AND SPO_OPT_VAL_CP_ITM.OPTION_CD='" & option_cd & "' "
            sql = sql & "AND SPO_OPT_VAL.AVAIL='Y' "
            'sql = sql & "AND NVL(SPO_OPT_VAL.START_DT,SYSDATE-5) <= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
            'sql = sql & "AND NVL(SPO_OPT_VAL.DROP_DT,SYSDATE+300) >= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
            sql = sql & "AND SPO_OPT_VAL.OPTION_TP='" & option_type & "' "
            sql = sql & "GROUP BY SPO_OPT_VAL.DES, SPO_OPT_VAL.OPTION_VAL "
            sql = sql & "ORDER BY SPO_OPT_VAL.DES "

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            Dim list As DropDownList = e.Item.FindControl("cbo_option")
            list.Items.Clear()


            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read) Then
                    list.DataSource = ds
                    list.DataValueField = "OPTION_VAL"
                    list.DataTextField = "DES"
                    list.DataBind()
                Else
                    'Give them everything!
                    MyDataReader.Close()
                    oAdp.Dispose()
                    ds.Dispose()
                    objSql.Dispose()

                    sql = "SELECT SPO_OPT_VAL.DES, SPO_OPT_VAL.OPTION_VAL "
                    sql = sql & "FROM SPO_OPT_VAL_CP_ITM, SPO_OPT_VAL "
                    sql = sql & "WHERE SPO_OPT_VAL_CP_ITM.OPTION_VE_CD = SPO_OPT_VAL.VE_CD "
                    sql = sql & "AND SPO_OPT_VAL_CP_ITM.ITM_CD='" & Request("itm_cd") & "' "
                    sql = sql & "AND SPO_OPT_VAL_CP_ITM.OPTION_VE_CD='" & opt_ve_cd & "' "
                    sql = sql & "AND SPO_OPT_VAL_CP_ITM.OPTION_CD='" & option_cd & "' "
                    sql = sql & "AND SPO_OPT_VAL.AVAIL='Y' "
                    sql = sql & "AND SPO_OPT_VAL.OPTION_VAL=SPO_OPT_VAL_CP_ITM.OPTION_VAL "
                    'sql = sql & "AND NVL(SPO_OPT_VAL.START_DT,SYSDATE-5) <= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
                    'sql = sql & "AND NVL(SPO_OPT_VAL.DROP_DT,SYSDATE+300) >= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
                    sql = sql & "GROUP BY SPO_OPT_VAL.DES, SPO_OPT_VAL.OPTION_VAL "
                    sql = sql & "ORDER BY SPO_OPT_VAL.DES "

                    ds = New DataSet
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
                    oAdp.Fill(ds)
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read) Then
                        list.DataSource = ds
                        list.DataValueField = "OPTION_VAL"
                        list.DataTextField = "DES"
                        list.DataBind()
                    Else
                        'Give them everything!
                        MyDataReader.Close()
                        oAdp.Dispose()
                        ds.Dispose()
                        objSql.Dispose()

                        sql = "SELECT SPO_OPT_VAL.DES, SPO_OPT_VAL.OPTION_VAL "
                        sql = sql & "FROM SPO_OPT_VAL "
                        sql = sql & "WHERE SPO_OPT_VAL.VE_CD='" & opt_ve_cd & "' "
                        sql = sql & "AND SPO_OPT_VAL.AVAIL='Y'"
                        sql = sql & "AND SPO_OPT_VAL.OPTION_TP='" & option_type & "' "
                        'sql = sql & "AND NVL(SPO_OPT_VAL.START_DT,SYSDATE-5) <= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
                        'sql = sql & "AND NVL(SPO_OPT_VAL.DROP_DT,SYSDATE+300) >= TO_DATE('" & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') "
                        sql = sql & "GROUP BY SPO_OPT_VAL.DES, SPO_OPT_VAL.OPTION_VAL "
                        sql = sql & "ORDER BY SPO_OPT_VAL.DES "

                        ds = New DataSet
                        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
                        oAdp.Fill(ds)
                        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                        'Store Values in String Variables 
                        If (MyDataReader.Read) Then
                            list.DataSource = ds
                            list.DataValueField = "OPTION_VAL"
                            list.DataTextField = "DES"
                            list.DataBind()
                        End If
                    End If
                End If


                list.Items.Insert(0, "PLEASE SELECT " & e.Item.Cells(3).Text.ToString())
                list.Items.FindByText("PLEASE SELECT " & e.Item.Cells(3).Text.ToString()).Value = ""
                list.SelectedIndex = 0

                'Close Connection 
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

    End Sub

    Protected Sub btn_Combination_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Combination.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim MyDataReader As OracleDataReader

        sql = "SELECT RET_PRC FROM SPO_COMB WHERE ITM_CD='" & Request("itm_cd") & "' AND COMB_NUM='" & txt_combo.Text & "'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read) Then
                lbl_error.Text = txt_combo.Text & " has been accepted."
                'Update the combination number on the order line to reflect the recently entered data
                sql = "UPDATE TEMP_ITM SET ID_NUM=:ID_NUM WHERE SESSION_ID=:SESSION_ID AND ITM_CD=:ITM_CD AND ID_NUM IS NULL"

                Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand


                objSql2.CommandText = sql
                objSql2.Connection = conn
                objSql2.Parameters.Add(":ID_NUM", OracleType.VarChar)
                objSql2.Parameters.Add(":ITM_CD", OracleType.VarChar)
                objSql2.Parameters.Add(":SESSION_ID", OracleType.VarChar)

                objSql2.Parameters(":ID_NUM").Value = txt_combo.Text
                objSql2.Parameters(":ITM_CD").Value = Request("itm_cd")
                objSql2.Parameters(":SESSION_ID").Value = Session.SessionID.ToString.Trim

                objSql2.ExecuteNonQuery()
                objSql2.Parameters.Clear()

                Response.Write("<script language='javascript'>" & vbCrLf)
                Response.Write("var parentWindow = window.parent; ")
                Response.Write("parentWindow.SelectAndClosePopup10();")
                Response.Write("parentWindow.location.href='order_detail.aspx?LEAD=" & Request("LEAD") & "';")
                Response.Write("</script>")
                'Close Connection 
                conn.Close()
            Else
                lbl_error.Text = "The combination number you entered is invalid."
                'Close Connection 
                conn.Close()
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click

        Dim comb_num As String = GetCombinationNo()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand


        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        Try

            'Create the master combination entry in SPO_COMB
            sql = "INSERT INTO SPO_COMB (COMB_NUM, ITM_CD, RET_PRC, COMB_CST, START_DT, DROP_DT) "
            sql = sql & "VALUES (:COMB_NUM, :ITM_CD, 0, 0, TO_DATE(SYSDATE,'dd/mm/RRRR'), NULL)"

            objSql.CommandText = sql
            objSql.Connection = conn
            objSql.Parameters.Add(":COMB_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)

            objSql.Parameters(":COMB_NUM").Value = comb_num
            objSql.Parameters(":ITM_CD").Value = Request("itm_cd")

            objSql.ExecuteNonQuery()
            objSql.Parameters.Clear()

            Dim seq As Integer = 1

            Dim intRow As Integer = 0

            Dim ve_cd As String = ""
            Dim option_cd As String = ""
            Dim option_val As DropDownList

            '
            ' Number of rows in the DataGrid
            '
            Dim intRows As Integer = GridView1.Items.Count - 1
            '
            ' DataGrid rows are really DataGridItems.
            '
            Dim GridItem As DataGridItem

            For intRow = 0 To intRows
                '
                ' Get the grid row.
                '
                GridItem = GridView1.Items(intRow)

                'Retrieves the selected option value
                option_val = DirectCast(GridItem.FindControl("cbo_option"), DropDownList)

                ve_cd = GridItem.Cells(1).Text().Trim()
                option_cd = GridItem.Cells(2).Text().Trim()

                If option_val.SelectedValue & "" <> "" Then
                    'Add the child entries for each option item value into SPO_COMB_OPT
                    sql = "INSERT INTO SPO_COMB_OPT (COMB_NUM, VE_CD, SEQ, OPTION_CD, OPTION_VAL, QTY, OPTION_VE_CD) "
                    sql = sql & "VALUES (:COMB_NUM, :VE_CD, :SEQ, :OPTION_CD, :OPTION_VAL, 1, :OPTION_VE_CD)"

                    objSql.CommandText = sql
                    objSql.Parameters.Add(":COMB_NUM", OracleType.VarChar)
                    objSql.Parameters.Add(":VE_CD", OracleType.VarChar)
                    objSql.Parameters.Add(":SEQ", OracleType.Number)
                    objSql.Parameters.Add(":OPTION_CD", OracleType.VarChar)
                    objSql.Parameters.Add(":OPTION_VAL", OracleType.VarChar)
                    objSql.Parameters.Add(":OPTION_VE_CD", OracleType.VarChar)

                    objSql.Parameters(":COMB_NUM").Value = comb_num
                    objSql.Parameters(":VE_CD").Value = ve_cd
                    objSql.Parameters(":SEQ").Value = seq
                    objSql.Parameters(":OPTION_CD").Value = option_cd
                    objSql.Parameters(":OPTION_VAL").Value = option_val.SelectedValue
                    objSql.Parameters(":OPTION_VE_CD").Value = ve_cd

                    objSql.ExecuteNonQuery()
                    objSql.Parameters.Clear()
                End If

                seq = seq + 1
            Next

            'Update the combination number on the order line to reflect the recently entered data
            sql = "UPDATE TEMP_ITM SET ID_NUM=:ID_NUM WHERE SESSION_ID=:SESSION_ID AND ITM_CD=:ITM_CD AND ID_NUM IS NULL"

            objSql.CommandText = sql
            objSql.Connection = conn
            objSql.Parameters.Add(":ID_NUM", OracleType.VarChar)
            objSql.Parameters.Add(":ITM_CD", OracleType.VarChar)
            objSql.Parameters.Add(":SESSION_ID", OracleType.VarChar)

            objSql.Parameters(":ID_NUM").Value = comb_num
            objSql.Parameters(":ITM_CD").Value = Request("itm_cd")
            objSql.Parameters(":SESSION_ID").Value = Session.SessionID.ToString.Trim

            objSql.ExecuteNonQuery()
            objSql.Parameters.Clear()

        Catch ex As Exception
            Throw New Exception("Error in combination number creation ", ex)

        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup10();")
        Response.Write("parentWindow.location.href='order_detail.aspx?LEAD=" & Request("LEAD") & "';")
        Response.Write("</script>")

    End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class
