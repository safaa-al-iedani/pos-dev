<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Invoice_Admin.aspx.vb" Inherits="Invoice_Admin" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="frmsubmit" runat="server">
        <b><u>Default Invoice File:</u></b>
        <br />
        <asp:DropDownList ID="cbo_default_file" runat="server" Height="24px" Width="268px" CssClass="style5">
        </asp:DropDownList>&nbsp;&nbsp;<asp:Button ID="btn_default" runat="server" Text="Save Default Invoice" CssClass="style5" />
        <br />
        <br />
        <asp:DataGrid ID="Gridview1" runat="server" AutoGenerateColumns="False" CellPadding="2"
            DataKeyField="store_cd" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" Height="144px"
            Width="673px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" OnItemDataBound="PopulateFiles" OnDeleteCommand="DoItemSave">
            <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
            <HeaderStyle Font-Italic="False" Font-Strikeout="False"
                Font-Underline="False" Font-Overline="False" Font-Bold="False" Height="20px"
                HorizontalAlign="Left"></HeaderStyle>
            <Columns>
                <asp:BoundColumn DataField="store_cd" HeaderText="Store CD" ReadOnly="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:BoundColumn>
                 <asp:TemplateColumn HeaderText="Invoice Source File">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                    <ItemTemplate>
                         <asp:DropDownList id="drpInvFile" Runat="server" DataTextField="invoice_file" DataValueField="invoice_files" AutoPostBack="true" CssClass="style5">
                         <asp:ListItem Text="Select A File" Value="" Selected="True"/>
                        </asp:DropDownList>
                </ItemTemplate>    
            </asp:TemplateColumn>
            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Save24.gif' border='0'&gt;">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" VerticalAlign="Top" CssClass="style5" />
                </asp:ButtonColumn>
            </Columns>
            <ItemStyle VerticalAlign="Top" />
        </asp:DataGrid>
        <br />
        <%--<input id="MyFile" type="file" runat="Server" style="width: 398px; height: 24px;" Class="style5">
        <input id="file_submit" type="submit" value="Upload" onserverclick="Upload_Click"
            runat="Server" style="width: 89px" Class="style5">
        <span id="Span1" style="color: red" runat="Server" /><span id="Span2" style="color: red"
            runat="Server" />
        <br />
        <div id="UploadDetails" visible="False" runat="Server" Class="style5">
            File Name: <span id="FileName" runat="Server" />
            <br>
            File Content: <span id="FileContent" runat="Server" />
            <br>
            File Size: <span id="FileSize" runat="Server" />bytes
            <br>
        </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_msg" runat="server" Text="The following stores were found and added:<br />" Visible=false CssClass="style5"></asp:Label>
</asp:Content>
