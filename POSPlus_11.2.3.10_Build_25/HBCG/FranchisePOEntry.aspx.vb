﻿Imports DevExpress.Web.Data
Imports System.Data
Imports System.Data.OracleClient
Imports System.Collections.Generic
Imports DevExpress.Web.ASPxEditors

Imports HBCG_Utils
Partial Class FranchisePOEntry
    Inherits POSBasePage

    Private objFrPOBiz As FranchisePOBiz = New FranchisePOBiz()
    Private objTransDtc As TransDtc = New TransDtc
    Private objCartItems As CartItemDtc = New CartItemDtc
    Const _MAX_LINE_ITEMS As Int16 = 20
    Private _PAGE_VALID As String = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            storeCdPopulate()

            'Get Vendor

            With ddlVendorCd
                .DataSource = objFrPOBiz.GetVendorCD_LEFranchise()
                .ValueField = "ve_cd"
                .TextField = "ve_name"
                .DataBind()
            End With



            'ddlVendorCd.Items.Insert(0, New ListEditItem(" -Select- "))

            ASPxDateEdit.Value = DateTime.Parse(Date.Today).ToString("dd-MMM-yyyy")
            SetInitialRow()

            btnConfirmASP.Enabled = True
            btnClearAll.Visible = False

        End If

    End Sub


    ''' <summary>
    ''' populates store code drop down and sets default to home store code; uses data security if enabled
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub storeCdPopulate()
        Dim s_home_store_code = Session("HOME_STORE_CD")
        Dim userType As Double = -1
        Dim sup_user_flag As String = String.Empty

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            sup_user_flag = Session("str_sup_user_flag")
        End If
        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")
        Dim emp_init As String = Session("emp_init")
        'Dim datSet As DataSet = objFrPOBiz.GetStores_MCCL1(sup_user_flag, userType, coCd)
        Dim datSet As DataSet = objFrPOBiz.GetStores_FRPOE(sup_user_flag, emp_init)

        If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then

            With ddl_StoreCds
                .DataSource = datSet
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

            'ddl_StoreCds.Items.Insert(0, New ListEditItem(" -Select- "))

        End If

        If Not IsNothing(Session("HOME_STORE_CD")) Then

            Try
                ddl_StoreCds.ValueField = Session("HOME_STORE_CD").ToString
            Catch argEx As ArgumentOutOfRangeException
                ' setup is wrong - probably no stores show - never saw this fail but slsp did so playing it safe
            End Try
        End If

    End Sub

    ''' <summary>
    ''' populates store code drop down and sets default to home store code; uses data security if enabled
    ''' </summary>
    ''' <remarks></remarks>
    ''' 

    Private Sub PopulateOrderFromStoreCode(ByVal fr_store_cd As String)
        Try
            Dim sup_user_flag As String = String.Empty

            If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
                sup_user_flag = Session("str_sup_user_flag")
            End If

            Dim datSet As DataSet = objFrPOBiz.GetCorpStoresforFrPOEntry(sup_user_flag, fr_store_cd)
            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then

                With ddlOrdCorpStoreCd
                    .DataSource = datSet
                    '.ValueField = "corp_store_cd"
                    '.TextField = "store_name"
                    .ValueField = "store_cd"
                    .TextField = "full_desc"
                    .DataBind()
                End With

                'ddl_StoreCds.Items.Insert(0, New ListEditItem(" -Select- "))
            End If
        Catch ex As Exception
            Throw ex
        End Try


    End Sub



    ''' <summary>
    ''' populates Zone Codes drop down by store code;
    ''' </summary>
    ''' <remarks></remarks>
    'Private Sub GetZoneCodesForCorpStore(ByVal p_OrderFromCorpStore As String)
    '    Try
    '        Dim datSet As DataSet = objFrPOBiz.GetZoneCodesForCorpStore(p_OrderFromCorpStore)

    '        If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then

    '            With ddlCorpDelZoneCd
    '                .DataSource = datSet
    '                .DataValueField = "del_zone_cd"
    '                .DataTextField = "des"
    '                .DataBind()
    '            End With

    '        End If
    '    Catch argEx As ArgumentOutOfRangeException
    '        Throw argEx
    '    End Try

    'End Sub
    ''' <summary>
    ''' Jan 10,2018 - mariam
    ''' when franchise store code is selected from dropdown, get franshise coporate store code, franshice customer code,  store name and display
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub cbo_StoreCds_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim s_store_code As String = ddl_StoreCds.Value

        'Get Franchise Store Details - whse_store_cd, storename, fr_cust_cd
        GetFrStoreDetails(s_store_code)

        'OrderingFromStore Code Populate
        ' PopulateOrderFromStoreCode()

        'GetBuyers
        Dim coCd As String = Session("CO_CD")
        Dim datSet As DataSet = objFrPOBiz.GetBuyersByCompCD(coCd)
        With cbo_Buyers
            .DataSource = datSet
            .ValueField = "emp_init"
            .TextField = "emp_init"
            .DataBind()
        End With

        PopulateOrderFromStoreCode(s_store_code)

    End Sub


    ''' <summary>
    ''' Jan 30,2018 - mariam
    ''' when Vendorcode is selected from dropdown, TAT_CD 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ddlVendorCd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim vendor_code As String = ddlVendorCd.Value
        Dim taxcode As String = String.Empty
        'Get PO Tax Code
        taxcode = objFrPOBiz.GetTaxCodeByVendorCd(vendor_code)
        txtPOTaxCode.Text = taxcode
    End Sub

    ''' <summary>
    ''' Jan 30,2018 - mariam
    ''' when Vendorcode is selected from dropdown, TAT_CD 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ddlOrdCorpStoreCd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        ''GetZoneCode
        'GetZoneCodesForCorpStore(ddlOrdCorpStoreCd.Value)

        Try
            'Dim datSet As DataSet = objFrPOBiz.GetZoneCodesForCorpStore(ddlOrdCorpStoreCd.Value)

            Dim datSet As DataSet = objFrPOBiz.GetZonesForCorpStoreCdBySelectedFranchiseStore_Franchise(ddlOrdCorpStoreCd.Value, lblCorpFrStoreNo.Text.Trim())

            If SystemUtils.dataSetHasRows(datSet) AndAlso SystemUtils.dataRowIsNotEmpty(datSet.Tables(0).Rows(0)) Then

                With ddlCorpDelZoneCd
                    .DataSource = datSet
                    .DataValueField = "ZONE_CD"
                    .DataTextField = "full_desc"
                    .DataBind()
                End With
            Else
                ddlCorpDelZoneCd.Items.Clear()
            End If
        Catch argEx As ArgumentOutOfRangeException
            Throw argEx
        End Try
    End Sub

    Protected Sub rdbCreate_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("MODE") = "CREATE"
        dvCreate.Style("display") = "block"

        btnSearch.Visible = False
        btnClear.Visible = False
        lblQuery_DocNum.Visible = False
        lblQuery_ConfirmNum.Visible = False
        txtQuery_DocNum.Visible = False
        txtQuery_ConfirmationNum.Visible = False
        lblMessage.Text = String.Empty
        ASPxDateEdit.Value = DateTime.Parse(Date.Today).ToString("dd-MMM-yyyy")
        ASPxDateEdit.Enabled = False

        cbo_Buyers.Enabled = True
        ddlOrdCorpStoreCd.Enabled = True
        ddlVendorCd.Enabled = True
        ddlCorpDelZoneCd.Enabled = True
        txtShipVia.Enabled = True
        txtPOSortCode.Text = "CPO"

        dvQuery.Style("display") = "none"

    End Sub
    Protected Sub rdbQuery_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("MODE") = "QUERY"
        dvCreate.Style("display") = "none"

        btnSearch.Visible = True
        btnClear.Visible = True
        'lblQuery_DocNum.Visible = True
        lblQuery_ConfirmNum.Visible = True
        'txtQuery_DocNum.Visible = True
        txtQuery_ConfirmationNum.Visible = True

        ASPxDateEdit.Text = String.Empty
        ASPxDateEdit.Enabled = False
        'ddl_StoreCds.Items.Insert(0, New ListEditItem(String.Empty, String.Empty))
        ddl_StoreCds.SelectedIndex = -1

        cbo_Buyers.Enabled = False
        cbo_Buyers.SelectedIndex = -1
        ddlOrdCorpStoreCd.Enabled = False
        ddlOrdCorpStoreCd.SelectedIndex = -1
        ddlVendorCd.Enabled = False
        ddlVendorCd.SelectedIndex = -1
        ddlCorpDelZoneCd.Items.Clear()
        ddlCorpDelZoneCd.Enabled = False
        txtPOSortCode.Text = String.Empty
        lblFrCustCd.Text = String.Empty
        lblCorpFrStoreNo.Text = String.Empty
        lblCorpFrStoreName.Text = String.Empty

        txtShipVia.Enabled = False

        lblMessage.Text = String.Empty

        dvQuery.Style("display") = "block"

    End Sub
    Private Sub SetInitialRow()
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow = Nothing
        dt.Columns.Add(New DataColumn("RowNumber", GetType(String)))
        dt.Columns.Add(New DataColumn("Col1", GetType(String)))
        dt.Columns.Add(New DataColumn("Col2", GetType(String)))
        dt.Columns.Add(New DataColumn("Col3", GetType(String)))
        dt.Columns.Add(New DataColumn("Col4", GetType(String)))
        dt.Columns.Add(New DataColumn("Col5", GetType(String)))
        dt.Columns.Add(New DataColumn("Col6", GetType(String)))
        dt.Columns.Add(New DataColumn("Col7", GetType(String)))
        dt.Columns.Add(New DataColumn("Col8", GetType(String)))
        dr = dt.NewRow()
        dr("RowNumber") = 1
        dr("Col1") = String.Empty
        dr("Col2") = String.Empty
        dr("Col3") = String.Empty
        dr("Col4") = String.Empty
        dr("Col5") = String.Empty
        dr("Col6") = String.Empty
        dr("Col7") = String.Empty
        dr("Col8") = String.Empty
        dr("Col8") = String.Empty

        dt.Rows.Add(dr)

        ViewState("CurrentTable") = dt

        grvItems.DataSource = dt
        grvItems.DataBind()

        Session("linenumber") = 1
    End Sub


    Private Sub AddNewRow()
        Dim rowIndex As Integer = 0

        Try
            If Not ViewState("CurrentTable") Is Nothing Then

                Dim dtCurrentTable As DataTable = CType(ViewState("CurrentTable"), DataTable)
                Dim drCurrentRow As DataRow = Nothing

                If dtCurrentTable.Rows.Count > 0 Then

                    Dim i As Integer
                    For i = 1 To dtCurrentTable.Rows.Count Step i + 1

                        'extract the TextBox values

                        Dim box1 As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txtCorpSKU"), TextBox)
                        Dim box2 As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtVSN"), TextBox)
                        Dim box3 As TextBox = CType(grvItems.Rows(rowIndex).Cells(3).FindControl("txtDesc"), TextBox)
                        Dim box4 As DropDownList = CType(grvItems.Rows(rowIndex).Cells(4).FindControl("ddlQty"), DropDownList)
                        Dim box5 As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtPOCost"), TextBox)
                        Dim box6 As TextBox = CType(grvItems.Rows(rowIndex).Cells(3).FindControl("txtExtPOCost"), TextBox)
                        Dim box7 As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txtSOCost"), TextBox)
                        Dim box8 As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtExtSOCost"), TextBox)

                        'Dim box5 As TextBox = CType(grvItems.Rows(rowIndex).Cells(5).FindControl("txtPOCost"), TextBox)
                        'Dim box6 As TextBox = CType(grvItems.Rows(rowIndex).Cells(6).FindControl("txtExtPOCost"), TextBox)
                        'Dim box7 As TextBox = CType(grvItems.Rows(rowIndex).Cells(7).FindControl("txtSOCost"), TextBox)
                        'Dim box8 As TextBox = CType(grvItems.Rows(rowIndex).Cells(8).FindControl("txtExtSOCost"), TextBox)


                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = i + 1

                        dtCurrentTable.Rows(i - 1)("Col1") = box1.Text
                        If (box1.Text.Length = 0) Then
                            Return
                        Else
                            dtCurrentTable.Rows(i - 1)("Col2") = box2.Text
                            dtCurrentTable.Rows(i - 1)("Col3") = box3.Text
                            dtCurrentTable.Rows(i - 1)("Col4") = box4.Text
                            dtCurrentTable.Rows(i - 1)("Col5") = box5.Text
                            dtCurrentTable.Rows(i - 1)("Col6") = box6.Text
                            dtCurrentTable.Rows(i - 1)("Col7") = box7.Text
                            dtCurrentTable.Rows(i - 1)("Col8") = box8.Text

                            rowIndex = rowIndex + 1
                        End If
                    Next

                    dtCurrentTable.Rows.Add(drCurrentRow)
                    ViewState("CurrentTable") = dtCurrentTable


                    grvItems.DataSource = dtCurrentTable
                    grvItems.DataBind()

                End If
            Else
                Response.Write("ViewState is null")
            End If
        Catch ex As Exception
            Throw (ex)
        End Try
        SetPreviousData()
    End Sub

    Private Sub SetPreviousData()
        Dim rowIndex As Integer = 0

        If Not ViewState("CurrentTable") Is Nothing Then
            Dim dt As DataTable = CType(ViewState("CurrentTable"), DataTable)
            If dt.Rows.Count > 0 Then

                Dim i As Integer
                For i = 0 To dt.Rows.Count - 1 Step i + 1

                    Dim box1 As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txtCorpSKU"), TextBox)
                    Dim box2 As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtVSN"), TextBox)
                    Dim box3 As TextBox = CType(grvItems.Rows(rowIndex).Cells(3).FindControl("txtDesc"), TextBox)
                    Dim box4 As DropDownList = CType(grvItems.Rows(rowIndex).Cells(4).FindControl("ddlQty"), DropDownList)
                    Dim box5 As TextBox = CType(grvItems.Rows(rowIndex).Cells(5).FindControl("txtPOCost"), TextBox)
                    Dim box6 As TextBox = CType(grvItems.Rows(rowIndex).Cells(6).FindControl("txtExtPOCost"), TextBox)
                    Dim box7 As TextBox = CType(grvItems.Rows(rowIndex).Cells(7).FindControl("txtSOCost"), TextBox)
                    Dim box8 As TextBox = CType(grvItems.Rows(rowIndex).Cells(8).FindControl("txtExtSOCost"), TextBox)

                    box1.Text = dt.Rows(i)("Col1").ToString()
                    box2.Text = dt.Rows(i)("Col2").ToString()
                    box3.Text = dt.Rows(i)("Col3").ToString()
                    box4.Text = dt.Rows(i)("Col4").ToString()
                    box5.Text = dt.Rows(i)("Col5").ToString()
                    box6.Text = dt.Rows(i)("Col6").ToString()
                    box7.Text = dt.Rows(i)("Col7").ToString()
                    box8.Text = dt.Rows(i)("Col8").ToString()

                    rowIndex = rowIndex + 1
                Next
            End If
        End If

    End Sub

    Protected Sub grvItems_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        'SetRowData()
        If Not ViewState("CurrentTable") Is Nothing Then
            Dim dt As DataTable = CType(ViewState("CurrentTable"), DataTable)
            Dim drCurrentRow As DataRow = Nothing
            Dim rowIndex As Integer = Convert.ToInt32(e.RowIndex)
            If dt.Rows.Count > 1 Then
                dt.Rows.Remove(dt.Rows(rowIndex))
                drCurrentRow = dt.NewRow()
                ViewState("CurrentTable") = dt
                grvItems.DataSource = dt
                grvItems.DataBind()

                Dim i As Integer
                For i = 0 To grvItems.Rows.Count - 1 - 1 Step i + 1
                    grvItems.Rows(i).Cells(0).Text = Convert.ToString(i + 1)
                Next
                SetPreviousData()
            End If
            CalculateTotals()
        End If
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs)
        'lblMessage.Text = String.Empty
        If Convert.ToInt16(Session("linenumber")) = _MAX_LINE_ITEMS Then
            lblMessage.Text = "Maximum Items to Order is " & _MAX_LINE_ITEMS
        Else
            AddNewRow()
            Session("linenumber") = Convert.ToInt16(Session("linenumber")) + 1
        End If
    End Sub


    Protected Sub GetFrStoreDetails(ByVal p_FrStoreCode As String)
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String
        Dim sqlString As StringBuilder

        'sqlString = New StringBuilder("select fr.whse_store_cd, fr.cust_cd as fr_cust_cd, str.store_name ")
        'sqlString.Append("from FRTBL fr, store str ")
        'sqlString.Append("WHERE fr.store_cd =UPPER('" & p_FrStoreCode & "') and fr.store_cd = str.store_cd ")


        sqlString = New StringBuilder("select str.store_cd,fr.cust_cd as fr_cust_cd, str.store_name, fr.store_num as corporate_franchise_store_cd ")
        sqlString.Append("FROM store str inner join frtbl fr on str.store_cd=fr.store_cd ")
        sqlString.Append("WHERE str.store_cd =UPPER('" & p_FrStoreCode & "') ")

        sql = sqlString.ToString()




        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                lblCorpFrStoreNo.Text = IIf(IsDBNull(dbReader.Item("corporate_franchise_store_cd")), "", dbReader.Item("corporate_franchise_store_cd").ToString())
                lblCorpFrStoreName.Text = IIf(IsDBNull(dbReader.Item("store_name")), "", dbReader.Item("store_name").ToString())
                lblFrCustCd.Text = IIf(IsDBNull(dbReader.Item("fr_cust_cd")), "", dbReader.Item("fr_cust_cd").ToString())
            End If
        Catch ex As Exception
            Dim err As String = ex.ToString()

            'do nothing
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

    End Sub

    Public Sub txtCorpSKU_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'GetVSN based on Corp SKU#
        Dim itemSKU As String = String.Empty
        Dim ds As DataSet
        Dim storeCd As String = "SC"        'get from from control
        Dim VendorCd As String = String.Empty
        Dim strItemSKU As String = "N"
        lblMessage.Text = String.Empty
        Try
            Dim txt As TextBox = (CType((sender), TextBox))
            Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
            Dim rowIndex = gv.RowIndex

            Dim txtCorpSku As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txtCorpSKU"), TextBox)
            Dim txtvsn As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtVSN"), TextBox)
            Dim txtvsnDesc As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtDesc"), TextBox)

            itemSKU = txtCorpSku.Text.ToString().Trim()

            'Validate Item SKU#
            If Not ViewState("CurrentTable") Is Nothing Then
                Dim dt As DataTable = CType(ViewState("CurrentTable"), DataTable)
                If dt.Select("Col1='" + itemSKU + "'").Length > 0 Then
                    lblMessage.Text = "Item SKU already entered"
                    Return
                End If
            End If


            If itemSKU.Substring(0, 2) = "97" Then
                lblMessage.Text = "Items in Minor Code '97' are restricted, please contact Franchise Division"
                txtCorpSku.Text = String.Empty
                Return
            End If

            If objFrPOBiz.isvalidItemforInventoryType(itemSKU) = "N" Then  'Alice added on Aug 27,2018
                lblMessage.Text = "Item code - " & itemSKU & " - must be an inventory item"
                txtCorpSku.Text = String.Empty
                Return
            End If

            strItemSKU = objFrPOBiz.isvaliditmFR(itemSKU, Session("emp_init"))
            If strItemSKU = "N" Or strItemSKU & "" = "" Then
                lblMessage.Text = "Franchise Specific Items cannot be ordered through Corporate, please contact Franchise Division"
                txtCorpSku.Text = String.Empty
                Return

            Else


                'txtCorpSku.Enabled = False          'after validating user should not change it. the row could be deleted
                '----------------------- Get VSN and Description ------------------

                ds = objFrPOBiz.GetVSNByItemSKU(itemSKU)      'Get VSN No and Description based on Item SKU typed

                If ds.Tables(0).Rows.Count > 0 Then
                    txtvsn.Text = ds.Tables(0).Rows(0)("VSN")
                    txtvsnDesc.Text = ds.Tables(0).Rows(0)("Des")
                End If

                'Calculate Costs
                CalculateCosts(rowIndex)
            End If

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub
    Public Sub ddlQty_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim txt As DropDownList = (CType((sender), DropDownList))
        Dim gv As GridViewRow = (CType((txt.NamingContainer), GridViewRow))
        Dim rowIndex = gv.RowIndex

        'Calculate Costs
        CalculateCosts(rowIndex)
    End Sub
    'Validate Approver Password
    Public Sub txtApprover_Pswd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim isValid As String = String.Empty

        If txtApprover_Pswd.Text.Length > 0 Then
            isValid = objFrPOBiz.isvalidApproverPassword(txtApprover_Pswd.Text)

            If isValid = "N" Or isValid & "" = "" Then
                lblMessage.Text = "This Approver Password is not Valid or Authorized"
                Return
            Else
                'Get Approver User Init
                txtApprover_User_Init.Text = objFrPOBiz.GetApprUserInitByPass(txtApprover_Pswd.Text)
            End If
        End If
    End Sub
    Public Sub btnClearAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearAll.Click

        lblFrCustCd.Text = String.Empty
        ddl_StoreCds.SelectedIndex = -1
        lblCorpFrStoreNo.Text = String.Empty
        cbo_Buyers.SelectedIndex = -1

        ddlVendorCd.SelectedIndex = -1
        txtShipVia.Text = String.Empty

        ddlOrdCorpStoreCd.SelectedIndex = -1
        ddlCorpDelZoneCd.Items.Clear()
        ddlCorpDelZoneCd.SelectedIndex = -1


        lblMessage.Text = String.Empty
        txtComment2.Text = String.Empty
        txtComment3.Text = String.Empty
        txtComment4.Text = String.Empty
        txtComment5.Text = String.Empty

        txtSubTotPOCost.Text = String.Empty
        txtPOTaxCode.Text = String.Empty
        txtPOTaxAmount.Text = String.Empty
        txtPOTotal.Text = String.Empty


        txtSubTotSOCost.Text = String.Empty
        txtSOTaxCode.Text = String.Empty
        txtSOTaxAmount.Text = String.Empty
        txtSOTotal.Text = String.Empty

        txtApprover_Pswd.Text = String.Empty
        txtApprover_User_Init.Text = String.Empty
        txtConfirmationNum.Text = String.Empty


        SetInitialRow()

        btnConfirmASP.Enabled = True
        btnClearAll.Visible = False
    End Sub

    'Confirm 
    Public Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmASP.Click
        Const Const_Success As String = "SUCCESS"
        Dim objTrans As TransDtc = New TransDtc
        Dim lstCartItems As New List(Of CartItemDtc)

        Dim row As GridViewRow
        Dim result As String = String.Empty
        Dim rowIndex As Int16 = 0
        Dim SKU As String = String.Empty
        Dim strItemSKU As String = String.Empty
        Try
            'Validate Required Fields
            If IsValidRequiredFields() Then

                objTrans.ID = 0
                objTrans.Del_Doc_Num = String.Empty
                objTrans.FR_Cust_CD = lblFrCustCd.Text.Trim
                objTrans.FR_Store_CD = ddl_StoreCds.Value.ToString.Trim
                objTrans.FR_Store_Num = lblCorpFrStoreNo.Text.Trim.Substring(0, 2)
                objTrans.Buyer_Init = cbo_Buyers.Value.ToString.Trim

                objTrans.VE_CD = ddlVendorCd.Value.ToString.Trim
                objTrans.Ship_Via = txtShipVia.Text.Trim
                objTrans.PO_SRT_CD = txtPOSortCode.Text.Trim
                objTrans.Corp_Store_CD = IIf(ddlOrdCorpStoreCd.Text.Length > 0, ddlOrdCorpStoreCd.Value, "")
                'objTrans.Corp_Zone_CD = ddlCorpDelZoneCd.Value.ToString.Trim
                objTrans.Corp_Zone_CD = IIf(ddlCorpDelZoneCd.SelectedItem.Text.Length > 0, ddlCorpDelZoneCd.SelectedValue, "")
                If ASPxDateEdit.Value Is Nothing Then
                    objTrans.Create_Date = DateTime.Today()
                Else
                    objTrans.Create_Date = Convert.ToDateTime(ASPxDateEdit.Text)
                End If

                'objTrans.PO_CD = ""          'Later program will insert Approver User Initial + Generated Confirmation #

                objTrans.Comments2 = txtComment2.Text.Trim
                objTrans.Comments3 = txtComment3.Text.Trim
                objTrans.Comments4 = txtComment4.Text.Trim
                objTrans.Comments5 = txtComment5.Text.Trim


                objTrans.PO_Sub_Total = txtSubTotPOCost.Text.Trim
                objTrans.PO_TAX_CD = txtPOTaxCode.Text.Trim
                objTrans.PO_TAX_AMT = txtPOTaxAmount.Text.Trim
                objTrans.PO_Total = txtPOTotal.Text.Trim
                objTrans.PO_TAX_AMT = txtPOTaxAmount.Text.Trim

                objTrans.SO_Sub_Total = txtSubTotSOCost.Text.Trim
                objTrans.SO_TAX_CD = txtSOTaxCode.Text.Trim
                objTrans.SO_TAX_AMT = txtSOTaxAmount.Text.Trim
                objTrans.SO_TAX_AMT = txtSOTaxAmount.Text.Trim
                objTrans.SO_Total = txtSOTotal.Text.Trim

                objTrans.Approval_Pswd = txtApprover_Pswd.Text.Trim
                objTrans.Approval_User_Init = txtApprover_User_Init.Text.Trim

                'Get Line item details
                If Not ViewState("CurrentTable") Is Nothing Then

                    Dim dtCurrentTable As DataTable = CType(ViewState("CurrentTable"), DataTable)
                    Dim drCurrentRow As DataRow = Nothing

                    If dtCurrentTable.Rows.Count > 0 Then

                        Dim i As Integer
                        For i = 1 To dtCurrentTable.Rows.Count Step i + 1
                            Dim objCartItems As CartItemDtc = New CartItemDtc

                            objCartItems.LineNo = i
                            SKU = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txtCorpSKU"), TextBox).Text
                            strItemSKU = objFrPOBiz.isvaliditmFR(SKU, Session("emp_init"))
                            If strItemSKU = "N" Or strItemSKU & "" = "" Then
                                lblMessage.Text = "Invalid SKU - " & strItemSKU & " - Please make correction"
                                Return
                            End If
                            If (SKU.Trim.Length > 0) Then
                                objCartItems.ItemCode = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txtCorpSKU"), TextBox).Text
                                objCartItems.Qty = CType(grvItems.Rows(rowIndex).Cells(4).FindControl("ddlQty"), DropDownList).Text
                                objCartItems.PO_CST = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtPOCost"), TextBox).Text
                                objCartItems.PO_Ext_CST = CType(grvItems.Rows(rowIndex).Cells(3).FindControl("txtExtPOCost"), TextBox).Text
                                objCartItems.SO_CST = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txtSOCost"), TextBox).Text
                                objCartItems.SO_Ext_CST = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtExtSOCost"), TextBox).Text

                                lstCartItems.Add(objCartItems)
                                rowIndex = rowIndex + 1
                            End If
                        Next
                    End If


                    'Generate Confirmation Number
                    Dim confirmationNum As String = GetConfirmationNum(objTrans.FR_Store_CD) & "LN" 'add LN to avoid the confirmation number same as number which is produced in E1

                    If ConfirmationNumAlreadyExists(confirmationNum) = "Y" Then
                        lblMessage.Text = "Confirmation # " & txtConfirmationNum.Text & " already exists, Please contact the System Administrator."
                    Else

                        objTrans.Confirm_Num = confirmationNum

                        'Display confirmation Number
                        txtConfirmationNum.Text = objTrans.Confirm_Num

                        'Call Sproc to Insert values into table
                        result = objFrPOBiz.CreateFranchisePO(lstCartItems, objTrans)

                        lblMessage.Text = result
                        If result.ToUpper().Contains(Const_Success) Then
                            lblMessage.ForeColor = Color.Green
                            btnConfirmASP.Enabled = False
                            btnClearAll.Visible = True
                        Else
                            lblMessage.ForeColor = Color.Red
                        End If


                    End If



                Else
                    lblMessage.Text = "Error occurred at Confirm click"
                End If







            End If
        Catch Ex As Exception
            Throw Ex
        End Try
    End Sub

    'Get PO_CD. Added by alice on Dec18, 2018 to avoid duplicated PO_CD
    Public Function GetConfirmationNum(ByVal p_FRStoreCd As String) As String

        Dim confirmNum As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbSql = " select STD_FRPO_UTL.getPOCd (:frStoreCd, trunc(Sysdate)) from dual"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(UCase(dbSql), dbConnection)
            dbCommand.Parameters.Add(":frStoreCd", OracleType.VarChar)
            dbCommand.Parameters(":frStoreCd").Value = p_FRStoreCd
            confirmNum = dbCommand.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw
        Finally
            dbCommand.Dispose()
            dbConnection.Close()
        End Try
        Return confirmNum
    End Function
    'Added by alice on Dec18, 2018
    Function ConfirmationNumAlreadyExists(ByVal p_confirmationNum As String) As String

        Dim v_value As Integer = 0

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT count(*) cnt from frpo " &
                            " where PO_CD = '" & p_confirmationNum & "'"
        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("cnt")
            Else
                v_value = 1
            End If
        Catch
            v_value = 1
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return "Y"
        ElseIf v_value = 0 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function


    Protected Sub CalculateCosts(ByVal rowIndex As Integer)
        Dim vsn As String = String.Empty
        Dim itemSKU As String = String.Empty
        Dim ds As DataSet
        Dim storeCd As String = "SC"        'get from control
        Dim POCost As Double = 0.0
        Dim SOCost As Double = 0.0
        Dim VendorCd As String = String.Empty
        Dim ExtSOCost As Double = 0.0

        Try
            Dim txtCorpSku As TextBox = CType(grvItems.Rows(rowIndex).Cells(1).FindControl("txtCorpSKU"), TextBox)
            Dim ddlQty As DropDownList = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("ddlQty"), DropDownList)
            Dim txtPOCost As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtPOCost"), TextBox)
            Dim txtExtPOCost As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtExtPOCost"), TextBox)
            Dim txtSOCost As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtSOCost"), TextBox)
            Dim txtExtSOCost As TextBox = CType(grvItems.Rows(rowIndex).Cells(2).FindControl("txtExtSOCost"), TextBox)

            itemSKU = txtCorpSku.Text.ToString()
            '------------------------   Get PO Cost -------------------------

            ds = Nothing
            storeCd = If(ddl_StoreCds.SelectedIndex > -1, ddl_StoreCds.Value.ToString.Trim, String.Empty)
            Dim orderCorpStoreCd As String = If(ddlOrdCorpStoreCd.SelectedIndex > -1, ddlOrdCorpStoreCd.SelectedItem.Value.Trim, String.Empty)

            ' POCost = objFrPOBiz.GetPOCostByItemAndStore(itemSKU, storeCd)
            Dim barecost As Double = objFrPOBiz.GetBareCost(itemSKU, orderCorpStoreCd)
            Dim percentValue As Double = objFrPOBiz.GetPercentageValue(itemSKU, storeCd)

            If barecost <> 0 And percentValue <> 0 Then
                POCost = barecost + barecost * percentValue / 100
            End If

            'PO Cost 
            txtPOCost.Text = POCost.ToString("n2")                  '2 decimal 
            txtExtPOCost.Text = Convert.ToDouble(POCost * Convert.ToInt16(ddlQty.SelectedItem.ToString())).ToString("n2")

            'pass  Ordering From Corporate Store Code 
            Dim landedcost As Double = objFrPOBiz.GetLandedCost(itemSKU, ddlOrdCorpStoreCd.Value)
            SOCost = landedcost + landedcost * percentValue / 100
            txtSOCost.Text = SOCost.ToString("n2")                  '2 decimal 

            ExtSOCost = Convert.ToDouble(SOCost * Convert.ToInt16(ddlQty.SelectedItem.ToString()))
            txtExtSOCost.Text = ExtSOCost.ToString("n2")

            'Call Calculate Sub-Total and Totals
            CalculateTotals()


            'if ddlVendorCd not selected - display message to select
            VendorCd = ddlVendorCd.Text

            'Get Tax Codes for SO

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub
    Protected Sub CalculateTotals()
        Dim POSubTotal As Double = 0.0
        Dim SOSubTotal As Double = 0.0
        Dim POTaxAmount As Double = 0.0
        Dim SOTaxAmount As Double = 0.0

        Dim vendor_code As String = ddlVendorCd.Value
        Dim POtaxcode As String = String.Empty
        Dim SOtaxcode As String = String.Empty

        Dim txtExtPOCost As TextBox = CType(grvItems.Rows(0).Cells(2).FindControl("txtExtPOCost"), TextBox)
        Dim txtExtSOCost As TextBox = CType(grvItems.Rows(0).Cells(2).FindControl("txtExtSOCost"), TextBox)
        Dim ddlQty As DropDownList = CType(grvItems.Rows(0).Cells(2).FindControl("ddlQty"), DropDownList)


        'Loop the no of items in grid, total ExtPOCost and ExtSOCost

        Dim row As GridViewRow
        For Each row In grvItems.Rows
            'if using TemplateField columns then you may need to use FindControl method
            Dim tb As TextBox = CType(row.FindControl("txtExtPOCost"), TextBox)
            If tb.Text.Length > 0 Then
                POSubTotal = POSubTotal + Convert.ToDouble(tb.Text)               ' get the value from TextBox
                txtSubTotPOCost.Text = POSubTotal.ToString("n2")
            Else
                txtSubTotPOCost.Text = POSubTotal.ToString("n2")
            End If

            tb = CType(row.FindControl("txtExtSOCost"), TextBox)
            If tb.Text.Length > 0 Then
                SOSubTotal = SOSubTotal + Convert.ToDouble(tb.Text)               ' get the value from TextBox
                txtSubTotSOCost.Text = SOSubTotal.ToString("n2")
            Else
                txtSubTotSOCost.Text = SOSubTotal.ToString("n2")
            End If

        Next

        'Get Tax Code

        'Get PO Tax Code
        POtaxcode = objFrPOBiz.GetTaxCodeByVendorCd(vendor_code)
        txtPOTaxCode.Text = POtaxcode

        'Get SO Tax Code
        SOtaxcode = objFrPOBiz.GetTaxCodeByCustCd(lblFrCustCd.Text)
        txtSOTaxCode.Text = SOtaxcode

        'Find Tax Amount
        POTaxAmount = TaxUtils.getTaxAmt(txtPOTaxCode.Text, DateTime.Now, POSubTotal, "DEL")    'PO Tax Amount
        SOTaxAmount = TaxUtils.getTaxAmt(txtSOTaxCode.Text, DateTime.Now, SOSubTotal, "DEL")    'SO Tax Amount
        txtPOTaxAmount.Text = POTaxAmount.ToString("n2")
        txtSOTaxAmount.Text = SOTaxAmount.ToString("n2")

        'Calculate Totals 
        'Add Total SubPO cost and PO Tax Amount
        txtPOTotal.Text = (POSubTotal + POTaxAmount).ToString("n2")
        txtSOTotal.Text = (SOSubTotal + SOTaxAmount).ToString("n2")

    End Sub
    'Validate Required Fields
    Public Function IsValidRequiredFields() As Boolean
        Dim result As Boolean = False
        Dim requiredMsg As String = String.Empty
        requiredMsg = "Following Values are Mandatory - Franchise Store Code, Buyer Initials, Ordering From Corporate Store code, Corporate Delivery Zone Code, Vendor Code, Approver Password, Approving Associate's Code"
        Try
            If ddl_StoreCds.SelectedIndex = -1 Or
                cbo_Buyers.SelectedIndex = -1 Or
                ddlOrdCorpStoreCd.SelectedIndex = -1 Or
                ddlCorpDelZoneCd.SelectedIndex = -1 Or
                ddlVendorCd.SelectedIndex = -1 Or
                txtApprover_Pswd.Text.Trim.Length = 0 Or txtApprover_User_Init.Text.Trim.Length = 0 Then

                lblMessage.Text = requiredMsg
                result = False
            Else
                result = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return result
    End Function

    Public Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
        lblMessage.Text = String.Empty
        GridView1_Binddata()
        dvQuery.Style("display") = "block"
    End Sub
    'Query - Search initial main result 
    Private Sub GridView1_Binddata()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim CatchMe As Boolean
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim proceed As Boolean = False
        ds = New DataSet
        Dim LikeQry As String
        Dim objTrans As TransDtc = New TransDtc

        gvFRPO.DataSource = ""
        'CustTable.Visible = False
        CatchMe = False


        'Get search criteria from controls
        objTrans.FR_Cust_CD = lblFrCustCd.Text.Trim
            Dim a As Integer = ddl_StoreCds.SelectedIndex
            'objTrans.FR_Store_CD = ddl_StoreCds.Value.ToString.Trim
            objTrans.FR_Store_CD = If(ddl_StoreCds.SelectedIndex > -1, ddl_StoreCds.Value.ToString.Trim, String.Empty)
            objTrans.FR_Store_Num = If(lblCorpFrStoreNo.Text.Length > 0, lblCorpFrStoreNo.Text.Trim.Substring(0, 2), String.Empty)
            objTrans.Buyer_Init = If(cbo_Buyers.SelectedIndex > -1, cbo_Buyers.Value.ToString.Trim, String.Empty)

            objTrans.VE_CD = If(ddlVendorCd.SelectedIndex > -1, ddlVendorCd.Value.ToString.Trim, String.Empty)
            objTrans.Ship_Via = txtShipVia.Text.Trim
            objTrans.PO_SRT_CD = txtPOSortCode.Text.Trim
            objTrans.Corp_Store_CD = IIf(ddlOrdCorpStoreCd.Text.Trim.Length > 0, ddlOrdCorpStoreCd.Value, "")
            'objTrans.Corp_Zone_CD = ddlCorpDelZoneCd.Value.ToString.Trim
            If ddlCorpDelZoneCd.Items.Count > 0 Then
                objTrans.Corp_Zone_CD = ddlCorpDelZoneCd.SelectedItem.Value
            Else
                objTrans.Corp_Zone_CD = String.Empty
            End If
            objTrans.Del_Doc_Num = txtQuery_DocNum.Text.Trim
            objTrans.Confirm_Num = txtQuery_ConfirmationNum.Text.Trim

            'If ASPxDateEdit.Value Is Nothing Then
            '    objTrans.Create_Date = DBNull.Value
            'Else
            '    objTrans.Create_Date = Convert.ToDateTime(ASPxDateEdit.Text)
            'End If

            'If Request("ist") & "" = "" And Request("store_cd") & "" = "" And Request("dest_store_cd") & "" = "" _
            '    And Request("zone_cd") & "" = "" And Request("stat_cd") & "" = "" And Request("transfer_dt") & "" = "" Then
            '    CatchMe = True
            'End If
            gvFRPO.Visible = True
            ' If CatchMe = False Then
            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT * FROM FRPO WHERE "
            'FR_Cust_CD
            If objTrans.FR_Cust_CD & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.FR_Cust_CD, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CUST_CD " & LikeQry & " '" & Replace(objTrans.FR_Cust_CD, "'", "''") & "' "
                proceed = True
            End If
            'FR_Store_CD
            If objTrans.FR_Store_CD & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.FR_Store_CD, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "STORE_CD " & LikeQry & " '" & Replace(objTrans.FR_Store_CD, "'", "''") & "' "
                proceed = True
            End If
            'FR_Store_Num
            If objTrans.FR_Store_Num & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.FR_Store_Num, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "STORE_NUM " & LikeQry & " '" & Replace(objTrans.FR_Store_Num, "'", "''") & "' "
                proceed = True
            End If
            'Buyer_Init
            If objTrans.Buyer_Init & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.Buyer_Init, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "BUYER_INIT " & LikeQry & " '" & Replace(objTrans.Buyer_Init, "'", "''") & "' "
                proceed = True
            End If
            'VE_CD
            If objTrans.VE_CD & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.VE_CD, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "VE_CD " & LikeQry & " '" & Replace(objTrans.VE_CD, "'", "''") & "' "
                proceed = True
            End If
            'Ship_Via
            If objTrans.Ship_Via & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.Ship_Via, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "SHIP_VIA " & LikeQry & " '" & Replace(objTrans.Ship_Via, "'", "''") & "' "
                proceed = True
            End If
            'PO_SRT_CD
            If objTrans.PO_SRT_CD & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.PO_SRT_CD, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "PO_SRT_CD " & LikeQry & " '" & Replace(objTrans.PO_SRT_CD, "'", "''") & "' "
                proceed = True
            End If
            'Corp_Store_CD
            If objTrans.Corp_Store_CD & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.Corp_Store_CD, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CORP_STORE_CD " & LikeQry & " '" & Replace(objTrans.Corp_Store_CD, "'", "''") & "' "
                proceed = True
            End If
            'Corp_Store_CD
            If objTrans.Corp_Zone_CD & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.Corp_Zone_CD, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "CORP_ZONE_CD " & LikeQry & " '" & Replace(objTrans.Corp_Zone_CD, "'", "''") & "' "
                proceed = True
            End If

            'Del-Doc-Num
            If objTrans.Del_Doc_Num & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.Del_Doc_Num, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "DEL_DOC_NUM " & LikeQry & " '" & Replace(objTrans.Del_Doc_Num, "'", "''") & "' "
                proceed = True
            End If

            'Confirmation num
            If objTrans.Confirm_Num & "" <> "" Then
                If proceed = True Then sql = sql & "AND "
                If InStr(objTrans.Confirm_Num, "%") > 0 Then
                    LikeQry = "LIKE"
                Else
                    LikeQry = "="
                End If
                sql = sql & "PO_CD " & LikeQry & " '" & Replace(objTrans.Confirm_Num, "'", "''") & "' "
                proceed = True
            End If

        'Create Date
        'If objTrans.Create_Date & "" <> "" And Not IsNothing(objTrans.Create_Date) Then
        '    If proceed = True Then sql = sql & "AND "
        '    LikeQry = "="
        '    'sql = sql & "ENTER_DATE " & LikeQry & " '" & Replace(objTrans.Create_Date, "'", "''") & "' "
        '    sql = sql & "ENTER_DATE " & LikeQry & "TO_DATE('" & objTrans.Create_Date & "','dd/mm/yyyy') "
        '    proceed = True
        'End If

        If proceed Then 'if not all search fields are empty

            Try
                If Right(sql, 6) = "WHERE " Then
                    sql = Replace(sql, "WHERE", "")
                    'lblMessage.Text = "No Search Criteria entered."
                    'Exit Sub
                End If
                sql = UCase(sql)

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
                oAdp.Fill(ds)

                Dim MyTable As DataTable
                Dim numrows As Integer
                dv = ds.Tables(0).DefaultView
                MyTable = New DataTable
                MyTable = ds.Tables(0)
                numrows = MyTable.Rows.Count

                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read) Then
                    If numrows = 1 Then
                        gvFRPO.DataSource = dv
                        gvFRPO.DataBind()
                    Else
                        gvFRPO.DataSource = dv
                        gvFRPO.DataBind()
                    End If
                Else
                    gvFRPO.Visible = False
                End If
                'Close Connection 
                MyDataReader.Close()
                conn.Close()

            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If




        lbl_pageinfo.Text = "Page " + CStr(gvFRPO.PageIndex + 1) + " of " + CStr(gvFRPO.PageCount)
        ' make all the buttons visible if page count is more than 0
        If gvFRPO.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True


        ' then turn off buttons that don't make sense
        If gvFRPO.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If gvFRPO.PageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If gvFRPO.PageIndex = (gvFRPO.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        If gvFRPO.PageCount = 0 Then
            lblMessage.Text = "Sorry, no IST records found.  Please search again." & vbCrLf & vbCrLf
        End If

    End Sub
    'Query - Page Navigation
    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If gvFRPO.PageIndex < (gvFRPO.PageCount - 1) Then
                    gvFRPO.PageIndex += 1
                End If
            Case "Prev"
                If gvFRPO.PageIndex > 0 Then
                    gvFRPO.PageIndex -= 1
                End If
            Case "Last"
                gvFRPO.PageIndex = gvFRPO.PageCount - 1
            Case Else
                gvFRPO.PageIndex = 0
        End Select
        GridView1_Binddata()

    End Sub

    Public Sub GridView1_OnSelectionChanged(ByVal sender As Object, ByVal e As DevExpress.Data.SelectionChangedEventArgs)
        Dim test As String = String.Empty
        test = "this is test"
    End Sub

    Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim CatchMe As Boolean
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim proceed As Boolean
        ds = New DataSet
        Dim LikeQry As String
        Dim objTrans As TransDtc = New TransDtc

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim delDocNum As String = gvFRPO.DataKeys(e.Row.RowIndex).Value.ToString()
            'Dim gvFRPOLn As GridView = TryCast(e.Row.FindControl("gvFRPO_Ln"), GridView)

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
           ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT * FROM FRPO_LN WHERE DEL_DOC_NUM = '" & delDocNum & "'"
            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            Dim MyTable As DataTable
            Dim numrows As Integer
            dv = ds.Tables(0).DefaultView
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            'If numrows > 0 Then
            '    'Data for FRPO_Ln
            '    gvFRPOLn.DataSource = dv
            '    gvFRPOLn.DataBind()
            'End If
        End If
    End Sub

    'Private Sub gvFRPO_Ln_Bind(ByVal p_DelDocNum As String)
    '    Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As OracleDataReader
    '    Dim CatchMe As Boolean
    '    Dim ds As DataSet
    '    Dim oAdp As OracleDataAdapter
    '    Dim dv As DataView
    '    Dim proceed As Boolean
    '    ds = New DataSet
    '    Dim LikeQry As String
    '    Dim objTrans As TransDtc = New TransDtc

    '    If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
    '   ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
    '        Throw New Exception("Connection Error")
    '    Else
    '        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
    '    End If

    '    sql = "SELECT * FROM FRPO_LN WHERE DEL_DOC_NUM = " & p_DelDocNum
    '    'Set SQL OBJECT 
    '    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
    '    oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
    '    oAdp.Fill(ds)

    '    Dim MyTable As DataTable
    '    Dim numrows As Integer
    '    dv = ds.Tables(0).DefaultView
    '    MyTable = New DataTable
    '    MyTable = ds.Tables(0)
    '    numrows = MyTable.Rows.Count

    '    Try
    '        'Open Connection 
    '        conn.Open()
    '        'Execute DataReader 
    '        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

    '        'Store Values in String Variables 
    '        If (MyDataReader.Read) Then
    '            If numrows = 1 Then
    '                gvFRPO.DataSource = dv
    '                gvFRPO.DataBind()
    '            Else
    '                gvFRPO.DataSource = dv
    '                gvFRPO.DataBind()
    '            End If
    '        Else
    '            gvFRPO.Visible = False
    '        End If
    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try
    'End Sub
    'Clear Control Text
    Public Sub btnClear_Click(ByVal sender As Object, ByVal e As EventArgs)
        Const _CREATE As String = "CREATE"
        Const _QUERY As String = "QUERY"

        ASPxDateEdit.Text = String.Empty
        lblFrCustCd.Text = String.Empty
        ddl_StoreCds.SelectedIndex = -1
        lblCorpFrStoreNo.Text = String.Empty
        cbo_Buyers.SelectedIndex = -1

        ddlVendorCd.SelectedIndex = -1
        txtShipVia.Text = String.Empty
        txtPOSortCode.Text = String.Empty
        ddlOrdCorpStoreCd.SelectedIndex = -1
        ddlCorpDelZoneCd.Items.Clear()
        ddlCorpDelZoneCd.SelectedIndex = -1
        lblCorpFrStoreName.Text = String.Empty
        lblMessage.Text = String.Empty

        If (isNotEmpty(Session("MODE")) And Session("MODE").Equals(_QUERY)) Then

            gvFRPO.DataSource = Nothing
            gvFRPO.DataBind()
            dvQuery.Style("display") = "none"

            txtQuery_DocNum.Text = String.Empty
            txtQuery_ConfirmationNum.Text = String.Empty
        End If

        If (isNotEmpty(Session("MODE")) And Session("MODE").Equals(_CREATE)) Then
            txtComment2.Text = String.Empty
            txtComment3.Text = String.Empty
            txtComment4.Text = String.Empty
            txtComment5.Text = String.Empty


            txtSubTotPOCost.Text = String.Empty
            txtPOTaxCode.Text = String.Empty
            txtPOTaxAmount.Text = String.Empty
            txtPOTotal.Text = String.Empty
            txtPOTaxAmount.Text = String.Empty

            txtSubTotSOCost.Text = String.Empty
            txtSOTaxCode.Text = String.Empty
            txtSOTaxAmount.Text = String.Empty
            txtSOTaxAmount.Text = String.Empty
            txtSOTotal.Text = String.Empty

            txtApprover_Pswd.Text = String.Empty
            txtApprover_User_Init.Text = String.Empty

            grvItems.DataSource = Nothing
            grvItems.DataBind()

        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad

    End Sub
End Class
