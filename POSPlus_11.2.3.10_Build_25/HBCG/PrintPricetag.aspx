﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="PrintPricetag.aspx.vb" Inherits="PrintPricetag" Culture="auto" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="700px" >
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_store_cd" runat="server" meta:resourcekey="lbl_store_cd"></dx:ASPxLabel>
            </td>
            <td colspan="3">
                <dx:ASPxComboBox ID="cbo_store_cd" runat="server"
                    Width="350px" IncrementalFilteringMode="Contains" ClientInstanceName="cbo_store_cd">
                    <ClientSideEvents GotFocus="function(s, e) {cbo_store_cd.SelectAll();}" />
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_created_date" runat="server" meta:resourcekey="lbl_created_date"></dx:ASPxLabel>
            </td>
            <td colspan="3">
                <dx:ASPxDateEdit ID="dt_created_date" runat="server" Width="200" EditFormat="Custom"
                    DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" AutoPostBack="true">
                </dx:ASPxDateEdit>
            </td>
        </tr>
        &nbsp;&nbsp;
        <tr>
            <td></td>
            <td class="class_tdlbl" colspan="1" align="left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                <dx:ASPxLabel ID="lbl_OR" runat="server" meta:resourcekey="lbl_or" ForeColor="DarkRed" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_session_id" runat="server" meta:resourcekey="lbl_session_id"></dx:ASPxLabel>
            </td>
            <td colspan="3">
                <dx:ASPxTextBox ID="txt_session_id" runat="server" Width="200" AutoPostBack="True">
                    <ClientSideEvents KeyUp="function(s, e) {
	                                                    s.SetText(s.GetText().toUpperCase());
                                                        }" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btn_search" runat="server" meta:resourcekey="btn_search" Width="100"></dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_clear" runat="server" meta:resourcekey="btn_clear" Width="100"></dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <dx:ASPxLabel ID="lbl_Printer" runat="server" meta:resourcekey="lbl_printer"></dx:ASPxLabel>
            </td>
            <td colspan="3">
                <dx:ASPxComboBox ID="printer_drp" runat="server" Width="100px" DropDownRows="5">
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="center">
                <dx:ASPxGridView ID="GV_Pricetags" runat="server" AutoGenerateColumns="False" Width="550px" 
                    OnDataBinding="GV_PRICETAG_DataBinding">
                     <SettingsPager Position="TopAndBottom">
                        <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                    </SettingsPager>
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="SESSION_ID" Visible="true" VisibleIndex="0" Width="100px" meta:resourcekey="gv_col_session_id"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="STORE_CODE" Visible="true" VisibleIndex="1" Width="100px" meta:resourcekey="gv_col_store_cd"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CREATED_DATE" Visible="true" VisibleIndex="2" Width="100px" meta:resourcekey="gv_col_created_dt"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn  Visible="True" VisibleIndex="3" Width="100px" FieldName="RPT_TYPE" meta:resourcekey="gv_col_rpt_type">
                        </dx:GridViewDataTextColumn>                   
                     </Columns>
                    <Settings ShowFooter="true" />
                    <Templates>
                        <FooterRow>
                            <dx:ASPxLabel ID="GV_Footer_label" runat="server" ClientInstanceName="GV_Footer_label" 
                                Text=" " ForeColor="Red"></dx:ASPxLabel>
                        </FooterRow>
                    </Templates>
                </dx:ASPxGridView>
            </td>
        </tr>
    </table>
    <dx:ASPxLabel ID="lbl_warning" runat="server" Width="100%" ForeColor="Red"  ></dx:ASPxLabel>
    
</asp:Content>
