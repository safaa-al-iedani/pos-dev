Imports System.Data.OracleClient

Partial Class CreditMemoPopulate
    Inherits POSBasePage

    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Proceed.Click

        If cbo_crm_cause_cd.Text & "" = "" Or txt_del_doc_num.Text & "" = "" Then 'Alice updated on June 11, 2019 for request 81
            lbl_warning.Text = Resources.creditmemopopulate.label06
            lbl_warning.ForeColor = Color.Red
            Exit Sub
        End If

        'MM-9227
        Dim sot As New SavedOrderType
        If Not IsNothing(Session("SavedOrderType")) Then
            sot = Session("SavedOrderType")
        End If
        Dim del_doc_num As String
        del_doc_num = UCase(txt_del_doc_num.Text)
        Dim crm_cause_cd As String
        crm_cause_cd = UCase(cbo_crm_cause_cd.Value.ToString)
        Session("Proceed") = True
        ' original document should be SAL; if found, must be SAL; if not found, do ...
        Dim del_doc_tab As DataTable = OrderUtils.GetOrderDetails(del_doc_num)
        If del_doc_tab.Rows.Count > 0 Then
            Dim del_doc As DataRow = del_doc_tab.Rows(0)
            If AppConstants.Order.TYPE_SAL <> del_doc("ORD_TP_CD") Then
                lbl_warning.Text = Resources.creditmemopopulate.Label03
                lbl_warning.ForeColor = Color.Red
                Exit Sub
            End If
            'Else  ' Not found Then maybe invalid or maybe not in current tables - TODO probably should warn user and let them decide to continue or not like E1
            '   lbl_warning.Text = "WARNING: Original Document not on file."

            'check if referenced order is final or not
            If del_doc("STAT_CD") <> "F" Then
                lbl_warning.Text = Resources.creditmemopopulate.Label04
                lbl_warning.ForeColor = Color.Red
                Exit Sub
            End If

            'Alice add for request 4477 on Feb 22, 2019
            Dim co_grp_cd As String = Session("str_co_grp_cd")
            Session("OriginalOrderExist") = "True"
            Session("OriginalDocNum") = del_doc_num
            If co_grp_cd = "BRK" Then
                Dim origSlsp As DataRow = OrderUtils.Get_Orig_Slsp_forExchangeSo(del_doc_num)
                If Not origSlsp Is Nothing Then
                    Session("Orig_slsp1_forExchangeSo") = origSlsp("so_emp_slsp_cd1")
                    Session("Orig_slsp2_forExchangeSo") = origSlsp("so_emp_slsp_cd2")
                    Session("Orig_pct1_forExchangeSo") = origSlsp("pct_of_sale1")
                    Session("Orig_pct2_forExchangeSo") = origSlsp("pct_of_sale2")
                End If

            End If

        Else 'Alice added on May 10, 2019 for request 81
            Session("OriginalOrderExist") = "False"
            lbl_warning.Text = Resources.creditmemopopulate.Label01
            lbl_warning.ForeColor = Color.Red
            If Session("str_co_grp_cd").ToString = "BRK" Then
                btn_Continue.Visible = True
                Session("adj_ivc_cd") = del_doc_num
                Session("crm_cause_cd") = crm_cause_cd
            End If
            Exit Sub
        End If

            Session("adj_ivc_cd") = del_doc_num ' this is confusing - adj_ivc_cd is it NOT - it is the ivc_cd, the SAL del_Doc_num
        'MM-9227
        Session("crm_cause_cd") = crm_cause_cd
        
        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; " & vbCrLf)
        Response.Write("parentWindow.SelectAndClosePopup1();" & vbCrLf)
        'MM-9227
        If ((sot.OrderTypeCode = AppConstants.Order.TYPE_CRM) OrElse (sot.OrderTypeCode = AppConstants.Order.TYPE_MCR) OrElse (sot.OrderTypeCode = AppConstants.Order.TYPE_MDB)) AndAlso (IsNothing(Session("DeleteTempOrder")) OrElse String.IsNullOrEmpty(Session("DeleteTempOrder"))) Then
            Response.Write("parentWindow.location.href='startup.aspx?fin=" & ASPxCheckBox1.Value & "';" & vbCrLf)
        Else
            Response.Write("parentWindow.location.href='startup.aspx?adj_ivc_cd=" & del_doc_num & "&fin=" & ASPxCheckBox1.Value & "&crm_cause_cd=" & cbo_crm_cause_cd.Value & "';" & vbCrLf)

        End If
        Response.Write("</script>")
        'MM-9227
        sot.OrderTypeCode = ""

    End Sub

    Protected Sub btn_Continue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Continue.Click
        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; " & vbCrLf)
        Response.Write("parentWindow.SelectAndClosePopup1();" & vbCrLf)
        Response.Write("parentWindow.location.href='startup.aspx';" & vbCrLf)
        Response.Write("</script>")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") Then
                lbl_warning.Text = Resources.creditmemopopulate.Label05
            Else
                Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

                Dim sot As New SavedOrderType
                If Not IsNothing(Session("SavedOrderType")) Then
                    sot = Session("SavedOrderType")
                End If
                If sot.OrderTypeCode = "" Then
                    sot.OrderTypeCode = Session("re_order_tp_cd")
                End If
                Dim sql As String
                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim ds As New DataSet
                Dim cmdGetCodes As OracleCommand
                cmdGetCodes = DisposablesManager.BuildOracleCommand


                If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                    Throw New Exception("Connection Error")
                Else
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                End If
                conn.Open()
                'MM-9227
                If (Session("ord_tp_cd") <> sot.OrderTypeCode) Then
                    txt_del_doc_num.Text = ""
                    txt_del_doc_num.Enabled = True
                    cbo_crm_cause_cd.Text = ""

                Else
                    If Not String.IsNullOrEmpty(sot.OrderNumber) AndAlso Not IsNothing(sot.OrderTypeCode) AndAlso sot.OrderTypeCode <> AppConstants.Order.TYPE_SAL Then
                        txt_del_doc_num.Text = sot.OrderNumber
                        txt_del_doc_num.Enabled = False
                    Else
                        txt_del_doc_num.Enabled = True
                    End If
                    If Not String.IsNullOrEmpty(sot.CauseCode) AndAlso Not IsNothing(sot.OrderTypeCode) AndAlso sot.OrderTypeCode <> AppConstants.Order.TYPE_SAL Then
                        cbo_crm_cause_cd.Text = sot.CauseCode
                    End If
                End If
                
                If Session("ord_tp_cd") = "CRM" Then
                    sql = "SELECT crm_cause_cd, des, crm_cause_cd || ' - ' || des as full_desc FROM CRM_CAUSE order by CRM_CAUSE_CD"
                Else
                    sql = "SELECT mdc_cause_cd AS crm_cause_cd, des, mdc_cause_cd || ' - ' || des as full_desc FROM mdc_CAUSE order by mdc_CAUSE_CD"
                End If
                Try
                    With cmdGetCodes
                        .Connection = conn
                        .CommandText = sql
                    End With


                    Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
                    oAdp.Fill(ds)

                    With cbo_crm_cause_cd
                        .DataSource = ds
                        .ValueField = "crm_cause_cd"
                        .TextField = "full_desc"
                        .DataBind()
                    End With
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
                conn.Close()
            End If
            Session("crm_cause_cd") = ""
            Session("adj_ivc_cd") = ""

        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class
