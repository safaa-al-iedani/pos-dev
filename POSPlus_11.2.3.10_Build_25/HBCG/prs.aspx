<%@ Page Language="VB" AutoEventWireup="false" CodeFile="prs.aspx.vb" Inherits="cust_find"
    MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server">
        <dx:ASPxGridView ID="GridView1" runat="server" Width="98%" AutoGenerateColumns="False"
            KeyFieldName="CUST_CD">
            <SettingsBehavior AllowSort="False" />
            <SettingsPager Visible="False">
            </SettingsPager>
            <Columns>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label131 %>" FieldName="CORP_NAME" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label263 %>" FieldName="LNAME" Visible="False"
                    VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label211 %>" FieldName="FNAME" Visible="False"
                    VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label22 %>" FieldName="ADDR1" Visible="False" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label79 %>" FieldName="CITY" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label693 %>" FieldName="ST_CD" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label229 %>" FieldName="HOME_PHONE" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label132 %>" FieldName="CUST_CD" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label20 %>" FieldName="ADDR1" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="9">
                    <DataItemTemplate>
                        <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                            <Items>
                                <dx:MenuItem Text="<%$ Resources:LibResources, Label528 %>" NavigateUrl="logout.aspx">
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <%--<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Style="position: relative"
            Width="100%" AllowPaging="True" BorderStyle="None" BorderColor="White" BorderWidth="0px"
            PagerSettings-Visible="false">
            <Columns>
                <asp:BoundField DataField="CORP_NAME" HeaderText="Corp Name" SortExpression="Corporate Name">
                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                </asp:BoundField>
                <asp:BoundField DataField="LNAME" HeaderText="Last Name" SortExpression="Last Name">
                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                </asp:BoundField>
                <asp:BoundField DataField="FNAME" HeaderText="First Name" SortExpression="FIRST NAME">
                </asp:BoundField>
                <asp:BoundField DataField="ADDR1" HeaderText="Address" SortExpression="ADDRESS"></asp:BoundField>
                <asp:BoundField DataField="CITY" HeaderText="City" SortExpression="CITY"></asp:BoundField>
                <asp:BoundField DataField="home_phone" HeaderText="Home Phone" SortExpression="Home Phone">
                </asp:BoundField>
                <asp:BoundField DataField="CUST_CD" HeaderText="Customer Code" SortExpression="CUSTOMER CODE">
                </asp:BoundField>
                <asp:CommandField ShowSelectButton="True" ButtonType="Button">
                    <ControlStyle CssClass="style5" />
                </asp:CommandField>
            </Columns>
            <PagerSettings Visible="False" />
            <HeaderStyle Font-Bold="True" ForeColor="Black" />
            <AlternatingRowStyle BackColor="Beige" />
        </asp:GridView>--%>
        <table id="nav_table" runat="server" width="100%">
            <tr>
                <td align="center">
                    <table width="98%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label210 %>"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label425 %>"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label337 %>"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="<%$ Resources:LibResources, Label261 %>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="20%" align="center">
                                <dx:ASPxButton ID="btn_search_again" runat="server" Text="<%$ Resources:LibResources, Label527 %>" OnClick="btn_search_again_Click">
                                </dx:ASPxButton>
                            </td>
                            <td width="20%" align="center">
                                <dx:ASPxButton ID="cmd_new_cust" runat="server" Text="<%$ Resources:LibResources, Label15 %>">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="Div1">
        <div class="global_page_normaltext" style="width: 300px; padding-top: 4px;">
            <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label451 %>" />:&nbsp;<asp:DropDownList ID="cbo_records" runat="server" AutoPostBack="True"
                Style="position: relative;" CssClass="style5">
                <asp:ListItem Selected="True" Value="10">10</asp:ListItem>
                <asp:ListItem Value="25">25</asp:ListItem>
                <asp:ListItem Value="50">50</asp:ListItem>
                <asp:ListItem Value="100">100</asp:ListItem>
                <asp:ListItem Value="9999" Text="<%$ Resources:LibResources, Label23 %>"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="global_page_normaltext" style="width: 300px; padding-top: 4px;" align="center">
            <asp:Button ID="cmd_add_new" runat="server" Text="<%$ Resources:LibResources, Label15 %>" Visible="false"
                CssClass="style5" />
        </div>
    </div>
</asp:Content>
