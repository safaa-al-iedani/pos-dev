Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization


Partial Class FSAPC
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If IsPostBack Then

            If (Not GridView2.DataSource Is Nothing) Then
                GridView2.DataBind()
            End If

        Else
            srch_fsa.Items.Clear()
            srch_zone_cd.Items.Clear()
            srch_st_cd.Items.Clear()

            populateST_CD()
            populateFSA()
            PopulateZoneCodes()

            srch_fsa.Enabled = True
            srch_zone_cd.Enabled = True
            srch_st_cd.Enabled = True

        End If



    End Sub
    Protected Sub Populate_Results(ByVal p_postal_cd As String, ByVal p_zone_cd As String, ByVal p_st_cd As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If p_zone_cd & "" = "" And p_postal_cd & "" = "" And p_st_cd & "" = "" Then
            'lbl_msg.Text = "Please select a criteria to Search - Province, Zone Code or FSA/PC"
            lbl_msg.Text = Resources.LibResources.Label912
            Exit Sub
        End If

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "select CO_GRP_CD,POSTAL_CD, a.ZONE_CD ZONE_CD,DES, ST_CD, NVL(CHARGES,0) CHARGES "
        sql = sql & " from ops$daemon.zone2postalcode a, zone b "
        sql = sql & " where a.zone_cd = b.zone_cd "
        sql = sql & " and a.postal_cd = nvl('" & p_postal_cd & "', a.postal_cd)"
        sql = sql & " and a.zone_cd   = nvl('" & p_zone_cd & "', a.zone_cd)"
        sql = sql & " and a.st_cd   = nvl('" & p_st_cd & "', a.st_cd)"
        sql = sql & " and a.co_grp_cd = std_multi_co.getcogrp('" & Session("emp_init") & "') "
        sql = sql & " order by co_grp_cd, a.postal_cd, a.zone_cd "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then
                Try
                    GridView2.DataSource = ds
                    GridView2.DataBind()

                Catch ex As Exception
                    conn.Close()
                End Try

                MyDataReader.Close()
                srch_fsa.Enabled = False
                srch_zone_cd.Enabled = False
                srch_st_cd.Enabled = False

            Else
                srch_fsa.Enabled = True
                srch_zone_cd.Enabled = True
                srch_st_cd.Enabled = True

            End If
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw

        End Try

    End Sub
    Private Sub populateFSA()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT distinct postal_cd from ops$daemon.zone2postalcode order by 1"

        srch_fsa.Items.Insert(0, Resources.LibResources.Label918)
        srch_fsa.Items.FindByText(Resources.LibResources.Label918).Value = ""
        srch_fsa.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_fsa
                .DataSource = ds
                .DataValueField = "postal_cd"
                .DataTextField = "postal_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Private Sub populateST_CD()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT distinct st st_cd from store order by 1"

        srch_st_cd.Items.Insert(0, Resources.LibResources.Label919)
        srch_st_cd.Items.FindByText(Resources.LibResources.Label919).Value = ""
        srch_st_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_st_cd
                .DataSource = ds
                .DataValueField = "st_cd"
                .DataTextField = "st_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try



    End Sub
    Private Sub PopulateZoneCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT distinct zone_cd from zone order by zone_cd"

        srch_zone_cd.Items.Insert(0, Resources.LibResources.Label899)
        srch_zone_cd.Items.FindByText(Resources.LibResources.Label899).Value = ""
        srch_zone_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_zone_cd
                .DataSource = ds
                .DataValueField = "zone_cd"
                .DataTextField = "zone_cd"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try


    End Sub
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        GridView2.DataSource = ""
        GridView2.DataBind()

        srch_fsa.SelectedIndex = 0
        srch_zone_cd.SelectedIndex = 0
        srch_st_cd.SelectedIndex = 0

        srch_fsa.Items.Clear()
        srch_zone_cd.Items.Clear()
        srch_st_cd.Items.Clear()

        populateFSA()
        PopulateZoneCodes()
        populateST_CD()

        srch_fsa.Enabled = True
        srch_zone_cd.Enabled = True
        srch_st_cd.Enabled = True

    End Sub
    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        Populate_Results(HttpUtility.UrlEncode(srch_fsa.Text), HttpUtility.UrlEncode(srch_zone_cd.Text), HttpUtility.UrlEncode(srch_st_cd.Text))


    End Sub
    
    Protected Function isValidZone(ByVal p_zone As String) As String

        Dim v_ind As String = String.Empty

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String = "SELECT 'Y' as AAA FROM zone " &
                            " where zone_cd = '" & p_zone & "'"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("AAA")
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_ind & "" = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    Public Function isvalidpostalcd(ByVal p_postal_cd As String) As String

        'Canadian postal codes can't contain the letters D, F, I, O, Q, or U, and cannot start with W or Z
        If Len(p_postal_cd) = 6 Then
            If Regex.IsMatch(UCase(p_postal_cd), "[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ][0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]") Then
                Return "Y"
            Else
                Return "N"
            End If
        ElseIf Len(p_postal_cd) = 3 Then
            If Regex.IsMatch(UCase(p_postal_cd), "[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ]") Then
                Return "Y"
            Else
                Return "N"
            End If
        Else
            Return "N"
        End If


    End Function
    Public Function isvalidstcd(ByVal p_st_cd As String) As String

        If p_st_cd & "" = "" Then
            Return "N"
        ElseIf p_st_cd = "AB" Or p_st_cd = "BC" Or p_st_cd = "MB" Or p_st_cd = "NB" Or
               p_st_cd = "NL" Or p_st_cd = "NS" Or p_st_cd = "NT" Or p_st_cd = "ON" Or
               p_st_cd = "PE" Or p_st_cd = "QC" Or p_st_cd = "SK" Or p_st_cd = "YT" Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function
    Protected Sub GridView2_ParseValue(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxParseValueEventArgs)

        Dim hasError As Boolean
        Dim errorMsg As String = ""

        If e.FieldName = "ZONE_CD" Then
            If e.Value & "" = "" Or isValidZone(UCase(e.Value)) <> "Y" Then
                hasError = True
                'errorMsg = "Invalid Zone Code - Please try again."
                errorMsg = Resources.LibResources.Label914
            End If
        ElseIf e.FieldName = "POSTAL_CD" Then
            If e.Value & "" = "" Or Isvalidpostalcd(UCase(e.Value)) <> "Y" Then
                hasError = True
                'errorMsg = "Invalid POSTAL Code/FSA please try again"
                errorMsg = Resources.LibResources.Label911
            End If
        ElseIf e.FieldName = "ST_CD" Then
            If e.Value & "" = "" Or isvalidstcd(UCase(e.Value)) <> "Y" Then
                hasError = True
                'errorMsg = "Invalid Province Code please try again"
                errorMsg = Resources.LibResources.Label913
            End If
        ElseIf e.FieldName = "CHARGES" Then
            If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then  'charges only for Leons
                If e.Value & "" = "" Then
                    'errorMsg = "Invalid CHARGES - please try again"
                    errorMsg = Resources.LibResources.Label916
                    hasError = True
                Else
                    If Not IsNumeric(e.Value) Then
                        'errorMsg = "Invalid CHARGES - please try again"
                        errorMsg = Resources.LibResources.Label916
                        hasError = True
                    End If
                End If
            Else
                If e.Value & "" = "" Then
                    e.Value = 0
                Else
                    If e.Value <> 0 Then
                        'errorMsg = "Invalid CHARGES - must be zero for " & UCase(Session("CO_CD"))
                        errorMsg = Resources.LibResources.Label917 & " " & UCase(Session("CO_CD"))
                        hasError = True
                    End If
                End If

            End If
        End If

        If hasError Then
            Throw New Exception(errorMsg)
        End If

    End Sub

    Protected Sub GridView2_BatchUpdate(ByVal sender As Object, ByVal e As ASPxDataBatchUpdateEventArgs)

        Dim v_err_found As String = ""
        Dim v_del_ctr As Integer = 0
        Dim v_invalid_return As Integer = 0
        Dim v_msg As String = ""

        For Each args In e.InsertValues
            If DoInsert(args.NewValues) <> True Then
                v_invalid_return = v_invalid_return + 1
            End If
        Next args

        For Each args In e.UpdateValues
            If DoUpdate(args.Keys, args.NewValues, args.OldValues) <> True Then
                v_invalid_return = v_invalid_return + 1
            End If
        Next args

        For Each args In e.DeleteValues
            v_del_ctr = v_del_ctr + 1
            DoDelete(args.Keys, args.Values)
        Next args

        e.Handled = True

        v_msg = Resources.LibResources.Label915
        If v_del_ctr = 0 And v_invalid_return = 0 Then
            Throw New Exception(v_msg)
        End If

    End Sub
    Protected Function DoInsert(ByVal newValues As OrderedDictionary) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim v_co_grp_cd As String = LeonsBiz.GetGroupCode(Session("CO_CD"))
        Dim v_charges_sql As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
            v_charges_sql = newValues("CHARGES").ToString()
        Else
            v_charges_sql = " 0 "
        End If

        sql = "Insert into OPS$DAEMON.ZONE2POSTALCODE (co_grp_cd,zone_cd,postal_cd,st_cd,charges) "
        sql = sql & " values ( "
        sql = sql & "'" & v_co_grp_cd & "',"
        sql = sql & "'" + UCase(newValues("ZONE_CD").ToString()) + "',"
        sql = sql & "'" + newValues("POSTAL_CD").ToString().ToUpper + "' ,"
        sql = sql & "'" + newValues("ST_CD").ToString().ToUpper + "', "
        sql = sql & v_charges_sql & ")"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()

        Return True

    End Function
    Protected Function DoUpdate(ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary, ByVal oldValues As OrderedDictionary) As Boolean


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()
        Dim v_co_grp_cd As String = LeonsBiz.GetGroupCode(Session("CO_CD"))
        Dim v_charges_sql As String = ""

        If LeonsBiz.isLeonsUser(Session("CO_CD")) = "Y" Then
            v_charges_sql = newValues("CHARGES").ToString()
        Else
            v_charges_sql = " 0 "
        End If

        sql = "UPDATE OPS$DAEMON.ZONE2POSTALCODE SET "
        sql = sql & " POSTAL_CD = '" + UCase(newValues("POSTAL_CD").ToString()) + "'"
        sql = sql & " ,ZONE_CD = '" + UCase(newValues("ZONE_CD").ToString()) + "'"
        sql = sql & " ,CHARGES = " + v_charges_sql
        sql = sql & " WHERE CO_GRP_CD = '" + v_co_grp_cd + "'"
        sql = sql & " and ZONE_CD = '" + oldValues("ZONE_CD").ToString() + "'"
        sql = sql & " and POSTAL_CD = '" + oldValues("POSTAL_CD").ToString() + "'"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Mydatareader.Close()
        Catch
            Throw New Exception("Error updating the record. Please try again.")
            conn.Close()
        End Try
        conn.Close()

        Return True

    End Function
    Protected Function DoDelete(ByVal keys As OrderedDictionary, ByVal newValues As OrderedDictionary) As Boolean

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim v_co_grp_cd As String = LeonsBiz.GetGroupCode(Session("CO_CD"))

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        sql = "DELETE OPS$DAEMON.ZONE2POSTALCODE "
        sql = sql & " WHERE CO_GRP_CD = '" + v_co_grp_cd + "'"
        sql = sql & " and ZONE_CD = '" + newValues("ZONE_CD").ToString() + "'"
        sql = sql & " and POSTAL_CD = '" + newValues("POSTAL_CD").ToString() + "'"
        sql = sql & " and ST_CD = '" + newValues("ST_CD").ToString() + "'"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()
        Catch
            Throw
            conn.Close()
        End Try
        conn.Close()

        Return True

    End Function

    Protected Sub GridView2_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs)
        If e.Column.FieldName = "CO_GRP_CD" Or e.Column.FieldName = "DES" Then
            e.Editor.ReadOnly = True
            e.Editor.ClientEnabled = False
        Else
            e.Editor.ReadOnly = False
            e.Editor.ClientEnabled = True
        End If
        
    End Sub
    Protected Sub GridView2_InitNewRow(ByVal sender As Object, ByVal e As ASPxDataInitNewRowEventArgs)

        e.NewValues("CO_GRP_CD") = ""
        e.NewValues("ZONE_CD") = ""
        e.NewValues("POSTAL_CD") = ""
        e.NewValues("DES") = ""
        e.NewValues("ST_CD") = ""
        e.NewValues("CHARGES") = ""
         

    End Sub


End Class
