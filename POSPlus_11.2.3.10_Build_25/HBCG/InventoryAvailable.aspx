<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="InventoryAvailable.aspx.vb" Inherits="InventoryAvailable" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table id="CustTable" runat="server" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td colspan="1">
                &nbsp;
            </td>
            <td colspan="2">
                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="<%$ Resources:LibResources, Label747 %>">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="1" align="left">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label707 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxDateEdit runat="server" ID="dt_needed">
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label708 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbo_store_grp_cd" runat="server" IncrementalFilteringMode ="StartsWith">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label550 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_sku" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="<%$ Resources:LibResources, Label653 %>">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_vsn" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <dx:ASPxCheckBox ID="chk_all" runat="server" Text="<%$ Resources:LibResources, Label745 %>" Checked="False">
                            </dx:ASPxCheckBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" colspan="2" align="left">
                &nbsp;<dx:ASPxCheckBox ID="chk_whs" runat="server" CheckState="Checked" Text="<%$ Resources:LibResources, Label748 %>">
                </dx:ASPxCheckBox>
                <dx:ASPxCheckBox ID="chk_shw" runat="server" CheckState="Checked" Text="<%$ Resources:LibResources, Label449 %>">
                </dx:ASPxCheckBox>
                <dx:ASPxCheckBox ID="chk_po" runat="server" CheckState="Checked" Text="<%$ Resources:LibResources, Label443 %>">
                </dx:ASPxCheckBox>
                <dx:ASPxCheckBox ID="chk_ist" runat="server" CheckState="Checked" Text="<%$ Resources:LibResources, Label242 %>">
                </dx:ASPxCheckBox>
                <dx:ASPxCheckBox ID="chk_out" runat="server" CheckState="UnChecked" Text="Outlet">
                </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnSubmit" runat="server" Text="<%$ Resources:LibResources, Label746 %>">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btn_clear" runat="server" Text="<%$ Resources:LibResources, Label84 %>" OnClick="btn_clear_Click">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <br />
                <dx:ASPxLabel ID="lbl_errors" runat="server" Width="100%">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <asp:DataGrid ID="MyDataGrid" runat="server" AllowSorting="True" AlternatingItemStyle-BackColor="Beige"
        AutoGenerateColumns="False" BorderColor="Black" CellPadding="3" DataKeyField="ITM_CD"
        Height="16px" OnSortCommand="MyDataGrid_Sort" PagerStyle-HorizontalAlign="Right"
        PagerStyle-Mode="NumericPages" Width="100%">
        <Columns>
            <asp:BoundColumn DataField="ITM_CD" HeaderText="SKU" Visible="False" SortExpression="itm_cd asc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="VSN" ItemStyle-Width="100px" HeaderText="<%$ Resources:LibResources, Label653 %>" Visible="False" SortExpression="vsn asc">
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="">
                <ItemTemplate>
                    <asp:ImageButton ID="img_gm" runat="server" ImageUrl="images/icons/find.gif" Height="15"
                        Width="15" CommandName="view_inv" Visible="True" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn SortExpression="itm_cd asc" HeaderText="<%$ Resources:LibResources, Label550%>">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_sku" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn SortExpression="vsn asc" HeaderText="<%$ Resources:LibResources, Label653 %>">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_vsn" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="WHS">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_whs" runat="server" Text="0">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="SHW">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_shw" runat="server" Text="0">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="PO">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_po" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="IST">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_ist" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="OUT">
                <ItemTemplate>
                    <dx:ASPxLabel ID="lbl_out" runat="server" Text="">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" Mode="NumericPages" />
        <AlternatingItemStyle BackColor="Gainsboro" Font-Bold="False" Font-Italic="False"
            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
            Font-Underline="False" ForeColor="Black" />
        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
            Font-Underline="False" VerticalAlign="Top" />
    </asp:DataGrid>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl3" runat="server" Width="679px" Height="500px"
        HeaderText="Inventory Lookup" PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter"
        PopupAction="None" Modal="True" CloseAction="CloseButton" AllowDragging="true">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    &nbsp; &nbsp;
    <dx:ASPxLabel ID="lbl_Warning" Text="" runat="server" Width="100%">
    </dx:ASPxLabel>
</asp:Content>
