Imports System.Data.OracleClient

Partial Class Email_Templates
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_ord_tp.Text = Request("ord_tp_cd")
        lbl_del_doc_num.Text = Request("del_doc_num")
        lbl_slsp1.Text = Request("slsp1")
        lbl_slsp2.Text = Request("slsp2")
        lbl_cust_cd.Text = Request("cust_cd")
        If Not IsPostBack Then
            template_populate()
        End If

    End Sub

    Public Sub template_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim SQL As String
        Dim MyTable As DataTable
        Dim numrows As Integer

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        End If

        SQL = "SELECT * FROM EMAIL_TEMPLATES WHERE EMAIL_PERM='Y' "
        If Session("Email_Template") = "Y" Then
            SQL = SQL & "OR SESSION_ID='" & HttpContext.Current.Session.SessionID.ToString & "'"
        End If
        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = SQL

            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            If numrows = 0 Then
                cbo_template.Enabled = False
                btn_submit.Enabled = False
                lbl_error.Text = "No templates found, you cannot setup a new email template"
                Exit Sub
            End If

            With cbo_template
                .DataSource = ds
                .ValueField = "EMAIL_ID"
                .TextField = "EMAIL_DESC"
                .DataBind()
            End With

            If ConfigurationManager.AppSettings("email_freeform").ToString = "Y" Then
                cbo_template.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Create New", ""))
                cbo_template.Items.FindByText("Create New").Value = ""
            End If

            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        bindgrid()

    End Sub

    Protected Sub cbo_template_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_template.SelectedIndexChanged

        bindgrid()

    End Sub

    Public Sub bindgrid()

        If ConfigurationManager.AppSettings("email_freeform").ToString = "Y" Then
            If cbo_template.SelectedItem.Text = "Create New" Then
                txt_to.Enabled = True
                txt_cc.Enabled = True
                txt_bcc.Enabled = True
                txt_subject.Enabled = True
                txt_body.Enabled = True
                txt_from.Enabled = True
                txt_from.Text = ""
                txt_to.Text = ""
                txt_cc.Text = ""
                txt_bcc.Text = ""
                txt_subject.Text = ""
                txt_body.Text = ""
                Exit Sub
            End If
        End If
        Dim slsp1 As String = ""
        Dim slsp2 As String = ""
        Dim slsp1_email As String = ""
        Dim slsp2_email As String = ""
        Dim cust_cd As String = ""
        Dim cust_email As String = ""
        Dim cust_ship_email As String = ""

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If lbl_del_doc_num.Text & "" = "" Then
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "SELECT EMAIL FROM CUST_INFO WHERE SESSION_ID='" & Session.SessionID.ToString & "'"
            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    cust_email = MyDataReader.Item("EMAIL").ToString
                End If

                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            conn.Close()
            slsp1 = lbl_slsp1.Text
            slsp2 = lbl_slsp2.Text
            cust_cd = lbl_cust_cd.Text

        ElseIf lbl_ord_tp.Text = "CRM" Then
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "SELECT SLSP1, CUST_CD, EMAIL_ADDR FROM RELATIONSHIP WHERE REL_NO='" & lbl_del_doc_num.Text & "'"
            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    slsp1 = MyDataReader.Item("SLSP1").ToString
                    cust_cd = MyDataReader.Item("CUST_CD").ToString
                    cust_email = MyDataReader.Item("EMAIL_ADDR").ToString
                End If

                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            conn.Close()
        Else
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            sql = "SELECT CUST_CD, SO_EMP_SLSP_CD1, SO_EMP_SLSP_CD2 FROM SO WHERE DEL_DOC_NUM='" & lbl_del_doc_num.Text & "'"
            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    slsp1 = MyDataReader.Item("SO_EMP_SLSP_CD1").ToString
                    cust_cd = MyDataReader.Item("CUST_CD").ToString
                    slsp2 = MyDataReader.Item("SO_EMP_SLSP_CD2").ToString
                End If

                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
            conn.Close()
        End If

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "SELECT EMAIL_ADDR FROM EMP WHERE EMP_CD='" & slsp1 & "'"
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                slsp1_email = MyDataReader.Item("EMAIL_ADDR").ToString
            End If

            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If slsp2 & "" <> "" Then
            sql = "SELECT EMAIL_ADDR FROM EMP WHERE EMP_CD='" & slsp2 & "'"
            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    slsp2_email = MyDataReader.Item("EMAIL_ADDR").ToString
                End If

                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

        If cust_cd <> "NEW" Then
            sql = "SELECT EMAIL_ADDR, EMAIL_ADDR_SHIP_TO FROM CUST WHERE CUST_CD='" & cust_cd & "'"
            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    If cust_email & "" = "" Then
                        cust_email = MyDataReader.Item("EMAIL_ADDR").ToString
                    End If
                    cust_ship_email = MyDataReader.Item("EMAIL_ADDR_SHIP_TO").ToString
                End If

                'Close Connection 
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If
        conn.Close()

        If cbo_template.Value & "" <> "" Then
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "SELECT * FROM EMAIL_TEMPLATES WHERE EMAIL_ID=" & cbo_template.Value

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    txt_from.Text = MyDataReader.Item("EMAIL_FROM").ToString
                    txt_to.Text = MyDataReader.Item("EMAIL_TO").ToString
                    txt_cc.Text = MyDataReader.Item("EMAIL_CC").ToString
                    txt_bcc.Text = MyDataReader.Item("EMAIL_BCC").ToString
                    txt_subject.Text = MyDataReader.Item("EMAIL_SUBJECT").ToString
                    txt_body.Text = MyDataReader.Item("EMAIL_BODY").ToString
                End If

                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            If InStr(txt_from.Text, "<SLSP1_EMAIL>") > 0 Then
                txt_from.Text = Replace(txt_from.Text, "<SLSP1_EMAIL>", "")
                If slsp1_email & "" <> "" Then
                    txt_from.Text = txt_from.Text & slsp1_email & ","
                End If
            End If
            If InStr(txt_from.Text, "<SLSP2_EMAIL>") > 0 Then
                txt_from.Text = Replace(txt_from.Text, "<SLSP2_EMAIL>", "")
                If slsp2_email & "" <> "" Then
                    txt_from.Text = txt_from.Text & slsp2_email & ","
                End If
            End If
            If InStr(txt_from.Text, "<CUST_EMAIL>") > 0 Then
                txt_from.Text = Replace(txt_from.Text, "<CUST_EMAIL>", "")
                If cust_email & "" <> "" Then
                    txt_from.Text = txt_from.Text & cust_email & ","
                End If
            End If
            If InStr(txt_from.Text, "<CUST_SHIP_EMAIL>") > 0 Then
                txt_from.Text = Replace(txt_from.Text, "<CUST_SHIP_EMAIL>", "")
                If cust_ship_email & "" <> "" Then
                    txt_from.Text = txt_from.Text & cust_ship_email & ","
                End If
            End If
            If InStr(txt_from.Text, "<DONT_ALLOW>") > 0 Then
                txt_from.Text = Replace(txt_from.Text, "<DONT_ALLOW>", "")
                txt_from.Enabled = False
            End If
            If InStr(txt_from.Text, "<ALLOW_FREEFORM>") > 0 Then
                txt_from.Text = Replace(txt_from.Text, "<ALLOW_FREEFORM>", "")
                txt_from.Enabled = True
            Else
                txt_from.Enabled = False
            End If

            If InStr(txt_to.Text, "<SLSP1_EMAIL>") > 0 Then
                txt_to.Text = Replace(txt_to.Text, "<SLSP1_EMAIL>", "")
                If slsp1_email & "" <> "" Then
                    txt_to.Text = txt_to.Text & slsp1_email & ","
                End If
            End If
            If InStr(txt_to.Text, "<SLSP2_EMAIL>") > 0 Then
                txt_to.Text = Replace(txt_to.Text, "<SLSP2_EMAIL>", "")
                If slsp2_email & "" <> "" Then
                    txt_to.Text = txt_to.Text & slsp2_email & ","
                End If
            End If
            If InStr(txt_to.Text, "<CUST_EMAIL>") > 0 Then
                txt_to.Text = Replace(txt_to.Text, "<CUST_EMAIL>", "")
                If cust_email & "" <> "" Then
                    txt_to.Text = txt_to.Text & cust_email & ","
                End If
            End If
            If InStr(txt_to.Text, "<CUST_SHIP_EMAIL>") > 0 Then
                txt_to.Text = Replace(txt_to.Text, "<CUST_SHIP_EMAIL>", "")
                If cust_ship_email & "" <> "" Then
                    txt_to.Text = txt_to.Text & cust_ship_email & ","
                End If
            End If
            If InStr(txt_to.Text, "<DONT_ALLOW>") > 0 Then
                txt_to.Text = Replace(txt_to.Text, "<DONT_ALLOW>", "")
                txt_to.Enabled = False
            End If
            If InStr(txt_to.Text, "<ALLOW_FREEFORM>") > 0 Then
                txt_to.Text = Replace(txt_to.Text, "<ALLOW_FREEFORM>", "")
                txt_to.Enabled = True
            Else
                txt_to.Enabled = False
            End If

            If InStr(txt_cc.Text, "<SLSP1_EMAIL>") > 0 Then
                txt_cc.Text = Replace(txt_cc.Text, "<SLSP1_EMAIL>", "")
                If slsp1_email & "" <> "" Then
                    txt_cc.Text = txt_cc.Text & slsp1_email & ","
                End If
            End If
            If InStr(txt_cc.Text, "<SLSP2_EMAIL>") > 0 Then
                txt_cc.Text = Replace(txt_cc.Text, "<SLSP2_EMAIL>", "")
                If slsp2_email & "" <> "" Then
                    txt_cc.Text = txt_cc.Text & slsp2_email & ", "
                End If
            End If
            If InStr(txt_cc.Text, "<CUST_EMAIL>") > 0 Then
                txt_cc.Text = Replace(txt_cc.Text, "<CUST_EMAIL>", "")
                If cust_email & "" <> "" Then
                    txt_cc.Text = txt_cc.Text & cust_email & ","
                End If
            End If
            If InStr(txt_cc.Text, "<CUST_SHIP_EMAIL>") > 0 Then
                txt_cc.Text = Replace(txt_cc.Text, "<CUST_SHIP_EMAIL>", "")
                If cust_ship_email & "" <> "" Then
                    txt_cc.Text = txt_cc.Text & cust_ship_email & ","
                End If
            End If
            If InStr(txt_cc.Text, "<DONT_ALLOW>") > 0 Then
                txt_cc.Text = Replace(txt_cc.Text, "<DONT_ALLOW>", "")
                txt_cc.Enabled = False
            End If
            If InStr(txt_cc.Text, "<ALLOW_FREEFORM>") > 0 Then
                txt_cc.Text = Replace(txt_cc.Text, "<ALLOW_FREEFORM>", "")
                txt_cc.Enabled = True
            Else
                txt_cc.Enabled = False
            End If

            If InStr(txt_bcc.Text, "<SLSP1_EMAIL>") > 0 Then
                txt_bcc.Text = Replace(txt_bcc.Text, "<SLSP1_EMAIL>", "")
                If slsp1_email & "" <> "" Then
                    txt_bcc.Text = txt_bcc.Text & slsp1_email & ","
                End If
            End If
            If InStr(txt_bcc.Text, "<SLSP2_EMAIL>") > 0 Then
                txt_bcc.Text = Replace(txt_bcc.Text, "<SLSP2_EMAIL>", "")
                If slsp2_email & "" <> "" Then
                    txt_bcc.Text = txt_bcc.Text & slsp2_email & ","
                End If
            End If
            If InStr(txt_bcc.Text, "<CUST_EMAIL>") > 0 Then
                txt_bcc.Text = Replace(txt_bcc.Text, "<CUST_EMAIL>", "")
                If cust_email & "" <> "" Then
                    txt_bcc.Text = txt_bcc.Text & cust_email & ","
                End If
            End If
            If InStr(txt_bcc.Text, "<CUST_SHIP_EMAIL>") > 0 Then
                txt_bcc.Text = Replace(txt_bcc.Text, "<CUST_SHIP_EMAIL>", "")
                If cust_ship_email & "" <> "" Then
                    txt_bcc.Text = txt_bcc.Text & cust_ship_email & ","
                End If
            End If
            If InStr(txt_bcc.Text, "<DONT_ALLOW>") > 0 Then
                txt_bcc.Text = Replace(txt_bcc.Text, "<DONT_ALLOW>", "")
                txt_bcc.Enabled = False
            End If
            If InStr(txt_bcc.Text, "<ALLOW_FREEFORM>") > 0 Then
                txt_bcc.Text = Replace(txt_bcc.Text, "<ALLOW_FREEFORM>", "")
                txt_bcc.Enabled = True
            Else
                txt_bcc.Enabled = False
            End If

            If InStr(txt_subject.Text, "<ALLOW_FREEFORM>") > 0 Then
                txt_subject.Text = Replace(txt_subject.Text, "<ALLOW_FREEFORM>", "")
                txt_subject.Enabled = True
            Else
                txt_subject.Enabled = False
            End If

            If InStr(txt_body.Text, "<ALLOW_FREEFORM>") > 0 Then
                txt_body.Text = Replace(txt_body.Text, "<ALLOW_FREEFORM>", "")
                txt_body.Enabled = True
            Else
                txt_body.Enabled = False
            End If
        End If

    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        lbl_error.Text = ""

        If txt_from.Text & "" = "" Then
            lbl_error.Text = "From address must be completed"
            Exit Sub
        End If
        If txt_to.Text & "" = "" Then
            lbl_error.Text = "To address must be completed"
            Exit Sub
        End If
        If txt_subject.Text & "" = "" Then
            lbl_error.Text = "Subject must be completed"
            Exit Sub
        End If
        If txt_body.Text & "" = "" Then
            lbl_error.Text = "Body must be completed"
            Exit Sub
        End If

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand

        conn.Open()

        sql = "DELETE FROM EMAIL_TEMPLATES WHERE SESSION_ID='" & Session.SessionID.ToString & "'"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        sql = "INSERT INTO EMAIL_TEMPLATES (EMAIL_FROM, EMAIL_TO, EMAIL_CC, EMAIL_BCC, EMAIL_SUBJECT, EMAIL_BODY, SESSION_ID) "
        sql = sql & "VALUES(:EMAIL_FROM, :EMAIL_TO, :EMAIL_CC, :EMAIL_BCC, :EMAIL_SUBJECT, :EMAIL_BODY, :SESSION_ID)"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        objSql.Parameters.Add(":EMAIL_FROM", OracleType.VarChar)
        objSql.Parameters.Add(":EMAIL_TO", OracleType.VarChar)
        objSql.Parameters.Add(":EMAIL_CC", OracleType.VarChar)
        objSql.Parameters.Add(":EMAIL_BCC", OracleType.VarChar)
        objSql.Parameters.Add(":EMAIL_SUBJECT", OracleType.VarChar)
        objSql.Parameters.Add(":EMAIL_BODY", OracleType.Clob)
        objSql.Parameters.Add(":SESSION_ID", OracleType.VarChar)

        objSql.Parameters(":EMAIL_FROM").Value = txt_from.Text
        objSql.Parameters(":EMAIL_TO").Value = txt_to.Text
        objSql.Parameters(":EMAIL_CC").Value = txt_cc.Text
        objSql.Parameters(":EMAIL_BCC").Value = txt_bcc.Text
        objSql.Parameters(":EMAIL_SUBJECT").Value = txt_subject.Text
        objSql.Parameters(":EMAIL_BODY").Value = txt_body.Text
        objSql.Parameters(":SESSION_ID").Value = Session.SessionID.ToString
        objSql.ExecuteNonQuery()

        Session("email_template") = "Y"
        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopupEmailTemplate();")
        Response.Write("</script>")

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class
