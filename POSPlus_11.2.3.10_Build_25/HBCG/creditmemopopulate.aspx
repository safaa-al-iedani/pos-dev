<%@ Page Language="VB" AutoEventWireup="false" CodeFile="creditmemopopulate.aspx.vb"
    Inherits="CreditMemoPopulate" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Credit Memo - Original Invoice Selection</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
        <table >
            <tr>
                <td valign="middle" align="center">
                    <img src="images/icons/Symbol-Information.png" style="width: 174px; height: 174px" />
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td valign="middle" align="center" style="width: 300px">
                    <div align="center">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:creditmemopopulate,Label08 %>"  Height="62px">
                        </dx:ASPxLabel>
                        <br />
                        <dx:ASPxComboBox ID="cbo_crm_cause_cd" runat="server" Width="232px" IncrementalFilteringMode ="StartsWith">
                        </dx:ASPxComboBox>
                        <br />
                        <dx:ASPxTextBox ID="txt_del_doc_num" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                        <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" CheckState="Checked" 
                             Text="<%$ Resources:creditmemopopulate,Label07 %>" >
                        </dx:ASPxCheckBox>
                        <br />
                        <dx:ASPxButton ID="btn_Proceed" runat="server"  Text="<%$ Resources:LibResources,Label577 %>">
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btn_Continue" runat="server" Visible ="false"  Text="<%$ Resources:creditmemopopulate,Label02 %>">
                        </dx:ASPxButton>
        <dx:ASPxLabel ID="lbl_warning" runat="server" Text="" Width="100%">
        </dx:ASPxLabel>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
