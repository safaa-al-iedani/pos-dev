Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Linq

Imports HBCG_Utils

Partial Class DeliveryDate
    Inherits POSBasePage

    Dim pt_cnt As String
    Dim MaxDelDate As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' MM-3179 - Enter key kicks out of session
        Me.Form.DefaultButton = Me.btn_Change.UniqueID

            If Session("ord_tp_cd") = "MCR" Or Session("ord_tp_cd") = "MDB" Then
                lbl_Delivery.Visible = True
                txt_Delivery_Charge.Visible = True
                lbl_Setup.Visible = True
                txt_setup_charge.Visible = True
                btn_Change.Visible = True
            lblDesc.Text = "You cannot schedule deliveries/pickups for Miscellaneous Credits or Debits."

            If Not IsPostBack Then


                txt_Delivery_Charge.Text = If(IsNumeric(Session("DEL_CHG")), FormatNumber(CDbl(Session("DEL_CHG")), 2), FormatNumber(0, 2))
                txt_setup_charge.Text = If(IsNumeric(Session("SETUP_CHG")), FormatNumber(CDbl(Session("SETUP_CHG")), 2), FormatNumber(0, 2))
            End If
            Exit Sub
        End If

        'Daniela add validation for negative values
        If IsNumeric(txt_Delivery_Charge.Text) AndAlso txt_Delivery_Charge.Text < 0 Then
            lbl_curr_data.Text = Resources.LibResources.Label800
            lbl_curr_data.Font.Bold = True
            lbl_curr_data.ForeColor = Color.Red
            Exit Sub
        End If
        If IsNumeric(txt_setup_charge.Text) AndAlso txt_setup_charge.Text < 0 Then
            lbl_curr_data.Text = Resources.LibResources.Label800
            lbl_curr_data.Font.Bold = True
            lbl_curr_data.ForeColor = Color.Red
            Exit Sub
        End If
        'Daniela IT Request 2475
        If (Session("ord_tp_cd") = "CRM" Or Session("ord_tp_cd") = "MCR") AndAlso isNotEmpty(Session("adj_ivc_cd")) Then
            If IsNumeric(txt_Delivery_Charge.Text) AndAlso IsNumeric(Session("ORIG_DEL_CHG")) AndAlso txt_Delivery_Charge.Text > Session("ORIG_DEL_CHG") Then
                Session("DEL_CHG") = 0
                Session("SETUP_CHG") = 0
                txt_Delivery_Charge.Text = If(IsNumeric(Session("DEL_CHG")), FormatNumber(CDbl(Session("DEL_CHG")), 2), FormatNumber(0, 2))
                txt_setup_charge.Text = If(IsNumeric(Session("SETUP_CHG")), FormatNumber(CDbl(Session("SETUP_CHG")), 2), FormatNumber(0, 2))
                lbl_curr_data.Text = Resources.LibResources.Label969
                lbl_curr_data.Font.Bold = True
                lbl_curr_data.ForeColor = Color.Red
                Exit Sub
            End If
            If IsNumeric(txt_setup_charge.Text) AndAlso IsNumeric(Session("ORIG_SETUP_CHG")) AndAlso txt_setup_charge.Text > Session("ORIG_SETUP_CHG") Then
                Session("DEL_CHG") = 0
                Session("SETUP_CHG") = 0
                txt_setup_charge.Text = If(IsNumeric(Session("SETUP_CHG")), FormatNumber(CDbl(Session("SETUP_CHG")), 2), FormatNumber(0, 2))
                txt_Delivery_Charge.Text = If(IsNumeric(Session("DEL_CHG")), FormatNumber(CDbl(Session("DEL_CHG")), 2), FormatNumber(0, 2))
                lbl_curr_data.Text = Resources.LibResources.Label969
                lbl_curr_data.Font.Bold = True
                lbl_curr_data.ForeColor = Color.Red
                Exit Sub
            End If
        End If

        If Session("IP") & "" <> "" Then
            lbl_Delivery.Visible = False
            txt_Delivery_Charge.Visible = False
            lbl_Setup.Visible = False
            txt_setup_charge.Visible = False
            btn_Change.Visible = False
        Else
            lbl_Delivery.Visible = True
            txt_Delivery_Charge.Visible = True
            lbl_Setup.Visible = True
            txt_setup_charge.Visible = True
            btn_Change.Visible = True
        End If

        If Not Page.IsPostBack Then
            'If the page is visited for the first time, Clear the session so that new data gets filled in
            Session("DeliveryDates") = Nothing
            grid_del_date_Binddata()

            If Session("MP") & "" = "" Then
                If IsNumeric(Session("DEL_CHG")) Then
                    txt_Delivery_Charge.Text = FormatNumber(CDbl(Session("DEL_CHG")), 2)
                Else
                    txt_Delivery_Charge.Text = FormatNumber(0, 2)
                End If
                hidOriginalDeliveryCharges.Value = txt_Delivery_Charge.Text
                If IsNumeric(Session("SETUP_CHG")) Then
                    txt_setup_charge.Text = FormatNumber(CDbl(Session("SETUP_CHG")), 2)
                Else
                    txt_setup_charge.Text = FormatNumber(0, 2)
                End If
                calculate_total(Page.Master)
                If IsDate(Session("del_dt")) And Not String.IsNullOrEmpty(Session("zone_cd")) Then
                    If Session("PD") = "D" Then
                        lbl_curr_data.Text = "This order is currently linked to zone " & Session("zone_cd") & " scheduled for delivery on " & Session("del_dt")
                    ElseIf Session("PD") = "P" Then
                        'Daniela french
                        'lbl_curr_data.Text = "This order is currently scheduled for pickup on " & Session("del_dt")
                        lbl_curr_data.Text = Resources.LibResources.Label603 & " " & Session("del_dt")
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub grid_del_date_Binddata()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim CatchMe As Boolean
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        ds = New DataSet
        Dim CurrentDate As DateTime = DateTime.Today
        'MM-7300
        lblZoneCd.Text = Resources.POSMessages.MSG0039 + Session("zone_des")

        grid_del_date.DataSource = ""
        CatchMe = False
        If Session("ZONE_CD") & "" = "" Then
            If Request("LEAD") = "TRUE" Then
                Response.Redirect("Delivery.aspx?LEAD=TRUE")
            Else
                Response.Redirect("Delivery.aspx")
            End If
        Else
            CatchMe = True
        End If
        grid_del_date.Visible = True
        If CatchMe = True Then
            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

            objConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            objConnection.Open()
            'Moved the existing code into seperate block
            MaxDelDate = getMaxAvailableDate()

            If IsNothing(Session("DeliveryDates")) Then
                sql = "SELECT DEL_DT, DAY, DEL_STOPS, ACTUAL_STOPS, CLOSED, (DEL_STOPS - ACTUAL_STOPS) AS REMAINING_STOPS,EXMPT_DEL_CD "
                sql = sql & "FROM DEL "
                sql = sql & "WHERE (ZONE_CD = '" & Session("ZONE_CD") & "') AND (DEL_DT >= TO_DATE('" & CurrentDate & "','mm/dd/RRRR')) "
                sql = sql & "AND NVL(CLOSED,'N')='N' "
                sql = sql & "ORDER BY DEL_DT"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
                Session("DeliveryDates") = ds
            Else
                ds = Session("DeliveryDates")
            End If
            dv = ds.Tables(0).DefaultView

            Try
                If SystemUtils.dataSetHasRows(ds) Then
                    Dim MaxDate As DateTime
                    Dim MinDate As DateTime
                    Dim totalrows As Integer = 0
                    grid_del_date.DataSource = dv
                    grid_del_date.DataBind()
                    totalrows = ds.Tables(0).Rows.Count()

                    If IsDate(ds.Tables(0).Rows(totalrows - 1)("DEL_DT")) Then
                        MaxDate = Convert.ToDateTime(ds.Tables(0).Rows(totalrows - 1)("DEL_DT"))
                    Else
                        MaxDate = Today
                    End If
                    If IsDate(ds.Tables(0).Rows(0)("DEL_DT")) Then
                        MinDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("DEL_DT"))
                    Else
                        MinDate = Today
                    End If
                    If totalrows > grid_del_date.PageSize Then
                        cboTargetDate.Enabled = True
                    Else
                        cboTargetDate.Enabled = False
                    End If

                    setCalanderMinAndMaxDays(MinDate.AddDays(-1), MaxDate)
                Else
                    grid_del_date.Visible = False
                End If

            Catch ex As Exception

                Throw
            Finally
                conn.Close()
            End Try
        End If
        lbl_pageinfo.Text = "Displaying Page " + CStr(grid_del_date.CurrentPageIndex + 1) + " of " + CStr(grid_del_date.PageCount)
        ' make all the buttons visible if page count is more than 0
        If grid_del_date.PageCount > 0 Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            lbl_pageinfo.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True

        ' then turn off buttons that don't make sense
        If grid_del_date.PageCount = 1 Then
            ' only 1 page of data
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If grid_del_date.CurrentPageIndex = 0 Then
                ' first page
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If grid_del_date.CurrentPageIndex = (grid_del_date.PageCount - 1) Then
                    ' last page
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        If grid_del_date.PageCount = 0 And CatchMe = True Then
            lblDesc.Text = Resources.LibResources.Label562 & " <a href=""Delivery.aspx"">" & Resources.LibResources.Label416 & "</a>.  <br><br>" & Resources.LibResources.Label230
            lblDesc.Visible = True
            lbl_Delivery.Visible = False
            txt_Delivery_Charge.Visible = False
            lbl_Setup.Visible = False
            txt_setup_charge.Visible = False
            btn_Change.Visible = False
            'MM-7300
            lblZoneCd.Text = ""
            SessVar.ZoneDes = ""
            Session("ZONE_CD") = ""
            Session("DEL_CHG") = 0
            Session("SETUP_CHG") = 0
            cboTargetDate.Visible = False
            lblFilterDate.Visible = False
        Else
            cboTargetDate.Visible = True
            lblFilterDate.Visible = True
        End If
        Disable_DeliveryDates()

    End Sub

    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Select Case strArg
            Case "Next"
                If grid_del_date.CurrentPageIndex < (grid_del_date.PageCount - 1) Then
                    grid_del_date.CurrentPageIndex += 1
                End If
            Case "Prev"
                If grid_del_date.CurrentPageIndex > 0 Then
                    grid_del_date.CurrentPageIndex -= 1
                End If
            Case "Last"
                grid_del_date.CurrentPageIndex = grid_del_date.PageCount - 1
            Case Else
                grid_del_date.CurrentPageIndex = 0
        End Select

        grid_del_date_Binddata()
        'cboTargetDate.Text = String.Empty ' March 29 fix next button bugtest !!!

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("MP") & "" <> "" Then
            Me.MasterPageFile = "~/MasterPages/NoWizard2.Master"
        End If
        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "Mobile.Master"
        End If

    End Sub

    Public Sub Disable_DeliveryDates()

        Dim intRow As Integer = 0
        Dim intRows As Integer = grid_del_date.Items.Count - 1
        Dim gridItem As DataGridItem
        Dim theTMBiz As TransportationBiz = New TransportationBiz
        Dim remainingPoints As String
        MaxDelDate = getMaxAvailableDate()
        For intRow = 0 To intRows
            gridItem = grid_del_date.Items(intRow)

            remainingPoints = gridItem.Cells(4).Text().Trim()

            If pt_cnt & "" = "" Then pt_cnt = "0"

            If IsNumeric(remainingPoints) Then
                gridItem.Cells(5).Enabled = theTMBiz.GetDisableDeliveryDates(pt_cnt, remainingPoints, gridItem.Cells(6).Text().Trim())
            End If

            'Jira:2513 (If the max availability date  is falls short, then disable the button. Else enable it.)
            Dim DelDate As String = String.Empty

            DelDate = gridItem.Cells(0).Text.ToString

            'if ARS is enabled, then only consider disabling the button and the order type = "SAL" 
            If Session("STORE_ENABLE_ARS") = "True" AndAlso
                Not Session("ord_tp_cd") = Nothing AndAlso
                AppConstants.Order.TYPE_SAL = Session("ord_tp_cd").ToString().ToUpper() Then

                If DelDate = String.Empty Or MaxDelDate = String.Empty Then
                    gridItem.Cells(5).Enabled = False
                    ASPlblError.Text = "No delivery date found for Item(s)"
                    tblError.Visible = True
                ElseIf Convert.ToDateTime(DelDate) < Convert.ToDateTime(MaxDelDate) Then
                    gridItem.Cells(5).Enabled = False
                End If
            End If
        Next

    End Sub

    Protected Sub grid_del_date_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grid_del_date.ItemCreated

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim myButton As Button = CType(e.Item.Cells(5).Controls(0), Button)
            myButton.CssClass = "style5"
        End If

    End Sub

    Public Sub grid_del_date_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grid_del_date.ItemDataBound

        If Session("IP") & "" <> "" Then
            e.Item.Cells(5).Visible = False
        End If

    End Sub

    Private Sub grid_del_date_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grid_del_date.ItemCommand

        Session("del_dt") = e.Item.Cells(0).Text
        Response.Redirect("payment.aspx")

    End Sub

    Protected Sub btn_Change_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Change.Click
        'Need to check for the syspm Sales #31. Require approval to override default delivery charges in PSOE and SOE? 
        'If this is set to Y then PSOE/SOE in E1 will require an approval code if the user changes the delivery charge
        Dim syspmForApproval As String = SysPms.deliveryChargeApproval
        Dim deliveryCharges As Double = 0
        Dim sessionDelCharges As Double = 0

        Double.TryParse(txt_Delivery_Charge.Text.Trim, deliveryCharges)
        Double.TryParse(Session("DEL_CHG") & "", sessionDelCharges)

        If IsNumeric(txt_Delivery_Charge.Text) AndAlso deliveryCharges >= 0 Then
            If syspmForApproval.Equals("Y") AndAlso Not (sessionDelCharges = deliveryCharges) Then
                'Bring the popup here
                showApprovalPopup(Resources.POSMessages.MSG0062)
            Else
                ProcessPriceChanges()
            End If
        Else
            ProcessPriceChanges()
        End If
    End Sub

    Private Sub ProcessPriceChanges()
        Dim setupCharges As Double = 0
        Double.TryParse(txt_setup_charge.Text.Trim, setupCharges)
        Dim deliveryCharges As Double = 0
        Double.TryParse(txt_Delivery_Charge.Text.Trim, deliveryCharges)
        ASPlblError.Text = String.Empty
        If IsNumeric(txt_Delivery_Charge.Text) AndAlso deliveryCharges >= 0 Then
            txt_Delivery_Charge.Text = FormatNumber(txt_Delivery_Charge.Text, 2)
            Session("DEL_CHG") = txt_Delivery_Charge.Text
        Else
            txt_Delivery_Charge.Text = FormatNumber(0, 2)
            Session("DEL_CHG") = txt_Delivery_Charge.Text
            ASPlblError.Text = Resources.POSErrors.ERR0047 & vbCrLf
        End If
        If IsNumeric(txt_setup_charge.Text.Trim) AndAlso setupCharges >= 0 Then
            txt_setup_charge.Text = FormatNumber(txt_setup_charge.Text, 2)
            Session("SETUP_CHG") = txt_setup_charge.Text
        Else
            txt_setup_charge.Text = FormatNumber(0, 2)
            Session("SETUP_CHG") = txt_setup_charge.Text
            ASPlblError.Text = ASPlblError.Text & Resources.POSErrors.ERR0048
        End If

        Dim SQL As String = ""
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        'Open Connection 
        conn.Open()
        SQL = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        SQL = SQL & "FROM TAT, TAX_CD$TAT "
        SQL = SQL & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        SQL = SQL & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        SQL = SQL & "AND TAX_CD$TAT.TAX_CD='" & Session("TAX_CD") & "' "
        SQL = SQL & "AND TAT.DEL_TAX = 'Y' "
        SQL = SQL & "GROUP BY TAX_CD$TAT.TAX_CD "
        'Determine the taxability of the setup and delivery charges
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Session("DEL_TAX") = MyDataReader.Item("SumOfPCT")
            Else
                Session("DEL_TAX") = "N"
            End If
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        SQL = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        SQL = SQL & "FROM TAT, TAX_CD$TAT "
        SQL = SQL & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        SQL = SQL & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        SQL = SQL & "AND TAX_CD$TAT.TAX_CD='" & Session("TAX_CD") & "' "
        SQL = SQL & "AND TAT.SU_TAX = 'Y' "
        SQL = SQL & "GROUP BY TAX_CD$TAT.TAX_CD "
        'Determine the taxability of the setup and delivery charges
        objSql = DisposablesManager.BuildOracleCommand(SQL, conn)

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Session("SU_TAX") = MyDataReader.Item("SumOfPCT")
            Else
                Session("SU_TAX") = "N"
            End If
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

        calculate_total(Page.Master)
    End Sub

    Protected Sub cboTargetDatechanged(ByVal sender As Object, ByVal e As System.EventArgs)
     
            Dim rowsFound As Boolean = True
            Dim dates As DataSet = Session("DeliveryDates")
            'Bind all the data by default
            grid_del_date.DataSource = dates
            'Enable the navigation buttons by default
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = True
            btnLast.Enabled = True
            If cboTargetDate.Value & "" = String.Empty Then
                'Binds all tthe data 


            ElseIf dates IsNot Nothing Then 'Alice added on June 5, 2019 in case that if dates object is nothing, will cause exception

                'filter out the rows and bind only the matching data
                Dim dt As DataTable = dates.Tables(0)
                'This  filters the data based on the selection of date!
                Dim qry = (From row In dt.AsEnumerable() Where row.Field(Of Date?)("DEL_DT") = Convert.ToDateTime(cboTargetDate.Value.ToString()) Select row).Distinct()
                'disable the prev and first buttons by defult
                btnFirst.Enabled = False
                btnPrev.Enabled = False
                ASPlblError.Text = String.Empty
                'if any rows found for the selected date
                If qry.Any() Then
                    Dim filteredDates As DataTable = qry.CopyToDataTable()
                    'Some times the returned data will have multiple rows, need to the 
                    If filteredDates.Rows.Count > 1 Then
                        For lc As Integer = 1 To filteredDates.Rows.Count - 1
                            filteredDates.Rows.RemoveAt(lc)
                        Next
                        filteredDates.AcceptChanges()
                    End If
                    grid_del_date.DataSource = filteredDates
                    btnFirst.Enabled = False
                    btnNext.Enabled = False
                    btnPrev.Enabled = False
                    btnLast.Enabled = False
                Else
                    rowsFound = False
                    btnFirst.Enabled = False
                    btnNext.Enabled = False
                    If dates.Tables(0).Rows.Count > grid_del_date.PageSize Then
                        btnNext.Enabled = True
                        btnLast.Enabled = True
                    Else
                        btnNext.Enabled = False
                        btnLast.Enabled = False
                    End If
                End If
            End If
            grid_del_date.CurrentPageIndex = 0
            grid_del_date.DataBind()
            Disable_DeliveryDates()
            lbl_pageinfo.Text = "Displaying Page " + CStr(grid_del_date.CurrentPageIndex + 1) + " of " + CStr(grid_del_date.PageCount)
            If rowsFound Then
                'Do Nothing

            Else
                ASPlblError.Text = "No delivery dates found for the selected date - " & cboTargetDate.Value
                cboTargetDate.Value = String.Empty
            End If

    End Sub

    Private Sub setCalanderMinAndMaxDays(ByVal startDate As Date, ByVal endDate As Date)
        cboTargetDate.MinDate = startDate
        If endDate = Today Then
            'Do nothing, dont set the end Date

        Else
            cboTargetDate.MaxDate = endDate
        End If

    End Sub
    Private Function getMaxAvailableDate() As String
        Dim maxDeliveredBy As String = String.Empty
        maxDeliveredBy = SalesUtils.GetMaxAvailableDate(Session.SessionID, pt_cnt)
        Return maxDeliveredBy
    End Function

    Protected Sub ucMgrOverride_wasCanceled(sender As Object, e As EventArgs) Handles ucMgrOverride.wasCanceled
        hideApprovalPopup()
        'if calcelled, retin the original delivery charges.
        txt_Delivery_Charge.Text = hidOriginalDeliveryCharges.Value
        ProcessPriceChanges()
    End Sub
    Protected Sub ucMgrOverride_wasApproved(sender As Object, e As EventArgs) Handles ucMgrOverride.wasApproved
        hideApprovalPopup()
        ProcessPriceChanges()
    End Sub

    Private Sub hideApprovalPopup()
        Session("SHOWPOPUP") = Nothing
        popupMgrOverride.ShowOnPageLoad = False
    End Sub
    Private Sub showApprovalPopup(ByVal message As String)
        popupMgrOverride.ShowOnPageLoad = True
        ucMgrOverride.SetDisplayMessage(message)
        Session("SHOWPOPUP") = True
    End Sub
End Class
