﻿Imports HBCG_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization


Partial Class TrafficSlsp
    Inherits POSBasePage

    Private LeonsBiz As LeonsBiz = New LeonsBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If isNotEmpty(Session("CO_CD")) Then
                PopulateStoreCodes(Session("CO_CD"))
            End If
            If isEmpty(Session("CO_CD")) Then
                PopulateStoreCodes("")
            End If
        End If

        If (Not Gridview1.DataSource Is Nothing) Then
            Gridview1.DataBind()
        End If

    End Sub

    Protected Function Get_traffic_count(ByVal storeCd As String, ByVal trafficDt As String) As String

        Dim trafficCount As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select count(*) count from brk_traffic_slsp "
        sql = sql & "where store_cd = :STORE and WHEN = to_date(:WHEN, 'DD-MON-YYYY')"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            dbCommand.Parameters.Add(":STORE", OracleType.VarChar)
            dbCommand.Parameters(":STORE").Value = storeCd
            dbCommand.Parameters.Add(":WHEN", OracleType.VarChar)
            dbCommand.Parameters(":WHEN").Value = trafficDt

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                trafficCount = dbReader.Item("count").ToString
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try
        Return trafficCount

    End Function

    Protected Function Get_Traffic_Sales_People(ByVal storeCd As String, ByVal trafficDt As String, ByVal hour As String) As String

        Dim trafficHour As String = String.Empty
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String = "select * from brk_traffic_slsp "
        sql = sql & "where store_cd = :STORE and WHEN = to_date(:WHEN, 'DD-MON-YYYY')"

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)

            dbCommand.Parameters.Add(":STORE", OracleType.VarChar)
            dbCommand.Parameters(":STORE").Value = storeCd
            dbCommand.Parameters.Add(":WHEN", OracleType.VarChar)
            dbCommand.Parameters(":WHEN").Value = trafficDt

            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                Dim colName As String = "HR" + hour
                trafficHour = dbReader.Item(colName).ToString
                If isEmpty(trafficHour) Then
                    trafficHour = "0"
                End If
            Else
                Return ""
            End If
            dbReader.Close()
            dbCommand.Dispose()
        Catch ex As Exception
            Throw ex
        Finally
            dbConnection.Close()
        End Try
        Return trafficHour

    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Private Sub PopulateStoreCodes(ByVal coCd As String)

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet

        Dim SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_store_desc from store WHERE co_cd = nvl(:CO_CD, co_cd) order by store_cd"

        cmdGetCodes.Parameters.Add(":CO_CD", OracleType.VarChar)
        cmdGetCodes.Parameters(":CO_CD").Value = coCd

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = SQL
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With txt_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_store_desc"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub

    Protected Sub btn_lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_lookup.Click
        lbl_info.Text = ""
        If isEmpty(txt_traffic_dt.Text) Then
            lbl_info.Text = "Please enter valid Traffic Date ( DD-MON-YYYY )"
            Exit Sub
        End If
        BindGrid()

    End Sub

    Sub BindGrid()

        Dim countTr As String = Get_traffic_count(txt_store_cd.Text, txt_traffic_dt.Text)

        Dim dt As DataTable = New DataTable()

        dt.Columns.Add("start_hr")
        dt.Columns.Add("slsp_cnt")

        Dim dr As DataRow

        For value As Integer = 0 To 23
            dr = dt.NewRow()
            Dim start_hr As String = value.ToString.PadLeft(2, "0")
            dr("start_hr") = start_hr
            If countTr = "0" Then
                dr("slsp_cnt") = "0"
            End If
            If countTr = "1" Then
                dr("slsp_cnt") = Get_Traffic_Sales_People(txt_store_cd.Text, txt_traffic_dt.Text, start_hr)
            End If
            dt.Rows.Add(dr)
        Next

        Gridview1.DataSource = dt
        Gridview1.DataBind()

    End Sub

    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_clear.Click
        Response.Redirect("TrafficSlsp.aspx")
    End Sub

    Protected Sub btn_update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_update.Click

        Dim seq As Integer = 1
        Dim slspBox As TextBox
        Dim slspCnt As String
        Dim slspCntArr(23) As String
        Dim intRows As Integer = Gridview1.Items.Count - 1
        Dim GridItem As DataGridItem
        For intRow = 0 To intRows
            GridItem = Gridview1.Items(intRow)
            slspBox = DirectCast(GridItem.FindControl("slsp_cnt"), TextBox)
            slspCnt = slspBox.Text
            'Dim hour As String = GridItem.Cells(0).Text().Trim()
            slspCntArr(intRow) = slspCnt
            seq = seq + 1
        Next
        Dim countTr As String = Get_traffic_count(txt_store_cd.Text, txt_traffic_dt.Text)
        Dim updateStatus As Boolean
        If countTr = "0" Then
            updateStatus = InsertNewItem(txt_store_cd.SelectedValue, txt_traffic_dt.Text, slspCntArr)
        End If
        If countTr = "1" Then
            updateStatus = UpdateItems(txt_store_cd.SelectedValue, txt_traffic_dt.Text, slspCntArr)
        End If

        If UpdateStatus Then
            lbl_info.Text = "Record(s) Updated"
        Else : lbl_info.Text = "Error updating records. Please try again."
        End If

        Gridview1.DataBind()

    End Sub

    Protected Function InsertNewItem(ByVal storeCd As String, ByVal trafficDt As String, ByVal slspCntStr() As String) As Boolean

        Dim slspCnt() As String = slspCntStr
        Dim hr00 As String = slspCnt(0)
        Dim hr01 As String = slspCnt(1)
        Dim hr02 As String = slspCnt(2)
        Dim hr03 As String = slspCnt(3)
        Dim hr04 As String = slspCnt(4)
        Dim hr05 As String = slspCnt(5)
        Dim hr06 As String = slspCnt(6)
        Dim hr07 As String = slspCnt(7)
        Dim hr08 As String = slspCnt(8)
        Dim hr09 As String = slspCnt(9)
        Dim hr10 As String = slspCnt(10)
        Dim hr11 As String = slspCnt(11)
        Dim hr12 As String = slspCnt(12)
        Dim hr13 As String = slspCnt(13)
        Dim hr14 As String = slspCnt(14)
        Dim hr15 As String = slspCnt(15)
        Dim hr16 As String = slspCnt(16)
        Dim hr17 As String = slspCnt(17)
        Dim hr18 As String = slspCnt(18)
        Dim hr19 As String = slspCnt(19)
        Dim hr20 As String = slspCnt(20)
        Dim hr21 As String = slspCnt(21)
        Dim hr22 As String = slspCnt(22)
        Dim hr23 As String = slspCnt(23)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        sql = "Insert into BRK_TRAFFIC_SLSP (STORE_CD,WHEN,HR00,HR01,HR02,HR03,HR04,HR05,HR06,HR07,HR08,HR09,HR10,HR11,HR12, "
        sql = sql & "HR13,HR14,HR15,HR16,HR17,HR18,HR19,HR20,HR21,HR22,HR23) "
        sql = sql & "values (:STORE,to_date(:WHEN,'DD-MON-YYYY'),:HR00,:HR01,:HR02,:HR03,:HR04,:HR05,:HR06,:HR07,:HR08,:HR09,:HR10,:HR11,:HR12,:HR13, "
        sql = sql & ":HR14,:HR15,:HR16,:HR17,:HR18,:HR19,:HR20,:HR21,:HR22,:HR23)"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        objsql.Parameters.Add(":STORE", OracleType.VarChar)
        objsql.Parameters(":STORE").Value = storeCd
        objsql.Parameters.Add(":WHEN", OracleType.VarChar)
        objsql.Parameters(":WHEN").Value = trafficDt
        objsql.Parameters.Add(":HR00", OracleType.VarChar)
        objsql.Parameters(":HR00").Value = hr00
        objsql.Parameters.Add(":HR01", OracleType.VarChar)
        objsql.Parameters(":HR01").Value = hr01
        objsql.Parameters.Add(":HR02", OracleType.VarChar)
        objsql.Parameters(":HR02").Value = hr02
        objsql.Parameters.Add(":HR03", OracleType.VarChar)
        objsql.Parameters(":HR03").Value = hr03
        objsql.Parameters.Add(":HR04", OracleType.VarChar)
        objsql.Parameters(":HR04").Value = hr04
        objsql.Parameters.Add(":HR05", OracleType.VarChar)
        objsql.Parameters(":HR05").Value = hr05
        objsql.Parameters.Add(":HR06", OracleType.VarChar)
        objsql.Parameters(":HR06").Value = hr06
        objsql.Parameters.Add(":HR07", OracleType.VarChar)
        objsql.Parameters(":HR07").Value = hr07
        objsql.Parameters.Add(":HR08", OracleType.VarChar)
        objsql.Parameters(":HR08").Value = hr08
        objsql.Parameters.Add(":HR09", OracleType.VarChar)
        objsql.Parameters(":HR09").Value = hr09
        objsql.Parameters.Add(":HR10", OracleType.VarChar)
        objsql.Parameters(":HR10").Value = hr10
        objsql.Parameters.Add(":HR11", OracleType.VarChar)
        objsql.Parameters(":HR11").Value = hr11
        objsql.Parameters.Add(":HR12", OracleType.VarChar)
        objsql.Parameters(":HR12").Value = hr12
        objsql.Parameters.Add(":HR13", OracleType.VarChar)
        objsql.Parameters(":HR13").Value = hr13
        objsql.Parameters.Add(":HR14", OracleType.VarChar)
        objsql.Parameters(":HR14").Value = hr14
        objsql.Parameters.Add(":HR15", OracleType.VarChar)
        objsql.Parameters(":HR15").Value = hr15
        objsql.Parameters.Add(":HR16", OracleType.VarChar)
        objsql.Parameters(":HR16").Value = hr16
        objsql.Parameters.Add(":HR17", OracleType.VarChar)
        objsql.Parameters(":HR17").Value = hr17
        objsql.Parameters.Add(":HR18", OracleType.VarChar)
        objsql.Parameters(":HR18").Value = hr18
        objsql.Parameters.Add(":HR19", OracleType.VarChar)
        objsql.Parameters(":HR19").Value = hr19
        objsql.Parameters.Add(":HR20", OracleType.VarChar)
        objsql.Parameters(":HR20").Value = hr20
        objsql.Parameters.Add(":HR21", OracleType.VarChar)
        objsql.Parameters(":HR21").Value = hr21
        objsql.Parameters.Add(":HR22", OracleType.VarChar)
        objsql.Parameters(":HR22").Value = hr22
        objsql.Parameters.Add(":HR23", OracleType.VarChar)
        objsql.Parameters(":HR23").Value = hr23

        Try
            'Execute DataReader 
            objsql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            'Throw New Exception("Error inserting the record. Please try again.")
            conn.Close()
            Return False
        End Try

        Return True
    End Function

    Protected Function UpdateItems(ByVal storeCd As String, ByVal trafficDt As String, ByVal slspCntStr() As String) As Boolean

        Dim slspCnt() As String = slspCntStr
        Dim hr00 As String = slspCnt(0)
        Dim hr01 As String = slspCnt(1)
        Dim hr02 As String = slspCnt(2)
        Dim hr03 As String = slspCnt(3)
        Dim hr04 As String = slspCnt(4)
        Dim hr05 As String = slspCnt(5)
        Dim hr06 As String = slspCnt(6)
        Dim hr07 As String = slspCnt(7)
        Dim hr08 As String = slspCnt(8)
        Dim hr09 As String = slspCnt(9)
        Dim hr10 As String = slspCnt(10)
        Dim hr11 As String = slspCnt(11)
        Dim hr12 As String = slspCnt(12)
        Dim hr13 As String = slspCnt(13)
        Dim hr14 As String = slspCnt(14)
        Dim hr15 As String = slspCnt(15)
        Dim hr16 As String = slspCnt(16)
        Dim hr17 As String = slspCnt(17)
        Dim hr18 As String = slspCnt(18)
        Dim hr19 As String = slspCnt(19)
        Dim hr20 As String = slspCnt(20)
        Dim hr21 As String = slspCnt(21)
        Dim hr22 As String = slspCnt(22)
        Dim hr23 As String = slspCnt(23)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        sql = "Update BRK_TRAFFIC_SLSP set "
        sql = sql & "HR00 = :HR00, "
        sql = sql & "HR01 = :HR01, "
        sql = sql & "HR02 = :HR02, "
        sql = sql & "HR03 = :HR03, "
        sql = sql & "HR04 = :HR04, "
        sql = sql & "HR05 = :HR05, "
        sql = sql & "HR06 = :HR06, "
        sql = sql & "HR07 = :HR07, "
        sql = sql & "HR08 = :HR08, "
        sql = sql & "HR09 = :HR09, "
        sql = sql & "HR10 = :HR10, "
        sql = sql & "HR11 = :HR11, "
        sql = sql & "HR12 = :HR12, "
        sql = sql & "HR13 = :HR13, "
        sql = sql & "HR14 = :HR14, "
        sql = sql & "HR15 = :HR15, "
        sql = sql & "HR16 = :HR16, "
        sql = sql & "HR17 = :HR17, "
        sql = sql & "HR18 = :HR18, "
        sql = sql & "HR19 = :HR19, "
        sql = sql & "HR20 = :HR20, "
        sql = sql & "HR21 = :HR21, "
        sql = sql & "HR22 = :HR22, "
        sql = sql & "HR23 = :HR23 "

        sql = sql & "WHERE store_cd = :STORE and WHEN = to_date(:WHEN,'DD-MON-YYYY')"

        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        objsql.Parameters.Add(":STORE", OracleType.VarChar)
        objsql.Parameters(":STORE").Value = storeCd
        objsql.Parameters.Add(":WHEN", OracleType.VarChar)
        objsql.Parameters(":WHEN").Value = trafficDt
        objsql.Parameters.Add(":HR00", OracleType.VarChar)
        objsql.Parameters(":HR00").Value = hr00
        objsql.Parameters.Add(":HR01", OracleType.VarChar)
        objsql.Parameters(":HR01").Value = hr01
        objsql.Parameters.Add(":HR02", OracleType.VarChar)
        objsql.Parameters(":HR02").Value = hr02
        objsql.Parameters.Add(":HR03", OracleType.VarChar)
        objsql.Parameters(":HR03").Value = hr03
        objsql.Parameters.Add(":HR04", OracleType.VarChar)
        objsql.Parameters(":HR04").Value = hr04
        objsql.Parameters.Add(":HR05", OracleType.VarChar)
        objsql.Parameters(":HR05").Value = hr05
        objsql.Parameters.Add(":HR06", OracleType.VarChar)
        objsql.Parameters(":HR06").Value = hr06
        objsql.Parameters.Add(":HR07", OracleType.VarChar)
        objsql.Parameters(":HR07").Value = hr07
        objsql.Parameters.Add(":HR08", OracleType.VarChar)
        objsql.Parameters(":HR08").Value = hr08
        objsql.Parameters.Add(":HR09", OracleType.VarChar)
        objsql.Parameters(":HR09").Value = hr09
        objsql.Parameters.Add(":HR10", OracleType.VarChar)
        objsql.Parameters(":HR10").Value = hr10
        objsql.Parameters.Add(":HR11", OracleType.VarChar)
        objsql.Parameters(":HR11").Value = hr11
        objsql.Parameters.Add(":HR12", OracleType.VarChar)
        objsql.Parameters(":HR12").Value = hr12
        objsql.Parameters.Add(":HR13", OracleType.VarChar)
        objsql.Parameters(":HR13").Value = hr13
        objsql.Parameters.Add(":HR14", OracleType.VarChar)
        objsql.Parameters(":HR14").Value = hr14
        objsql.Parameters.Add(":HR15", OracleType.VarChar)
        objsql.Parameters(":HR15").Value = hr15
        objsql.Parameters.Add(":HR16", OracleType.VarChar)
        objsql.Parameters(":HR16").Value = hr16
        objsql.Parameters.Add(":HR17", OracleType.VarChar)
        objsql.Parameters(":HR17").Value = hr17
        objsql.Parameters.Add(":HR18", OracleType.VarChar)
        objsql.Parameters(":HR18").Value = hr18
        objsql.Parameters.Add(":HR19", OracleType.VarChar)
        objsql.Parameters(":HR19").Value = hr19
        objsql.Parameters.Add(":HR20", OracleType.VarChar)
        objsql.Parameters(":HR20").Value = hr20
        objsql.Parameters.Add(":HR21", OracleType.VarChar)
        objsql.Parameters(":HR21").Value = hr21
        objsql.Parameters.Add(":HR22", OracleType.VarChar)
        objsql.Parameters(":HR22").Value = hr22
        objsql.Parameters.Add(":HR23", OracleType.VarChar)
        objsql.Parameters(":HR23").Value = hr23

        Try
            'Execute DataReader 
            objsql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            'Throw New Exception("Error inserting the record. Please try again.")
            conn.Close()
            Return False
        End Try

        Return True
    End Function

End Class
