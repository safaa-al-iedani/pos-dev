<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" 
    CodeFile="PosVen.aspx.vb" Inherits="PosVen" meta:resourcekey="PageResource1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div id="Div1" runat="server">  
   <%--<script type="text/javascript" language="javascript">
         function SetCaret_OnFocus(s, e) {
             // sabrina set the focus on the begining of each column
             var text = s.GetText().replace(/ /g, "").replace(/_/g, "");
             window.setTimeout(function () { s.SetCaretPosition(2); }, 10);
             }
         }
    </script> --%>
     <%-- =============== HEADING ==========================================   --%> 
    <table  width="100%" class="style5">
      <tr>
      <td align="center" > 
      <asp:Label ID="Label1" runat="server" Text="Oracle Vendor Maintenance" Font-Bold="True" ></asp:Label>
      </td>
      </tr>
     </table>
       <br />
      <%-- ===============MESSAGES =========lbl_msg ==========================================   --%> 

     <table>
         <tr>
             <td>
     <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" Font-Size="small" ForeColor="Blue" BackColor="White" runat="server"
        Text="" Width="100%"> 
     </dx:ASPxLabel>
            </td>
            </tr>
     </table>
  <%-- ============================================   --%> 
    <asp:Panel ID="Panel5" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >
     <table>
          <tr>
         
           <td style="text-align:right"> <dx:ASPxLabel ID="ASPxLabel5" Text="CO CD" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
           <td> <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" 
                Width  ="300px" ID="srch_co_cd">                   
                </asp:dropdownlist>   </td>
           </tr>
         <tr></tr>
          <tr></tr>
           
         <tr>
           <td style="text-align:right"> <dx:ASPxLabel ID="ASPxLabel2" Text="VE CD" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
           <td> <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" 
                Width  ="300px" ID="srch_ve_cd">                   
                </asp:dropdownlist>   </td>
          </tr>
         <tr></tr>
          <tr></tr>

              <tr>
           
           <td style="text-align:right"> <dx:ASPxLabel ID="ASPxLabel6" Text="Supplier No." Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
           <td> <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" 
                Width  ="300px" ID="srch_supplier_no">                   
                </asp:dropdownlist>   </td>
          </tr>
          <tr></tr>
          <tr></tr>    
          </table>
    <br />
  
        
      <table>
          <%-- SEARCH BUTTON --%>
            <tr>
                <td style="width: 60px;">
                <dx:ASPxButton ID="btn_search" runat="server" Font-Bold="true" Text="Search">
                </dx:ASPxButton>
           </td>     
         
         <%-- CLEAR BUTTON --%>
           <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Font-Bold="true" Text="<%$ Resources:LibResources, Label888 %>">
                </dx:ASPxButton>
           </td> 
       </tr>
       </table>
       </asp:Panel>
    <br />
    <br />
    <%-- -------------------------------------------------- ----------------------------------------------------- --%>
    <%-- -------------------------------------------------- EDIT grid-------------------------------------------- --%>
    <%-- -------------------------------------------------- ----------------------------------------------------- --%>
    
     <%-- ===========================================================================   --%>  
          <dxpc:ASPxPopupControl ID="ASPxPopupPosVen" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Oracle Vendor Maintenance" Height="80px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="750px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" DefaultButton="btnSubmit" runat="server">
        
          <asp:TextBox ID="lbl_msg2" BorderColor="Brown"  BackColor="WhiteSmoke" forecolor="Blue" font-size="Small" runat="server" AutoPostBack="true" Width="900px" CssClass="style5" Enabled="false" ReadOnly="True" BorderStyle="None" Font-Bold="True"></asp:TextBox>
          <br />
          <br />
 
    <asp:Panel ID="Panel3" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
       <dx:ASPxGridView ID="GridView1" Settings-ShowFooter="true"  runat="server" 
           KeyFieldName="CO_CD;VE_CD;SUPPLIER_NO;SUPPLIER_NM;SUPPLIER_SITE;PAYMENT_TERMS;ROW_ID" 
           AutoGenerateColumns="False" Width="98%" ClientInstanceName="GridView1" OnInitNewRow="GridView1_InitNewRow" oncelleditorinitialize="GridView1_CellEditorInitialize" OnRowInserting="ASPxGridView1_RowInserting" OnRowDeleting="ASPxGridView1_RowDeleting" OnRowUpdating="ASPxGridView1_RowUpdating" 
           OnRowValidating="ASPxGridView1_RowValidating">
        <SettingsEditing mode="EditForm"/>
        <SettingsCommandButton> 
         <UpdateButton Text="<%$ Resources:LibResources, Label885 %>"></UpdateButton>
         <CancelButton Text="<%$ Resources:LibResources, Label886 %>"></CancelButton>
        </SettingsCommandButton>
                 
 
        <Columns>
          
           <dx:GridViewDataTextColumn Caption="COCD" FieldName="CO_CD" VisibleIndex="2">
                <PropertiesTextEdit Width="100" MaxLength="3">
                 <ClientSideEvents keyUp="function(s, e) { var txt = s.GetText(); s.SetText(txt.toUpperCase()); }"  />
                    <ValidationSettings>
                      <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Vendor" FieldName="VE_CD" VisibleIndex="3">
                <PropertiesTextEdit Width="100" MaxLength="4">
                     <ClientSideEvents keyUp="function(s, e) { var txt = s.GetText(); s.SetText(txt.toUpperCase()); }"  />
                    <ValidationSettings>
                    <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                                     
                    </ValidationSettings>
                </PropertiesTextEdit>
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Supplier#" FieldName="SUPPLIER_NO" VisibleIndex="4">
                <PropertiesTextEdit Width="150" MaxLength="30">
                    <ClientSideEvents keyUp="function(s, e) { var txt = s.GetText(); s.SetText(txt.toUpperCase()); }"  />
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Right" >
                        <RequiredField IsRequired="false" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                        <RegularExpression ErrorText="Invalid Value" />
                    </ValidationSettings>
                </PropertiesTextEdit>
           </dx:GridViewDataTextColumn>
            
           <dx:GridViewDataTextColumn Caption="Supplier Name" FieldName="SUPPLIER_NM" VisibleIndex="5">
               <PropertiesTextEdit Width="400" MaxLength="60">
               <%--<ClientSideEvents keyUp="function(s, e) { var txt = s.GetText(); s.SetText(txt.toUpperCase()); }"  />--%>
                    <ValidationSettings>
                      <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
             <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Supplier Site" FieldName="SUPPLIER_SITE" VisibleIndex="6">
               <PropertiesTextEdit Width="150" MaxLength="40">
                <%--<ClientSideEvents keyUp="function(s, e) { var txt = s.GetText(); s.SetText(txt.toUpperCase()); }"  />--%>
                    <ValidationSettings>
                      <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>

         <dx:GridViewDataTextColumn Caption="Payment Terms" FieldName="PAYMENT_TERMS" VisibleIndex="7">
               <PropertiesTextEdit Width="150" MaxLength="40">
                   <%--<ClientSideEvents keyUp="function(s, e) { var txt = s.GetText(); s.SetText(txt.toUpperCase()); }"  />--%>
                    <ValidationSettings>
                      <RequiredField IsRequired="true" ErrorText="<%$ Resources:LibResources, Label909 %>"/>
                    </ValidationSettings>
                </PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>
          
          <dx:GridViewDataTextColumn Caption="WHS TP" FieldName="WHS_TP" VisibleIndex="8">
              <PropertiesTextEdit  Width="150" MaxLength="50">
              <ClientSideEvents keyUp="function(s, e) { var txt = s.GetText(); s.SetText(txt.toUpperCase()); }"  />
              </PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>
                       
          <dx:GridViewDataTextColumn Caption="TAX CD" FieldName="TAX_CD" VisibleIndex="9">
               <PropertiesTextEdit Width="150" MaxLength="50">
                <ClientSideEvents keyUp="function(s, e) { var txt = s.GetText(); s.SetText(txt.toUpperCase()); }"  />
                </PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>
            
         <%-- ROWID ** sy note** THIS FIELD IS ADDED TO MAKE THE ROW UNIQUE--%>
         <dx:GridViewDataTextColumn Caption="" FieldName="ROW_ID" ReadOnly="true" Visible="false" VisibleIndex="11">
         </dx:GridViewDataTextColumn>
       
         <dx:GridViewDataTextColumn Caption="Last Upd" FieldName="LAST_UPDATE" ReadOnly="true" Visible="true" VisibleIndex="13">
         <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" DisplayFormatString="MM/dd/yyyy"  Width="100"></PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         
         <dx:GridViewDataTextColumn Caption="UPD BY" FieldName="CHANGED_BY" ReadOnly="true" Visible="true" VisibleIndex="14">
         <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" Width="100"></PropertiesTextEdit>
         </dx:GridViewDataTextColumn>


         <dx:GridViewCommandColumn ShowEditButton="true" ShowNewButtonInHeader="true" ShowDeleteButton="true" VisibleIndex="19"/>
         </Columns>
        
        <SettingsBehavior AllowSort="False"/>
        <SettingsPager Position="Bottom" Visible="true" Mode="ShowPager" PageSize="10"/>
           
        <SettingsBehavior ConfirmDelete="true"   />
        <SettingsText  ConfirmDelete="Please press 'OK' to confirm and Delete this record" />
    
    </dx:ASPxGridView>
   
    </asp:Panel>

    </dxpc:PopupControlContentControl>
    </ContentCollection>
    </dxpc:ASPxPopupControl>
<%-- -------------------------------------------------- ----------------------------------------------------- --%>
<%-- -------------------------------------  BROWSE grid ----------------------------------------------------- --%>
<%-- -------------------------------------------------- ----------------------------------------------------- --%>
<%-- ===========================================================================   --%>  
 <dxpc:ASPxPopupControl ID="ASPxPopupPosVen2" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Oracle Vendor Mapping" Height="80px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="750px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" DefaultButton="btnSubmit" runat="server">
        
         <br />
         <br />
     <%-- gridview --%>
<%-- ===== --%>
    <asp:Panel ID="Panel1" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  
         <dx:ASPxGridView ID="GridView9" Settings-ShowFooter="true" runat="server" 
             KeyFieldName="CO_CD;VE_CD;SUPPLIER_NO;SUPPLIER_NM;SUPPLIER_SITE;PAYMENT_TERMS;WHS_TP;TAX_CD;ROW_ID" 
             AutoGenerateColumns="False" 
            Width="98%"  ClientInstanceName="GridView9">
         <Columns>
          <dx:GridViewDataTextColumn Caption="COCD" FieldName="CO_CD" VisibleIndex="2">
             <PropertiesTextEdit Width="100" ReadOnlyStyle-BackColor="Snow" MaxLength="10"></PropertiesTextEdit>
             <CellStyle Wrap="False" />                 
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Vendor" FieldName="VE_CD" VisibleIndex="3">
                <PropertiesTextEdit Width="100"  ReadOnlyStyle-BackColor="Snow"></PropertiesTextEdit>
              <CellStyle Wrap="False" /> 
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Supplier#" FieldName="SUPPLIER_NO" VisibleIndex="4">
                <PropertiesTextEdit Width="150"   ReadOnlyStyle-BackColor="Snow"></PropertiesTextEdit>
               <CellStyle Wrap="False" /> 
           </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="Supplier Name" FieldName="SUPPLIER_NM" VisibleIndex="5">
               <PropertiesTextEdit Width="400"  ReadOnlyStyle-BackColor="Snow"></PropertiesTextEdit>
             <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Supplier Site" FieldName="SUPPLIER_SITE" VisibleIndex="6">
               <PropertiesTextEdit Width="150" MaxLength="50" ReadOnlyStyle-BackColor="Snow"></PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn Caption="Payment Terms" FieldName="PAYMENT_TERMS" VisibleIndex="7">
               <PropertiesTextEdit Width="150" MaxLength="50" ReadOnlyStyle-BackColor="Snow"></PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="WHS TP" FieldName="WHS_TP" VisibleIndex="8">
               <PropertiesTextEdit Width="150" MaxLength="50" ReadOnlyStyle-BackColor="Snow"></PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="TAX CD" FieldName="TAX_CD" VisibleIndex="9">
               <PropertiesTextEdit Width="150" MaxLength="50" ReadOnlyStyle-BackColor="Snow"></PropertiesTextEdit>
              <CellStyle Wrap="False"/>
          </dx:GridViewDataTextColumn>
       
               <%-- ROWID ** sy note** THIS FIELD IS ADDED TO MAKE THE ROW UNIQUE--%>
             <dx:GridViewDataTextColumn Caption="" FieldName="ROW_ID" ReadOnly="true" Visible="false" VisibleIndex="11">
             </dx:GridViewDataTextColumn>

         <dx:GridViewDataTextColumn Caption="Last Upd" FieldName="LAST_UPDATE" ReadOnly="true" Visible="true" VisibleIndex="13">
           <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" DisplayFormatString="MM/dd/yyyy"  Width="100"></PropertiesTextEdit>
         </dx:GridViewDataTextColumn>
         <dx:GridViewDataTextColumn Caption="UPD BY" FieldName="CHANGED_BY" ReadOnly="true" Visible="true" VisibleIndex="14">
             <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" Width="100"></PropertiesTextEdit></dx:GridViewDataTextColumn>
         <%--<dx:GridViewCommandColumn ShowEditButton="true" ShowNewButtonInHeader="true" ShowDeleteButton="true" VisibleIndex="19"/>--%>
      </Columns>
        
        <SettingsBehavior  AllowSort="False" />
        <SettingsPager  Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />
      
    </dx:ASPxGridView>
    </asp:Panel>

</dxpc:PopupControlContentControl>
</ContentCollection>
</dxpc:ASPxPopupControl>

 
    <br />
   </div>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
