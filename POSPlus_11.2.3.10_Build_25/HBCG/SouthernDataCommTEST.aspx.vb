Imports System.Data.OracleClient
Imports SD_Utils

Partial Class SouthernDataCommTEST
    Inherits POSBasePage

    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim thistransaction As OracleTransaction
        Dim NEW_EXP_DT As String = ""

        If txtCardNum.Text & "" = "" Then
            lbl_Full_Name.Text = "No Credit Card Number Found"
            Exit Sub
        End If
        If TextBox2.Text & "" = "" Or TextBox3.Text & "" = "" Then
            lbl_Full_Name.Text = "No Expiration Date Found"
            Exit Sub
        End If
        Dim myVerify As New VerifyCC
        Dim myTypeValid As New VerifyCC.TypeValid
        myTypeValid = myVerify.GetCardInfo(txtCardNum.Text)
        If myTypeValid.CCValid = True Then
            lbl_card_type.Text = UCase(myTypeValid.CCType)
        Else
            lbl_card_type.Text = "This card is not valid, please try again."
            txtCardNum.Text = ""
            lbl_Full_Name.Text = ""
            TextBox2.Text = ""
            TextBox3.Text = ""
            lbl_card_type.Text = ""
            txtCardNum.Enabled = True
            TextBox2.Enabled = True
            TextBox3.Enabled = True
            txtCardNum.Focus()
            Exit Sub
        End If

        NEW_EXP_DT = MonthLastDay(TextBox2.Text & "/1/" & TextBox3.Text)
        objConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        objConnection.Open()
        thistransaction = objConnection.BeginTransaction
        With cmdInsertItems
            .Transaction = thistransaction
            .Connection = objConnection
            .CommandText = "insert into payment (sessionid, MOP_CD, AMT, DES, BC, EXP_DT) values ('" & Session.SessionID.ToString.Trim & "','BC'," & txt_amt.Text & ",'" & UCase(myTypeValid.CCType) & "','" & AppUtils.Encrypt(Trim(txtCardNum.Text), "CrOcOdIlE") & "','" & NEW_EXP_DT & "')"
        End With
        cmdInsertItems.ExecuteNonQuery()
        thistransaction.Commit()

        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim payment_id As Integer = 0
        Dim sql As String = ""
        sql = "SELECT ROW_ID FROM PAYMENT WHERE SESSIONID='" & Session.SessionID.ToString.Trim & "' AND MOP_CD='BC' AND AMT=" & txt_amt.Text & " ORDER BY ROW_ID DESC"
        'Set SQL OBJECT 
        objsql = DisposablesManager.BuildOracleCommand(sql, objConnection)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

            If (MyDataReader.Read()) Then
                payment_id = CInt(MyDataReader.Item("ROW_ID").ToString)
            End If

            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            objConnection.Close()
            Throw
        End Try
        objConnection.Close()

        Response.Write(SD_Submit_Query(payment_id, "09"))

    End Sub

    Public Function MonthLastDay(ByVal dCurrDate As Date)

        MonthLastDay = DateSerial(Year(dCurrDate), Month(dCurrDate), 1).AddMonths(1).AddDays(-1)

    End Function

    Protected Sub txtCardNum_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCardNum.TextChanged

        If InStr(txtCardNum.Text, "^") > 0 Then
            Dim sCC As Array
            sCC = Split(txtCardNum.Text.ToString, "^")
            txtCardNum.Text = Right(sCC(0).Trim, Len(sCC(0)) - 2)
            TextBox3.Text = Left(sCC(2).Trim, 2)
            TextBox2.Text = Left(Mid(sCC(2).Trim, 3), 2)
            lbl_Full_Name.Text = sCC(1).Trim
            txtCardNum.Enabled = False
            TextBox2.Enabled = False
            TextBox3.Enabled = False
        Else
            TextBox2.Focus()
        End If
        Dim myVerify As New VerifyCC
        Dim myTypeValid As New VerifyCC.TypeValid
        myTypeValid = myVerify.GetCardInfo(txtCardNum.Text)
        If myTypeValid.CCValid = True Then
            lbl_card_type.Text = UCase(myTypeValid.CCType)
        Else
            lbl_card_type.Text = "This card is not valid, please try again."
            txtCardNum.Text = ""
            lbl_Full_Name.Text = ""
            TextBox2.Text = ""
            TextBox3.Text = ""
            lbl_card_type.Text = ""
            txtCardNum.Enabled = True
            TextBox2.Enabled = True
            TextBox3.Enabled = True
            txtCardNum.Focus()
        End If

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txtCardNum.Text = ""
        lbl_Full_Name.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        lbl_card_type.Text = ""
        txtCardNum.Enabled = True
        TextBox2.Enabled = True
        TextBox3.Enabled = True
        txtCardNum.Focus()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
