﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" CodeFile="FranchiseShippingSchedule.aspx.vb" 
    Inherits="FranchiseShippingSchedule" culture="auto"  meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.v13.2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
       
        function pageLoad(sender, args) {
            

            $('input[type=submit], a, button').click(function (event) {

                if ($('#hidUpdLnNos').val() != null && $.trim($('#hidUpdLnNos').val()) != '' && $(this).attr('href') != null) {

                    if ($.trim($(this).attr('href')) != '' && $.trim($(this).attr('href')).indexOf('javascript') < 0) {

                        $('#hidDestUrl').val($.trim($(this).attr('href')));

                        event.preventDefault();
                    }
                }
            });
        }

        function disableButton(id) {
            // Daniela B 20140723 prevent double click
            try {
                var a = document.getElementById(id);
                a.style.display = 'none';
            } catch (err) {
                alert('Error in disableButton ' + err.message);
                return false;
            }
            return true;
        }

        function _getKeyCode(evt) {
            return (typeof (evt.keyCode) != "undefined" && evt.keyCode != 0) ?
                evt.keyCode : evt.charCode;
        }

        $("[src*=plus]").live("click", function () {
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("src", "images/minus.png");
        });
            $("[src*=minus]").live("click", function () {
                $(this).attr("src", "images/plus.png");
                $(this).closest("tr").next().remove();
            });

    </script>

  <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
   <ContentTemplate>
    
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>

    <table id="FranchiseShippingSchedule" style="position: relative; width: 1000px; left: 0px; top: 0px;" cellpadding="2px">

       <tr style="border:solid;border-color:green;border-width:medium">
                <td style="width:17%">  </td>
                <td style="width:35%"> </td>
                <td style="width:40%">  Last Change Date:  
                
                     <asp:Label runat="server" ID="lblDate" MaxLength="11" Text="" Enabled="false" ></asp:Label>
                     <dx:ASPxDateEdit ID="ASPxDateEdit" runat="server" Visible="false" EnableViewState="true" AutoPostBack="true" Width="100px"  AllowUserInput="false" style="border:solid;border-width:thin;border-color:steelblue" ToolTip="Select a date to filter the delivery dates.">
                    </dx:ASPxDateEdit>
                </td>
               <%-- <td style="width:20%"> 
                     <dx:ASPxDateEdit ID="ASPxDateEdit" runat="server" Visible="false" EnableViewState="true" AutoPostBack="true" Width="100px"  AllowUserInput="false" style="border:solid;border-width:thin;border-color:steelblue" ToolTip="Select a date to filter the delivery dates.">
                    </dx:ASPxDateEdit>
                </td>--%>
           </tr>    
           
          <tr >
                <td style="width:17%">
                    <asp:Label runat="server" ID="lblCreate" Text="Create/Query"></asp:Label>
                </td>
                <td style="width:35%">
                    <asp:RadioButton ID="rdbCreate" runat="server" Text="Create" Checked="true" GroupName="CreateQuery" OnCheckedChanged="rdbCreate_OnCheckedChanged" AutoPostBack="true" /> &nbsp;
                    <asp:RadioButton ID="rdbQuery" runat="server" Text="Query"  GroupName="CreateQuery" OnCheckedChanged="rdbQuery_OnCheckedChanged" AutoPostBack="true"/>
                </td>
                    <td style="width:40%">
                        <asp:Label runat="server" ID="lblFrCustCdCaption" Text="Franchise Customer Code"></asp:Label>
                        <asp:Label runat="server" ID="lblFrCustCd" Text=""></asp:Label>
                </td>
               <%-- <td style="width:20%">
                    <asp:Label runat="server" ID="lblFrCustCd" Text=""></asp:Label>
                </td>--%>
           </tr>       
           
            <!-- Row 2 --> 
            <tr>
                <td style="width:17%">
                    <asp:Label runat="server" ID="lblFrStoreCd" Text="Franchise Store #"></asp:Label>
                </td>
                <td style="width:35%">
                      <dx:ASPxComboBox ID="ddl_StoreCds" runat="server" AutoPostBack="True"   CssClass="style5"  IncrementalFilteringMode="StartsWith" Paddings-PaddingLeft="2px"
                           OnSelectedIndexChanged="cbo_StoreCds_SelectedIndexChanged"   AppendDataBoundItems="True" Width="250px" Height="25px" TabIndex="1" style="border:solid;border-width:thin;border-color:steelblue" EnableViewState="true">
                    </dx:ASPxComboBox>
                    <%--<asp:TextBox runat="server" ID="txtFrStoreName"></asp:TextBox>--%>
                </td>
              <%--      <td style="width:17%">
                        <asp:Label runat="server" ID="lblCorpFrStoreNoCaption" Text="Franchise Store #"></asp:Label>
                </td>--%>
                <td style="width:35%">
                    <%--<asp:Label runat="server" ID="lblCorpFrStoreNo" Text=""></asp:Label>--%>
                    <%--<asp:Label runat="server" ID="lblCorpFrStoreName" Text=""></asp:Label>--%>
                </td>
            </tr>       
           
                        
            <!-- Row 3 --> 
            <tr>
                <td style="width:17%">
                    <asp:Label runat="server" ID="lblCityCaption" Text="City"></asp:Label>
                </td>
                <td style="width:35%">
                     <asp:Label runat="server" ID="lblCity" Text=""></asp:Label>
                </td>
            </tr>      

        <!-- Row 4 -Show only when Query is selected -->
        <tr>
                <td style="width:17%">
                        <asp:Label runat="server" ID="lblQuery_ConfirmNum" Text="Confirmation #" Visible="false" ></asp:Label>
                </td>
                <td style="width:35%">
                    <asp:TextBox runat="server" ID="txtQuery_ConfirmationNum" MaxLength="17" Width="200px" Visible="false"></asp:TextBox>
                </td>
        </tr>
        </table>

       <div style="width:950px;width:95%;text-align:center" >
             <dx:ASPxButton ID="btnSearch" runat="server" Text="Search"  OnClick="btnSearch_Click" Visible="false" >
             </dx:ASPxButton>
            <dx:ASPxButton ID="btnClear" runat="server" OnClick="btnClear_Click"  Text="Clear" Visible="false">
           </dx:ASPxButton>
       </div>
    <hr />

    <div id="dvCreate" runat="server">
       
    <asp:Label ID="litMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    <!-- GRIDVIEW FOR ITEMS -->
    <div id="dvGridItems" runat="server">
        <asp:GridView ID="grvItems" runat="server" ShowFooter="True" AutoGenerateColumns="false"  OnRowDataBound="grvItems_OnRowDataBound" OnRowCommand="grvItems_OnRowCommand"
                    CellPadding="4" ForeColor="#333333"   GridLines="None" OnRowDeleting="grvItems_RowDeleting" >
            <Columns>
                <asp:BoundField DataField="RowNumber"  HeaderText="Line #" />
                <asp:TemplateField HeaderText="Day of the Week">
                    <ItemTemplate>
                         <asp:DropDownList ID="ddlDayofWeek" runat="server"  >
                            <asp:ListItem Value="M">Monday</asp:ListItem>
                            <asp:ListItem Value="T">Tuesday</asp:ListItem>
                            <asp:ListItem Value="W">Wednesday</asp:ListItem>
                            <asp:ListItem Value="R">Thursday</asp:ListItem>
                            <asp:ListItem Value="F">Friday</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Corporate Store Code">
                    <ItemTemplate>
                     <asp:DropDownList ID="ddlCorpStoreCode" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCorpStoreCode_SelectedIndexChanged" ></asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>

              <%--  <asp:TemplateField HeaderText="Corporate Store Name">
                    <ItemTemplate>
                        <asp:TextBox ID="txtDesc" runat="server"  MaxLength="30" Width="250px" Text='<%# Eval("col3")%>' Enabled="false"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>--%>

                <asp:TemplateField HeaderText="Corporate Delivery Zone Code">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlDelZoneCode" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDelZoneCode_SelectedIndexChanged">
                        </asp:DropDownList>
                    </ItemTemplate>

                      <FooterStyle HorizontalAlign="Right" />
                    <FooterTemplate>
                        <asp:Button ID="ButtonAdd" runat="server" 

                                Text="Add New Row" OnClick="btnAdd_Click" />
                    </FooterTemplate>
                </asp:TemplateField>


                <asp:CommandField ShowDeleteButton="True" />
            </Columns>

            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </div>

<%--    <div class="row" style="width:100%">
    <div class="col-md-4" style="border-right-style:solid;width:45%">
      <table id="Table1" width="45%">
          <tr >
                <td style="width:15%">
                    <asp:Label runat="server" ID="Label1" Text="Create/Query"></asp:Label>
                </td>
                <td style="width:30%">
                    <asp:TextBox runat="server" ID="TextBox1"></asp:TextBox>
                </td>
          </tr> 
          <tr>
                <td style="width:15%">
                        <asp:Label runat="server" ID="Label2" Text="Franchise Customer Code"></asp:Label>
                </td>
                <td style="width:30%">
                    <asp:TextBox runat="server" ID="TextBox2"></asp:TextBox>
                </td>
           </tr>       
      </table>
    </div>--%>

  
    <hr />

   <!-- APPROVING AREA -->
    <table style="width:100%" cellpadding="2px">
        <tr>
            <td style="width:10%">
                <asp:Label runat="server" ID="Label9" Text="Approval Password:"></asp:Label>
            </td>
            <td style="width:25%">
                <asp:TextBox runat="server" ID="txtApprover_Pswd" MaxLength="17" Width="300px" AutoPostBack="true" OnTextChanged="txtApprover_Pswd_TextChanged" ></asp:TextBox>
            </td>

            <td style="width:45%">
                <asp:Label runat="server" ID="Label10" Text="Approving Associate's Code:"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="txtApprover_User_Init" Width="360px" Enabled="false" ></asp:TextBox>
            </td>
           <%-- <td style="width:25%">
                <asp:TextBox runat="server" ID="txtApprover_User_Init" Width="300px"></asp:TextBox>
            </td>--%>
        </tr>

         <tr>
            <td style="width:10%">
               
            </td>
            <td style="width:25%;text-align:right;padding-right:50px">
                <asp:Button ID="btnConfirmASP" runat="server" Text="COMMIT" Click="btnConfirm_Click" Enabled="true" CausesValidation="False" />
               
                <%--<asp:Button ID="btnConfirmASP" runat="server" Text="COMMIT" BorderWidth="5px" BorderStyle="Solid" BorderColor="RoyalBlue" Click="btnConfirm_Click" Enabled="true" />--%>
                <%--<dx:ASPxButton  ID="btnConfirm" runat="server" Text="COMMIT" Click="btnConfirm_Click" Enabled="true" ></dx:ASPxButton>--%>
             </td>

            <td style="width:45%">
                <asp:Label runat="server" ID="lblConfirm" Text="Confirmation #"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                  <asp:TextBox runat="server" ID="txtConfirmationNum" Width="430px" Enabled="false" ></asp:TextBox>
            </td>
           <%-- <td style="width:35%">
                <asp:TextBox runat="server" ID="txtConfirmationNum" Width="300px"></asp:TextBox>
            </td>--%>
        </tr>
    </table>

    </div>

     <div id="dvQuery" runat="server" style="width:98%">
      
         <!-- Gridview Expand -->
         <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="false" CssClass="Grid"
            DataKeyNames="CONFIRM_NUM"  OnRowDataBound="OnRowDataBound" PageSize="10"  AllowSorting="true"  >
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <img id="detailImage" runat="server" alt = "" style="cursor: pointer" src="images/plus.png" />
                    
                           <asp:Panel ID="pnl_Ln" runat="server" Style="display: none">
                    
                            <asp:GridView ID="gvDetails_Ln" runat="server" AutoGenerateColumns="false" CssClass = "ChildGrid" OnRowDeleting="gvDetails_Ln_RowDeleting">
                                <Columns>
                                    <asp:BoundField ItemStyle-Width="125px" DataField="CONFIRM_NUM" HeaderText="Confirmation #" />
                                    <asp:BoundField ItemStyle-Width="125px" DataField="DAY_OF_WEEK" HeaderText="Day Of the Week" />
                                    <asp:BoundField ItemStyle-Width="330px" DataField="CORP_STORE_FULL_DESC" HeaderText="Corp. Store Code" />
                                    <asp:BoundField ItemStyle-Width="330px" DataField="DEL_ZONE_FULL_DESC" HeaderText="Corp. Delivery Zone Code" />

                                   <asp:CommandField ShowDeleteButton="True" />  
                                </Columns>
                                    <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                             </asp:GridView>
                       
                        </asp:Panel>
                    </ItemTemplate>
                </asp:TemplateField>
                 
                <asp:BoundField ItemStyle-Width="100px" DataField="CONFIRM_NUM" HeaderText="Confirmation #" />
                <asp:BoundField ItemStyle-Width="280px" DataField="STORE_FULL_DESC" HeaderText="Store #" />
                <asp:BoundField ItemStyle-Width="100px" DataField="CO_CD" HeaderText="Company CD" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="100px" DataField="CUST_CD" HeaderText="Customer CD" ControlStyle-Width="100px" />
                <asp:BoundField ItemStyle-Width="70px" DataField="APRV_USER_INIT" HeaderText="Approver" />
                <asp:BoundField ItemStyle-Width="100px" DataField="UPD_BY" HeaderText="Updated By" />
                <asp:BoundField ItemStyle-Width="130px" DataField="UPD_DT" HeaderText="Last Updated Date" HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}" />
            </Columns>
        </asp:GridView>

           <table width="98%">
            <tr>
                <td align="center">
                    <table width="35%">
                        <tr>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnFirst" OnClick="PageButtonClick" runat="server" Text="<< First"
                                    ToolTip="Go to first page" CommandArgument="First" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnPrev" OnClick="PageButtonClick" runat="server" Text="< Prev"
                                    ToolTip="Go to the previous page" CommandArgument="Prev" Visible="false">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnNext" OnClick="PageButtonClick" runat="server" Text="Next >"
                                    ToolTip="Go to the next page" CommandArgument="Next" Visible="False">
                                </dx:ASPxButton>
                            </td>
                            <td width="15%" align="center">
                                <dx:ASPxButton ID="btnLast" OnClick="PageButtonClick" runat="server" Text="Last >>"
                                    ToolTip="Go to the last page" CommandArgument="Last" Visible="false">
                                </dx:ASPxButton>
                            </td>

                        </tr>
                    </table>
                    <dx:ASPxLabel ID="lbl_pageinfo" runat="server" Text="" Visible="false">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>


     </div>
  
      </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>