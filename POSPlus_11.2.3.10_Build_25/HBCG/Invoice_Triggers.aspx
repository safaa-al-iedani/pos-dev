<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Invoice_Triggers.aspx.vb" Inherits="Invoice_Triggers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="admin_form" runat="server">
        <table class="style5">
            <tr>
                <td>
                    If
                </td>
                <td>
                    <asp:DropDownList ID="cbo_trigger_types" runat="server" CssClass="style5">
                        <asp:ListItem Value="P_D">DELIVERY/PICKUP</asp:ListItem>
                        <asp:ListItem Value="DROP_DT">DROP DATE</asp:ListItem>
                        <asp:ListItem Value="ITM_TP_CD">ITEM TYPE CODE</asp:ListItem>
                        <asp:ListItem Value="MAJOR_CD">MAJOR</asp:ListItem>
                        <asp:ListItem Value="MINOR_CD">MINOR</asp:ListItem>
                        <asp:ListItem Value="ITM_CD">SKU</asp:ListItem>
                        <asp:ListItem Value="STORE_CD">STORE CODE</asp:ListItem>
                        <asp:ListItem Value="SORT_CD">SORT CODE</asp:ListItem>
                        <asp:ListItem Value="ORD_TP_CD">ORDER TYPE CODE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    =
                </td>
                <td style="width: 394px">
                    <asp:TextBox ID="txt_value" runat="server" CssClass="style5" Width="112px"></asp:TextBox>
                    <asp:Label ID="lbl_warnings" runat="server" ForeColor="Red" Width="268px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    then display:
                </td>
                <td colspan="4">
                    <asp:TextBox ID="txt_trigger_text" runat="server" Width="567px" Height="69px" TextMode="MultiLine"
                        MaxLength="2000"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Sequence:
                </td>
                <td>
                    <asp:DropDownList ID="cbo_sequence" runat="server" CssClass="style5" Enabled="true">
                        <asp:ListItem Selected="True">1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btn_Submit" runat="server" Text="Submit Rule" CssClass="style5" /><br />
        <br />
        <span style="text-decoration: underline">Current Rules:<br />
            <br />
        </span>
        <asp:DataGrid ID="MyDataGrid" runat="server" Width="677px" BorderColor="Black" CellPadding="3"
            AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanged="MyDataGrid_Page"
            OnSortCommand="MyDataGrid_Sort" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Right"
            DataKeyField="trigger_id" Height="16px" PageSize="7" AlternatingItemStyle-BackColor="Beige">
            <Columns>
                <asp:BoundColumn HeaderText="ID" SortExpression="trigger asc" DataField="trigger_id"
                    Visible="false" />
                <asp:BoundColumn HeaderText="Field" SortExpression="trigger_field asc" DataField="trigger_field" ItemStyle-VerticalAlign="Middle" />
                <asp:BoundColumn SortExpression="trigger_eq asc" DataField="trigger_eq" Visible="false" />
                <asp:TemplateColumn HeaderText="" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate>
                        <asp:Label ID="lbl_equals" runat="server" Text="="></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" VerticalAlign="Middle" />
                </asp:TemplateColumn>
                <asp:BoundColumn HeaderText="Value" SortExpression="trigger_value asc" DataField="trigger_value" ItemStyle-VerticalAlign="Middle" />
                <asp:TemplateColumn HeaderText="Sequence" SortExpression="trigger_eq asc" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate>
                        <asp:DropDownList ID="cbo_grid_sequence" runat="server" CssClass="style5" Enabled="true">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                            <asp:ListItem>24</asp:ListItem>
                            <asp:ListItem>25</asp:ListItem>
                            <asp:ListItem>26</asp:ListItem>
                            <asp:ListItem>27</asp:ListItem>
                            <asp:ListItem>28</asp:ListItem>
                            <asp:ListItem>29</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" VerticalAlign="Middle" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Response" SortExpression="trigger_response asc">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_response" MaxLength="2000" TextMode="MultiLine" Width="450px"
                            Height="100px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trigger_response") %>'></asp:TextBox>
                    </ItemTemplate>
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" VerticalAlign="Top" />
                </asp:TemplateColumn>
                <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;">
                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:ButtonColumn>
                <asp:ButtonColumn CommandName="Save" Text="&lt;img src='images/icons/Save.png' border='0'&gt;">
                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" />
                </asp:ButtonColumn>
            </Columns>
            <PagerStyle HorizontalAlign="Right" Mode="NumericPages" />
            <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                Font-Strikeout="False" Font-Underline="False" />
            <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                Font-Underline="False" ForeColor="Black" />
        </asp:DataGrid>
    </div>
</asp:Content>
