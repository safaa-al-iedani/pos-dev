<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login_Page" culture="auto" uiCulture="auto"%>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>LOGIN TEMPLATE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <style type="text/css">
        .style1 {
            width: 251px;
        }
    </style>
    <script type="text/javascript">
        window.name = "ActiveTab"
    </script>
</head>
<body>
    <form id="frmLogin" runat="server">
        <div align="center">
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="517px">
                <PanelCollection>
                    <dx:PanelContent runat="server">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="100%" align="left" valign="middle">
                                    <div style="padding-left: 18px; padding-top: 10px" id="main_body" runat="server">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="middle" width="54%" style="text-align: center">
                                                     <img src="<% = ConfigurationManager.AppSettings("company_logo").ToString %>" />
                                                   <%--Daniela french <img runat="server" src="<%$ Resources:LibResources, Label751 %>" /> --%>
                                                    <br />
                                                    <br />
                                                    <br />
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;
                                                </td>
                                                <td align="left" valign="middle">
                                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnLogin" Width="260px">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label638 %>">
                                                                                </dx:ASPxLabel>                                                                               
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxTextBox ID="txtLoginUser" runat="server" Width="130px" MaxLength="30" AutoPostBack="True">
                                                                                </dx:ASPxTextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label383 %>">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxTextBox ID="txt_password" runat="server" Width="130px" Password="True">
                                                                                </dx:ASPxTextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Company:" Visible="False">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxComboBox ID="cboxCompanies" AutoPostBack="True" runat="server"
                                                                                    IncrementalFilteringMode ="StartsWith" ValueType="System.String" Width="200px" Visible="False">
                                                                                </dx:ASPxComboBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Company Password:" Visible="False">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxTextBox ID="txt_co_password" runat="server" Password="True" Visible="False"
                                                                                    Width="130px">
                                                                                </dx:ASPxTextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br />
                                                                </td>
                                                                <td>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="right">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                 <%--Daniela french--%>
                                                                                <dx:ASPxButton ID="btnLogin" runat="server" Text="<%$ Resources:LibResources, Label547%>     " Font-Size="X-Small"
                                                                                    Width="116px">
                                                                                    <Image Height="25px" Url="~/images/login_icon.gif" Width="25px">
                                                                                    </Image>
                                                                                </dx:ASPxButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="middle" width="491px" style="height: 3px; padding-left:10px">&nbsp;
                                <dx:ASPxLabel ID="lbl_version" runat="server" Text="" Width="491px">
                                </dx:ASPxLabel>
                                </td>
                            </tr>
                            
<%--                             <tr>
                                <td align="left" valign="middle" width="491px" style="height: 3px">&nbsp;
                               
                               <span style="font-size:12px; "> <asp:Literal ID="lit_mmhfLink" runat="server"></asp:Literal> </span>
                            
                                </td>
                            </tr>--%>

                            <tr>
                                <td align="left" valign="middle" width="491px" style="height: 3px; ">&nbsp;
                                <dx:ASPxLabel ID="lblInfo" runat="server" Text="" Width="400px">
                                </dx:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="middle" width="491px" style="height: 3px; ">
                                    <asp:ImageButton ID="btnChangePwd" runat="server" Height="25px" ImageUrl="~/images/password.png"
                                        Width="25px" AlternateText="Change Password" ToolTip="Change Password" visible="false"/>
                                </td>
                            </tr>
                        </table>
                    </dx:PanelContent>
                </PanelCollection>
                <HeaderTemplate>
                    <div style="padding-left: 13px; padding-top: 2px">
                        <strong>
                             <%--Daniela french--%>
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label662%>">
                            </dx:ASPxLabel>
                        </strong>
                    </div>
                </HeaderTemplate>
            </dx:ASPxRoundPanel>
        </div>
        <dx:ASPxPopupControl ID="PopupChangePwd" runat="server" CloseAction="CloseButton"
            HeaderText="Change Password" Modal="True" Width="322px" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter">
            <HeaderStyle Font-Size="X-Small" />
            <ContentCollection>
                <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="UserName:">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txt_change_username" runat="server" Width="130px" MaxLength="30">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Current Password:">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txt_change_curr_password" runat="server" Width="130px" Password="True">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="New Password:">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txt_change_new_password" runat="server" Width="130px" Password="True">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Confirm New Password:">
                                </dx:ASPxLabel>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txt_change_new_password2" runat="server" Width="130px" Password="True">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <dx:ASPxLabel ID="lbl_change_label" runat="server" ForeColor="Red" Width="100%">
                                </dx:ASPxLabel>
                                <br />
                                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Change Password" Font-Size="X-Small"
                                    Width="177px" Wrap="False">
                                    <Image Height="25px" Url="~/images/password.png" Width="25px">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
    </form>
</body>
</html>
