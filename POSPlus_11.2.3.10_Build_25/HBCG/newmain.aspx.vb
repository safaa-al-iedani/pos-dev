Imports System.Data.OracleClient
Imports HBCG_Utils
Imports System.ComponentModel
Imports System.Globalization


Partial Class newmain
    Inherits POSBasePage

    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()  ' Daniela

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
        Me.Theme = ConfigurationManager.AppSettings("app_theme").ToString
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim str_co_grp_cd As String
        Dim str_co_cd As String
        Dim str_sup_user_flag As String
        Dim str_flag As String

        Dim Term_IP As String = ""

        'Oct 29 prevent OutOfMemory
        Session("ORD_TBL") = Nothing

        If isEmpty(Session("str_sup_user_flag")) Or isEmpty(Session("CO_CD")) Then
            If isNotEmpty(Session("EMP_CD")) Then
                str_flag = LeonsBiz.lucy_check_sup_user(UCase(Session("EMP_CD")), str_co_grp_cd, str_co_cd, str_sup_user_flag) 'lucy use the function even not check super user
            Else
                ''mm - Sep 20, 2016 - backbutton security concern
                ClearCache()
                Response.Redirect("Login.aspx")
            End If
        End If
        'Franchise change
        If isEmpty(Session("str_co_grp_cd")) And isNotEmpty(str_co_grp_cd) Then
            Session("str_co_grp_cd") = str_co_grp_cd
        End If
        ' Daniela Nov 7, 2014 Hide if not super user
        ' Default to "N" is no super user found so that Reports are hidden
        If isNotEmpty(str_sup_user_flag) Then
            Session("str_sup_user_flag") = str_sup_user_flag
        Else
            str_sup_user_flag = "N"
        End If

        ' Nov 07 2017 add user type to session
        If isNotEmpty(str_co_grp_cd) And isEmpty(Session("USER_TYPE")) Then
            Dim userType As Double
            userType = LeonsBiz.getUserType(Session("EMP_INIT"), str_co_grp_cd, str_co_cd, str_sup_user_flag, Session("HOME_STORE_CD"))
            If isNotEmpty(userType) And (Not userType = -1) Then
                Session("USER_TYPE") = userType
            Else
                Exit Sub
            End If

        End If
        If str_sup_user_flag = "Y" Then
            ASPxHyperLink23.Visible = True
            ASPxHyperLink24.Visible = True
        End If
        If isEmpty(str_sup_user_flag) Or str_sup_user_flag = "N" Then
            ASPxButton3.Text = ""
            ASPxButton4.Text = ""
            ASPxButton3.PostBackUrl = ""
            ASPxButton4.PostBackUrl = ""
            ASPxHyperLink23.Text = ""
            ASPxHyperLink24.Text = ""
            ASPxHyperLink23.Visible = False
            ASPxHyperLink24.Visible = False
        End If
        If isNotEmpty(str_co_cd) Then
            Session("CO_CD") = str_co_cd
        End If

        Dim str_yn As String = ""  'lucy
        lucy_check_commit(str_yn)  '15-oct-14, replace lucy_check_payment
        If str_yn = "NO" Then
            Exit Sub
        Else
            Session("cash_carry") = Nothing  'Daniela fix error in Order Stage 
            'Session("cash_carry") = "" 'Lucy
            If (Not IsPostBack) Then
                'If the current user has write access then perform the below operation
                'If the current has has read-only permission then no need to perform below activity            
                If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
                    ViewState("RefUrl") = Convert.ToString(Request.UrlReferrer)
                    Dim previousPageURL As String = Convert.ToString(ViewState("RefUrl"))
                    If Not String.IsNullOrWhiteSpace(previousPageURL) Then
                        ' For stale data issue
                        ' Need to perform the action when user navigates back to Home page from SOM+, SORW+ page            
                        Dim pageName As String = String.Empty
                        pageName = System.IO.Path.GetFileName(previousPageURL)

                        Dim accessInfo As New AccessControlBiz()
                        accessInfo.DeleteRecordsforPage(pageName)
                    End If
                End If
            End If

            ' Daniela Nov 19
            If Not Session("clientip") Is Nothing Then
                Term_IP = Session("clientip")
            Else
                Term_IP = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
            End If
            ' end Daniela

            'Session("PKGSPLITDATA") = Nothing
            If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
                lbl_header.Text = "* TRAIN MODE *"
                lbl_header.ForeColor = Color.Red
            End If

            If ConfigurationManager.AppSettings("pos") = "Y" Then
                Sales_Top.Visible = True
                Sales_Top_Right.Visible = True
                Shared_Functions.Visible = True
                CRM_Top.Visible = False
                Payment.Visible = False
            Else
                HyperLink1.Visible = False
                ASPxImage1.Visible = False
                hyperlink2.Visible = False
                ASPxHyperLink2.Visible = False
                HyperLink3.Visible = False
                ASPxImage2.Visible = False
                HyperLink4.Visible = False
                Sales_Top.Visible = False
                Sales_Top_Right.Visible = False
                Shared_Functions.Visible = False
                CRM_Top.Visible = False
                Payment.Visible = False
            End If

            Dim Version_String As String = "RedPrairie MM POS Version " & Get_Version("/HandleCode/HighlandsPOS", "Version")
            lbl_version.Text = Version_String

            If ConfigurationManager.AppSettings("crm") = "Y" Then
                CRM_Top.Visible = True
                HyperLink9.Visible = True
                ASPxImage5.Visible = True
                ASPxHyperLink1.Visible = True
                HyperLink7.Visible = True
                ASPxImage4.Visible = True
                HyperLink8.Visible = True
                HyperLink5.Visible = True
                ASPxImage3.Visible = True
                HyperLink6.Visible = True
            Else
                CRM_Top.Visible = False
                HyperLink9.Visible = False
                ASPxImage5.Visible = False
                ASPxHyperLink1.Visible = False
                HyperLink7.Visible = False
                ASPxImage4.Visible = False
                HyperLink8.Visible = False
                HyperLink5.Visible = False
                ASPxImage3.Visible = False
                HyperLink6.Visible = False
            End If

            gift_card.Visible = (ConfigurationManager.AppSettings("gift_card") = "Y")
            Payment.Visible = (ConfigurationManager.AppSettings("payment") = "Y")
            ' Daniela comment
            'Kiosk_Summary.Visible = (ConfigurationManager.AppSettings("kiosk") = "Y" AndAlso
            '                         Find_Security("SYSPM", Session("EMP_CD")) = "N")

            If Not IsPostBack Then

                If Session("EMP_CD") & "" = "" Then
                    Response.Redirect("login.aspx")
                End If

                If Request("payment_hold") & "" <> "" Then
                    ASPxPopupControl4.ShowOnPageLoad = True
                End If

                'has to be placed here, otherwise, the TEMP_ITM has been cleared already
                'upon CANCEL/SAVE order data, the pending reservations must be removed
                If Not IsNothing(Request("save_order_info")) Then
                    UnreserveInventory()
                End If

                Dim Saved_Data As Boolean = False
                If Request("verify_save") = "TRUE" Then
                    Saved_Data = Verify_Save()
                    'DB April 4 - revert price change
                    Dim conn As OracleConnection
                    conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    Try
                        Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand

                        conn.Open()
                        With cmdUpdateItems
                            .Connection = conn
                            .CommandText = "UPDATE temp_itm SET ret_prc = orig_prc WHERE ret_prc <> orig_prc and prc_chg_app_cd is null and session_id='" & Session.SessionID.ToString & "'"
                        End With
                        cmdUpdateItems.ExecuteNonQuery()
                    Catch ex As Exception
                        conn.Close()
                    End Try
                End If

                If Request("save_order_info") = "TRUE" Then
                    Call HBCG_Utils.Store_Order_Data()
                    Saved_Data = True
                    Response.Redirect("newmain.aspx")
                End If

                If Find_Security("CCRC", Session("emp_cd")) = "N" Then
                    hpl_cash_carry.NavigateUrl = ""
                    hpl_cash_carry.ToolTip = "Option Not Available"
                    hpl_cash_carry_express.NavigateUrl = ""
                    hpl_cash_carry_express.ToolTip = "Option Not Available"
                End If

                If ConfigurationManager.AppSettings("inv_avail").ToString = "CUSTOM" Then
                    hpl_inventory.NavigateUrl = "Custom_Availability.aspx"
                End If

                If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink1").ToString) Then
                    ASPxButton6.Text = ConfigurationManager.AppSettings("link1_desc").ToString
                    ASPxButton6.PostBackUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink1").ToString
                End If

                If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink2").ToString) Then
                    ASPxButton7.Text = ConfigurationManager.AppSettings("link2_desc").ToString
                    ASPxButton7.PostBackUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink2").ToString
                End If

                If Saved_Data <> True Then

                    Dim dbConn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                    Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
                    Dim dbReader As OracleDataReader
                    Dim dbSql As String

                    If Session("EMP_FNAME") & "" = "" OrElse Session("EMP_LNAME") & "" = "" Then
                        Try
                            dbConn.Open()
                            dbSql = "SELECT FNAME, LNAME, EMP_CD, HOME_STORE_CD FROM EMP WHERE EMP_CD='" & UCase(Session("EMP_CD")) & "'"
                            dbCmd.CommandText = dbSql
                            dbReader = DisposablesManager.BuildOracleDataReader(dbCmd)

                            If (dbReader.Read()) Then
                                Session("EMP_CD") = dbReader.Item("EMP_CD").ToString
                                Session("EMP_FNAME") = dbReader.Item("FNAME").ToString
                                Session("EMP_LNAME") = dbReader.Item("LNAME").ToString
                                Session("HOME_STORE_CD") = dbReader.Item("HOME_STORE_CD").ToString
                            End If

                            dbSql = "SELECT SHIP_TO_STORE_CD FROM STORE WHERE STORE_CD='" & UCase(Session("HOME_STORE_CD")) & "'"
                            dbCmd.CommandText = dbSql
                            dbReader = DisposablesManager.BuildOracleDataReader(dbCmd)

                            If (dbReader.Read()) Then
                                Session("pd_store_cd") = dbReader.Item("SHIP_TO_STORE_CD").ToString
                            End If

                            dbReader.Close()
                        Catch ex As Exception
                            dbConn.Close()
                            Throw
                        End Try
                    End If

                    If Session("EMP_FNAME") & "" <> "" AndAlso Session("EMP_LNAME") & "" <> "" Then
                        'Daniela french
                        lbl_Header1.Text = "Welcome " & Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
                        lbl_Header1.Text = Resources.LibResources.Label660 & " " & Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
                    End If
                    If Session("HOME_STORE_CD") & "" <> "" Then
                        'Daniela French
                        'lbl_Header2.Text = "Store Code: " & Session("HOME_STORE_CD") & vbCrLf & "Terminal: " & Term_IP
                        lbl_Header2.Text = Resources.LibResources.Label573 & ": " & Session("HOME_STORE_CD") & vbCrLf & "Terminal: " & Term_IP
                    End If

                    Try
                        If (ConnectionState.Closed = dbConn.State) Then dbConn.Open()
                        dbSql = "SELECT COUNT(SESSION_ID) As SALES FROM TEMP_MASTER WHERE EMP_CD='" & Session("EMP_CD") & "'"
                        dbCmd.CommandText = dbSql
                        dbReader = DisposablesManager.BuildOracleDataReader(dbCmd)

                        If (dbReader.Read()) Then
                            If IsNumeric(dbReader.Item("SALES").ToString) Then
                                'Daniela french
                                'hpl_saved_orders.Text = "Saved Orders (" & dbReader.Item("SALES").ToString & ")"
                                hpl_saved_orders.Text = Resources.LibResources.Label523 & " (" & dbReader.Item("SALES").ToString & ")"
                                hpl_saved_orders.NavigateUrl = "restore_saved_order.aspx"
                            End If
                        End If

                        dbReader.Close()
                        dbConn.Close()
                    Catch ex As Exception
                        dbConn.Close()
                        Throw
                    End Try

                    Dim emp_cd As String = Session("EMP_CD")
                    Dim empInit As String = Session("EMP_INIT")
                    Dim store_cd As String = Session("HOME_STORE_CD")
                    Dim co_cd As String = Session("CO_CD")
                    Dim fname As String = Session("EMP_FNAME")
                    Dim lname As String = Session("EMP_LNAME")
                    Dim IPAD As String = Session("IPAD")



                    If Not IsNothing(Session.SessionID.ToString.Trim) Then
                        theSalesBiz.DeleteTempOrder(Session.SessionID.ToString.Trim)
                    End If

                    ViewState("deleteBrowserID") = HttpContext.Current.Session("deleteBrowserID")

                    Dim client_IP As String = Session("clientip") ' Daniela
                    'Nov 2 save user flag
                    Dim supUser As String = Session("str_sup_user_flag")
                    'Feb 04 save co_grp_cd
                    Dim coGrpCd As String = Session("str_co_grp_cd")
                    Dim userType As String = Session("USER_TYPE")

                    Session.Clear()
                    Session("clientip") = client_IP  ' Daniela
                    Session("str_sup_user_flag") = supUser ' Daniela
                    Session("str_co_grp_cd") = coGrpCd ' Daniela
                    Session("USER_TYPE") = userType ' Daniela
                    Session("EMP_INIT") = empInit
                    Session("IPAD") = IPAD
                    Session("EMP_CD") = emp_cd
                    Session("CO_CD") = co_cd
                    Session("EMP_FNAME") = fname
                    Session("EMP_LNAME") = lname
                    Session("HOME_STORE_CD") = store_cd
                    Session("MP") = "DELIVERY AVAILABILITY"
                    Session("IP") = "INVENTORY LOOKUP"
                End If

                Populate_Relationships()

            End If
            If Terminal_Locked(Term_IP) = "C" And ConfigurationManager.AppSettings("term_lock_down").ToString = "Y" Then
                hpl_cash_carry.NavigateUrl = ""
                hpl_cash_carry.ToolTip = "Option Not Available"
                hpl_cash_carry_express.NavigateUrl = ""
                hpl_cash_carry_express.ToolTip = "Option Not Available"
                hpl_saved_orders.NavigateUrl = ""
                hpl_saved_orders.ToolTip = "Option Not Available"
                hpl_sls_order.NavigateUrl = ""
                hpl_sls_order.ToolTip = "Option Not Available"
            End If

            If ConfigurationManager.AppSettings("gift_card").ToString = "N" Then
                ASPxHyperLink4.NavigateUrl = ""
                ASPxHyperLink4.ToolTip = "Option Not Available"
                ASPxHyperLink12.NavigateUrl = ""
                ASPxHyperLink12.ToolTip = "Option Not Available"
            End If

            'Enable if there is even one ECA provider setup in ASP
            ' DB Jan 07 moved link to Customization
            'If (Not ECAProvidersExist()) Then
            '    hpl_credit_app.NavigateUrl = ""
            '    hpl_credit_app.ToolTip = "Option Not Available"
            '    hpl_otb.NavigateUrl = ""
            '    hpl_otb.ToolTip = "Option Not Available"
            'End If

            If Request("otb_app") = "TRUE" Then
                ASPxPopupControl13.ShowOnPageLoad = True
            End If
            If Find_Security("SBOB", Session("emp_cd")) = "N" Then
                hplSBOB.NavigateUrl = ""
                hplSBOB.ToolTip = "Option Not Available"
            End If
            If Find_Security("CSHA", Session("emp_cd")) = "N" Then
                hpl_csha.NavigateUrl = ""
                hpl_csha.ToolTip = "Option Not Available"
            End If

            If Find_Security("ISTM", Session("emp_cd")) = "N" Then
                ASPxHyperLink30.NavigateUrl = ""
                ASPxHyperLink30.ToolTip = "Option Not Available"
            End If

            If Find_Security("POMA", Session("emp_cd")) = "N" Then
                ASPxHyperLink31.NavigateUrl = ""
                ASPxHyperLink31.ToolTip = "Option Not Available"
            End If

            If Find_Security("AIQ", Session("emp_cd")) = "N" Then
                ASPxHyperLink32.NavigateUrl = ""
                ASPxHyperLink32.ToolTip = "Option Not Available"
            End If

            If Find_Security("SORW", Session("emp_cd")) = "N" Then
                ASPxHyperLink15.NavigateUrl = ""
                ASPxHyperLink15.ToolTip = "Option Not Available"
            End If

            If Find_Security("INAV", Session("emp_cd")) = "N" Then
                hpl_inventory.NavigateUrl = ""
                hpl_inventory.ToolTip = "Option Not Available"
            End If

            If Find_Security("SOM", Session("emp_cd")) = "N" Then
                ASPxHyperLink5.NavigateUrl = ""
                ASPxHyperLink5.ToolTip = "Option Not Available"
            End If

            If Find_Security("CUSM", Session("emp_cd")) = "N" Then
                ASPxHyperLink6.NavigateUrl = ""
                ASPxHyperLink6.ToolTip = "Option Not Available"
            End If

            If Find_Security("OCINQ", Session("emp_cd")) = "N" Then
                ASPxHyperLink7.NavigateUrl = ""
                ASPxHyperLink7.ToolTip = "Option Not Available"
            End If

            If Find_Security("IPRT", Session("emp_cd")) = "N" Then
                ASPxHyperLink8.NavigateUrl = ""
                ASPxHyperLink8.ToolTip = "Option Not Available"
            End If

            If Find_Security("POSEXC", Session("emp_cd")) = "N" Then
                ASPxHyperLink3.NavigateUrl = ""
                ASPxHyperLink3.ToolTip = "Option Not Available"
            End If

            If Find_Security("POA", Session("emp_cd")) = "N" Then
                ASPxHyperLink13.NavigateUrl = ""
                ASPxHyperLink13.ToolTip = "Option Not Available"
                ASPxHyperLink14.NavigateUrl = ""
                ASPxHyperLink14.ToolTip = "Option Not Available"
            End If

            'Enable 'create Sales order' links only if user has at least one security out of CRM, SAL, MDB/MCR       
            If Not HasOrderTypeSecurities() Then
                hpl_sls_order.NavigateUrl = ""
                hpl_sls_order.ToolTip = "Option Not Available"
                ASPxHyperLink2.NavigateUrl = ""
                ASPxHyperLink2.ToolTip = "Option Not Available"
            End If

            Dim PTAG_Access As String = AppUtils.Decrypt(Get_Connection_String("/HandleCode/HBCG/PCF_Config", "Ident_String"), "CrOcOdIlE")
            If PTAG_Access <> "HIGHLANDS" Then
                ASPxHyperLink29.NavigateUrl = ""
                ASPxHyperLink29.ToolTip = "Option Not Available"
            End If

            ' clear this out just to be sure
            Session("referrerToSom") = Nothing

            If Find_Security("CDB", Session("emp_cd")) = "N" Then
                ASPxHyperLink26.Enabled = False
            End If
            Session("preventMessageID") = Nothing

            'Peter 15Sep2014, Hijack link to point to Desjardin or TD site. 
            'Db Jan 07 moved link to cutomization
            'hpl_credit_app.NavigateUrl = GetCreditApp("XXX")
            'hpl_credit_app.ToolTip = "Credit Application site"

            'Daniela 01June2015, Hijack link to point to Easy Finance. 
            'Db Jan 07 moved link to cutomization
            'If Session("CO_CD") = "LFL" Then
            '    hpl_ez_finance.Visible = True
            '    hpl_ez_finance.NavigateUrl = GetEasyFinance(Session("EMP_CD"))
            '    hpl_ez_finance.ToolTip = "Easy Finance"
            '    img1.Visible = True
            'End If

            If (Not IsPostBack) Then
                ' For stale data issue
                ' Since sessions are being cleared above (refer Session.Clear() - Line 309) in this page,
                ' To restore the "deleteBrowserID" back to session the below code is required        
                If Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT")))) Then
                    HttpContext.Current.Session("deleteBrowserID") = ViewState("deleteBrowserID")
                End If
            End If
        End If ' lucy
    End Sub
    Public Sub lucy_check_commit(ByRef p_yn As String)
        ' lucy
        SalesUtils.lucy_check_payment(p_yn, Session.SessionID.ToString.Trim)

        If p_yn = "NO" Then   'authorization on file
            ' Daniela do not exit the screen if payment pending
            Dim previousPage As String = Page.Request.UrlReferrer.ToString
            If isEmpty(previousPage) Then
                previousPage = "x"
            End If

            'If InStr(PreviousPage.ToLower, "payment") > 0 Or InStr(PreviousPage, "Order_Stage") > 0 Then

            Session("lucy_from_logout") = "YES"
            Response.Redirect(previousPage)
            'End If

        End If

    End Sub
    ''' <summary>
    ''' Checks if the employee logged in has the proper securities necessary to allow creation of a
    ''' sales order. These securities include the SAL_CREATE, CRM_CREATE, MCR_CREATE and MDB_CREATE
    ''' securities. 
    ''' </summary>
    ''' <returns>A flag if <b>true</b>indicates that the user has at least one of the securities needed to
    '''          proceed with creating a sales order.
    ''' </returns>
    Private Function HasOrderTypeSecurities() As Boolean

        Dim proceed As Boolean = False

        'If user is setup to override all securities, then simply return and exit
        If SystemUtils.HasSecurityOverride(Session("emp_cd")) Then
            proceed = True
        Else
            'First find if the user has the sale create security
            proceed = (Find_Security("SAL_CREATE", Session("EMP_CD")) = "Y")

            'Means that the user did not have create sale security, check for return security
            If (Not proceed) Then
                proceed = (Find_Security("CRM_CREATE", Session("EMP_CD")) = "Y")
            End If

            'Means that the user did not have create sale or return security, check for MCR security
            If (Not proceed) Then
                proceed = (Find_Security("MCR_CREATE", Session("EMP_CD")) = "Y")
            End If

            'Means that the user did not have any other 3 securities, check for MDB security
            If (Not proceed) Then
                proceed = (Find_Security("MDB_CREATE", Session("EMP_CD")) = "Y")
            End If
        End If

        Return proceed

    End Function

    Private Function ECAProvidersExist() As Boolean
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmd As OracleCommand
        Dim rdr As OracleDataReader
        Dim rtnVal As Boolean = False

        'check if there are any active, electronic finance providers that also do ECAs
        Dim sql As String = "SELECT  count(AS_CD)  AS TOTAL_PROVIDERS FROM ASP" &
                                                " WHERE  ACTIVE = 'Y' " &
                                                " AND  AS_TP_CD = 'F' AND ELECTRONIC_INT= 'Y'  " &
                                                " AND ELECTRONIC_CR_APP = 'Y' "

        Try
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            rdr = DisposablesManager.BuildOracleDataReader(cmd)


            If (rdr.Read()) Then
                rtnVal = CInt(rdr.Item("TOTAL_PROVIDERS").ToString) > 0
            End If
            rdr.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Return rtnVal
    End Function


    Public Function Convert_Month(ByVal month As String)

        Select Case month
            Case "01"
                Convert_Month = "JAN"
            Case "02"
                Convert_Month = "FEB"
            Case "03"
                Convert_Month = "MAR"
            Case "04"
                Convert_Month = "APR"
            Case "05"
                Convert_Month = "MAY"
            Case "06"
                Convert_Month = "JUN"
            Case "07"
                Convert_Month = "JUL"
            Case "08"
                Convert_Month = "AUG"
            Case "09"
                Convert_Month = "SEP"
            Case "10"
                Convert_Month = "OCT"
            Case "11"
                Convert_Month = "NOV"
            Case "12"
                Convert_Month = "DEC"
            Case Else
                Convert_Month = "ERROR"
        End Select

    End Function

    Public Sub Populate_Relationships()

        Dim sql As String
        Dim HTML As String
        Dim Proceed As Boolean
        Dim conn2 As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim objsql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim x, y As Integer
        Dim z As Double

        Proceed = False
        x = 0
        y = 0
        z = 0

        HTML = ""

        conn2.Open()
        Dim default_invoice As String = ""
        If default_invoice & "" = "" Then
            sql = "select store_cd, quote_file, default_file from quote_admin where store_cd='" & Session("STORE_CD") & "'"

            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql2)

                Do While MyDataReader.Read
                    If MyDataReader.Item("quote_file").ToString & "" = "" Then
                        default_invoice = MyDataReader.Item("DEFAULT_FILE").ToString
                    Else
                        default_invoice = MyDataReader.Item("quote_file").ToString
                    End If
                Loop
                MyDataReader.Close()
            Catch ex As Exception
                conn2.Close()
                Throw
            End Try

            If String.IsNullOrEmpty(default_invoice) Then
                sql = "select store_cd, quote_file, default_file from quote_admin"

                objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                Try
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objsql2)

                    If MyDataReader.Read Then
                        default_invoice = MyDataReader.Item("DEFAULT_FILE").ToString
                    End If
                    MyDataReader.Close()
                Catch ex As Exception
                    conn2.Close()
                    Throw
                End Try
            End If
        End If
        ' Daniela date issue in French
        'sql = "SELECT REL_NO, NVL(CORP_NAME,LNAME) AS LNAME, HPHONE, PROSPECT_TOTAL, APPT_TIME, APPT_TYPE, EMAIL_ADDR, CONVERSION_DT FROM RELATIONSHIP WHERE SLSP1='" & UCase(Session("EMP_CD")) & "' AND FOLLOW_UP_DT=TO_DATE('" & FormatDateTime(Today.Date.ToString, 2) & "','mm/dd/RRRR') ORDER BY APPT_TIME"
        sql = "SELECT REL_NO, NVL(CORP_NAME,LNAME) AS LNAME, HPHONE, PROSPECT_TOTAL, APPT_TIME, APPT_TYPE, EMAIL_ADDR, CONVERSION_DT FROM RELATIONSHIP WHERE SLSP1='" & UCase(Session("EMP_CD")) & "' AND FOLLOW_UP_DT=TO_DATE(to_char(sysdate,'mm/dd/RRRR'),'mm/dd/RRRR') ORDER BY APPT_TIME"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If Not MyDataReader2.HasRows Then
                ' main_body.InnerHtml = "<br />&nbsp;&nbsp;No follow up transactions found for today."
                main_body.InnerHtml = "<br />&nbsp;&nbsp; " & Resources.LibResources.Label344
            Else
                Proceed = True
                HTML = "<table width=""100%"" cellpadding=""0"" cellspacing=""0"">"
                HTML = HTML & "<tr>"
                HTML = HTML & "<td width=2 colspan=2></td>"
                HTML = HTML & "<td><b><u>Customer</u></b></td>"
                HTML = HTML & "<td><b><u>Home Phone</u></b></td>"
                HTML = HTML & "<td><b><u>Total</u></b></td>"
                HTML = HTML & "<td><b><u>Time</u></b></td>"
                HTML = HTML & "<td><b><u>Type</u></b></td>"
                HTML = HTML & "<td width=3></td>"
                HTML = HTML & "</tr>"
            End If
            Do While MyDataReader2.Read()
                HTML = HTML & "<tr>"
                HTML = HTML & "<td width=2></td>"
                HTML = HTML & "<td><a href=Relationship_Maintenance.aspx?query_returned=Y&del_doc_num=" & MyDataReader2.Item("REL_NO").ToString & "><img src=""images/memo.png"" width=16 height=16 border=0 alt=""Relationship Maintenance""></a></td>"
                HTML = HTML & "<td>" & MyDataReader2.Item("LNAME").ToString & "</td>"
                HTML = HTML & "<td>" & MyDataReader2.Item("HPHONE").ToString & "</td>"
                If IsNumeric(MyDataReader2.Item("PROSPECT_TOTAL").ToString) Then
                    HTML = HTML & "<td>" & FormatCurrency(MyDataReader2.Item("PROSPECT_TOTAL").ToString, 2) & "</td>"
                    z = z + FormatNumber(MyDataReader2.Item("PROSPECT_TOTAL").ToString, 2)
                Else
                    HTML = HTML & "<td>" & FormatCurrency(0, 2) & "</td>"
                End If
                If MyDataReader2.Item("APPT_TIME").ToString & "" <> "" Then
                    HTML = HTML & "<td>" & String.Format("{0:t}", MyDataReader2.Item("APPT_TIME").ToString) & "</td>"
                Else
                    HTML = HTML & "<td>&nbsp;</td>"
                End If
                HTML = HTML & "<td>" & MyDataReader2.Item("APPT_TYPE").ToString & "</td>"
                x = x + 1
                If MyDataReader2.Item("APPT_TIME").ToString & "" <> "" Then
                    y = y + 1
                End If
                If Not String.IsNullOrEmpty(MyDataReader2.Item("EMAIL_ADDR").ToString) Then
                    HTML = HTML & "<td width=20 align=center><a href=""mailto:" & MyDataReader2.Item("EMAIL_ADDR").ToString & """><img src=""images/icons/envelope.png"" width=18 height=18 alt=""Send Email to: " & MyDataReader2.Item("EMAIL_ADDR").ToString & """ border=0></a></td>"
                Else
                    HTML = HTML & "<td align=center></td>"
                End If
                HTML = HTML & "<td width=20 align=center><a href=Invoices/" & default_invoice & "?DEL_DOC_NUM=" & MyDataReader2.Item("REL_NO").ToString & " target=_new><img src=""images/Office_PDF.png"" width=18 height=18 alt=""View Quote " & MyDataReader2.Item("REL_NO").ToString & """ border=0></a></td>"
                If IsDate(MyDataReader2.Item("CONVERSION_DT").ToString) Then
                    HTML = HTML & "<td width=20 align=center><img src=""images/icons/convert-small.gif"" width=18 height=18 alt=""Converted " & FormatDateTime(MyDataReader2.Item("CONVERSION_DT").ToString, DateFormat.ShortDate) & """></td>"
                Else
                    HTML = HTML & "<td align=center></td>"
                End If
                sql = "SELECT * FROM RELATIONSHIP_COMMENTS WHERE REL_NO='" & MyDataReader2.Item("REL_NO").ToString & "'"
                objsql = DisposablesManager.BuildOracleCommand(sql, conn2)
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                If MyDataReader.Read Then
                    HTML = HTML & "<td width=20 align=center><img src=""images/icons/comment_balloon.gif"" width=18 height=18 alt=""Comments""></td>"
                Else
                    HTML = HTML & "<td align=center></td>"
                End If
                MyDataReader.Close()

                HTML = HTML & "</tr>"
            Loop
            If Proceed = True Then
                HTML = HTML & "</table>"
                If x > 0 Then
                    'HTML = HTML & "<br />&nbsp;<b><u>Daily Summary:</u></b><br />"
                    'HTML = HTML & "&nbsp;Prospect(s): &nbsp; " & x & "<br />"
                    'HTML = HTML & "&nbsp;Appointment(s): &nbsp; " & y & "<br />"
                    'HTML = HTML & "&nbsp;Potential Revenue: &nbsp; " & FormatCurrency(z, 2) & "<br />"
                    HTML = HTML & "<br /><b><u>" & Resources.LibResources.Label780 & ":</u></b><br />"
                    HTML = HTML & "&nbsp;" & Resources.LibResources.Label781 & ": &nbsp; " & x & "<br />"
                    HTML = HTML & "&nbsp;" & Resources.LibResources.Label782 & ": &nbsp; " & y & "<br />"
                    HTML = HTML & "&nbsp;" & Resources.LibResources.Label783 & ": &nbsp; " & FormatCurrency(z, 2) & "<br />"
                End If
                main_body.InnerHtml = HTML
            End If
            'Close Connection 
            MyDataReader2.Close()
            conn2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Unreserves all lines in the current order.   This process gets called after the order 
    ''' has been saved, or if the order is being cancelled, to remove the ITM_RES records,
    ''' and release the hold on the inventory.
    ''' </summary>
    Private Sub UnreserveInventory()
        Dim soLns As DataTable = HBCG_Utils.GetTempItmInfoBySingleKey(Session.SessionID.ToString.Trim,
                                                                      temp_itm_key_tp.tempItmBySessionId,
                                                                      temp_itm_where_clause.qtyGreaterZero)
        If (Not IsNothing(soLns)) Then
            For Each tmpItemRow In soLns.Rows
                UnreserveInventory(tmpItemRow)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Checks if the line passed in has soft-reservations attached, and if so, removes them
    ''' Since the reservation is removed, the Store/Location are also cleared from the line
    ''' </summary>
    ''' <param name="tmpItemRow">a Datarow that contains the line to remove reservations when present</param>
    Private Sub UnreserveInventory(ByRef tmpItemRow As DataRow)

        Dim isReserved As Boolean = IIf(IsDBNull(tmpItemRow.Item("RES_ID")) OrElse
                                        (tmpItemRow.Item("RES_ID")).ToString.isEmpty, False, True)
        If (isReserved) Then
            Dim request As UnResRequestDtc = InventoryUtils.GetRequestForUnreserve(
                                                                    tmpItemRow.Item("RES_ID").ToString,
                                                                    tmpItemRow.Item("ITM_CD").ToString,
                                                                    tmpItemRow.Item("STORE_CD").ToString,
                                                                    tmpItemRow.Item("LOC_CD").ToString,
                                                                    tmpItemRow.Item("QTY").ToString)
            theInvBiz.RemoveSoftRes(request)
        End If
    End Sub

    Public Function Verify_Save()

        Dim conn2 As OracleConnection
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        If Session("cust_cd") & "" <> "" Or _
        Session("tax_rate") & "" <> "" Or _
        Session("tax_cd") & "" <> "" Or _
        Session("no_tax") & "" <> "" Or _
        Session("tet_cd") & "" <> "" Or _
        Session("del_chg") & "" <> "" Or _
        Not SessVar.discCd.isEmpty Or _
        Session("zip_cd") & "" <> "" Or _
        Session("zone_cd") & "" <> "" Or _
        Session("scomments") & "" <> "" Or _
        Session("dcomments") & "" <> "" Or _
        Session("arcomments") & "" <> "" Or _
        Session("del_dt") & "" <> "" Or _
        Session("setup_chg") & "" <> "" Then
            Verify_Save = True
        Else
            Verify_Save = False
        End If
        If Verify_Save <> True Then
            conn2 = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            'Open Connection 
            conn2.Open()

            sql = "SELECT * FROM TEMP_ITM WHERE SESSION_ID='" & Session.SessionID.ToString & "'"

            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If MyDataReader2.Read Then
                    If MyDataReader2.Item("ITM_CD").ToString & "" <> "" Then
                        Verify_Save = True
                    End If
                End If
                MyDataReader2.Close()
                conn2.Close()
            Catch
                conn2.Close()
                Throw
            End Try
        End If

        If Verify_Save <> True Then
            conn2 = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            'Open Connection 
            conn2.Open()

            sql = "SELECT * FROM PAYMENT WHERE SESSIONID='" & Session.SessionID.ToString & "'"

            'Set SQL OBJECT 
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                'Store Values in String Variables 
                If MyDataReader2.Read Then
                    If MyDataReader2.Item("AMT").ToString & "" <> "" Then
                        Verify_Save = True
                    End If
                End If
                MyDataReader2.Close()
                conn2.Close()
            Catch
                conn2.Close()
                Throw
            End Try
        End If

        If Verify_Save = True Then
            Dim sPreviousURL As String = Request.ServerVariables("HTTP_REFERER")
            If InStr(sPreviousURL, "LEAD=TRUE") > 0 Then
                ASPxPopupControl1.ContentUrl = "store_order_data.aspx?LEAD=TRUE"
                ASPxPopupControl1.ShowOnPageLoad = True
            Else
                ASPxPopupControl1.ContentUrl = "store_order_data.aspx"
                ASPxPopupControl1.ShowOnPageLoad = True
            End If
        End If

    End Function

    Public Function MonthLastDay(ByVal dCurrDate As Date)

        MonthLastDay = DateSerial(Year(dCurrDate), Month(dCurrDate), 1).AddMonths(1).AddDays(-1)

    End Function

    Public Function MonthFirstDay(ByVal dCurrDate As Date)

        MonthFirstDay = DateSerial(Year(dCurrDate), Month(dCurrDate), 1).AddMonths(-2)

    End Function

    'Peter 15 Sep 2014, Hijack the link to point to Desjardin site
    Public Function GetCreditApp(ByVal empCd As String) As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmd As OracleCommand
        Dim rdr As OracleDataReader
        Dim rtnVal As String = Nothing

        'check if there are any active, electronic finance providers that also do ECAs
        Dim sql As String = "SELECT std_multi_co.getCreditAppURL('" & Session("EMP_CD") & "') AS l_URL FROM DUAL"

        Try
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            rdr = DisposablesManager.BuildOracleDataReader(cmd)


            If (rdr.Read()) Then
                rtnVal = rdr.Item("l_URL").ToString
            End If
            rdr.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Return rtnVal
    End Function

    'Peter 15 Sep 2014, Hijack the link to point to Desjardin site
    Public Function GetEasyFinance(ByVal empCd As String) As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmd As OracleCommand
        Dim rdr As OracleDataReader
        Dim rtnVal As String = Nothing

        'check if there are any active, electronic finance providers that also do ECAs
        Dim sql As String = "SELECT std_multi_co.getEzFinanceURL('" & empCd & "') AS l_URL FROM DUAL"

        Try
            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            rdr = DisposablesManager.BuildOracleDataReader(cmd)


            If (rdr.Read()) Then
                rtnVal = rdr.Item("l_URL").ToString
            End If
            rdr.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Return rtnVal
    End Function

    Protected Sub localeLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles localeLink.Click

        Dim cult As String = UICulture

        If UICulture.StartsWith("French") Then
            localeLink.Text = "English"
        Else : localeLink.Text = "French"
        End If
       
    End Sub

    Private Sub ChangeLanguage(ByVal lang As String)

        For Each c As Control In Me.Controls

            Dim resources As ComponentResourceManager = New ComponentResourceManager(GetType(newmain))

            resources.ApplyResources(c, c.ID, New CultureInfo(lang))

        Next c

    End Sub

    Protected Sub ClearCache()
        ''mm -sep 16,2016 - backbutton security concern
        Session.Abandon()
        Session.Clear()
        Session.RemoveAll()
        FormsAuthentication.SignOut()
        Response.ClearHeaders()
        Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
        Response.AddHeader("Pragma", "no-cache")

        Dim nextpage As String = "Logout.aspx"
        Response.Write("<script language=javascript>")

        Response.Write("{")
        Response.Write(" var Backlen=history.length;")

        Response.Write(" history.go(-Backlen);")
        Response.Write(" window.location.href='" + nextpage + "'; ")

        Response.Write("}")
        Response.Write("</script>")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Buffer = True
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1D)
        Response.Expires = -1000
        Response.CacheControl = "no-cache"

    End Sub

End Class
