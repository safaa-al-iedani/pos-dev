<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Picture_Upload.aspx.vb"
    Inherits="Picture_Upload" Title="Image Upload" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <style type="text/css">
        
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding-left: 0px; padding-top: 3px">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    &nbsp;
                </td>
                <td width="810px" align="left" valign="middle">
                    <table width="810px" border="1" cellspacing="0" cellpadding="0" bgcolor="White">
                        <tr>
                            <td height="56">
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top" width="60%">
                                            <div style="padding-left: 8px; padding-top: 8px;">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxImage ID="img_logo" runat="server">
                                                            </dx:ASPxImage>
                                                        </td>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel ID="lbl_header" runat="server" Text="<%$ Resources:LibResources, Label515 %>">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br />
                                        </td>
                                        <td align="right" valign="top" style="width: 222px">
                                            <div style="padding-right: 13px; padding-top: 8px">
                                                <dx:ASPxLabel ID="lbl_Header1" runat="server" Text="">
                                                </dx:ASPxLabel>
                                                <br />
                                                <dx:ASPxLabel ID="lbl_Header2" runat="server" Text="">
                                                </dx:ASPxLabel>
                                            </div>
                                        </td>
                                        <td align="right" valign="top">
                                            <div style="padding-right: 13px; padding-top: 8px">
                                                <dx:ASPxMenu ID="ASPxMenu2" runat="server">
                                                    <Items>
                                                        <dx:MenuItem Text="Logout" NavigateUrl="Logout.aspx">
                                                            <Image Url="~/images/login_icon.gif" Width="25px">
                                                            </Image>
                                                        </dx:MenuItem>
                                                    </Items>
                                                </dx:ASPxMenu>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="32" align="left">
                                <dx:ASPxMenu ID="ASPxMenu1" runat="server" Width="100%">
                                    <Items>
                                        <dx:MenuItem Text="Home" NavigateUrl="~/newmain.aspx">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Reporting" NavigateUrl="~/Reporting.aspx">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Utilities" NavigateUrl="~/Utilities.aspx">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </dx:MenuItem>
                                    </Items>
                                </dx:ASPxMenu>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <table width="810px" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="22" align="left" valign="top">
                                            <div style="padding-left: 13px; padding-top: 4px">
                                                <dx:ASPxLabel ID="lblTabID" runat="server" Text="" Font-Bold="True">
                                                </dx:ASPxLabel>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:HyperLink ID="hpl_exit" runat="server" Visible="False"><< Back</asp:HyperLink></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <div style="padding-left: 5px; padding-top: 4px">
                                                <table width="694" border="0" cellpadding="0" cellspacing="0" background="images/repus_8.jpg">
                                                    <tr>
                                                        <td height="22" align="left" valign="top" background="images/kettle_7.jpg">
                                                            <div style="padding-left: 13px; padding-top: 4px">
                                                                <asp:Label ID="Label1" runat="server" Text="" CssClass="style5"></asp:Label>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="style5" Visible="False"><< Back</asp:HyperLink></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" class="style5" background="Images/bottom_5.jpg" style="background-repeat: no-repeat;
                                                            background-position: bottom left">
                                                            <div style="padding-left: 5px; padding-top: 4px">
                                                                <div id="frmsubmit" runat="server">
                                                                    Choose Your File To Upload
                                                                    <br>
                                                                    <input id="MyFile" type="file" runat="Server" style="width: 475px" class="style5">
                                                                    <br />
                                                                    <br />
                                                                    <input id="file_submit" type="submit" value="Upload" onserverclick="Upload_Click"
                                                                        runat="Server" class="style5">
                                                                    <br />
                                                                    <br />
                                                                    <div id="UploadDetails" visible="False" runat="Server">
                                                                        File Name: <span id="FileName" runat="Server" />
                                                                        <br>
                                                                        File Content: <span id="FileContent" runat="Server" />
                                                                        <br>
                                                                        File Size: <span id="FileSize" runat="Server" />bytes
                                                                        <br>
                                                                    </div>
                                                                    <br />
                                                                    When uploading pictures, you must name the image the same as the SKU number you
                                                                    are uploading. For example, if you upload a picture for a FRD65343 SOFA with a SKU
                                                                    of 345560008, you would need to name your image 345560008.jpg or 345560008.gif.
                                                                    <br />
                                                                    <br />
                                                                    <span id="Span1" style="color: red" runat="Server" /><span id="Span2" style="color: red"
                                                                        runat="Server" />
                                                                    <br />
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="20" align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="46" align="center" valign="top">
                                <div style="padding-left: 0px; padding-top: 18px">
                                    <dx:ASPxHyperLink ID="ASPxHyperLink22" NavigateUrl="newmain.aspx" runat="server"
                                        Text="Home">
                                    </dx:ASPxHyperLink>
                                    <img src="images/spacer.gif" width="12" height="1">:<img src="images/spacer.gif"
                                        width="12" height="1"><dx:ASPxHyperLink ID="ASPxHyperLink23" NavigateUrl="reporting.aspx"
                                            runat="server" Text="Reporting">
                                        </dx:ASPxHyperLink>
                                    <img src="images/spacer.gif" width="12" height="1">:<img src="images/spacer.gif"
                                        width="12" height="1"><dx:ASPxHyperLink ID="ASPxHyperLink24" NavigateUrl="utilities.aspx"
                                            runat="server" Text="Utilities">
                                        </dx:ASPxHyperLink>
                                    <img src="images/spacer.gif" width="12" height="1">:<img src="images/spacer.gif"
                                        width="12" height="1"><dx:ASPxHyperLink ID="ASPxHyperLink25" NavigateUrl="merch_images\POS_Plus_UserGuide.pdf"
                                            runat="server" Text="User Guide" Target="_new">
                                        </dx:ASPxHyperLink>
                                    <img src="images/spacer.gif" width="12" height="1">:<img src="images/spacer.gif"
                                        width="12" height="1"><dx:ASPxHyperLink ID="ASPxHyperLink27" NavigateUrl="logout.aspx"
                                            runat="server" Text="Logout">
                                        </dx:ASPxHyperLink>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="center" valign="middle">
                                <dx:ASPxHyperLink ID="ASPxHyperLink28" NavigateUrl="http://www.redprairie.com" runat="server"
                                    Text="Copyright
                                    &copy; 2011-2013. All Rights Reserved">
                                </dx:ASPxHyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td width="694" align="left" valign="middle">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>