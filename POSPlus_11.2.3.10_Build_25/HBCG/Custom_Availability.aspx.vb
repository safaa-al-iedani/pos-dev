Imports System.Data
Imports System.Data.OracleClient
Imports System.Data.SqlClient
Imports System.IO

Partial Class Custom_Availability
    Inherits POSBasePage

    Public Function FileExists(ByVal FileFullPath As String) _
          As Boolean

        Dim f As New IO.FileInfo(FileFullPath)
        Return f.Exists

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        Dim sql As String = ""
        Dim HTML As String = ""
        Dim SKU As String = ""
        Dim picture As String = ""
        Dim INV_AVAIL, ROW_ID As String
        SKU = Request("SKU")

        INV_AVAIL = Request("INV_SELECTION")
        ROW_ID = Request("ROW_ID")

        conn2 = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn2.Open()

        If Request("STORE_CD") & "" <> "" And Request("LOC_CD") & "" <> "" And Request("Process") = "TRUE" Then
            If Request("referrer") <> "SOM" Then
                sql = "UPDATE TEMP_ITM SET STORE_CD='" & Request("STORE_CD") & "', LOC_CD='" & Request("LOC_CD") & "' WHERE ROW_ID=" & ROW_ID
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                objSql2.ExecuteNonQuery()
                objSql2.Dispose()
            End If
            Response.Write("<script language='javascript'>" & vbCrLf)
            Response.Write("var parentWindow = window.parent; ")
            Response.Write("parentWindow.SelectAndClosePopup3();")
            If Request("referrer") = "SOM" Then
                Response.Write("parentWindow.location.href='SalesOrderMaintenance.aspx?DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned") & "&store_cd=" & Request("STORE_CD") & "&LOC_CD=" & Request("LOC_CD") & "&ROW_ID=" & Request("ROW_ID") & "';")
            Else
                Response.Write("parentWindow.location.href='order_detail.aspx';")
            End If
            Response.Write("</script>")
            Response.End()
        End If
        conn2.Close()

        If Len(SKU) = 10 Then SKU = Right(SKU, 9)
        If String.IsNullOrEmpty(SKU) Then
            HTML = "<div align=center>"
            HTML = HTML & "<form name=frmsubmit METHOD=POST action=""Custom_availability.aspx"">"
            HTML = HTML & "<table width=""30%"">"
            HTML = HTML & "<tr>"
            HTML = HTML & "<td><br /><br /><input type=text name=SKU class=""style5"">&nbsp;&nbsp;<input type=submit value=""View Availability"" class=""style5""></td>"
            HTML = HTML & "</tr>"
            HTML = HTML & "</table>"
            HTML = HTML & "</form>"
            HTML = HTML & "</div>"
        Else
            Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Inventory Availability"

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim ret_prc As Double = 0

            picture = ""
            If FileExists(Request.ServerVariables("APPL_PHYSICAL_PATH") & "images\furniture\" & SKU & ".gif") Then
                picture = SKU & ".gif"
            ElseIf FileExists(Request.ServerVariables("APPL_PHYSICAL_PATH") + "images\furniture\" & SKU & ".jpg") Then
                picture = SKU & ".jpg"
            End If

            sql = "SELECT ITM.ITM_CD, ITM.VE_CD, ITM.RET_PRC, ITM.ADV_PRC, ITM.VSN, ITM.DES, ITM.FINISH, ITM.ITM_TP_CD "
            sql = sql & "FROM ITM WHERE ITM.ITM_CD='" & SKU & "'"
            sql = UCase(sql)

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                If MyDataReader.Read Then
                    If IsNumeric(MyDataReader.Item("RET_PRC").ToString) Then
                        ret_prc = CDbl(MyDataReader.Item("RET_PRC").ToString)
                    End If
                    If IsNumeric(MyDataReader.Item("ADV_PRC").ToString) Then
                        If CDbl(MyDataReader.Item("ADV_PRC").ToString) < ret_prc Then
                            ret_prc = CDbl(MyDataReader.Item("ADV_PRC").ToString)
                        End If
                    End If
                    HTML = "<div align=center>"
                    HTML = HTML & "<table width=""350"" cellpadding=0 cellspacing=0 style=""font:10pt Tahoma"" border=0>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td>&nbsp;</td>"
                    HTML = HTML & "<td colspan=2 align=""left"">" & MyDataReader.Item("VSN").ToString & "</td>"
                    HTML = HTML & "<td rowspan=2 align=center valign=middle>"
                    If picture & "" <> "" Then
                        HTML = HTML & "<a href=""picture_display.aspx?IMG=" & picture & """><img src=""images/pictures.gif"" border=""0""></a>"
                    End If
                    HTML = HTML & "</td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td>&nbsp;</td>"
                    HTML = HTML & "<td colspan=2 align=""left"">" & MyDataReader.Item("DES").ToString & "</td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4>&nbsp;</td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td>&nbsp;</td>"
                    HTML = HTML & "<td valign=top align=""left""><b>SKU:</b></td>"
                    HTML = HTML & "<td valign=top align=""left"">" & MyDataReader.Item("ITM_CD").ToString & "</td>"
                    HTML = HTML & "<td rowspan=""3"" valign=""middle"" align=""center""><font size=""5"">" & FormatCurrency(CDbl(ret_prc), 2) & "</font></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td>&nbsp;</td>"
                    HTML = HTML & "<td align=""left""><b>Vendor:</b></td>"
                    HTML = HTML & "<td colspan=1 align=""left"">" & MyDataReader.Item("VE_CD").ToString & "</td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td>&nbsp;</td>"
                    HTML = HTML & "<td align=""left""><b>Dimensions:</b></td>"
                    HTML = HTML & "<td colspan=1 align=""left"">" & MyDataReader.Item("FINISH").ToString & "</td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Net Available</b></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Warehouse</b></td>"
                    HTML = HTML & "</tr>"

                    Dim sqlconn As New SqlConnection
                    Dim sqlobj As New SqlCommand
                    Dim sqlReader As SqlDataReader
                    Dim prmSKU As New SqlParameter

                    Try
                        sqlconn.ConnectionString = "Data Source=06LEVINDW;Initial Catalog=LevinTransactionalData;Persist Security Info=True;User ID=highlandspos;Password=highlands"
                        sqlconn.Open()

                        'Check Net Availability for the Warehouse
                        sqlobj.Connection = sqlconn
                        sqlobj.CommandText = "ProductAvailibilityNet"
                        sqlobj.CommandType = CommandType.StoredProcedure
                        sqlobj.CommandTimeout = 120
                        prmSKU = New SqlParameter("@ItemCode", SqlDbType.VarChar, 50)
                        prmSKU.Direction = ParameterDirection.Input
                        prmSKU.Value = SKU
                        sqlobj.Parameters.Add(prmSKU)

                        sqlReader = sqlobj.ExecuteReader

                        Do While sqlReader.Read
                            HTML = HTML & "<tr>"
                            If IsNumeric(sqlReader.Item("NetAvailable").ToString) Then
                                HTML = HTML & "<td colspan=4 align=""center"">" & CDbl(sqlReader.Item("NetAvailable").ToString) & "</td>"
                            Else
                                HTML = HTML & "<td colspan=4 align=""center"">0</td>"
                            End If
                            HTML = HTML & "</tr>"
                        Loop
                        sqlReader.Close()
                        sqlobj.Parameters.Clear()
                    Catch
                        main_body.InnerHtml = "We are having trouble communicating with Red Prairie.  Please <a href=""Custom_Avilability.aspx?" & Request.QueryString.ToString & """>try again</a>."
                        conn.Close()
                        sqlconn.Close()
                        Throw
                        Exit Sub
                    End Try

                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4>&nbsp;</td>"
                    HTML = HTML & "</tr>"


                    '
                    ' Show Store Availability
                    '

                    sql = "SELECT ITM_FIFL.STORE_CD, ITM_FIFL.LOC_CD, ITM_FIFL.QTY "
                    sql = sql & "FROM ITM_FIFL, LOC"
                    sql = sql & " WHERE ITM_FIFL.ITM_CD='" & SKU & "' "
                    sql = sql & "AND LOC.LOC_CD=ITM_FIFL.LOC_CD AND LOC.LOC_TP_CD='S' "
                    sql = sql & "AND LOC.STORE_CD=ITM_FIFL.STORE_CD "
                    sql = sql & "ORDER BY ITM_FIFL.STORE_CD, ITM_FIFL.LOC_CD"

                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4>&nbsp;</td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Showroom Inventory</b></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=2 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Store</b></td>"
                    HTML = HTML & "<td colspan=1 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Location</b></td>"
                    HTML = HTML & "<td colspan=1 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Qty</b></td>"
                    HTML = HTML & "</tr>"

                    Dim y As Integer = 0
                    Dim x As Integer = 0
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    Try
                        MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql)

                        If Not MyDataReader2.HasRows Then
                            HTML = HTML & "<tr>"
                            HTML = HTML & "<td colspan=4>&nbsp;</td>"
                            HTML = HTML & "</tr>"
                            HTML = HTML & "<tr>"
                            HTML = HTML & "<td>&nbsp;</td>"
                            HTML = HTML & "<td colspan=3>No Store Inventory Found</td>"
                            HTML = HTML & "</tr>"
                            HTML = HTML & "<tr>"
                            HTML = HTML & "<td colspan=4>&nbsp;</td>"
                            HTML = HTML & "</tr>"
                        End If
                        Do While MyDataReader2.Read
                            If ConfigurationManager.AppSettings("avail_details") = "Y" Then
                                HTML = HTML & "<tr>"
                                HTML = HTML & "<td>&nbsp;</td>"
                                HTML = HTML & "<td>"
                                If Request("LEAD") <> "TRUE" And Request("referrer") & "" <> "" And ROW_ID <> "NEW" And Session("IP") & "" = "" Then
                                    HTML = HTML & "<a href=Custom_Availability.aspx?process=TRUE&store_cd=" & MyDataReader2.Item("STORE_CD").ToString & "&LOC_CD=" & MyDataReader2.Item("LOC_CD").ToString & "&DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned") & "&ROW_ID=" & Request("ROW_ID") & "&referrer=" & Request("referrer") & "&SKU=" & Request("SKU") & " class=""style5"">"
                                    HTML = HTML & MyDataReader2.Item("STORE_CD").ToString & "</a></td>"
                                Else
                                    HTML = HTML & MyDataReader2.Item("STORE_CD").ToString & "</td>"
                                End If
                                HTML = HTML & "<td>" & MyDataReader2.Item("LOC_CD").ToString & "</td>"
                                HTML = HTML & "<td>" & MyDataReader2.Item("QTY").ToString & "</td>"
                                HTML = HTML & "</tr>"
                            End If
                            x = x + CDbl(MyDataReader2.Item("QTY").ToString)
                        Loop
                        MyDataReader2.Close()
                    Catch ex As Exception
                        conn.Close()
                        sqlconn.Close()
                        Throw
                    End Try

                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4>&nbsp;</td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Scheduled Incoming PO's</b></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=2 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>PO# Avail</b></td>"
                    HTML = HTML & "<td colspan=1 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Incoming Date</b></td>"
                    HTML = HTML & "<td colspan=1 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Net</b></td>"
                    HTML = HTML & "</tr>"

                    'Check Net Availability for the Incoming PO's
                    sqlobj.Connection = sqlconn
                    sqlobj.CommandText = "ProductAvailibilityRP"
                    sqlobj.CommandType = CommandType.StoredProcedure
                    sqlobj.CommandTimeout = 120
                    prmSKU.Direction = ParameterDirection.Input
                    prmSKU.Value = SKU
                    sqlobj.Parameters.Add(prmSKU)

                    sqlReader = sqlobj.ExecuteReader

                    Do While sqlReader.Read
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td colspan=2 align=""left"">" & sqlReader.Item("PreAdviceID").ToString & "</td>"
                        HTML = HTML & "<td colspan=1 align=""center"">" & sqlReader.Item("DueDate").ToString & "</td>"
                        If IsNumeric(sqlReader.Item("QtyDue").ToString) Then
                            HTML = HTML & "<td colspan=1 align=""right"">" & CDbl(sqlReader.Item("QtyDue").ToString) & "</td>"
                        Else
                            HTML = HTML & "<td colspan=1 align=""right"">0</td>"
                        End If
                        HTML = HTML & "</tr>"
                    Loop
                    If Right(HTML, 20) = "<b>Net</b></td></tr>" Then
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td colspan=4><br /><br /><br /></td>"
                        HTML = HTML & "</tr>"
                    End If
                    sqlReader.Close()
                    sqlobj.Parameters.Clear()

                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4>&nbsp;</td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Orders Assigned to Incoming PO's</b></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=2 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>SO#</b></td>"
                    HTML = HTML & "<td colspan=2 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Qty</b></td>"
                    HTML = HTML & "</tr>"

                    'Check Net Availability for the Incoming PO's attached to SO's
                    sqlobj.Connection = sqlconn
                    sqlobj.CommandText = "ProductAvailibilitySO"
                    sqlobj.CommandType = CommandType.StoredProcedure
                    sqlobj.CommandTimeout = 120
                    prmSKU.Direction = ParameterDirection.Input
                    prmSKU.Value = SKU
                    sqlobj.Parameters.Add(prmSKU)

                    sqlReader = sqlobj.ExecuteReader

                    Do While sqlReader.Read
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td colspan=2 align=""left"">" & sqlReader.Item("DelDocNum").ToString & "</td>"
                        If IsNumeric(sqlReader.Item("QtyDue").ToString) Then
                            HTML = HTML & "<td colspan=1 align=""right"">" & CDbl(sqlReader.Item("QtyDue").ToString) & "</td>"
                        Else
                            HTML = HTML & "<td colspan=1 align=""right"">0</td>"
                        End If
                        HTML = HTML & "</tr>"
                    Loop
                    If Right(HTML, 20) = "<b>Qty</b></td></tr>" Then
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td colspan=4><br /><br /><br /></td>"
                        HTML = HTML & "</tr>"
                    End If
                    sqlReader.Close()
                    sqlobj.Parameters.Clear()

                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4>&nbsp;</td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=4 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>PO's on Order</b></td>"
                    HTML = HTML & "</tr>"
                    HTML = HTML & "<tr>"
                    HTML = HTML & "<td colspan=2 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>PO#</b></td>"
                    HTML = HTML & "<td colspan=1 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Est. Arrival Time</b></td>"
                    HTML = HTML & "<td colspan=1 align=""center""  background=""images/kettle_7.jpg"" style=""background-repeat: repeat-x; background(-position) : Left""><b>Net</b></td>"
                    HTML = HTML & "</tr>"

                    'Check Net Availability for the Incoming PO's
                    sqlobj.Connection = sqlconn
                    sqlobj.CommandText = "ProductAvailibilityPO"
                    sqlobj.CommandType = CommandType.StoredProcedure
                    sqlobj.CommandTimeout = 120
                    prmSKU.Direction = ParameterDirection.Input
                    prmSKU.Value = SKU
                    sqlobj.Parameters.Add(prmSKU)

                    sqlReader = sqlobj.ExecuteReader

                    Do While sqlReader.Read
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td colspan=2 align=""left"">" & sqlReader.Item("PO_Code").ToString & "</td>"
                        HTML = HTML & "<td colspan=1 align=""center"">" & sqlReader.Item("ArrivalDate").ToString & "</td>"
                        If IsNumeric(sqlReader.Item("QtyDue").ToString) Then
                            HTML = HTML & "<td colspan=1 align=""right"">" & CDbl(sqlReader.Item("QtyDue").ToString) & "</td>"
                        Else
                            HTML = HTML & "<td colspan=1 align=""right"">0</td>"
                        End If
                        HTML = HTML & "</tr>"
                    Loop
                    If Right(HTML, 20) = "<b>Net</b></td></tr>" Then
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td colspan=4><br /><br /><br /></td>"
                        HTML = HTML & "</tr>"
                    End If
                    sqlReader.Close()
                    sqlobj.Parameters.Clear()
                    sqlconn.Close()

                    'Ignore package SKUS
                    If MyDataReader.Item("ITM_TP_CD") = "PKG" Then
                        HTML = Replace(HTML, "XXXXXDDDDDXXXXX", "INFORMATION UNAVAILABLE")
                        HTML = HTML & "<tr>"
                        HTML = HTML & "<td>&nbsp;</td>"
                        HTML = HTML & "<td colspan=3 align=center><br><br><b>YOU HAVE SELECTED A NON-INVENTORY SKU - ABORTING</b></td>"
                        HTML = HTML & "</tr>"
                        HTML = HTML & "</table>"
                        HTML = HTML & "<br />"
                        main_body.InnerHtml = HTML
                        Exit Sub
                    Else
                        HTML = HTML & "</table>"
                    End If
                Else
                    main_body.InnerHtml = "<br />SKU NOT FOUND.  PLEASE TRY AGAIN<br /><br />"
                    Exit Sub
                End If
                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            objSql.Dispose()
        End If

        conn2.Close()

        If picture & "" <> "" Then
            HTML = HTML & "<img src=images\furniture\" & picture & ">"
        End If
        HTML = HTML & "</div>"
        main_body.InnerHtml = HTML

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Request("INV_SELECTION") <> "Y" Then
            Me.MasterPageFile = "~/MasterPages/NoWizard2.Master"
        Else
            Me.MasterPageFile = "~/Blank.Master"
        End If

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

End Class
