<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Sales_Invoice.aspx.vb"
    Inherits="Sales_Invoice" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <img src="<% = ConfigurationManager.AppSettings("company_logo").ToString %>" />
        </div>
        <asp:Label ID="Label1" runat="server" Text="* QUOTE *" Style="position: absolute;
            left: 586px; top: 14px;" Font-Bold="True" Font-Names="Arial" Font-Size="24pt"
            Height="46px" Width="161px"></asp:Label>
        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 11px; position: absolute; top: 102px" Text="Customer Name:"
            Width="110px"></asp:Label>
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 62px; position: absolute; top: 120px" Text="Address:"
            Width="58px"></asp:Label>
        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 20px; position: absolute; top: 138px" Text="City, State, Zip:"
            Width="102px"></asp:Label>
        <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 30px; position: absolute; top: 156px" Text="Home Phone:"
            Width="94px"></asp:Label>
        <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 7px; position: absolute; top: 174px" Text="Bus., Day or Cell:"
            Width="117px"></asp:Label>
        <asp:Label ID="lbl_cust" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 133px; position: absolute; top: 103px" Width="351px"></asp:Label>
        <asp:Label ID="lbl_Comments" runat="server" Font-Bold="False" Font-Names="Arial"
            Font-Size="10pt" Height="117px" Style="left: 12px; position: absolute; top: 579px"
            Width="518px"></asp:Label>
        <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 524px; position: absolute; top: 73px" Text="Follow Up Date:"
            Width="109px"></asp:Label>
        <asp:Label ID="lbl_follow_up_dt" runat="server" Font-Bold="True" Font-Names="Arial"
            Font-Size="10pt" Height="12px" Style="left: 638px; position: absolute; top: 74px"
            Width="106px"></asp:Label>
        <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 542px; position: absolute; top: 92px" Text="Relationship:"
            Width="89px"></asp:Label>
        <asp:Label ID="lbl_REL_NO" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 638px; position: absolute; top: 93px" Width="106px"></asp:Label>
        <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 570px; position: absolute; top: 111px" Text="Account:"
            Width="64px"></asp:Label>
        <asp:Label ID="lbl_Account" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 638px; position: absolute; top: 112px" Width="106px"></asp:Label>
        <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 507px; position: absolute; top: 130px" Text="Design Consultant:"
            Width="122px"></asp:Label>
        <asp:Label ID="lbl_slsp1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 637px; position: absolute; top: 131px" Width="106px"></asp:Label>
        <asp:Label ID="lbl_Sales_Name" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 507px; position: absolute; top: 153px" Width="236px"></asp:Label>
        <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 569px; position: absolute; top: 579px" Text="Sub Total:"
            Width="66px"></asp:Label>
        <asp:Label ID="lbl_Subtotal" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 647px; position: absolute; top: 580px" Width="106px"></asp:Label>
        <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 578px; position: absolute; top: 598px" Text="Delivery:"
            Width="58px"></asp:Label>
        <asp:Label ID="lbl_Delivery" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 647px; position: absolute; top: 599px" Width="106px"></asp:Label>
        <asp:Label ID="Label234" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 567px; position: absolute; top: 617px" Text="Sales Tax:"
            Width="72px"></asp:Label>
        <asp:Label ID="lbl_tax" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 647px; position: absolute; top: 618px" Width="106px"></asp:Label>
        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 556px; position: absolute; top: 636px" Text="Quote Total:"
            Width="87px"></asp:Label>
        <asp:Label ID="lbl_grand_total" runat="server" Font-Bold="True" Font-Names="Arial"
            Font-Size="10pt" Height="12px" Style="left: 646px; position: absolute; top: 637px"
            Width="106px"></asp:Label>
        <asp:Label ID="Label13" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 556px; position: absolute; top: 656px" Text="Discount:"
            Width="87px"></asp:Label>
        <asp:Label ID="lbl_Discount" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 646px; position: absolute; top: 657px" Width="106px"></asp:Label>
        <asp:Label ID="lbl_addr" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 133px; position: absolute; top: 121px" Width="351px"></asp:Label>
        <asp:Label ID="lbl_city" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 133px; position: absolute; top: 139px" Width="351px"></asp:Label>
        <asp:Label ID="lbl_home" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 133px; position: absolute; top: 157px" Width="351px"></asp:Label>
        <asp:Label ID="lbl_bus" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
            Height="12px" Style="left: 133px; position: absolute; top: 175px" Width="351px"></asp:Label>
        &nbsp;&nbsp;
        <br />
        <asp:DataGrid ID="Gridview1" Style="position: relative; top: 100px; left: 2px;" runat="server"
             AutoGenerateColumns="False" CellPadding="0"
            Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Small"
            Font-Strikeout="False" Font-Underline="False" Height="125px" Width="728px" Font-Names="Arial" GridLines="None" PageSize="20">
            <HeaderStyle BackColor="Silver" ForeColor="White" Font-Italic="False" Font-Strikeout="False"
                Font-Underline="False" Font-Overline="False" Font-Bold="False" Font-Names="Arial"></HeaderStyle>
            <Columns>
                <asp:BoundColumn DataField="line" HeaderText="Ln #" ReadOnly="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" ForeColor="Black" Width="30px" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="itm_cd" HeaderText="SKU" ReadOnly="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" ForeColor="Black" Width="50px" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ve_cd" HeaderText="Vendor" ReadOnly="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" ForeColor="Black" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="vsn" HeaderText="VSN" ReadOnly="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" ForeColor="Black" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="des" HeaderText="Description" ReadOnly="True">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" ForeColor="Black" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="qty" HeaderText="Qty">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" ForeColor="Black" Width="30px" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ret_prc" HeaderText="Price" DataFormatString="{0:0.00}">
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" ForeColor="Black" Width="50px" />
                    <ItemStyle Width="48px" />
                </asp:BoundColumn>
                </Columns>
        </asp:DataGrid>
        <div align="center">
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
                Height="12px" Style="left: 11px; position: absolute; top: 701px" Text="**  Please Note:  Information contained on this quote is subject to change.  Please verify with your salesperson current pricing information."
                Width="730px"></asp:Label>
        </div>
    </form>
</body>
</html>
