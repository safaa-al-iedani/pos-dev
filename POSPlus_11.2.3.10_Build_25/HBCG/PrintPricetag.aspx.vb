﻿Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient

Partial Class PrintPricetag
    Inherits POSBasePage

    Public Const gs_version As String = "Version 1.0"

    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of Print Product Price tag. 
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        If IsPostBack = False Then

            Dim commandBtnPrint As GridViewCommandColumnCustomButton = New GridViewCommandColumnCustomButton()
            commandBtnPrint.ID = "print"
            commandBtnPrint.Text = Resources.CustomMessages.MSG0085
            commandBtnPrint.Visibility = GridViewCustomButtonVisibility.BrowsableRow

            Dim commandColPrint As GridViewCommandColumn = New GridViewCommandColumn(Resources.CustomMessages.MSG0085)
            commandColPrint.CustomButtons.Add(commandBtnPrint)
            GV_Pricetags.Columns.Add(commandColPrint)

            'Populate Store Code
            PopulateStoreCodes()

            lbl_Printer.Visible = False
            printer_drp.Visible = False

        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : To set title for this page.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        Dim _PanelHeader As DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel = Master.FindControl("arpMain")
        Dim _lblHeader As DevExpress.Web.ASPxEditors.ASPxLabel = _PanelHeader.FindControl("lbl_Round_Header")
        _lblHeader.Text = Resources.CustomMessages.MSG0083

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : Search button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub Search_Click(sender As Object, e As EventArgs) Handles btn_search.Click


        Dim objcmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim QString As StringBuilder

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        lbl_warning.Text = ""

        'If Validate_Fields() = False Then
        '    Return
        'End If

        QString = New StringBuilder("SELECT prrh.session_id As session_id, ")
        QString.Append("store_cd As store_code, ")
        QString.Append("prrh.output_file_smt As smt, ")
        QString.Append("prrh.output_file_rfl as rfl, ")
        QString.Append("prrh.output_file_trk as trk, ")
        QString.Append("prrh.output_file_hpt as hpt, ")
        QString.Append("TO_CHAR(prrh.created_dt,'DD-MON-YYYY') as created_date ")
        QString.Append("FROM ptag_rpt_run_hdr prrh ")

        If String.IsNullOrEmpty(dt_created_date.Text) = False Then
            QString.Append("WHERE prrh.store_cd = :p_store_code ")
            QString.Append("AND to_date(prrh.created_dt) = :p_created_date ")
            QString.Append("AND prrh.run_for = 'POS+' ")
            QString.Append("ORDER BY prrh.created_dt DESC ")

            objcmd = DisposablesManager.BuildOracleCommand(QString.ToString, conn)
            objcmd.CommandText = QString.ToString
            objcmd.CommandType = CommandType.Text

            objcmd.Parameters.Add(":p_store_code", OracleType.VarChar)
            objcmd.Parameters(":p_store_code").Value = cbo_store_cd.SelectedItem.Value

            objcmd.Parameters.Add(":p_created_date", OracleType.DateTime)
            objcmd.Parameters(":p_created_date").Value = dt_created_date.Value
        ElseIf String.IsNullOrEmpty(txt_session_id.Text) = False Then
            QString.Append("WHERE prrh.session_id = :p_session_id ")
            QString.Append("AND prrh.run_for = 'POS+' ")
            QString.Append("ORDER BY prrh.created_dt DESC ")

            objcmd = DisposablesManager.BuildOracleCommand(QString.ToString, conn)
            objcmd.CommandText = QString.ToString
            objcmd.CommandType = CommandType.Text

            objcmd.Parameters.Add(":p_session_id", OracleType.VarChar)
            objcmd.Parameters(":p_session_id").Value = txt_session_id.Text
        End If

        conn.Open()

        Dim objreader As OracleDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

        Dim dt As DataTable
        Dim dr As DataRow

        dt = New DataTable()

        dt.Columns.Add("SESSION_ID")
        dt.Columns.Add("STORE_CODE")
        dt.Columns.Add("CREATED_DATE")
        dt.Columns.Add("RPT_TYPE")

        While (objreader.Read())

            If Not IsDBNull(objreader("smt")) Then
                dr = dt.NewRow()
                dr("SESSION_ID") = objreader.Item("session_id").ToString
                dr("STORE_CODE") = objreader.Item("store_code").ToString
                dr("CREATED_DATE") = objreader.Item("created_date").ToString
                dr("RPT_TYPE") = "SMT"
                dt.Rows.Add(dr)
            End If

            If Not IsDBNull(objreader("rfl")) Then
                dr = dt.NewRow()
                dr("SESSION_ID") = objreader.Item("session_id").ToString
                dr("STORE_CODE") = objreader.Item("store_code").ToString
                dr("CREATED_DATE") = objreader.Item("created_date").ToString
                dr("RPT_TYPE") = "RFL"
                dt.Rows.Add(dr)
            End If

            If Not IsDBNull(objreader("trk")) Then
                dr = dt.NewRow()
                dr("SESSION_ID") = objreader.Item("session_id").ToString
                dr("STORE_CODE") = objreader.Item("store_code").ToString
                dr("CREATED_DATE") = objreader.Item("created_date").ToString
                dr("RPT_TYPE") = "TRK"
                dt.Rows.Add(dr)
            End If

            If Not IsDBNull(objreader("hpt")) Then
                dr = dt.NewRow()
                dr("SESSION_ID") = objreader.Item("session_id").ToString
                dr("STORE_CODE") = objreader.Item("store_code").ToString
                dr("CREATED_DATE") = objreader.Item("created_date").ToString
                dr("RPT_TYPE") = "HPT"
                dt.Rows.Add(dr)
            End If

            dr = dt.NewRow()
            dr("SESSION_ID") = objreader.Item("session_id").ToString
            dr("STORE_CODE") = objreader.Item("store_code").ToString
            dr("CREATED_DATE") = objreader.Item("created_date").ToString
            dr("RPT_TYPE") = "Summary"
            dt.Rows.Add(dr)

        End While

        If dt.Rows.Count > 0 Then
            GV_Pricetags.PageIndex = 0
            cbo_store_cd.Enabled = False
            dt_created_date.Enabled = False
            txt_session_id.Enabled = False
            printer_drp.Visible = True
            lbl_Printer.Visible = True
            sy_printer_drp_Populate()
            Session("GV_PRICETAG") = dt
        Else
            'No records found for given search criteria
            lbl_warning.Text = Resources.CustomMessages.MSG0040
            printer_drp.Visible = False
            lbl_Printer.Visible = False
            Session("GV_PRICETAG") = Nothing
        End If
        GV_Pricetags.DataBind()
    End Sub

    Protected Sub GV_PRICETAG_DataBinding(ByVal sender As Object, ByVal e As EventArgs)

        Dim dt As DataTable = Session("GV_PRICETAG")

        If IsNothing(dt) Then
            GV_Pricetags.DataSource = Nothing
            GV_Pricetags.Enabled = False
        Else
            GV_Pricetags.DataSource = dt
            GV_Pricetags.Enabled = True
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : To Validate mandatory tag fields.
    ''' </summary>
    ''' <remarks></remarks>

    Private Function Validate_Fields() As Boolean

        If String.IsNullOrEmpty(dt_created_date.Value) = True And String.IsNullOrEmpty(txt_session_id.Text) = True Then
            lbl_warning.Text = Resources.CustomMessages.MSG0086
            Return False
        End If
        Return True

    End Function

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : Clear button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click

        cbo_store_cd.SelectedIndex = Convert.ToInt16((Session("defaultStoreIndex")))
        txt_session_id.Text = ""
        dt_created_date.Text = ""
        lbl_warning.Text = ""

        cbo_store_cd.Focus()

        lbl_Printer.Visible = False
        printer_drp.Visible = False

        cbo_store_cd.Enabled = True
        dt_created_date.Enabled = True
        txt_session_id.Enabled = True

        Session("GV_PRICETAG") = Nothing
        GV_Pricetags.DataBind()

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : Created date changed event
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub Createddate_changed() Handles dt_created_date.DateChanged

        If String.IsNullOrEmpty(dt_created_date.Text) = False Then
            txt_session_id.Enabled = False
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : Session id changed event
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Protected Sub Sessionid_Enterted() Handles txt_session_id.TextChanged

        If String.IsNullOrEmpty(txt_session_id.Text) = False Then
            cbo_store_cd.Enabled = False
            dt_created_date.Enabled = False
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : Grid view call back event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub GV_Pricetags_CustomButtonCallback(sender As Object, e As ASPxGridViewCustomButtonCallbackEventArgs) Handles GV_Pricetags.CustomButtonCallback

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objCmd As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim emp_id As String = Session("EMP_CD")

        lbl_warning.Text = ""


        Dim GV_grid As ASPxGridView = CType(sender, ASPxGridView)
        Dim GV_Footer_label As DevExpress.Web.ASPxEditors.ASPxLabel = GV_grid.FindFooterRowTemplateControl("GV_Footer_label")

        GV_Footer_label.Text = ""
        If printer_drp.SelectedItem.Value = Resources.CustomMessages.MSG0087 Then
            GV_Footer_label.Text = Resources.CustomMessages.MSG0088
            Return
        End If

        If e.ButtonID.Equals("print") Then
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            objCmd.Connection = conn
            objCmd.CommandText = "OPS$DAEMON.custom_brick_ptag_ppkg.print_pricetag"

            objCmd.CommandType = CommandType.StoredProcedure
            objCmd.Parameters.Clear()
        End If

        If (GV_grid.GetRowValues(e.VisibleIndex, "RPT_TYPE") = "Summary") Then

            objCmd.Parameters.Add("p_prg_nm", OracleType.VarChar).Value = "SUMMARY"
            objCmd.Parameters.Add("p_spooler", OracleType.VarChar).Value = printer_drp.SelectedItem.ToString
            objCmd.Parameters.Add("p_store_cd", OracleType.VarChar).Value = GV_grid.GetRowValues(e.VisibleIndex, "STORE_CODE")
            objCmd.Parameters.Add("p_session_id", OracleType.VarChar).Value = GV_grid.GetRowValues(e.VisibleIndex, "SESSION_ID")
            objCmd.Parameters.Add("p_rpt_type", OracleType.VarChar).Value = "ALL"
            objCmd.Parameters.Add("p_emp_id", OracleType.VarChar).Value = Session("EMP_CD")

            conn.Open()
            objCmd.ExecuteNonQuery()
            conn.Close()

            GV_Footer_label.Text = Resources.CustomMessages.MSG0089

        Else

            objCmd.Parameters.Add("p_prg_nm", OracleType.VarChar).Value = "PRINTBTN"
            objCmd.Parameters.Add("p_spooler", OracleType.VarChar).Value = printer_drp.SelectedItem.ToString
            objCmd.Parameters.Add("p_store_cd", OracleType.VarChar).Value = GV_grid.GetRowValues(e.VisibleIndex, "STORE_CODE")
            objCmd.Parameters.Add("p_session_id", OracleType.VarChar).Value = GV_grid.GetRowValues(e.VisibleIndex, "SESSION_ID")
            objCmd.Parameters.Add("p_rpt_type", OracleType.VarChar).Value = GV_grid.GetRowValues(e.VisibleIndex, "RPT_TYPE")
            objCmd.Parameters.Add("p_emp_id", OracleType.VarChar).Value = Session("EMP_CD")

            conn.Open()
            objCmd.ExecuteNonQuery()
            conn.Close()

            GV_Footer_label.Text = Resources.CustomMessages.MSG0090

        End If

    End Sub


    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : To populate store code
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim SQL As String

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")

        If (Not (s_co_cd Is Nothing)) AndAlso s_co_cd.isNotEmpty Then

            SQL = "SELECT s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_store_desc "
            SQL = SQL & "FROM store s, store_grp$store sgs, store_grp sg "
            SQL = SQL & "WHERE s.store_cd = sgs.store_cd "
            SQL = SQL & "AND sgs.store_grp_cd = sg.store_grp_cd "
            SQL = SQL & "AND NVL(sg.pricing_store_grp,'N') = 'Y' "
            SQL = SQL & "AND co_cd = UPPER(:x_co_cd) "
            SQL = SQL & "ORDER BY store_cd"

            'Set SQL OBJECT 
            cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, connErp)
            cmdGetCodes.Parameters.Add(":x_co_cd", OracleType.VarChar)
            cmdGetCodes.Parameters(":x_co_cd").Value = s_co_cd

            Dim dataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)

            Try
                dataAdapter.Fill(ds)

            Catch
                Throw
                connErp.Close()
            End Try
            connErp.Close()

            If ds.Tables(0).Rows.Count = 0 Then
                lbl_warning.Text = Resources.CustomMessages.MSG0039
                Return
            End If

            Dim defaultStoreIndex As Integer = 0
            For Each row As DataRow In ds.Tables(0).Rows
                cbo_store_cd.Items.Add(row.ItemArray(2).ToString(), row.ItemArray(0).ToString(), row.ItemArray(1).ToString())
            Next

            cbo_store_cd.SelectedIndex = defaultStoreIndex
            Session("defaultStoreIndex") = defaultStoreIndex

        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : To populate printer list in combo box
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub sy_printer_drp_Populate()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet
        Dim printer_found As Boolean = False
        Dim v_user_init As String = sy_get_emp_init(Session("EMP_CD"))
        Dim v_user_cd As String = Session("EMP_CD")
        Dim v_default_printer As String = sy_get_default_printer_id(v_user_cd)

        If IsDBNull(v_default_printer) = True Or v_default_printer Is Nothing Or v_default_printer = "" Then
            v_default_printer = Resources.CustomMessages.MSG0087
        End If

        Session("default_prt") = v_default_printer

        ' Dec 27, 2016 - MARIAM -  Change SQL to include Invoice as well as Regular Report Printers
        'sql = "select pi.printer_nm PRINT_Q "
        'sql = sql & "from  printer_info pi "
        'sql = sql & "where pi.store_cd = (select home_store_cd "
        'sql = sql & "from emp "
        'sql = sql & "where emp_init = :v_user_init) "
        'sql = sql & "and   pi.printer_tp = 'INVOICE'"

        sql = "select c.print_q from print_queue c where SUBSTR(c.print_q,1,2)=(select home_store_cd from emp where emp_init = :v_user_init) "
        sql = sql & " union "
        sql = sql & " select pi.printer_nm PRINT_Q from  printer_info pi "
        sql = sql & " where pi.store_cd = (select home_store_cd from emp where emp_init =:v_user_init)"
        sql = sql & " and   pi.printer_tp = 'INVOICE' "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

        objSql.Parameters.Add(":v_user_init", OracleType.VarChar)
        objSql.Parameters(":v_user_init").Value = v_user_init

        oAdp.Fill(ds)

        printer_drp.Items.Clear()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                printer_found = True
                printer_drp.DataSource = ds
                printer_drp.ValueField = "PRINT_Q"
                printer_drp.TextField = "PRINT_Q"
                printer_drp.DataBind()
            End If

            printer_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(v_default_printer))
            printer_drp.Items.FindByText(v_default_printer).Selected = True
            printer_drp.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception

            MyDataReader.Close()
            conn.Close()
            Throw

        End Try

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : To get employee intials
    ''' </summary>
    ''' <remarks></remarks>

    Private Function sy_get_emp_init(ByVal p_user As String) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select emp_init as EMP_INIT from emp where emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader As OracleDataReader

        Try
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader.Read Then
                Return MyDatareader.Item("EMP_INIT").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Dec 2015
    ''' Description    : To get default printer
    ''' </summary>
    ''' <param name="p_user"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Private Function sy_get_default_printer_id(ByVal p_user As String) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select c.print_q as PRINT_Q from console c, emp e "
        sql = sql & "where e.print_grp_con_cd = c.con_cd And e.emp_cd = '" & p_user & "'"

        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader As OracleDataReader

        Try
            MyDatareader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDatareader.Read Then
                Return MyDatareader.Item("PRINT_Q").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

End Class