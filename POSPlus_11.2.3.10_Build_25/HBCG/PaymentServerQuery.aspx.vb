Imports HBCG_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports DevExpress.Web.ASPxEditors
Imports System.Data

Partial Class PaymentServerQuery
    Inherits POSBasePage

    Public Const gn_maxRecordsRetrieved As Integer = 200
    Public Const gs_version As String = "Version 4.2"
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0: Initial PSQ screen - basic query
    '
    '   1.0.1:
    '   (1) Label change requested by Dave L.
    '
    '   1.1:
    '   (1) Bug fix
    '
    '   2.0:
    '   (1)	New grid-style view of data results � user can toggle between one row detail to full list
    '   (2)	Visa, MC, and Amex transactions card# masked
    '   (3)	Queries modified to properly pull re-written invoices� transactions
    '   (4)	Queries modified to properly pull Brick Card Payments
    '   (5)	Bug-fixes
    '
    '   2.0.1:
    '   (1) Special �Transactions which have missing ARTRN records� button (the light bulb) � query creates dynamic SQL based on user entry.
    '       If a terminal ID was entered, the SQL�s UNION clause for terminals which have not  been set up (ie. SVS transactions) was referencing incorrectly.
    '
    '   2.0.2:
    '   (1) Query/detail results screen - Bug fix to regular query displaying transactions for stores which user should be unable to see.
    '   (2) Grid Results screen - modified to display the transaction message.
    '   (3) Grid Results screen - changed �Search� button to use an image icon instead to gain space.
    '
    '   2.0.3:
    '   (1) main query modified as transactions were not being retrieved as expected
    '       - Added one more union for refunds where it exact matches to ar_trn.adj_ivc_cd.
    '       - Removed match based on card number because masking doesn�t always give us what we need.
    '       - See query comments
    '   2.0.4:
    '   (1) "not in ARTRN" query modified as per Jacquie's changes - changed gift card check to TDFs, removed using deldoc for company code
    '
    '   2.0.5:
    '   (1) "Not in ARTRN" query modified as per Jacquie's changes - bug fix
    '   (2)	Main query modified to include GP_PINPAD_MERCHANT (query #3)
    '   (3) �Not in ARTRN� query modified to include GP_PINPAD_MERCHANT (query #2)
    '   (4) SO employee changed from EMP_CD_OP to EMP_CD_KEYER
    '
    '   2.0.6:
    '   (1) both queries require update to include company code check - bug fix
    '
    '   2.1:
    '   (1) Main query changed to retrieve PET records only. Then, upon retrieval, single AR_TRN records retrieved to populate ARTRN data.
    '       As users cycle through PET records, the associated ARTRN record retrieved. Due to difficulties of matching criteria, there is
    '       a chance that multiple records would be found. Special functionality has been created to match based on transaction type and
    '       has been hard-coded - see new function ABC.
    '
    '   3.0:    POS+ French conversion: 27-Aug-2015 - Kumaran 27-Aug-2015
    '            (1) Labels, button captions, tooltip and command messages are converted into French strings.
    '
    '
    '   4.0:    Reprint Logic Implementation: 22-Sep-2015 - Kumaran
    '            (1) Receipt Reprint logic implementation
    '   4.1:    Diplay a message after receipt has been sent to the printer.

    '   4.2:    POS+ - PSQ Changes: 25-Nov-2015 - Kumaran
    '            (1) To display additional transaction message in popup box
    '            (2) To display the plan number using provided logic
    '            (3) New format receipts should be printed for BRICK CARD and VISAD payments
    '
    '------------------------------------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           ResetResults()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Clears all fields.
    '   Called by:      btn_Search_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub ResetAllFields()

        txt_terminal_ipaddr.Text = ""
        txt_pc_name.Text = ""
        txt_company_code.Text = ""

        txt_PET_trans_type.Text = ""
        txt_PET_ps_invoice.Text = ""
        txt_PET_amount.Text = ""
        txt_PET_entry_type.Text = ""
        txt_PET_card_number.Text = ""
        txt_PET_expiry_date.Text = ""
        txt_PET_card_type.Text = ""
        txt_PET_plan_num.Text = ""
        txt_PET_auth_num.Text = ""
        txt_PET_dsp_trans_msg.Text = ""
        txt_PET_cust_cd.Text = ""
        txt_PET_date_time.Text = ""
        txt_PET_batch_num.Text = ""
        txt_PET_seq_num.Text = ""
        txt_PET_lpsf_indicator.Text = ""

        txt_ARTRN_trans_type.Text = ""
        txt_ARTRN_invoice.Text = ""
        txt_ARTRN_amount.Text = ""
        txt_ARTRN_adj_invoice.Text = ""
        txt_ARTRN_contract_code.Text = ""
        txt_ARTRN_cash_drawer.Text = ""
        txt_ARTRN_status_code.Text = ""
        txt_ARTRN_mop_cd.Text = ""
        txt_ARTRN_source.Text = ""
        txt_ARTRN_reference_des.Text = ""
        txt_ARTRN_post_date.Text = ""
        txt_ARTRN_employee_code.Text = ""
        txt_ARTRN_employee_name.Text = ""
        txt_SO_employee_code.Text = ""
        txt_SO_employee_name.Text = ""

        Session("ds_queryResults") = New DataSet
        Session("POSITION_INDEX") = 0
        Session("ROW_NUM") = ""

    End Sub

    Private Sub ResetARTRNDataFields()

        txt_ARTRN_trans_type.Text = ""
        txt_ARTRN_invoice.Text = ""
        txt_ARTRN_amount.Text = ""
        txt_ARTRN_adj_invoice.Text = ""
        txt_ARTRN_contract_code.Text = ""
        txt_ARTRN_cash_drawer.Text = ""
        txt_ARTRN_status_code.Text = ""
        txt_ARTRN_mop_cd.Text = ""
        txt_ARTRN_source.Text = ""
        txt_ARTRN_reference_des.Text = ""
        txt_ARTRN_post_date.Text = ""
        txt_ARTRN_receipt_no.Text = ""
        txt_ARTRN_employee_code.Text = ""
        txt_ARTRN_employee_name.Text = ""

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PopulateStoreCodes()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which populates the store code drop-down list.
    '   Called by:      Page_Load()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub PopulateStoreCodes()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim SQL As String

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")

        'for debugging
        If (Not (s_co_cd Is Nothing)) AndAlso s_co_cd.isNotEmpty Then

            SQL = "SELECT store_cd, store_cd as full_store_desc "
            SQL = SQL & "FROM store "
            SQL = SQL & "WHERE co_cd = UPPER(:x_co_cd) "
            SQL = SQL & "ORDER BY store_cd"

            'Set SQL OBJECT 
            cmdGetCodes = DisposablesManager.BuildOracleCommand(SQL, connErp)
            cmdGetCodes.Parameters.Add(":x_co_cd", OracleType.VarChar)
            cmdGetCodes.Parameters(":x_co_cd").Value = s_co_cd


            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            txt_store_cd.Items.Insert(0, GetLocalResourceObject("combo_select_store_code.Text").ToString())
            txt_store_cd.Items.FindByText(GetLocalResourceObject("combo_select_store_code.Text").ToString()).Value = ""
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            txt_store_cd.SelectedIndex = 0

            Try
                With cmdGetCodes
                    .Connection = connErp
                    .CommandText = SQL
                End With

                connErp.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
                oAdp.Fill(ds)

                With txt_store_cd
                    .DataSource = ds
                    .DataValueField = "store_cd"
                    .DataTextField = "full_store_desc"
                    .DataBind()
                End With

                connErp.Close()

            Catch ex As Exception
                connErp.Close()
                Throw
            End Try

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           SpecialQueryApprovedTransMissingARTRN()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Approved transactions which do not exist in OCINQ
    '   Called by:      
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub SpecialQueryApprovedTransMissingARTRN(p_terminalID As String, p_transDate As String, p_storeCode As String)

        Dim s_errorMessage As String = ""

        If (String.IsNullOrEmpty(Trim(p_transDate.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0021
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            txt_terminal_id.Text = Trim(txt_terminal_id.Text)
            dateEdit_transaction_date.Text = Trim(dateEdit_transaction_date.Text)
            txt_store_cd.Text = Trim(txt_store_cd.Text)
            txt_PET_trans_type.Text = Trim(txt_PET_trans_type.Text)
            txt_PET_ps_invoice.Text = Trim(txt_PET_ps_invoice.Text)
            txt_PET_amount.Text = Trim(txt_PET_amount.Text)
            txt_PET_card_number.Text = Trim(txt_PET_card_number.Text)
            txt_PET_card_type.Text = Trim(txt_PET_card_type.Text)
            txt_PET_auth_num.Text = Trim(txt_PET_auth_num.Text)
            txt_PET_cust_cd.Text = Trim(txt_PET_cust_cd.Text)
            Return

        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")

        If Not ((Not (s_co_cd Is Nothing)) AndAlso s_co_cd.isNotEmpty) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0022
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return

        End If

        '--------------------------------------------------------------------------
        '-- PET and ARTRN data retrieval
        '--------------------------------------------------------------------------
        '-- UNION of two queries:
        '-- 1. TERMINAL/PET/ARTRN - all transactions which are tied to a given terminal which has been set up (gp_pinpad_mapping)
        '-- 2. PET/AR_TRN - all transactions which are tied to a given terminal which has NOT been set up (for Giftcard until Leon's adds gp_pinpad_mapping)
        '----------------------------

        'AND clauses to add based on parameters passed in
        Dim s_clauseTerminalID = "AND gpm.pinpad_id LIKE UPPER(:x_terminalID) "
        Dim s_clauseGCTerminalID = "AND pet.trm LIKE UPPER(:x_terminalID) "
        Dim s_clauseTransDate = "AND pet.dent = UPPER(:x_transDate) "
        Dim s_clauseStoreCode = "AND gpm.store_cd LIKE UPPER(:x_storeCode) "
        Dim s_clauseGCStoreCode = "AND SUBSTR(pet.del_doc_num,6,2) LIKE UPPER(:x_storeCode) "

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Starts

        '-- 1. TERMINAL/PET/ARTRN - all transactions which are tied to a given terminal which has been set up (gp_pinpad_mapping)
        SQL = "SELECT gpm.pinpad_id as terminal_id, gpm.pc_nm as pc_name, gpm.pc_ipaddr as terminal_ipaddr, gpm.store_cd as store_cd, s.co_cd as company_code, "
        SQL = SQL & "pet.session_id as PET_session_id, pet.cust_cd as PET_cust_cd, pet.del_doc_num as PET_ps_invoice, pet.cre_dt as PET_date_time, pet.dent as PET_date, "
        SQL = SQL & "pet.response_cd as PET_response_code, pet.line_text as PET_line_text, pet.dsp as PET_dsp_trans_msg, pet.rcp as PET_rcp_trans_msg, "
        SQL = SQL & "pet.crn as PET_card_number, pet.crt as PET_card_type, pet.exp as PET_expiry_date, "

        SQL = SQL & "CASE "
        SQL = SQL & "WHEN INSTR(pet.crt,'VISA') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'AMERICAN EXPRESS') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'MASTER CARD') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR( pet.crt,'GIFT CARD') > 0 THEN LPAD ( SUBSTR(pet.crn, -4), LENGTH(pet.crn), 'X') "
        SQL = SQL & "ELSE pet.crn "
        SQL = SQL & "END AS PET_card_number_masked, "

        SQL = SQL & "DECODE(RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)), 'ICC', 'DIPPED',RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)) ) as PET_entry_type, "
        SQL = SQL & "pet.aut as PET_auth_num, pet.rcp as PET_rcp, "
        SQL = SQL & "pet.amt as PET_amount, pet.trm as PET_trm, pet.s_seq as PET_seq_num, pet.s_tr_tp as PET_trans_type, "
        SQL = SQL & "pet.s_batch as PET_batch_num, pet.s_swip as PET_lpsf_indicator, "
        SQL = SQL & "custom_brick_ppkg.get_pmtplan_id(pet.del_doc_num, "
        SQL = SQL & "pet.crt, "
        SQL = SQL & "pet.psd, "
        SQL = SQL & "pet.s_pln, "
        SQL = SQL & "pet.cust_cd, "
        SQL = SQL & "pet.amt, "
        SQL = SQL & "pet.dent, "
        SQL = SQL & "pet.s_term, "
        SQL = SQL & "pet.s_grce, "
        SQL = SQL & "pet.aut) as PET_plan_num, "
        SQL = SQL & "'' as ARTRN_trans_type, '' as ARTRN_invoice, '' ARTRN_amount, '' as ARTRN_adj_invoice, '' as ARTRN_post_date, '' as ARTRN_status_code, "
        SQL = SQL & "'' as ARTRN_contract_code, '' as ARTRN_cash_drawer, "
        SQL = SQL & "'' as ARTRN_employee_code, '' as ARTRN_emp_cd_cshr, "
        SQL = SQL & "'' as ARTRN_source, '' as ARTRN_reference_des, '' as ARTRN_mop_cd, "
        SQL = SQL & "'' as EMP_employee_name "
        SQL = SQL & ", rownum as ROW_NUM "
        SQL = SQL & "FROM pos_electronic_transaction pet, gp_pinpad_mapping gpm, store s "
        SQL = SQL & "WHERE gpm.store_cd = s.store_cd "
        SQL = SQL & "AND pet.trm = gpm.pinpad_id "
        SQL = SQL & "AND pet.response_cd = 'A' "

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Ends

        'add clauses to TERMINAL/PET/ARTRN part of query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(p_terminalID.ToString())))) Then
            SQL = SQL & s_clauseTerminalID
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            SQL = SQL & s_clauseTransDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            SQL = SQL & s_clauseStoreCode
        End If

        SQL = SQL & "AND s.co_cd = UPPER(:x_co_cd) "
        'JAM 11Feb2015: Jacquie requests change not exists as it is not grabbing data properly

        SQL = SQL & "AND NOT EXISTS ( "
        SQL = SQL & "SELECT 'x' FROM ar_trn a "
        SQL = SQL & "WHERE a.cust_cd = pet.cust_cd "

        SQL = SQL & "AND pet.dent = a.post_dt "
        SQL = SQL & "AND NVL(a.app_cd, 'XXXXX') = pet.aut ) "
        'JAM 10Mar2015: Jacquie requests to remove

        '--------------------------------------------------------------------------
        'UNION -- for Giftcard until Leon's adds gp_pinpad_mapping
        SQL = SQL & "UNION "

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Starts

        'JAM 17MAR2015 (v2.0.5): added GP_PINPAD_MERCHANT table to query - store_cd used now, added to where clause

        ''-- 2. PET/AR_TRN - all transactions which are tied to a given terminal which has NOT been set up (for Giftcard until Leon's adds gp_pinpad_mapping)
        SQL = SQL & "SELECT pet.trm as terminal_id, 'N/A' as pc_name, 'N/A' as terminal_ipaddr, gpm.store_cd as store_cd, '' as company_code, "
        SQL = SQL & "pet.session_id as PET_session_id, pet.cust_cd as PET_cust_cd, pet.del_doc_num as PET_ps_invoice, pet.cre_dt as PET_date_time, pet.dent as PET_date, "
        SQL = SQL & "pet.response_cd as PET_response_code, pet.line_text as PET_line_text, pet.dsp as PET_dsp_trans_msg, pet.rcp as PET_rcp_trans_msg, "
        SQL = SQL & "pet.crn as PET_card_number, pet.crt as PET_card_type, pet.exp as PET_expiry_date, "

        SQL = SQL & "CASE "
        SQL = SQL & "WHEN INSTR(pet.crt,'VISA') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'AMERICAN EXPRESS') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'MASTER CARD') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR( pet.crt,'GIFT CARD') > 0 THEN LPAD ( SUBSTR(pet.crn, -4), LENGTH(pet.crn), 'X') "
        SQL = SQL & "ELSE pet.crn "
        SQL = SQL & "END AS PET_card_number_masked, "

        SQL = SQL & "DECODE(RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)), 'ICC', 'DIPPED', RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)) ) as PET_entry_type, "
        SQL = SQL & "pet.aut as PET_auth_num, pet.rcp as PET_rcp, "
        SQL = SQL & "pet.amt as PET_amount, pet.trm as PET_trm, pet.s_seq as PET_seq_num, pet.s_tr_tp as PET_trans_type, "
        SQL = SQL & "pet.s_batch as PET_batch_num, pet.s_swip as PET_lpsf_indicator, "
        SQL = SQL & "custom_brick_ppkg.get_pmtplan_id(pet.del_doc_num, "
        SQL = SQL & "pet.crt, "
        SQL = SQL & "pet.psd, "
        SQL = SQL & "pet.s_pln, "
        SQL = SQL & "pet.cust_cd, "
        SQL = SQL & "pet.amt, "
        SQL = SQL & "pet.dent, "
        SQL = SQL & "pet.s_term, "
        SQL = SQL & "pet.s_grce, "
        SQL = SQL & "pet.aut) as PET_plan_num, "
        SQL = SQL & "'' as ARTRN_trans_type, '' as ARTRN_invoice, '' ARTRN_amount, '' as ARTRN_adj_invoice, '' as ARTRN_post_date, '' as ARTRN_status_code, "
        SQL = SQL & "'' as ARTRN_contract_code, '' as ARTRN_cash_drawer, "
        SQL = SQL & "'' as ARTRN_employee_code, '' as ARTRN_emp_cd_cshr, "
        SQL = SQL & "'' as ARTRN_source, '' as ARTRN_reference_des, '' as ARTRN_mop_cd, "
        SQL = SQL & "'' as EMP_employee_name "
        SQL = SQL & ", rownum as ROW_NUM "
        SQL = SQL & "FROM pos_electronic_transaction pet, gp_pinpad_merchant gpm, store s "
        SQL = SQL & "WHERE pet.response_cd = 'A' "
        'JAM 25Mar2015 (v2.0.6): company code required in query
        SQL = SQL & "AND gpm.store_cd = s.store_cd "
        'JAM 25Mar2015 (v2.0.6) (END)

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Ends

        'add clauses to PET/ARTRN part of query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(p_terminalID.ToString())))) Then
            SQL = SQL & s_clauseGCTerminalID
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            SQL = SQL & s_clauseTransDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            'JAM 17MAR2015 (v2.0.5): use GP_PINPAD_MERCHANT store code now - use the store code clause, no need to use the special GC one
            SQL = SQL & s_clauseStoreCode
            'JAM 17MAR2015 (v2.0.5) (END)
        End If

        'JAM 25Mar2015 (v2.0.6): company code required in query
        SQL = SQL & "AND s.co_cd = UPPER(:x_co_cd) "
        'JAM 25Mar2015 (v2.0.6) (END)

        'JAM 10Mar2015: Jacquie requests to change to TDF
        SQL = SQL & "AND pet.crt LIKE 'TDF%' "

        'JAM 17MAR2015 (v2.0.5): added GP_PINPAD_MERCHANT table to query - store_cd used now, added to where clause
        SQL = SQL & "AND SUBSTR(pet.trm,1,3) = SUBSTR(gpm.td_store_num,7,3) "
        'JAM 17MAR2015 (v2.0.5) (END)

        SQL = SQL & "AND NOT EXISTS ( "
        SQL = SQL & "SELECT 'x' FROM ar_trn a "
        SQL = SQL & "WHERE a.cust_cd = pet.cust_cd "
        SQL = SQL & "AND pet.dent = a.post_dt "
        SQL = SQL & "AND NVL(a.app_cd, 'XXXXX') = pet.aut) "
        SQL = SQL & "AND NOT EXISTS ( "
        SQL = SQL & "SELECT 'x' "
        SQL = SQL & "FROM gp_pinpad_mapping gp WHERE gp.pinpad_id = pet.trm) "

        SQL = SQL & "ORDER BY PET_date_time "
        'order by 9

        'Set SQL object
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        'bind variables based on parameters passed in
        objsql.Parameters.Add(":x_co_cd", OracleType.VarChar)
        objsql.Parameters(":x_co_cd").Value = s_co_cd

        If (Not (String.IsNullOrEmpty(Trim(p_terminalID.ToString())))) Then
            objsql.Parameters.Add(":x_terminalID", OracleType.VarChar)
            objsql.Parameters(":x_terminalID").Value = p_terminalID
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            objsql.Parameters.Add(":x_transDate", OracleType.DateTime)
            objsql.Parameters(":x_transDate").Value = p_transDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
            objsql.Parameters(":x_storeCode").Value = p_storeCode
        End If

        Dim ds As New DataSet
        Dim dv As DataView

        Try
            With objsql
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
            oAdp.Fill(ds)

            dv = New DataView(ds.Tables(0))

            Dim n_positionCounter As Integer = 1
            Dim n_queryResultsCount As Integer = dv.Count

            Session("ds_queryResults") = ds
            Session("POSITION_INDEX") = n_positionCounter
            Session("ROW_NUM") = ""

            If (n_queryResultsCount > 0) Then
                Session("ROW_NUM") = dv(0)("ROW_NUM").ToString
            End If

            'for debugging

            If (n_queryResultsCount > gn_maxRecordsRetrieved) Then

                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0020, n_queryResultsCount.ToString, gn_maxRecordsRetrieved.ToString)
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

                Session("ds_queryResults") = New DataSet
                Session("POSITION_INDEX") = 0
                Session("ROW_NUM") = ""
                Return
            End If

            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        'bind dataview data to fields on screen based on user's position in the data (in this case the first record)
        BindPETARTRNDataViewData()

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveTransaction()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Main procedure which retrieves the transaction information based on query parameters entered by the user.
    '   Called by:      "Lookup Prices" button - btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrieveTransaction(p_terminalID As String, p_transDate As String, p_storeCode As String, p_transType As String, p_invoice As String, p_amount As String, p_cardNumber As String, p_cardType As String, p_authNumber As String, p_custCode As String)

        Dim s_errorMessage As String = ""

        If (String.IsNullOrEmpty(Trim(p_terminalID.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_transDate.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_storeCode.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_transType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_invoice.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardNumber.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_authNumber.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_amount.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_custCode.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = String.Format(Resources.CustomMessages.MSG0023, vbCrLf)
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            txt_terminal_id.Text = Trim(txt_terminal_id.Text)
            dateEdit_transaction_date.Text = Trim(dateEdit_transaction_date.Text)
            txt_store_cd.Text = Trim(txt_store_cd.Text)
            txt_PET_trans_type.Text = Trim(txt_PET_trans_type.Text)
            txt_PET_ps_invoice.Text = Trim(txt_PET_ps_invoice.Text)
            txt_PET_amount.Text = Trim(txt_PET_amount.Text)
            txt_PET_card_number.Text = Trim(txt_PET_card_number.Text)
            txt_PET_card_type.Text = Trim(txt_PET_card_type.Text)
            txt_PET_auth_num.Text = Trim(txt_PET_auth_num.Text)
            txt_PET_cust_cd.Text = Trim(txt_PET_cust_cd.Text)
            Return

        End If

        'STORE CODE REQUIRES at least terminalID or transaction date or transaction type or invoice or card# or auth# or card type or amount
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString()))) AndAlso _
            (String.IsNullOrEmpty(Trim(p_terminalID.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_transDate.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_transType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_invoice.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardNumber.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_authNumber.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0024
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return
        End If

        'CARD TYPE REQUIRES at least terminalID or transaction date or transaction type or invoice or card# or auth# or store code or amount
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString()))) AndAlso _
            (String.IsNullOrEmpty(Trim(p_terminalID.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_transDate.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_transType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_invoice.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardNumber.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_authNumber.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_storeCode.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0025
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return
        End If

        'AMOUNT REQUIRES at least terminalID or transaction date or transaction type or invoice or card# or auth# or card type or store code
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString()))) AndAlso _
            (String.IsNullOrEmpty(Trim(p_terminalID.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_transDate.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_transType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_invoice.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardNumber.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_authNumber.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0026
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return

        End If

        'CARD NUMBER must be at least 3 digits long
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString()))) AndAlso p_cardNumber.Length < 4) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0027
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            
            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")

        'for debugging

        If Not ((Not (s_co_cd Is Nothing)) AndAlso s_co_cd.isNotEmpty) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0022
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return
        End If

        '--------------------------------------------------------------------------
        '-- PET and ARTRN data retrieval
        '--------------------------------------------------------------------------
        '-- UNION of four queries:
        '-- 1. TERMINAL/PET/ARTRN - all transactions which are tied to a given terminal which has been set up (gp_pinpad_mapping) which tries to match to ARTRN.ivc_cd
        '-- 2. same as above but matches to ARTRN.adj_ivc_cd (for refunds)
        '-- 3. PET/AR_TRN - all transactions which are tied to a given terminal which has NOT been set up (for Brick Card/TDF)
        '-- 4. PET/AR_TRN - all transactions which are tied to a brick card payment (the debit card portion)
        '----------------------------

        'AND clauses to add based on parameters passed in
        Dim s_clauseTerminalID = "AND gpm.pinpad_id = UPPER(:x_terminalID) "
        Dim s_clauseTransDate = "AND pet.dent = UPPER(:x_transDate) "
        Dim s_clauseStoreCode = "AND gpm.store_cd = UPPER(:x_storeCode) "
        Dim s_clauseTransType = "AND pet.s_tr_tp = UPPER(:x_transType) "
        Dim s_clauseInvoice = "AND pet.del_doc_num LIKE UPPER(:x_invoice) "
        Dim s_clauseAmount = "AND pet.amt = :x_amount "
        Dim s_clauseCardNumber = "AND pet.crn LIKE UPPER(:x_cardNumber) "
        Dim s_clauseCardType = "AND pet.crt LIKE UPPER(:x_cardType) "
        Dim s_clauseAuthNumber = "AND pet.aut = UPPER(:x_authNumber) "
        Dim s_clauseCustCode = "AND pet.cust_cd LIKE UPPER(:x_custCode) "

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Starts

        '---------------------------------------------------------------------------------
        '-- 1. TERMINAL/PET/ARTRN - all transactions which are tied to a given terminal which has been set up (gp_pinpad_mapping) which tries to match to ARTRN.ivc_cd
        SQL = "SELECT gpm.pinpad_id as terminal_id, gpm.pc_nm as pc_name, gpm.pc_ipaddr as terminal_ipaddr, gpm.store_cd as store_cd, s.co_cd as company_code, "
        SQL = SQL & "pet.session_id as PET_session_id, pet.cust_cd as PET_cust_cd, pet.del_doc_num as PET_ps_invoice, pet.cre_dt as PET_date_time, pet.dent as PET_date, "
        SQL = SQL & "pet.response_cd as PET_response_code, pet.line_text as PET_line_text, pet.dsp as PET_dsp_trans_msg, pet.rcp as PET_rcp_trans_msg, "
        SQL = SQL & "pet.crn as PET_card_number, pet.crt as PET_card_type, pet.exp as PET_expiry_date, "

        SQL = SQL & "CASE "
        SQL = SQL & "WHEN INSTR(pet.crt,'VISA') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'AMERICAN EXPRESS') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'MASTER CARD') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR( pet.crt,'GIFT CARD') > 0 THEN LPAD ( SUBSTR(pet.crn, -4), LENGTH(pet.crn), 'X') "
        SQL = SQL & "ELSE pet.crn "
        SQL = SQL & "END AS PET_card_number_masked, "

        SQL = SQL & "DECODE(RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)), 'ICC', 'DIPPED', RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)) ) as PET_entry_type, "
        SQL = SQL & "pet.aut as PET_auth_num, pet.rcp as PET_rcp, "
        SQL = SQL & "pet.amt as PET_amount, pet.trm as PET_trm, pet.s_seq as PET_seq_num, pet.s_tr_tp as PET_trans_type, "
        SQL = SQL & "pet.s_batch as PET_batch_num, pet.s_swip as PET_lpsf_indicator, "
        SQL = SQL & "custom_brick_ppkg.get_pmtplan_id(pet.del_doc_num, "
        SQL = SQL & "pet.crt, "
        SQL = SQL & "pet.psd, "
        SQL = SQL & "pet.s_pln, "
        SQL = SQL & "pet.cust_cd, "
        SQL = SQL & "pet.amt, "
        SQL = SQL & "pet.dent, "
        SQL = SQL & "pet.s_term, "
        SQL = SQL & "pet.s_grce, "
        SQL = SQL & "pet.aut) as PET_plan_num, "
        SQL = SQL & "a.trn_tp_cd as ARTRN_trans_type, a.ivc_cd as ARTRN_invoice, a.amt as ARTRN_amount, a.adj_ivc_cd as ARTRN_adj_invoice, TO_CHAR(a.post_dt, 'DD-MON-YYYY') as ARTRN_post_date, a.stat_cd as ARTRN_status_code, "
        SQL = SQL & "a.cntr_cd as ARTRN_contract_code, a.csh_dwr_cd as ARTRN_cash_drawer, "
        SQL = SQL & "a.emp_cd_op as ARTRN_employee_code, a.emp_cd_cshr as ARTRN_emp_cd_cshr, "
        SQL = SQL & "a.origin_cd as ARTRN_source, a.des as ARTRN_reference_des, a.mop_cd as ARTRN_mop_cd, "
        SQL = SQL & "'' as EMP_employee_name "
        SQL = SQL & ", rownum as ROW_NUM "
        SQL = SQL & "FROM pos_electronic_transaction pet, ar_trn a, gp_pinpad_mapping gpm, store s "
        SQL = SQL & "WHERE gpm.store_cd = s.store_cd "
        SQL = SQL & "AND pet.trm = gpm.pinpad_id "
        SQL = SQL & "AND pet.cust_cd = a.cust_cd(+) "
        SQL = SQL & "AND pet.aut = a.app_cd(+) "
        '--ADDED 12FEB2015
        SQL = SQL & "AND pet.dent = a.post_dt(+) "
        '--ADDED 12FEB2015 (END)

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Ends

        'JAM 11FEB2015: Jacquie requests to match on partial invoice due to rewrites, remove matching on amount as well
        'ORIG: SQL = SQL & "AND pet.del_doc_num = a.ivc_cd(+) "
        'ORIG: SQL = SQL & "AND pet.amt = a.amt(+) "
        SQL = SQL & "AND SUBSTR(pet.del_doc_num,1,11) = SUBSTR(a.ivc_cd(+),1,11) "
        'JAM 11FEB2015 (END)

        'JAM 03MAR2015: Jacquie requests to remove matching on card#
        'SQL = SQL & "AND SUBSTR(pet.crn, LENGTH(pet.crn)-3, 4) = SUBSTR(a.bnk_crd_num(+), LENGTH(a.bnk_crd_num(+))-3, 4) "
        'JAM 03MAR2015 (END)

        '--ADDED 12FEB2015
        SQL = SQL & "AND NOT EXISTS (SELECT 'x' "
        SQL = SQL & "FROM ar_trn a2 "
        SQL = SQL & "WHERE a2.cust_cd = pet.cust_cd "
        SQL = SQL & "AND pet.dent = a2.post_dt "
        SQL = SQL & "AND NVL(a2.app_cd, 'XXXXX') = pet.aut "

        '--ADDED 03MAR2015
        SQL = SQL & "AND pet.amt = a2.amt "
        SQL = SQL & "AND a2.ivc_cd = 'BRICK PAYMENT') "
        'ORIG: SQL = SQL & "AND DECODE(a2.ivc_cd,'BRICK PAYMENT', SUBSTR(pet.del_doc_num, 1, 11), SUBSTR(a2.ivc_cd, 1, 11)) = SUBSTR(pet.del_doc_num, 1, 11)) "
        '--ADDED 03MAR2015 (END)
        '--ADDED 12FEB2015 (END)

        '--ADDED 03MAR2015
        SQL = SQL & "AND NOT EXISTS (SELECT 'x' "
        SQL = SQL & "FROM ar_trn a2 "
        SQL = SQL & "WHERE a2.cust_cd = pet.cust_cd "
        SQL = SQL & "AND pet.dent = a2.post_dt "
        SQL = SQL & "AND a2.app_cd = pet.aut "
        SQL = SQL & "AND a2.adj_ivc_cd = pet.del_doc_num) "
        '--ADDED 03MAR2015 (END)

        SQL = SQL & "AND s.co_cd = UPPER(:x_co_cd) "

        'add clauses to TERMINAL/PET/ARTRN part of query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(p_terminalID.ToString())))) Then
            SQL = SQL & s_clauseTerminalID
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            SQL = SQL & s_clauseTransDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            SQL = SQL & s_clauseStoreCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transType.ToString())))) Then
            SQL = SQL & s_clauseTransType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            SQL = SQL & s_clauseInvoice
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            SQL = SQL & s_clauseAmount
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            SQL = SQL & s_clauseCardNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString())))) Then
            SQL = SQL & s_clauseCardType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
            SQL = SQL & s_clauseAuthNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            SQL = SQL & s_clauseCustCode
        End If

        '---------------------------------------------------------------------------------
        'UNION -- 2. same as #1 above but matches to ARTRN.adj_ivc_cd (for refunds)
        SQL = SQL & "UNION "

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Starts

        'JAM 03MAR2015: Jacquie requests to add new query for refunds

        '---------------------------------------------------------------------------------
        '-- 2. same as #1 above but matches to ARTRN.adj_ivc_cd (for REFUNDS)
        SQL = SQL & "SELECT gpm.pinpad_id as terminal_id, gpm.pc_nm as pc_name, gpm.pc_ipaddr as terminal_ipaddr, gpm.store_cd as store_cd, s.co_cd as company_code, "
        SQL = SQL & "pet.session_id as PET_session_id, pet.cust_cd as PET_cust_cd, pet.del_doc_num as PET_ps_invoice, pet.cre_dt as PET_date_time, pet.dent as PET_date, "
        SQL = SQL & "pet.response_cd as PET_response_code, pet.line_text as PET_line_text, pet.dsp as PET_dsp_trans_msg, pet.rcp as PET_rcp_trans_msg, "
        SQL = SQL & "pet.crn as PET_card_number, pet.crt as PET_card_type, pet.exp as PET_expiry_date, "

        SQL = SQL & "CASE "
        SQL = SQL & "WHEN INSTR(pet.crt,'VISA') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'AMERICAN EXPRESS') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'MASTER CARD') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR( pet.crt,'GIFT CARD') > 0 THEN LPAD ( SUBSTR(pet.crn, -4), LENGTH(pet.crn), 'X') "
        SQL = SQL & "ELSE pet.crn "
        SQL = SQL & "END AS PET_card_number_masked, "

        SQL = SQL & "DECODE(RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)), 'ICC', 'DIPPED', RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)) ) as PET_entry_type, "
        SQL = SQL & "pet.aut as PET_auth_num, pet.rcp as PET_rcp, "
        SQL = SQL & "pet.amt as PET_amount, pet.trm as PET_trm, pet.s_seq as PET_seq_num, pet.s_tr_tp as PET_trans_type, "
        SQL = SQL & "pet.s_batch as PET_batch_num, pet.s_swip as PET_lpsf_indicator, "
        SQL = SQL & "custom_brick_ppkg.get_pmtplan_id(pet.del_doc_num, "
        SQL = SQL & "pet.crt, "
        SQL = SQL & "pet.psd, "
        SQL = SQL & "pet.s_pln, "
        SQL = SQL & "pet.cust_cd, "
        SQL = SQL & "pet.amt, "
        SQL = SQL & "pet.dent, "
        SQL = SQL & "pet.s_term, "
        SQL = SQL & "pet.s_grce, "
        SQL = SQL & "pet.aut) as PET_plan_num, "
        SQL = SQL & "a.trn_tp_cd as ARTRN_trans_type, a.ivc_cd as ARTRN_invoice, a.amt as ARTRN_amount, a.adj_ivc_cd as ARTRN_adj_invoice, TO_CHAR(a.post_dt, 'DD-MON-YYYY') as ARTRN_post_date, a.stat_cd as ARTRN_status_code, "
        SQL = SQL & "a.cntr_cd as ARTRN_contract_code, a.csh_dwr_cd as ARTRN_cash_drawer, "
        SQL = SQL & "a.emp_cd_op as ARTRN_employee_code, a.emp_cd_cshr as ARTRN_emp_cd_cshr, "
        SQL = SQL & "a.origin_cd as ARTRN_source, a.des as ARTRN_reference_des, a.mop_cd as ARTRN_mop_cd, "
        SQL = SQL & "'' as EMP_employee_name "
        SQL = SQL & ", rownum as ROW_NUM "
        SQL = SQL & "FROM pos_electronic_transaction pet, ar_trn a, gp_pinpad_mapping gpm, store s "
        SQL = SQL & "WHERE gpm.store_cd = s.store_cd "
        SQL = SQL & "AND pet.trm = gpm.pinpad_id "
        SQL = SQL & "AND pet.cust_cd = a.cust_cd "
        SQL = SQL & "AND pet.aut = a.app_cd "
        SQL = SQL & "AND pet.dent = a.post_dt "
        SQL = SQL & "AND SUBSTR(pet.del_doc_num,1,11) = SUBSTR(a.adj_ivc_cd,1,11) "

        SQL = SQL & "AND NOT EXISTS (SELECT 'x' "
        SQL = SQL & "FROM ar_trn a2 "
        SQL = SQL & "WHERE a2.cust_cd = pet.cust_cd "
        SQL = SQL & "AND pet.dent = a2.post_dt "
        SQL = SQL & "AND NVL(a2.app_cd, 'XXXXX') = pet.aut "
        SQL = SQL & "AND pet.amt = a2.amt "
        SQL = SQL & "AND a2.ivc_cd = 'BRICK PAYMENT') "

        SQL = SQL & "AND s.co_cd = UPPER(:x_co_cd) "

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Ends

        'add clauses to TERMINAL/PET/ARTRN part of query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(p_terminalID.ToString())))) Then
            SQL = SQL & s_clauseTerminalID
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            SQL = SQL & s_clauseTransDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            SQL = SQL & s_clauseStoreCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transType.ToString())))) Then
            SQL = SQL & s_clauseTransType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            SQL = SQL & s_clauseInvoice
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            SQL = SQL & s_clauseAmount
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            SQL = SQL & s_clauseCardNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString())))) Then
            SQL = SQL & s_clauseCardType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
            SQL = SQL & s_clauseAuthNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            SQL = SQL & s_clauseCustCode
        End If

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Starts

        'JAM 03MAR2015: Jacquie requests to add new query for refunds (END)

        '---------------------------------------------------------------------------------
        'UNION -- 3. PET/AR_TRN - all transactions which are tied to a given terminal which has NOT been set up (for Brick Card/TDF)
        SQL = SQL & "UNION "

        'JAM 17MAR2015 (v2.0.5): added GP_PINPAD_MERCHANT table to query - store_cd used now, added to where clause

        '-- 3. PET/AR_TRN - all transactions which are tied to a given terminal which has NOT been set up (for Brick Card/TDF)
        SQL = SQL & "SELECT pet.trm as terminal_id, 'N/A' as pc_name, 'N/A' as terminal_ipaddr, gpm.store_cd as store_cd, '' as company_code, "
        SQL = SQL & "pet.session_id as PET_session_id, pet.cust_cd as PET_cust_cd, pet.del_doc_num as PET_ps_invoice, pet.cre_dt as PET_date_time, pet.dent as PET_date, "
        SQL = SQL & "pet.response_cd as PET_response_code, pet.line_text as PET_line_text, pet.dsp as PET_dsp_trans_msg, pet.rcp as PET_rcp_trans_msg, "
        SQL = SQL & "pet.crn as PET_card_number, pet.crt as PET_card_type, pet.exp as PET_expiry_date, "

        SQL = SQL & "CASE "
        SQL = SQL & "WHEN INSTR(pet.crt,'VISA') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'AMERICAN EXPRESS') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'MASTER CARD') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR( pet.crt,'GIFT CARD') > 0 THEN LPAD ( SUBSTR(pet.crn, -4), LENGTH(pet.crn), 'X') "
        SQL = SQL & "ELSE pet.crn "
        SQL = SQL & "END AS PET_card_number_masked, "

        SQL = SQL & "DECODE(RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)), 'ICC', 'DIPPED', RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)) ) as PET_entry_type, "
        SQL = SQL & "pet.aut as PET_auth_num, pet.rcp as PET_rcp, "
        SQL = SQL & "pet.amt as PET_amount, pet.trm as PET_trm, pet.s_seq as PET_seq_num, pet.s_tr_tp as PET_trans_type, "
        SQL = SQL & "pet.s_batch as PET_batch_num, pet.s_swip as PET_lpsf_indicator, "
        SQL = SQL & "custom_brick_ppkg.get_pmtplan_id(pet.del_doc_num, "
        SQL = SQL & "pet.crt, "
        SQL = SQL & "pet.psd, "
        SQL = SQL & "pet.s_pln, "
        SQL = SQL & "pet.cust_cd, "
        SQL = SQL & "pet.amt, "
        SQL = SQL & "pet.dent, "
        SQL = SQL & "pet.s_term, "
        SQL = SQL & "pet.s_grce, "
        SQL = SQL & "pet.aut) as PET_plan_num, "
        SQL = SQL & "a.trn_tp_cd as ARTRN_trans_type, a.ivc_cd as ARTRN_invoice, a.amt as ARTRN_amount, a.adj_ivc_cd as ARTRN_adj_invoice, TO_CHAR(a.post_dt, 'DD-MON-YYYY') as ARTRN_post_date, a.stat_cd as ARTRN_status_code, "
        SQL = SQL & "a.cntr_cd as ARTRN_contract_code, a.csh_dwr_cd as ARTRN_cash_drawer, "
        SQL = SQL & "a.emp_cd_op as ARTRN_employee_code, a.emp_cd_cshr as ARTRN_emp_cd_cshr, "
        SQL = SQL & "a.origin_cd as ARTRN_source, a.des as ARTRN_reference_des, a.mop_cd as ARTRN_mop_cd, "
        SQL = SQL & "'' as EMP_employee_name "
        SQL = SQL & ", rownum as ROW_NUM "
        SQL = SQL & "FROM pos_electronic_transaction pet, ar_trn a, gp_pinpad_merchant gpm, store s "
        'JAM 25Mar2015 (v2.0.6): company code required in query
        SQL = SQL & "WHERE gpm.store_cd = s.store_cd "
        'JAM 25Mar2015 (v2.0.6) (END)

        SQL = SQL & "AND pet.cust_cd = a.cust_cd(+) "
        SQL = SQL & "AND pet.aut = a.app_cd(+) "
        '--ADDED 12FEB2015
        SQL = SQL & "AND pet.dent = a.post_dt(+) "
        '--ADDED 12FEB2015 (END)

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Ends

        'JAM 11FEB2015: Jacquie requests to match on partial invoice due to rewrites, remove matching on amount as well
        'ORIG: SQL = SQL & "AND pet.del_doc_num = a.ivc_cd(+) "
        'ORIG: SQL = SQL & "AND pet.amt = a.amt(+) "

        'JAM 03MAR2015: Jacquie requests to remove matching on partial invoice
        'ORIG: SQL = SQL & "AND SUBSTR(pet.del_doc_num,1,11) = SUBSTR(a.ivc_cd(+),1,11) "
        'JAM 03MAR2015 (END)

        'JAM 11FEB2015 (END)

        SQL = SQL & "AND SUBSTR(pet.crn, LENGTH(pet.crn)-3, 4) = SUBSTR(a.bnk_crd_num(+), LENGTH(a.bnk_crd_num(+))-3, 4) "
        SQL = SQL & "AND pet.trm NOT IN (SELECT NVL(pinpad_id,'X') FROM gp_pinpad_mapping ) "

        'JAM 17MAR2015 (v2.0.5): Now that store code can be pulled from GP_PINPAD_MERCHANT
        'SQL = SQL & "AND SUBSTR(pet.del_doc_num, 6, 2) IN (SELECT store_cd FROM store WHERE co_cd = UPPER(:x_co_cd)) "
        SQL = SQL & "AND SUBSTR(pet.trm,1,3) = SUBSTR(gpm.td_store_num,7,3) "
        'JAM 17MAR2015 (END)

        'add clauses to PET/ARTRN part of query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            SQL = SQL & s_clauseTransDate
        End If

        'JAM 17MAR2015 (v2.0.5): Now that store code can be pulled from GP_PINPAD_MERCHANT
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            SQL = SQL & s_clauseStoreCode
        End If
        'JAM 17MAR2015 (END)

        'JAM 25Mar2015 (v2.0.6): company code required in query
        SQL = SQL & "AND s.co_cd = UPPER(:x_co_cd) "
        'JAM 25Mar2015 (v2.0.6) (END)

        If (Not (String.IsNullOrEmpty(Trim(p_transType.ToString())))) Then
            SQL = SQL & s_clauseTransType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            SQL = SQL & s_clauseInvoice
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            SQL = SQL & s_clauseAmount
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            SQL = SQL & s_clauseCardNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString())))) Then
            SQL = SQL & s_clauseCardType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            SQL = SQL & s_clauseCustCode
        End If


        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Starts
        '--ADDED 12FEB2015
        '---------------------------------------------------------------------------------
        'UNION -- 4. PET/AR_TRN - all transactions which are tied to a brick card payment (the debit card portion)
        SQL = SQL & "UNION "

        '-- 4. PET/AR_TRN - all transactions which are tied to a brick card payment (the debit card portion)
        SQL = SQL & "SELECT gpm.pinpad_id as terminal_id, gpm.pc_nm as pc_name, gpm.pc_ipaddr as terminal_ipaddr, gpm.store_cd as store_cd, s.co_cd as company_code, "
        SQL = SQL & "pet.session_id as PET_session_id, pet.cust_cd as PET_cust_cd, pet.del_doc_num as PET_ps_invoice, pet.cre_dt as PET_date_time, pet.dent as PET_date, "
        SQL = SQL & "pet.response_cd as PET_response_code, pet.line_text as PET_line_text, pet.dsp as PET_dsp_trans_msg, pet.rcp as PET_rcp_trans_msg, "
        SQL = SQL & "pet.crn as PET_card_number, pet.crt as PET_card_type, pet.exp as PET_expiry_date, "

        SQL = SQL & "CASE "
        SQL = SQL & "WHEN INSTR(pet.crt,'VISA') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'AMERICAN EXPRESS') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'MASTER CARD') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR( pet.crt,'GIFT CARD') > 0 THEN LPAD ( SUBSTR(pet.crn, -4), LENGTH(pet.crn), 'X') "
        SQL = SQL & "ELSE pet.crn "
        SQL = SQL & "END AS PET_card_number_masked, "

        SQL = SQL & "DECODE(RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)), 'ICC', 'DIPPED', RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)) ) as PET_entry_type, "
        SQL = SQL & "pet.aut as PET_auth_num, pet.rcp as PET_rcp, "
        SQL = SQL & "pet.amt as PET_amount, pet.trm as PET_trm, pet.s_seq as PET_seq_num, pet.s_tr_tp as PET_trans_type, "
        SQL = SQL & "pet.s_batch as PET_batch_num, pet.s_swip as PET_lpsf_indicator, "
        SQL = SQL & "custom_brick_ppkg.get_pmtplan_id(pet.del_doc_num, "
        SQL = SQL & "pet.crt, "
        SQL = SQL & "pet.psd, "
        SQL = SQL & "pet.s_pln, "
        SQL = SQL & "pet.cust_cd, "
        SQL = SQL & "pet.amt, "
        SQL = SQL & "pet.dent, "
        SQL = SQL & "pet.s_term, "
        SQL = SQL & "pet.s_grce, "
        SQL = SQL & "pet.aut) as PET_plan_num, "
        SQL = SQL & "a.trn_tp_cd as ARTRN_trans_type, a.ivc_cd as ARTRN_invoice, a.amt as ARTRN_amount, a.adj_ivc_cd as ARTRN_adj_invoice, TO_CHAR(a.post_dt, 'DD-MON-YYYY') as ARTRN_post_date, a.stat_cd as ARTRN_status_code, "
        SQL = SQL & "a.cntr_cd as ARTRN_contract_code, a.csh_dwr_cd as ARTRN_cash_drawer, "
        SQL = SQL & "a.emp_cd_op as ARTRN_employee_code, a.emp_cd_cshr as ARTRN_emp_cd_cshr, "
        SQL = SQL & "a.origin_cd as ARTRN_source, a.des as ARTRN_reference_des, a.mop_cd as ARTRN_mop_cd, "
        SQL = SQL & "'' as EMP_employee_name "
        SQL = SQL & ", rownum as ROW_NUM "
        SQL = SQL & "FROM pos_electronic_transaction pet, ar_trn a, gp_pinpad_mapping gpm, store s "
        SQL = SQL & "WHERE gpm.store_cd = s.store_cd "
        SQL = SQL & "AND pet.trm = gpm.pinpad_id "
        SQL = SQL & "AND pet.cust_cd = a.cust_cd "
        SQL = SQL & "AND pet.aut = a.app_cd "
        SQL = SQL & "AND pet.dent = a.post_dt "
        SQL = SQL & "AND pet.del_doc_num <> 'BRICK PAYMENT' "

        'JAM 03MAR2015: Jacquie requests change to query, added...
        SQL = SQL & "AND a.ivc_cd = 'BRICK PAYMENT' "
        'JAM 03MAR2015 (END)

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Ends

        '--- THIS REMOVED NOW AS pet <> artrn invoices
        'SQL = SQL & "AND SUBSTR(pet.del_doc_num,1,11) = SUBSTR(a.ivc_cd(+),1,11) "

        'JAM 03MAR2015: Jacquie requests to remove matching on card#
        'ORIG: SQL = SQL & "AND SUBSTR(pet.crn, LENGTH(pet.crn)-3, 4) = SUBSTR(a.bnk_crd_num, LENGTH(a.bnk_crd_num)-3, 4) "
        'JAM 03MAR2015 (END)

        '--ADDED 12FEB2015 (END)

        SQL = SQL & "AND s.co_cd = UPPER(:x_co_cd) "

        'add clauses to TERMINAL/PET/ARTRN part of query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(p_terminalID.ToString())))) Then
            SQL = SQL & s_clauseTerminalID
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            SQL = SQL & s_clauseTransDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            SQL = SQL & s_clauseStoreCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transType.ToString())))) Then
            SQL = SQL & s_clauseTransType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            SQL = SQL & s_clauseInvoice
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            SQL = SQL & s_clauseAmount
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            SQL = SQL & s_clauseCardNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString())))) Then
            SQL = SQL & s_clauseCardType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
            SQL = SQL & s_clauseAuthNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            SQL = SQL & s_clauseCustCode
        End If

        SQL = SQL & "ORDER BY PET_date_time "
        'order by 9

        'Set SQL object
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        'bind variables based on parameters passed in
        objsql.Parameters.Add(":x_co_cd", OracleType.VarChar)
        objsql.Parameters(":x_co_cd").Value = s_co_cd

        If (Not (String.IsNullOrEmpty(Trim(p_terminalID.ToString())))) Then
            objsql.Parameters.Add(":x_terminalID", OracleType.VarChar)
            objsql.Parameters(":x_terminalID").Value = p_terminalID
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            objsql.Parameters.Add(":x_transDate", OracleType.DateTime)
            objsql.Parameters(":x_transDate").Value = p_transDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
            objsql.Parameters(":x_storeCode").Value = p_storeCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transType.ToString())))) Then
            objsql.Parameters.Add(":x_transType", OracleType.VarChar)
            objsql.Parameters(":x_transType").Value = p_transType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            objsql.Parameters.Add(":x_invoice", OracleType.VarChar)
            objsql.Parameters(":x_invoice").Value = p_invoice
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            objsql.Parameters.Add(":x_amount", OracleType.VarChar)
            objsql.Parameters(":x_amount").Value = p_amount
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            objsql.Parameters.Add(":x_cardNumber", OracleType.VarChar)
            objsql.Parameters(":x_cardNumber").Value = p_cardNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString())))) Then
            objsql.Parameters.Add(":x_cardType", OracleType.VarChar)
            objsql.Parameters(":x_cardType").Value = p_cardType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
            objsql.Parameters.Add(":x_authNumber", OracleType.VarChar)
            objsql.Parameters(":x_authNumber").Value = p_authNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            objsql.Parameters.Add(":x_custCode", OracleType.VarChar)
            objsql.Parameters(":x_custCode").Value = p_custCode
        End If

        Dim ds As New DataSet
        Dim dv As DataView

        Try
            With objsql
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
            oAdp.Fill(ds)

            dv = New DataView(ds.Tables(0))

            Dim n_positionCounter As Integer = 1
            Dim n_queryResultsCount As Integer = dv.Count

            Session("ds_queryResults") = ds
            Session("POSITION_INDEX") = n_positionCounter
            Session("ROW_NUM") = ""

            If (n_queryResultsCount > 0) Then
                Session("ROW_NUM") = dv(0)("ROW_NUM").ToString
            End If

            'for debugging

            If (n_queryResultsCount > gn_maxRecordsRetrieved) Then

                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0020, n_queryResultsCount.ToString, gn_maxRecordsRetrieved.ToString)
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

                Session("ds_queryResults") = New DataSet
                Session("POSITION_INDEX") = 0
                Session("ROW_NUM") = ""

                Return
            End If

            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        'bind dataview data to fields on screen based on user's position in the data (in this case the first record)
        BindPETARTRNDataViewData()

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           BindPETARTRNDataViewData()
    '   Created by:     John Melenko
    '   Date:           Jun2015
    '   Description:    TODO
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub BindPETARTRNDataViewData()

        btnFirst.Visible = False
        btnPrev.Visible = False
        btnNext.Visible = False
        btnLast.Visible = False
        btn_ToggleToGridResults.Visible = False
        btn_ToggleToGridResults.Enabled = False

        'Code Added by Kumaran on 17-Sep-2015 for reprinting the receipts 
        btn_print_receipt.Visible = False
        printer_drp.Visible = False

        lbl_resultsInfo.Visible = True
        lbl_resultsInfo.ForeColor = Color.Green

        Dim ds As DataSet = Session("ds_queryResults")
        Dim dv = New DataView(ds.Tables(0))
        Dim n_positionCounter As Integer = Session("POSITION_INDEX")
        Dim n_queryResultsCount As Integer = dv.Count

        If (n_queryResultsCount > 0) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            lbl_resultsInfo.Text = String.Format(Resources.CustomMessages.MSG0019, n_positionCounter.ToString, n_queryResultsCount.ToString)
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

        Else

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            lbl_resultsInfo.Text = Resources.CustomMessages.MSG0031
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            lbl_resultsInfo.ForeColor = Color.Red

        End If

        ' make all the buttons visible if page count is more than 0
        If (n_queryResultsCount > 0) Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            btn_ToggleToGridResults.Visible = True
            btn_ToggleToGridResults.Enabled = True

            'Code Added by Kumaran on 17-Sep-2015 for reprinting the receipts 
            btn_print_receipt.Visible = True
            btn_print_receipt.Enabled = True
            printer_drp.Visible = True
        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True

        ' then turn off buttons that don't make sense
        If (n_queryResultsCount = 1) Then
            ' only 1 record
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If (n_positionCounter = 1) Then
                ' we are sitting at first record
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If (n_positionCounter = n_queryResultsCount) Then
                    ' we are sitting at last record
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If

        End If

        'JAM - to use a filter, but using position counter instead
        'If (ds Is Nothing OrElse Not ds.IsInitialized) Then
        '    'do nothing
        'Else
        '    dv.RowFilter = ""
        '    'Dim n_row_num = Session("n_row_num")
        '    Dim n_ROW_NUM = Session("ROW_NUM")
        '    If (Not (String.IsNullOrEmpty(Trim(n_ROW_NUM.ToString())))) Then

        '        dv.RowFilter = "ROW_NUM = " & n_ROW_NUM.ToString

        '    End If
        'End If

        Dim n_current_position = 0

        For Each drv As DataRowView In dv

            n_current_position += 1
            If (n_current_position = n_positionCounter) Then

                txt_terminal_id.Text = drv("TERMINAL_ID").ToString
                txt_terminal_ipaddr.Text = drv("TERMINAL_IPADDR").ToString
                txt_pc_name.Text = drv("PC_NAME").ToString

                txt_store_cd.Text = drv("STORE_CD").ToString
                txt_company_code.Text = drv("COMPANY_CODE").ToString

                txt_PET_trans_type.Text = drv("PET_TRANS_TYPE").ToString
                txt_PET_ps_invoice.Text = drv("PET_PS_INVOICE").ToString

                txt_PET_amount.Text = ""

                If (Not (String.IsNullOrEmpty(Trim(drv("PET_AMOUNT").ToString())))) Then

                    txt_PET_amount.Text = FormatCurrency(drv("PET_AMOUNT").ToString, 2)

                End If

                txt_PET_entry_type.Text = drv("PET_ENTRY_TYPE").ToString
                txt_PET_expiry_date.Text = drv("PET_EXPIRY_DATE").ToString
                txt_PET_card_type.Text = drv("PET_CARD_TYPE").ToString

                ''txt_PET_card_number.Text = drv("PET_CARD_NUMBER_MASKED").ToString         'Aug 18,2017 - mariam- #3216 and #3324
                txt_PET_card_number.Text = GetCardNumber(drv)

                txt_PET_plan_num.Text = drv("PET_PLAN_NUM").ToString
                txt_PET_auth_num.Text = drv("PET_AUTH_NUM").ToString
                txt_PET_dsp_trans_msg.Text = drv("PET_DSP_TRANS_MSG").ToString
                txt_PET_rcp_trans_msg.Text = drv("PET_RCP_TRANS_MSG").ToString

                txt_PET_cust_cd.Text = drv("PET_CUST_CD").ToString
                txt_PET_date_time.Text = drv("PET_DATE_TIME").ToString
                txt_PET_batch_num.Text = drv("PET_BATCH_NUM").ToString
                txt_PET_seq_num.Text = drv("PET_SEQ_NUM").ToString
                txt_PET_lpsf_indicator.Text = drv("PET_LPSF_INDICATOR").ToString

                txt_ARTRN_trans_type.Text = drv("ARTRN_TRANS_TYPE").ToString
                txt_ARTRN_invoice.Text = drv("ARTRN_INVOICE").ToString

                txt_ARTRN_amount.Text = ""

                If (Not (String.IsNullOrEmpty(Trim(drv("ARTRN_AMOUNT").ToString())))) Then
                    txt_ARTRN_amount.Text = FormatCurrency(drv("ARTRN_AMOUNT").ToString, 2)
                End If

                txt_ARTRN_adj_invoice.Text = drv("ARTRN_ADJ_INVOICE").ToString
                txt_ARTRN_contract_code.Text = drv("ARTRN_CONTRACT_CODE").ToString
                txt_ARTRN_cash_drawer.Text = drv("ARTRN_CASH_DRAWER").ToString
                txt_ARTRN_status_code.Text = drv("ARTRN_STATUS_CODE").ToString
                txt_ARTRN_mop_cd.Text = drv("ARTRN_MOP_CD").ToString
                txt_ARTRN_source.Text = drv("ARTRN_SOURCE").ToString
                txt_ARTRN_reference_des.Text = drv("ARTRN_REFERENCE_DES").ToString
                txt_ARTRN_post_date.Text = drv("ARTRN_POST_DATE").ToString
                txt_ARTRN_employee_code.Text = drv("ARTRN_EMPLOYEE_CODE").ToString
                txt_ARTRN_employee_name.Text = drv("EMP_EMPLOYEE_NAME").ToString

                txt_ARTRN_employee_name.Text = RetrieveEmployeeName(drv("ARTRN_EMPLOYEE_CODE").ToString)

                txt_SO_employee_code.Text = RetrieveSalesOrderEmployee(txt_PET_ps_invoice.Text)
                txt_SO_employee_name.Text = RetrieveEmployeeName(txt_SO_employee_code.Text)

                Session("ROW_NUM") = drv("ROW_NUM").ToString

                Exit For
            End If
        Next

        Dim s_trans_date As String = txt_PET_date_time.Text

        If Not (String.IsNullOrEmpty(Trim(s_trans_date.ToString()))) Then
            Dim d_trans_date As Date = Convert.ToDateTime(s_trans_date).ToString("dd MMM yyyy")
            dateEdit_transaction_date.Date = d_trans_date
        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrievePETTransactions()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Main procedure which retrieves the transaction information based on query parameters entered by the user.
    '   Called by:      "Lookup Prices" button - btn_Lookup_Click()
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrievePETTransactions(p_terminalID As String, p_transDate As String, p_storeCode As String, p_transType As String, p_invoice As String, p_amount As String, p_cardNumber As String, p_cardType As String, p_authNumber As String, p_custCode As String)

        Dim s_errorMessage As String = ""

        If (String.IsNullOrEmpty(Trim(p_terminalID.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_transDate.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_storeCode.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_transType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_invoice.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardNumber.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_authNumber.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_amount.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_custCode.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = String.Format(Resources.CustomMessages.MSG0023, vbCrLf)
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            txt_terminal_id.Text = Trim(txt_terminal_id.Text)
            dateEdit_transaction_date.Text = Trim(dateEdit_transaction_date.Text)
            txt_store_cd.Text = Trim(txt_store_cd.Text)
            txt_PET_trans_type.Text = Trim(txt_PET_trans_type.Text)
            txt_PET_ps_invoice.Text = Trim(txt_PET_ps_invoice.Text)
            txt_PET_amount.Text = Trim(txt_PET_amount.Text)
            txt_PET_card_number.Text = Trim(txt_PET_card_number.Text)
            txt_PET_card_type.Text = Trim(txt_PET_card_type.Text)
            txt_PET_auth_num.Text = Trim(txt_PET_auth_num.Text)
            txt_PET_cust_cd.Text = Trim(txt_PET_cust_cd.Text)
            Return
        End If

        'STORE CODE REQUIRES at least terminalID or transaction date or transaction type or invoice or card# or auth# or card type or amount
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString()))) AndAlso _
            (String.IsNullOrEmpty(Trim(p_terminalID.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_transDate.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_transType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_invoice.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardNumber.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_authNumber.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0024
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return
        End If

        'CARD TYPE REQUIRES at least terminalID or transaction date or transaction type or invoice or card# or auth# or store code or amount
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString()))) AndAlso _
            (String.IsNullOrEmpty(Trim(p_terminalID.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_transDate.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_transType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_invoice.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardNumber.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_authNumber.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_storeCode.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0025
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return
        End If

        'AMOUNT REQUIRES at least terminalID or transaction date or transaction type or invoice or card# or auth# or card type or store code
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString()))) AndAlso _
            (String.IsNullOrEmpty(Trim(p_terminalID.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_transDate.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_transType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_invoice.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardNumber.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_authNumber.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardType.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0026
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return
        End If

        'CARD NUMBER must be at least 3 digits long
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString()))) AndAlso p_cardNumber.Length < 4) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0027
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")

        'for debugging

        If Not ((Not (s_co_cd Is Nothing)) AndAlso s_co_cd.isNotEmpty) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0022
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")
            Return

        End If

        '--------------------------------------------------------------------------
        '-- PET data retrieval
        '--------------------------------------------------------------------------
        '-- UNION of two queries:
        '-- 1. TERMINAL/PET - all transactions which are tied to a given terminal which has been set up (gp_pinpad_mapping)
        '-- 2. PET - all transactions which are tied to a given terminal which has NOT been set up (for Brick Card/TDF) - uses gp_pinpad_merchant
        '----------------------------

        'AND clauses to add based on parameters passed in
        Dim s_clauseTerminalID = "AND gpm.pinpad_id = UPPER(:x_terminalID) "
        Dim s_clauseTransDate = "AND pet.dent = UPPER(:x_transDate) "
        Dim s_clauseStoreCode = "AND gpm.store_cd = UPPER(:x_storeCode) "
        Dim s_clauseTransType = "AND pet.s_tr_tp = UPPER(:x_transType) "
        Dim s_clauseInvoice = "AND pet.del_doc_num LIKE UPPER(:x_invoice) "
        Dim s_clauseAmount = "AND pet.amt = :x_amount "
        Dim s_clauseCardNumber = "AND pet.crn LIKE UPPER(:x_cardNumber) "
        Dim s_clauseCardType = "AND pet.crt LIKE UPPER(:x_cardType) "
        Dim s_clauseAuthNumber = "AND pet.aut = UPPER(:x_authNumber) "
        Dim s_clauseCustCode = "AND pet.cust_cd LIKE UPPER(:x_custCode) "

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Starts

        '---------------------------------------------------------------------------------
        '-- 1. TERMINAL/PET - all transactions which are tied to a given terminal which has been set up (gp_pinpad_mapping) 
        SQL = "SELECT gpm.pinpad_id as terminal_id, gpm.pc_nm as pc_name, gpm.pc_ipaddr as terminal_ipaddr, gpm.store_cd as store_cd, s.co_cd as company_code, "
        SQL = SQL & "pet.session_id as PET_session_id, pet.cust_cd as PET_cust_cd, pet.del_doc_num as PET_ps_invoice, pet.cre_dt as PET_date_time, pet.dent as PET_date, "
        SQL = SQL & "pet.response_cd as PET_response_code, pet.line_text as PET_line_text, pet.dsp as PET_dsp_trans_msg, pet.rcp as PET_rcp_trans_msg, "
        SQL = SQL & "pet.crn as PET_card_number, pet.crt as PET_card_type, pet.exp as PET_expiry_date, "

        SQL = SQL & "CASE "
        SQL = SQL & "WHEN INSTR(pet.crt,'VISA') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'AMERICAN EXPRESS') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'MASTER CARD') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR( pet.crt,'GIFT CARD') > 0 THEN LPAD ( SUBSTR(pet.crn, -4), LENGTH(pet.crn), 'X') "
        SQL = SQL & "ELSE pet.crn "
        SQL = SQL & "END AS PET_card_number_masked, "

        SQL = SQL & "DECODE(RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)), 'ICC', 'DIPPED', RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)) ) as PET_entry_type, "
        SQL = SQL & "pet.aut as PET_auth_num, pet.rcp as PET_rcp, "
        SQL = SQL & "pet.amt as PET_amount, pet.trm as PET_trm, pet.s_seq as PET_seq_num, pet.s_tr_tp as PET_trans_type, "
        SQL = SQL & "pet.s_batch as PET_batch_num, pet.s_swip as PET_lpsf_indicator, "
        SQL = SQL & "custom_brick_ppkg.get_pmtplan_id(pet.del_doc_num, "
        SQL = SQL & "pet.crt, "
        SQL = SQL & "pet.psd, "
        SQL = SQL & "pet.s_pln, "
        SQL = SQL & "pet.cust_cd, "
        SQL = SQL & "pet.amt, "
        SQL = SQL & "pet.dent, "
        SQL = SQL & "pet.s_term, "
        SQL = SQL & "pet.s_grce, "
        SQL = SQL & "pet.aut) as PET_plan_num, "
        SQL = SQL & "'' as EMP_employee_name "
        SQL = SQL & ", rownum as ROW_NUM "
        SQL = SQL & "FROM pos_electronic_transaction pet, gp_pinpad_mapping gpm, store s "

        SQL = SQL & "WHERE gpm.store_cd = s.store_cd "
        SQL = SQL & "AND pet.trm = gpm.pinpad_id "
        SQL = SQL & "AND s.co_cd = UPPER(:x_co_cd) "

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Ends

        'add clauses to TERMINAL/PET/ARTRN part of query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(p_terminalID.ToString())))) Then
            SQL = SQL & s_clauseTerminalID
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            SQL = SQL & s_clauseTransDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            SQL = SQL & s_clauseStoreCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transType.ToString())))) Then
            SQL = SQL & s_clauseTransType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            SQL = SQL & s_clauseInvoice
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            SQL = SQL & s_clauseAmount
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            SQL = SQL & s_clauseCardNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString())))) Then
            SQL = SQL & s_clauseCardType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
            SQL = SQL & s_clauseAuthNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            SQL = SQL & s_clauseCustCode
        End If


        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Starts

        '---------------------------------------------------------------------------------
        'UNION-- 2. PET - all transactions which are tied to a given terminal which has NOT been set up (for Brick Card/TDF) - uses gp_pinpad_merchant
        SQL = SQL & "UNION "

        '-- 2. PET - all transactions which are tied to a given terminal which has NOT been set up (for Brick Card/TDF) - uses gp_pinpad_merchant
        SQL = SQL & "SELECT pet.trm as terminal_id, 'N/A' as pc_name, 'N/A' as terminal_ipaddr, gpm.store_cd as store_cd, '' as company_code, "
        SQL = SQL & "pet.session_id as PET_session_id, pet.cust_cd as PET_cust_cd, pet.del_doc_num as PET_ps_invoice, pet.cre_dt as PET_date_time, pet.dent as PET_date, "
        SQL = SQL & "pet.response_cd as PET_response_code, pet.line_text as PET_line_text, pet.dsp as PET_dsp_trans_msg, pet.rcp as PET_rcp_trans_msg, "
        SQL = SQL & "pet.crn as PET_card_number, pet.crt as PET_card_type, pet.exp as PET_expiry_date, "

        SQL = SQL & "CASE "
        SQL = SQL & "WHEN INSTR(pet.crt,'VISA') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'AMERICAN EXPRESS') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR(pet.crt,'MASTER CARD') > 0 THEN LPAD( SUBSTR(pet.crn, -4), LENGTH(pet.crn),'X') "
        SQL = SQL & "WHEN INSTR( pet.crt,'GIFT CARD') > 0 THEN LPAD ( SUBSTR(pet.crn, -4), LENGTH(pet.crn), 'X') "
        SQL = SQL & "ELSE pet.crn "
        SQL = SQL & "END AS PET_card_number_masked, "

        SQL = SQL & "DECODE(RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)), 'ICC', 'DIPPED', RTRIM(SUBSTR( pet.swp, 1, INSTR( pet.swp, '[')-1)) ) as PET_entry_type, "
        SQL = SQL & "pet.aut as PET_auth_num, pet.rcp as PET_rcp, "
        SQL = SQL & "pet.amt as PET_amount, pet.trm as PET_trm, pet.s_seq as PET_seq_num, pet.s_tr_tp as PET_trans_type, "
        SQL = SQL & "pet.s_batch as PET_batch_num, pet.s_swip as PET_lpsf_indicator, "
        SQL = SQL & "custom_brick_ppkg.get_pmtplan_id(pet.del_doc_num, "
        SQL = SQL & "pet.crt, "
        SQL = SQL & "pet.psd, "
        SQL = SQL & "pet.s_pln, "
        SQL = SQL & "pet.cust_cd, "
        SQL = SQL & "pet.amt, "
        SQL = SQL & "pet.dent, "
        SQL = SQL & "pet.s_term, "
        SQL = SQL & "pet.s_grce, "
        SQL = SQL & "pet.aut) as PET_plan_num, "
        SQL = SQL & "'' as EMP_employee_name "
        SQL = SQL & ", rownum as ROW_NUM "
        SQL = SQL & "FROM pos_electronic_transaction pet, gp_pinpad_merchant gpm, store s "
        SQL = SQL & "WHERE gpm.store_cd = s.store_cd "

        SQL = SQL & "AND pet.trm NOT IN (SELECT NVL(pinpad_id,'X') FROM gp_pinpad_mapping ) "

        SQL = SQL & "AND SUBSTR(pet.trm,1,3) = SUBSTR(gpm.td_store_num,7,3) "

        'Code Modified by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Ends

        'add clauses to PET/ARTRN part of query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            SQL = SQL & s_clauseTransDate
        End If

        'JAM 17MAR2015 (v2.0.5): Now that store code can be pulled from GP_PINPAD_MERCHANT
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            SQL = SQL & s_clauseStoreCode
        End If
        'JAM 17MAR2015 (END)

        'JAM 25Mar2015 (v2.0.6): company code required in query
        SQL = SQL & "AND s.co_cd = UPPER(:x_co_cd) "
        'JAM 25Mar2015 (v2.0.6) (END)

        If (Not (String.IsNullOrEmpty(Trim(p_transType.ToString())))) Then
            SQL = SQL & s_clauseTransType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            SQL = SQL & s_clauseInvoice
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            SQL = SQL & s_clauseAmount
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            SQL = SQL & s_clauseCardNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString())))) Then
            SQL = SQL & s_clauseCardType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            SQL = SQL & s_clauseCustCode
        End If

        SQL = SQL & "ORDER BY PET_date_time "
        'order by 9

        'Set SQL object
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        'bind variables based on parameters passed in
        objsql.Parameters.Add(":x_co_cd", OracleType.VarChar)
        objsql.Parameters(":x_co_cd").Value = s_co_cd

        If (Not (String.IsNullOrEmpty(Trim(p_terminalID.ToString())))) Then
            objsql.Parameters.Add(":x_terminalID", OracleType.VarChar)
            objsql.Parameters(":x_terminalID").Value = p_terminalID
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            objsql.Parameters.Add(":x_transDate", OracleType.DateTime)
            objsql.Parameters(":x_transDate").Value = p_transDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_storeCode.ToString())))) Then
            objsql.Parameters.Add(":x_storeCode", OracleType.VarChar)
            objsql.Parameters(":x_storeCode").Value = p_storeCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_transType.ToString())))) Then
            objsql.Parameters.Add(":x_transType", OracleType.VarChar)
            objsql.Parameters(":x_transType").Value = p_transType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            objsql.Parameters.Add(":x_invoice", OracleType.VarChar)
            objsql.Parameters(":x_invoice").Value = p_invoice
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            objsql.Parameters.Add(":x_amount", OracleType.VarChar)
            objsql.Parameters(":x_amount").Value = p_amount
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            objsql.Parameters.Add(":x_cardNumber", OracleType.VarChar)
            objsql.Parameters(":x_cardNumber").Value = p_cardNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardType.ToString())))) Then
            objsql.Parameters.Add(":x_cardType", OracleType.VarChar)
            objsql.Parameters(":x_cardType").Value = p_cardType
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
            objsql.Parameters.Add(":x_authNumber", OracleType.VarChar)
            objsql.Parameters(":x_authNumber").Value = p_authNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            objsql.Parameters.Add(":x_custCode", OracleType.VarChar)
            objsql.Parameters(":x_custCode").Value = p_custCode
        End If

        Dim ds As New DataSet
        Dim dv As DataView

        Try
            With objsql
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
            oAdp.Fill(ds)

            dv = New DataView(ds.Tables(0))

            Dim n_positionCounter As Integer = 1
            Dim n_queryResultsCount As Integer = dv.Count

            Session("ds_queryResults") = ds
            Session("POSITION_INDEX") = n_positionCounter
            Session("ROW_NUM") = ""

            If (n_queryResultsCount > 0) Then
                Session("ROW_NUM") = dv(0)("ROW_NUM").ToString
            End If

            'for debugging

            If (n_queryResultsCount > gn_maxRecordsRetrieved) Then

                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                s_errorMessage = String.Format(Resources.CustomMessages.MSG0020, n_queryResultsCount.ToString, gn_maxRecordsRetrieved.ToString)
                'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                DisplayMessage(s_errorMessage, "LABEL", "ERROR")

                Session("ds_queryResults") = New DataSet
                Session("POSITION_INDEX") = 0
                Session("ROW_NUM") = ""

                Return
            End If

            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        'bind dataview data to fields on screen based on user's position in the data (in this case the first record)
        BindPETDataViewData()

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           BindPETDataViewData()
    '   Created by:     John Melenko
    '   Date:           Jun2015
    '   Description:    TODO
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub BindPETDataViewData()

        btnFirst.Visible = False
        btnPrev.Visible = False
        btnNext.Visible = False
        btnLast.Visible = False
        btn_ToggleToGridResults.Visible = False
        btn_ToggleToGridResults.Enabled = False


        'Code Added by Kumaran on 17-Sep-2015 for reprinting the receipts 
        btn_print_receipt.Visible = False
        printer_drp.Visible = False

        lbl_resultsInfo.Visible = True
        lbl_resultsInfo.ForeColor = Color.Green

        Dim ds As DataSet = Session("ds_queryResults")
        Dim dv = New DataView(ds.Tables(0))
        Dim n_positionCounter As Integer = Session("POSITION_INDEX")
        Dim n_queryResultsCount As Integer = dv.Count

        If (n_queryResultsCount > 0) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            lbl_resultsInfo.Text = String.Format(Resources.CustomMessages.MSG0019, n_positionCounter.ToString, n_queryResultsCount.ToString)
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

        Else

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            lbl_resultsInfo.Text = Resources.CustomMessages.MSG0031
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends
            lbl_resultsInfo.ForeColor = Color.Red
        End If

        ' make all the buttons visible if page count is more than 0
        If (n_queryResultsCount > 0) Then
            btnFirst.Visible = True
            btnPrev.Visible = True
            btnNext.Visible = True
            btnLast.Visible = True
            btn_ToggleToGridResults.Visible = True
            btn_ToggleToGridResults.Enabled = True


            'Code Added by Kumaran on 17-Sep-2015 for reprinting the receipts 
            btn_print_receipt.Enabled = True
            btn_print_receipt.Visible = True
            printer_drp.Visible = True

        End If

        ' turn all buttons on by default
        btnFirst.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True

        ' then turn off buttons that don't make sense
        If (n_queryResultsCount = 1) Then
            ' only 1 record
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            btnNext.Enabled = False
            btnLast.Enabled = False
        Else
            If (n_positionCounter = 1) Then
                ' we are sitting at first record
                btnFirst.Enabled = False
                btnPrev.Enabled = False
            Else
                If (n_positionCounter = n_queryResultsCount) Then
                    ' we are sitting at last record
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If

        End If

        'reset all ARTRN data fields
        ResetARTRNDataFields()

        Dim n_current_position = 0
        Dim s_PET_response_code As String = ""

        For Each drv As DataRowView In dv

            n_current_position += 1
            If (n_current_position = n_positionCounter) Then

                s_PET_response_code = drv("PET_RESPONSE_CODE").ToString

                txt_terminal_id.Text = drv("TERMINAL_ID").ToString
                txt_terminal_ipaddr.Text = drv("TERMINAL_IPADDR").ToString
                txt_pc_name.Text = drv("PC_NAME").ToString

                txt_store_cd.Text = drv("STORE_CD").ToString
                txt_company_code.Text = drv("COMPANY_CODE").ToString

                txt_PET_trans_type.Text = drv("PET_TRANS_TYPE").ToString
                txt_PET_ps_invoice.Text = drv("PET_PS_INVOICE").ToString

                txt_PET_amount.Text = ""

                If (Not (String.IsNullOrEmpty(Trim(drv("PET_AMOUNT").ToString())))) Then
                    txt_PET_amount.Text = FormatCurrency(drv("PET_AMOUNT").ToString, 2)
                End If

                txt_PET_entry_type.Text = drv("PET_ENTRY_TYPE").ToString
                txt_PET_expiry_date.Text = drv("PET_EXPIRY_DATE").ToString
                txt_PET_card_type.Text = drv("PET_CARD_TYPE").ToString

                ''txt_PET_card_number.Text = drv("PET_CARD_NUMBER_MASKED").ToString     'Aug 18,2017 - mariam- #3216 and #3324
                txt_PET_card_number.Text = GetCardNumber(drv)
                
                txt_PET_plan_num.Text = drv("PET_PLAN_NUM").ToString
                txt_PET_auth_num.Text = drv("PET_AUTH_NUM").ToString
                txt_PET_dsp_trans_msg.Text = drv("PET_DSP_TRANS_MSG").ToString
                txt_PET_rcp_trans_msg.Text = drv("PET_RCP_TRANS_MSG").ToString

                txt_PET_cust_cd.Text = drv("PET_CUST_CD").ToString
                txt_PET_date_time.Text = drv("PET_DATE_TIME").ToString
                txt_PET_batch_num.Text = drv("PET_BATCH_NUM").ToString
                txt_PET_seq_num.Text = drv("PET_SEQ_NUM").ToString
                txt_PET_lpsf_indicator.Text = drv("PET_LPSF_INDICATOR").ToString

                txt_SO_employee_code.Text = RetrieveSalesOrderEmployee(txt_PET_ps_invoice.Text)
                txt_SO_employee_name.Text = RetrieveEmployeeName(txt_SO_employee_code.Text)

                Session("ROW_NUM") = drv("ROW_NUM").ToString

                Exit For
            End If
        Next


        Dim s_trans_date As String = txt_PET_date_time.Text
        Dim d_trans_date As Date

        If Not (String.IsNullOrEmpty(Trim(s_trans_date.ToString()))) Then
            d_trans_date = Convert.ToDateTime(s_trans_date).ToString("dd MMM yyyy")
            dateEdit_transaction_date.Date = d_trans_date
        End If

        'Retrieve ARTRN data only if current PET transaction is approved
        If (s_PET_response_code.Equals("A")) Then
            Dim n_PET_amount As Decimal = CDec(txt_PET_amount.Text)
            RetrieveARTRNTransaction(dateEdit_transaction_date.Text, txt_PET_cust_cd.Text, txt_PET_auth_num.Text, n_PET_amount, txt_PET_card_number.Text, txt_PET_ps_invoice.Text)

        End If

        get_addln_msg()

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveARTRNTransaction()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    TODO
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Sub RetrieveARTRNTransaction(p_transDate As String, p_custCode As String, p_authNumber As String, p_amount As String, p_cardNumber As String, p_invoice As String)

        Dim s_errorMessage As String = ""
        Dim s_displayMessage As String = ""

        If (String.IsNullOrEmpty(Trim(p_transDate.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_cardNumber.ToString())) _
            AndAlso String.IsNullOrEmpty(Trim(p_authNumber.ToString())) AndAlso String.IsNullOrEmpty(Trim(p_custCode.ToString()))) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0028
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return
        End If

        'CARD NUMBER must be at least 3 digits long
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString()))) AndAlso p_cardNumber.Length < 4) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0027
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        Dim s_co_cd As String = Session("CO_CD")
        Dim s_employee_code As String = Session("EMP_CD")

        'for debugging

        If Not ((Not (s_co_cd Is Nothing)) AndAlso s_co_cd.isNotEmpty) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_errorMessage = Resources.CustomMessages.MSG0022
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_errorMessage, "LABEL", "ERROR")

            Return
        End If

        '--------------------------------------------------------------------------
        '-- ARTRN data retrieval
        '--------------------------------------------------------------------------
        Dim s_clauseTransDate = "AND a.post_dt = UPPER(:x_transDate) "
        Dim s_clauseCustCode = "AND a.cust_cd = UPPER(:x_custCode) "
        Dim s_clauseAuthNumber = "AND a.app_cd = UPPER(:x_authNumber) "
        Dim s_clauseAmount = "AND a.amt = :x_amount "
        Dim s_clauseInvoice = "AND NVL(a.adj_ivc_cd,SUBSTR(a.ivc_cd,1,11)) = UPPER(:x_invoice) "
        Dim s_clauseCardNumber = "AND SUBSTR(NVL(a.bnk_crd_num, :x_cardNumber ), LENGTH(NVL(a.bnk_crd_num, :x_cardNumber)) - 3, 4) = SUBSTR(:x_cardNumber, LENGTH(:x_cardNumber) - 3, 4) "

        '---------------------------------------------------------------------------------
        '-- ARTRN - all transactions which are tied to the given PET transaction
        SQL = ""
        SQL = SQL & "SELECT a.trn_tp_cd as ARTRN_trans_type, a.ivc_cd as ARTRN_invoice, a.amt as ARTRN_amount, a.adj_ivc_cd as ARTRN_adj_invoice, TO_CHAR(a.post_dt, 'DD-MON-YYYY') as ARTRN_post_date, "
        SQL = SQL & "a.stat_cd as ARTRN_status_code, a.cntr_cd as ARTRN_contract_code, a.csh_dwr_cd as ARTRN_cash_drawer, a.emp_cd_op as ARTRN_employee_code, a.emp_cd_cshr as ARTRN_emp_cd_cshr, "
        SQL = SQL & "a.origin_cd as ARTRN_source, a.des as ARTRN_reference_des, a.mop_cd as ARTRN_mop_cd, a.trn_tp_cd as ARTRN_trans_type, "
        SQL = SQL & "DECODE( TRIM(a.doc_seq_num), '', '', TO_CHAR(a.post_dt,'MMDDY') || NVL(a.pmt_store,origin_store) || a.doc_seq_num ) as ARTRN_receipt_no, "
        SQL = SQL & "'' as EMP_employee_name "
        SQL = SQL & "FROM ar_trn a "

        SQL = SQL & "WHERE a.co_cd = UPPER(:x_co_cd) "

        'add clauses to ARTRN query based on parameters values passed in
        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            SQL = SQL & s_clauseTransDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            SQL = SQL & s_clauseCustCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
            SQL = SQL & s_clauseAuthNumber
        End If

        'take a snapshot of the SQL statement - date, customer and auth#
        Dim SQL_Query4_NO_AmountInvoiceCardNo = SQL

        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            SQL = SQL & s_clauseCardNumber
        End If

        'take a snapshot of the SQL statement - date, customer and auth# and card#
        Dim SQL_Query3_NO_AmountInvoice = SQL

        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            SQL = SQL & s_clauseInvoice
        End If

        'take a snapshot of the SQL statement - date, customer and auth# and card# and invoice
        Dim SQL_Query2_NO_Amount = SQL

        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            SQL = SQL & s_clauseAmount
        End If

        '---------------------------------------------------------------------------------
        SQL = SQL & "ORDER BY ar_trn_pk "
        'order by 9

        'Set SQL object
        objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

        'bind variables based on parameters passed in
        objsql.Parameters.Add(":x_co_cd", OracleType.VarChar)
        objsql.Parameters(":x_co_cd").Value = s_co_cd

        If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
            objsql.Parameters.Add(":x_transDate", OracleType.DateTime)
            objsql.Parameters(":x_transDate").Value = p_transDate
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
            objsql.Parameters.Add(":x_custCode", OracleType.VarChar)
            objsql.Parameters(":x_custCode").Value = p_custCode
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
            objsql.Parameters.Add(":x_authNumber", OracleType.VarChar)
            objsql.Parameters(":x_authNumber").Value = p_authNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
            objsql.Parameters.Add(":x_cardNumber", OracleType.VarChar)
            objsql.Parameters(":x_cardNumber").Value = p_cardNumber
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
            objsql.Parameters.Add(":x_invoice", OracleType.VarChar)
            objsql.Parameters(":x_invoice").Value = p_invoice
        End If
        If (Not (String.IsNullOrEmpty(Trim(p_amount.ToString())))) Then
            objsql.Parameters.Add(":x_amount", OracleType.VarChar)
            objsql.Parameters(":x_amount").Value = p_amount
        End If

        Dim ds As New DataSet
        Dim dv As DataView

        Dim b_dataFound As Boolean = False
        Dim b_useFilteredDataRowView As Boolean = False
        Dim drv_AssociatedARTRNDataRow As DataRowView

        '--------------------------------------------------------------------------------------------------
        '-- QUERY RUN ONE -- find ARTRN transaction(s) that match all data including AMOUNT, INVOICE and CARD#
        '--------------------------------------------------------------------------------------------------
        Try
            With objsql
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
            oAdp.Fill(ds)

            dv = New DataView(ds.Tables(0))

            Dim n_positionCounter As Integer = 1
            Dim n_queryResultsCount As Integer = dv.Count

            If (n_queryResultsCount = 0) Then

                b_dataFound = False

            Else
                b_dataFound = True
                s_displayMessage = lbl_resultsInfo.Text

                If (n_queryResultsCount > 1) Then

                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                    s_errorMessage = String.Format(Resources.CustomMessages.MSG0029, s_errorMessage, n_queryResultsCount.ToString)
                    'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                    drv_AssociatedARTRNDataRow = determineAssociatedARTRNRecord(txt_PET_trans_type.Text, txt_PET_card_type.Text, dv)
                    b_useFilteredDataRowView = True

                End If
            End If

            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        '--------------------------------------------------------------------------------------------------
        '-- QUERY RUN TWO -- remove AMOUNT matching from query and re-query using INVOICE and CARD#
        '--------------------------------------------------------------------------------------------------
        If (Not b_dataFound) Then

            're-execute query without card#
            SQL = SQL_Query2_NO_Amount
            SQL = SQL & "ORDER BY ar_trn_pk "

            'Set SQL object
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            'bind variables based on parameters passed in
            objsql.Parameters.Add(":x_co_cd", OracleType.VarChar)
            objsql.Parameters(":x_co_cd").Value = s_co_cd

            If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
                objsql.Parameters.Add(":x_transDate", OracleType.DateTime)
                objsql.Parameters(":x_transDate").Value = p_transDate
            End If
            If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
                objsql.Parameters.Add(":x_custCode", OracleType.VarChar)
                objsql.Parameters(":x_custCode").Value = p_custCode
            End If
            If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
                objsql.Parameters.Add(":x_authNumber", OracleType.VarChar)
                objsql.Parameters(":x_authNumber").Value = p_authNumber
            End If
            If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
                objsql.Parameters.Add(":x_cardNumber", OracleType.VarChar)
                objsql.Parameters(":x_cardNumber").Value = p_cardNumber
            End If
            If (Not (String.IsNullOrEmpty(Trim(p_invoice.ToString())))) Then
                objsql.Parameters.Add(":x_invoice", OracleType.VarChar)
                objsql.Parameters(":x_invoice").Value = p_invoice
            End If

            Try
                With objsql
                    .Connection = conn
                    .CommandText = SQL
                End With

                conn.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
                oAdp.Fill(ds)

                dv = New DataView(ds.Tables(0))

                Dim n_queryResultsCount As Integer = dv.Count

                If (n_queryResultsCount = 0) Then

                    b_dataFound = False

                Else

                    b_dataFound = True
                    s_displayMessage = lbl_resultsInfo.Text

                    If (n_queryResultsCount > 1) Then

                        'for debugging

                        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                        s_displayMessage = String.Format(Resources.CustomMessages.MSG0050, s_displayMessage, n_queryResultsCount.ToString)
                        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                        DisplayMessage(s_displayMessage, "LABEL", "WARNING")

                    End If
                End If

                conn.Close()

            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If

        '--------------------------------------------------------------------------------------------------
        '-- QUERY RUN THREE -- remove INVOICE matching from query and re-query using CARD#
        '--------------------------------------------------------------------------------------------------
        If (Not b_dataFound) Then

            're-execute query without card#
            SQL = SQL_Query3_NO_AmountInvoice
            SQL = SQL & "ORDER BY ar_trn_pk "

            'Set SQL object
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            'bind variables based on parameters passed in
            objsql.Parameters.Add(":x_co_cd", OracleType.VarChar)
            objsql.Parameters(":x_co_cd").Value = s_co_cd

            If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
                objsql.Parameters.Add(":x_transDate", OracleType.DateTime)
                objsql.Parameters(":x_transDate").Value = p_transDate
            End If
            If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
                objsql.Parameters.Add(":x_custCode", OracleType.VarChar)
                objsql.Parameters(":x_custCode").Value = p_custCode
            End If
            If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
                objsql.Parameters.Add(":x_authNumber", OracleType.VarChar)
                objsql.Parameters(":x_authNumber").Value = p_authNumber
            End If
            If (Not (String.IsNullOrEmpty(Trim(p_cardNumber.ToString())))) Then
                objsql.Parameters.Add(":x_cardNumber", OracleType.VarChar)
                objsql.Parameters(":x_cardNumber").Value = p_cardNumber
            End If

            Try
                With objsql
                    .Connection = conn
                    .CommandText = SQL
                End With

                conn.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
                oAdp.Fill(ds)

                dv = New DataView(ds.Tables(0))

                Dim n_queryResultsCount As Integer = dv.Count

                If (n_queryResultsCount = 0) Then

                    b_dataFound = False

                Else
                    b_dataFound = True

                    s_displayMessage = lbl_resultsInfo.Text

                    If (n_queryResultsCount > 1) Then

                        'for debugging

                        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                        s_displayMessage = String.Format(Resources.CustomMessages.MSG0051, s_displayMessage, n_queryResultsCount.ToString)
                        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                        DisplayMessage(s_displayMessage, "LABEL", "WARNING")

                    End If
                End If

                conn.Close()

            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If

        '--------------------------------------------------------------------------------------------------
        '-- QUERY RUN FOUR -- remove CARD# matching from query and re-query using the BASE query
        '--------------------------------------------------------------------------------------------------
        If (Not b_dataFound) Then

            're-execute query without card#
            SQL = SQL_Query4_NO_AmountInvoiceCardNo
            SQL = SQL & "ORDER BY ar_trn_pk "

            'Set SQL object
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            'bind variables based on parameters passed in
            objsql.Parameters.Add(":x_co_cd", OracleType.VarChar)
            objsql.Parameters(":x_co_cd").Value = s_co_cd

            If (Not (String.IsNullOrEmpty(Trim(p_transDate.ToString())))) Then
                objsql.Parameters.Add(":x_transDate", OracleType.DateTime)
                objsql.Parameters(":x_transDate").Value = p_transDate
            End If
            If (Not (String.IsNullOrEmpty(Trim(p_custCode.ToString())))) Then
                objsql.Parameters.Add(":x_custCode", OracleType.VarChar)
                objsql.Parameters(":x_custCode").Value = p_custCode
            End If
            If (Not (String.IsNullOrEmpty(Trim(p_authNumber.ToString())))) Then
                objsql.Parameters.Add(":x_authNumber", OracleType.VarChar)
                objsql.Parameters(":x_authNumber").Value = p_authNumber
            End If

            Try
                With objsql
                    .Connection = conn
                    .CommandText = SQL
                End With

                conn.Open()
                Dim oAdp = DisposablesManager.BuildOracleDataAdapter(objsql)
                oAdp.Fill(ds)

                dv = New DataView(ds.Tables(0))

                Dim n_queryResultsCount As Integer = dv.Count

                If (n_queryResultsCount = 0) Then

                    b_dataFound = False

                Else
                    b_dataFound = True

                    s_displayMessage = lbl_resultsInfo.Text

                    If (n_queryResultsCount > 1) Then

                        'for debugging

                        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
                        s_displayMessage = String.Format(Resources.CustomMessages.MSG0052, s_displayMessage, n_queryResultsCount.ToString)
                        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

                        DisplayMessage(s_displayMessage, "LABEL", "WARNING")

                    End If
                End If

                conn.Close()

            Catch ex As Exception
                conn.Close()
                Throw
            End Try

        End If

        If (b_dataFound) Then
            'bind dataview data to fields on screen based on user's position in the data (in this case the first record)

            Dim drv_useThisDataRowView As DataRowView
            If (b_useFilteredDataRowView) Then
                drv_useThisDataRowView = drv_AssociatedARTRNDataRow
            Else
                For Each drv As DataRowView In dv
                    drv_useThisDataRowView = drv
                    Exit For
                Next
            End If

            txt_ARTRN_trans_type.Text = drv_useThisDataRowView("ARTRN_TRANS_TYPE").ToString
            txt_ARTRN_invoice.Text = drv_useThisDataRowView("ARTRN_INVOICE").ToString

            txt_ARTRN_amount.Text = ""

            If (Not (String.IsNullOrEmpty(Trim(drv_useThisDataRowView("ARTRN_AMOUNT").ToString())))) Then
                txt_ARTRN_amount.Text = FormatCurrency(drv_useThisDataRowView("ARTRN_AMOUNT").ToString, 2)
            End If

            txt_ARTRN_adj_invoice.Text = drv_useThisDataRowView("ARTRN_ADJ_INVOICE").ToString
            txt_ARTRN_contract_code.Text = drv_useThisDataRowView("ARTRN_CONTRACT_CODE").ToString
            txt_ARTRN_cash_drawer.Text = drv_useThisDataRowView("ARTRN_CASH_DRAWER").ToString
            txt_ARTRN_status_code.Text = drv_useThisDataRowView("ARTRN_STATUS_CODE").ToString
            txt_ARTRN_mop_cd.Text = drv_useThisDataRowView("ARTRN_MOP_CD").ToString
            txt_ARTRN_source.Text = drv_useThisDataRowView("ARTRN_SOURCE").ToString
            txt_ARTRN_reference_des.Text = drv_useThisDataRowView("ARTRN_REFERENCE_DES").ToString
            txt_ARTRN_post_date.Text = drv_useThisDataRowView("ARTRN_POST_DATE").ToString
            txt_ARTRN_employee_code.Text = drv_useThisDataRowView("ARTRN_EMPLOYEE_CODE").ToString

            txt_ARTRN_receipt_no.Text = drv_useThisDataRowView("ARTRN_RECEIPT_NO").ToString

            txt_ARTRN_employee_name.Text = drv_useThisDataRowView("EMP_EMPLOYEE_NAME").ToString

            txt_ARTRN_employee_name.Text = RetrieveEmployeeName(drv_useThisDataRowView("ARTRN_EMPLOYEE_CODE").ToString)

        End If

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           determineAssociatedARTRNRecord()
    '   Created by:     John Melenko
    '   Date:           Jun2015
    '   Description:    TODO
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function determineAssociatedARTRNRecord(p_PET_trans_type As String, p_PET_card_type As String, p_ARTRN_dataview As DataView) As DataRowView

        Dim drv_resultDataRowView As DataRowView
        Dim s_PET_trans_type As String = p_PET_trans_type.ToUpper
        Dim s_PET_card_type As String = p_PET_card_type.ToUpper
        Dim n_queryCount As Integer = p_ARTRN_dataview.Count

        For Each drv As DataRowView In p_ARTRN_dataview
            drv_resultDataRowView = drv
            Exit For
        Next

        '----------------------------

        If (n_queryCount.Equals(1)) Then
            For Each drv As DataRowView In p_ARTRN_dataview
                drv_resultDataRowView = drv
            Next
            Return drv_resultDataRowView
        End If

        If (String.IsNullOrEmpty(Trim(s_PET_trans_type.ToString())) AndAlso s_PET_card_type.Equals("TDF")) Then

            'find p_ARTRN_dataview.trn_tp_cd = BCP or BCR and pass back that datarow

            p_ARTRN_dataview.RowFilter = "ARTRN_TRANS_TYPE IN ( 'BCP', 'BCR')"
            n_queryCount = p_ARTRN_dataview.Count
            For Each drv As DataRowView In p_ARTRN_dataview
                drv_resultDataRowView = drv
                Exit For
            Next

        ElseIf (s_PET_trans_type.Contains("PURCH")) Then

            'find p_ARTRN_dataview.trn_tp_cd = DEP or PMT and pass back that datarow

            p_ARTRN_dataview.RowFilter = "ARTRN_TRANS_TYPE IN ( 'DEP', 'PMT')"
            n_queryCount = p_ARTRN_dataview.Count
            For Each drv As DataRowView In p_ARTRN_dataview
                drv_resultDataRowView = drv
                Exit For
            Next

        ElseIf (s_PET_trans_type.Contains("REFUND")) Then

            'find p_ARTRN_dataview.trn_tp_cd = R and pass back that datarow

            p_ARTRN_dataview.RowFilter = "ARTRN_TRANS_TYPE = 'R'"
            n_queryCount = p_ARTRN_dataview.Count
            For Each drv As DataRowView In p_ARTRN_dataview
                drv_resultDataRowView = drv
                Exit For
            Next

        ElseIf (s_PET_trans_type.Contains("AMOUNT") AndAlso Not s_PET_trans_type.Contains("AMOUNT DPF")) Then

            'find p_ARTRN_dataview.trn_tp_cd = GCS and pass back that datarow

            p_ARTRN_dataview.RowFilter = "ARTRN_TRANS_TYPE = 'GCS'"
            n_queryCount = p_ARTRN_dataview.Count
            For Each drv As DataRowView In p_ARTRN_dataview
                drv_resultDataRowView = drv
                Exit For
            Next

        ElseIf (s_PET_trans_type.Contains("AMT REDEEMED")) Then

            'find p_ARTRN_dataview.trn_tp_cd = DEP or PMT and pass back that datarow

            p_ARTRN_dataview.RowFilter = "ARTRN_TRANS_TYPE IN ( 'DEP', 'PMT')"
            n_queryCount = p_ARTRN_dataview.Count
            For Each drv As DataRowView In p_ARTRN_dataview
                drv_resultDataRowView = drv
                Exit For
            Next

        ElseIf (s_PET_trans_type.Contains("BALANCE") Or s_PET_trans_type.Contains("NOUVEAU SOLDE")) Then

            'find p_ARTRN_dataview.trn_tp_cd = R and pass back that datarow

            p_ARTRN_dataview.RowFilter = "ARTRN_TRANS_TYPE = 'R'"
            n_queryCount = p_ARTRN_dataview.Count
            For Each drv As DataRowView In p_ARTRN_dataview
                drv_resultDataRowView = drv
                Exit For
            Next

        ElseIf (s_PET_trans_type.Contains("CORR")) Then

            'find p_ARTRN_dataview.trn_tp_cd = PMT or R and pass back that datarow, if more than one, use first found

            p_ARTRN_dataview.RowFilter = "ARTRN_TRANS_TYPE IN ( 'PMT', 'R')"
            n_queryCount = p_ARTRN_dataview.Count
            For Each drv As DataRowView In p_ARTRN_dataview
                drv_resultDataRowView = drv
                Exit For
            Next

        ElseIf (s_PET_trans_type.Contains("TEL")) Then 'OR "TEL+EMAIL"

            'find p_ARTRN_dataview.trn_tp_cd = DEP, PMT or R and pass back that datarow, if more than one, use first found

            p_ARTRN_dataview.RowFilter = "ARTRN_TRANS_TYPE IN ( 'DEP', 'PMT', 'R')"
            n_queryCount = p_ARTRN_dataview.Count
            For Each drv As DataRowView In p_ARTRN_dataview
                drv_resultDataRowView = drv
                Exit For
            Next

        End If

        Dim s_displayMessage As String = lbl_resultsInfo.Text

        If (n_queryCount > 1) Then

            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
            s_displayMessage = String.Format(Resources.CustomMessages.MSG0029, s_displayMessage, n_queryCount.ToString)
            'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

            DisplayMessage(s_displayMessage, "LABEL", "WARNING")
        End If

        Return drv_resultDataRowView

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveSalesOrderEmployee()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which TODO
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function RetrieveSalesOrderEmployee(p_invoice As String) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim s_result As String = ""

        If (Not (p_invoice Is Nothing) AndAlso p_invoice.isNotEmpty) Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand


            'JAM 17MAR2015 (v2.0.5): Jacquie requests that EMP_CD_OP be changed to EMP_CD_KEYER
            'SQL = "SELECT s.emp_cd_op as SO_emp_cd_op "
            SQL = "SELECT s.emp_cd_keyer as SO_emp_cd_keyer "
            SQL = SQL & "FROM so s "
            SQL = SQL & "WHERE s.del_doc_num = UPPER(:x_invoice) "
            'JAM 17MAR2015 (v2.0.5) (END)

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_invoice", OracleType.VarChar)
            objsql.Parameters(":x_invoice").Value = p_invoice

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then
                    s_result = Mydatareader.Item("SO_EMP_CD_KEYER").ToString
                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

        End If

        Return s_result

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           RetrieveEmployeeName()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Procedure which retrieves the employee full name based on employee code
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Private Function RetrieveEmployeeName(p_employeeCode As String) As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim SQL As String
        Dim s_result As String = ""

        If (Not (p_employeeCode Is Nothing) AndAlso p_employeeCode.isNotEmpty) Then

            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            SQL = "SELECT e.fname || ' ' || e.lname as EMP_employee_name "
            SQL = SQL & "FROM emp e "
            SQL = SQL & "WHERE e.emp_cd = UPPER(:x_employeeCode) "

            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(SQL, conn)

            objsql.Parameters.Add(":x_employeeCode", OracleType.VarChar)
            objsql.Parameters(":x_employeeCode").Value = p_employeeCode

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then
                    s_result = Mydatareader.Item("EMP_EMPLOYEE_NAME").ToString
                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()

        End If

        Return s_result

    End Function

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           PageButtonClick()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    
    '   Called by:      TODO
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)

        Dim strArg As String
        strArg = sender.CommandArgument

        Dim ds As DataSet = Session("ds_queryResults")
        Dim dv = New DataView(ds.Tables(0))
        Dim n_positionCounter As Integer = Session("POSITION_INDEX")
        Dim n_queryResultsCount As Integer = dv.Count

        Select Case strArg
            Case "Next"
                If (n_positionCounter < n_queryResultsCount) Then
                    n_positionCounter = n_positionCounter + 1
                End If

            Case "Prev"
                If (n_positionCounter > 1) Then
                    n_positionCounter = n_positionCounter - 1
                End If

            Case "Last"
                n_positionCounter = n_queryResultsCount

            Case "First"
                n_positionCounter = 1

        End Select

        Session("POSITION_INDEX") = n_positionCounter
        BindPETDataViewData()

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           AlertMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display alert messages
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub AlertMessage(p_messageText)
        MsgBox(p_messageText, MsgBoxStyle.Exclamation)
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           DisplayMessage()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    Generic procedure used to display messages - can toggle between a label or message box
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Public Sub DisplayMessage(p_messageText, p_messageType, p_msgBoxStyle)

        Dim color_fontColor As Color
        Dim msgBoxStyle As MsgBoxStyle

        If (p_msgBoxStyle = "ERROR") Then
            color_fontColor = Color.Red
            msgBoxStyle = msgBoxStyle.Critical
        ElseIf (p_msgBoxStyle = "WARNING") Then
            color_fontColor = Color.Orange
            msgBoxStyle = msgBoxStyle.Exclamation
        ElseIf (p_msgBoxStyle = "INFO") Then
            color_fontColor = Color.Green
            msgBoxStyle = msgBoxStyle.Information
        End If

        If (p_messageType = "LABEL") Then
            lbl_resultsInfo.Text = p_messageText
            lbl_resultsInfo.ForeColor = color_fontColor
            lbl_resultsInfo.Visible = True
        ElseIf (p_messageType = "MSGBOX") Then
            MsgBox(p_messageText, msgBoxStyle)
        End If
    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_Load()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form - minor changes
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            PopulateStoreCodes()


            'A query has already been initiated
            If Request("selected_row") = "Y" Then

                'for debugging

                Session("ROW_NUM") = Request("ROW_NUM")
                Session("POSITION_INDEX") = Request("POSITION_INDEX")

                Session("PSQGRID_INDEX") = 1 'reset the gridview index variable just in case
                BindPETDataViewData()

                Session("ROW_NUM") = "" 'reset the row number

            Else

                'JAM added 20Oct2014
                'Dim d_currdate As Date = dateEdit.Date
                '                If (IfNull(d_currdate.Date)) Then
                dateEdit_transaction_date.Date = Today.Date
                'End If

                'Code Modified by Kumaran on 30-Jul-2015 - To allow transaction date got back as far as Feb 19,2015 to query the data (V2.2) -- Starts
                'Dim d_mindate As Date = Date.Today
                'd_mindate = d_mindate.AddDays(-90)
                Dim d_mindate As Date = New Date(2015, 2, 19)
                dateEdit_transaction_date.MinDate = d_mindate
                'Code Modified by Kumaran on 30-Jul-2015 - To allow transaction date got back as far as Feb 19,2015 to query the data (V2.2) -- Ends


                dateEdit_transaction_date.TimeSectionProperties.Visible = False
                dateEdit_transaction_date.UseMaskBehavior = True
                dateEdit_transaction_date.EditFormatString = "dd MMM yyyy"
                dateEdit_transaction_date.DisplayFormatString = "dd MMM yyyy"

            End If
            sy_printer_drp_Populate()
        End If

        'JAM Jan2015 - set up fields which are editable and readonly

        lbl_versionInfo.Text = gs_version

        txt_terminal_ipaddr.ReadOnly = True
        txt_pc_name.ReadOnly = True
        txt_company_code.ReadOnly = True

        txt_PET_trans_type.ReadOnly = True
        txt_PET_entry_type.ReadOnly = True

        txt_PET_expiry_date.ReadOnly = True

        txt_PET_plan_num.ReadOnly = True
        txt_PET_dsp_trans_msg.ReadOnly = True
        txt_PET_date_time.ReadOnly = True
        txt_PET_batch_num.ReadOnly = True
        txt_PET_seq_num.ReadOnly = True
        txt_PET_lpsf_indicator.ReadOnly = True

        txt_ARTRN_trans_type.ReadOnly = True
        txt_ARTRN_invoice.ReadOnly = True
        txt_ARTRN_amount.ReadOnly = True
        txt_ARTRN_adj_invoice.ReadOnly = True
        txt_ARTRN_contract_code.ReadOnly = True
        txt_ARTRN_cash_drawer.ReadOnly = True

        txt_ARTRN_status_code.ReadOnly = True
        txt_ARTRN_mop_cd.ReadOnly = True
        txt_ARTRN_source.ReadOnly = True
        txt_ARTRN_reference_des.ReadOnly = True
        txt_ARTRN_post_date.ReadOnly = True
        txt_ARTRN_employee_code.ReadOnly = True
        txt_ARTRN_employee_name.ReadOnly = True
        txt_SO_employee_code.ReadOnly = True
        txt_SO_employee_name.ReadOnly = True

        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts

        lbl_terminal_id.ToolTip = GetLocalResourceObject("tool_terminal_id.Text").ToString()
        lbl_transaction_date.ToolTip = GetLocalResourceObject("tool__transaction_date.Text").ToString()
        lbl_store_cd.ToolTip = GetLocalResourceObject("tool_store_cd.Text").ToString()
        lbl_PET_trans_type.ToolTip = GetLocalResourceObject("tool_PET_trans_type.Text").ToString()
        lbl_PET_ps_invoice.ToolTip = GetLocalResourceObject("tool_PET_ps_invoice.Text").ToString()
        lbl_PET_amount.ToolTip = GetLocalResourceObject("tool_PET_amount.Text").ToString()
        lbl_PET_card_number.ToolTip = GetLocalResourceObject("tool_PET_card_number.Text").ToString()
        lbl_PET_card_type.ToolTip = GetLocalResourceObject("tool_PET_card_type.Text").ToString()
        lbl_PET_auth_num.ToolTip = GetLocalResourceObject("tool_PET_auth_num.Text").ToString()
        lbl_PET_cust_cd.ToolTip = GetLocalResourceObject("tool_PET_cust_cd.Text").ToString()
        btn_SpecialAttention.ToolTip = GetLocalResourceObject("tool_btn_SpecialAttention.Text").ToString()
        btn_ToggleToGridResults.ToolTip = GetLocalResourceObject("tool_btn_ToggleToGridResults.Text").ToString()

        'Code Modified by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

        'Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015 - Starts
        'Tooltip for Button's
        'Go to first page
        btnFirst.ToolTip = GetLocalResourceObject(("tool_btnFirst.Text").ToString())
        'Go to the last page
        btnLast.ToolTip = GetLocalResourceObject(("tool_btnLast.Text").ToString())
        'Go to the previous page
        btnPrev.ToolTip = GetLocalResourceObject(("tool_btnPrev.Text").ToString())
        'Go to the next page
        btnNext.ToolTip = GetLocalResourceObject(("tool_btnNext.Text").ToString())
        'Code Added by Kumaran for POS+ French Conversion on 01-Sep-2015 - Ends

    End Sub

    'JAM added 20Oct2014
    'Protected Function GetFormatString(ByVal value As Object) As String
    '    If value Is Nothing Then
    '        Return String.Empty
    '    Else
    '        Return value.ToString()
    '    End If
    'End Function
    'JAM added 20Oct2014

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Clear_Click()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        Response.Redirect("PaymentServerQuery.aspx")

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_Search_Click()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click

        RetrievePETTransactions(txt_terminal_id.Text, dateEdit_transaction_date.Text, txt_store_cd.Text, txt_PET_trans_type.Text, txt_PET_ps_invoice.Text, txt_PET_amount.Text, txt_PET_card_number.Text, txt_PET_card_type.Text, txt_PET_auth_num.Text, txt_PET_cust_cd.Text)

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_SpecialAttention_Click()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    --
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_SpecialAttention_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SpecialAttention.Click

        SpecialQueryApprovedTransMissingARTRN(txt_terminal_id.Text, dateEdit_transaction_date.Text, txt_store_cd.Text)

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           btn_ToggleToGridResults_Click()
    '   Created by:     John Melenko
    '   Date:           Jan2015
    '   Description:    --
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btn_ToggleToGridResults_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ToggleToGridResults.Click

        Response.Redirect("PaymentServerQueryGridResults.aspx")

    End Sub

    '------------------------------------------------------------------------------------------------------------------------------------
    '   Name:           Page_PreInit()
    '   Created by:     PRE-EXISTING
    '   Date:           Jan2015
    '   Description:    Copied from Franchise form and kept same functionality
    '   Called by:      --
    '------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub btn_print_receipt_Click(sender As Object, e As EventArgs) Handles btn_print_receipt.Click
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim receiptNo As String

        If String.IsNullOrEmpty(txt_ARTRN_receipt_no.Text) And
            InStr(txt_ARTRN_invoice.Text, "VISAD") = 0 And
            InStr(txt_ARTRN_invoice.Text, "BRICK") = 0 And
            txt_ARTRN_trans_type.Text <> "GCS" Then
            DisplayMessage(Resources.CustomMessages.MSG0081, "LABEL", "ERROR")
            Return
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        If txt_ARTRN_trans_type.Text = "GCS" Then
            Dim seq_found As Integer = 0
            myCMD.Connection = objConnection
            myCMD.CommandText = "select count(*) from ar_trn at where at.ivc_cd = :p_ivc_cd and at.doc_seq_num is not null"
            myCMD.CommandType = CommandType.Text
            myCMD.Parameters.Add(New OracleParameter("p_ivc_cd", OracleType.VarChar)).Value = txt_ARTRN_invoice.Text
            objConnection.Open()
            Try
                seq_found = myCMD.ExecuteScalar()
                objConnection.Close()
            Catch x
                objConnection.Close()
            End Try

            If seq_found = 0 Then
                DisplayMessage(Resources.CustomMessages.MSG0081, "LABEL", "ERROR")
                Return
            End If
        End If


        myCMD = DisposablesManager.BuildOracleCommand()

        Dim lang As String = "E"
        Dim v_ufm As String = "LPO"
        Dim cult As String = UICulture
        Dim co_cd As String = Session("CO_CD")
        Dim grpCd As String = ""
        If isNotEmpty(co_cd) Then
            grpCd = LeonsBiz.GetGroupCode(co_cd)
        End If

        If txt_ARTRN_trans_type.Text = "GCS" Or txt_ARTRN_source.Text = "PGC" Then
            If InStr(cult.ToLower, "fr") >= 1 Then
                v_ufm = "GCF"
            Else
                v_ufm = "GCE"
            End If
            receiptNo = txt_ARTRN_invoice.Text
        ElseIf (InStr(txt_ARTRN_invoice.Text, "VISAD") > 0 Or
                    InStr(txt_ARTRN_invoice.Text, "BRICK") > 0) And grpCd = "BRK" Then

            If InStr(cult.ToLower, "fr") >= 1 Then
                lang = "F"
            End If

            'Code Added by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Starts
            If lang.Equals("F") Then
                v_ufm = "BFP"
            Else
                v_ufm = "BCP"
            End If
            'Code Added by Kumaran for Payment Server Query - Brick Card And VisaD Changes on 27-Nov-2015 - Ends

            receiptNo = txt_PET_ps_invoice.Text
        Else
            If InStr(cult.ToLower, "fr") >= 1 Then
                v_ufm = "PRF"
            End If

            If grpCd = "BRK" Then
                If InStr(cult.ToLower, "fr") >= 1 Then
                    v_ufm = "BFC"
                Else
                    v_ufm = "BRC"
                End If
            End If


            receiptNo = txt_ARTRN_receipt_no.Text
        End If

        Dim PETDateTime As DateTime = Convert.ToDateTime(txt_PET_date_time.Text)
        Dim printer As String = printer_drp.SelectedItem.Value.ToString()

        Dim ReportParam As StringBuilder = New StringBuilder(Session("CO_CD").ToString)
        ReportParam.Append("~")
        ReportParam.Append(txt_PET_cust_cd.Text)
        ReportParam.Append("~~")
        ReportParam.Append(txt_PET_ps_invoice.Text.Substring(5, 2))
        ReportParam.Append("~")
        ReportParam.Append(txt_ARTRN_employee_code.Text)
        ReportParam.Append("~")
        ReportParam.Append(txt_ARTRN_cash_drawer.Text)
        ReportParam.Append("~")
        ReportParam.Append(receiptNo.Substring(7))
        ReportParam.Append("~")
        ReportParam.Append(PETDateTime.ToString("dd-MMM-yy"))
        ReportParam.Append("~")
        ReportParam.Append(PETDateTime.ToString("HH:mm:ss"))
        ReportParam.Append("~0~0~0~0~0~0~POA~o~")
        ReportParam.Append(v_ufm)
        ReportParam.Append("~@")
        ReportParam.Append(printer)

        'Check for duplicate in BQ table Jul 25
        Try
            Dim dupl As String = get_chk_dup_arprt(ReportParam.ToString())
            If dupl = "Y" Then
                DisplayMessage(Resources.CustomMessages.MSG0082, "LABEL", "ERROR")
                Exit Sub
            End If
        Catch x
            DisplayMessage(Resources.CustomMessages.MSG0062, "LABEL", "ERROR")
            Exit Sub
        End Try
        ' End duplicate check Jul 25

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_iprt.invoice_print_pos"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Clear()
        myCMD.Parameters.Add(New OracleParameter("doc_i", OracleType.Char)).Value = txt_ARTRN_invoice.Text
        myCMD.Parameters.Add(New OracleParameter("emp_cd_i", OracleType.Char)).Value = Session("emp_cd").ToString()
        myCMD.Parameters.Add(New OracleParameter("ord_tp_cd_i", OracleType.Char)).Value = "SAL" ' This parameter not used inside package
        myCMD.Parameters.Add(New OracleParameter("pu_del_i", OracleType.Char)).Value = "P" ' This parameter not used inside package
        myCMD.Parameters.Add(New OracleParameter("in_param", OracleType.LongVarChar)).Value = ReportParam.ToString()
        Dim prgq_id As OracleParameter = New OracleParameter("prgq_id_o", OracleType.Number, 5, ParameterDirection.Output, True, 0, 0, "prgq_id_o", DataRowVersion.Default, 0)
        myCMD.Parameters.Add(prgq_id)
        myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang ' This parameter not used inside package

        objConnection.Open()
        Try
            myCMD.ExecuteNonQuery()
            objConnection.Close()
            DisplayMessage(Resources.CustomMessages.MSG0082, "LABEL", "INFO")
        Catch x
            objConnection.Close()
            DisplayMessage(Resources.CustomMessages.MSG0062, "LABEL", "ERROR")
        End Try
    End Sub

    Public Sub sy_printer_drp_Populate()

        'sabrina new sub added
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet
        Dim printer_found As Boolean = False
        Dim v_user_init As String = sy_get_emp_init(Session("EMP_CD"))
        Dim v_user_cd As String = Session("EMP_CD")
        Dim v_default_printer As String = sy_get_default_printer_id(v_user_cd) 'sabrina emp_cd to get default printer

        If IsDBNull(v_default_printer) = True Or v_default_printer Is Nothing Or v_default_printer = "" Then
            v_default_printer = "Printers:"
        End If

        Session("default_prt") = v_default_printer

        sql = "select pi.printer_nm PRINT_Q "
        sql = sql & "from  printer_info pi "
        sql = sql & "where pi.store_cd = (select home_store_cd "
        sql = sql & "from emp "
        sql = sql & "where emp_init = :v_user_init) "
        sql = sql & "and   pi.printer_tp = 'INVOICE'"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

        objSql.Parameters.Add(":v_user_init", OracleType.VarChar)
        objSql.Parameters(":v_user_init").Value = v_user_init

        oAdp.Fill(ds)

        printer_drp.Items.Clear()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                printer_found = True
                printer_drp.DataSource = ds
                printer_drp.ValueField = "PRINT_Q"
                printer_drp.TextField = "PRINT_Q"
                printer_drp.DataBind()
            End If

            printer_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(v_default_printer))
            printer_drp.Items.FindByText(v_default_printer).Selected = True
            printer_drp.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception

            MyDataReader.Close()
            conn.Close()
            Throw

        End Try

    End Sub

    Public Function sy_get_default_printer_id(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select c.print_q as PRINT_Q from console c, emp e "
        sql = sql & "where e.print_grp_con_cd = c.con_cd And e.emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("PRINT_Q").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Public Function sy_get_emp_init(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select emp_init as EMP_INIT from emp where emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("EMP_INIT").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Nov 2015
    ''' Description    : To display additional message in popup box
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub get_addln_msg()
        lbl_PET_rcp_trans_msg.Text = ""
        lbl_PET_rcp_trans_msg.Text = txt_PET_rcp_trans_msg.Text
    End Sub

    Public Function get_chk_dup_arprt(ByVal p_param As String) As String
        'Daniela new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select STD_CHK_BQ. chk_dup_ARPRT('" & p_param & "') dupl from dual"
     
        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("dupl").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Public Function GetCardNumber(ByVal p_drv As DataRowView) As String
        'Oct(13, 2017 - mariam - #3216 & 3324)
        '        Mask card no for - visa, master card, american express
        '        Gift card - if user has security then display card no else mask it
        '        Debit card and TDF - display card number
        '        for others - if user have security to see then display
        Const SECURITY_GIFTCARD As String = "PSQPOSGC"
        Const SECURITY_POSPSQCARD As String = "POSPSQCARD"

        Dim ccType As String = String.Empty
        Dim ccNo As String = String.Empty
        Dim ccNoMasked As String = String.Empty
        Dim cardNum As String = String.Empty

        ccType = p_drv("PET_CARD_TYPE").ToString
        ccNo = p_drv("PET_CARD_NUMBER").ToString
        ccNoMasked = p_drv("PET_CARD_NUMBER_MASKED").ToString

        If ccNo.Length > 4 Then
            If InStr(ccType, "VISA") > 0 Or InStr(ccType, "AMERICAN EXPRESS") > 0 Or InStr(ccType, "MASTER CARD") > 0 Then
                cardNum = ccNoMasked
            ElseIf InStr(ccType, "GIFT CARD") > 0 Then
                cardNum = If(SecurityUtils.hasSecurity(SECURITY_GIFTCARD, Session("emp_init")) = True, ccNo, ccNoMasked)
            ElseIf InStr(ccType, "DEBIT") > 0 Or InStr(ccType, "TDF") > 0 Then
                cardNum = ccNo
            Else
                If SecurityUtils.hasSecurity(SECURITY_POSPSQCARD, Session("emp_init")) Then
                    cardNum = ccNo
                Else
                    'if NOT Debit card nor TDF or user have NO security for other cards then MASK card no
                    cardNum = ccNo.Substring(ccNo.Length - 4).PadLeft(ccNo.Length, "X")
                End If
            End If
        Else
            cardNum = ccNo
        End If
        Return cardNum
    End Function
End Class