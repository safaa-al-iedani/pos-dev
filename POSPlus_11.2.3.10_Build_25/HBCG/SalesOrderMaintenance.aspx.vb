Imports System.Collections.Generic
Imports System.Data
Imports System.Data.OracleClient
Imports System.Linq
Imports System.Web.UI.WebControls
Imports System.Math
Imports HBCG_Utils
Imports InventoryUtils
Imports SalesUtils
Imports TaxUtils
Imports SD_Utils
Imports jda.mm.order.DataTransferClass

Imports Renci.SshNet   'lucy add the 5 lines
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp

Imports System.Globalization 'Daniela

Partial Class SalesOrderMaintenance
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    Private thePmtBiz As PaymentBiz = New PaymentBiz()
    Private theInvBiz As InventoryBiz = New InventoryBiz()
    Private theTMBiz As TransportationBiz = New TransportationBiz()
    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz() 'lucy
    Dim splitErrMsg As StringBuilder = New StringBuilder()
    Dim isRsrvLnCalled As Boolean = False
    Dim SecurityCache As New System.Collections.Generic.Dictionary(Of String, Boolean)
    Dim isRedirectedFromSORW As Boolean = False
    Dim fab_found As Boolean
    Dim pt_cnt As String
    Dim GM_Sec As String = ""
    Dim slsp_ds As New DataSet
    Dim salesInfo As Hashtable = New Hashtable()
    Dim accessInfo As New AccessControlBiz()
    Private Const thisProgramName As String = "SOM+"
    Private Const DelChargesApproval As String = "DELCHARGESAPPROVAL"
    Private Const ORDERSAVE = "ORDERSAVE"
    Private Class SoLnGrid
        Public Const DEL_DOC_LN_NUM As Integer = 0
        Public Const ITM_CD As Integer = 1
        Public Const VE_CD As Integer = 2
        Public Const VSN_COL As Integer = 3 ' includes image, vendor, serial number, po, ist, warr, leave-in_carton
        Public Const SKU_DET As Integer = 4
        ' 5, 6, 7, 8 are only enabled if INV type 
        Public Const INV_LOOKUP As Integer = 5
        Public Const STORE_CD As Integer = 6
        Public Const LOC_CD As Integer = 7
        Public Const FAB_CHKBOX As Integer = 8  ' ref'd as  ROW_ID ???
        Public Const WARR_CHKBOX As Integer = 9
        Public Const UNIT_PRC As Integer = 10
        Public Const QTY As Integer = 11
        Public Const DEL_CHKBOX As Integer = 12 ' and add button
        Public Const ITM_TP_CD As Integer = 13
        Public Const TREATABLE As Integer = 14
        Public Const TREATED_BY_ITM As Integer = 15  ' this is treated by itm_cd or nbsp
        Public Const VOID_FLAG As Integer = 16   ' checked for finding IST line
        Public Const NEW_LINE As Integer = 17  ' compared written dt to SHU date and  ???
        Public Const ADDON_WR_DT As Integer = 18   ' this is a date and compared to SHU - if before SHU, then price, qty enabled - seems backwards
        Public Const SERIAL_TP As Integer = 19
        Public Const OUT_ID As Integer = 20
        Public Const OUT_CD As Integer = 21
        Public Const INVENTORY As Integer = 22
        Public Const LV_IN_CARTON As Integer = 23
        Public Const CUST_TAX_CHG As Integer = 24  'this appears to be a tax on the line (total, not unit) - cust_tax_chg
        Public Const LN_SLSP1 As Integer = 25
        Public Const LN_SLSP2 As Integer = 26
        Public Const LN_SLSP_PCT1 As Integer = 27
        Public Const LN_SLSP_PCT2 As Integer = 28
        Public Const WARR_BY_SKU As Integer = 29
        Public Const WARRANTABLE As Integer = 30
        Public Const WARR_BY_LN As Integer = 31
        Public Const WARR_BY_DOC_NUM As Integer = 32
        Public Const INV_PICK_VOID As Integer = 33
        Public Const MAX_CRM_QTY As Integer = 34
        Public Const MAX_RET_PRC As Integer = 35
        Public Const ORIG_SO_LN_NUM As Integer = 36
        Public Const RES_ID As Integer = 37
        Public Const IsStoreFromInvXref As Integer = 38
    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Daniela start
        If Not IsPostBack Then
            If isNotEmpty(Session("emp_cd")) Then
                If isEmpty(Session("emp_init")) Then
                    Session("emp_init") = LeonsBiz.GetEmpInit(Session("emp_cd"))
                End If
            Else
                ClearCache()    'mm-sep 20,2016 - backbutton concern
                Response.Redirect("Login.aspx")
            End If
            If Request("query_returned") = "Y" Then
                sy_printer_drp_Populate()
            End If
        End If
        ' Daniela end

        Dim v_brk_user As String   'lucy, req-214, nov-2019
        v_brk_user = is_brk_user()
        If v_brk_user = "Y" Then
            btn_add_spc.Visible = True
        Else
            btn_add_spc.Visible = False
        End If
        If (Request.ServerVariables("HTTP_REFERER")) IsNot Nothing Then
            ' Daniela Oct 28 prevent OutOfMemory
            Session("ORD_TBL") = Nothing
            Dim previousPage() As String = Request.ServerVariables("HTTP_REFERER").Trim().Replace("\", "/").Split("/")
            previousPage = previousPage(previousPage.Length - 1).ToUpper().Trim().Split("?")
            If (previousPage(0).ToUpper().Trim().Equals("SALESREWRITE.ASPX")) AndAlso Not Page.IsPostBack AndAlso Session("Redirected") = Nothing Then

                isRedirectedFromSORW = True
            End If
        End If
        If Not String.IsNullOrEmpty(Session("hidUpdLnNos")) Then
            hidUpdLnNos.Value = Session("hidUpdLnNos")
        End If
        If Request.Form("__EVENTTARGET") = "SummaryPriceChangeApproval" Then
            PromptForSummaryPriceMgrOverride()
        End If

        '** check and save the modify tax code security in the viewstate, if needed
        If IsNothing(ViewState("CanModifyTax")) OrElse ViewState("CanModifyTax").ToString = String.Empty Then
            ViewState("CanModifyTax") = CheckSecurity(SecurityUtils.OVERRIDE_TAXCD_MGMT, Session("EMP_CD"))
        End If
        'If hidPopupStatus.Value = "Y" Then
        '    ASPxPopupSKU.Visible = False
        '    SKU1.ClearSearchContents()
        '    ViewState("ReplaceInvoked") = False
        'Else
        If Not ViewState("ReplaceInvoked") Is Nothing AndAlso ViewState("ReplaceInvoked") = True Then
            ASPxPopupSKU.Visible = True
        Else
            ASPxPopupSKU.Visible = False
        End If

        If Not IsPostBack Then
            Session("ISTFound") = Nothing
            Session("SOCACHED") = Nothing
            ASPxPopupSKU.Visible = False
            ASPxPopupSKU.ShowOnPageLoad = False

            Dim dsStates As DataSet = Nothing
            dsStates = HBCG_Utils.GetStates()
            With cbo_State
                .DataSource = dsStates
                .DataValueField = "st_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

            cbo_State.Items.Insert(0, " ")
            cbo_State.Items.FindByText(" ").Value = ""
            'initilize the view state before using it to avoid the exception            
            ViewState("MaxAvailDdate") = String.Empty
            'initilize the sessions before using them to avoid any exceptions.
            Session("USR_FLD_1") = String.Empty
            Session("USR_FLD_2") = String.Empty
            Session("USR_FLD_3") = String.Empty
            Session("USR_FLD_4") = String.Empty
            Session("USR_FLD_5") = String.Empty
            Session("IP") = ""

            txt_po.Visible = ("Y" = ConfigurationManager.AppSettings("show_po").ToString)
            lbl_po.Visible = ("Y" = ConfigurationManager.AppSettings("show_po").ToString)
            btnPrintPickReport.Visible = Not ("N" = ConfigurationManager.AppSettings("picr").ToString)

            cbo_void_cause_cd.Enabled = False
            cbo_void_cause_cd.Visible = False
            cbo_related_docs.Visible = False

            ASPxRoundPanel1.Visible = False
            final_store_cd.Enabled = False
            txt_fv_dt.Enabled = False

            If txt_del_doc_num.Text & "" = "" Then
                btn_Save.Enabled = False
                btn_invoice.Enabled = False
                btn_payments.Enabled = False
                btn_Lookup.Visible = True
                tblTaxCd.Visible = False
                txt_del_doc_num.Enabled = "True"
                ' Daniela reprint
                btn_reprint.Enabled = False
            Else
                btn_Save.Enabled = True
                ' Daniela comment
                'btn_invoice.Enabled = True
                tblTaxCd.Visible = True
                ' Daniela reprint
                btn_reprint.Enabled = True
            End If

            lbl_Payments.Text = "0.00"
            lbl_financed.Text = "0.00"
            lblTotal.Text = "0.00"

            'Check for item reservation details
            ' do this if it is not redirected from SORW page
            If Not isRedirectedFromSORW Then

                If Not String.IsNullOrWhiteSpace(Request("ROW_ID")) Then

                    Dim ds As DataSet = Session("DEL_DOC_LN#")
                    Dim dt As DataTable = ds.Tables(0)
                    Dim dr As DataRow
                    'TODO - error here on void query or something - or maybe below going thru rows
                    ' also got above error when going from payments back 'TO SOM' - maybe not a sale, maybe MCR/MDB
                    For Each dr In dt.Rows
                        'finds the matching row that needs to be updated
                        If dr("DEL_DOC_LN#") = Request("ROW_ID") Then
                            If Not String.IsNullOrWhiteSpace(Request("STORE_CD")) Then
                                dr("STORE_CD") = UCase(Request("STORE_CD"))
                            End If
                            If Not String.IsNullOrWhiteSpace(Request("LOC_CD")) Then
                                dr("LOC_CD") = UCase(Request("LOC_CD"))
                            End If
                            If Not String.IsNullOrWhiteSpace(Request("SER_NUM")) Then
                                dr("SERIAL_NUM") = UCase(Request("SER_NUM"))
                            End If
                            If Not String.IsNullOrWhiteSpace(Request("OUT_ID")) Then
                                dr("OUT_ID") = UCase(Request("OUT_ID"))
                            End If
                            If Not String.IsNullOrWhiteSpace(Request("SPIFF")) Then
                                dr("SPIFF") = UCase(Request("SPIFF"))
                            End If
                            If Not String.IsNullOrWhiteSpace(Request("OUT_CD")) Then
                                dr("OUT_CD") = UCase(Request("OUT_CD"))
                            End If
                        End If
                    Next
                    Session("DEL_DOC_LN#") = ds
                    ASPxPageControl1.ActiveTabIndex = 1
                End If
            End If

            PopulateConfirmationCodes()

            Dim doPostBindGrid As Boolean = False
            If Request("query_returned") = "Y" Then

                ViewState("PTS_RF") = theInvBiz.GetPTSRFStatus()
                Initialize()  ' does bindgrid
                doPostBindGrid = True
            End If

            If txt_del_doc_num.Text & "" <> "" AndAlso AppConstants.Order.STATUS_OPEN = txt_Stat_cd.Text Then

                If (AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text AndAlso HasStoreOrLocation()) Then
                    'NOTE: When processing CRMs, reservations can only be made if user
                    ' has choosen to FINALIZE the order, therefore IF at least one line 
                    ' is found to have a STORE or LOCATION, sets the order to FINAL 
                    cbo_stat_cd.SelectedValue = AppConstants.Order.STATUS_FINAL
                    ' MM-7088
                    ' To get the store, location data
                    GetStoreLocationData()
                    doPostBindGrid = True  ' needed to doPostBindGrid
                ElseIf AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text.Trim Then
                    GetStoreLocationData(True)
                End If
                ASPxRoundPanel1.Visible = True
                If cbo_stat_cd.SelectedValue = "C" Then
                    final_store_cd.Enabled = False
                    txt_fv_dt.Enabled = False
                ElseIf AppConstants.Order.STATUS_VOID = cbo_stat_cd.SelectedValue Then
                    If cbo_void_cause_cd.SelectedValue = "" Then PopulateVoidCause()
                    cbo_void_cause_cd.Enabled = True
                    cbo_void_cause_cd.Visible = True
                    final_store_cd.Enabled = True
                    txt_fv_dt.Enabled = True
                Else
                    cbo_void_cause_cd.Enabled = False
                    cbo_void_cause_cd.Visible = False
                    final_store_cd.Enabled = True
                    txt_fv_dt.Enabled = True
                End If
                btn_Save.Enabled = True
            End If

            If lbl_changes_found.Text = "Y" Or Request("line_added") = "TRUE" Then
                btn_payments.Enabled = False
                ' Daniela French
                'lbl_desc.Text = "Payments can only be added after changes are saved"
                lbl_desc.Text = Resources.LibResources.Label391
            End If

            'The lastSeqNum would have been populated above if found that a HEADER discount exists, 
            'if will reapply it, if ANY of the lines added qualify for the discount.
            ' NOTE THAT BINDGRID ABOVE AVOIDED BECAUSE DOING HERE - IF THIS CONDITION CHANGES, CHANGE ABOVE TO MATCH
            If SessVar.LastLnSeqNum.isNotEmpty() AndAlso SessVar.discReCalc.needToReCalc = True Then

                Dim soLnDatSet As DataSet = Session("DEL_DOC_LN#")
                ' Dim soLnTable As DataTable = soLnDatSet.Tables(0)

                ReapplyDiscounts(soLnDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
                Session("DEL_DOC_LN#") = soLnDatSet
                bindgrid()
                doPostBindGrid = True
            End If

            If doPostBindGrid Then

                doPostBindGrid = False
                doAfterBindgrid()
            End If
            txt_del_doc_num.Focus()

        ElseIf lbl_changes_found.Text = "Y" Or Request("line_added") = "TRUE" Then
            btn_payments.Enabled = False
            lbl_desc.Text = Resources.LibResources.Label391
        Else
            If txt_del_doc_num.Text & "" <> "" And Right(Request.Form("__EVENTTARGET"), 16) <> "ASPxPageControl1" Then
                lbl_changes_found.Text = "Y"
                btn_payments.Enabled = False
                lbl_desc.Text = Resources.LibResources.Label391
            End If
        End If

        If IsNumeric(lbl_financed.Text) Then
            btn_addendum.Visible = (CDbl(lbl_financed.Text) > 0)
        End If

        If (Request("referrer") + "").isNotEmpty Then
            Session("referrerToSom") = Request("referrer")
        End If

        If SystemUtils.isNotEmpty(Session("referrerToSom")) Then
            btn_return.Visible = True
            btn_return.Text = SetReturnButton(Session("referrerToSom"))
        Else
            btn_return.Visible = False
        End If

        'MM-7170
        If CheckSecurity(SecurityUtils.SOM_DEL_CHG, Session("EMP_CD")) Then
            lblDelivery.Enabled = True
        Else
            lblDelivery.Enabled = False
        End If

        'Daniela IT Request 2006
        If AppConstants.Order.STATUS_OPEN = txt_Stat_cd.Text AndAlso Len(lbl_trk_stop.Text) > 3 Then
            lbl_warning.Text = Resources.LibResources.Label801

        End If
        'Daniela IT Request 2289
        If AppConstants.Order.STATUS_OPEN = txt_Stat_cd.Text AndAlso isNotEmpty(Session("OpenIST")) Then   ' lucy 11-aug-16, need to clear the message
            lbl_warning.Text = Resources.LibResources.Label970
        End If
        'Daniela IT Request CRM not allow to split
        If AppConstants.Order.STATUS_OPEN = txt_Stat_cd.Text AndAlso AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text Then
            btnSORW.Enabled = False
        End If
        'Daniela Oct 20 as per Dave add SORW security
        If Not CheckSecurity("SORW", Session("EMP_CD")) Then
            btnSORW.Enabled = False
        End If
        If Find_Security("SOCTRK", Session("emp_cd")) = "N" And Len(lbl_trk_stop.Text) > 3 Then
            txt_fi_amt.Enabled = False
            txt_fi_appr.Enabled = False
            txt_f_name.Enabled = False
            txt_l_name.Enabled = False
            txt_addr1.Enabled = False
            txt_addr2.Enabled = False
            txt_h_phone.Enabled = False
            txt_b_phone.Enabled = False
            txt_city.Enabled = False
            cbo_State.Enabled = False
            txt_zip.Enabled = False
            txt_slsp1.Enabled = False
            txt_slsp2.Enabled = False
            txt_comm1.Enabled = False
            txt_comm2.Enabled = False
            txt_exempt_id.Enabled = False
            btn_Save.Enabled = False
            cbo_finance_company.Enabled = False
            cbo_tax_exempt.Enabled = False
            cboOrdSrt.Enabled = False
            lblSetup.Enabled = False
            lblDelivery.Enabled = False
            lbl_desc.Text = "This order has been trucked.  Changes are not allowed."
        End If

        ' Daniela delivery should not be negative
        If (isNotEmpty(lblDelivery.Text) AndAlso IsNumeric(lblDelivery.Text) AndAlso lblDelivery.Text < 0) Then
            lblDelivery.Text = "0.00"
            Session("DEL_CHG") = lblDelivery.Text
            lbl_desc.Text = Resources.LibResources.Label800
        End If
        ' Daniela setup charges should not be negative
        If (isNotEmpty(lblSetup.Text) AndAlso IsNumeric(lblSetup.Text) AndAlso lblSetup.Text < 0) Then
            lblSetup.Text = "0.00"
            Session("SETUP_CHG") = lblSetup.Text
            lbl_desc.Text = Resources.LibResources.Label800
        End If

        'End Daniela

        ' MDB/MCR pu_del is set to Pickup
        If cbo_PD.Value = "P" AndAlso (AppConstants.Order.TYPE_SAL = txt_ord_tp_cd.Text OrElse AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text) Then
            lblDelivery.Text = "0.00"
            Session("DEL_CHG") = lblDelivery.Text
            Session("SETUP_CHG") = lblSetup.Text
        End If

        'setPickupDate()
        Dim orderLinesTabSelected = (ASPxPageControl1.ActiveTabIndex = 1)
        ChkBoxViewSlspersons.Visible = orderLinesTabSelected
        chk_calc_avail.Visible = orderLinesTabSelected
        chk_reserve_all.Visible = orderLinesTabSelected

        If Page.IsPostBack = False Then
            'Loading for the first time, Clear the session
            'Session("OrderRequest") = Nothing   ''mariam - sep 130,2016 -  back button IT Request- search result show
        End If
        'hidPopupStatus.Value = String.Empty
        If (Not IsNothing(Session("oracleException"))) Then
            Dim oracleExceptionMessage As String = Convert.ToString(Session("oracleException"))
            lbl_desc.Text = oracleExceptionMessage
            Session.Remove("oracleException")
        End If

        If String.IsNullOrEmpty(Session("browserID")) Then
            Dim browserID As Guid? = Nothing
            If String.IsNullOrEmpty(Convert.ToString(Session("browserID"))) Then
                browserID = Guid.NewGuid()
                Dim getBrowserID As String = browserID.ToString()
                getBrowserID = getBrowserID.Replace("-", "")
                Session("browserID") = getBrowserID.ToString()
            End If
        End If

        If IsPostBack Then

            If Not (String.IsNullOrEmpty(Convert.ToString(ViewState("del_doc_num")))) And Not (String.IsNullOrEmpty(Convert.ToString(Session("browserID")))) Then
                Dim ctl As String = GetPostBackControlName()
                If Not (ctl) Is Nothing Then
                    ' To avoid database check when user clicks on Clear Button or drowdownlist which consists of Del_Doc_Num(s)
                    If Not (ctl.Equals("btn_Clear") Or ctl.Equals("cbo_related_docs")) Then
                        'For Void and Finalized sales no need to insert the log
                        If Not (txt_Stat_cd.Text.Equals("F") Or txt_Stat_cd.Text.Equals("V")) Then
                            Dim empCode As String = Session("emp_cd")
                            If isNotEmpty(empCode) Then ' Daniela check empCode
                                accessInfo.VerifyRequest(thisProgramName, txt_ord_tp_cd.Text, Convert.ToString(txt_del_doc_num.Text), empCode, Convert.ToString(Session("browserID")))
                                If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 1) Then
                                    ViewState("LockedBy") = empCode
                                End If
                                ModifyControls(Convert.ToInt32(Session("provideAccess")))
                            End If
                        End If
                    End If
                End If
            End If
        Else
            ViewState("del_doc_num") = Request("del_doc_num")
            If Not (String.IsNullOrEmpty(Convert.ToString(ViewState("del_doc_num")))) And Not (String.IsNullOrEmpty(Convert.ToString(Session("browserID")))) Then
                Dim empCode As String = Session("emp_cd")
                If isNotEmpty(empCode) Then ' Daniela check empCode
                    accessInfo.VerifyRequest(thisProgramName, txt_ord_tp_cd.Text, Convert.ToString(txt_del_doc_num.Text), empCode, Convert.ToString(Session("browserID")))
                    If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 1) Then
                        ViewState("LockedBy") = empCode
                    End If
                    'New item will be added to the grid when an item is selected in inventory lookup
                    If Not IsNothing(Request("SKU")) Then
                        If GridView1.Controls.Count > 0 Then
                            Dim footerIndex As Integer = GridView1.Controls(0).Controls.Count - 1
                            Dim txtNewItmCd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_itm_cd"), TextBox)
                            txtNewItmCd.Text = Request("SKU")
                            Add_SKU_To_Order(txtNewItmCd, EventArgs.Empty)
                            ASPxPageControl1.ActiveTabIndex = 1
                        End If
                    End If

                    ModifyControls(Convert.ToInt32(Session("provideAccess")))
                End If
            End If
        End If
        'Enable the Comment tab
        SetCommntSection(True)
        'ByDefault disable the buttons
        If cbo_PD.Value = "P" Then
            cboTargetDate.Visible = False
            lblFilterDate.Visible = False
        End If
        If Page.IsPostBack = False AndAlso Not Session("confirmationValidationMessage") & "" = String.Empty Then
            lbl_desc.Text = Session("confirmationValidationMessage")
            Session.Remove("confirmationValidationMessage")
        End If
    End Sub


    Protected Function is_brk_user() As String
        ' Lucy Req-214, nov-2019

        Dim v_ind As String = String.Empty
        Dim v_emp_init As String = Session("emp_init")
        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader

        Dim sql As String =
        " SELECT 'Y' as AAA FROM STORE S ,co_grp cg,emp" &
        " WHERE cg.co_grp_cd='BRK'" &
        " AND cg.co_cd = s.co_cd " &
        " AND emp.home_store_cd = s.store_cd " &
        " AND emp.emp_init='" & v_emp_init & "'"



        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)
            If dbReader.Read() Then
                v_ind = dbReader.Item("AAA")
            Else
                Return "N"
            End If
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        Catch ex As Exception
            Throw ex
        Finally
            dbReader.Close()
            dbCommand.Dispose()
            dbConnection.Close()
        End Try

        If v_ind & "" = "" Then
            Return "N"
        ElseIf v_ind = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function


    Public Function send_out_fi_request() As String
        'Lucy created

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_crt As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_dis_line_1 As String
        Dim v_dis_line_2 As String
        Dim v_reserve As String
        Dim v_term_id As String

        Dim v_rpt_name As String
        Dim v_trans_type As String
        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_trans_no As String
        Dim v_auth_num As String
        Dim v_track2 As String
        Dim v_auth_no As String
        Dim v_oth As String
        Dim v_length As Integer
        Dim v_response As String
        Dim v_mop_cd As String

        Dim v_private_lbl_finance_tp As String
        Dim v_private_lbl_plan_num As String
        Dim v_count As Integer

        Dim l_filename As String
        Dim l_printcmd As String
        Dim strAsCd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim
        Dim str_cust_cd As String = ""
        '  If Left(Session("lucy_as_cd"), 1) <> "E" And Left(Session("lucy_as_cd"), 1) <> "D" Then
        'strAsCd = "E12"
        ' Else
        strAsCd = Session("lucy_as_cd")
        ' End If

        ' Daniela Nov 19
        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            Return "No designated pinpad"
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Session("auth_no") = ""
        Session("lucy_approval_cd") = ""

        v_sub_type = ""

        Dim str_del_doc_num As String = txt_del_doc_num.Text
        Dim str_emp_cd As String = Session("emp_cd")
        If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
            str_del_doc_num = ""
        End If
        If IsDBNull(txt_Cust_cd.Text) Or isEmpty(txt_Cust_cd.Text) Then
            str_cust_cd = ""
        Else
            str_cust_cd = txt_Cust_cd.Text
        End If
        Session("del_doc_num") = ""



        v_auth_no = "          "    '10 spaces

        v_mop_cd = "FI"

        v_trans_type = "12"


        v_amt = CDbl(txt_fi_amt.Text)


        ' deferred payment 
        v_private_lbl_finance_tp = strAsCd
        '   v_private_lbl_finance_tp = "D"
        ' v_private_lbl_plan_num = "081"

        '  if l_data_rec.private_lbl.finance_tp ='D' then
        '  l_data_rec.private_lbl.grc_prd :=99;
        '  l_data_rec.private_lbl.noPayinMth :='00';
        '  equal payment 
        '  elsif	l_data_rec.private_lbl.finance_tp ='E' then 
        ' l_data_rec.private_lbl.noPayinMth :=99;
        ' l_data_rec.private_lbl.grc_prd :='00';
        ' end if;
        If Len(Trim(txt_fi_acct.Text) + txt_exp_dt.Text) >= 20 Then   'manualy enterred
            v_card_num = Left(txt_fi_acct.Text, 16)
            v_expire_date = Right(txt_fi_acct.Text, 4)
            Session("lucy_bnk_card") = v_card_num
            Session("lucy_exp_dt") = v_expire_date
            str_s_or_m = "M"
        ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False And Len(Trim(Session("lucy_card_num"))) = 16 And IsNumeric(Session("lucy_card_num")) = True Then 'auto populated
            v_card_num = Session("lucy_card_num")
            ' v_expire_date = txt_fi_exp_dt.Text
            v_card_num = ""   'force to swipe or need a exp_dt enterred
            v_expire_date = ""
            str_s_or_m = "S"
        Else    'swipe
            v_card_num = ""
            v_expire_date = ""
            str_s_or_m = "S"
        End If

        Try 'Daniela

            Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

            myCMD.Connection = objConnection
            myCMD.CommandText = "std_tenderretail_pos.sendsubtp8"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_auth

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust_cd
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num
            'select lpad(glpmt_seq.nextval,12,'0') into v_trans_no from dual;
            ' peter will get v_trans_no in his procedure which is a sequence#
            ' myCMD.Parameters.Add(New OracleParameter("p_transTp", OracleType.VarChar)).Value = v_trans_type
            ' myCMD.Parameters.Add(New OracleParameter("p_authNo", OracleType.VarChar)).Value = v_auth_no

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output


            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            ' myCMD.Parameters.Add("p_rct", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
            'sabrina changed 5 to 10
            'myCMD.Parameters.Add("p_res", OracleType.VarChar, 5).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"
            End If

            ' If l_aut <> "empty" Then ' peter put empty means l_aut has null value
            v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces  
            v_trans_no = myCMD.Parameters("p_transTp").Value

            l_dsp = myCMD.Parameters("p_dsp").Value
            lblErrorMsg.Text = l_dsp
            If l_aut <> "empty" Then   'transation done successfully
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value

                l_dsp = myCMD.Parameters("p_dsp").Value
                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                    l_exp = myCMD.Parameters("p_exp").Value
                    l_crn = myCMD.Parameters("p_crn").Value
                    Session("lucy_bnk_card") = Left(l_crn, 16)
                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)
                    If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                        txt_fi_acct.Text = Left(l_crn, 16)
                    ElseIf Len(Trim(txt_fi_acct.Text)) >= 16 And IsNumeric(txt_fi_acct.Text) = False Then 'auto populated account#
                        txt_fi_acct.Text = Left(l_crn, 16)
                    End If
                Else
                    l_exp = myCMD.Parameters("p_exp").Value
                    Dim str_month As String = Left(l_exp, 2)
                    Dim str_year As String = Right(l_exp, 2)
                    Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)

                End If


                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value
                Session("str_print_cmd") = l_printcmd
                Session("auth_no") = l_aut
                Session("lucy_approval_cd") = l_aut

                print_receipt(l_printcmd)

                txt_fi_appr.Text = l_aut

            Else   'transation not done 

                '   Session("auth_no") = l_aut
                Session("lucy_approval_cd") = ""
                Session("lucy_bnk_card") = ""
                l_dsp = "empty"
                If Len(Trim(txt_fi_acct.Text)) < 16 Then  ' suppose not manual entered
                    txt_fi_acct.Text = ""
                End If
            End If
            Session("lucy_display") = Left(l_dsp, 20)

            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()

            Return l_dsp

        Catch e As Exception
            objConnection.Close()
            If InStr(e.Message.ToString, "timed") > 0 Then
                Return "Timeout in finance detail"
            End If
            Throw New Exception("Swipe/Insert Error in SOM+")
        End Try

    End Function


    Public Sub lucy_get_detail()

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_err As String
        Dim str_del_doc_num = txt_del_doc_num.Text
        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()




        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_pos_func.get_ar_trn_info"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_del_doc_num", OracleType.Char)).Value = str_del_doc_num
        ' myCMD.Parameters.Add(New OracleParameter("p_store_cd", OracleType.Char)).Value = p_store_cd
        myCMD.Parameters.Add("p_store_cd", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_bnk_crd_num", OracleType.VarChar, 50).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_exp_dt", OracleType.VarChar, 20).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_cust_cd", OracleType.VarChar, 20).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_err", OracleType.VarChar, 20).Direction = ParameterDirection.Output



        myCMD.ExecuteNonQuery()
        str_err = myCMD.Parameters("p_err").Value
        If str_err <> "N" Then
            lblErrorMsg.Text = str_err
            Exit Sub
        End If

        Session("lucy_exp_dt") = myCMD.Parameters("p_exp_dt").Value
        Session("cust_cd") = myCMD.Parameters("p_cust_cd").Value
        Session("bnk_crd_num") = myCMD.Parameters("p_bnk_crd_num").Value
        Session("del_doc_num") = txt_del_doc_num.Text
        Session("orig_store_cd") = myCMD.Parameters("p_store_cd").Value



        Dim MyDA As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(myCMD)

        Try

            MyDA.Fill(Ds)

        Catch x
            Throw

            objConnection.Close()
        End Try
        objConnection.Close()

    End Sub
    Public Function GetIPAddress() As String


        ' lucy created
        Dim strHostName As String

        Dim strIPAddress As String
        Dim str_term_id As String

        strHostName = System.Net.Dns.GetHostName()

        strIPAddress = System.Net.Dns.GetHostByName(strHostName).AddressList(0).ToString()



        Return strIPAddress

        ' MessageBox.Show("Host Name: " & strHostName & "; IP Address: " & strIPAddress)

    End Function

    ' Daniela comment
    'Public Function get_term_id() As String
    '    ' Lucy created
    '    Dim connString As String
    '    Dim objConnection As New OracleConnection
    '    Dim x As Exception
    '    Dim Ds As New DataSet()
    '    Dim str_term_id As String
    '    Dim strIP As String

    '    strIP = GetIPAddress()
    '    strIP = Request.ServerVariables("REMOTE_ADDR")
    '    connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
    '    objConnection = DisposablesManager.BuildOracleConnection(connString)
    '    objConnection.Open()

    '    Dim myCMD As New OracleCommand()

    '    myCMD.Connection = objConnection
    '    myCMD.CommandText = "std_tenderretail_pos.getTerminal"
    '    myCMD.CommandType = CommandType.StoredProcedure
    '    myCMD.Parameters.Add(New OracleParameter("p_ip", OracleType.Char)).Value = strIP
    '    myCMD.Parameters.Add("p_terminal", OracleType.VarChar, 20).Direction = ParameterDirection.Output

    '    myCMD.ExecuteNonQuery()

    '    str_term_id = myCMD.Parameters("p_terminal").Value


    '    Dim MyDA As New OracleDataAdapter(myCMD)

    '    Try

    '        MyDA.Fill(Ds)
    '        Return str_term_id

    '    Catch x
    '        ' lbl_Lucy_Test.Text = x.Message.ToString

    '        objConnection.Close()
    '    End Try
    'End Function
    Public Function MonthLastDay(ByVal dCurrDate As Date)
        'lucy
        MonthLastDay = DateSerial(Year(dCurrDate), Month(dCurrDate), 1).AddMonths(1).AddDays(-1)

    End Function

    Protected Sub print_receipt(ByVal p_prt_cmd As String)
        Dim str_ip As String
        Dim str_user As String
        Dim str_pswd As String


        str_ip = get_database_ip()
        str_user = get_user()
        str_pswd = get_pswd()


        Dim syclient As New Renci.SshNet.SshClient(str_ip, str_user, str_pswd)
        ' lblInfo.Text = "in testprint1 "

        Dim sycmd As Renci.SshNet.SshCommand
        Dim str_excep As String
        Dim str_print_cmd As String

        ' str_print_cmd = Session("str_print_cmd")
        '---------'connect to test
        Try
            syclient.Connect()

        Catch
            Throw

            str_excep = Err.Description
        End Try

        '  sycmd = syclient.RunCommand("touch /moved5/lucy/pr.lst")
        sycmd = syclient.RunCommand(p_prt_cmd)
        Try
            ' Catch syex3 As Exception
        Catch syex3 As SshException

            str_excep = Err.Description
        End Try


        sycmd.Dispose()
        syclient.Disconnect()


    End Sub
    Public Function get_database_ip()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_ip As String


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()



        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshIP"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_ip", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue


        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            str_ip = myCMD.Parameters("p_ip").Value


        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_ip
    End Function
    Public Function get_user()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_user As String
        Dim p_user As String



        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()


        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshUsr"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_user", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try


            myCMD.ExecuteNonQuery()

            str_user = myCMD.Parameters("p_user").Value

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_user
    End Function
    Public Function get_pswd()
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim p_pswd As String
        Dim str_pswd As String



        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()




        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.getSshPwd"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add("p_pswd", OracleType.VarChar, 20).Direction = ParameterDirection.ReturnValue






        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

            str_pswd = myCMD.Parameters("p_pswd").Value

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
        Return str_pswd
    End Function
    Public Function send_out_void_fi_request() As String
        'Lucy created new one

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_crt As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_dis_line_1 As String
        Dim v_dis_line_2 As String
        Dim v_reserve As String
        Dim v_term_id As String

        Dim v_rpt_name As String
        Dim v_trans_type As String
        Dim v_sub_type As String
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String
        Dim v_trans_no As String
        Dim v_auth_num As String
        Dim v_track2 As String
        Dim v_auth_no As String
        Dim v_oth As String
        Dim v_length As Integer
        Dim v_response As String
        Dim v_mop_cd As String

        Dim v_private_lbl_finance_tp As String
        Dim v_private_lbl_plan_num As String
        Dim v_count As Integer

        Dim l_filename As String
        Dim l_printcmd As String

        Dim str_auth As String = ""
        Dim str_app_cd As String = ""
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp As String = ""
        Dim sessionid As String = Session.SessionID.ToString.Trim

        lucy_get_detail()

        ' Daniela Nov 19

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        'Sep 29 validate pinpad
        If isEmpty(v_term_id) Or v_term_id = "empty" Then
            Return "No designated pinpad"
        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        v_sub_type = ""
        Session("lucy_display") = ""
        Dim str_del_doc_num As String = Session("del_doc_num")
        If IsDBNull(str_del_doc_num) Or isEmpty(str_del_doc_num) Then
            str_del_doc_num = ""
        End If

        Dim str_cust As String = Session("cust_cd")


        ' v_trans_type = "P2"
        '  If Len(Trim(txt_fi_amt.Text) = 0 Then
        'txt_fi_amt.Text = Session("lucy_amt")

        ' End If
        v_amt = CDbl(txt_fi_amt.Text)

        v_mop_cd = "FI"
        v_card_num = Left(Session("bnk_crd_num"), 16)
        str_s_or_m = "M"
        str_auth = ""
        'sabrina add
        If IsDBNull(Session("lucy_exp_dt")) = True Then
            v_expire_date = ""
        Else
            v_expire_date = Session("lucy_exp_dt")
        End If

        str_app_cd = txt_fi_appr.Text
        v_private_lbl_finance_tp = "DVD"     'lucy_btn_cmnt used to be as finance void(cancel)
        str_pmt_tp = ""
        str_correction = "N"

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_pos.sendsubtp8"
        myCMD.CommandType = CommandType.StoredProcedure
        myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = v_card_num
        myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = v_expire_date
        myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
        myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
        myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
        myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
        myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
        myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
        myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
        myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
        myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = str_app_cd
        myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = str_app_cd '  str_auth changed to str_app_cd - Nov 04 as per Sabrina

        myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
        myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = str_cust
        myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = str_del_doc_num

        myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output

        myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        ' myCMD.Parameters.Add("p_rct", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
        'sabrina changed 
        'myCMD.Parameters.Add("p_res", OracleType.VarChar, 5).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
        myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

        myCMD.ExecuteNonQuery()

        If Not IsDBNull(myCMD.Parameters("p_aut").Value) Then
            l_aut = myCMD.Parameters("p_aut").Value
        End If

        If Not IsDBNull(myCMD.Parameters("p_dsp").Value) Then
            l_dsp = myCMD.Parameters("p_dsp").Value  'sabrina add
            lbl_desc.Text = myCMD.Parameters("p_dsp").Value
        End If
        If l_aut <> "empty" And Left(l_dsp, 7) = "APPROVE" Then 'transation done successfully
            l_ack = myCMD.Parameters("p_ackInd").Value
            l_res = myCMD.Parameters("p_res").Value
            l_dsp = myCMD.Parameters("p_dsp").Value
            'sabrina comment out txt_fi_appr.Text = ""  
            'sabrina comment out lucy_update_so()
            'lucy_insert_to_payment(v_card_num, l_aut)   ' no need to use it, it seems JDA code auto commit the transaction
            lbl_desc.Text = l_dsp & ", please save the transaction"
            If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then

                l_tr2 = myCMD.Parameters("p_tr2").Value
                'sabrina add
                If IsDBNull(myCMD.Parameters("p_exp").Value) = False Then 'sabrina add
                    l_exp = myCMD.Parameters("p_exp").Value
                End If

                l_crn = myCMD.Parameters("p_crn").Value

                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value

                Session("str_print_cmd") = l_printcmd
                Session("auth_no") = l_aut
                Session("lucy_approval_cd") = l_aut
                Session("lucy_bnk_card") = Left(l_crn, 16)

                ' Dim str_month As String = Left(l_exp, 2)
                'Dim str_year As String = Right(l_exp, 2)
                'Session("lucy_exp_dt") = MonthLastDay(str_month & "/1/" & str_year)

                print_receipt(l_printcmd)

            End If

        Else   'transation not done 

            Session("lucy_approval_cd") = ""
            Session("lucy_bnk_card") = ""
            Session("lucy_exp_dt") = ""
            'sabrina comment out l_dsp = "empty"

        End If
        Session("lucy_display") = Left(l_dsp, 20)

        objConnection.Close()

        Return l_dsp


    End Function
    Protected Sub lucy_update_so()
        Dim proceed As Boolean = False
        Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Try

            'UPDATE ONLY THE AMT IN SO table
            conn.Open()
            Dim sql As String = "UPDATE SO SET APPROVAL_CD=null  WHERE DEL_DOC_NUM = :DEL_DOC_NUM" 'lucy add APPROVAL_CD
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            ' cmd.Parameters.Add(":AMT", OracleType.Double)
            ' cmd.Parameters(":AMT").Value = CDbl(txt_fi_amt.Text)
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
            cmd.ExecuteNonQuery()

            proceed = True
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
        If proceed Then
            UpdateAmountsAfterModifyFinance()
            Session("ORIG_FI_INFO") = Nothing
            PopupModifyFinance.ShowOnPageLoad = False
        End If
    End Sub

    Public Sub Add_Finance_Company()
        'lucy add it was not at this version
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT AS_CD, NAME FROM ASP WHERE ACTIVE='Y' ORDER BY NAME"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        cbo_finance_company.ClearSelection()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)


            If (MyDataReader.Read()) Then
                cbo_finance_company.DataSource = ds
                cbo_finance_company.DataValueField = "AS_CD"
                cbo_finance_company.DataTextField = "NAME"
                cbo_finance_company.DataBind()
            End If

            cbo_finance_company.Items.Insert(0, Resources.LibResources.Label415)
            cbo_finance_company.Items.FindByText(Resources.LibResources.Label415).Value = ""
            cbo_finance_company.SelectedIndex = 0

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub
    Public Sub lucy_btn_cmnt_Click(sender As Object, e As EventArgs) Handles lucy_btn_cmnt.Click
        Session("del_doc_num") = Request("del_doc_num")
        Session("from_lucy_cmnt") = ""
        Response.Redirect("pdcm.aspx")



    End Sub
    Protected Sub lucy_popup_cmnt()

        ' setVisibleChkBoxViewLineSlspersons()
        ASPxPageControl1.ActiveTabIndex = 3   'lucy , 3 is the position of the tab, current module, comments is 3 (start from 0,1,2,3...)

        Session("from_lucy_cmnt") = "NO"

        GetComments()

    End Sub

    Public Sub lucy_log(ByVal p_msg As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String



        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()




        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_pos.prn"
        myCMD.CommandType = CommandType.StoredProcedure
        'strMsg = p_msg & v_count
        strMsg = p_msg
        myCMD.Parameters.Add(New OracleParameter("p_in", OracleType.Char)).Value = strMsg


        myCMD.ExecuteNonQuery()


        v_count = v_count + 1

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
    End Sub
    Public Sub lucy_check_payment(ByRef p_yn As String)
        Dim str_itm_cd As String
        Dim str_rfid As String
        Dim str_store_cd As String
        Dim str_loc_cd As String
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim str_disposition As String = "RES"
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Exception
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        sql = "select mop_cd from payment where sessionid='" & Session.SessionID.ToString.Trim & "'"
        sql = sql & " and (mop_cd = 'VI' or (mop_cd='FI'  and FI_ACCT_NUM is not null  and APPROVAL_CD is not null ) or mop_cd='MC' or mop_cd='FV' or mop_cd='AMX' or mop_cd='DC')"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then

                lbl_desc.Text = "Authorized transaction on file, please save the changes"

                p_yn = "NO" 'cannot exit without save

            Else
                Session("lucy_from_logout") = "NO"
                p_yn = "YES"
            End If
            conn.Close()
        Catch x
            Throw


            conn.Close()
        End Try
    End Sub

    Public Sub lucy_print_salesbill(ByVal p_delDocNum As String, ByRef p_emp_cd As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String
        Dim str_emp_cd As String


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        lucy_log("from independent")
        lucy_log(p_delDocNum)

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.invoice_print"
        myCMD.CommandType = CommandType.StoredProcedure


        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd

        'Daniela french lang parameter
        Dim cult As String = UICulture
        Dim lang As String = "E"
        If InStr(cult.ToLower, "fr") >= 1 Then
            lang = "F"
        End If
        myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        End Try
    End Sub

    Public Sub sy_print_salesbill(ByVal p_delDocNum As String, ByRef p_emp_cd As String, p_ufm As String)
        'sabrina 

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String

        Dim l_co_cd As String = Session("CO_CD")
        Dim l_cust_cd As String = Session("cust_cd")
        Dim l_doc_seq As String = ""

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection

        myCMD.CommandText = "std_tenderretail_util.receipt_print"   'sabrina
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_doc_seq_num", OracleType.Char)).Value = l_doc_seq
        myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.Char)).Value = l_cust_cd
        myCMD.Parameters.Add(New OracleParameter("p_co_cd", OracleType.Char)).Value = l_co_cd
        myCMD.Parameters.Add(New OracleParameter("p_ufm", OracleType.Char)).Value = p_ufm

        Try
            myCMD.ExecuteNonQuery()
        Catch x
            objConnection.Close()
        End Try
    End Sub

    Protected Sub btn_reprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_reprint.Click
        ' Daniela reprint option
        ' IT Req 2919
        'Dim printer As String = printer_drp.SelectedItem.Value.ToString()
        If txt_del_doc_num.Text.isNotEmpty AndAlso Not Session("emp_cd") Is Nothing Then
            If Session("email_checked") = "Y" Then
                Dim emailString = txt_email.Text
                reprint_salesbill(txt_del_doc_num.Text, Session("emp_cd"), "", emailString)
            Else
                Dim printer As String = printer_drp.SelectedItem.Value.ToString()
                If (isNotEmpty(printer)) Then
                    If Session("EER_STORE_GRP") = True Then
                        reprint_salesbill(txt_del_doc_num.Text, Session("emp_cd"), printer, "")
                    Else
                        reprint_salesbill(txt_del_doc_num.Text, Session("emp_cd"), printer)
                    End If
                End If
            End If
        End If

        lbl_desc.Text = Resources.SalesExchange.Label77 & " " & txt_del_doc_num.Text & " " & Resources.SalesExchange.Label78

    End Sub

    Public Sub sy_printer_drp_Populate()

        'sabrina new sub added
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter

        ds = New DataSet
        Dim printer_found As Boolean = False
        'Daniela use emp_init from session
        Dim v_user_init As String
        If isEmpty(Session("emp_init")) Then
            v_user_init = LeonsBiz.GetEmpInit(Session("emp_cd"))
        Else
            v_user_init = Session("emp_init")
        End If
        'Dim v_user_init As String = sy_get_emp_init(Session("EMP_CD"))
        Dim v_user_cd As String = Session("EMP_CD")
        Dim v_default_printer As String = sy_get_default_printer_id(v_user_cd) 'sabrina emp_cd to get default printer
        'Dim default_printer As String = ""
        If IsDBNull(v_default_printer) = True Or v_default_printer Is Nothing Or v_default_printer = "" Then
            v_default_printer = "Printers:"
        End If

        Session("default_prt") = v_default_printer

        'sql = "SELECT PRINT_Q FROM misc.PRINT_QUEUE a WHERE PRINT_Q IS NOT NULL "
        'sql = sql & " AND (STD_CHK_PRINTER.IS_VALID_STR_PRINTER(PRINT_Q,STD_CHK_PRINTER.GET_HOME_STR("
        'sql = sql & "'" & v_user_init & "')) = 'Y'"
        'sql = sql & " OR STD_CHK_PRINTER.IS_SUPR_USR('" & v_user_init & "') = 'Y') "
        'sql = sql & " and exists (select 'x' from misc.console c where c.print_q = a.print_q) "
        'sql = sql & " and print_q <> '" & v_default_printer & "'"
        'sql = sql & " ORDER BY PRINT_Q "
        'Daniela Sept 08
        sql = "select pi.printer_nm PRINT_Q "
        sql = sql & "from  printer_info pi "
        sql = sql & "where pi.store_cd = (select home_store_cd "
        sql = sql & "from emp "
        sql = sql & "where emp_init = :v_user_init) "
        sql = sql & "and   pi.printer_tp = 'INVOICE'"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)

        objSql.Parameters.Add(":v_user_init", OracleType.VarChar)
        objSql.Parameters(":v_user_init").Value = v_user_init

        oAdp.Fill(ds)

        printer_drp.Items.Clear()

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                printer_found = True
                printer_drp.DataSource = ds
                printer_drp.ValueField = "PRINT_Q"
                printer_drp.TextField = "PRINT_Q"
                printer_drp.DataBind()
            End If

            printer_drp.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(v_default_printer))
            printer_drp.Items.FindByText(v_default_printer).Selected = True
            printer_drp.SelectedIndex = 0

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception

            MyDataReader.Close()
            conn.Close()
            Throw

        End Try

    End Sub
    Private Function sy_get_emp_init(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select emp_init as EMP_INIT from emp where emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("EMP_INIT").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function
    Private Function sy_get_default_printer_id(ByVal p_user As String) As String
        'sabrina new function
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select c.print_q as PRINT_Q from console c, emp e "
        sql = sql & "where e.print_grp_con_cd = c.con_cd And e.emp_cd = '" & p_user & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("PRINT_Q").ToString
            Else
                Return ""
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Public Sub reprint_salesbill(ByVal p_delDocNum As String, ByRef p_emp_cd As String, ByVal p_printer As String)
        ' Daniela reprint
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String
        Dim str_emp_cd As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.invoice_print_reprint"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_printer", OracleType.Char)).Value = p_printer

        'Daniela add french
        Dim cult As String = UICulture
        Dim lang As String = "E"
        If InStr(cult.ToLower, "fr") >= 1 Then
            lang = "F"
        End If
        myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang

        Try
            myCMD.ExecuteNonQuery()

        Catch x
            objConnection.Close()
        End Try
    End Sub
    Public Sub reprint_salesbill(ByVal p_delDocNum As String, ByRef p_emp_cd As String, ByVal p_printer As String, ByVal p_email As String)
        ' Daniela reprint
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1
        Dim strMsg As String
        Dim str_emp_cd As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.invoice_email_reprint"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_printer", OracleType.Char)).Value = p_printer
        myCMD.Parameters.Add(New OracleParameter("p_email", OracleType.Char)).Value = p_email

        'Daniela add french
        Dim cult As String = UICulture
        Dim lang As String = "E"
        If InStr(cult.ToLower, "fr") >= 1 Then
            lang = "F"
        End If
        myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang

        Try
            myCMD.ExecuteNonQuery()

        Catch x
            objConnection.Close()
        End Try
    End Sub
    'MM-7088
    'MM-7088
    Private Sub GetStoreLocationData(Optional ByVal receivedOnly As Boolean = False)
        cbo_void_cause_cd.Enabled = False
        cbo_void_cause_cd.Visible = False
        final_store_cd.Enabled = True
        txt_fv_dt.Enabled = True
        If (AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text) Then
            ' Below line has been commented out since that method has been called n bindgrid()
            'doAfterBindgrid()   'added so that components are enabled
            Dim soLnsDataSet As DataSet = Session("DEL_DOC_LN#")
            ' MM - 5672
            ' When CRM is about to finalize then populating the store, location fields for the grid from INV_XREF table
            ' I will update the store and location back to the dataset and it will bind to the grid
            If ((soLnsDataSet) IsNot Nothing) And (soLnsDataSet.Tables(0).Rows.Count > 0) Then
                For Each dataRow As DataRow In soLnsDataSet.Tables(0).Rows
                    If (Not "Y".Equals(dataRow("BULK_TP_ITM"))) AndAlso ("INV".Equals(dataRow("ITM_TP_CD"))) AndAlso ("Y".Equals(dataRow("INVENTORY"))) AndAlso ("N".Equals(dataRow("void_flag"))) Then
                        Dim store As String = String.Empty
                        Dim location As String = String.Empty
                        theSalesBiz.GetStoreLocation(dataRow("ITM_CD"), dataRow("DEL_DOC_NUM"), dataRow("DEL_DOC_LN#"), store, location)
                        ' To see whether the store is RF enabled or not
                        If Not (String.IsNullOrWhiteSpace(store)) AndAlso (Not String.IsNullOrWhiteSpace(location)) Then
                            If receivedOnly Then
                                dataRow("InvXrefStore") = store
                                dataRow("InvXrefLocation") = location
                            Else
                                dataRow("STORE_CD") = store
                                dataRow("LOC_CD") = location
                                dataRow("IsStoreFromInvXref") = True
                            End If
                        End If
                    End If
                Next

            End If
            Session("DEL_DOC_LN#") = soLnsDataSet
            bindgrid(soLnsDataSet)
        End If
    End Sub



    Private Sub Initialize()

        txt_SHU.Text = FormatDateTime(SessVar.shuDt, DateFormat.ShortDate)

        'have to do the populate of these d.d. here because the PopulateResults relies and sets d.d. values based on retrieved values
        PopulateTaxExempt()
        PopulateOrderSortCodes()

        PopulateResults()

        PopulateUDF()
        Try
            If Convert.ToDateTime(txt_wr_dt.Text) <= Convert.ToDateTime(txt_SHU.Text) Then
                lbl_desc.Text = vbCrLf & "*INFORMATION - " & String.Format(Resources.POSErrors.ERR0020, "modify") & vbCrLf & vbCrLf
            End If
        Catch ex As Exception

        End Try

        pmt_btn.Enabled = True
        btn_payments.Enabled = True

        Dim f_security As String = "Y"
        Dim v_security As String = "Y"

        If Find_Security("SOMF", Session("emp_cd")) = "N" Then
            Dim removeFItem As ListItem = cbo_stat_cd.Items.FindByText("Finalize")
            cbo_stat_cd.Items.Remove(removeFItem)
            f_security = "N"
        End If
        If Find_Security("SOMV", Session("emp_cd")) = "N" Then
            Dim removeVItem As ListItem = cbo_stat_cd.Items.FindByText("Void")
            cbo_stat_cd.Items.Remove(removeVItem)
            v_security = "N"
        End If

        If f_security = "N" And v_security = "N" Then
            cbo_stat_cd.Enabled = False
        End If

        If Find_Security("SOMSCPCT", Session("emp_cd")) = "Y" And txt_Stat_cd.Text = "O" Then
            txt_slsp1.Enabled = True
            txt_slsp2.Enabled = True
            txt_comm1.Enabled = True
            txt_comm2.Enabled = True
        Else
            txt_slsp1.Enabled = False
            txt_slsp2.Enabled = False
            txt_comm1.Enabled = False
            txt_comm2.Enabled = False
        End If
        calculate_total()


        'whenever an order is retrieved, store the original total for comparison later
        Session("ORIG_SO_TOTAL") = lblTotal.Text

    End Sub

    ''' <summary>
    ''' Finds out if at least if one of the lines in the order has a Store or Location
    ''' already populated
    ''' </summary>
    ''' <returns>TRUE if a store or location is found in any line; FALSE otherwise</returns>
    Private Function HasStoreOrLocation() As Boolean
        Dim storeLocFound As Boolean = False
        Dim storeCd As String
        Dim locCd As String

        Dim linesDataSet As DataSet = Session("DEL_DOC_LN#")
        For Each datRow As DataRow In linesDataSet.Tables(0).Rows

            storeCd = If(IsDBNull(datRow("STORE_CD")), String.Empty, datRow("STORE_CD"))
            locCd = If(IsDBNull(datRow("LOC_CD")), String.Empty, datRow("LOC_CD"))
            If (Not storeCd.isEmpty() OrElse Not locCd.isEmpty()) Then
                storeLocFound = True
                Exit For
            End If
        Next
        Return storeLocFound
    End Function

    ''' <summary>
    ''' Uses calling program to set the return button label
    ''' </summary>
    ''' <returns>Button label</returns>
    ''' <remarks>There is probably a better way to do this for future but for now, maybe table, this will work</remarks>
    Private Function SetReturnButton(ByVal callingPage As String) As String

        Dim retBtnLbl As String = "Return"
        If (callingPage = "salesbookofbusiness.aspx") Then

            retBtnLbl = retBtnLbl + " to SBOB"
        End If
        If (callingPage = "salesbookofbusinessMgment.aspx") Then   'lucy, 20-may-16

            retBtnLbl = retBtnLbl + " to SBOBM"
        End If
        If (callingPage = "SalesConfirmation.aspx") Then 'daniela 

            retBtnLbl = retBtnLbl + " to SOC"
        End If

        If (callingPage = "CommCustomerInquiry.aspx") Then

            retBtnLbl = retBtnLbl + " to COMAINQ"
        End If
        Return retBtnLbl
    End Function

    Protected Sub Carton_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim chkCarton As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(chkCarton.NamingContainer, DataGridItem)

        Dim datSet As DataSet = Session("DEL_DOC_LN#")
        Dim datRow As DataRow

        For Each datRow In datSet.Tables(0).Rows

            If datRow("DEL_DOC_LN#") = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text Then

                If chkCarton.Checked = True Then
                    datRow("LV_IN_CARTON") = "Y"
                Else
                    datRow("LV_IN_CARTON") = "N"
                End If

                Exit For
            End If
        Next

        Session("DEL_DOC_LN#") = datSet
    End Sub

    ''' <summary>
    ''' Used to determine if Confirmation Codes have been setup in the system and if so,
    ''' processing should occur for them.   However when NONE have been setup yet, then
    ''' ALL processing for these codes is skipped, otherwise the app fails during save.
    ''' NOTE: session value is set in PopulateConfirmationCodes() (called in PAGE-LOAD)
    ''' </summary>
    ''' <returns>TRUE if confirmation codes exist; FALSE otherwise</returns>
    Private Function AreConfirmationCodesAvailable() As Boolean
        Return (Session("ConfCodes") IsNot Nothing)
    End Function

    Private Sub PopulateOrderSortCodes()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        SQL = "SELECT ord_srt_cd, des, ord_srt_cd || ' - ' || des as full_desc FROM ord_srt_cd order by ord_srt_cd"
        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = SQL

            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cboOrdSrt
                .DataSource = ds
                .ValueField = "ord_srt_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

            cboOrdSrt.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Sort Code", ""))
            cboOrdSrt.Items.FindByText("Please Select A Sort Code").Value = ""

            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Private Sub PopulateTaxExempt()

        Dim ds As DataSet = theSalesBiz.GetTaxExemptCodes()
        With cbo_tax_exempt
            .DataSource = ds
            .ValueField = "tet_cd"
            .TextField = "full_desc"
            .DataBind()
            If Session("tet_cd") & "" <> "" Then
                .Value = Session("tet_cd")
            End If
        End With
        ' Daniela French
        'cbo_tax_exempt.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Tax Exempt Code", ""))
        'cbo_tax_exempt.Items.FindByText("Please Select A Tax Exempt Code").Value = ""
        cbo_tax_exempt.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label414, ""))
        cbo_tax_exempt.Items.FindByText(Resources.LibResources.Label414).Value = ""

        If Session("tet_cd") & "" = "" And cbo_tax_exempt.Value & "" = "" Then
            cbo_tax_exempt.SelectedIndex = 0
        End If

        If Session("tet_cd") & "" = "" And cbo_tax_exempt.SelectedIndex <> 0 Then
            Session("tet_cd") = cbo_tax_exempt.Value
        End If

    End Sub

    Private Sub PopulateUDF()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet
        ds = New DataSet

        Dim MyDataReader As OracleDataReader
        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        sql = "SELECT FLD_NAME, FLD_TITLE, FLD_LENGTH, FLD_DEF_VAL FROM USR_FLD WHERE TBL_NAME='SO'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                MyDataGrid.DataSource = ds
                MyDataGrid.DataBind()
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub


    ''' <summary>
    ''' Retrieves the confirmation status codes from the Database and binds them
    ''' to the drop-down accordingly.
    ''' </summary>
    Private Sub PopulateConfirmationCodes()
        Session("ConfCodes") = Nothing   'used later to determine if Conf-Codes should be processed
        Dim dsConfCodes As DataSet = theSalesBiz.GetConfirmationCodes()
        If dsConfCodes.Tables.Count > 0 AndAlso dsConfCodes.Tables(0).Rows.Count > 0 Then
            Dim dt As DataTable = dsConfCodes.Tables(0)
            Session("ConfCodes") = dt
            ddlConfStaCd.DataSource = dt
            ddlConfStaCd.ValueField = "Code"
            ddlConfStaCd.TextField = "Description"
            ddlConfStaCd.DataBind()
        End If
    End Sub

    Private Sub PopulateVoidCause()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetCodes As OracleCommand
        cmdGetCodes = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        SQL = "SELECT void_cd, des, void_cd || ' - ' || des as full_desc FROM void_cause order by void_cd"

        Try
            With cmdGetCodes
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With cbo_void_cause_cd
                .DataSource = ds
                .DataValueField = "void_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

            cbo_void_cause_cd.Items.Insert(0, "Please Select A Cause Code")
            cbo_void_cause_cd.Items.FindByText("Please Select A Cause Code").Value = ""

            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Private Sub PopulateStores()
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        'Nov 9 MCCL new logic
        ' SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store order by store_cd"
        Dim userType As Double = -1
        Dim SQL As String = ""

        If (isNotEmpty(Session("USER_TYPE"))) Then
            userType = Session("USER_TYPE")
        End If
        Dim coCd = Session("CO_CD")

        If (isNotEmpty(Session("str_sup_user_flag")) And Session("str_sup_user_flag").Equals("Y")) Then
            SQL = "SELECT store_cd, store_name, store_cd || ' - ' || store_name as full_desc FROM store "
        End If

        If (isNotEmpty(Session("str_sup_user_flag")) And Not Session("str_sup_user_flag").Equals("Y")) Then

            SQL = " select s.store_cd, s.store_name, s.store_cd || ' - ' || s.store_name as full_desc " +
               "from MCCL_STORE_GROUP a, store s, store_grp$store b " +
               "where a.user_id = :userType and " +
               "s.store_cd = b.store_cd and " +
               "a.store_grp_cd = b.store_grp_cd "
            ' Leons Franchise exception
            If isNotEmpty(userType) And userType = 5 Then
                SQL = SQL & " and '" & coCd & "' = s.co_cd "

            End If
        End If
        SQL = SQL & " order by store_cd"
        ' Daniela end

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            ' Parameters only if not Super User
            If (isNotEmpty(userType) And userType > 0) Then

                cmdGetStores.Parameters.Add(":userType", OracleType.VarChar)
                cmdGetStores.Parameters(":userType").Value = userType

            End If

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With final_store_cd
                .DataSource = ds
                .DataValueField = "store_cd"
                .DataTextField = "full_desc"
                .DataBind()
            End With

            With cbo_pd_store_cd
                .DataSource = ds
                .ValueField = "store_cd"
                .TextField = "full_desc"
                .DataBind()
            End With

        Catch ex As Exception
            conn.Close()
            ' Daniela Nov 21
            Throw New Exception("Invalid Order Store Code for Emp " & Session("EMP_INIT") & " and User type " & userType)
        End Try
        conn.Close()

    End Sub

    Public Sub Finance_Print()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim UpPanel As UpdatePanel
        Dim fin_invoice As String = ""
        Dim promo_cd As String = ""
        Dim as_cd As String = ""
        Dim wdr As Boolean = False
        Dim wdr_promo As String = ""

        UpPanel = Master.FindControl("UpdatePanel1")

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        sql = "SELECT FIN_CUST_CD, PROMO_CD FROM SO_ASP, SO " &
                    "WHERE SO.DEL_DOC_NUM=SO_ASP.DEL_DOC_NUM AND SO.DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            conn.Open()
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDataReader2.Read Then
                promo_cd = MyDataReader2.Item("PROMO_CD").ToString
                as_cd = MyDataReader2.Item("FIN_CUST_CD").ToString
            End If
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Dim dpMopCodes As String = DP_String_Creation()
        If (dpMopCodes.isNotEmpty) Then
            sql = "SELECT MOP_CD FROM AR_TRN WHERE IVC_CD='" & txt_del_doc_num.Text & "' AND MOP_CD IN (" & dpMopCodes & ")"
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

                If MyDataReader2.Read Then
                    wdr = True
                End If
                MyDataReader2.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

        conn.Close()
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT b.INVOICE_NAME, b.INVOICE_NAME_WDR FROM PROMO_ADDENDUMS b WHERE PROMO_CD='" & promo_cd & "' AND AS_CD='" & as_cd & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDataReader2.Read Then
                '                If wdr = False Then
                fin_invoice = MyDataReader2.Item("INVOICE_NAME").ToString
                'Else
                '   fin_invoice = MyDataReader2.Item("INVOICE_NAME_WDR").ToString
                'End If
            End If
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If fin_invoice & "" = "" Then
            lbl_desc.Text = "No finance information found"
            Exit Sub
        End If

        ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike8", "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & txt_del_doc_num.Text.ToString & "&UFM=" & fin_invoice & "','frmFinance8" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

        conn.Close()
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        'Duplicate the process for down payment transactions
        sql = "select  rtrim(substr(b.text,42,5))	WDR_promo_code "
        sql = sql & "from "
        sql = sql & "so_cmnt b "
        sql = sql & ",so      a "
        sql = sql & "where  a.del_doc_num       =  '" & txt_del_doc_num.Text & "' "
        sql = sql & "and  a.so_wr_dt     +0   =  b.so_wr_dt    (+) "
        sql = sql & "and  a.so_store_cd || '' =  b.so_store_cd (+) "
        sql = sql & "and  a.so_seq_num  || '' =  b.so_seq_num  (+) "
        sql = sql & "and  b.text           like  'Promotion-Code Assigned to WDR Deposit = %'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDataReader2.Read Then
                wdr_promo = MyDataReader2.Item("WDR_promo_code").ToString
            End If
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()
        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT b.INVOICE_NAME, b.INVOICE_NAME_WDR FROM PROMO_ADDENDUMS b WHERE PROMO_CD='" & wdr_promo & "' AND AS_CD='" & as_cd & "'"
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDataReader2.Read Then
                '                If wdr = False Then
                fin_invoice = MyDataReader2.Item("INVOICE_NAME_WDR").ToString
                'Else
                '   fin_invoice = MyDataReader2.Item("INVOICE_NAME_WDR").ToString
                'End If
            End If
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        conn.Close()
        'Print down payment type finance transactions
        If wdr = True And fin_invoice & "" <> "" Then
            ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike8", "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & txt_del_doc_num.Text.ToString & "&UFM=" & fin_invoice & "','frmFinancew3248" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        End If

    End Sub

    Protected Sub ChkBoxVoid_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
            Exit Sub
        End If

        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          '25-may-16
            lbl_final.Text = strErr
            Exit Sub
        End If
        Session("lucy_ctl_change") = "N"  ' 29-jul-16
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user makes the line VOID
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                'If Not (accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Voiding a line ")) Then Exit Sub
                If Not (accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Voiding a line ")) Then
                    Exit Sub
                End If
            End If
            Dim dgItem As DataGridItem = CType(CType(sender, CheckBox).NamingContainer, DataGridItem)
            If cbo_related_docs.Visible Then
                ViewState("VoidRow") = dgItem.ItemIndex
                ValidateSplitOrder(dgItem)
            Else
                VoidLine(dgItem)
            End If
        End If
    End Sub



    ''' <summary>
    ''' Returns a list of line sequence numbers of all the warrantable lines that have the passed-in
    ''' warranty line attached to it.
    ''' </summary>
    ''' <param name="warrantySkuLnNum">line sequence number of the warranty</param>
    ''' <returns>an array list of the line sequences of all the warrantable lines with the warranty</returns>
    Private Function GetLinkedLineNumbers(ByVal warrantySkuLnNum As String) As ArrayList

        Dim warrLnNumList As New ArrayList
        Dim soLnDataSet As DataSet = Session("DEL_DOC_LN#")

        For Each soLnRow As DataRow In soLnDataSet.Tables(0).Rows

            If "N".Equals(soLnRow("VOID_FLAG")) AndAlso soLnRow("WARR_BY_LN").ToString.isNotEmpty AndAlso soLnRow("WARR_BY_LN") = CInt(warrantySkuLnNum) Then
                'finds the warrantied row with the same warranty attached and adds it to the list
                warrLnNumList.Add(soLnRow("DEL_DOC_LN#").ToString)
            End If
        Next

        Return warrLnNumList
    End Function

    ''' <summary>
    ''' Gets the datarow from the cached session dataset table that corresponds to the line number passed-in
    ''' and updates the link info of the warranty-warrantables. It optionally voids the line too.
    ''' </summary>
    ''' <param name="lineNum">the line# of the line to find in the dataset. This is the line being modified
    '''                       that needs it's warranty link info updated.</param>
    ''' <param name="setAsVoid">Optional flag to indicate if the line with the line# passed-in should be set as "VOID' or not. Default is false.</param>
    Private Sub UpdateDataRowLinkInfo(ByVal lineNum As String, Optional ByVal setAsVoid As Boolean = False)
        Dim drSoLn As DataRow() = Nothing

        If Not IsNothing(Session("DEL_DOC_LN#")) Then
            Dim soLnDataSet As DataSet = Session("DEL_DOC_LN#")
            If soLnDataSet.Tables.Count > 0 Then
                Dim dtSoLn As DataTable = soLnDataSet.Tables(0)
                drSoLn = dtSoLn.Select("[DEL_DOC_LN#] = " + lineNum)
            End If
        End If

        If Not IsNothing(drSoLn) AndAlso drSoLn.Length > 0 Then
            'Void if the current item is inventory or otherwise if it warranty item and has not linked to other linked item.
            If (Not drSoLn(0)("ITM_TP_CD") = "WAR") OrElse (drSoLn(0)("ITM_TP_CD") = "WAR" AndAlso (Not theSalesBiz.CheckWarrLink(drSoLn(0)("DEL_DOC_NUM")))) Then
                'finds the matching datarow from the cached dataset table and updates it
                drSoLn(0)("VOID_FLAG") = IIf(setAsVoid, "Y", "N")

                drSoLn(0)("WARR_BY_SKU") = System.DBNull.Value
                drSoLn(0)("WARR_BY_LN") = System.DBNull.Value
                drSoLn(0)("WARR_BY_DOC_NUM") = System.DBNull.Value
            End If
        End If
    End Sub

    Protected Sub Price_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim soLnDatSet As DataSet = Session("DEL_DOC_LN#")
        Dim txtRetPrc As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(txtRetPrc.NamingContainer, DataGridItem)
        Dim dr As DataRow = OrderUtils.GetOrderLine(soLnDatSet, dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text)


        If dr IsNot Nothing Then
            lbl_desc.Text = ""
            If Not IsNumeric(txtRetPrc.Text) Then
                lbl_desc.Text = Resources.POSMessages.MSG0021
                lbl_desc.Visible = True
                txtRetPrc.Text = dr("UNIT_PRC")
                txtRetPrc.Focus()

                hidUpdLnNos.Value = String.Empty
                Exit Sub
            Else
                lbl_desc.Text = String.Empty
                lbl_desc.Visible = False
                txtRetPrc.Text = Math.Abs(CDbl(txtRetPrc.Text))
            End If

            Dim hidMaxDisc As HiddenField = CType(dgItem.FindControl("hidMaxDisc"), HiddenField)
            If (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso Not String.IsNullOrEmpty(hidMaxDisc.Value)) Then

                Dim maxDiscPct As Decimal = 0, enteredRetPrice As Decimal = 0
                Dim effDt As String = If(IsDBNull(dr("ADDON_WR_DT")), txt_wr_dt.Text, dr("ADDON_WR_DT").ToString)

                Dim drItmRetPrice As Double = 0
                Double.TryParse(dr("itm_ret_prc").ToString, drItmRetPrice)

                'this represents the calendar or item retail price
                Dim itmRetPrc As Double = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                                     dr("Store_cd").ToString,
                                                                     String.Empty,
                                                                     effDt,
                                                                     dr("ITM_CD").ToString,
                                                                     drItmRetPrice)

                If Decimal.TryParse(hidMaxDisc.Value, maxDiscPct) AndAlso Decimal.TryParse(txtRetPrc.Text, enteredRetPrice) Then

                    Dim maxDiscAmt As Decimal = itmRetPrc - (itmRetPrc * (maxDiscPct / 100))
                    maxDiscAmt = Math.Round(maxDiscAmt, 2)  'Alice added on June 6, 2019, the issue was found during test 4168

                    'Alice updated on May 24, 2019 for request 4168
                    '1.	Allow max discount item to have its price manually changed And move forward for manager approval if outlet id reserved to item.
                    '2.	Allow max discount item to have its price changed if ORDER's sort code is acceptable/approved order sort codes.

                    'add start
                    Dim ItemIsAssociatePurchaseOrHasOutletId As Boolean = False

                    If CType(dgItem.FindControl("lbl_outnum"), Label).Visible = True Then
                        ItemIsAssociatePurchaseOrHasOutletId = True
                    End If


                    If SalesUtils.isAllowMaxDiscountSortCode(cboOrdSrt.SelectedItem.Value) = "Y" Then
                        ItemIsAssociatePurchaseOrHasOutletId = True
                    End If


                    If ItemIsAssociatePurchaseOrHasOutletId And maxDiscPct <> 100 Then
                        CType(dgItem.FindControl("lbl_max"), Label).Text = "MAX"
                    End If

                    'add end



                    'MM-6914 - Maximum discount restriction should not be applied in case of CRMs, MDBs and MCRs

                    If (maxDiscAmt > enteredRetPrice) And ItemIsAssociatePurchaseOrHasOutletId = False Then  'Alice updated for request 4168
                        lbl_desc.Text = String.Format(Resources.POSErrors.ERR0019, FormatCurrency(maxDiscAmt, 2))
                        lbl_desc.Visible = True

                        txtRetPrc.Text = FormatNumber(dr("UNIT_PRC"))
                        txtRetPrc.Focus()

                        RemoveUpdLn(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.ToString)
                        ' hidUpdLnNos.Value = String.Empty
                        Exit Sub
                    End If
                End If
            End If

            Dim margin As Double = 0
            If IsNumeric(ConfigurationManager.AppSettings("margin").ToString) Then
                margin = CDbl(ConfigurationManager.AppSettings("margin").ToString)
            End If

            'will prompt for mgr approver if set to 'Line' or "order' OR if "N" but the GM is < the specified margin
            Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
            Dim promptForApproval As Boolean = AppConstants.ManagerOverride.BY_LINE = prcOverride OrElse
                                               AppConstants.ManagerOverride.BY_ORDER = prcOverride OrElse
                                               (margin > 0 AndAlso determine_gm(dgItem.Cells(SoLnGrid.ITM_CD).Text, txtRetPrc.Text) < margin)

            If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso promptForApproval Then
                If (AppConstants.ManagerOverride.BY_ORDER = prcOverride) Then

                    'store the line# of the line being modified to use later
                    If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
                        hidUpdLnNos.Value = hidUpdLnNos.Value.ToString() & ";" & dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
                        Session("hidUpdLnNos") = hidUpdLnNos.Value
                    Else
                        hidUpdLnNos.Value = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
                        Session("hidUpdLnNos") = hidUpdLnNos.Value
                    End If

                    'update the price in the dataset right away, so that everything shows the updated price
                    Dim ds As DataSet = Session("DEL_DOC_LN#")
                    For Each lnRow In ds.Tables(0).Rows
                        If lnRow("DEL_DOC_LN#") = CInt(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text) Then
                            lnRow("UNIT_PRC") = CDbl(txtRetPrc.Text)
                            Exit For
                        End If
                    Next
                Else
                    'means that Mgr Override is by Line OR that the GM is below the configured margin, so approval is needed
                    'Store the grid row and line num in the session for using later in the approval event handler
                    MgrByLineApprovalPopup(dgItem, txtRetPrc.Text)

                End If
                txtRetPrc.Text = Replace(FormatNumber(txtRetPrc.Text, 2), ",", "")
            Else
                '*** Not a SAL type of transaction OR that approval is not needed

                txtRetPrc.Text = Replace(FormatNumber(txtRetPrc.Text, 2), ",", "")
                Dim maxExtRet As Double = 0
                If OrderUtils.isCrmWithOrigSal(txt_ord_tp_cd.Text, Session("ORIG_DEL_DOC_NUM")) AndAlso dr("max_ret_prc") < CDbl(txtRetPrc.Text) Then

                    lbl_desc.Text = "Credit " + FormatCurrency(txtRetPrc.Text, 2) + " entered for SKU " + dr("ITM_CD") +
                    " exceeds the available credit for this line from the original document; credit changed to maximum."
                    txtRetPrc.Text = FormatNumber(dr("max_ret_prc").ToString, 2)
                    RemoveUpdLn(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.ToString)
                    Exit Sub

                ElseIf OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then  ' calc and enforce max extended  

                    ' just happens that max_crm_qty got loaded with orig sal qty so we can use it here to calc MCR only extended too
                    maxExtRet = Math.Round(CDbl(dr("max_crm_qty").ToString) * CDbl(dr("max_ret_prc").ToString), SystemUtils.CurrencyPrecision)
                    If Math.Round(CDbl(txtRetPrc.Text) * CDbl(dr("qty").ToString), SystemUtils.CurrencyPrecision) > maxExtRet Then

                        lbl_desc.Text = "Maximum credit available for SKU " + dgItem.Cells(SoLnGrid.ITM_CD).Text + " is " + FormatNumber(maxExtRet, 2) + "; price changed to lesser of previous or maximum."

                        If CDbl(dr("max_ret_prc").ToString) > CDbl(dr("UNIT_PRC").ToString) Then
                            txtRetPrc.Text = FormatNumber(dr("UNIT_PRC").ToString, 2)
                        Else
                            txtRetPrc.Text = FormatNumber(dr("max_ret_prc").ToString, 2)
                        End If

                        lbl_desc.Visible = True
                        RemoveUpdLn(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.ToString)
                        Exit Sub
                    End If
                End If
                If AppConstants.ManagerOverride.BY_LINE = prcOverride Then
                    MgrByLineApprovalPopup(dgItem, txtRetPrc.Text)
                End If
                dr("UNIT_PRC") = FormatNumber(txtRetPrc.Text, 2) ' txtRetPrc.Text
                dr("MANUAL_PRC") = FormatNumber(txtRetPrc.Text, 2) ' txtRetPrc.Text


                '***** Need to check here if discounts need to be reapplied for ONLY a SAL & when the prc override syspm is set as "NO" 
                '***** This condition should ideally be checked up above beofre getting to this 'if' instead of here but that is more restructuring
                If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso AppConstants.ManagerOverride.NONE = prcOverride Then
                    'reapplies the discounts (if any) when applicable
                    ReapplyDiscounts(soLnDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)
                    Session("DEL_DOC_LN#") = soLnDatSet
                    GridView1.DataSource = soLnDatSet
                    GridView1.DataBind()
                    '6635
                    doAfterBindgrid()
                End If
            End If
            ''dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
            'If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT Then
            '    theSalesBiz.updateSplitItmShareWhenPriceChange(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text, txtRetPrc.Text)
            'End If

            calculate_total()
        End If
    End Sub

    Protected Sub Gridview1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridView1.ItemCommand
        Try
            Dim row As Integer = e.Item.DataSetIndex
            Session("SELECTEDSKUINDEX") = row

            If e.CommandName = "find_inventory" Then
                'While user was finding an inventory information inside the grid
                If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                    'If Not (accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Finding an inventory")) Then Exit Sub
                    accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Finding an inventory")
                End If

                Dim lbl_itm_cd As Label = CType(e.Item.FindControl("lbl_SKU"), Label)
                If ConfigurationManager.AppSettings("inv_avail").ToString = "CUSTOM" Then
                    ASPxPopupControl3.ContentUrl = "Custom_Availability.aspx?referrer=SOM&SKU=" & lbl_itm_cd.Text & "&ROW_ID=" & GridView1.DataKeys(e.Item.ItemIndex) & "&INV_SELECTION=Y&DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned")
                Else
                    ASPxPopupControl3.ContentUrl = "InventoryLookup.aspx?referrer=SOM&SKU=" & lbl_itm_cd.Text & "&ROW_ID=" & GridView1.DataKeys(e.Item.ItemIndex) & "&INV_SELECTION=Y&DEL_DOC_NUM=" & Request("del_doc_num") & "&query_returned=Y" & "&allowres=" & AllowReservations()
                End If
                ASPxPopupControl3.ShowOnPageLoad = True
            ElseIf e.CommandName = "inventory_lookup" Then
                'While user viewes inventory information inside the grid
                If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                    accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Looking at inventory")
                End If

                Dim lbl_itm_cd As Label
                lbl_itm_cd = CType(e.Item.FindControl("lbl_SKU"), Label)
                ASPxPopupControl4.ContentUrl = "Special_Order_Popup.aspx?SKU=" & lbl_itm_cd.Text & "&ROW_ID=" & GridView1.DataKeys(e.Item.ItemIndex) & "&SPEC_ORD=N"
                ASPxPopupControl4.ShowOnPageLoad = True
            ElseIf e.CommandName = "find_gm" Then
                'While user was finding the gross margin inside the grid
                If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                    accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Finding the gross margin")
                End If
                Dim sql As String
                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim objSql As OracleCommand
                Dim MyDataReader As OracleDataReader
                Dim frt_factor As Double = 0
                Dim repl_cst As Double = 0
                Dim ret_prc As Double = 0
                conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)

                Dim txt_ret_prc As TextBox
                txt_ret_prc = CType(e.Item.FindControl("unit_prc"), TextBox)

                If IsNumeric(txt_ret_prc.Text) Then ret_prc = CDbl(txt_ret_prc.Text)
                sql = "select repl_cst, frt_fac from itm where itm_cd='" & e.Item.Cells(SoLnGrid.ITM_CD).Text & "'"
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    conn.Open()
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If MyDataReader.Read() Then
                        If IsNumeric(MyDataReader.Item("REPL_CST").ToString) Then repl_cst = CDbl(MyDataReader.Item("REPL_CST").ToString)
                        If IsNumeric(MyDataReader.Item("FRT_FAC").ToString) Then frt_factor = CDbl(MyDataReader.Item("FRT_FAC").ToString)
                    End If
                    MyDataReader.Close()
                Catch
                    conn.Close()
                    Throw
                End Try
                conn.Close()
                If frt_factor > 0 Then repl_cst = FormatNumber(repl_cst + (repl_cst * (frt_factor / 100)), 2)
                lbl_gm.Text = "Retail: " & FormatCurrency(ret_prc, 2) & "<br />"
                lbl_gm.Text = lbl_gm.Text & "Cost: " & FormatCurrency(repl_cst, 2) & "<br />"
                lbl_gm.Text = lbl_gm.Text & "Gross Profit: " & FormatCurrency(ret_prc - repl_cst, 2) & "<br />"
                lbl_gm.Text = lbl_gm.Text & "Gross Profit Margin: " & FormatPercent((ret_prc - repl_cst) / ret_prc, 2) & "<br />"
                ASPxPopupControl7.ShowOnPageLoad = True
            ElseIf e.CommandName.ToUpper() = "REPLACE" Then
                Dim lbl_itm_cd As Label
                lbl_itm_cd = CType(e.Item.FindControl("lbl_SKU"), Label)
                ASPxPopupSKU.Visible = True
                ASPxPopupSKU.ShowOnPageLoad = True
                SKU1.ExistingSKU = lbl_itm_cd.Text.ToUpper().Trim()
                ViewState("ReplaceInvoked") = True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Retrieves the Serial numbers and binds the results to the grid component displayed in the Serial Numbers popup
    ''' </summary>
    ''' <param name="selRowId">the row_id value coming from the in memory OrderLines, indicates the row being processed</param>
    ''' <param name="itemCd">the item code being processed</param>
    ''' <param name="storeCd">store code provided by the user to be used for this item</param>
    ''' <param name="locCd">location code provided by the user to be used for this item</param>
    ''' <returns>TRUE if serial numbers are found for the store and location entered; FALSE otherwise</returns>
    Private Function PromptForSerialNumber(ByVal selRowId As String,
                                           ByVal itemCd As String,
                                           ByVal storeCd As String,
                                           ByVal locCd As String) As Boolean
        Dim sNumbersFound As Boolean = True

        'makes sure to display only those Serial #s that have not been assigned in the current order
        Dim lnsData As DataSet = Session("DEL_DOC_LN#")
        Dim sNumbersInUse As String = theSalesBiz.GetSerialNumbersInUse(lnsData)
        Dim sNumbersDataSet As DataSet = theInvBiz.GetSerialNumbersAvailable(itemCd, storeCd, locCd, sNumbersInUse)

        lbl_ser_row_id.Text = selRowId
        lbl_ser_itm_cd.Text = itemCd
        lbl_ser_loc_cd.Text = locCd
        lbl_ser_store_cd.Text = storeCd
        ASPxPopupControl10.ShowOnPageLoad = True
        ASPxGridView4.DataSource = sNumbersDataSet
        ASPxGridView4.DataBind()
        If sNumbersDataSet.Tables(0).Rows.Count < 1 Then
            lbl_ser_err.Text = "No items were found for the selected store and location."
            sNumbersFound = False    'to notify caller that no serial numbers were found
        End If
        Return sNumbersFound

    End Function

    ''' <summary>
    ''' Retrieves the Outlet IDs and binds the results to the grid component displayed in the Outlet Ids popup
    ''' </summary>
    ''' <param name="selRowId">the row_id value coming from the TEMP_ITEM table, indicates the row being processed</param>
    ''' <param name="itemCd">the item code being processed</param>
    ''' <param name="storeCd">store code provided by the user to be used for this item</param>
    ''' <param name="locCd">location code provided by the user to be used for this item</param>
    ''' <returns>TRUE if outlet IDs are found for the store and location entered; FALSE otherwise</returns>
    Private Function PromptForOutlet(ByVal selRowId As String,
                                     ByVal itemCd As String,
                                     ByVal storeCd As String,
                                     ByVal locCd As String) As Boolean

        Dim outIdsFound As Boolean = True
        If AppConstants.Order.TYPE_SAL = txt_ord_tp_cd.Text Then
            'makes sure to display only those Outlet Ids that have not been assigned in the current order
            Dim lnsData As DataSet = Session("DEL_DOC_LN#")
            Dim outIdsInUse As String = theSalesBiz.GetOutletIdsInUse(lnsData)
            Dim outIdsDataSet As DataSet = theInvBiz.GetOutletIdsAvailable(itemCd, storeCd, locCd, outIdsInUse)

            lbl_out_row_id.Text = selRowId
            lbl_out_itm_cd.Text = itemCd
            lbl_out_loc_cd.Text = locCd
            lbl_out_store_cd.Text = storeCd
            ASPxPopupControl12.ShowOnPageLoad = True
            ASPxGridView5.DataSource = outIdsDataSet
            ASPxGridView5.DataBind()

            If outIdsDataSet.Tables(0).Rows.Count < 1 Then
                lbl_out_err.Text = "No items were found for the selected store and location."
                ASPxPopupControl12.ShowCloseButton = True
                ASPxPopupControl12.CloseAction = DevExpress.Web.ASPxClasses.CloseAction.CloseButton
                outIdsFound = False       'to notify caller no outlet IDs were found
            End If

        ElseIf AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text Then

            Dim outCodesDataSet As DataSet = theInvBiz.GetOutletCodes()
            lbl_out_cd_row_id.Text = selRowId
            ASPxPopupControl14.ShowOnPageLoad = True
            ASPxGridView14.DataSource = outCodesDataSet
            ASPxGridView14.DataBind()

            If outCodesDataSet.Tables(0).Rows.Count < 1 Then
                lbl_out_cd_err.Text = "No outlet codes were found for selection."
                ASPxPopupControl14.ShowCloseButton = True
                ASPxPopupControl14.CloseAction = DevExpress.Web.ASPxClasses.CloseAction.CloseButton
                outIdsFound = False       'to notify caller no outlet IDs were found
            End If
        End If
        Return outIdsFound

    End Function

    ''' <summary>
    ''' Used this transient cache to avoid hitting the database multiple times for
    ''' every security and every line placed on the order, mostly during the
    ''' itemdatabound process
    ''' </summary>
    ''' <param name="key">the Security KEY that needs to be read</param>
    ''' <param name="emp">the Employee code to check security for</param>
    ''' <returns>TRUE, if user has the security indicated; FALSE otherwise</returns>
    Private Function CheckSecurity(key As String, emp As String) As Boolean
        Dim catKey As String = key + "~" + emp
        If Not SecurityCache.ContainsKey(catKey) Then
            SecurityCache.Add(catKey, SecurityUtils.hasSecurity(key, emp))
        End If
        Return SecurityCache(catKey)
    End Function


    Public Class SoLn2Disc

        Property delDocNum As String
        Property delDocLnNum As Integer
        Property discCd As String
        Property discAmt As Double
        Property discReasonCd As String
        Property discCmnt As String
        Property discPct As Double
        Property seqNum As Integer
    End Class

    ''' <summary>
    ''' Creates a new row in the dataset for a line that was a result of a split due to auto reserve. This happens
    ''' when the entire qty on a line cannot be fully reserved out of the same store-location based on priority fill.
    ''' So a new split line is created based on the values of the original line.
    ''' </summary>
    ''' <param name="splitLnRequest">the custom object containing the data needed for creating a new dataset row</param>
    ''' <returns>the line number of the newly created line</returns>
    Private Function CreateNewSoLnForInvSplit(ByVal splitLnRequest As SplitLineRequest) As Integer

        Dim newLnNum As Integer = 0

        If Not Session("DEL_DOC_LN#") Is Nothing Then ' should never happen

            Dim soLnDatSet As DataSet = Session("DEL_DOC_LN#")
            Dim soLnTbl As DataTable = soLnDatSet.Tables(0)
            Dim newSoLnRow As DataRow = soLnTbl.NewRow
            'newSoLnRow.ItemArray = soLnTbl.Rows(splitLnRequest.origLnNum - 1).ItemArray  ' TODO DSA - this may not work if order is split
            Dim expression As String = "[DEL_DOC_LN#]='" & splitLnRequest.origLnNum & "'"
            newSoLnRow.ItemArray = soLnTbl.Select(expression).ToArray(0).ItemArray()

            newLnNum = GetNextDelDocLnNum(Trim(txt_del_doc_num.Text))
            newSoLnRow("DEL_DOC_LN#") = newLnNum
            newSoLnRow("NEW_LINE") = "Y"
            newSoLnRow("STORE_CD") = splitLnRequest.newLnStoreCd
            newSoLnRow("LOC_CD") = splitLnRequest.newLnLocCd
            newSoLnRow("QTY") = splitLnRequest.newLnQty
            newSoLnRow("RES_ID") = splitLnRequest.newResId
            If IsNumeric(splitLnRequest.newLnTax) Then
                newSoLnRow("CUST_TAX_CHG") = CDbl(splitLnRequest.newLnTax)
            End If

            '**** add warranty links for new split lines            
            If (splitLnRequest.newLnWarrBySku.isNotEmpty) Then

                newSoLnRow("WARR_BY_LN") = CInt(splitLnRequest.newLnWarrByLine)
                newSoLnRow("WARR_BY_SKU") = splitLnRequest.newLnWarrBySku
                newSoLnRow("WARR_BY_DOC_NUM") = splitLnRequest.origDelDocNum
                newSoLnRow("WARRANTABLE") = "Y"
            End If

            soLnTbl.Rows.Add(newSoLnRow)
            Session("DEL_DOC_LN#") = soLnDatSet

            ' no inventory to split because that is what we are splitting already
            ' no treatment to split because E1 doesn't update the treatment line when split (so like E1) and the treatable line pointers are not wrong
            ' split comm_paid and so_ln2disc when save
            ' split line tax amounts
        End If

        Return newLnNum

    End Function

    ''' <summary>
    ''' Gets the next line number/sequence for the order passed-in.
    ''' </summary>
    ''' <param name="delDocNum">the document/order number</param>
    ''' <returns>the next order line number/sequence</returns>
    Private Function GetNextDelDocLnNum(ByVal delDocNum As String) As Integer

        Dim curr_ln_num As Integer = 0

        If Not IsNothing(Session("DEL_DOC_LN#")) Then

            Dim datSet As DataSet = Session("DEL_DOC_LN#")
            Dim datTbl As DataTable = datSet.Tables(0)
            Dim datRow As DataRow

            'find max del_doc_ln# in table
            For Each datRow In datSet.Tables(0).Rows

                If IsNumeric(datRow("DEL_DOC_LN#")) AndAlso
                    datRow("DEL_DOC_LN#") > curr_ln_num Then

                    curr_ln_num = datRow("DEL_DOC_LN#")
                End If
            Next
        End If

        If curr_ln_num < 1 Then

            Dim sql As String = "SELECT MAX(DEL_DOC_LN#) FROM SO_LN WHERE SO_DOC_NUM = :soDocNum "
            Dim conn As OracleConnection = GetConn(Connection_Constants.CONN_ERP)
            Dim cmd = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                conn.Open()

                cmd.Parameters.Add(":soDocNum", OracleType.VarChar)
                cmd.Parameters(":soDocNum").Value = Left(lbl_so_doc_num.Text, 11)
                Dim datRdr As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)


                If datRdr.Read() Then
                    If IsNumeric(datRdr.Item("DEL_DOC_LN#").ToString) Then
                        curr_ln_num = CInt(datRdr.Item("DEL_DOC_LN#").ToString)
                    End If
                End If
                datRdr.Close()
            Finally
                conn.Close()
            End Try

        End If

        Return (curr_ln_num + 1)
    End Function

    ''' <summary>
    ''' Returns the current line number
    ''' </summary>
    Private Function GetCurrentLineNum() As Integer

        Dim currLineNum As Integer = 0

        If Not IsNothing(Session("DEL_DOC_LN#")) Then

            Dim datSet As DataSet = Session("DEL_DOC_LN#")
            Dim datTbl As DataTable = datSet.Tables(0)
            Dim datRow As DataRow

            'find max del_doc_ln# in table
            For Each datRow In datSet.Tables(0).Rows

                If IsNumeric(datRow("DEL_DOC_LN#")) AndAlso
                    datRow("DEL_DOC_LN#") > currLineNum Then

                    currLineNum = datRow("DEL_DOC_LN#")
                End If
            Next
        End If

        Return currLineNum

    End Function

    Public Shared Sub splitTaxAmt(ByVal origTaxAmt As Double, ByVal origQty As Double, ByVal splitOffQty As Double, ByRef taxAmtOnNew As Double, ByRef taxAmtRemOnOrig As Double)
        '  Split the original tax amount for the original quantity and return the tax amount for the new quantity

        If origQty > 0.0 Then

            taxAmtOnNew = (origTaxAmt / origQty) * splitOffQty
            taxAmtRemOnOrig = origTaxAmt - taxAmtOnNew

        ElseIf splitOffQty > 0.0 Then

            taxAmtOnNew = origTaxAmt
            taxAmtRemOnOrig = 0.0

        Else
            taxAmtOnNew = 0.0
            taxAmtRemOnOrig = 0.0
        End If
    End Sub

    ''' <summary>
    ''' Attempts to reserve ALL inventoriable lines that are not linked to a PO and not already reserved 
    ''' </summary>
    ''' <remarks>Called from the 'Reserve ALL' checkbox</remarks>
    Private Sub ReserveInventory(Optional ByVal lnsToReserve As List(Of String) = Nothing)

        Dim valMsg As String = InventoryUtils.AllowReservations(SessVar.zoneCd, SessVar.puDelStoreCd)
        If (valMsg.isNotEmpty()) Then
            lbl_desc.Text = valMsg
        Else
            Dim resRequest As ResRequestDtc
            Dim resResponse As ResResponseDtc
            Dim callAutoRes As Boolean
            Dim reservationsMade As Boolean = False
            Dim lnNum As String
            Dim soLnsDataSet As DataSet = Session("DEL_DOC_LN#")

            'Has to create a copy of the Table that needs to be processed, otherwise, if lines
            'are splitted, it will throw an exception since the SO-Lines dataset gets modified
            Dim soLnsTblCopy As DataTable = soLnsDataSet.Tables(0).Clone()  'copies the structure
            For Each soLnRow In soLnsDataSet.Tables(0).Rows
                soLnsTblCopy.ImportRow(soLnRow)
            Next

            'Attempts to reserve each line in the CLONE table, no updates happen on this clone
            For Each soLnRow In soLnsTblCopy.Rows

                lnNum = soLnRow("DEL_DOC_LN#")
                If (IsNothing(lnsToReserve)) Then
                    ' Attempt reservation for NON-VOIDED Inventory lines, that are NOT already reserved
                    callAutoRes = Not "Y" = soLnRow("VOID_FLAG").ToString AndAlso
                                  Not IsDBNull(soLnRow("INVENTORY")) AndAlso
                                  "Y" = soLnRow("INVENTORY").ToString AndAlso
                                  ((IsDBNull(soLnRow("STORE_CD")) OrElse soLnRow("STORE_CD").ToString.isEmpty) OrElse
                                   (IsDBNull(soLnRow("LOC_CD")) OrElse soLnRow("LOC_CD").ToString.isEmpty))
                Else
                    'only process this line if found in the list
                    callAutoRes = lnsToReserve.Contains(lnNum)
                End If

                If callAutoRes Then
                    resRequest = GetRequestForAutoRes(soLnRow("ITM_CD"),
                                                      soLnRow("QTY"),
                                                      Session("PD"),
                                                      Session("PD_STORE_CD"),
                                                      Session("ZONE_CD"))
                    resRequest.lineNum = lnNum

                    resResponse = ReserveInventory(resRequest, soLnRow)
                    ' =========================================================
                    ' when reservations made, checks if NEW lines needed due to a partial reservation
                    ' Check for null reference
                    If (Not IsNothing(resResponse)) AndAlso (resResponse.resId.Trim.isNotEmpty()) Then
                        If resResponse.isForARSStore AndAlso resResponse.isLineSplitted Then
                            'reservationsMade = False
                        Else
                            reservationsMade = True
                        End If

                    End If
                    ' =========================================================
                End If
            Next

            If reservationsMade Then
                bindgrid() 'since new lines got added
                doAfterBindgrid()
            End If
        End If

    End Sub

    ''' <summary>
    ''' Attempts to reserve the line represented by the details passed in 
    ''' </summary>
    ''' <param name="resRequest">custom object with details needed for the reservation</param>
    ''' <returns>custom object with the reservation details</returns>
    Private Function ReserveInventory(ByVal resRequest As ResRequestDtc, ByVal lnToReserve As DataRow) As ResResponseDtc
        Dim resResponse As ResResponseDtc = theInvBiz.RequestSoftReserve(resRequest)
        If (Not IsNothing(resResponse)) AndAlso (resResponse.resId.Trim.isNotEmpty()) Then

            Dim invRow As DataRow
            Dim cnt As Integer = 0
            Dim newLnQty As Double
            Dim resLnQty As Double = resRequest.qty
            Dim resLnStore As String = String.Empty  'this get populated if a RES is made otherwise, empty is correct
            Dim resLnLoc As String = String.Empty    'this get populated if a RES is made otherwise, empty is correct
            Dim splitLnReqList As ArrayList = IIf(Session("SPLIT_SO_LN") Is Nothing, New ArrayList(), Session("SPLIT_SO_LN"))
            Dim splitLnReq As SplitLineRequest
            ' TODO future use tax method on tax code
            Dim remainingTax As Double = 0.0
            Dim taxChgPerUnit As Double = 0.0
            Dim taxOnNewLine As Double = 0.0

            If ("Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) AndAlso
                Not IsDBNull(lnToReserve("CUST_TAX_CHG")) AndAlso IsNumeric(lnToReserve("CUST_TAX_CHG")) AndAlso lnToReserve("CUST_TAX_CHG") > 0.0) Then

                Dim custTaxCharge As Double = CDbl(lnToReserve("CUST_TAX_CHG"))
                taxChgPerUnit = Round(custTaxCharge / resRequest.qty, TaxUtils.Tax_Constants.taxPrecision)
                remainingTax = custTaxCharge
            End If
            If SessVar.isArsEnabled AndAlso resResponse.reservations.Rows.Count > 1 Then
                'Do not split the line and reserve!!
                'resResponse.resId = String.Empty
                If Not resResponse.reservations.Columns.Contains("RES_ID") Then
                    resResponse.reservations.Columns.Add("RES_ID", GetType(String))
                End If
                If Not resResponse.reservations.Columns.Contains("ITM_CD") Then
                    resResponse.reservations.Columns.Add("ITM_CD", GetType(String))
                End If
                If Not resResponse.reservations.Columns.Contains("SERIAL_NUM") Then
                    resResponse.reservations.Columns.Add("SERIAL_NUM", GetType(String))
                End If
                If Not resResponse.reservations.Columns.Contains("OUT_ID") Then
                    resResponse.reservations.Columns.Add("OUT_ID", GetType(String))
                End If
                resResponse.isForARSStore = True
                resResponse.isLineSplitted = True
                'remove the soft reservation
                For Each invRow In resResponse.reservations.Rows
                    invRow("RES_ID") = resResponse.resId
                    invRow("ITM_CD") = lnToReserve("ITM_CD")
                    invRow("SERIAL_NUM") = lnToReserve("SERIAL_NUM")
                    invRow("OUT_ID") = lnToReserve("OUT_ID")
                    theSalesBiz.UnreserveLine(invRow)
                Next
                'msg.Append("Item : ").Append(lnToReserve("ITM_CD")).Append(" at line# ").Append(lnToReserve("ITM_CD")).Append(" is not reserved since it requires line split")
                If splitErrMsg.Length = 0 Then
                    splitErrMsg.Append(vbCrLf)
                End If
                splitErrMsg.Append(String.Format(Resources.POSErrors.ERR0043, lnToReserve("ITM_CD"), resRequest.lineNum))
                splitErrMsg.Append(vbCrLf)
                'msg.Length = 0
            Else
                For Each invRow In resResponse.reservations.Rows
                    If cnt = 0 Then   ' first row only 
                        'has to cache these to update the line triggering reservations
                        resLnStore = UCase(invRow("STORE_CD").ToString)
                        resLnLoc = UCase(invRow("LOC_CD").ToString)
                        resLnQty = invRow("QTY")
                    Else
                        newLnQty = CDbl(UCase(invRow("QTY").ToString))
                        taxOnNewLine = Round(taxChgPerUnit * newLnQty, TaxUtils.Tax_Constants.taxPrecision) ' just remains zero if no line tax
                        remainingTax = remainingTax - taxOnNewLine

                        splitLnReq = New SplitLineRequest
                        splitLnReq.newDelDocNum = Trim(txt_del_doc_num.Text)
                        splitLnReq.origDelDocNum = Trim(txt_del_doc_num.Text) ' SOM does not split docs so these are same
                        splitLnReq.origLnNum = resRequest.lineNum
                        splitLnReq.newLnQty = newLnQty
                        splitLnReq.newLnStoreCd = UCase(invRow("STORE_CD").ToString)
                        splitLnReq.newLnLocCd = UCase(invRow("LOC_CD").ToString)
                        splitLnReq.newResId = If(splitLnReq.newLnStoreCd.isNotEmpty() AndAlso
                                                 splitLnReq.newLnLocCd.isNotEmpty(),
                                                    resResponse.resId.Trim, String.Empty)
                        splitLnReq.newLnTax = If("Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString), taxOnNewLine.ToString, "")

                        If (Not IsDBNull(lnToReserve("WARR_BY_LN")) AndAlso
                            lnToReserve("WARR_BY_LN").ToString().isNotEmpty() AndAlso
                            lnToReserve("WARR_BY_LN").ToString() > 0) Then

                            splitLnReq.newLnWarrByDocNum = lnToReserve("WARR_BY_DOC_NUM").ToString()
                            splitLnReq.newLnWarrByLine = lnToReserve("WARR_BY_LN").ToString()
                            splitLnReq.newLnWarrBySku = lnToReserve("WARR_BY_SKU").ToString()
                        End If

                        splitLnReq.newLnNum = CreateNewSoLnForInvSplit(splitLnReq)
                        ' need to retain list to split related tables (COMM_PAID, SO_LN2DISC)

                        splitLnReq.soDiscCd = SessVar.discCd
                        splitLnReqList.Add(splitLnReq)
                    End If
                    cnt = cnt + 1
                Next
                Session("SPLIT_SO_LN") = splitLnReqList   'used during the SAVE of the order 

                '-- Finally updates the SO-LINE that triggered this method
                UpdateSoLineForReservation(resResponse.resId, resLnStore, resLnLoc,
                                           resRequest.lineNum, resLnQty, remainingTax)
            End If
        End If
        Return resResponse
    End Function

    ''' <summary>
    ''' Updates in the SO-LINES grid, the row that matches the line id passed in. 
    ''' this method gets called after an Auto-Reserve of lines causes the lines to be split
    ''' when only partially reserved
    ''' </summary>
    ''' <param name="resId">the unique reservation ID</param>
    ''' <param name="storeCd">the store code were item was reserved from; or empty if not reserved</param>
    ''' <param name="locCd">the location code were item was reserved from; or empty if not reserved</param>
    ''' <param name="lineNum">the line that will be updated</param>
    ''' <param name="lnQty">qty to be set on the line. Represents, the reserved qty after split</param>
    ''' <param name="custTaxChg">the taxes to be set on the line, only needed when line was splitted</param>
    ''' <remarks> TO BE CALLED AFTER AFTER RESERVATIONS ARE ATTEMPTED </remarks>
    Private Sub UpdateSoLineForReservation(ByVal resId As String, ByVal storeCd As String, ByVal locCd As String,
                                           ByVal lineNum As Integer, ByVal lnQty As Double,
                                           Optional ByVal custTaxChg As Double = 0.0)

        Dim lnsDataSet As DataSet = Session("DEL_DOC_LN#")
        For Each lnRow In lnsDataSet.Tables(0).Rows

            If lnRow("DEL_DOC_LN#") = lineNum Then

                lnRow("invPickVoid") = "Y"
                lnRow("OUT_ID") = System.DBNull.Value
                lnRow("OUT_CD") = System.DBNull.Value
                lnRow("SERIAL_NUM") = System.DBNull.Value
                lnRow("PO_CD") = System.DBNull.Value
                lnRow("PO_DESC") = System.DBNull.Value
                lnRow("RES_ID") = resId
                lnRow("QTY") = lnQty
                lnRow("LOC_CD") = IIf(locCd.isEmpty, System.DBNull.Value, locCd)
                lnRow("STORE_CD") = IIf(storeCd.isEmpty, System.DBNull.Value, storeCd)

                If (custTaxChg > 0.0) Then
                    lnRow("CUST_TAX_CHG") = Round(custTaxChg, TaxUtils.Tax_Constants.taxPrecision)
                End If
                Exit For 'no need to keep on iterating, updated the correct row already
            End If
        Next
    End Sub

    ''' <summary>
    ''' Called from the Store and Location text box listeners.  Combined since these events
    ''' require most of the same processing
    ''' </summary>
    ''' <param name="theTextBox">the field that triggered the event</param>
    ''' <param name="isStoreUpdate">true=indicates store changed, false=indicates location changed</param>
    Private Sub DoStoreLocationUpdate(ByVal theTextBox As TextBox, ByVal isStoreUpdate As Boolean)

        'Outlet validations are failing due to case sensitivity.  Forcing upper case as soon as value is entered.
        theTextBox.Text = UCase(theTextBox.Text).Trim
        Dim dgItem As DataGridItem = CType(theTextBox.NamingContainer, DataGridItem)
        Dim tBoxStoreCd As TextBox = CType(dgItem.FindControl("store_cd"), TextBox)
        Dim tboxLocCd As TextBox = CType(dgItem.FindControl("loc_cd"), TextBox)
        Dim tboxQty As TextBox = CType(dgItem.FindControl("qty"), TextBox)
        Dim tboxRetPrc As TextBox = CType(dgItem.FindControl("unit_prc"), TextBox)
        Dim isOutletLoc As Boolean = False
        If (tBoxStoreCd.Text.isNotEmpty And tboxLocCd.Text.isNotEmpty) Then
            isOutletLoc = theInvBiz.IsOutletLocation(tBoxStoreCd.Text, tboxLocCd.Text)
        End If
        'Showroom location
        Dim isShowroomLoc As Boolean = False
        If ((Not isOutletLoc) And tBoxStoreCd.Text.isNotEmpty And tboxLocCd.Text.isNotEmpty) Then
            isShowroomLoc = LeonsBiz.IsShowroomLocation(tBoxStoreCd.Text, tboxLocCd.Text)
        End If
        Dim resId As String = String.Empty
        Dim lnsDataSet As DataSet = Session("DEL_DOC_LN#")
        Dim lnDataRow As DataRow = Nothing
        'soft reservations ONLY apply for SAL documents
        Dim isSale As Boolean = (AppConstants.Order.TYPE_SAL = txt_ord_tp_cd.Text)
        Dim isCRM As Boolean = (AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text)
        Dim isBeingFinalized As Boolean = (AppConstants.Order.STATUS_FINAL = cbo_stat_cd.SelectedValue)
        Try

            'finds the ROW being processed, is needed later in this method
            For Each lnRow In lnsDataSet.Tables(0).Rows
                If lnRow("DEL_DOC_LN#") = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text Then
                    lnDataRow = lnRow
                    Exit For 'no need to keep on iterating, found the one
                End If
            Next

            '-- Regardless of the change to store or location, soft reservations must be removed
            If (isSale AndAlso (dgItem.Cells(SoLnGrid.RES_ID).Text).isNotEmpty) Then
                Dim request As UnResRequestDtc = GetRequestForUnreserve(dgItem.Cells(SoLnGrid.RES_ID).Text,
                                                                        dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                                                        dgItem.Cells(SoLnGrid.STORE_CD).Text,
                                                                        dgItem.Cells(SoLnGrid.LOC_CD).Text,
                                                                        tboxQty.Text)
                theInvBiz.RemoveSoftRes(request)
            End If

            '-- When changing or clearing the STORE, the location gets cleared by default (MM-6326)
            If (isStoreUpdate) Then tboxLocCd.Text = String.Empty

            '-- When STORE/LOCATION are present, a prompt for serial number may occur.
            ' Done at this point, because if no serial #s exist for the store/location combination, 
            ' user is informed and the store/location are cleared, so no additional process happens
            ' IMPORTANT: If the SKU is serialized, but an Outlet location was entered, the 
            '            requirement is to prompt for Outlet ID and not the Serial#
            '=======================================================================================
            'NOTE: at this point, NO prompting will happen, unless is a SAL document, IF required 
            '      later, this code needs to be changed accordingly
            '=======================================================================================
            If (isSale AndAlso isOutletLoc = False AndAlso isShowroomLoc = False AndAlso
                ConfigurationManager.AppSettings("enable_serialization").ToString = "Y" AndAlso
                IsSerialType(dgItem.Cells(SoLnGrid.SERIAL_TP).Text) AndAlso
                tboxLocCd.Text.isNotEmpty AndAlso
                tBoxStoreCd.Text.isNotEmpty AndAlso Not "R".Equals(tBoxStoreCd.Text)) Then

                If (Not PromptForSerialNumber(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text,
                                              dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                              tBoxStoreCd.Text,
                                              tboxLocCd.Text)) Then
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                End If
            End If

            '-- When STORE/LOCATION are present, a prompt for Outlet ID may occur.
            ' Done at this point, because if no outlet id exist for the store/location combination, 
            ' user is informed and the store/location are cleared, so no additional process happens
            '=======================================================================================
            'NOTE: Prompt ONLY happens for SALE or CRM when being finalized
            '=======================================================================================
            If ((isSale OrElse (isCRM AndAlso isBeingFinalized)) AndAlso
                isOutletLoc AndAlso isShowroomLoc AndAlso tboxLocCd.Text.isNotEmpty AndAlso
                tBoxStoreCd.Text.isNotEmpty AndAlso Not "R".Equals(tBoxStoreCd.Text)) Then

                If IsNumeric(tboxQty.Text) AndAlso tboxQty.Text > 1 Then
                    lbl_out_err.Text = "Quantity must equal 1 for SKUS reserved from outlet locations. Quantity set to 1."
                    tboxQty.Text = "1"
                ElseIf Not IsNumeric(tboxQty.Text) Then
                    lbl_out_err.Text = "Quantity must be a valid number. Quantity set to 1."
                    tboxQty.Text = "1"
                End If

                If (Not PromptForOutlet(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text,
                                        dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                        tBoxStoreCd.Text,
                                        tboxLocCd.Text)) Then
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                End If
            End If

            '==================================  PROCESS RESERVATIONS =========================================
            '-- When STORE/LOCATION are present (if not cleared during Serial #s OR Outlet ID processing above)
            'attempts to do a DIRECTED soft reservation (only allowed for NON outlet location)
            ' ------------------------------  OR  -------------------------------------------------------------
            '-- When the STORE field has an "R" attempts to do an AUTO soft reservation
            Dim reservationsMade As Boolean = False
            Dim resRequest As ResRequestDtc = Nothing

            '=======================================================================================
            'NOTE: SOFT-RESERVATIONS are only to happen for SAL documents
            '=======================================================================================
            If (isSale) Then
                If (isOutletLoc = False AndAlso isShowroomLoc = False AndAlso tboxLocCd.Text.isNotEmpty AndAlso
                    tBoxStoreCd.Text.isNotEmpty AndAlso Not "R".Equals(tBoxStoreCd.Text)) Then

                    resRequest = GetRequestForDirectedRes(dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                                          tboxQty.Text,
                                                          tBoxStoreCd.Text,
                                                          tboxLocCd.Text,
                                                          Session("PD"))

                ElseIf (isStoreUpdate AndAlso tBoxStoreCd.Text.isNotEmpty AndAlso "R".Equals(tBoxStoreCd.Text.Trim)) Then

                    Dim zoneCd As String = IIf(Session("zone_cd") Is Nothing, String.Empty, Session("zone_cd"))
                    ' if priority fill is by zone, then the api will error w/o a zone code 
                    If zoneCd.isEmpty AndAlso SysPms.isPriorityFillByZone Then

                        lbl_desc.Text = Resources.POSErrors.ERR0004
                        tBoxStoreCd.Text = String.Empty
                        tboxLocCd.Text = String.Empty
                    Else
                        resRequest = GetRequestForAutoRes(dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                                          tboxQty.Text,
                                                          Session("PD"),
                                                          Session("PD_STORE_CD"),
                                                          Session("ZONE_CD"))
                    End If
                End If

                If (resRequest IsNot Nothing) Then  'reservations need to be processed
                    resRequest.lineNum = CInt(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text)

                    Dim resResponse As ResResponseDtc = ReserveInventory(resRequest, lnDataRow)
                    If (Not IsNothing(resResponse)) AndAlso (resResponse.resId.Trim.isNotEmpty()) Then
                        reservationsMade = True
                        Dim invRow As DataRow = resResponse.reservations.Rows(0)
                        tBoxStoreCd.Text = UCase(invRow("STORE_CD").ToString)
                        tboxLocCd.Text = UCase(invRow("LOC_CD").ToString)
                        tboxQty.Text = invRow("QTY").ToString
                    Else
                        'if here means NO reservations were made, therefore clears the STORE/LOCATION values
                        tBoxStoreCd.Text = String.Empty
                        tboxLocCd.Text = String.Empty
                    End If
                End If
            End If         'enf if (isSale)

            'NOTE: SOFT-RESERVATIONS are only to happen for SAL documents.  When reservations are
            'processed, the current line gets updated as part of that process, no need to update here
            If (Not reservationsMade) Then
                dgItem.Cells(SoLnGrid.INV_PICK_VOID).Text = "Y"   ' void the pick
                UpdateSoLineForReservation(resId, tBoxStoreCd.Text, tboxLocCd.Text,
                                           CInt(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text), CDbl(tboxQty.Text))
            End If

            If (tBoxStoreCd.Text.isEmpty) Then
                tBoxStoreCd.Focus()
            ElseIf (tboxLocCd.Text.isEmpty) Then
                tboxLocCd.Focus()
            Else
                tboxRetPrc.Focus()
            End If

            Session("DEL_DOC_LN#") = lnsDataSet
            ' because of picked status, now need to bind after loc updates too
            bindgrid()
            doAfterBindgrid()

        Catch oracleException As OracleException
            Select Case oracleException.Code
                Case 20101
                    lbl_desc.Text = HBCG_Utils.FormatOracleException(oracleException.Message.ToString())
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
            End Select
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub Store_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          '25-may-16
            lbl_final.Text = strErr
            Exit Sub
        End If

        Dim textdata As TextBox = CType(sender, TextBox)
        If AppConstants.Order.STATUS_FINAL = cbo_stat_cd.SelectedValue AndAlso AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text.Trim AndAlso theInvBiz.IsRfStore(textdata.Text.Trim.ToUpper) Then
            lbl_desc.Text = Resources.POSMessages.MSG0057
            textdata.Text = ""
        Else
            DoStoreLocationUpdate(textdata, True)
        End If
    End Sub

    Protected Sub Loc_Update(ByVal sender As Object, ByVal e As EventArgs)
        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          '25-may-16
            lbl_final.Text = strErr
            Exit Sub
        End If

        Dim textdata As TextBox = CType(sender, TextBox)

        DoStoreLocationUpdate(textdata, False)
    End Sub

    Protected Sub Qty_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          '11-aug-16, no check as per Wes
            lbl_final.Text = strErr

            Exit Sub
        End If
        Session("lucy_ctl_change") = "N"    ' lucy 11-aug-16
        'If user has write mode access then continue

        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user updates the quantity on the line
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                'If Not (accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " User changing the Quantity on line ")) Then Exit Sub
                If Not (accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " User changing the Quantity on line ")) Then
                    Exit Sub
                End If
            End If

            Dim txtBoxQty As TextBox = CType(sender, TextBox)
            Dim dgItem As DataGridItem = CType(txtBoxQty.NamingContainer, DataGridItem)
            Dim ds As DataSet = Session("DEL_DOC_LN#")
            Dim calculateDiscounts As Boolean = False
            Dim qty As Double
            Dim isCredDocWithOrigSal As Boolean = OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num"))

            Dim dr As DataRow = OrderUtils.GetOrderLine(Session("DEL_DOC_LN#"), dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text)
            If txtBoxQty.Text.isEmpty OrElse Not (IsNumeric(txtBoxQty.Text)) Then

                lbl_desc.Text = "Quantity must be entered and must be a valid number.  Quantity reset."
                txtBoxQty.Text = FormatNumber(dr("QTY").ToString, 0)
            Else
                If (AppConstants.Order.TYPE_SAL = txt_ord_tp_cd.Text) Then
                    ' ---- REMOVE PENDING (SOFT) RESERVATIONS WHEN PRESENT
                    theSalesBiz.UnreserveLine(dr)
                    Dim tBoxStoreCd As TextBox = CType(dgItem.FindControl("store_cd"), TextBox)
                    Dim tboxLocCd As TextBox = CType(dgItem.FindControl("loc_cd"), TextBox)
                    tBoxStoreCd.Text = String.Empty
                    tboxLocCd.Text = String.Empty
                End If

                qty = CDbl(txtBoxQty.Text)
                If qty < 0 Then
                    txtBoxQty.Text = IIf(isCredDocWithOrigSal, dr("QTY").ToString, qty * -1)
                End If

                If SkuUtils.ITM_TP_CPT.Equals(dgItem.Cells(SoLnGrid.ITM_TP_CD).Text) Then

                    txtBoxQty.Text = FormatNumber(txtBoxQty.Text, 2)

                ElseIf InStr(txtBoxQty.Text, ".") > 0 Then

                    txtBoxQty.Text = FormatNumber(dr("QTY").ToString, 0)
                    lbl_desc.Text = "Fractional quantities are only allowed for carpet SKUs.  Quantity reset."

                ElseIf qty = 0 Then
                    'User is not allowed to change QTY to zero.
                    'dr("VOID_FLAG") = "Y"
                    txtBoxQty.Text = dr("QTY").ToString
                    'Dim chk_delete As CheckBox = DirectCast(dgItem.FindControl("ChkBoxVoid"), CheckBox)
                    'chk_delete.Checked = True

                ElseIf qty > 1 Then

                    If isCredDocWithOrigSal Then
                        ValidateCreditDocQty(txtBoxQty.Text, dr)
                    Else
                        If SkuUtils.ITM_TP_WAR.Equals(dgItem.Cells(SoLnGrid.ITM_TP_CD).Text) Then ' warranties still sold at qty 1

                            lbl_desc.Text = "Quantity must equal 1 for warranty SKUs. Quantity set to 1."
                            txtBoxQty.Text = FormatNumber(1, 0)
                        ElseIf "Y".Equals(dgItem.Cells(SoLnGrid.WARRANTABLE).Text) AndAlso dgItem.Cells(SoLnGrid.WARR_BY_LN).Text.isNotEmpty Then

                            If Not SysPms.isOne2ManyWarr AndAlso CInt(txtBoxQty.Text) > 1 Then
                                lbl_desc.Text = "Quantity must equal 1 when a warranty is attached to a SKU. Quantity set to 1."
                                txtBoxQty.Text = FormatNumber(1, 0)
                            End If
                        Else
                            txtBoxQty.Text = FormatNumber(qty, 0)
                        End If
                    End If
                End If

                'set the datarow qty to the text field value
                dr("QTY") = CDbl(txtBoxQty.Text)
                calculateDiscounts = True
            End If

            If (calculateDiscounts) Then
                'reapplies the discounts (if any) when applicable
                ReapplyDiscounts(ds.Tables(0), ReapplyDiscountRequestDtc.LnChng.Qty)

            End If

            Session("DEL_DOC_LN#") = ds
            doAfterBindgrid()  'to refresh the display accordingly
            calculate_total()
        End If
    End Sub

    ''' <summary>
    ''' Recalculates the discounts (when applicable)
    ''' </summary>
    ''' <param name="dtLines">the table of all lines in the current order stored in the session </param>
    ''' <param name="action">Any of the values in the ReapplyDiscountRequestDtc.LnChng</param>
    ''' <param name="lnSeq">used only for add line, last line that existed before discounts to be recalc'd</param>
    Private Sub ReapplyDiscounts(ByRef dtLines As DataTable, ByVal action As ReapplyDiscountRequestDtc.LnChng, Optional ByVal lnSeq As String = "", Optional ByVal IsCanceled As Boolean = False)

        If (AppConstants.Order.TYPE_SAL = Session("ORD_TP_CD")) Then

            Dim soTbl As DataSet = Session("ORIG_SO")
            'Dim soDatRow As DataRow = soTbl.Tables(0).Rows(0)("so_wr_dt")
            Dim soWrDt As Date = soTbl.Tables(0).Rows(0)("so_wr_dt")   'soDatRow("so_wr_dt")
            Dim discRequest As ReapplyDiscountRequestDtc = New ReapplyDiscountRequestDtc(action, Session("pd"), dtLines, Session("DISC_APPLIED"),
                                                                                         lnSeq, soWrDt)
            Dim isSOM As Boolean = True
            'MM-9870
            Dim discResponse As ReapplyDiscountResponseDtc
            If IsCanceled Then
                discRequest.discApplied = Session("discountsApplied")
                discResponse = theSalesBiz.CheckDiscountsForLineChangeSOM(discRequest, isSOM, True)
            Else
                discResponse = theSalesBiz.CheckDiscountsForLineChangeSOM(discRequest, isSOM)
            End If


            'update the session variables with the objects that have been updated and returned as part of the response
            dtLines = discResponse.orderLines

            '*** this is to ensure that the discount tab gets refreshed properly since the active tab changed does not fire.
            If Not IsNothing(discResponse.discountsApplied) Then

                Session("DISC_APPLIED") = discResponse.discountsApplied
                ucDiscount.BindDiscountsApplied(discResponse.discountsApplied)
            End If
            If discResponse.message.isNotEmpty Then
                lbl_desc.Text = discResponse.message   'SHU'd lines not updateable
            End If
        End If
        SessVar.Remove(SessVar.discReCalcVarNm)
    End Sub

    ''' <summary>
    ''' Unreserves all lines in the current order.   This process gets called after the order 
    ''' has been saved, or if the order is being cancelled, to remove the ITM_RES records,
    ''' and release the hold on the inventory.
    ''' </summary>
    Private Sub UnreserveInventory()
        Dim soLnDs As DataSet = Session("DEL_DOC_LN#")
        If (Not IsNothing(soLnDs)) Then
            For Each lnRow In soLnDs.Tables(0).Rows
                theSalesBiz.UnreserveLine(lnRow)
            Next
        End If
    End Sub


    ''' <summary>
    ''' Validates the qty for a credit document like a CRM/MCR
    ''' </summary>
    ''' <param name="qtyText">the qty entered that needs to be validated</param>
    ''' <param name="dr">the datarow with the data of the row being modified</param>
    Private Sub ValidateCreditDocQty(ByRef qtyText As String, ByVal dr As DataRow)

        If OrderUtils.isCrmWithOrigSal(txt_ord_tp_cd.Text, Session("ORIG_DEL_DOC_NUM")) AndAlso dr("max_crm_qty") < CDbl(qtyText) Then

            lbl_desc.Text = "Quantity " + qtyText + " entered for SKU " + dr("ITM_CD") +
                " is greater than available quantity to return from original document; quantity changed to maximum."
            qtyText = FormatNumber(dr("max_crm_qty").ToString, 0)

        Else ' calc and enforce max extended  

            Dim maxExtRet As Double = 0
            ' just happens that max_crm_qty got loaded with orig sal qty so we can use it here to calc MCR only extended too
            maxExtRet = Math.Round(CDbl(dr("max_crm_qty").ToString) * CDbl(dr("max_ret_prc").ToString), SystemUtils.CurrencyPrecision)
            If Math.Round(CDbl(qtyText) * CDbl(dr("unit_prc").ToString), SystemUtils.CurrencyPrecision) > maxExtRet Then

                lbl_desc.Text = "Maximum credit available for SKU " + dr("ITM_CD") + " is " + FormatNumber(maxExtRet, 2) + "; quantity changed to lesser of previous or maximum."
                If dr("max_crm_qty").ToString > dr("qty") Then
                    qtyText = FormatNumber(dr("qty").ToString, 0)
                Else
                    qtyText = FormatNumber(dr("max_crm_qty").ToString, 0)
                End If
            End If
        End If
        lbl_desc.Visible = True

    End Sub

    ''' <summary>
    ''' Determine if the document number being passed has been previously queried from the
    ''' database and the SO_LN rows saved into the order lines session variable.  This allows us to avoid 
    ''' requerying the SO_LN data when it may have already been changed locally.
    ''' </summary>
    ''' <param name="docNum">SO table document number being requested to process</param>
    ''' <returns>True if the document has already been queried and we have the lines, false otherwise </returns>
    Private Function SameDocInProcess(ByVal docNum As String) As Boolean

        Dim sameDocInProc As Boolean = False

        If SessVar.ordLns IsNot Nothing AndAlso SessVar.ordLns.Tables.Count > 0 AndAlso SessVar.ordLns.Tables(0) IsNot Nothing Then

            Dim soLnDatSet As DataSet = SessVar.ordLns
            If CType(SessVar.ordLns, DataSet).Tables(0).Rows.Count > 0 AndAlso soLnDatSet.Tables(0).Rows(0)(0).ToString() = Trim(docNum) Then

                sameDocInProc = True
            End If
        End If

        Return sameDocInProc
    End Function

    Private Sub bindgrid()

        Dim soLnDatSet As DataSet = New DataSet
        Dim DEL_DOC_NUM As String = Request("del_doc_num")
        Dim NewSoFound As Boolean = True

        If SameDocInProcess(DEL_DOC_NUM) Then

            NewSoFound = False
            soLnDatSet = SessVar.ordLns   'Session("DEL_DOC_LN#")
            'Dim nutbl As DataTable = soLnDatSet.Tables(0)
            'Dim tmpDisp As String
            'For Each drx As DataRow In nutbl.Rows
            '    tmpDisp = drx("DEL_DOC_LN#") & " / " & drx("ITM_CD")
            'Next
            GridView1.DataSource = soLnDatSet
            GridView1.DataBind()

        Else
            soLnDatSet.Clear()
            Session("DEL_DOC_LN#") = soLnDatSet
        End If

        If isRedirectedFromSORW = True Then

            NewSoFound = True
            soLnDatSet = New DataSet
        Else
            'Do nothing
        End If

        GridView1.Visible = True
        If NewSoFound = True Then

            GridView1.DataSource = ""

            soLnDatSet = theSalesBiz.GetLineItemsForSOM(DEL_DOC_NUM)

            'MM-5672
            Dim addNewColumn1 As DataColumn
            addNewColumn1 = New DataColumn("IsStoreFromInvXref", System.Type.GetType("System.Boolean"))
            soLnDatSet.Tables(0).Columns.Add(addNewColumn1)

            For Each dr As DataRow In soLnDatSet.Tables(0).Rows
                dr.BeginEdit()
                dr.Item("IsStoreFromInvXref") = False
                dr.EndEdit()
            Next
            Dim addNewColumn3, addNewColumn2 As DataColumn
            addNewColumn3 = New DataColumn("InvXrefStore", System.Type.GetType("System.String"))
            soLnDatSet.Tables(0).Columns.Add(addNewColumn3)
            addNewColumn2 = New DataColumn("InvXrefLocation", System.Type.GetType("System.String"))
            soLnDatSet.Tables(0).Columns.Add(addNewColumn2)
            'Checking for CRM or MCR
            If OrderUtils.isCredDocWithOrigSal(txt_ord_tp_cd.Text, Session("ORIG_DEL_DOC_NUM")) Then
                Dim maxRetQtyList As DataSet
                If IsNothing(Session("MaxCredList")) Then
                    maxRetQtyList = OrderUtils.getMaxCreditListByLn(Session("ORIG_DEL_DOC_NUM"), txt_del_doc_num.Text)
                    Session("MaxCredList") = maxRetQtyList
                Else
                    maxRetQtyList = Session("MaxCredList")
                End If
                If SystemUtils.dataSetHasRows(maxRetQtyList) AndAlso SystemUtils.dataRowIsNotEmpty(maxRetQtyList.Tables(0).Rows(0)) Then
                    Dim solnTbl As DataTable = soLnDatSet.Tables(0)
                    Dim maxCred As New OrderUtils.MaxCredResponse
                    For Each solnRow In solnTbl.Rows
                        If Not "Y".Equals(solnRow("void_flag")) Then
                            ' Daniela Nov 20 fix cast error
                            If (Not IsDBNull(solnRow("orig_so_ln_num"))) Then
                                maxCred = OrderUtils.getLnMaxCredit(solnRow("itm_cd"), solnRow("orig_so_ln_num"), solnRow("unit_prc"), Session("ord_tp_cd"), maxRetQtyList)
                                solnRow("max_crm_qty") = maxCred.maxQty
                                solnRow("max_ret_prc") = maxCred.maxUnitRetail
                            End If ' End Daniela
                            Dim solnnum As String = If(IsDBNull(solnRow("orig_so_ln_num")), String.Empty, solnRow("orig_so_ln_num").ToString)
                            '6886 -if the original SO line number (orig_so_ln_num) is empty,then we are assigning del_doc_ln# value of original order of particular line item  to orig_so_ln_num
                            maxCred = OrderUtils.getLnMaxCredit(solnRow("itm_cd"), If(solnnum.ToString.isEmpty, solnRow("DEL_DOC_LN#"), solnnum), solnRow("unit_prc"), Session("ord_tp_cd"), maxRetQtyList)
                            solnRow("max_crm_qty") = maxCred.maxQty
                            solnRow("max_ret_prc") = maxCred.maxUnitRetail
                        End If
                    Next
                End If
            End If

            '----- Retrieves PO details as needed -----
            'Added arrival date to the PO Details
            Dim dbReader As OracleDataReader
            Dim sql As New StringBuilder
            Dim dbConn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
            dbConn.Open()

            sql.Append("SELECT so_ln$po_ln.po_cd, so_ln$po_ln.po_cd || ' - Line#' || so_ln$po_ln.ln# || '  (' || (po_ln.arrival_dt + " & SysPms.PadDaysForReceivingPOandIST & ") || ')' as po_desc,  nvl(po_ln.arrival_dt,'') as POArrival, ")
            sql.Append("so_ln$po_ln.ln# AS PO_LN, po_ln.arrival_dt, so_ln$po_ln.del_doc_num ")
            sql.Append("FROM so_ln$po_ln, po, po_ln ")
            sql.Append("WHERE po.po_cd=so_ln$po_ln.po_cd and po.po_cd=po_ln.po_cd and PO_LN.LN#=so_ln$po_ln.ln# ")
            sql.Append("AND del_doc_num = :delDocNum AND del_doc_ln# = :delDocLnNum")
            objSql.Parameters.Clear()
            objSql.CommandText = sql.ToString()
            objSql.Parameters.Add(":delDocNum", OracleType.VarChar)
            objSql.Parameters.Add(":delDocLnNum", OracleType.Number)

            ' Add new column into data table #ITREQUEST-337
            soLnDatSet.Tables(0).Columns.Add(New DataColumn("BOOKING_STATUS"))

            For Each soLn As DataRow In soLnDatSet.Tables(0).Rows
                objSql.Parameters(":delDocNum").Value = UCase(DEL_DOC_NUM)
                objSql.Parameters(":delDocLnNum").Value = soLn("DEL_DOC_LN#")
                dbReader = DisposablesManager.BuildOracleDataReader(objSql)

                If (dbReader.Read()) Then
                    soLn("PO_CD") = dbReader.Item("PO_CD")
                    soLn("PO_DESC") = "PO: " & dbReader.Item("PO_DESC")
                    soLn("PO_LN") = dbReader.Item("PO_LN")
                    Dim PODate As DateTime = dbReader.Item("POArrival")
                    PODate = PODate.AddDays(SysPms.PadDaysForReceivingPOandIST)
                    soLn("POArrival") = PODate

                    ' Get purchase order line booking status #ITREQUEST-377
                    soLn("BOOKING_STATUS") = theSalesBiz.getPOLineBookingStatus(soLn("PO_CD").ToString, soLn("PO_LN").ToString)

                End If
            Next
            If (Not IsNothing(dbReader)) Then dbReader.Close()
            dbConn.Close()

            '============== Updated dataset to memory
            Session("DEL_DOC_LN#") = soLnDatSet

            'If ConfigurationManager.AppSettings("package_breakout") = "N" Then
            '    Package_Explosion()
            '    soLnDatSet = Session("DEL_DOC_LN#")
            'End If

            GridView1.DataSource = soLnDatSet
            GridView1.DataBind()
        End If

        ' disable/enable components where conditions don't apply
        'doAfterBindgrid()

    End Sub

    ' MM - 5672
    Public Sub bindgrid(ByVal soLnDatSet As DataSet)
        GridView1.DataSource = soLnDatSet
        GridView1.DataBind()
        ' disable/enable components where conditions don't apply
        doAfterBindgrid()
    End Sub

    Private Function createOrdLn(ByVal soLn As DataRow, ByVal isNewTaxALineMethod As Boolean, Optional ByVal useOrig As Boolean = False) As OrderLine
        ' creates an orderLine object from the so_ln datarow
        '   only values needed for original implementation have been set;  more values may be added as needed
        '   NOTE can only used when datarow has at least the referenced columns by the exact referenced names
        ' assign numbers to 'nothing' to set to null, zero for zero

        Dim ordLn As New OrderLine
        Dim tmpStr As String

        ordLn.delDocNum = IIf(useOrig, soLn("del_doc_num", DataRowVersion.Original).ToString(), soLn("del_doc_num").ToString())
        ordLn.lnNum = IIf(useOrig, soLn("del_doc_ln#", DataRowVersion.Original).ToString(), soLn("del_doc_ln#").ToString())
        ordLn.itmCd = IIf(useOrig, soLn("itm_cd", DataRowVersion.Original).ToString(), soLn("itm_cd").ToString())   ' SKU should NEVER change; we don't handle it but the process needs the itm_cd

        ordLn.unitPrc = IIf(useOrig, soLn("unit_prc", DataRowVersion.Original).ToString(), soLn("unit_prc").ToString())
        ordLn.prcChgAppCd = IIf(useOrig, soLn("prc_chg_app_cd", DataRowVersion.Original).ToString, soLn("prc_chg_app_cd").ToString())

        ordLn.qty = IIf(useOrig, soLn("qty", DataRowVersion.Original).ToString(), soLn("qty").ToString())
        ordLn.lvInCarton = IIf(useOrig, soLn("lv_in_carton", DataRowVersion.Original).ToString(), soLn("lv_in_carton").ToString())

        ordLn.voidFlag = IIf(useOrig, soLn("void_flag", DataRowVersion.Original).ToString(), soLn("void_flag").ToString())
        ordLn.voidDt = convStrToDt(IIf(useOrig, soLn("void_dt", DataRowVersion.Original).ToString(), soLn("void_dt").ToString()))

        ordLn.picked = IIf(useOrig, soLn("picked", DataRowVersion.Original).ToString(), soLn("picked").ToString())
        tmpStr = IIf(useOrig, soLn("fill_dt", DataRowVersion.Original).ToString(), soLn("fill_dt").ToString())
        ordLn.fillDt = convStrToDt(tmpStr)

        ordLn.discAmt = IIf(useOrig, soLn("disc_amt", DataRowVersion.Original).ToString(), soLn("disc_amt").ToString())
        ordLn.storeCd = IIf(useOrig, soLn("store_cd", DataRowVersion.Original).ToString(), soLn("store_cd").ToString())
        ordLn.locCd = IIf(useOrig, soLn("loc_cd", DataRowVersion.Original).ToString(), soLn("loc_cd").ToString())
        ordLn.outCd = IIf(useOrig, soLn("out_cd", DataRowVersion.Original).ToString(), soLn("out_cd").ToString())
        ordLn.outId = IIf(useOrig, soLn("out_id", DataRowVersion.Original).ToString(), soLn("out_id").ToString())
        ordLn.serNum = IIf(useOrig, soLn("serial_num", DataRowVersion.Original).ToString(), soLn("serial_num").ToString())
        tmpStr = IIf(useOrig, soLn("ooc_qty", DataRowVersion.Original).ToString(), soLn("ooc_qty").ToString())
        ordLn.oocQty = convStrToNum(tmpStr)
        tmpStr = IIf(useOrig, soLn("fifo_cst", DataRowVersion.Original).ToString(), soLn("fifo_cst").ToString())
        ordLn.fifoCst = convStrToNum(tmpStr)
        tmpStr = IIf(useOrig, soLn("fifo_dt", DataRowVersion.Original).ToString(), soLn("fifo_dt").ToString())
        ordLn.fifoDt = convStrToDt(tmpStr)
        tmpStr = IIf(useOrig, soLn("fifl_dt", DataRowVersion.Original).ToString(), soLn("fifl_dt").ToString())
        ordLn.fiflDt = convStrToDt(tmpStr)

        'update slsperson info for the line
        ordLn.empCdSlsp1 = IIf(useOrig, soLn("LN_SLSP1", DataRowVersion.Original).ToString(), soLn("LN_SLSP1").ToString())
        ordLn.empCdSlsp2 = IIf(useOrig, soLn("LN_SLSP2", DataRowVersion.Original).ToString(), soLn("LN_SLSP2").ToString())
        ordLn.pctOfSale1 = IIf(useOrig, soLn("LN_SLSP1_PCT", DataRowVersion.Original).ToString(), soLn("LN_SLSP1_PCT").ToString())
        ordLn.pctOfSale2 = IIf(useOrig, soLn("LN_SLSP2_PCT", DataRowVersion.Original).ToString(), soLn("LN_SLSP2_PCT").ToString())

        ' if orig, then use orig values, may or may not be null; if new, then only put in line taxing if line tax method
        'If useOrig OrElse ' TODO - at this point in time, not fixing line taxing
        '    isNewTaxALineMethod Then
        ordLn.taxCd = IIf(useOrig, soLn("tax_cd", DataRowVersion.Original).ToString(), soLn("tax_cd").ToString())
        ordLn.taxResp = IIf(useOrig, soLn("tax_resp", DataRowVersion.Original).ToString(), soLn("tax_resp").ToString())
        ordLn.taxBasis = convStrToNum(IIf(useOrig, soLn("tax_basis", DataRowVersion.Original).ToString(), soLn("tax_basis").ToString()))
        ordLn.custTaxChg = convStrToNum(IIf(useOrig, soLn("cust_tax_chg", DataRowVersion.Original).ToString(), soLn("cust_tax_chg").ToString()))
        ordLn.warrByLnNum = convStrToNum(IIf(useOrig, soLn("warr_by_ln", DataRowVersion.Original).ToString(), soLn("warr_by_ln").ToString()))
        ordLn.warrBySku = IIf(useOrig, soLn("WARR_BY_SKU", DataRowVersion.Original).ToString(), soLn("WARR_BY_SKU").ToString())
        ordLn.warrByDocNum = IIf(useOrig, soLn("WARR_BY_DOC_NUM", DataRowVersion.Original).ToString(), soLn("WARR_BY_DOC_NUM").ToString())
        ordLn.addonWrDt = Convert.ToDateTime(txt_wr_dt.Text)
        Return ordLn
    End Function

    ''' <summary>
    ''' Extract the datarow for the input ln number from the global sales order lines dataset
    ''' </summary>
    ''' <param name="delDocLnNum">the sales order line (del_Doc_ln# column) number for the line to be extracted</param>
    ''' <returns>row from the global sales order line table for the line number input</returns>
    Private Function getSoLnDatTblRow(ByVal delDocLnNum As String) As DataRow
        Dim soLnRowDetails As DataRow = Nothing

        If delDocLnNum.isNotEmpty Then
            Dim soLnsDataSet As DataSet = Session("DEL_DOC_LN#")
            Dim soLnsDatTbl As DataTable = soLnsDataSet.Tables(0)

            For Each lnRow In soLnsDatTbl.Rows
                If lnRow("DEL_DOC_LN#") = delDocLnNum Then
                    soLnRowDetails = lnRow
                    Exit For
                End If
            Next
        End If

        Return soLnRowDetails
    End Function

    ''' <summary>
    ''' Enables/disables and hides/shows different components based on conditions such as the 
    ''' status of the order, sku attributes, etc. every time the datagrid is refreshed with new 
    ''' order line data.
    ''' </summary>
    Private Sub doAfterBindgrid()
        Dim soLnsDataSet As DataSet = Session("DEL_DOC_LN#")
        Dim soLnsDatTbl As DataTable = soLnsDataSet.Tables(0)
        Dim isInventory As Boolean
        Dim isWarrantable As Boolean
        Dim isTreatable As Boolean
        Dim isVoid As Boolean
        Dim isSerialTp As Boolean
        Dim isResFromOutlet As Boolean
        Dim itmTypeCode As String
        'If the User has both "SOMSCLN" and "SOMSCPCT" securities, then he can chage the sales person 1 and 2 percentage at the line level!
        Dim canChangeSalesPerson As Boolean = SecurityUtils.hasSecurity(SecurityUtils.SOMSCPCT, Session("EMP_CD")) AndAlso SecurityUtils.hasSecurity(SecurityUtils.SOMSCLN, Session("EMP_CD"))
        'show a msg based on if ars store and cannot do reservations
        Dim allowRes As Boolean = AllowReservations()
        lblARSMsg.Visible = (IsOpenSaleOrder() And Not allowRes)
        lblARSMsg.Text = IIf(allowRes, "", Resources.POSMessages.MSG0004)
        chk_reserve_all.Enabled = allowRes   'reservation allowed 
        Dim btnReplace As ImageButton
        Dim lblReplace As Label
        Dim PackageBreakout As String = ConfigurationManager.AppSettings("package_breakout").ToString.ToUpper()
        Dim invStoreCd As String = String.Empty   ' inventory store code
        Dim soLnRowDetails As DataRow = Nothing

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        dbConnection.Open()
        Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConnection)
        Dim availDt As DateTime = Today.Date
        Dim lblAvailResult As Label
        Dim isOutletLoc As Boolean = False
        Dim isShowroomLoc As Boolean = False

        Try
            For Each soLnRow As DataGridItem In GridView1.Items
                Dim tboxQty As TextBox = CType(soLnRow.FindControl("qty"), TextBox)
                Dim lbl_warr As Label = CType(soLnRow.FindControl("lbl_warr"), Label)
                Dim lbl_warr_sku As Label = CType(soLnRow.FindControl("lbl_warr_sku"), Label)
                Dim chkbox_void As CheckBox = CType(soLnRow.FindControl("ChkBoxVoid"), CheckBox)
                Dim retPrc As TextBox = CType(soLnRow.FindControl("unit_prc"), TextBox)
                btnReplace = CType(soLnRow.FindControl("btnReplace"), ImageButton)
                lblReplace = CType(soLnRow.FindControl("lblReplace"), Label)
                Dim locCd As TextBox = CType(soLnRow.FindControl("loc_cd"), TextBox)
                Dim storeCd As TextBox = CType(soLnRow.FindControl("store_cd"), TextBox)

                isInventory = ResolveYnToBooleanEquiv(soLnRow.Cells(SoLnGrid.INVENTORY).Text.Trim())
                isWarrantable = ResolveYnToBooleanEquiv(soLnRow.Cells(SoLnGrid.WARRANTABLE).Text.Trim())
                isTreatable = ResolveYnToBooleanEquiv(soLnRow.Cells(SoLnGrid.TREATABLE).Text().Trim())
                isVoid = ResolveYnToBooleanEquiv(soLnRow.Cells(SoLnGrid.VOID_FLAG).Text().Trim())
                isSerialTp = IsSerialType(soLnRow.Cells(SoLnGrid.SERIAL_TP).Text)
                isResFromOutlet = (soLnRow.Cells(SoLnGrid.OUT_ID).Text).isNotEmpty()
                chkbox_void.Checked = isVoid
                Dim SLSP1 As DropDownList = CType(soLnRow.FindControl("cbo_slsp1"), DropDownList)
                Dim SLSP2 As DropDownList = CType(soLnRow.FindControl("cbo_slsp2"), DropDownList)
                Dim txtLineSlsp1Pct As TextBox = CType(soLnRow.FindControl("txtLineSlsp1Pct"), TextBox)
                Dim txtLineSlsp2Pct As TextBox = CType(soLnRow.FindControl("txtLineSlsp2Pct"), TextBox)

                If (AppConstants.Order.STATUS_FINAL = txt_Stat_cd.Text OrElse
                    AppConstants.Order.STATUS_VOID = txt_Stat_cd.Text OrElse
                    isVoid) Then

                    If (isVoid) Then tboxQty.Text = "0"

                    soLnRow.Cells(SoLnGrid.FAB_CHKBOX).Enabled = False
                    soLnRow.Cells(SoLnGrid.INV_LOOKUP).Enabled = False
                    soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = False
                    soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = False
                    soLnRow.Cells(SoLnGrid.UNIT_PRC).Enabled = False
                    soLnRow.Cells(SoLnGrid.QTY).Enabled = False
                    soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                    '7366


                    SLSP1.Enabled = False
                    SLSP2.Enabled = False
                    txtLineSlsp1Pct.Enabled = False
                    txtLineSlsp2Pct.Enabled = False
                Else
                    '===============================================================
                    ' Lands here for OPEN documents when line is non-voided
                    '===============================================================
                    'Finds the row in the DATASET to get a hold of the complete details for the line
                    soLnRowDetails = getSoLnDatTblRow(soLnRow.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text)
                    itmTypeCode = soLnRowDetails.Item("TYPECODE").ToString

                    'shows drop code (when available)
                    If (Not IsNothing(soLnRowDetails)) Then
                        Dim lbl_itm_warning As Label = CType(soLnRow.FindControl("lbl_itm_warning"), Label)
                        lbl_itm_warning.Visible = (Not IsDBNull(soLnRowDetails("DROP_DT"))) AndAlso IsDate(soLnRowDetails("DROP_DT"))
                        If (lbl_itm_warning.Visible) Then
                            lbl_itm_warning.Text = "SKU was dropped on " & FormatDateTime(soLnRowDetails("DROP_DT").ToString, DateFormat.ShortDate) &
                                                    " (" & soLnRowDetails("DROP_CD").ToString & ")"
                        End If
                    End If

                    soLnRow.Cells(SoLnGrid.FAB_CHKBOX).Enabled = isInventory AndAlso isTreatable
                    soLnRow.Cells(SoLnGrid.INV_LOOKUP).Enabled = isInventory

                    'STORE and LOCATION should be enabled on the following conditions: 
                    '1) line has an INVENTORY sku  AND
                    '2) it is an open Sale Line OR
                    '   2.1) If it is an ARS store, then also it has to be paid in full
                    '3) it is NOT linked to a PO
                    '3) it is a CRM being finalized
                    Dim linkedToPO As Boolean = Not IsNothing(soLnRowDetails) AndAlso
                                                Not IsDBNull(soLnRowDetails("PO_CD")) AndAlso
                                                Not String.IsNullOrEmpty(soLnRowDetails("PO_CD"))
                    Dim isFinalizingCRM As Boolean = (AppConstants.Order.TYPE_CRM = (Session("ord_tp_cd")) AndAlso
                                                      AppConstants.Order.STATUS_FINAL = cbo_stat_cd.SelectedValue)
                    ' While finalizing the CRM store code and location code needs to disbled so I have removed the condtion "isFinalizingCRM"
                    ' MM - 5672
                    ' When CRM is about to finalize then disabling the store, location fields inside the grid
                    If (isFinalizingCRM) Then
                        Dim IsStoreFromInvXref As Boolean = Convert.ToBoolean(soLnRow.Cells(SoLnGrid.IsStoreFromInvXref).Text.Trim())
                        Dim isStoreHasValue As Boolean = False
                        Dim getStoreCode As TextBox = CType(soLnRow.FindControl("store_cd"), TextBox)
                        If (((getStoreCode) IsNot Nothing) AndAlso (Not String.IsNullOrWhiteSpace(getStoreCode.Text))) Then
                            isStoreHasValue = True
                        End If

                        ' MM - 946 / 5762
                        ' For Non Inventory type SKU's store and location fields will be disabled
                        If (Not isInventory) Then
                            soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = False
                            soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = False
                        ElseIf (isInventory AndAlso (Not linkedToPO) AndAlso (IsStoreFromInvXref) AndAlso (isStoreHasValue)) Then
                            soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = False
                            soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = False
                            ' Added as a part of MM-7088
                        ElseIf (SysPms.isRFEnabled AndAlso theInvBiz.IsRfStore(Session("pd_store_cd"))) Then
                            soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = False
                            soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = False
                        Else
                            soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = True
                            soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = True
                        End If
                    Else
                        Dim enableStLoc As Boolean = isInventory AndAlso (Not linkedToPO) AndAlso (allowRes)
                        soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = enableStLoc
                        soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = enableStLoc
                    End If

                    If itmTypeCode = "PKG" Then
                        Dim checkFlag As Boolean
                        Dim prc As Double = retPrc.Text.Trim
                        If prc > 0 Then
                            If CheckSecurity("", Session("EMP_CD")) Then
                                soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                retPrc.Enabled = True
                                tboxQty.Enabled = False
                                checkFlag = True
                            Else
                                retPrc.Enabled = False
                                tboxQty.Enabled = False
                            End If
                            If Not checkFlag Then
                                If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKG_CHG_PRC, Session("EMP_CD"))) Then
                                    retPrc.Enabled = True
                                    tboxQty.Enabled = False
                                End If
                            End If
                        Else
                            If CheckSecurity("", Session("EMP_CD")) Then
                                retPrc.Enabled = False
                                tboxQty.Enabled = False
                                checkFlag = True
                            Else
                                retPrc.Enabled = False
                                tboxQty.Enabled = False
                            End If
                            If Not checkFlag Then
                                If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKG_CHG_PRC, Session("EMP_CD"))) Then
                                    retPrc.Enabled = False
                                    tboxQty.Enabled = False
                                End If
                            End If
                        End If
                        'if the user has the security "PKG_HEADDER_VOID", then he should be able to void the line. This does not holds good if the package is newly added and order is not saved!
                        'If the package is newly added but not saved, then allowed to void the line
                        If CheckSecurity(SecurityUtils.PKG_HEADDER_VOID, Session("EMP_CD")) OrElse Not soLnRow.Cells(SoLnGrid.NEW_LINE).Text.Equals("N") Then
                            soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                        Else
                            soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                        End If
                    ElseIf itmTypeCode = "CMP" Then
                        Dim FilterText As String
                        Dim pkgDelDocLn As String
                        Dim pkgItm As String
                        Dim pkgCmp As String
                        Dim distinctPackageParent() As DataRow
                        Dim checkFlag As Boolean
                        If Not (soLnsDatTbl.Columns.Contains("ISREPLACED")) Then
                            If soLnRow.Cells(SoLnGrid.NEW_LINE).Text = "N" Then
                                pkgItm = soLnRow.Cells(SoLnGrid.ITM_CD).Text.ToString().Trim()
                                FilterText = "[ITM_CD]='" + pkgItm + "'"
                                distinctPackageParent = soLnsDatTbl.Select(FilterText)
                                pkgCmp = distinctPackageParent(0)("PKG_SOURCE").ToString
                                FilterText = "[ITM_CD]='" + pkgCmp + "' and [DEL_DOC_LN#] <" + soLnRow.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text + ""

                            Else
                                pkgDelDocLn = soLnRow.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.ToString().Trim()
                                FilterText = "[DEL_DOC_LN#]='" + pkgDelDocLn + "'"
                                distinctPackageParent = soLnsDatTbl.Select(FilterText)
                                pkgCmp = distinctPackageParent(0)("PACKAGE_PARENT").ToString
                                FilterText = "[DEL_DOC_LN#]='" + pkgCmp + "' and [DEL_DOC_LN#] <" + soLnRow.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text + ""
                            End If
                        Else
                            pkgItm = soLnRow.Cells(SoLnGrid.ITM_CD).Text.ToString().Trim()
                            FilterText = "[ITM_CD]='" + pkgItm + "'"
                            distinctPackageParent = soLnsDatTbl.Select(FilterText)
                            pkgCmp = distinctPackageParent(0)("PKG_SOURCE").ToString
                            FilterText = "[ITM_CD]='" + pkgCmp + "' and [DEL_DOC_LN#] <" + soLnRow.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text + ""

                        End If

                        If Not pkgCmp.Equals("") Then
                            distinctPackageParent = soLnsDatTbl.Select(FilterText)
                        End If
                        Dim prc As Double = 0
                        If distinctPackageParent.Length > 0 Then
                            prc = distinctPackageParent(distinctPackageParent.Length - 1)("UNIT_PRC").ToString
                        End If
                        If prc > 0 Then
                            If CheckSecurity("", Session("EMP_CD")) Then
                                soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                retPrc.Enabled = False
                                tboxQty.Enabled = True
                                btnReplace.Enabled = True
                                checkFlag = True
                            Else
                                soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                retPrc.Enabled = False
                                tboxQty.Enabled = False
                                btnReplace.Enabled = False
                            End If
                            If Not checkFlag Then
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG, Session("EMP_CD")) Then
                                    retPrc.Enabled = False
                                    tboxQty.Enabled = False
                                    soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                    btnReplace.Enabled = True
                                End If
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG_QTY, Session("EMP_CD")) Then
                                    soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    tboxQty.Enabled = False
                                End If
                                If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKGCMPT_CHG_PRC, Session("EMP_CD"))) Then
                                    retPrc.Enabled = False
                                End If
                            End If
                        ElseIf prc = 0 Then
                            If CheckSecurity("", Session("EMP_CD")) Then
                                soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                retPrc.Enabled = True
                                tboxQty.Enabled = True
                                btnReplace.Enabled = True
                                checkFlag = True
                            Else
                                soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                retPrc.Enabled = False
                                tboxQty.Enabled = False
                                btnReplace.Enabled = False
                            End If
                            If Not checkFlag Then
                                'The security "PKGCMPT_CHG" does not allow the changing of a SKU on a line.The line must be deleted or voided and then a new line must be added.
                                'And this security will not allow changes to the qty.
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG, Session("EMP_CD")) Then
                                    retPrc.Enabled = False
                                    tboxQty.Enabled = False
                                    soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                    btnReplace.Enabled = True
                                End If
                                'This security "PKGCMPT_CHG_QTY" will allow to change the qty alone.  
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG_QTY, Session("EMP_CD")) Then
                                    soLnRow.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    tboxQty.Enabled = True
                                End If
                                'This security "SOM_CHG_PRC" for SOM+screen, it will allows only to change retail price for any line except package sku if it is explored.
                                ' This security "PKGCMPT_CHG_PRC" is to change the retail price of the component.This is on top of SOERET.
                                'The user must have both to be able to change the component price.
                                If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKGCMPT_CHG_PRC, Session("EMP_CD"))) Then
                                    retPrc.Enabled = True
                                End If
                            End If
                        End If
                    Else
                        'the QTY should not be enterable when line is linked to PO
                        tboxQty.Enabled = (Not linkedToPO)

                        retPrc.Enabled = False
                        If CheckSecurity("", Session("EMP_CD")) Then
                            retPrc.Enabled = True
                        Else
                            retPrc.Enabled = False
                        End If
                        If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) Then
                            retPrc.Enabled = True
                        End If

                    End If

                    ''MM-6635 - if SHU'd, then price and qty not updatable, no exceptions
                    If soLnsDatTbl.Rows(soLnRow.DataSetIndex)("SHU").ToString() = "Y" Then

                        retPrc.Enabled = False
                        tboxQty.Enabled = False
                    End If

                    'MM-5805
                    If Not soLnsDatTbl.Rows(soLnRow.DataSetIndex)("PKG_SOURCE").ToString().Equals("") AndAlso
                        soLnsDatTbl.Rows(soLnRow.DataSetIndex)("SHU").ToString() = "N" AndAlso
                        soLnsDatTbl.Rows(soLnRow.DataSetIndex)("VOID_FLAG").ToString() <> "Y" AndAlso
                        txt_Stat_cd.Text = "O" Then

                        ' btnReplace.Visible = True
                        ' lblReplace.Visible = True

                        btnReplace.Visible = False   'lucy
                        lblReplace.Visible = False    'lucy

                        Dim drFiltered As DataRow()
                        'If drFiltered.Length = 0 Then

                        If Not ViewState("REPLACED") Is Nothing Then
                            Dim toBeReplace As DataTable = CType(ViewState("REPLACED"), DataTable)
                            drFiltered = toBeReplace.Select("LINE = '" + soLnRow.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.ToString().Trim() + "'")
                            If drFiltered.Length > 0 Then
                                lblReplace.Text = "Replaced with : " + drFiltered(0)("OldSku").ToString().Trim()

                            End If
                        End If
                    Else
                        btnReplace.Visible = False
                        lblReplace.Visible = False
                    End If

                    invStoreCd = If(IsDBNull(soLnRowDetails("STORE_CD")), String.Empty,
                                                  If(IsNothing(soLnRowDetails("STORE_CD")), String.Empty, soLnRowDetails("STORE_CD")))

                    'sets the PO info, which includes date and PO #
                    If linkedToPO AndAlso String.IsNullOrEmpty(invStoreCd) Then
                        Dim lbl_poDes As Label = CType(soLnRow.FindControl("lbl_PO"), Label)

                        ' Display PO description #ITREQUEST-337
                        Dim poDesc As String = String.Empty
                        If Not IsDBNull(soLnRowDetails("PO_DESC")) Then
                            ' Set PO description
                            poDesc = soLnRowDetails("PO_DESC")

                            ' Find last index of "(" to display "po_arr_dt" date to next line
                            Dim poDescBracketIndex As Integer = poDesc.LastIndexOf("(")
                            If (poDescBracketIndex <> -1) Then
                                poDesc = poDesc.Remove(poDescBracketIndex, "(".Length).Insert(poDescBracketIndex, "<br/>(")
                            End If

                            ' Display booking status if exists
                            If soLnRowDetails.Table.Columns.Contains("BOOKING_STATUS") AndAlso Not IsDBNull(soLnRowDetails("BOOKING_STATUS")) Then
                                poDesc += " " & soLnRowDetails("BOOKING_STATUS")
                            End If

                            ' Set PO description
                            lbl_poDes.Text = poDesc
                        End If
                    End If


                    If (isInventory AndAlso (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") OrElse AppConstants.Order.TYPE_CRM = Session("ord_tp_cd")) AndAlso AppConstants.Order.STATUS_OPEN = txt_Stat_cd.Text) Then
                        isOutletLoc = False
                        isShowroomLoc = False
                        'hides/shows the Inventory Count
                        UpdateInvCount(soLnRow)
                        'hides/shows the available date components
                        If (storeCd.Text.isNotEmpty) AndAlso (locCd.Text.isNotEmpty) Then
                            isOutletLoc = theInvBiz.IsOutletLocation(storeCd.Text, locCd.Text, dbCommand)
                        End If
                        ' DB Jan 11 2018
                        'Jan 11 2018 Showroom items manual fill
                        If (Not isOutletLoc And (storeCd.Text.isNotEmpty) AndAlso (locCd.Text.isNotEmpty)) Then
                            isShowroomLoc = LeonsBiz.IsShowroomLocation(storeCd.Text, locCd.Text)
                        End If

                        If (Session("IsARSStore") And (Not isOutletLoc) And (Not isShowroomLoc)) Then
                            availDt = GetAvailDate(soLnRowDetails)
                        End If

                        If (isOutletLoc) Then
                            availDt = Date.Today
                            If Not (Session("pd_store_cd").Equals(storeCd.Text)) Then
                                If (IsNothing(ViewState("zoneLeadDays")) AndAlso String.IsNullOrWhiteSpace(ViewState("zoneLeadDays"))) Then
                                    ViewState("zoneLeadDays") = theTMBiz.GetZoneLeadDays(SessVar.zoneCd)
                                End If
                                availDt = availDt.AddDays(ViewState("zoneLeadDays"))
                            End If
                        End If
                        'Jan 11 2018 Showroom items manual fill
                        If (isShowroomLoc And Session("IsARSStore")) Then
                            availDt = Date.Today
                            If Not (Session("pd_store_cd").Equals(storeCd.Text)) Then
                                If (IsNothing(ViewState("zoneLeadDays")) AndAlso String.IsNullOrWhiteSpace(ViewState("zoneLeadDays"))) Then
                                    ViewState("zoneLeadDays") = theTMBiz.GetZoneLeadDays(SessVar.zoneCd)
                                End If
                                availDt = availDt.AddDays(ViewState("zoneLeadDays"))
                            End If
                        End If

                        ViewState("MaxAvailDdate") = availDt
                        lblAvailResult = CType(soLnRow.FindControl("lblAvailResults"), Label)
                        Dim lbl_poDes As Label = CType(soLnRow.FindControl("lbl_PO"), Label)
                        If (Session("IsARSStore")) Then
                            If Not (availDt = DateTime.MinValue) And isInventory Then
                                lblAvailResult.Text = Resources.POSMessages.MSG0002 & availDt.ToString("dd-MMM-yyyy")
                                lblAvailResult.Visible = True
                            End If
                        Else
                            ViewState("MaxAvailDdate") = Today.Date
                            lblAvailResult.Visible = False
                        End If
                        If linkedToPO Then
                            'soLn("POArrival") 
                            If FormatDateTime(soLnRowDetails("POArrival") & "", DateFormat.ShortDate) < FormatDateTime(availDt, DateFormat.ShortDate) Then
                                availDt = FormatDateTime(soLnRowDetails("POArrival") & "", DateFormat.ShortDate)
                                lblAvailResult.Visible = False
                            Else
                                lblAvailResult.Visible = True
                            End If
                        End If
                        'If IsNothing(lbl_poDes) = False AndAlso Not lbl_poDes.Text.Trim().Equals(String.Empty) Then
                        '    'Means the line item is linked to PO, hide the item available available date field.
                        '    lblAvailResult.Visible = False
                        '    Dim PODetails() As String = lbl_poDes.Text.Split("(")
                        '    Dim PODate As DateTime
                        '    If PODetails.Length > 1 Then
                        '        PODate = FormatDateTime(PODetails(1).Replace(")", ""), DateFormat.ShortDate)
                        '    End If

                        'Else
                        '    'Do Nothing
                        'End If
                    End If
                    'Check for the security 
                    If canChangeSalesPerson Then
                        'Do Nothing
                    Else
                        SLSP1.Enabled = False
                        SLSP2.Enabled = False
                        txtLineSlsp1Pct.Enabled = False
                        txtLineSlsp2Pct.Enabled = False
                    End If
                End If
                'warranty components
                If (isInventory AndAlso isWarrantable) Then
                    Dim isWarranted As Boolean = (soLnRow.Cells(SoLnGrid.WARR_BY_SKU).Text + "").isNotEmpty
                    Dim cbWarr As CheckBox = CType(soLnRow.FindControl("ChkBoxWarr"), CheckBox)
                    cbWarr.Checked = isWarranted
                    lbl_warr.Visible = isWarranted
                    lbl_warr_sku.Visible = isWarranted
                    soLnRow.Cells(SoLnGrid.WARR_CHKBOX).Enabled = (AppConstants.Order.TYPE_SAL = (Session("ord_tp_cd")) AndAlso
                                                                   AppConstants.Order.STATUS_OPEN = txt_Stat_cd.Text)
                Else
                    lbl_warr.Visible = False
                    lbl_warr_sku.Visible = False
                    soLnRow.Cells(SoLnGrid.WARR_CHKBOX).Enabled = False
                End If

                'serial number components
                Dim lbl_sernum As Label = CType(soLnRow.FindControl("lbl_sernum"), Label)
                Dim lbl_sernumval As Label = CType(soLnRow.FindControl("lbl_sernumval"), Label)
                lbl_sernum.Visible = isSerialTp
                lbl_sernumval.Visible = isSerialTp

                'Outlet ID components
                Dim lbl_outnum As Label = CType(soLnRow.FindControl("lbl_outnum"), Label)
                Dim lbl_outnumval As Label = CType(soLnRow.FindControl("lbl_outnumval"), Label)
                lbl_outnum.Visible = isResFromOutlet
                lbl_outnumval.Visible = isResFromOutlet

                If (isSerialTp OrElse isResFromOutlet) Then
                    soLnRow.Cells(SoLnGrid.QTY).Enabled = False
                End If


                ' leave-in-carton components
                ' Daniela Nov 11 hide Leave in carton
                'Dim lbl_carton As Label = CType(soLnRow.FindControl("lbl_carton"), Label)
                'Dim chk_carton As CheckBox = CType(soLnRow.FindControl("chk_carton"), CheckBox)
                'lbl_carton.Visible = isInventory
                'chk_carton.Visible = isInventory
                'chk_carton.Enabled = (isInventory AndAlso Not isVoid)
                'chk_carton.Checked = ("Y" = soLnRow.Cells(SoLnGrid.LV_IN_CARTON).Text.Trim())

                ' treated flag
                Dim cbFab As CheckBox = DirectCast(soLnRow.FindControl("SelectFab"), CheckBox)
                cbFab.Checked = (soLnRow.Cells(SoLnGrid.TREATED_BY_ITM).Text + "").isNotEmpty
            Next
            dbConnection.Close()
        Catch ex As Exception
            Throw
        Finally
            dbConnection.Dispose()
            dbCommand.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Checks if the Inventory count should be displayed and if so, 
    ''' retrieves the count to display on it.
    ''' </summary>
    ''' <param name="soLnRow">the datagrid row being modified</param>
    Private Sub UpdateInvCount(ByRef soLnRow As DataGridItem)
        Dim lbl_invcount As Label = CType(soLnRow.FindControl("lbl_invcount"), Label)
        lbl_invcount.Visible = (AppConstants.Order.TYPE_SAL = txt_ord_tp_cd.Text)

        If (lbl_invcount.Visible) Then
            Dim totalWhse As Integer = 0
            Dim lbl_sku As Label = CType(soLnRow.FindControl("lbl_SKU"), Label)
            Dim skuId As String = lbl_sku.Text.Trim

            If (SysPms.priorityFillForInvAvail) Then
                Dim pfReq As PriFillReq = New PriFillReq
                pfReq.itmCd = skuId
                pfReq.homeStoreCd = SessVar.homeStoreCd
                pfReq.puDel = SessVar.puDel
                pfReq.puDelStoreCd = SessVar.puDelStoreCd
                pfReq.takeWith = "N"
                pfReq.zoneCd = SessVar.zoneCd
                pfReq.zoneZipCd = SessVar.zipCode4Zone
                pfReq.locTpCd = AppConstants.Loc.TYPE_CD_W
                totalWhse = GetQtyCount(theInvBiz.GetInvPriFill(pfReq))
            Else
                'NOTE: When SYSPM priority fill is on, the API takes into account the soft-res
                '      but in this case counts have to be adjusted manually
                Dim softRes As DataSet = theInvBiz.GetReservations(skuId)
                Dim whseInv As DataSet = theInvBiz.GetStoreData(skuId, AppConstants.Loc.TYPE_CD_W)
                totalWhse = GetQtyCount(InventoryUtils.RemoveResCounts(whseInv, softRes, AppConstants.Loc.TYPE_CD_W))
            End If
            lbl_invcount.Text = totalWhse
        End If
    End Sub


    ''' <summary>
    ''' Executes when the 'warranty' check box in the item details tab grid is checked or unchecked
    ''' </summary>
    Protected Sub CheckWarrClicked(ByVal sender As Object, ByVal e As EventArgs)
        Dim chkWarr As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(chkWarr.NamingContainer, DataGridItem)
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
            Exit Sub
        End If
        If chkWarr.Checked = False AndAlso cbo_related_docs.Visible Then
            ViewState("WarrRow") = dgItem.ItemIndex
            ValidateSplitOrder(dgItem)
        Else
            RemoveWarranty(dgItem)
        End If
    End Sub



    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        Session("OpenIST") = Nothing ' lucy 11-aug-16
        lbl_warning.Text = String.Empty  ' lucy 11-aug-16
        '---needs to unreserve inventory for those lines that are SOFT-RESERVED
        UnreserveInventory()

        lbl_changes_found.Text = ""
        lblTax.Text = "0.00"
        lblDelivery.Text = "0.00"

        txt_del_doc_num.Text = ""
        txt_wr_dt.Text = ""
        txt_ord_tp_cd.Text = ""
        txt_store_cd.Text = ""
        txt_Stat_cd.Text = ""
        txt_Stat_dt.Text = ""
        txt_pd_display.Text = ""
        txt_pd_dt_display.Text = ""
        txtSearchZone.Text = ""
        txt_fi_acct.Text = ""
        txt_fi_appr.Text = ""
        txt_fi_amt.Text = ""
        lbl_tax_cd.Text = ""

        txt_slsp1.Text = ""
        txt_slsp2.Text = ""
        txt_comm1.Text = ""
        txt_comm2.Text = ""

        txt_Cust_cd.Text = ""
        txt_f_name.Text = ""
        txt_l_name.Text = ""
        txt_addr1.Text = ""
        txt_addr2.Text = ""
        txt_h_phone.Text = ""
        txt_b_phone.Text = ""
        txt_city.Text = ""
        cbo_State.SelectedValue = ""
        txt_zip.Text = ""
        txt_email.Text = ""

        txt_del_doc_num.Enabled = True
        txt_Cust_cd.Enabled = True
        txt_pd_display.Enabled = True
        txt_pd_dt_display.Enabled = True
        txtSearchZone.Enabled = True

        chk_email.Visible = False

        btnConfigureEmail.Visible = False
        btn_payments.Enabled = False
        btn_invoice.Enabled = False
        btn_Lookup.Visible = False
        ' Daniela reprint
        btn_reprint.Enabled = False

        Session("DEL_DOC") = System.DBNull.Value
        Session("DEL_DOC_LN#") = System.DBNull.Value
        Session("ORIG_SO") = System.DBNull.Value
        Session("ORIG_SO") = Nothing
        Session("ORIG_SO_TOTAL") = Nothing
        Session("ORIG_DEL_DOC_NUM") = Nothing
        Session("origSoWrDt") = Nothing
        Session("DEL_DOC") = Nothing
        Session("DEL_DOC_LN#") = Nothing
        Session("USR_FLD_1") = Nothing
        Session("USR_FLD_2") = Nothing
        Session("USR_FLD_3") = Nothing
        Session("USR_FLD_4") = Nothing
        Session("USR_FLD_5") = Nothing
        Session("ord_tp_cd") = Nothing
        Session("pd_store_cd") = Nothing
        Session("SPLIT_SO_LN") = Nothing
        Session("MaxCredList") = Nothing
        Session("referrerToSom") = Nothing ' cannot go back if clear in SOM
        Session("TAX_CD") = Nothing
        Session("TAX_RATE") = Nothing
        Session("MANUAL_TAX_CD") = Nothing
        Session("STORE_ENABLE_ARS") = False
        Session("IsARSStore") = Nothing
        Session("DISC_APPLIED") = Nothing
        Session("UNRESERVE") = Nothing
        'clear the comments 
        Session("scomments") = Nothing
        Session("dcomments") = Nothing
        Session("arcomments") = Nothing
        Session("hidUpdLnNos") = Nothing
        DeleteRecordonButtonClick()
        Session("lucy_cmnt") = ""   'lucy
        Session("from_lucy_cmnt") = "NO" 'lucy

        Response.Redirect("SalesOrderMaintenance.aspx")

    End Sub

    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        Session("OpenIST") = Nothing ' lucy 11-aug-16
        lbl_warning.Text = Nothing   ' lucy 11-aug-16
        lbl_desc.Text = String.Empty
        Dim lookupOrders As Boolean = True
        Dim wrtnDate As String = If(String.IsNullOrEmpty(txt_wr_dt.Text), "", txt_wr_dt.Text.Trim())
        Dim fromDateVal As String = If(String.IsNullOrEmpty(txt_pd_dt_display.Text), "", txt_pd_dt_display.Text.Trim())
        Dim toDateVal As String = If(String.IsNullOrEmpty(txtPDDateTodate.Text), "", txtPDDateTodate.Text.Trim())

        If (wrtnDate.isNotEmpty() AndAlso Not IsDate(wrtnDate)) Then
            lbl_desc.Text = "You have entered an invalid written date.  Please correct and resubmit." & Environment.NewLine
            txt_wr_dt.Focus()
            lookupOrders = False
        End If

        If (fromDateVal.isNotEmpty() AndAlso Not IsDate(fromDateVal)) Then
            lbl_desc.Text += "You have entered an invalid pickup/delivery From date.  Please correct and resubmit." & Environment.NewLine
            txt_pd_dt_display.Focus()
            lookupOrders = False
        End If

        If (toDateVal.isNotEmpty() AndAlso Not IsDate(toDateVal)) Then
            lbl_desc.Text += "You have entered an invalid pickup/delivery To date.  Please correct and resubmit." & Environment.NewLine
            txtPDDateTodate.Focus()
            lookupOrders = False
        End If

        If ((fromDateVal.isEmpty() AndAlso toDateVal.isNotEmpty()) OrElse (fromDateVal.isNotEmpty() AndAlso toDateVal.isEmpty())) Then
            lbl_desc.Text += "You have to enter both dates for date range.  Please correct and resubmit." & Environment.NewLine
            lookupOrders = False
            If fromDateVal.isEmpty() Then
                txt_pd_dt_display.Focus()
            Else
                txtPDDateTodate.Focus()
            End If
        ElseIf IsDate(fromDateVal) AndAlso IsDate(toDateVal) AndAlso DateTime.Parse(fromDateVal) > DateTime.Parse(toDateVal) Then
            lbl_desc.Text += "You have entered an invalid pickup/delivery To date range.  Please correct and resubmit." & Environment.NewLine
            txtPDDateTodate.Focus()
            lookupOrders = False
        End If

        Dim orderTp As String = If(String.IsNullOrEmpty(txt_ord_tp_cd.Text), "", txt_ord_tp_cd.Text.Trim.ToUpper())
        If (Not String.IsNullOrEmpty(orderTp) AndAlso
            AppConstants.Order.TYPE_SAL <> orderTp AndAlso AppConstants.Order.TYPE_CRM <> orderTp AndAlso
            AppConstants.Order.TYPE_MCR <> orderTp AndAlso AppConstants.Order.TYPE_MDB <> orderTp) Then
            lbl_desc.Text += "You have entered an invalid order type. Valid types are: SAL, CRM, MCR, MDB.  Please correct and resubmit." & Environment.NewLine
            txt_ord_tp_cd.Focus()
            lookupOrders = False
        End If

        Dim pdFlag As String = If(String.IsNullOrEmpty(txt_pd_display.Text), "", txt_pd_display.Text.Trim.ToUpper())
        If (Not String.IsNullOrEmpty(pdFlag) AndAlso AppConstants.PICKUP <> pdFlag AndAlso AppConstants.DELIVERY <> pdFlag) Then
            lbl_desc.Text += "You have entered an invalid type. Valid types are: P, D.  Please correct and resubmit." & Environment.NewLine
            txt_pd_display.Focus()
            lookupOrders = False
        End If

        Dim statCd As String = If(String.IsNullOrEmpty(txt_Stat_cd.Text), "", txt_Stat_cd.Text.Trim.ToUpper())
        If (Not String.IsNullOrEmpty(statCd) AndAlso AppConstants.Order.STATUS_FINAL <> statCd AndAlso
            AppConstants.Order.STATUS_VOID <> statCd AndAlso AppConstants.Order.STATUS_OPEN <> statCd) Then
            lbl_desc.Text += "You have entered an invalid status type. Valid types are: O, F, V.  Please correct and resubmit." & Environment.NewLine
            txt_Stat_cd.Focus()
            lookupOrders = False
        End If

        If lookupOrders Then
            Dim lookupRequest As New OrderRequestDtc
            lookupRequest.docNum = txt_del_doc_num.Text.Trim
            lookupRequest.wrDt = txt_wr_dt.Text
            lookupRequest.ordTp = txt_ord_tp_cd.Text
            lookupRequest.empCdOp = Session("EMP_CD")
            lookupRequest.custCd = txt_Cust_cd.Text
            lookupRequest.statCd = txt_Stat_cd.Text
            lookupRequest.shipFName = txt_f_name.Text.Trim
            lookupRequest.shipLName = txt_l_name.Text.Trim
            lookupRequest.shipHPhone = txt_h_phone.Text.Trim
            lookupRequest.shipBPhone = txt_b_phone.Text.Trim
            lookupRequest.shipAddr1 = txt_addr1.Text.Trim
            lookupRequest.shipAddr2 = txt_addr2.Text.Trim
            lookupRequest.shipcity = txt_city.Text
            lookupRequest.shipState = cbo_State.SelectedValue.ToString()
            lookupRequest.shipZip = txt_zip.Text
            lookupRequest.puDel = txt_pd_display.Text
            lookupRequest.puDelDt = txt_pd_dt_display.Text
            lookupRequest.orderDateTo = txtPDDateTodate.Text.Trim
            lookupRequest.soStore = txt_store_cd.Text
            lookupRequest.corpName = txt_corp_name.Text
            lookupRequest.slspInit1 = txt_slsp1.Text
            lookupRequest.slspInit2 = txt_slsp2.Text
            lookupRequest.allForSlspCd = ""
            lookupRequest.custTpCd = ""
            lookupRequest.queryOrderDateRange = True
            lookupRequest.shipToName = True
            lookupRequest.shipToHPhone = True
            lookupRequest.orderZone = txtSearchZone.Text.ToUpper().Trim()
            lookupRequest.orderUserField1 = Session("USR_FLD_1").ToString().Trim()
            lookupRequest.orderUserField2 = Session("USR_FLD_2").ToString().Trim()
            lookupRequest.orderUserField3 = Session("USR_FLD_3").ToString().Trim()
            lookupRequest.orderUserField4 = Session("USR_FLD_4").ToString().Trim()
            lookupRequest.orderUserField5 = Session("USR_FLD_5").ToString().Trim()
            lookupRequest.poNumber = txt_po.Text.Trim
            lookupRequest.emailAddr = txt_email.Text.Trim
            If (ddlConfStaCd.SelectedItem Is Nothing) Then
                lookupRequest.orderQueryRequestFrom = "0"
                lookupRequest.confStatusCd = String.Empty
            Else
                lookupRequest.orderQueryRequestFrom = ddlConfStaCd.SelectedItem.Value
                lookupRequest.confStatusCd = ddlConfStaCd.SelectedItem.Value
            End If

            Session("OrderRequest") = lookupRequest
            Response.Redirect("Main_SO_Lookup.aspx")
        End If

    End Sub

    ' start lucy add, Req-214, Nov, 2019
    Public Sub Save_spc_record(ByRef p_flag As String)


        If txt_spc_num.Text.Length = 16 Then
            Dim proceed As Boolean = False
            Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Try


                conn.Open()
                Dim sql As String = "UPDATE SO SET USR_FLD_5=:spc_num  WHERE DEL_DOC_NUM = :DEL_DOC_NUM"
                Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)

                cmd.Parameters.Add(":spc_num", OracleType.VarChar)
                cmd.Parameters(":spc_num").Value = "SPC" + txt_spc_num.Text
                cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
                cmd.ExecuteNonQuery()


                p_flag = "Y"
            Catch ex As Exception
                p_flag = "N"
                Throw
            Finally
                conn.Close()
            End Try


        End If

    End Sub

    Public Sub clear_spc_num_hint()
        If Left(txt_spc_num.Text, 3) = "SPC" Then
            txt_spc_num.Text = Nothing

        End If

    End Sub

    Public Sub btn_spc_save_click(sender As Object, e As EventArgs)

        Dim str_flag As String
        If txt_spc_num.Text.Length = 16 Then
            str_flag = "N"
            Save_spc_record(str_flag)
            If str_flag = "Y" Then
                lbl_msg.Text = Resources.LibResources.Label1203
            Else
                lbl_msg.Text = Resources.LibResources.Label1204
            End If
        Else
            lbl_msg.Text = Resources.LibResources.Label1205
        End If
    End Sub


    Protected Sub btn_spc_exit_Click(sender As Object, e As EventArgs)
        txt_spc_num.Text = Nothing
        lbl_msg.Text = Nothing
        ASPxPopupControl_spc_num.ShowOnPageLoad = False

    End Sub

    Protected Sub btn_spc_clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_spc_clear.Click

        txt_spc_num.Text = Nothing
        lbl_msg.Text = Nothing
    End Sub

    Protected Sub btn_add_spc_Click(sender As Object, e As EventArgs) Handles btn_add_spc.Click
        ' lucy req-214, Nov-19
        Dim strco_cd As String
        strco_cd = Session("CO_CD")
        If Not String.IsNullOrEmpty(txt_del_doc_num.Text) Then
            If strco_cd <> "LFL" Then

                ASPxPopupControl_spc_num.ShowOnPageLoad = True
            End If
        End If
    End Sub

    ' end lucy add


    ''' <summary>
    ''' Checks if for an ARS store, the order is considered as paid in full or not based on the call to
    ''' an API and security.
    ''' </summary>
    ''' <returns>a flag indicating if reservations should be allowed or not</returns>
    Private Function AllowReservations() As Boolean
        Dim rtnVal As Boolean = False
        If IsOpenSaleOrder() Then
            rtnVal = True
            If (Session("STORE_ENABLE_ARS")) Then
                rtnVal = IsSalePaidInFull()
            End If
        End If
        Return rtnVal
    End Function

    ''' <summary>
    ''' Finds out if the current order is paid in full.  If order is not 
    ''' then it checks if the user has the security to override it.
    ''' >>>>>>>>>>>>>>>  Intended only for SALE OPEN orders <<<<<<<<<<<<<<<<
    ''' </summary>
    ''' <returns>TRUE if order can be considered paid in full; FALSE otherwise</returns>
    Private Function IsSalePaidInFull() As Boolean

        Dim rtnVal As Boolean = False
        Dim pifReq As New PaidInFullReq
        pifReq.ordTpCd = Session("ord_tp_cd")
        pifReq.custCd = txt_Cust_cd.Text
        pifReq.delDocNum = txt_del_doc_num.Text.Trim.ToUpper
        pifReq.writtenStoreCd = txt_store_cd.Text
        pifReq.finCoCd = cbo_finance_company.SelectedValue
        pifReq.finApp = txt_fi_appr.Text
        pifReq.origFinAmt = txt_fi_amt.Text

        Dim paidInFull As Boolean = theSalesBiz.IsPaidInFull(pifReq)
        If paidInFull Then
            'Since the Paid in full returns as true when it it is queried in SOM+ for the first time(based on SOE+ status), if new line are added, then the order
            'stands not paid in full. if the custom implementation is in place, then whatever the package returns will be final
            If SysPmsConstants.PAID_IN_FULL_CALC_CUSTOM = SysPms.paidInFullCalc Then
                rtnVal = True
            Else
                'If the custom paid in full logic not in place, then if the the balance amount is > 0, then we are treating the order as not paid in full.
                If CDbl(lbl_balance.Text) > 0 Then
                    rtnVal = False
                Else
                    rtnVal = True
                End If
            End If
        Else
            'do nothing
            'rtnVal = CheckSecurity(SecurityUtils.OVERRIDE_PAID_IN_FULL, Session("emp_cd"))
        End If
        Return rtnVal

    End Function

    ''' <summary>
    ''' Retrieves and sets the order details based on the search criteria specified.
    ''' </summary>
    Private Sub PopulateResults()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        Dim oAdp As OracleDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sql As String
        Dim DEL_DOC_NUM As String
        cboTargetDate.Visible = False
        lblFilterDate.Visible = False
        txt_del_doc_num.Enabled = False
        btn_Lookup.Visible = False
        ' Daniela reprint
        'btn_invoice.Enabled = True
        btn_reprint.Enabled = True

        DEL_DOC_NUM = Request("del_doc_num")
        sql = "SELECT SO.*, CUST.EMAIL_ADDR, CUST.TET_ID#, tax_method, tax.des as tax_des, e1.emp_init AS slsp1, e2.emp_init AS slsp2, s.zip_cd AS store_zip " &
                "FROM emp e1, emp e2, CUST, tax_cd tax, store s, so " &
                "WHERE SO.CUST_CD = CUST.CUST_CD " &
                "AND e1.emp_cd = so.so_emp_slsp_cd1 AND e2.emp_cd (+) = so.so_emp_slsp_cd2 " &
                "AND S.STORE_CD =  SO.PU_DEL_STORE_CD " &
                " AND so.tax_cd = tax.tax_cd (+) AND DEL_DOC_NUM='" & DEL_DOC_NUM & "'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Session("ORIG_SO") = ds
        'Daniela count check
        If ds.Tables(0).Rows.Count = 0 Then
            Throw New Exception(DEL_DOC_NUM & " does not exist.")
        End If
        '--sets the original header discount (if any) and all discount details; avoid resetting if processing same doc;
        SessVar.discCd = If(IsDBNull(ds.Tables(0).Rows(0)("disc_cd")), String.Empty, ds.Tables(0).Rows(0)("disc_cd"))
        If (SessVar.discCd.isNotEmpty()) AndAlso Not SameDocInProcess(DEL_DOC_NUM) Then

            Dim ln2Discs As DataTable = theSalesBiz.GetDiscountsApplied(DEL_DOC_NUM, SessVar.discCd, False, False) ' for SOM populate results from so_ln or so_ln2disc
            Session("DISC_APPLIED") = ln2Discs
            If SystemUtils.dataTblIsNotEmpty(ln2Discs) Then

                ' sort to find if existing disc or not
                Dim drhdrDisc As DataRow() = ln2Discs.Select("DISC_LEVEL='H'")
                If drhdrDisc.Length > 0 AndAlso SystemUtils.dataRowIsNotEmpty(drhdrDisc(0)) Then

                    SessVar.existHdrDiscCd = drhdrDisc(0)("DISC_CD")
                End If

            End If
        End If

        ' MM-4240
        ' To get the tax method in to view state
        ViewState("TaxMethod") = ds.Tables(0).Rows(0)("TAX_METHOD").ToString()
        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then

                'Check first if the written date is a valid date and abort processing if NOT.
                If Not IsDBNull(MyDataReader.Item("SO_WR_DT")) AndAlso Not (IsDate(MyDataReader.Item("SO_WR_DT").ToString)) Then
                    lbl_desc.Text = "Invalid Written Date."
                    Exit Sub
                End If

                'if here means that the written date is valid, so set it
                txt_wr_dt.Text = FormatDateTime(MyDataReader.Item("SO_WR_DT").ToString, DateFormat.ShortDate)
                txt_wr_dt.Enabled = False
                cbo_PD.Enabled = True
                cbo_pd_store_cd.Enabled = True
                Session("USR_FLD_1") = MyDataReader.Item("USR_FLD_1").ToString
                Session("USR_FLD_2") = MyDataReader.Item("USR_FLD_2").ToString
                Session("USR_FLD_3") = MyDataReader.Item("USR_FLD_3").ToString
                Session("USR_FLD_4") = MyDataReader.Item("USR_FLD_4").ToString
                Session("USR_FLD_5") = MyDataReader.Item("USR_FLD_5").ToString
                Session("ORIG_DEL_DOC_NUM") = MyDataReader.Item("ORIG_DEL_DOC_NUM").ToString
                txt_del_doc_num.Text = MyDataReader.Item("DEL_DOC_NUM").ToString
                txt_del_doc_num.Enabled = False
                Session("pd_store_cd") = MyDataReader.Item("PU_DEL_STORE_CD").ToString
                cbo_pd_store_cd.Value = MyDataReader.Item("PU_DEL_STORE_CD").ToString
                final_store_cd.SelectedValue = MyDataReader.Item("SO_STORE_CD").ToString
                lbl_curr_pd_store.Text = MyDataReader.Item("PU_DEL_STORE_CD").ToString
                Session("PD") = MyDataReader.Item("PU_DEL").ToString
                Session("ZONE_ZIP_CD") = MyDataReader("STORE_ZIP").ToString
                '***** set if store is ARS enabeld
                Dim store As StoreData = theSalesBiz.GetStoreInfo(MyDataReader.Item("SO_STORE_CD").ToString)
                Session("STORE_ENABLE_ARS") = store.enableARS
                Session("IsARSStore") = store.enableARS

                cbo_PD.Value = MyDataReader.Item("PU_DEL").ToString
                txt_pd_display.Text = MyDataReader.Item("PU_DEL").ToString
                txt_pd_display.Enabled = False
                txt_pd_dt_display.Text = FormatDateTime(MyDataReader.Item("PU_DEL_DT").ToString, DateFormat.ShortDate)
                txt_pd_dt_display.Enabled = False

                Try
                    txtSearchZone.Text = MyDataReader.Item("SHIP_TO_ZONE_CD").ToString.Trim()
                    txtSearchZone.Enabled = False
                Catch ex As Exception

                End Try

                txtPDDateTodate.Enabled = False
                lbl_curr_zone.Text = MyDataReader.Item("SHIP_TO_ZONE_CD").ToString ' populated for both pickups and deliveries
                Session("ZONE_CD") = lbl_curr_zone.Text
                lbl_curr_dt.Text = FormatDateTime(MyDataReader.Item("PU_DEL_DT").ToString, DateFormat.ShortDate)
                lbl_curr_charges.Text = MyDataReader.Item("DEL_CHG").ToString
                lbl_new_tp.Text = ""

                If (AreConfirmationCodesAvailable()) Then
                    ' June 21 2017 default to UNC
                    If isNotEmpty(MyDataReader.Item("CONF_STAT_CD").ToString) Then
                        Session("OrigConfCode") = MyDataReader.Item("CONF_STAT_CD").ToString
                        ddlConfStaCd.Value = MyDataReader.Item("CONF_STAT_CD").ToString
                    Else
                        Session("OrigConfCode") = "UNC"
                        ddlConfStaCd.Value = "UNC"
                    End If
                    ddlConfStaCd.Enabled = (AppConstants.Order.STATUS_OPEN = MyDataReader.Item("STAT_CD").ToString().ToUpper() AndAlso
                                            (AppConstants.Order.TYPE_SAL = MyDataReader.Item("ORD_TP_CD").ToString() OrElse
                                             AppConstants.Order.TYPE_CRM = MyDataReader.Item("ORD_TP_CD").ToString()))

                    ApplyConfirmationCodesSecurities()
                End If

                If MyDataReader.Item("PU_DEL").ToString = "P" Then
                    Calendar1.Visible = True
                Else
                    Calendar1.Visible = False
                    GridView2.Visible = False
                    nav_table.Visible = False
                    navdt_table.Visible = True
                    grid_del_date.Visible = True
                    grid_del_date_Binddata()
                End If

                txt_po.Text = MyDataReader.Item("ORDER_PO_NUM").ToString
                lbl_so_doc_num.Text = MyDataReader.Item("SO_DOC_NUM").ToString
                Session("ord_tp_cd") = MyDataReader.Item("ORD_TP_CD").ToString
                txt_ord_tp_cd.Text = MyDataReader.Item("ORD_TP_CD").ToString
                If AppConstants.Order.TYPE_CRM.Equals(txt_ord_tp_cd.Text) And Session("ORIG_DEL_DOC_NUM") + "" <> "" Then
                    Dim origSo As DataRow = OrderUtils.Get_Orig_Del_Doc(Session("ORIG_DEL_DOC_NUM"))
                    If SystemUtils.dataRowIsNotEmpty(origSo) Then
                        Session("origSoWrDt") = origSo("so_wr_dt")   ' if there is an original sale for the return, then the tax should be based on the original sale tax
                    Else
                        Session("origSoWrDt") = ""      ' TODO - set to avoid problems but should change references to handle empty Session variable - and then of course, we should have an SO header Session object
                    End If
                End If

                cboOrdSrt.Value = MyDataReader.Item("ORD_SRT_CD").ToString
                txt_ord_tp_cd.Enabled = False
                ' Added the below code as a part of MM-4240
                ' The below code was adding an "-" symbol in the txtTaxCd textbox so making this change
                If Not IsDBNull(MyDataReader.Item("TAX_CD")) Then
                    txtTaxCd.Text = MyDataReader.Item("TAX_CD").ToString & " - " & MyDataReader.Item("TAX_DES").ToString
                Else
                    txtTaxCd.Text = String.Empty
                End If
                Session("TAX_CD") = MyDataReader.Item("TAX_CD").ToString
                ' MM-4240
                If (String.IsNullOrWhiteSpace(Session("TAX_CD"))) Then
                    ViewState("TaxExempted") = True
                Else
                    ViewState("TaxExempted") = False
                End If
                Session("MANUAL_TAX_CD") = Nothing

                cbo_tax_exempt.Value = MyDataReader.Item("TET_CD").ToString
                If MyDataReader.Item("TET_CD").ToString & "" <> "" Then
                    txt_exempt_id.Text = MyDataReader.Item("TET_ID#").ToString
                End If
                If SecurityUtils.hasSecurity("PSOETAXCD", Session("EMP_CD")) = False Then
                    cbo_tax_exempt.Enabled = False
                    txt_exempt_id.Enabled = False
                Else
                    cbo_tax_exempt.Enabled = True
                    txt_exempt_id.Enabled = True
                End If

                lbl_tax_cd.Text = MyDataReader.Item("TAX_CD").ToString
                txt_wr_dt.Text = MyDataReader.Item("SO_WR_DT").ToString
                If IsDate(txt_wr_dt.Text.ToString) Then
                    txt_wr_dt.Text = FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate)
                End If
                txt_wr_dt.Enabled = False
                '7366
                If IsNumeric(MyDataReader.Item("PCT_OF_SALE1").ToString) Then
                    txt_comm1.Text = FormatNumber(CDbl(MyDataReader.Item("PCT_OF_SALE1").ToString), 2)
                Else
                    txt_comm1.Text = "0.00"
                End If

                txt_email.Text = MyDataReader.Item("EMAIL_ADDR").ToString
                If String.IsNullOrEmpty(txt_email.Text.ToString) Then
                    txt_email.BackColor = Color.LightYellow
                End If
                lbl_curr_tp.Text = MyDataReader.Item("PU_DEL").ToString
                If MyDataReader.Item("PU_DEL").ToString = "P" Then
                    ASPxButton2.Visible = False
                ElseIf MyDataReader.Item("PU_DEL").ToString = "D" Then
                    ASPxButton2.Visible = True
                End If
                txt_corp_name.Text = MyDataReader.Item("SHIP_TO_CORP").ToString
                txt_f_name.Text = MyDataReader.Item("SHIP_TO_F_NAME").ToString
                txt_l_name.Text = MyDataReader.Item("SHIP_TO_L_NAME").ToString
                txt_Cust_cd.Text = MyDataReader.Item("CUST_CD").ToString
                txt_Cust_cd.Enabled = False
                txt_Stat_cd.Text = MyDataReader.Item("STAT_CD").ToString
                txt_store_cd.Text = MyDataReader.Item("SO_STORE_CD").ToString
                txt_store_cd.Enabled = False
                txt_Stat_cd.Enabled = False
                If AppConstants.Order.STATUS_OPEN = txt_Stat_cd.Text.ToString Then
                    txt_Stat_dt.Text = ""
                    ASPxRoundPanel1.Visible = True
                    txt_fv_dt.Text = FormatDateTime(Today.Date, DateFormat.ShortDate)
                    txt_fv_dt.Enabled = False
                    PopulateStores()
                    final_store_cd.Enabled = False
                    btn_Save.Enabled = True
                    tblTaxCd.Visible = True
                Else
                    txt_Stat_dt.Text = MyDataReader.Item("FINAL_DT").ToString
                    btn_Save.Enabled = False
                    tblTaxCd.Visible = False
                End If
                If IsDate(txt_Stat_dt.Text.ToString) Then
                    txt_Stat_dt.Text = FormatDateTime(txt_Stat_dt.Text, DateFormat.ShortDate)
                End If
                txt_Stat_dt.Enabled = False
                txt_addr1.Text = MyDataReader.Item("SHIP_TO_ADDR1").ToString
                txt_addr2.Text = MyDataReader.Item("SHIP_TO_ADDR2").ToString
                txt_h_phone.Text = MyDataReader.Item("SHIP_TO_H_PHONE").ToString
                txt_b_phone.Text = MyDataReader.Item("SHIP_TO_B_PHONE").ToString
                txt_city.Text = MyDataReader.Item("SHIP_TO_CITY").ToString
                cbo_State.SelectedValue = MyDataReader.Item("SHIP_TO_ST_CD").ToString
                txt_zip.Text = MyDataReader.Item("SHIP_TO_ZIP_CD").ToString
                lbl_hdr_slsp1_cd.Text = MyDataReader.Item("SO_EMP_SLSP_CD1").ToString
                lbl_hdr_slsp2_cd.Text = MyDataReader.Item("SO_EMP_SLSP_CD2").ToString
                txt_slsp1.Text = MyDataReader.Item("SLSP1").ToString
                txt_slsp2.Text = MyDataReader.Item("SLSP2").ToString
                If txt_slsp2.Text.Trim & "" = "" Then
                    txt_comm2.Text = "0.00"
                Else
                    '7366
                    If IsNumeric(MyDataReader.Item("PCT_OF_SALE2").ToString) Then
                        txt_comm2.Text = FormatNumber(CDbl(MyDataReader.Item("PCT_OF_SALE2").ToString), 2)
                    End If
                End If
                HidslsComm1.Value = FormatNumber(CDbl(txt_comm1.Text.Trim), 2)
                HidslsComm2.Value = FormatNumber(CDbl(txt_comm2.Text.Trim), 2)

                ViewState("COMM1") = HidslsComm1.Value.ToString()
                ViewState("COMM2") = HidslsComm2.Value.ToString()
                ViewState("LBL_HDR_SLSP1_CD") = lbl_hdr_slsp1_cd.Text
                ViewState("LBL_HDR_SLSP2_CD") = lbl_hdr_slsp2_cd.Text

                StoreSalesPersonInfo()
                lbl_PD.Text = MyDataReader.Item("PU_DEL").ToString
                ' MM-4240
                ' We need to perform the below query when the tax method is Line Level
                If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Or ("L".Equals(ViewState("TaxMethod"))) Then
                    sql = "SELECT NVL(SUM(CUST_TAX_CHG),0) AS CUST_TAX_CHG FROM SO_LN " &
                            "WHERE DEL_DOC_NUM='" & DEL_DOC_NUM & "' AND VOID_FLAG='N'"

                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                    Try
                        'Execute DataReader 
                        MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql)

                        'Store Values in String Variables 
                        If MyDataReader2.Read Then
                            If IsNumeric(MyDataReader2.Item("CUST_TAX_CHG").ToString) Then
                                lblTax.Text = FormatNumber(CDbl(MyDataReader2.Item("CUST_TAX_CHG").ToString), 2)
                            Else
                                lblTax.Text = FormatNumber(0, 2)
                            End If
                        Else
                            lblTax.Text = FormatNumber(0, 2)
                        End If
                    Catch
                        Throw
                    End Try
                    If IsNumeric(MyDataReader.Item("TAX_CHG").ToString) Then
                        lblTax.Text = CDbl(lblTax.Text) + CDbl(MyDataReader.Item("TAX_CHG").ToString)
                    End If
                ElseIf IsNumeric(MyDataReader.Item("TAX_CHG").ToString) Then
                    lblTax.Text = FormatNumber(MyDataReader.Item("TAX_CHG").ToString, 2)
                Else
                    lblTax.Text = FormatNumber(0, 2)
                End If

                If IsNumeric(MyDataReader.Item("SETUP_CHG").ToString) Then
                    lblSetup.Text = FormatNumber(MyDataReader.Item("SETUP_CHG").ToString, 2)
                Else
                    lblSetup.Text = FormatNumber(0, 2)
                End If
                If IsNumeric(MyDataReader.Item("DEL_CHG").ToString) Then
                    lblDelivery.Text = FormatNumber(MyDataReader.Item("DEL_CHG").ToString, 2)
                Else
                    lblDelivery.Text = FormatNumber(0, 2)
                End If
                hidDeliveryCharges.Value = lblDelivery.Text
                'MM-7170
                If CheckSecurity(SecurityUtils.SOM_DEL_CHG, Session("EMP_CD")) Then
                    lblDelivery.Enabled = True
                Else
                    lblDelivery.Enabled = False
                End If
                If IsNumeric(MyDataReader.Item("ORIG_FI_AMT").ToString) Then
                    PaymentUtils.PopulateFinanceProviders(txt_store_cd.Text, cbo_finance_company)
                    lbl_financed.Text = FormatNumber(MyDataReader.Item("ORIG_FI_AMT").ToString, 2)
                    txt_fi_amt.Text = FormatNumber(MyDataReader.Item("ORIG_FI_AMT").ToString, 2)
                    GetPromoCodes()
                    txt_fi_acct.Text = thePmtBiz.GetFinanceAccount(txt_del_doc_num.Text, txt_ord_tp_cd.Text)
                Else
                    lbl_financed.Text = FormatNumber(0, 2)
                End If

                ' Set Finance popup provider code (#ITREQUEST-149)
                If Not String.IsNullOrEmpty(MyDataReader.Item("FIN_CUST_CD").ToString()) Then
                    If cbo_finance_company.Items.Count > 0 Then
                        For Each item As ListItem In cbo_finance_company.Items
                            If item.Value = MyDataReader.Item("FIN_CUST_CD").ToString() Then
                                cbo_finance_company.SelectedItem.Value = MyDataReader.Item("FIN_CUST_CD").ToString()
                                Exit For
                            End If
                        Next
                    End If
                End If

                ' Set Finance popup authorization code (#ITREQUEST-149)
                txt_fi_appr.Text = MyDataReader.Item("APPROVAL_CD").ToString()

                'May 22 IT Req 2919 email
                Dim emailString = txt_email.Text
                Dim eerStoreGrp As Boolean = LeonsBiz.CheckStoreGroup("EER", Session("HOME_STORE_CD"))
                Session("EER_STORE_GRP") = eerStoreGrp
                If (isNotEmpty(emailString)) Then
                    If (LeonsBiz.ValidateEmail(emailString) AndAlso
                       eerStoreGrp = True) Then
                        chk_email.Visible = True
                        chk_email.Enabled = True
                    End If
                End If

                'btnConfigureEmail.Visible = True
            Else
                Exit Sub
            End If

            sql = "SELECT DT, TRK_NUM, STOP#, SEQ#, TO_CHAR(TO_DATE(EST_ARRV_TIME,'SSSSS'),'HH24:MI') EST_ARRV_TIME, " &
                  "TO_CHAR(TO_DATE(ACT_ARRV_TIME,'SSSSS'),'HH24:MI') ACT_ARRV_TIME FROM TRK_STOP WHERE DEL_DOC_NUM='" & DEL_DOC_NUM & "'"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try

                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If MyDataReader2.Read Then
                    lbl_del_dt.Text = MyDataReader2.Item("DT").ToString
                    lbl_trk_stop.Text = MyDataReader2.Item("TRK_NUM").ToString & "/" & MyDataReader2.Item("STOP#").ToString & "/" & MyDataReader2.Item("SEQ#").ToString
                    lbl_arrival_time.Text = MyDataReader2.Item("EST_ARRV_TIME").ToString & "/" & MyDataReader2.Item("ACT_ARRV_TIME").ToString
                End If
            Catch
                Throw
            End Try

            Find_Related_Sales(MyDataReader.Item("SO_DOC_NUM").ToString)

            MyDataReader.Close()
            objSql.Dispose()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        bindgrid()

        Dim warning_list As String = ""
        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        sql = "SELECT DES FROM CUST_WARN, CUST2CUST_WARN " &
                " WHERE CUST_WARN.CUST_WARN_CD=CUST2CUST_WARN.CUST_WARN_CD AND " &
                "CUST2CUST_WARN.CUST_CD='" & txt_Cust_cd.Text & "'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read()
                warning_list = warning_list & "- " & MyDataReader.Item("des") & vbCrLf
            Loop

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If warning_list & "" <> "" Then
            img_warn.Visible = True
            lbl_cust_warn.Visible = True
            lbl_cust_warn.Text = "PLEASE REVIEW"
            img_warn.ToolTip = warning_list
        Else
            lbl_cust_warn.Visible = False
            img_warn.Visible = False
        End If

        If Request("line_Added") = "True" Then
            ASPxPageControl1.ActiveTabIndex = 1
        End If

        'when the store is ARS Enabled, then the calc. avail functionality is turned off
        Dim isOpenSale = IsOpenSaleOrder()
        chk_calc_avail.Enabled = (isOpenSale AndAlso Not Session("STORE_ENABLE_ARS"))
        chk_calc_avail.Checked = False

        If Not (AppConstants.Order.STATUS_OPEN.Equals(txt_Stat_cd.Text)) Then
            txt_fi_amt.Enabled = False
            cbo_finance_company.Enabled = False
            txt_fi_appr.Enabled = False
            txt_f_name.Enabled = False
            txt_l_name.Enabled = False
            txt_addr1.Enabled = False
            txt_addr2.Enabled = False
            txt_h_phone.Enabled = False
            txt_b_phone.Enabled = False
            txt_city.Enabled = False
            cbo_State.Enabled = False
            txt_zip.Enabled = False
            txt_slsp1.Enabled = False
            txt_slsp2.Enabled = False
            txt_comm1.Enabled = False
            txt_comm2.Enabled = False
            lblSetup.Enabled = False
            lblDelivery.Enabled = False
            btn_Save.Enabled = False
            cbo_tax_exempt.Enabled = False
            txt_comments.Enabled = False
            txt_del_comments.Enabled = False
            txt_ar_comments.Enabled = False
            cboOrdSrt.Enabled = False
            txt_email.Enabled = False
            txt_exempt_id.Enabled = False
            cbo_PD.Enabled = False
        End If

        If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_MCR Or txt_ord_tp_cd.Text = AppConstants.Order.TYPE_MDB Then
            ASPxPageControl1.TabPages(5).Enabled = False
        End If

        If txt_Stat_cd.Text = AppConstants.Order.STATUS_VOID Then
            btn_payments.Enabled = False
        End If


        'Daniela Nov 18
        If (AppConstants.Order.STATUS_VOID = txt_Stat_cd.Text) Then
            btn_reprint.Enabled = False ' reprint does not work for voided Orders
        End If

    End Sub

    Private Function getValidLinePODate(ByVal PurchaseOrderDate As String) As String
        'returns todays date when th PO date is already past
        Dim PODate As String = String.Empty
        If FormatDateTime(PurchaseOrderDate, DateFormat.ShortDate) <= DateTime.Today Then
            PODate = FormatDateTime(Today, DateFormat.ShortDate)
        Else
            PODate = FormatDateTime(PurchaseOrderDate, DateFormat.ShortDate)
        End If
        Return FormatDateTime(PODate, DateFormat.ShortDate)
    End Function

    ''' <summary>
    ''' Calls an API to calculate availability for a store that has been setup to use
    ''' advanced reservation logic (ARS) unless line is already reserved in which case is either
    ''' available today or today plus zone lead days if inventory fill store (so_ln) not the 
    ''' pickup/delivery store.
    ''' </summary>
    ''' <param name="soLn">the sales order line datarow being processed</param>
    ''' <returns>the available date</returns>
    ''' <remarks></remarks>
    Private Function GetAvailDate(ByVal soLn As DataRow) As Date
        Dim availDt As Date
        Dim POBiz As POBiz = New POBiz()
        Dim POArrivalDate As Date
        Dim linkedToPO As Boolean = Not IsNothing(soLn) AndAlso
                                                Not IsDBNull(soLn("PO_CD")) AndAlso
                                                Not String.IsNullOrEmpty(soLn("PO_CD"))
        Dim POAvailableDate = DateTime.Today
        If linkedToPO Then
            POAvailableDate = If(IsDBNull(soLn("POArrival")), FormatDateTime(POBiz.GetPOArrivalDate(soLn("PO_CD")), DateFormat.ShortDate), soLn("POArrival"))
            POArrivalDate = getValidLinePODate(POAvailableDate)
        End If
        'calculate availability for ARS store, if any 
        'if line is inventory AND ARS enabled AND not a void line, then figure avail dt
        If Session("STORE_ENABLE_ARS") AndAlso SystemUtils.dataRowIsNotEmpty(soLn) AndAlso soLn("inventory") = "Y" AndAlso soLn("void_flag") = "N" Then
            'If not a new line and SKU did not change and qty is same or decreased from original on line 
            ' AND the line was originally reserved
            If soLn("new_line") = "N" AndAlso soLn("itm_cd", DataRowVersion.Original) = soLn("itm_cd") AndAlso
                    soLn("qty", DataRowVersion.Original) >= soLn("qty") AndAlso
                    (Not (IsDBNull(soLn("store_cd", DataRowVersion.Original)) OrElse
                    IsDBNull(soLn("loc_cd", DataRowVersion.Original)))) Then
                ' if the line is reserved, then it's available now or as soon as we can get it to the pickup/delivery store
                'If (no longer reserved AND orig was not outlet) 
                '  OR (same store AND same loc as original)  
                If (StringUtils.NullValStr(soLn("store_cd"), String.Empty).isEmpty And IsDBNull(soLn("out_cd", DataRowVersion.Original))) OrElse
                    (soLn("store_cd", DataRowVersion.Original) = StringUtils.NullValStr(soLn("store_cd"), String.Empty) AndAlso
                        soLn("loc_cd", DataRowVersion.Original) = StringUtils.NullValStr(soLn("loc_cd"), String.Empty)) Then
                    If SessVar.puDelStoreCd = soLn("store_cd", DataRowVersion.Original) Then
                        availDt = FormatDateTime(Today, DateFormat.ShortDate)
                    Else
                        availDt = FormatDateTime(Today.AddDays(theInvBiz.GetZoneLeadDays(SessVar.zoneCd)), DateFormat.ShortDate)
                    End If
                Else ' res changed 
                    availDt = theInvBiz.CalcAvailARS(soLn("itm_cd"), Session("PD"), SessVar.zoneCd, soLn("qty"), SessVar.puDelStoreCd)
                End If
            Else  ' if new line or anything changed then re-calc
                availDt = theInvBiz.CalcAvailARS(soLn("itm_cd"), Session("PD"), SessVar.zoneCd, soLn("qty"), SessVar.puDelStoreCd)
            End If
            If linkedToPO AndAlso POArrivalDate < availDt Then
                'avail date would be the PO date
                availDt = POArrivalDate
            Else
                'Do Nothing
            End If

            'Jira MM-2513
            'this viewstate will always have max of available dates of all ites of order
            If ViewState("MaxAvailDdate") IsNot Nothing AndAlso ViewState("MaxAvailDdate").ToString().Trim().isEmpty Then
                ViewState("MaxAvailDdate") = availDt
            ElseIf availDt > Convert.ToDateTime(ViewState("MaxAvailDdate").ToString) Then
                ViewState("MaxAvailDdate") = availDt
            Else
                'Nothing
            End If
        End If

        Return FormatDateTime(availDt, DateFormat.ShortDate)
    End Function

    Public Sub Find_Related_Sales(ByVal del_doc As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds = New DataSet

        conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        sql = "Select DEL_DOC_NUM FROM SO WHERE SO_DOC_NUM='" & del_doc & "'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        If numrows > 1 Then
            With cbo_related_docs
                .DataSource = ds
                .DataValueField = "DEL_DOC_NUM"
                .DataTextField = "DEL_DOC_NUM"
                .DataBind()
            End With
            cbo_related_docs.SelectedValue = txt_del_doc_num.Text
            cbo_related_docs.Visible = True
        End If
        conn.Close()

    End Sub

    Public Sub calculate_total()

        Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String = ""
        Dim objSql As OracleCommand
        Dim DEL_DOC_NUM As String
        Dim MyDataReader As OracleDataReader

        DEL_DOC_NUM = Request("del_doc_num")
        conn.Open()

        ' MM-4240
        Dim tax_pct As Double = 0
        Dim max_tax As Double = 0
        Dim isMaximumTaxisSet As Boolean = False

        If DEL_DOC_NUM & "" <> "" Then
            If Session("DEL_DOC_LN#") Is Nothing Then ' And Not IsPostBack
                If txt_Stat_cd.Text <> "V" Then
                    Dim sqlStmt As New StringBuilder
                    Select Case txt_ord_tp_cd.Text
                        Case AppConstants.Order.TYPE_SAL
                            sqlStmt.Append("SELECT AMT, BNK_CRD_NUM FROM AR_TRN WHERE TRN_TP_CD='SAL' AND IVC_CD='" & DEL_DOC_NUM & "'")
                        Case AppConstants.Order.TYPE_CRM
                            sqlStmt.Append("SELECT AMT, BNK_CRD_NUM FROM AR_TRN WHERE TRN_TP_CD='CRM' AND ADJ_IVC_CD='" & DEL_DOC_NUM & "'")
                        Case AppConstants.Order.TYPE_MCR
                            sqlStmt.Append("SELECT AMT, BNK_CRD_NUM FROM AR_TRN WHERE TRN_TP_CD='MCR' AND ADJ_IVC_CD='" & DEL_DOC_NUM & "'")
                        Case AppConstants.Order.TYPE_MDB
                            sqlStmt.Append("SELECT AMT, BNK_CRD_NUM FROM AR_TRN WHERE TRN_TP_CD='MDB' AND ADJ_IVC_CD='" & DEL_DOC_NUM & "'")
                    End Select
                    If sqlStmt.Length < 1 Then Exit Sub

                    sqlStmt.Append(" AND co_cd = '").Append(Session("CO_CD")).Append("' AND cust_cd = '").Append(txt_Cust_cd.Text).Append("' ")


                    objSql = DisposablesManager.BuildOracleCommand(sqlStmt.ToString, conn)

                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    If (MyDataReader.Read) Then
                        lblTotal.Text = FormatCurrency(CDbl(MyDataReader.Item("AMT").ToString), 2)
                        If MyDataReader.Item("BNK_CRD_NUM").ToString & "" <> "" Then
                            txt_fi_acct.Text = MyDataReader.Item("BNK_CRD_NUM").ToString
                        End If
                    End If

                    MyDataReader.Close()
                    objSql.Dispose()
                    lblSubtotal.Text = FormatCurrency(CDbl(lblTotal.Text) - (CDbl(lblTax.Text) + CDbl(lblDelivery.Text) + CDbl(lblSetup.Text)), 2)
                Else
                    sql = "SELECT SUM(QTY*UNIT_PRC) AS TOTAL_ITEMS FROM SO_LN WHERE DEL_DOC_NUM='" & DEL_DOC_NUM & "' AND VOID_FLAG='N'"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    If MyDataReader.Read Then
                        If IsNumeric(MyDataReader.Item("TOTAL_ITEMS").ToString) Then
                            lblSubtotal.Text = FormatCurrency(CDbl(MyDataReader.Item("TOTAL_ITEMS").ToString), 2)
                        Else
                            lblSubtotal.Text = FormatCurrency(0, 2)
                        End If
                    End If

                    MyDataReader.Close()
                    objSql.Dispose()

                End If
            ElseIf cbo_tax_exempt.SelectedIndex <> 0 Then
                ' MM-4240
                ' Since the tax exemption has been given to the customer we should update SO_LN with no tax information
                Dim merch_total As Double = 0

                Dim ds As DataSet
                ds = Session("DEL_DOC_LN#")
                Dim dr As DataRow
                For Each dr In ds.Tables(0).Rows
                    If dr("VOID_FLAG") = "N" Then
                        merch_total = merch_total + (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))
                    End If
                    dr("CUST_TAX_CHG") = CDbl(0)
                    dr("TAXABLE_AMT") = CDbl(0)
                    dr("TAX_BASIS") = CDbl(0)
                    dr("TAX_RESP") = ""
                    dr("TAX_CD") = ""
                Next
                lblTax.Text = FormatNumber(CDbl(0), 2)
                lblSubtotal.Text = FormatNumber(CDbl(merch_total), 2)
                Session("DEL_DOC_LN#") = ds
            ElseIf cbo_tax_exempt.SelectedIndex = 0 AndAlso (Not String.IsNullOrWhiteSpace(lbl_tax_cd.Text)) Then
                Dim ds As DataSet
                Dim tax As Double = 0

                ' TODO - someday this needs to be based on tax code tax method
                ' TODO - line taxing has to be based on whether tax auth taxes itm_tp for each line

                If ("Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString)) Then
                    Dim taxDt As String = txt_wr_dt.Text
                    If AppConstants.Order.TYPE_CRM.Equals(txt_ord_tp_cd.Text) AndAlso Session("origSoWrDt") + "" <> "" AndAlso IsDate(Session("origSoWrDt")) Then
                        taxDt = Session("origSoWrDt")
                    End If
                    'Calculate Tax on Merchandise and Line Items
                    tax_pct = TaxUtils.getTaxPct(lbl_tax_cd.Text, FormatDateTime(taxDt, DateFormat.ShortDate), "")
                End If  ' line tax

                ds = Session("DEL_DOC_LN#")
                Dim merch_total As Double = 0

                Dim dt As DataTable = ds.Tables(0)
                Dim dr As DataRow
                ' line taxing calculates the tax for each line and delivery and setup charges separately
                ' header taxing sums all the lines by item type and then calculates the tax by tax authority based on the total percent by itm_tp
                ' therefore, line processing below calculates the tax whereas header processing just accumulates until the end and then calls the calculation;
                Dim hdrTaxReq As New taxHdrCalcRequest()
                Dim tmp_Amt As Double = 0.0

                ' MM-4240
                ' This method will get TAX % and Maximum tax for the tax code
                tax_pct = TaxUtils.getTaxPct(lbl_tax_cd.Text, FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate), "", max_tax, isMaximumTaxisSet)
                ' line taxing calculates the tax for each line and delivery and setup charges separately
                ' header taxing sums all the lines by item type and then calculates the tax by tax authority based on the total percent by itm_tp
                ' therefore, line processing below calculates the tax whereas header processing just accumulates until the end and then calls the calculation 

                For Each dr In ds.Tables(0).Rows
                    ' MM-4240
                    ' Below variable is used to calculate the tax on line level
                    Dim lineLevelTax As Double = 0.0
                    If dr("VOID_FLAG") = "N" Then
                        merch_total = merch_total + (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))
                    End If

                    ' TODO - someday this needs to be based on tax code tax method
                    ' TODO - line taxing has to be based on written date andAlso based on whether tax auth taxes itm_tp for each line                    
                    If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then
                        ' MM-4240
                        ' Whether the TAX_METHOD is header or line level we need to update this 
                        ' tax information to SO_LN since tax_line syspm is set to "Y"

                        If IsNumeric(dr("QTY")) AndAlso IsNumeric(dr("UNIT_PRC")) AndAlso
                            dr("VOID_FLAG") = "N" AndAlso Determine_Taxability(dr("ITM_TP_CD")) = "Y" Then

                            tmp_Amt = (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))  ' TODO no unit carpet logic
                            If (isMaximumTaxisSet = False) Then
                                lineLevelTax = (tmp_Amt * tax_pct)
                                dr("CUST_TAX_CHG") = FormatNumber(Math.Round(CDbl(lineLevelTax), 2, MidpointRounding.AwayFromZero))
                                tax = tax + lineLevelTax
                            Else
                                lineLevelTax = (tmp_Amt * tax_pct)
                                If max_tax >= 0.0 Then
                                    If (lineLevelTax > max_tax) Then
                                        lineLevelTax = max_tax
                                    End If
                                End If
                                tax = tax + lineLevelTax
                                dr("CUST_TAX_CHG") = FormatNumber(Math.Round(lineLevelTax, 2, MidpointRounding.AwayFromZero))
                            End If

                            dr("TAXABLE_AMT") = CDbl(dr("UNIT_PRC"))    ' TODO - this is not always true, no need to reset tax_resp at this time as we don't do store or split yet
                            dr("TAX_CD") = lbl_tax_cd.Text
                            dr("TAX_BASIS") = dr("TAXABLE_AMT") ' TODO - merge these so not duplicated but need to make sure no issues in doing so

                        Else
                            dr("CUST_TAX_CHG") = CDbl(0)
                            dr("TAXABLE_AMT") = CDbl(0)
                            dr("TAX_BASIS") = CDbl(0)
                            dr("TAX_RESP") = ""
                            dr("TAX_CD") = ""
                        End If
                        ' unknown what this tax method view state is - E1 does tax method by tax code;
                        '    POS+ still does tax method by POS+ sales parameter; in dev or QA environments
                        '    there may be data that was created in E1 under a different tax method and is queried here
                        '    so the results are unpredictable; in customer production, this should not be a problem
                        '    - until the tax method logic in POS+ is reworked, use the POS+ param to drive this
                        'ElseIf ("L".Equals(ViewState("TaxMethod"))) Then ' line level tax method
                        '    ' This is to calculate tax (max tax)
                        '    If dr("VOID_FLAG").ToString = "N" AndAlso IsNumeric(dr("QTY")) AndAlso IsNumeric(dr("UNIT_PRC")) Then
                        '        tmp_Amt = (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))  ' TODO no unit carpet logic
                        '        If (isMaximumTaxisSet = False) Then
                        '            lineLevelTax = (tmp_Amt * tax_pct)
                        '            dr("CUST_TAX_CHG") = FormatNumber(Math.Round(CDbl(lineLevelTax), 2, MidpointRounding.AwayFromZero))
                        '            tax = tax + lineLevelTax
                        '        Else
                        '            lineLevelTax = (tmp_Amt * tax_pct)
                        '            If max_tax >= 0.0 Then
                        '                If (lineLevelTax > max_tax) Then
                        '                    lineLevelTax = max_tax
                        '                End If
                        '            End If
                        '            tax = tax + lineLevelTax
                        '            dr("CUST_TAX_CHG") = FormatNumber(Math.Round(lineLevelTax, 2, MidpointRounding.AwayFromZero))
                        '        End If
                        '        dr("TAXABLE_AMT") = CDbl(dr("UNIT_PRC"))    ' TODO - this is not always true, no need to reset tax_resp at this time as we don't do store or split yet
                        '        dr("TAX_CD") = lbl_tax_cd.Text
                        '        dr("TAX_BASIS") = dr("TAXABLE_AMT") ' TODO - merge these so not duplicated but need to make sure no issues in doing so
                        '    Else
                        '        dr("CUST_TAX_CHG") = CDbl(0)
                        '        dr("TAXABLE_AMT") = CDbl(0)
                        '        dr("TAX_BASIS") = CDbl(0)
                        '        dr("TAX_RESP") = ""
                        '        dr("TAX_CD") = ""
                        '    End If
                    Else ' header tax method
                        ' For Header level tax we will not update Heder Tax information to SO_LN table since tax_line SysPm will be set to "No"
                        If dr("VOID_FLAG").ToString = "N" AndAlso IsNumeric(dr("QTY")) AndAlso IsNumeric(dr("UNIT_PRC")) Then
                            'tmp_Amt = (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))  ' TODO no unit carpet logic
                            'If (isMaximumTaxisSet = False) Then
                            '    lineLevelTax = (tmp_Amt * tax_pct)
                            '    tax = tax + lineLevelTax
                            'Else
                            '    lineLevelTax = (tmp_Amt * tax_pct)
                            '    If max_tax >= 0.0 Then
                            '        If (lineLevelTax > max_tax) Then
                            '            lineLevelTax = max_tax
                            '        End If
                            '    End If
                            '    tax = tax + lineLevelTax
                            'End If

                            dr("CUST_TAX_CHG") = CDbl(0)
                            dr("TAXABLE_AMT") = CDbl(0)
                            dr("TAX_BASIS") = CDbl(0)
                            dr("TAX_RESP") = ""
                            dr("TAX_CD") = ""

                            tmp_Amt = (CDbl(dr("QTY")) * CDbl(dr("UNIT_PRC")))  ' TODO no unit carpet logic
                            TaxUtils.AccumTaxingAmts(dr("ITM_TP_CD").ToString + "", tmp_Amt, hdrTaxReq.itmTpAmts)
                        Else
                            dr("CUST_TAX_CHG") = CDbl(0)
                            dr("TAXABLE_AMT") = CDbl(0)
                            dr("TAX_BASIS") = CDbl(0)
                            dr("TAX_RESP") = ""
                            dr("TAX_CD") = ""
                            ' VOID lines retail will not be considered here
                        End If
                    End If
                Next

                Session("DEL_DOC_LN#") = ds

                If IsNumeric(lblDelivery.Text) AndAlso CDbl(lblDelivery.Text) > 0.0 Then
                    ' TODO - someday this needs to be based on tax code tax method
                    If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then 'Or (Not ("L".Equals(ViewState("TaxMethod")))) Then
                        ' if line tax
                        'tax = tax + getTaxAmt(lbl_tax_cd.Text, FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate), CDbl(lblDelivery.Text), "DEL")

                        Dim deliveryTaxCharges As Double = 0.0
                        'Declaring another tax variable because, the tax on delivery charges differ from regular tax, the Tax_code variable is used in further calculations. in order to avoid the errors, using another variable
                        Dim delTaxPct As Double = TaxUtils.getTaxPct(lbl_tax_cd.Text.Trim().ToUpper(), txt_wr_dt.Text.ToString(), "DEL", max_tax, isMaximumTaxisSet)
                        If (isMaximumTaxisSet = False) Then
                            'deliveryTaxCharges = (CDbl(lblDelivery.Text) * tax_pct)
                            deliveryTaxCharges = (CDbl(lblDelivery.Text) * delTaxPct)
                            tax = tax + deliveryTaxCharges
                        Else
                            'deliveryTaxCharges = (CDbl(lblDelivery.Text) * tax_pct)
                            deliveryTaxCharges = (CDbl(lblDelivery.Text) * delTaxPct)
                            If max_tax >= 0.0 Then
                                If (tax_pct > max_tax) Then
                                    deliveryTaxCharges = max_tax
                                End If
                            End If
                            tax = tax + deliveryTaxCharges
                        End If

                    Else ' if header
                        TaxUtils.AccumTaxingAmts("DEL", CDbl(lblDelivery.Text), hdrTaxReq.itmTpAmts)
                    End If
                End If

                If IsNumeric(lblSetup.Text) AndAlso CDbl(lblSetup.Text) > 0.0 Then

                    ' TODO - someday this needs to be based on tax code tax method
                    If "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then 'Or (Not ("L".Equals(ViewState("TaxMethod")))) Then
                        ' if line tax
                        'tax = tax + getTaxAmt(lbl_tax_cd.Text, FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate), CDbl(lblSetup.Text), "SETUP")
                        Dim setupTaxCharges As Double = 0.0
                        If (isMaximumTaxisSet = False) Then
                            setupTaxCharges = (CDbl(lblSetup.Text) * tax_pct)
                            tax = tax + setupTaxCharges
                        Else
                            setupTaxCharges = (CDbl(lblSetup.Text) * tax_pct)
                            If max_tax >= 0.0 Then
                                If (setupTaxCharges > max_tax) Then
                                    setupTaxCharges = max_tax
                                End If
                            End If
                            tax = tax + setupTaxCharges
                        End If

                    Else ' header method
                        TaxUtils.AccumTaxingAmts("SETUP", CDbl(lblSetup.Text), hdrTaxReq.itmTpAmts)
                    End If
                End If

                ' TODO - someday this needs to be based on tax code tax method
                'If it is a Header Tax
                If Not "Y".Equals(ConfigurationManager.AppSettings("tax_line").ToString) Then '("L".Equals(ViewState("TaxMethod"))) Then
                    ' if header tax
                    hdrTaxReq.taxCd = lbl_tax_cd.Text
                    If (Not Session("origSoWrDt") Is Nothing) AndAlso (Session("origSoWrDt").ToString + "").isNotEmpty AndAlso IsDate(Session("origSoWrDt").ToString) Then
                        hdrTaxReq.taxDt = FormatDateTime(Session("origSoWrDt").ToString, DateFormat.ShortDate)
                    Else
                        hdrTaxReq.taxDt = FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate)
                    End If
                    tax = TaxUtils.ComputeHdrTax(hdrTaxReq)
                    'itm_prc = TaxUtils.ComputeLineTax(linesDatRdr.Item("ITM_CD").ToString, linesDatRdr.Item("RET_PRC").ToString, 1, Session("tax_cd"), Session("store_cd"), Session("PD"), "")
                End If

                'If IsPostBack Or Request("line_added") = "TRUE" Then  ' jira 2165 fix installed and checked in with jira 2200
                lblTax.Text = FormatNumber(CDbl(tax), 2)

                lblSubtotal.Text = FormatNumber(CDbl(merch_total), 2)
            End If
        End If

        Dim sqlQueryBuilder As New StringBuilder
        sqlQueryBuilder.Append("select ")
        sqlQueryBuilder.Append("nvl(SUM(DECODE(T.DC_CD,'D',-A.AMT,'C',A.AMT,0)),0) AS AMT ")
        sqlQueryBuilder.Append("from ")
        sqlQueryBuilder.Append("ar_trn_tp t ")
        sqlQueryBuilder.Append(", ar_trn a ")
        sqlQueryBuilder.Append("where  a.trn_tp_cd = t.trn_tp_cd  ")
        sqlQueryBuilder.Append(" AND a.CO_CD = :coCd")
        sqlQueryBuilder.Append(" AND a.CUST_CD   = :CUST_CD ")
        Select Case txt_ord_tp_cd.Text
            Case AppConstants.Order.TYPE_SAL
                sqlQueryBuilder.Append(" and  a.ivc_cd = :DEL_DOC_NUM ")
            Case AppConstants.Order.TYPE_CRM
                sqlQueryBuilder.Append(" and  a.adj_ivc_cd =:DEL_DOC_NUM  ")
            Case AppConstants.Order.TYPE_MCR
                sqlQueryBuilder.Append(" and  a.adj_ivc_cd = :DEL_DOC_NUM  ")
            Case AppConstants.Order.TYPE_MDB
                sqlQueryBuilder.Append(" and  a.adj_ivc_cd = :DEL_DOC_NUM  ")
        End Select
        sqlQueryBuilder.Append(" and a.trn_tp_cd not in ('SAL','CRM','MDB','MCR','SER') ")

        objSql = DisposablesManager.BuildOracleCommand(sqlQueryBuilder.ToString, conn)
        objSql.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
        objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
        objSql.Parameters.Add(":coCd", OracleType.VarChar)
        objSql.Parameters(":DEL_DOC_NUM").Value = DEL_DOC_NUM
        objSql.Parameters(":coCd").Value = Session("CO_CD")
        objSql.Parameters(":CUST_CD").Value = txt_Cust_cd.Text

        MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

        If (MyDataReader.Read) Then
            If MyDataReader.Item("AMT").ToString & "" <> "" Then
                If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM Or txt_ord_tp_cd.Text = AppConstants.Order.TYPE_MCR Then
                    lbl_Payments.Text = FormatCurrency(-CDbl(MyDataReader.Item("AMT").ToString), 2)
                    lblPmts.Text = "Refunds:"
                Else
                    lbl_Payments.Text = FormatCurrency(CDbl(MyDataReader.Item("AMT").ToString), 2)
                    lblPmts.Text = "Payments:"
                End If
                btn_payments.Enabled = True
            Else
                lbl_Payments.Text = FormatCurrency(0, 2)
            End If
        End If

        objSql.Parameters.Clear()
        If lblTax.Text & "" = "" Then lblTax.Text = FormatNumber(0, 2)
        If lblSetup.Text & "" = "" Then lblSetup.Text = FormatNumber(0, 2)
        If lbl_financed.Text & "" = "" Then lbl_financed.Text = FormatNumber(0, 2)
        If lblDelivery.Text & "" = "" Then lblDelivery.Text = FormatNumber(0, 2)

        'Close Connection 
        conn.Close()

        If txt_Stat_cd.Text = "V" Then
            lblTotal.Text = FormatCurrency(0, 2)
            lbl_financed.Text = "0.00"
        Else
            lblTotal.Text = FormatCurrency(CDbl(lblSubtotal.Text) + (CDbl(lblTax.Text) + CDbl(lblDelivery.Text) + CDbl(lblSetup.Text)), 2)
        End If
        If CDbl(lblTotal.Text) - CDbl(lbl_Payments.Text) <= 0 Then
            lbl_balance.Text = FormatCurrency(CDbl(lblTotal.Text) - (CDbl(lbl_Payments.Text)), 2)
        Else
            lbl_balance.Text = FormatCurrency(CDbl(lblTotal.Text) - (CDbl(lbl_Payments.Text) + CDbl(lbl_financed.Text)), 2)
        End If

    End Sub

    Protected Sub btn_payments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_payments.Click
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then

            If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
                PromptForSummaryPriceMgrOverride()
            Else
                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim sql As String
                Dim objSql As OracleCommand
                Dim ds As DataSet
                ds = New DataSet
                Dim DEL_DOC_NUM As String
                Dim MyDataReader As OracleDataReader

                txt_del_doc_num.Enabled = "False"
                btn_Lookup.Visible = False

                DEL_DOC_NUM = Request("del_doc_num")
                conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                sql = "SELECT APP_PASS FROM EMP WHERE EMP_CD='" & Session("emp_cd") & "'"

                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                Dim csh_pass As String = ""
                Try

                    conn.Open()
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    If (MyDataReader.Read) Then
                        csh_pass = MyDataReader.Item("APP_PASS")
                    End If

                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
                conn.Close()

                Dim URL_String As String
                Dim ord_total As Double = 0

                If IsNumeric(lblTotal.Text) Then
                    ord_total = CDbl(lblTotal.Text)
                End If
                If IsNumeric(lbl_Payments.Text) Then
                    ord_total = ord_total - CDbl(lbl_Payments.Text)
                End If
                lbl_balance.Text = CDbl(ord_total)

                Dim cshPwd As String = AppUtils.Encrypt(csh_pass, "CrOcOdIlE")

                'If the current user has write access then perform the below operation
                'If the current has has read-only permission then no need to perform below activity            
                If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
                    ViewState("RefUrl") = Convert.ToString(Request.UrlReferrer)
                    Dim previousPageURL As String = Convert.ToString(ViewState("RefUrl"))
                    If Not String.IsNullOrWhiteSpace(previousPageURL) Then
                        ' For stale data issue
                        ' Need to perform the action when user navigates back to Home page from SOM+, SORW+ page            
                        Dim pageName As String = String.Empty
                        pageName = System.IO.Path.GetFileName(previousPageURL)

                        Dim accessInfo As New AccessControlBiz()
                        accessInfo.DeleteRecordsforPage(pageName)
                    End If
                End If

                URL_String = "?referrer=salesordermaintenance.aspx&ord_total=" & ord_total & "&ip_csh_pass=" & cshPwd & "&ip_cust_cd=" & txt_Cust_cd.Text & "&ip_ivc_cd=" & txt_del_doc_num.Text '&store_cd=" & txt_store_cd.Text & "

                Response.Redirect("IndependentPaymentProcessing.aspx" & URL_String)
            End If
        End If
    End Sub

    Protected Sub btn_invoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_invoice.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql As OracleCommand
        Dim MyDatareader As OracleDataReader
        Dim sql, default_invoice As String
        default_invoice = ""

        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        sql = "select store_cd, invoice_file, default_file from invoice_admin where store_cd='" & txt_store_cd.Text & "'"
        objsql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            conn.Open()
            MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

            Do While MyDatareader.Read
                If MyDatareader.Item("invoice_file").ToString & "" = "" Then
                    default_invoice = MyDatareader.Item("DEFAULT_FILE").ToString
                Else
                    default_invoice = MyDatareader.Item("invoice_file").ToString
                End If
            Loop
            MyDatareader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If String.IsNullOrEmpty(default_invoice) Then
            sql = "select store_cd, invoice_file, default_file from invoice_admin"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                MyDatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If MyDatareader.Read Then
                    default_invoice = MyDatareader.Item("DEFAULT_FILE").ToString
                End If
                MyDatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

        conn.Close()

        Dim UpPanel As UpdateProgress
        UpPanel = Master.FindControl("UpdateProgress1")
        UpPanel.Visible = False

        'resolve invoice to print
        If (default_invoice.isNotEmpty And default_invoice.Contains(".aspx")) Then
            ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('Invoices/" & default_invoice & "?DEL_DOC_NUM=" & txt_del_doc_num.Text & "&EMAIL=" & chk_email.Checked & "','frmTest" & "','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        ElseIf (default_invoice.isNotEmpty And default_invoice.Contains(".repx")) Then
            ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike", "window.open('Invoices/DesignerInvoice.aspx" & "?DEL_DOC_NUM=" & txt_del_doc_num.Text & "&EMAIL=" & chk_email.Checked & "&INVOICE_NAME=" & default_invoice & "','frmTest','height=700px,width=800px,top=10,left=10,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)
        End If
    End Sub

    Private Sub SoLnCurrentInvAddProcess(ByVal soLn As DataRow, ByVal orig_dr As DataRow, ByRef updt As soLn_updates,
                                     ByRef origOrdLn As OrderLine, ByRef chngOrdLn As OrderLine, ByRef cmd As OracleCommand)

        ' Check if price has changed when adding new item
        If CDbl(soLn("ITM_RET_PRC", DataRowVersion.Current).ToString()) <> CDbl(soLn("UNIT_PRC", DataRowVersion.Current).ToString()) Then

            ' Get price approval employee code
            Dim approverCd = Session("EMP_CD")
            If Not IsDBNull(soLn("prc_chg_app_cd", DataRowVersion.Current)) AndAlso soLn("prc_chg_app_cd", DataRowVersion.Current).ToString.isNotEmpty Then
                approverCd = soLn("prc_chg_app_cd", DataRowVersion.Current).ToString
            End If

            ' Declare variables to store current unit price
            Dim retailPrice As Double = 0

            ' Declare variables to store original retail price
            Dim originalRetailPrice As Double = 0

            ' Set current item retail price
            Double.TryParse(soLn("ITM_RET_PRC", DataRowVersion.Current) & "", originalRetailPrice)

            ' Set changed unit price
            Double.TryParse(soLn("UNIT_PRC", DataRowVersion.Current) & "", retailPrice)

            ' Add price change comment
            ' No need to check for discount ("DISC") as original state of "DISC_AMT" is not set yet when adding new item
            HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "SRC", originalRetailPrice.ToString(), retailPrice.ToString(), approverCd, soLn("DEL_DOC_LN#"))
        End If
    End Sub

    Public Sub SoLnPreInvUpdtProcess(ByVal soLn As DataRow, ByVal orig_dr As DataRow, ByRef updt As soLn_updates,
                                     ByRef origOrdLn As OrderLine, ByRef chngOrdLn As OrderLine, ByRef cmd As OracleCommand)

        origOrdLn = createOrdLn(soLn, Tax_Constants.lineTaxMethod.Equals(orig_dr("tax_method")), True)
        chngOrdLn = createOrdLn(soLn, Tax_Constants.lineTaxMethod.Equals(orig_dr("tax_method")))

        If soLn("VOID_FLAG", DataRowVersion.Current) = "Y" And soLn("VOID_FLAG", DataRowVersion.Original) = "N" Then
            ' MM-4240
            ' When the line has been voided then remove tax information on SO_LN 
            updt.void = True
            If ("L".Equals(ViewState("TaxMethod"))) Or (ConfigurationManager.AppSettings("tax_line").ToString = "Y") Then
                chngOrdLn.taxResp = ""
                chngOrdLn.taxCd = ""
                chngOrdLn.taxBasis = 0
                chngOrdLn.custTaxChg = 0
                chngOrdLn.storeTaxChg = 0
            End If
            '***Check if the warranty links got changed
            updt.warrLinks = HasWarrantyLinkChanged(soLn("warr_by_ln", DataRowVersion.Original).ToString, soLn("warr_by_ln", DataRowVersion.Current).ToString)

        Else ' if voiding, no other changes matter or allowed 

            If CDbl(soLn("UNIT_PRC", DataRowVersion.Current)) <> CDbl(soLn("UNIT_PRC", DataRowVersion.Original)) Then
                updt.prc = True
            End If
            If CDbl(soLn("QTY", DataRowVersion.Current)) <> CDbl(soLn("QTY", DataRowVersion.Original)) Then
                updt.qty = True
            End If

            '***Check if the warranty links got changed
            updt.warrLinks = HasWarrantyLinkChanged(soLn("warr_by_ln", DataRowVersion.Original).ToString, soLn("warr_by_ln", DataRowVersion.Current).ToString)

            '  the whole system assumes either entirely line tax if line tax enabled or header tax otherwise
            '   line vs header depends on tax_cd.tax_method - cannot throw that in here without redoing all 
            If ConfigurationManager.AppSettings("tax_line").ToString = "Y" AndAlso
              ((Not NumbersEqual(soLn("cust_tax_chg").ToString, soLn("cust_tax_chg", DataRowVersion.Original).ToString, True)) OrElse
               soLn("tax_resp").ToString <> soLn("tax_resp", DataRowVersion.Original).ToString OrElse
               soLn("tax_cd").ToString <> soLn("tax_cd", DataRowVersion.Original).ToString OrElse
               (Not NumbersEqual(soLn("tax_basis").ToString, soLn("tax_basis", DataRowVersion.Original).ToString, True))) Then
                ' hdr vs line tax is not quite right thru out; so if fields change, update and if no tax chg, clear
                updt.tax = True
                If IsNumeric(soLn("CUST_TAX_CHG")) Then ' TODO - maybe sb looking at the tax method for current tax code
                    'chngOrdLn.custTaxChg = dr("CUST_TAX_CHG")
                    'chngOrdLn.taxCd = cbo_tax_cd.Value + ""  ' coming in here when change to tax exempt, not right but leave for now - TODO
                    If (String.IsNullOrWhiteSpace(lbl_tax_cd.Text)) Then
                        chngOrdLn.taxResp = ""
                        chngOrdLn.taxCd = ""
                        chngOrdLn.taxBasis = 0
                        chngOrdLn.custTaxChg = 0
                        chngOrdLn.storeTaxChg = 0
                    Else
                        chngOrdLn.taxCd = lbl_tax_cd.Text + ""
                        chngOrdLn.custTaxChg = soLn("CUST_TAX_CHG")
                        chngOrdLn.taxBasis = soLn("TAXABLE_AMT")
                        chngOrdLn.taxResp = "C"
                        chngOrdLn.storeTaxChg = 0
                    End If
                Else
                    chngOrdLn.taxResp = ""
                    chngOrdLn.taxCd = ""
                    chngOrdLn.taxBasis = "NaN"
                    chngOrdLn.custTaxChg = "NaN"
                End If

            ElseIf ("L".Equals(ViewState("TaxMethod"))) Then
                updt.tax = True
                If IsNumeric(soLn("CUST_TAX_CHG")) Then ' TODO - maybe sb looking at the tax method for current tax code
                    chngOrdLn.custTaxChg = soLn("CUST_TAX_CHG")
                    chngOrdLn.taxResp = "C"
                    chngOrdLn.taxCd = lbl_tax_cd.Text + ""  ' coming in here when change to tax exempt, not right but leave for now - TODO
                    chngOrdLn.taxBasis = soLn("TAXABLE_AMT")
                    chngOrdLn.storeTaxChg = 0
                Else
                    chngOrdLn.taxResp = ""
                    chngOrdLn.taxCd = ""
                    chngOrdLn.taxBasis = "NaN"
                    chngOrdLn.custTaxChg = "NaN"
                    chngOrdLn.storeTaxChg = 0
                End If
            End If
        End If   'end if voiding a line

        'check if any of the salesperson info changed or not
        ' Daniela do not update so_ln if SO Header Slsp info has changed 
        updt.slsprsnInfo = HasLineSlspersonInfoChanged(soLn) ' TODO - need to add auto_cmnt on slsp chng

        ' TODO - need to add cmd to auto comment creation for atomic transaction
        'Update the auto comments if the quantity/SKU/retail change
        If CDbl(soLn("QTY", DataRowVersion.Original).ToString) <> CDbl(soLn("QTY", DataRowVersion.Current).ToString) Then
            HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "SQC", soLn("QTY", DataRowVersion.Original).ToString, soLn("QTY", DataRowVersion.Current).ToString, Session("EMP_CD"), soLn("DEL_DOC_LN#"))
        End If
        ' SKU is not allowed to change on the line - inventory update doesn't handle this or updt process
        'If soLn("ITM_CD", DataRowVersion.Original).ToString <> soLn("ITM_CD", DataRowVersion.Current).ToString Then
        '    Add_Auto_GERS_Comments(txt_del_doc_num.Text, txt_wr_dt.Text, "SKC", soLn("ITM_CD", DataRowVersion.Original).ToString, soLn("ITM_CD", DataRowVersion.Current).ToString, soLn("DEL_DOC_LN#"))
        'End If
        If CDbl(soLn("UNIT_PRC", DataRowVersion.Original).ToString) <> CDbl(soLn("UNIT_PRC", DataRowVersion.Current).ToString) Then
            Dim approverCd = Session("EMP_CD")
            If Not IsDBNull(soLn("prc_chg_app_cd", DataRowVersion.Current)) AndAlso soLn("prc_chg_app_cd", DataRowVersion.Current).ToString.isNotEmpty Then
                approverCd = soLn("prc_chg_app_cd", DataRowVersion.Current).ToString
            End If
            'HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "SRC", soLn("UNIT_PRC", DataRowVersion.Original).ToString, soLn("UNIT_PRC", DataRowVersion.Current).ToString, approverCd, soLn("DEL_DOC_LN#"))
            Dim currentDiscountAmount As Double = 0
            Dim originalDiscountAmount As Double = 0
            Dim retailPrice As Double = 0
            Dim originalRetailPrice As Double = 0
            Double.TryParse(soLn("UNIT_PRC", DataRowVersion.Current) & "", retailPrice)
            Double.TryParse(soLn("UNIT_PRC", DataRowVersion.Original) & "", originalRetailPrice)
            Double.TryParse(soLn("DISC_AMT", DataRowVersion.Current) & "", currentDiscountAmount)
            Double.TryParse(soLn("DISC_AMT", DataRowVersion.Original) & "", originalDiscountAmount)

            If (currentDiscountAmount = 0 AndAlso originalDiscountAmount = 0) Then
                'no discount is appled at all
                HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "SRC", soLn("UNIT_PRC", DataRowVersion.Original).ToString, soLn("UNIT_PRC", DataRowVersion.Current).ToString, approverCd, soLn("DEL_DOC_LN#"))
            ElseIf currentDiscountAmount = 0 AndAlso originalDiscountAmount <> 0 Then
                'if discount is removed
                If (retailPrice - originalDiscountAmount) <> originalRetailPrice Then
                    'if discount is removed and price also changed!
                    HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "SRC", originalRetailPrice, retailPrice - originalDiscountAmount, approverCd, soLn("DEL_DOC_LN#"))
                Else
                    'do nothing
                End If
                HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "DISC", retailPrice - originalDiscountAmount, retailPrice, approverCd, soLn("DEL_DOC_LN#"))
            Else
                Dim isPriceChanged = False
                If (originalRetailPrice - currentDiscountAmount + originalDiscountAmount = retailPrice) Then
                    'If Discount alone is added or removed!
                Else
                    'when discount applied and there is price change
                    isPriceChanged = True
                    HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "SRC", soLn("UNIT_PRC", DataRowVersion.Original).ToString, retailPrice + currentDiscountAmount, approverCd, soLn("DEL_DOC_LN#"))
                End If
                Dim oldValue As Double = 0
                If isPriceChanged Then
                    oldValue = retailPrice + currentDiscountAmount
                Else
                    oldValue = originalRetailPrice
                End If
                HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "DISC", oldValue, retailPrice, approverCd, soLn("DEL_DOC_LN#"))

            End If
        End If
    End Sub

    ''' <summary>
    ''' Determines if the warranty link has indeed changed based on the original and current
    ''' line number of the warranty line. 
    ''' </summary>
    ''' <param name="origWarrByLnNum">the original value of the warranty line number</param>
    ''' <param name="currentWarrByLnNum">the current value of the warranty line number</param>
    ''' <returns>a flag indicating if the link was updated</returns>
    Private Function HasWarrantyLinkChanged(ByVal origWarrByLnNum As String, ByVal currentWarrByLnNum As String) As Boolean
        Dim rtnVal As Boolean = False

        Dim origValEmpty As Boolean = origWarrByLnNum.isEmpty
        Dim currValEmpty As Boolean = currentWarrByLnNum.isEmpty

        'this will catch if the orig was empty and now is not, or that it was something and now is not
        'implying that the link was updated.
        rtnVal = (origValEmpty AndAlso Not currValEmpty) OrElse (Not origValEmpty AndAlso currValEmpty)

        Return rtnVal
    End Function

    ''' <summary>
    ''' Updates the SO or order header information
    ''' </summary>
    ''' <param name="cmd">the command object to use to execute the query</param>
    Private Sub UpdateOrderHeader(ByRef cmd As OracleCommand)

        Dim sql As String
        Try
            Dim soRowid As String = String.Empty
            Try
                soRowid = SalesUtils.LockSoRec(txt_del_doc_num.Text, cmd)

            Catch oraex As OracleException
                ModifyControls(1) ' disable controls other than Clear button!
                Throw oraex
            Catch genExp As Exception
                ModifyControls(1) ' disable controls other than Clear button!
                Throw genExp
            End Try
            sql = "UPDATE SO "
            sql = sql & "SET SHIP_TO_F_NAME='" & Replace(txt_f_name.Text.ToString.Trim, "'", "''") & "', "
            sql = sql & "SHIP_TO_L_NAME='" & Replace(txt_l_name.Text.ToString.Trim, "'", "''") & "', "
            sql = sql & "SHIP_TO_CORP='" & Replace(txt_corp_name.Text.ToString.Trim, "'", "''") & "', "
            sql = sql & "SHIP_TO_ADDR1='" & Replace(txt_addr1.Text.ToString.Trim, "'", "''") & "', "
            sql = sql & "SHIP_TO_ADDR2='" & Replace(txt_addr2.Text.ToString.Trim, "'", "''") & "', "
            sql = sql & "SHIP_TO_H_PHONE='" & StringUtils.FormatPhoneNumber(txt_h_phone.Text.ToString.Trim) & "', "
            sql = sql & "SHIP_TO_B_PHONE='" & StringUtils.FormatPhoneNumber(txt_b_phone.Text.ToString.Trim) & "', "
            sql = sql & "SHIP_TO_CITY='" & Replace(txt_city.Text.ToString.Trim, "'", "''") & "', "
            sql = sql & "SHIP_TO_ST_CD='" & cbo_State.SelectedValue.ToString.Trim & "', "
            sql = sql & "SHIP_TO_ZIP_CD='" & txt_zip.Text.ToString.Trim & "', "
            sql = sql & "SO_EMP_SLSP_CD1='" & lbl_hdr_slsp1_cd.Text.ToString.Trim & "' "
            If txt_slsp2.Text.ToString.Trim & "" <> "" Then
                sql = sql & ", SO_EMP_SLSP_CD2 = '" & lbl_hdr_slsp2_cd.Text.ToString.Trim & "' "
            Else
                sql = sql & ", SO_EMP_SLSP_CD2 = NULL "
            End If
            If Not String.IsNullOrEmpty(txt_comm1.Text.Trim) AndAlso (IsNumeric(txt_comm1.Text)) Then
                If CDbl(txt_comm1.Text) > 0 And CDbl(txt_comm1.Text) <= 100 Then
                    sql = sql & ",PCT_OF_SALE1='" & FormatNumber(txt_comm1.Text.ToString.Trim, 2) & "' "
                ElseIf CDbl(txt_comm1.Text) = 0 Then
                    txt_comm2.Text = 100
                    sql = sql & ",PCT_OF_SALE1='" & FormatNumber(txt_comm1.Text.ToString.Trim, 2) & "' "
                End If
            End If
            If Not String.IsNullOrEmpty(txt_comm2.Text.Trim) AndAlso (IsNumeric(txt_comm2.Text)) Then
                If CDbl(txt_comm2.Text) > 0 And CDbl(txt_comm2.Text) <= 100 Then
                    sql = sql & ",PCT_OF_SALE2='" & FormatNumber(txt_comm2.Text.ToString.Trim, 2) & "' "
                Else
                    sql = sql & ",PCT_OF_SALE2='" & FormatNumber(0, 2) & "' "
                End If
            Else
                sql = sql & ",PCT_OF_SALE2='" & FormatNumber(0, 2) & "' "
            End If
            If (SessVar.discCd.isEmpty) Then
                sql = sql & ", DISC_CD = NULL"
            Else
                sql = sql & ", DISC_CD = '" & SessVar.discCd & "' "
            End If
            '  If lbl_PD.Text <> lbl_new_tp.Text And lbl_new_tp.Text & "" <> "" Or lbl_new_dt.Text <> lbl_curr_dt.Text And lbl_new_tp.Text & "" <> "" Or lbl_curr_pd_store.Text <> lbl_new_pd_store.Text And lbl_new_tp.Text & "" <> "" Then
            If lbl_new_tp.Text.Trim & "" <> "" Then
                If lbl_new_tp.Text.Trim <> lbl_curr_tp.Text.Trim Then
                    sql = sql & ", PU_DEL = '" & lbl_new_tp.Text & "' "
                End If
                If lbl_curr_pd_store.Text.Trim <> lbl_new_pd_store.Text.Trim Then
                    sql = sql & ", PU_DEL_STORE_CD = '" & cbo_pd_store_cd.Value & "' "
                End If
                If lbl_new_zone.Text.Trim <> lbl_curr_zone.Text.Trim AndAlso lbl_new_zone.Text.Trim & "" <> "" Then
                    sql = sql & ", SHIP_TO_ZONE_CD = '" & lbl_new_zone.Text & "' "
                End If
                If lbl_new_dt.Text.Trim & "" <> "" Then
                    sql = sql & ", PU_DEL_DT = TO_DATE('" & lbl_new_dt.Text & "','mm/dd/RRRR') "
                End If
            End If
            If Session("USR_FLD_1") & "" <> "" Then
                sql = sql & ", USR_FLD_1 = '" & Session("USR_FLD_1") & "' "
            End If
            If Session("USR_FLD_2") & "" <> "" Then
                sql = sql & ", USR_FLD_2 = '" & Session("USR_FLD_2") & "' "
            End If
            If Session("USR_FLD_3") & "" <> "" Then
                sql = sql & ", USR_FLD_3 = '" & Session("USR_FLD_3") & "' "
            End If
            If Session("USR_FLD_4") & "" <> "" Then
                sql = sql & ", USR_FLD_4 = '" & Session("USR_FLD_4") & "' "
            End If
            If Session("USR_FLD_5") & "" <> "" Then
                sql = sql & ", USR_FLD_5 = '" & Session("USR_FLD_5") & "' "
            End If
            If cbo_tax_exempt.Value & "" <> "" And lbl_tax_cd.Text = "" Then
                sql = sql & ", TET_CD = '" & cbo_tax_exempt.Value & "' "
            Else
                sql = sql & ", TET_CD = NULL "
            End If
            If lbl_tax_cd.Text & "" <> "" Then
                sql = sql & ", TAX_CD = '" & lbl_tax_cd.Text & "' "
            Else
                sql = sql & ", TAX_CD = NULL "
            End If
            If cboOrdSrt.Value & "" <> "" Then
                sql = sql & ", ORD_SRT_CD = '" & cboOrdSrt.Value & "' "
            Else
                sql = sql & ", ORD_SRT_CD = NULL "
            End If
            If lbl_po.Text & "" <> "" Then
                sql = sql & ", ORDER_PO_NUM = '" & txt_po.Text & "' "
            Else
                sql = sql & ", ORDER_PO_NUM = NULL "
            End If
            Dim lineDelTax As Double = 0.0
            Dim lineSetupTax As Double = 0.0
            If IsNumeric(lblDelivery.Text) Then
                sql = sql & ",DEL_CHG=" & CDbl(lblDelivery.Text.ToString.Trim) & " "
                If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
                    lineDelTax = getTaxAmt(lbl_tax_cd.Text, FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate), CDbl(lblDelivery.Text), "DEL")
                End If
            End If
            If IsNumeric(lblSetup.Text) Then
                sql = sql & ",SETUP_CHG=" & CDbl(lblSetup.Text.ToString.Trim) & " "
                If ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
                    lineSetupTax = getTaxAmt(lbl_tax_cd.Text, FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate), CDbl(lblSetup.Text), "SETUP")
                End If
            End If
            If IsNumeric(lblTax.Text) Then ' includes setup and delivery tax whether header or line method; if header, total tax is put on SO.tax_chg; if line, this sb only del/setup tax
                If ConfigurationManager.AppSettings("tax_line").ToString = "N" Then ' header tax TODO but I can pick a header tax code even though line enabled so that's a problem
                    sql = sql & ",TAX_CHG=" & CDbl(lblTax.Text.ToString.Trim) & " "
                Else  ' line tax
                    sql = sql & ",TAX_CHG=" & lineDelTax + lineSetupTax & " "
                End If
            Else
                sql = sql & ",TAX_CHG=" & CDbl(0) & " "
            End If
            If cbo_finance_company.SelectedValue & "" <> "" Then
                sql = sql & ",FIN_CUST_CD='" & cbo_finance_company.SelectedValue & "' "
            End If
            If IsNumeric(txt_fi_amt.Text) Then
                If CDbl(txt_fi_amt.Text) > 0 Then
                    sql = sql & ",ORIG_FI_AMT=" & CDbl(txt_fi_amt.Text.ToString.Trim)
                End If
            End If
            If ddlConfStaCd.SelectedItem IsNot Nothing Then
                sql = sql & ",CONF_STAT_CD='" & ddlConfStaCd.SelectedItem.Value.ToString & "' "
            End If

            sql = sql & "WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
            sql = UCase(sql)
            cmd.CommandText = sql
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Adds auto comments based on the order data.
    ''' </summary>
    Private Sub AddAutoComments(ByRef dbCommand As OracleCommand, ByRef origDRow As DataRow, ByVal origDTable As DataTable)

        Try

            For Each origDRow In origDTable.Rows
                If txt_fi_amt.Text <> origDRow("ORIG_FI_AMT").ToString Then
                    If Not IsNumeric(txt_fi_amt.Text) Then txt_fi_amt.Text = 0
                    If Not IsNumeric(origDRow("ORIG_FI_AMT").ToString) Then origDRow("ORIG_FI_AMT") = 0
                    If CDbl(txt_fi_amt.Text) <> CDbl(origDRow("ORIG_FI_AMT").ToString) Then
                        HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "FA", origDRow("ORIG_FI_AMT").ToString, txt_fi_amt.Text, Session("EMP_CD"))
                    End If
                End If
                ' Removed finance add comment on "Save Changes" buttton and implemented code
                ' when finance information change during finance popup is open under "addFinancePopupComments" function (#ITREQUEST-149)
                'If financeProviderCode.ToString <> origDRow("FIN_CUST_CD").ToString Then
                '    HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "FCC", origDRow("FIN_CUST_CD").ToString, financeProviderCode.ToString, Session("EMP_CD"))
                'End If
                If lbl_PD.Text <> lbl_new_tp.Text And lbl_PD.Text & "" <> "" And lbl_new_tp.Text & "" <> "" Then
                    HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "PDF", lbl_PD.Text, lbl_new_tp.Text, Session("EMP_CD"))
                End If
                If Not String.IsNullOrEmpty(lbl_new_zone.Text) AndAlso lbl_new_zone.Text <> lbl_curr_zone.Text Then
                    HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "ZC", lbl_curr_zone.Text, lbl_new_zone.Text, Session("EMP_CD"))
                End If
                If Not IsNumeric(lblTax.Text) Then lblTax.Text = 0
                If Not IsNumeric(origDRow("TAX_CHG").ToString) Then origDRow("TAX_CHG") = 0
                If CDbl(lblTax.Text) <> CDbl(origDRow("TAX_CHG").ToString) Then
                    HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "TC", origDRow("TAX_CHG").ToString, lblTax.Text, Session("EMP_CD"))
                End If
                If Not IsNumeric(lblDelivery.Text) Then lblDelivery.Text = 0
                If Not IsNumeric(origDRow("DEL_CHG").ToString) Then origDRow("DEL_CHG") = 0
                If CDbl(lblDelivery.Text) <> CDbl(origDRow("DEL_CHG").ToString) Then
                    HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "DC", origDRow("DEL_CHG").ToString, lblDelivery.Text, Session("EMP_CD"))
                End If
                If Not IsNumeric(lblSetup.Text) Then lblSetup.Text = 0
                If Not IsNumeric(origDRow("SETUP_CHG").ToString) Then origDRow("SETUP_CHG") = 0
                If CDbl(lblSetup.Text) <> CDbl(origDRow("SETUP_CHG").ToString) Then
                    HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "SC", origDRow("SETUP_CHG").ToString, lblSetup.Text, Session("EMP_CD"))
                End If
                If lbl_new_dt.Text <> lbl_curr_dt.Text And IsDate(lbl_new_dt.Text) Then
                    HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "PDD", lbl_curr_dt.Text, lbl_new_dt.Text, Session("EMP_CD"))
                End If
                If StringUtils.NullValStr(origDRow("CONF_STAT_CD").ToString, " ") <> StringUtils.NullValStr(ddlConfStaCd.SelectedItem.Value, " ") Then

                    HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "CON", origDRow("CONF_STAT_CD").ToString,
                                    ddlConfStaCd.SelectedItem.Value, Session("EMP_CD"))
                End If
            Next

            ' Add auto comemnt for tax code change only when the user has actually gone in to change it
            If Not (IsNothing(Session("MANUAL_TAX_CD"))) AndAlso
                Session("MANUAL_TAX_CD").ToString.isNotEmpty AndAlso
                Session("MANUAL_TAX_CD") <> Session("TAX_CD") Then
                HBCG_Utils.SaveAutoComment(dbCommand, txt_del_doc_num.Text, txt_wr_dt.Text, "TCD", Session("MANUAL_TAX_CD"), Session("TAX_CD"), Session("EMP_CD"))
            End If
        Catch ex As Exception
            ' Fix ora error 06-15-2018
            dbCommand.Parameters.Clear()
        End Try
    End Sub

    ''' <summary>
    ''' Updates the customer info such as email and tax-exempt ID
    ''' </summary>
    ''' <param name="cmd">teh command object to use to execute the query</param>
    Private Sub UpdateCustInfo(ByRef cmd As OracleCommand)

        Dim Sql As String = String.Empty

        cmd.Parameters.Clear()

        If Not String.IsNullOrEmpty(txt_email.Text.ToString) Then ' do not use IsNullOrWhiteSpace or will not clear
            Sql = "UPDATE CUST "
            Sql = Sql & "SET EMAIL_ADDR='" & txt_email.Text.ToString.Trim & "' "
            Sql = Sql & "WHERE CUST_CD='" & txt_Cust_cd.Text & "'"
            Sql = UCase(Sql)
            cmd.CommandText = Sql
            cmd.ExecuteNonQuery()
        End If

        'Update email address in customer file
        If cbo_tax_exempt.Value & "" <> "" Then
            If txt_exempt_id.Text & "" <> "" Then
                Sql = "UPDATE CUST "
                Sql = Sql & "SET TET_ID#='" & txt_exempt_id.Text & "', TET_CD='" & cbo_tax_exempt.Value & "' "
                Sql = Sql & "WHERE CUST_CD='" & txt_Cust_cd.Text & "'"
                Sql = UCase(Sql)
                cmd.CommandText = Sql
                cmd.ExecuteNonQuery()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Updates and executes all the updates needed when the order passed-in is voided.
    ''' </summary>
    ''' <param name="cmd">the command object to use for executing the sql</param>
    ''' <param name="transPointTots">the transportation points object to be updated when voiding the order</param>
    Private Sub VoidOrder(ByRef cmd As OracleCommand, ByRef transPointTots As TransportationPoints)

        Dim sql As String
        ViewState("SaleVoid") = Nothing
        'Update the sales order table
        sql = "UPDATE SO SET STAT_CD='" & cbo_stat_cd.SelectedValue & "', FINAL_DT=TO_DATE('" & txt_fv_dt.Text & "','mm/dd/RRRR'), FINAL_STORE_CD='" & final_store_cd.SelectedValue & "' "
        If cbo_stat_cd.SelectedValue = AppConstants.Order.STATUS_VOID Then
            sql = sql & ", VOID_CAUSE_CD='" & cbo_void_cause_cd.SelectedValue & "', EMP_CD_OP = '" & Session("emp_cd") & "'"
        End If
        sql = sql & " WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"

        cmd.Parameters.Clear()
        cmd.CommandText = sql
        cmd.ExecuteNonQuery()

        Dim ds As DataSet = Session("DEL_DOC_LN#")
        Dim chngOrdLn As New OrderLine
        For Each dr In ds.Tables(0).Rows

            If dr.RowState <> DataRowState.Added AndAlso dr("VOID_FLAG", DataRowVersion.Original).ToString = "N" Then

                If dr("STORE_CD", DataRowVersion.Original).ToString & "" <> "" And txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then

                    '****send the original void flag, no SO_LN update needed on void of order header - confirmed in E1 SOM
                    '*****Even if Voiding the ENTIRE ORDER, DO NOT VOID the LINES - needed for E1 reports to work properly
                    If (Not Credit_Inventory(cmd, dr("QTY", DataRowVersion.Original).ToString, dr("DEL_DOC_NUM").ToString, dr("ITM_CD").ToString, dr("STORE_CD", DataRowVersion.Original).ToString,
                                             dr("LOC_CD", DataRowVersion.Original).ToString, dr("DEL_DOC_LN#").ToString, dr("FIFL_DT").ToString, dr("FIFO_CST").ToString, dr("FIFO_DT").ToString,
                                             dr("OOC_QTY"), chngOrdLn, dr("SERIAL_NUM", DataRowVersion.Original).ToString, dr("VOID_FLAG", DataRowVersion.Original).ToString, dr("OUT_ID", DataRowVersion.Original).ToString,
                                             dr("OUT_CD", DataRowVersion.Original).ToString)) Then
                        'since inv processing failed, stop the process
                        Throw New Exception(lbl_desc.Text & "Errors were encountered and the operation could not be completed.")
                    End If

                ElseIf txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM Then

                    HBCG_Utils.Finalize_Inv_Xref(dr("DEL_DOC_NUM"), dr("DEL_DOC_LN#"), dr("SERIAL_NUM").ToString, cmd)
                End If

                Calc_SoLn_Points(dr, txt_ord_tp_cd.Text, transPointTots)
            End If
        Next
        Session("DEL_DOC_LN#") = ds

        'Update the AR_TRN table
        Dim des As String = "VOID-" + String.Format("{0:MMddyyyy}", DateTime.Now)
        sql = "UPDATE AR_TRN SET AMT=0, STAT_CD='T', DES = '" & des & "', POST_DT = TO_DATE('"
        sql = sql & FormatDateTime(Now(), DateFormat.ShortDate) & "','mm/dd/RRRR') WHERE "

        Dim where As String = "ADJ_IVC_CD"
        If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL OrElse txt_ord_tp_cd.Text = AppConstants.Order.TYPE_MDB Then   ' TODO MDB is adj_ivc_cd ???
            where = "IVC_CD "
        End If
        sql = sql & where & "= '" & txt_del_doc_num.Text & "' "
        sql = sql & "AND TRN_TP_CD = '" & txt_ord_tp_cd.Text & "' "

        cmd.CommandText = sql
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()

        '*** Since the order got voided, remove all warranty links for it
        If cbo_related_docs.Visible Then
            SalesUtils.DeleteSoLnWarranties(txt_del_doc_num.Text, cmd, txt_wr_dt.Text, True)
        Else
            SalesUtils.DeleteSoLnWarranties(txt_del_doc_num.Text, cmd, txt_wr_dt.Text, False)
        End If


    End Sub

    Private Class ValidateSaveResponseDtc
        Property msg As String = String.Empty
        Property allowSave As Boolean = False
    End Class

    ''' <summary>
    ''' Makes sure that the salespersons and their commisions are valid
    ''' </summary>
    ''' <param name="slsp1">the first salesperson</param>
    ''' <param name="slsp1Comm">the commission for the first salesperson</param>
    ''' <param name="slsp2">the second salesperson</param>
    ''' <param name="slsp2Comm">the commission for the second salesperson</param>
    ''' <returns>a message result of the validation; or an empty string if validations passed</returns>
    Private Function ValidateSalesPersons(ByVal slsp1 As String, ByVal slsp1Comm As Double,
                                          ByVal slsp2 As String, ByVal slsp2Comm As Double) As String
        Dim valResult As String = String.Empty

        If slsp1.isEmpty() Then
            valResult = "You must enter at least one salesperson"

        ElseIf slsp2.isEmpty AndAlso slsp2Comm <> 0 Then
            valResult = "You must enter a second salesperson"

        ElseIf slsp2.isNotEmpty AndAlso slsp1 = slsp2 Then
            valResult = "First and Second salesperson must not be the same"

        ElseIf (slsp1Comm + slsp2Comm <> 100) Then
            valResult = "Total commission must equal 100"

        Else
            '-- validates salespersons exists
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String = "SELECT EMP.LNAME, EMP.FNAME, EMP.HOME_STORE_CD, EMP.EMP_CD FROM EMP, EMP_SLSP WHERE EMP.EMP_CD = EMP_SLSP.EMP_CD AND EMP.EMP_INIT = :slsPsn"
            Try
                dbConnection.Open()
                dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
                dbCommand.Parameters.Add(":slsPsn", OracleType.VarChar)
                dbCommand.Parameters(":slsPsn").Value = slsp1
                dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                If Not dbReader.Read Then
                    valResult = "Invalid Salesperson 1"

                ElseIf (Not slsp2.isEmpty()) Then
                    dbCommand.Parameters(":slsPsn").Value = slsp2
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    If Not dbReader.Read Then
                        valResult = "Invalid Salesperson 2"
                    End If
                End If
                dbReader.Close()

            Catch ex As Exception
                valResult = ex.ToString
            Finally
                dbConnection.Close()
            End Try
        End If
        Return valResult

    End Function

    ''' <summary>
    ''' performs validations required before the current document can be saved.
    ''' This method returns a message indicating the validation that didn't pass or an empty string
    ''' if all applied validations passed
    ''' </summary>
    ''' <returns>custom object with the result of the validation</returns>
    Private Function DoPreSaveValidation() As ValidateSaveResponseDtc

        Dim valResult As New ValidateSaveResponseDtc
        'Daniela add try
        Try
            'origStatus, represents the status of the document upon retrieval
            'currStatus, represents the status that the user changed it to (either no-change, 'F'inal or 'V'oid)
            Dim origStatus As String = txt_Stat_cd.Text
            Dim currStatus As String = cbo_stat_cd.SelectedValue
            Dim voidCd As String = cbo_void_cause_cd.SelectedValue
            Dim istFound As Boolean = (Session("ISTFound") & "" = "TRUE")
            Dim soLines As DataSet = Session("DEL_DOC_LN#")
            Dim pdZoneCd As String = Session("ZONE_CD")
            Dim origDelDocNum As String = Session("orig_del_doc_num")
            Dim shuDate As String = FormatDateTime(SessVar.shuDt, DateFormat.ShortDate)
            Dim finalVoidDate As String = txt_fv_dt.Text
            Dim finalStoreCd As String = final_store_cd.SelectedValue
            Dim homePhone As String = txt_h_phone.Text
            Dim busPhone As String = txt_b_phone.Text
            Dim orderTpCd As String = txt_ord_tp_cd.Text
            Dim slsPsn1 As String = txt_slsp1.Text
            Dim slsPsn2 As String = txt_slsp2.Text
            Dim slsPsn1Comm As Double = If(IsNumeric(txt_comm1.Text), CDbl(txt_comm1.Text), 0)
            Dim slsPsn2Comm As String = If(IsNumeric(txt_comm2.Text), CDbl(txt_comm2.Text), 0)

            If (AppConstants.Order.STATUS_FINAL = origStatus OrElse AppConstants.Order.STATUS_VOID = origStatus) Then
                valResult.msg = "You cannot modify voided or finalized sales"
                Return valResult
            End If

            '------------------------------------------------------------------------------------
            '--- From here onwards, process an order that when retrieved had the status of 'O'pen
            '------------------------------------------------------------------------------------

            ' Tax Validation
            ' Added the below code as a part of MM-4240


            Dim dtConfirmationCodes As DataTable = Session("ConfCodes")
            ' June 20 2017 fix yellow triangle for null confirmation code
            Dim confStatCd = If(Not IsNothing(ddlConfStaCd.SelectedItem), ddlConfStaCd.SelectedItem.Value, "UNC")
            Dim currConfCodeInfo() As DataRow = dtConfirmationCodes.Select("CODE = '" & confStatCd & "'")
            Dim confirmdCodeInfo() As DataRow = dtConfirmationCodes.Select("IS_CONFIRMED ='Y'")

            Dim isConfirmedOrAbove As Boolean = (currConfCodeInfo.Length > 0 AndAlso confirmdCodeInfo.Length > 0 AndAlso
                                                (currConfCodeInfo(0)("CODE") = confirmdCodeInfo(0)("CODE") OrElse
                                                currConfCodeInfo(0)("SEQ#") > confirmdCodeInfo(0)("SEQ#")))


            If String.IsNullOrWhiteSpace(txtTaxCd.Text) Then
                If (String.IsNullOrWhiteSpace(cbo_tax_exempt.Value)) Then
                    valResult.msg = Resources.POSMessages.MSG0022
                    Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
                    hidIsInvokeManually.Value = "true"

                    Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
                    Dim hidZipCD As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
                    hidTaxDt.Value = txt_wr_dt.Text.Trim()
                    If SecurityUtils.hasSecurity("SOMTAXC", Session("EMP_CD")) Then
                        hidZipCD.Value = String.Empty
                    End If
                    ucTaxUpdate.LoadTaxData()
                    PopupControlTax.ShowOnPageLoad = True
                    Return valResult
                Else
                    If String.IsNullOrWhiteSpace(txt_exempt_id.Text) Then
                        valResult.msg = Resources.POSMessages.MSG0023
                        txt_exempt_id.Focus()
                        Return valResult
                    End If
                End If
            End If

            '-- zone validation for delivery sale
            If Not String.IsNullOrEmpty(lbl_new_pd_store.Text) AndAlso Not String.IsNullOrEmpty(lbl_new_tp.Text) Then
                If (theInvBiz.RequireZoneCd(lbl_new_tp.Text, Session("STORE_ENABLE_ARS"), txt_ord_tp_cd.Text.Trim())) Then
                    If (String.IsNullOrWhiteSpace(lbl_new_zone.Text)) Then
                        valResult.msg = Resources.POSErrors.ERR0004
                        Return valResult
                    End If

                    If String.IsNullOrWhiteSpace(lbl_new_dt.Text) Then
                        If ("D".Equals(lbl_new_tp.Text)) Then
                            valResult.msg = Resources.POSErrors.ERR0011
                        Else
                            valResult.msg = Resources.POSErrors.ERR0045
                        End If
                        Return valResult
                    End If
                End If
            End If

            '--- Customer validations
            If ("Y" = ConfigurationManager.AppSettings("primary_phone").ToString AndAlso String.IsNullOrEmpty(homePhone)) Then
                valResult.msg = Resources.LibResources.Label820 & vbCrLf
            ElseIf ("Y" = ConfigurationManager.AppSettings("secondary_phone").ToString AndAlso String.IsNullOrEmpty(busPhone)) Then
                valResult.msg = Resources.LibResources.Label822 & vbCrLf
            End If
            If (valResult.msg.isNotEmpty()) Then Return valResult

            '--- Sales person validations
            Dim slsPsnValidation As String = ValidateSalesPersons(slsPsn1, slsPsn1Comm, slsPsn2, slsPsn2Comm)
            If (slsPsnValidation.isNotEmpty()) Then
                valResult.msg = slsPsnValidation
                Return valResult
            End If

            Dim soLn As DataRow
            '--- Confirms credit per line not exceeded and total order
            If SystemUtils.dataSetHasRows(soLines) AndAlso OrderUtils.isCredDocWithOrigSal(orderTpCd, origDelDocNum) Then
                Dim crmMsg As New StringBuilder
                Dim maxExtRet As Double = 0
                For Each soLn In soLines.Tables(0).Rows

                    If (AppConstants.Order.TYPE_CRM = orderTpCd) Then
                        If CDbl(soLn("unit_prc").ToString) > CDbl(soLn("max_ret_prc").ToString) Then
                            If crmMsg.ToString.Length > 0 Then crmMsg.Append(vbCrLf)
                            crmMsg.Append("SKU " + soLn("itm_cd").ToString + " retail " + soLn("unit_prc").ToString + " exceeds maximum " + FormatNumber(soLn("max_ret_prc").ToString, 2))
                        End If
                        If CDbl(soLn("qty").ToString) > CDbl(soLn("max_crm_qty").ToString) Then
                            If crmMsg.ToString.Length > 0 Then crmMsg.Append(vbCrLf)
                            crmMsg.Append("SKU " + soLn("itm_cd").ToString + " quantity " + soLn("qty").ToString + " exceeds maximum " + FormatNumber(soLn("max_crm_qty").ToString, 0))
                        End If
                    End If

                    ' calc and enforce max extended  
                    ' just happens that max_crm_qty got loaded with orig sal qty for MCR so we can use it here to calc MCR only extended too
                    maxExtRet = Math.Round(CDbl(soLn("max_crm_qty").ToString) * CDbl(soLn("max_ret_prc").ToString), SystemUtils.CurrencyPrecision)
                    If Math.Round(CDbl(soLn("qty").ToString) * CDbl(soLn("unit_prc").ToString), SystemUtils.CurrencyPrecision) > maxExtRet Then
                        If crmMsg.ToString.Length > 0 Then crmMsg.Append(vbCrLf)
                        crmMsg.Append("Maximum credit available for SKU " + soLn("itm_cd").ToString + " is " + FormatNumber(maxExtRet, 0))
                    End If
                Next
                If (Not String.IsNullOrEmpty(crmMsg.ToString())) Then
                    valResult.msg = crmMsg.ToString()
                    Return valResult
                End If
            End If

            '--- Order Lines Validations (only applicable for SAL and CRM when Order NOT being voided)
            If (AppConstants.Order.STATUS_VOID <> currStatus AndAlso
                (AppConstants.Order.TYPE_SAL = orderTpCd OrElse AppConstants.Order.TYPE_CRM = orderTpCd)) Then
                Dim atLeastOneNonVoidLn As Boolean = False
                Dim serialNumMissing As Boolean = False
                Dim numLnsWithInvalidRes As Integer = 0
                Dim origStore, currStore As String
                Dim origLoc, currLoc As String

                For Each soLn In soLines.Tables(0).Rows

                    'at least ONE non-voided line is required to save a SAL or CRM document
                    If ("Y" <> soLn("VOID_FLAG", DataRowVersion.Current).ToString()) Then  'only non-voided lines matter
                        atLeastOneNonVoidLn = True

                        'finds out if serial#s are missing because it should not allow finalization 
                        If (AppConstants.Order.STATUS_FINAL = currStatus AndAlso
                            IsSerialType(soLn("SERIAL_TP").ToString) AndAlso isEmpty(soLn("SERIAL_NUM").ToString)) Then
                            serialNumMissing = True
                        End If

                        'makes sure inventory is correct
                        If (soLn("INVENTORY").ToString = "Y") Then
                            currStore = If(Not IsDBNull(soLn("STORE_CD", DataRowVersion.Current)), soLn("STORE_CD", DataRowVersion.Current).ToString.Trim, "")
                            currLoc = If(Not IsDBNull(soLn("LOC_CD", DataRowVersion.Current)), soLn("LOC_CD", DataRowVersion.Current).ToString.Trim, "")

                            'the store is required for FINALIZATION, otherwise is just checked for inv available
                            If (String.IsNullOrEmpty(currStore)) Then
                                If (AppConstants.Order.STATUS_FINAL = currStatus) Then
                                    numLnsWithInvalidRes = numLnsWithInvalidRes + 1
                                End If
                            Else
                                origStore = If(DataRowState.Added = soLn.RowState OrElse IsDBNull(soLn("STORE_CD", DataRowVersion.Original)),
                                               "", soLn("STORE_CD", DataRowVersion.Original).ToString.Trim)
                                origLoc = If(DataRowState.Added = soLn.RowState OrElse IsDBNull(soLn("LOC_CD", DataRowVersion.Original)),
                                             "", soLn("LOC_CD", DataRowVersion.Original).ToString.Trim)

                                'added row, means new reservation; existing row only checks if reservation changed
                                'for SAL checks inventory Always, for CRM only if document is being finalized
                                If (DataRowState.Added = soLn.RowState OrElse origStore <> currStore OrElse origLoc <> currLoc) Then
                                    If (AppConstants.Order.TYPE_SAL = orderTpCd) Then
                                        If ("N" = Check_Inventory(currStore, currLoc, soLn("ITM_CD", DataRowVersion.Current).ToString, soLn("QTY", DataRowVersion.Current).ToString)) Then
                                            numLnsWithInvalidRes = numLnsWithInvalidRes + 1
                                        End If
                                    ElseIf (AppConstants.Order.TYPE_CRM = orderTpCd AndAlso AppConstants.Order.STATUS_FINAL = currStatus) Then
                                        If (theInvBiz.IsValidLocation(currStore, currLoc) = False) Then
                                            numLnsWithInvalidRes = numLnsWithInvalidRes + 1
                                        End If
                                    End If

                                End If
                            End If
                        End If         ' if line is inventoriable
                    End If          ' non-voided lines
                Next

                If (numLnsWithInvalidRes > 0) Then
                    valResult.msg = "There are " & numLnsWithInvalidRes & " items on your order without a filled or valid location.  You must fill all line items with a valid location before saving the order." & vbCrLf & vbCrLf
                ElseIf (serialNumMissing) Then
                    valResult.msg = "There is/are item(s) that require a serial number before your order can be finalized." & vbCrLf & vbCrLf
                ElseIf (atLeastOneNonVoidLn = False) Then
                    If (AppConstants.Order.TYPE_SAL = orderTpCd) Then
                        valResult.msg = "You cannot save an order with no valid (unvoided) lines. You must void the sale." & vbCrLf & vbCrLf
                    ElseIf (AppConstants.Order.TYPE_CRM = orderTpCd) Then
                        valResult.msg = "You cannot save a credit memo with no valid (unvoided) lines. You must void the credit memo." & vbCrLf & vbCrLf
                    End If
                End If
                If (valResult.msg.isNotEmpty()) Then Return valResult

            End If       'if SAL or CRM document   --END of LINE validation

            '--- Additional validations
            If (AppConstants.Order.STATUS_FINAL = currStatus OrElse AppConstants.Order.STATUS_VOID = currStatus) Then
                'if here the user is trying to 'V'oid or 'F'inalize this document

                '--- Makes sure a VOID code has been selected
                If (AppConstants.Order.STATUS_VOID = currStatus AndAlso String.IsNullOrEmpty(voidCd)) Then
                    valResult.msg = "You must select a void cause code"
                    Return valResult
                End If

                '--- Prevents the sale from being finalized if IST links are found.
                If (AppConstants.Order.STATUS_FINAL = currStatus AndAlso SysPms.preventFinalSOonIST AndAlso istFound) Then
                    valResult.msg = "You must remove the IST links before finalizing this sale." & vbCrLf
                    Return valResult
                End If

                ' --- Checks that Final or Void date is valid; stops validation if not
                If (IsDate(finalVoidDate)) Then
                    If DateDiff("d", FormatDateTime(Today.Date, DateFormat.ShortDate), finalVoidDate) > 0 Then
                        valResult.msg = "Your Finalize/Void date must be less than or equal to today" & vbCrLf
                    Else
                        If (IsDate(shuDate)) Then
                            If DateDiff("d", finalVoidDate, shuDate) >= 0 Then
                                valResult.msg = "Your Finalize/Void date must be later than the SHU Date: " & shuDate
                            Else
                                'makes sure the store selected has not closed for the date being used
                                valResult.msg = theSalesBiz.ValidateStoreClosedDate(finalStoreCd, finalVoidDate)
                            End If
                        Else
                            valResult.msg = "There was a problem with your SHU date - see administrator."
                        End If
                    End If
                Else
                    valResult.msg = "You must enter a valid Finalize/Void Date" & vbCrLf
                End If
                If (valResult.msg.isNotEmpty()) Then Return valResult

            Else  'if here the status of the document remains as 'O'pen

                If (AppConstants.Order.TYPE_SAL = orderTpCd OrElse AppConstants.Order.TYPE_CRM = orderTpCd) Then

                    Dim pkpDelDt As Date = Convert.ToDateTime(If(lbl_new_dt.Text.isEmpty, lbl_curr_dt.Text, lbl_new_dt.Text))
                    Dim paidInFull As Boolean = False

                    If (AppConstants.Order.TYPE_SAL = orderTpCd) Then
                        paidInFull = IsSalePaidInFull()

                        '--- Checks if the store is ARS enabled and does ARS validations (only applies to SALE docs)
                        If (Session("STORE_ENABLE_ARS")) Then
                            Dim maxAvailDt As Date
                            If (Not IsNothing(ViewState("MaxAvailDdate")) AndAlso ViewState("MaxAvailDdate").ToString().isNotEmpty) Then
                                maxAvailDt = Convert.ToDateTime(ViewState("MaxAvailDdate").ToString())
                            End If

                            Dim preventedItems As List(Of String) = theSalesBiz.GetPreventedItemsToBeReserved(soLines)
                            If (paidInFull) Then
                                ReserveInventory()    'ALL lines that need a reservation get reservee, since it is PAID in full
                            Else
                                ReserveInventory(preventedItems)   'ONLY dropped items will be reserved, since it is NOT paid in full
                            End If

                            Dim arsRequest As New ArsValidationRequestDtc
                            arsRequest.maxAvailDate = maxAvailDt
                            arsRequest.droppedItemsFlaggedPreventPO = preventedItems
                            arsRequest.checkInvReq.isPaidInFull = paidInFull
                            arsRequest.checkInvReq.zoneCd = If(lbl_new_zone.Text.isEmpty, lbl_curr_zone.Text, lbl_new_zone.Text)
                            arsRequest.checkInvReq.puDel = cbo_PD.Value
                            arsRequest.checkInvReq.puDelDt = pkpDelDt
                            arsRequest.checkInvReq.puDelStoreCd = cbo_pd_store_cd.Value

                            Dim arsResponse As ArsValidationResponseDtc
                            arsResponse = theSalesBiz.ValidateARSInventory(soLines, arsRequest)
                            If (arsResponse.isValid) Then
                                'since updates may have happened to the lines, replaces the session at this point
                                Session("DEL_DOC_LN#") = arsResponse.lnsDataset
                                If (arsResponse.lnsToReserve.Count > 0) Then
                                    ReserveInventory(arsResponse.lnsToReserve)   'IF reservations pending, completes them
                                Else
                                    bindgrid()   'when doing reservations, bind grid happens there already
                                End If
                            Else
                                valResult.msg = arsResponse.message
                                Return valResult
                            End If
                        End If
                    End If

                    '--- Does the Confirmation Logic Validation
                    If (AreConfirmationCodesAvailable()) Then
                        Dim request As New ConfStatusRequestDtc
                        request.isPaidInFull = paidInFull
                        request.hasConfirmPIFSecurity = CheckSecurity(SecurityUtils.OVERRIDE_PAID_IN_FULL, Session("emp_cd"))
                        request.orderType = orderTpCd
                        request.pkpDelDate = pkpDelDt
                        request.pkpDelZone = pdZoneCd
                        request.lnsData = soLines
                        request.confirmationCodes = Session("ConfCodes")
                        request.origConfCode = If(Not IsNothing(Session("OrigConfCode")), Session("OrigConfCode").ToString(), "")
                        request.currConfCode = If(Not IsNothing(ddlConfStaCd.SelectedItem), ddlConfStaCd.SelectedItem.Value.ToString(), "")
                        Dim response As ConfStatusResponseDtc = theSalesBiz.ValidateConfirmationStatus(request)
                        Dim confCdItm As DevExpress.Web.ASPxEditors.ListEditItem = ddlConfStaCd.Items.FindByValue(response.confCode)
                        ddlConfStaCd.SelectedItem = confCdItm
                        valResult.msg = response.message
                        valResult.allowSave = response.allowSaveOrder
                        'this validation may allow save with warning messages, hence the 'allowSave' check
                        If (valResult.msg.isNotEmpty() AndAlso Not valResult.allowSave) Then Return valResult

                        If response.IsConfirmationValidationPassed Then
                        Else
                            If response.confirmationErrorMessage.ToString() & "" = String.Empty Then

                            Else
                                Session("confirmationValidationMessage") = Resources.POSErrors.ERR0051 & response.confirmationErrorMessage.ToString()
                            End If

                        End If
                    End If

                End If  ' end if SAL or CRM

            End If   'end if status of the document remains as 'O'pen
            'Check if the delivery and setup charges are -ve.
            Dim deliveryCharges As Double = 0
            Dim setupCharges As Double = 0
            Dim isValidationSuccess = True
            Double.TryParse(lblSetup.Text.Trim, setupCharges)
            Double.TryParse(lblDelivery.Text.Trim, deliveryCharges)
            If IsNumeric(lblDelivery.Text) AndAlso deliveryCharges >= 0 Then
                lblDelivery.Text = FormatNumber(lblDelivery.Text, 2)
            Else
                lblDelivery.Text = FormatNumber(0, 2)
                valResult.msg = If(valResult.msg.Trim = String.Empty, "", valResult.msg & "<BR />") & Resources.POSErrors.ERR0047
                valResult.allowSave = False
                isValidationSuccess = False
            End If
            If IsNumeric(lblSetup.Text.Trim) AndAlso setupCharges >= 0 Then
                lblSetup.Text = FormatNumber(lblSetup.Text, 2)
            Else
                lblSetup.Text = FormatNumber(0, 2)
                valResult.msg = If(valResult.msg.Trim = String.Empty, "", valResult.msg & "<BR />") & Resources.POSErrors.ERR0048
                valResult.allowSave = False
                isValidationSuccess = False
            End If
            If isValidationSuccess = False Then
                Return valResult
            End If
            '------------------------------------------------------------------------------------
            '--------- if the code makes it this far, means that ALL validations passed ---------
            '------------------------------------------------------------------------------------
            valResult.allowSave = True
            Return valResult

            'Daniela handle errors      

        Catch ex As Exception
            valResult.msg = "System Error while validating the Sales Order. Please check your data and try again."
            Return valResult
        End Try

    End Function


    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click
        ' 11-may-16 lucy

        Dim strErr As String = "Y"
        Dim strDelDocNum As String
        Dim strValue As String = cbo_stat_cd.SelectedValue 'lucy, 11-May-16
        If strValue = "F" Then
            strDelDocNum = txt_del_doc_num.Text.Trim
            strErr = lucy_ctl_final(strDelDocNum)
            If strErr <> "Y" Then
                ' lbl_desc.Text = strErr
                lbl_final.Text = strErr
                Exit Sub
            End If
        End If

        If strValue = "V" Then
            strDelDocNum = txt_del_doc_num.Text.Trim
            strErr = lucy_ctl_void(strDelDocNum)   '25-may-16
            If strErr <> "Y" Then
                ' lbl_desc.Text = strErr
                ' strErr = "Change not allowed. Lines have been picked or checked. The user will need to unpick the affected lines."

                lbl_final.Text = strErr
                Exit Sub
            End If
        End If
        If txt_Stat_cd.Text = AppConstants.Order.STATUS_VOID Or txt_Stat_cd.Text = AppConstants.Order.STATUS_FINAL Then
            Dim conn As OracleConnection
            Dim command As OracleCommand
            Try
                'Save only comments
                Dim orig_ds As DataSet = Session("ORIG_SO")
                conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                conn.Open()
                command = DisposablesManager.BuildOracleCommand("", conn)
                Save_Comments(orig_ds.Tables(0).Rows(0), command)
                ASPxPageControl1.ActiveTabIndex = 0
            Catch ex As Exception
            Finally
                command.Dispose()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        Else
            If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
                PromptForSummaryPriceMgrOverride()
            Else
                Session("SaveSalesOrder") = True
                Session("DEL_DOC_LNOLD#") = Session("DEL_DOC_LN#")
                updateSOLNonUponCommit()
                'SaveSalesOrder()
                If cbo_stat_cd.SelectedValue = AppConstants.Order.STATUS_FINAL Then
                    'Check if all the validations are through 
                    Dim validateResult As ValidateSaveResponseDtc = DoPreSaveValidation()
                    If validateResult.allowSave = False Then
                        lbl_desc.Visible = True
                        lbl_desc.Text = validateResult.msg          'TODO - info messages don't display at this point
                    Else
                        'check if need approval when the order is not paid in full. look into the syspm
                        ProcessFinalizationRequest(ORDERSAVE)
                    End If
                Else
                    SaveSalesOrder()
                End If
            End If
        End If
        btn_Save.Enabled = True
    End Sub

    Private Sub SaveSalesOrder()
        Session("err") = ""   ' need to clear this or remains from previous
        lbl_desc.Text = ""
        Dim UpPanel As UpdateProgress

        '--- Performs the validation required before Save can be done
        Dim validateResult As ValidateSaveResponseDtc = DoPreSaveValidation()
        If validateResult.allowSave = False Then
            lbl_desc.Visible = True
            'TODO - info messages don't display at this point
            'Save the comments only if any one of the comments exists
            'MM-14116
            'atleast one comment should exist to procede
            Dim dec As Boolean = SaveComments()
            If dec Then
                lbl_desc.Text = "The Comments are saved.<BR />"
            End If
            lbl_desc.Text = lbl_desc.Text & validateResult.msg
            Exit Sub   'validation didn't pass, abort save process
        Else
            UpPanel = Master.FindControl("UpdateProgress1")
            UpPanel.Visible = True
        End If

        '--- Starts the SAVE process
        ' All update/insert/delete operations made in the "command" object will be part of 
        ' a single transaction to be able to rollback all changes if something fails
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim connAtomic As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim command As OracleCommand = DisposablesManager.BuildOracleCommand(connAtomic)
        Dim MyDataReader As OracleDataReader
        Dim objSql As OracleCommand
        Dim sql As String
        Dim isSplitDoc As Boolean = False
        Try

            If cbo_related_docs.Visible Then
                isSplitDoc = True
            End If
            conn.Open()
            connAtomic.Open()
            command.Transaction = connAtomic.BeginTransaction()

            '**** Update the SO header info
            UpdateOrderHeader(command)

            '**** Add Auto Comments
            Dim orig_ds As DataSet = Session("ORIG_SO")
            Dim orig_dt As DataTable = orig_ds.Tables(0)      'TODO - error here on void query or something - or maybe below going thru rows
            Dim orig_dr As DataRow
            AddAutoComments(command, orig_dr, orig_dt)

            '***** Update email address in customer file
            UpdateCustInfo(command)

            'Update the AR_TRN table
            sql = "UPDATE AR_TRN SET AMT=" & Replace(CDbl(lblTotal.Text), ",", "") & " "
            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL OrElse txt_ord_tp_cd.Text = AppConstants.Order.TYPE_MDB Then
                sql = sql & "WHERE IVC_CD='" & txt_del_doc_num.Text & "' "
            Else
                ' Daniela Nov 18 add co_cd to use index
                sql = sql & "WHERE ADJ_IVC_CD='" & txt_del_doc_num.Text & "' AND CO_CD = '" & Session("CO_CD") & "' "
            End If
            sql = sql & "AND TRN_TP_CD='" & txt_ord_tp_cd.Text & "' "

            command.CommandText = sql
            command.ExecuteNonQuery()

            Dim transPointTots As New TransportationPoints ' sum of all lines
            transPointTots.Init()

            Dim tax_basis As Double = 0.0
            Dim tax_chg As Double = 0.0

            ' if the order is open and the status is changing (to void or final)
            'If cbo_stat_cd.SelectedValue <> "Change Order Status" And txt_Stat_cd.Text = "O" Then
            If cbo_stat_cd.SelectedValue <> "C" And txt_Stat_cd.Text = "O" Then

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    FINALIZING     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                If cbo_stat_cd.SelectedValue = AppConstants.Order.STATUS_FINAL Then

                    'Update the sales order table
                    sql = "UPDATE SO SET STAT_CD='" & cbo_stat_cd.SelectedValue & "', FINAL_DT=TO_DATE('" & txt_fv_dt.Text & "','mm/dd/RRRR'), FINAL_STORE_CD='" & final_store_cd.SelectedValue & "',EMP_CD_OP= '" & Session("emp_cd") & "'"
                    sql = sql & " WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
                    command.CommandText = sql
                    command.ExecuteNonQuery()

                    Dim co_cd As String = ""
                    Dim acct_num As String = ""
                    ' TODO - hitting ar_tr for anything is sloooow  - need to use written store to co_cd instead
                    'sql = "SELECT co_cd FROM store WHERE store_cd = '" & txt_store_cd.Text & "'" ' needs to be written store 
                    ' BUT need finance acct number (BNK_CRD_NUM) from ar_trn anyway so either get here or elsewhere
                    Select Case txt_ord_tp_cd.Text
                        Case AppConstants.Order.TYPE_SAL
                            sql = "SELECT CO_CD, BNK_CRD_NUM FROM AR_TRN WHERE TRN_TP_CD='SAL' AND IVC_CD='" & txt_del_doc_num.Text & "'"
                        Case AppConstants.Order.TYPE_CRM
                            sql = "SELECT CO_CD, BNK_CRD_NUM FROM AR_TRN WHERE TRN_TP_CD='CRM' AND ADJ_IVC_CD='" & txt_del_doc_num.Text & "'"
                        Case AppConstants.Order.TYPE_MCR
                            sql = "SELECT CO_CD, BNK_CRD_NUM FROM AR_TRN WHERE TRN_TP_CD='MCR' AND ADJ_IVC_CD='" & txt_del_doc_num.Text & "'"
                        Case AppConstants.Order.TYPE_MDB
                            sql = "SELECT CO_CD, BNK_CRD_NUM FROM AR_TRN WHERE TRN_TP_CD='MDB' AND ADJ_IVC_CD='" & txt_del_doc_num.Text & "'"
                    End Select

                    'Set SQL OBJECT 
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If MyDataReader.Read() Then
                        co_cd = MyDataReader.Item("CO_CD").ToString
                        acct_num = HBCG_Utils.IfNull(MyDataReader.Item("BNK_CRD_NUM").ToString)
                    End If

                    If co_cd & "" = "" Then co_cd = Session("CO_CD")

                    'Update the sales order table
                    sql = "INSERT INTO GL_DOC_TRN (DOC_NUM, CO_CD, POST_DT, ORIGIN_CD, PROCESSED) "
                    sql = sql & "VALUES (:DOC_NUM, :CO_CD, SYSDATE, :ORIGIN_CD, :PROCESSED)"

                    command.CommandText = sql
                    command.Parameters.Add(":DOC_NUM", OracleType.VarChar)
                    command.Parameters.Add(":CO_CD", OracleType.VarChar)
                    command.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
                    command.Parameters.Add(":PROCESSED", OracleType.VarChar)

                    command.Parameters(":DOC_NUM").Value = txt_del_doc_num.Text
                    command.Parameters(":CO_CD").Value = co_cd
                    command.Parameters(":ORIGIN_CD").Value = "SOM" ' this must be specific value as used in GLBGP, SOM+ is not in there yet
                    command.Parameters(":PROCESSED").Value = "N"

                    command.ExecuteNonQuery()
                    command.Parameters.Clear()

                    'Update the AR_TRN table
                    sql = "UPDATE AR_TRN SET POST_DT=TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'), "
                    sql = sql & "STAT_CD='T' "
                    If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then
                        sql = sql & "WHERE IVC_CD='" & txt_del_doc_num.Text & "' "
                    Else ' CRM/MDB/MCR
                        sql = sql & "WHERE ADJ_IVC_CD='" & txt_del_doc_num.Text & "' "
                    End If
                    sql = sql & "AND TRN_TP_CD='" & txt_ord_tp_cd.Text & "' "

                    command.CommandText = sql
                    command.ExecuteNonQuery()

                    If (Not String.IsNullOrEmpty(cbo_finance_company.SelectedValue)) AndAlso IsNumeric(txt_fi_amt.Text) AndAlso CDbl(txt_fi_amt.Text) > 0 Then

                        HBCG_Utils.xferCustLiability(txt_del_doc_num.Text, Session("ORIG_DEL_DOC_NUM"), txt_ord_tp_cd.Text, cbo_stat_cd.SelectedValue, txt_fv_dt.Text, txt_store_cd.Text,
                                          CDbl(txt_fi_amt.Text.ToString.Trim), txt_Cust_cd.Text, cbo_finance_company.SelectedValue, Session("emp_cd"), co_cd,
                                          "SOM+", "O", acct_num, command)
                    End If

                    Dim ds As DataSet = Session("DEL_DOC_LN#")
                    Dim dt As DataTable = ds.Tables(0)
                    Dim dr As DataRow
                    Dim itmInfo As DataRow
                    Dim cst As Double = 0
                    ' store and loc are couple of few input (as opposed to selection) and need to be trimmed before processing
                    Dim curr_store_cd As String = ""
                    Dim orig_store_cd As String = ""
                    Dim curr_loc_cd As String = ""
                    Dim orig_loc_cd As String = ""
                    Dim serial_num As String = ""
                    Dim out_id As String = ""
                    Dim pkg_itm_cd As String = ""
                    Dim origOrdLn As OrderLine
                    Dim chngOrdLn As OrderLine
                    Dim updt As soLn_updates

                    For Each dr In ds.Tables(0).Rows

                        ' the voided lines were being processed as modified so eliminate that overhead
                        If dr.RowState = DataRowState.Added AndAlso "Y".Equals(dr("VOID_FLAG", DataRowVersion.Current).ToString) Then
                            ' Do nothing, Do not save add lines that were added and voided before saving (will fail if test on next if)

                        ElseIf dr.RowState = DataRowState.Added OrElse dr("VOID_FLAG", DataRowVersion.Original).ToString <> "Y" Then ' no original state if line added
                            'means that either the row was added or that originally the row was not void

                            ProcessPOLinks(dr, command)

                            curr_store_cd = IIf(String.IsNullOrWhiteSpace(dr("STORE_CD").ToString), "", UCase(dr("STORE_CD").ToString.Trim))
                            curr_loc_cd = IIf(String.IsNullOrWhiteSpace(dr("LOC_CD").ToString), "", UCase(dr("LOC_CD").ToString.Trim))
                            serial_num = IIf(String.IsNullOrWhiteSpace(dr("SERIAL_NUM").ToString), "", UCase(dr("SERIAL_NUM").ToString.Trim))
                            out_id = IIf(String.IsNullOrWhiteSpace(dr("OUT_ID").ToString), "", UCase(dr("OUT_ID").ToString.Trim))
                            itmInfo = SkuUtils.GetItemInfo(dr("ITM_CD").ToString())

                            If IsNumeric(dr("FIFO_CST").ToString) Then
                                cst = dr("FIFO_CST")
                            ElseIf IsNumeric(itmInfo("REPL_CST").ToString) Then
                                cst = itmInfo("REPL_CST")
                            Else
                                cst = 0
                            End If

                            If dr.RowState = DataRowState.Added Then

                                'caches the package item to be used in the SO_LN.PKG_SOURCE column when saving its components
                                If ("PKG" = dr("ITM_TP_CD")) Then
                                    pkg_itm_cd = dr("ITM_CD")
                                ElseIf (dr("PACKAGE_PARENT")) & "" = "" Then
                                    pkg_itm_cd = ""
                                End If
                                If (ConfigurationManager.AppSettings("tax_line").ToString = "Y" Or ("L".Equals(ViewState("TaxMethod")))) AndAlso
                                    IsNumeric(dr("TAXABLE_AMT").ToString) AndAlso
                                    IsNumeric(dr("CUST_TAX_CHG").ToString) Then

                                    tax_basis = CDbl(dr("TAXABLE_AMT").ToString)
                                    tax_chg = CDbl(dr("CUST_TAX_CHG").ToString)
                                End If

                                Add_Sales_Order_Line(command, dr, curr_store_cd, curr_loc_cd, serial_num, tax_basis, tax_chg, pkg_itm_cd, Session("SPLIT_SO_LN"))

                                If itmInfo("INVENTORY").ToString = "Y" Then
                                    Insert_ITM_TRN(dr("QTY"), dr("DEL_DOC_NUM"), dr("ITM_CD"), curr_store_cd, curr_loc_cd, dr("DEL_DOC_LN#"), dr("FIFL_DT").ToString,
                                                   cst.ToString, dr("FIFO_DT").ToString, command, dr("SERIAL_NUM").ToString, dr("OUT_ID").ToString, dr("OUT_CD").ToString)
                                End If
                                HBCG_Utils.SaveAutoComment(command, txt_del_doc_num.Text, txt_wr_dt.Text, "NLA", dr("itm_cd").ToString, dr("itm_cd").ToString, Session("EMP_CD"), dr("DEL_DOC_LN#"))

                                ' Check if price has changed while adding new item
                                updt = New soLn_updates
                                SoLnCurrentInvAddProcess(dr, orig_dr, updt, origOrdLn, chngOrdLn, command)

                            ElseIf dr.RowState = DataRowState.Modified Then
                                ''''line was modifed - Update the SO_LN table

                                updt = New soLn_updates

                                If (ConfigurationManager.AppSettings("tax_line").ToString = "Y" Or ("L".Equals(ViewState("TaxMethod")))) AndAlso
                                    IsNumeric(dr("TAXABLE_AMT").ToString) AndAlso
                                    IsNumeric(dr("CUST_TAX_CHG").ToString) Then

                                    tax_basis = CDbl(dr("TAXABLE_AMT").ToString)
                                    tax_chg = CDbl(dr("CUST_TAX_CHG").ToString)
                                End If

                                SoLnPreInvUpdtProcess(dr, orig_dr, updt, origOrdLn, chngOrdLn, command)

                                If itmInfo("INVENTORY").ToString = "Y" Then
                                    If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM And dr("VOID_FLAG", DataRowVersion.Current).ToString = "N" And curr_store_cd & "" <> "" Then
                                        Credit_Inventory(command, dr("QTY"), dr("DEL_DOC_NUM"), dr("ITM_CD"), curr_store_cd, curr_loc_cd, dr("DEL_DOC_LN#"), dr("FIFL_DT").ToString, dr("FIFO_CST").ToString,
                                                         dr("FIFO_DT").ToString, 0, chngOrdLn, dr("SERIAL_NUM").ToString, dr("VOID_FLAG", DataRowVersion.Current).ToString, dr("OUT_ID").ToString, dr("OUT_CD").ToString)
                                        updt.inv = True

                                    ElseIf txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then
                                        ' reserve, unreserve, or both as needed
                                        updt.inv = updtInventory(dr, chngOrdLn, command)

                                    End If
                                    If lbl_desc.Text & "" <> "" Then ' InStr(lbl_desc.Text, "Error") > 0 Then - DSA label_desc is not set yet or doesn't have 
                                        'lbl_desc.Text = Session("err") & "Errors were encountered and the operation could not be completed."
                                        Throw New ResUnresException(lbl_desc.Text & vbCrLf & "Errors were encountered and the operation could not be completed.")

                                    ElseIf dr("VOID_FLAG", DataRowVersion.Current).ToString = "N" AndAlso curr_store_cd & "" <> "" AndAlso
                                         (txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL OrElse txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM) Then

                                        Insert_ITM_TRN(dr("QTY"), dr("DEL_DOC_NUM"), dr("ITM_CD"), curr_store_cd, curr_loc_cd, dr("DEL_DOC_LN#"), dr("FIFL_DT").ToString,
                                                       cst.ToString, dr("FIFO_DT").ToString, command, dr("SERIAL_NUM").ToString, chngOrdLn.outId, dr("OUT_CD").ToString)
                                    ElseIf (dr("VOID_FLAG", DataRowVersion.Current).ToString = "Y") Then
                                        ' MM-4240
                                        ' When the line has been voided while finalizing the order we need to update the tax information, 
                                        ' If the tax method is line level tax
                                        ' MM-4240
                                        ' When the line has been voided while saving the order we need to update the tax information, 
                                        ' If the tax method is line level tax
                                        If ("L".Equals(ViewState("TaxMethod"))) Or ("H".Equals(ViewState("TaxMethod"))) Or ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
                                            updt.tax = True
                                        ElseIf (Convert.ToBoolean(ViewState("TaxExempted"))) Then
                                            ' When tax exemption has been given
                                            ' I have separated this condition so that we can have additional updates on SO_LN if required in furure
                                            updt.tax = True
                                        End If
                                        'If ("L".Equals(ViewState("TaxMethod"))) Or ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
                                        'updt.tax = True
                                        'End If
                                    End If
                                End If

                                Updt_SoLn(origOrdLn, chngOrdLn, updt, command, True, isSplitDoc)

                                ' if line not modified but non-void inventory, then still need to insert SOF to ITM_TRN
                            ElseIf itmInfo("INVENTORY").ToString = "Y" AndAlso dr("VOID_FLAG", DataRowVersion.Current).ToString = "N" AndAlso curr_store_cd & "" <> "" AndAlso
                                         (txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL OrElse txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM) Then

                                Insert_ITM_TRN(dr("QTY"), dr("DEL_DOC_NUM"), dr("ITM_CD"), curr_store_cd, curr_loc_cd, dr("DEL_DOC_LN#"), dr("FIFL_DT").ToString,
                                               cst.ToString, dr("FIFO_DT").ToString, command, dr("SERIAL_NUM").ToString, out_id, dr("OUT_CD").ToString)
                            End If  'end if dr.RowState = DataRowState.Modified

                            'Code is forcing Credit Memos to move to INV_XREF_ARC and throws off inventory.
                            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL AndAlso
                               itmInfo("INVENTORY").ToString = "Y" AndAlso
                                (ConfigurationManager.AppSettings("xref") = "Y" OrElse ConfigurationManager.AppSettings("enable_serialization") = "Y") AndAlso
                                (itmInfo("BULK_TP_ITM").ToString = "N") Then 'TODO E1 default is different - depends on mjr?

                                HBCG_Utils.Finalize_Inv_Xref(dr("DEL_DOC_NUM"), dr("DEL_DOC_LN#"), dr("SERIAL_NUM").ToString, command, "FIN")
                            End If
                            Calc_SoLn_Points(dr, txt_ord_tp_cd.Text, transPointTots)

                        End If
                    Next

                    '** if there are any CRM lines with orig order are being finalized, check if unlinking of warranties needs to occur                        
                    UnlinkFinalizedCrmLines(command)

                    If SysPms.allowMultDisc AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") Then
                        ProcessSoLnDiscounts(dr("DEL_DOC_NUM"), command)
                    End If

                    ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    VOIDING     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                ElseIf cbo_stat_cd.SelectedValue = AppConstants.Order.STATUS_VOID Then
                    VoidOrder(command, transPointTots)
                End If  'end if voiding the order

                ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    UPDATING OPEN ORDER(NOT VOIDING OR FINALIZING)    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            Else
                'Update the AR_TRN table    ' TODO - this appears to duplicate the exact same update right before final/void/update breakout - AR_TRN updts are expensive
                sql = "UPDATE AR_TRN SET AMT=" & CDbl(lblTotal.Text) & " "
                If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL OrElse txt_ord_tp_cd.Text = AppConstants.Order.TYPE_MDB Then
                    sql = sql & "WHERE IVC_CD='" & txt_del_doc_num.Text & "'"
                Else
                    'Sep 16 improve performance
                    sql = sql & "WHERE ADJ_IVC_CD='" & txt_del_doc_num.Text & "' AND CO_CD = '" & Session("CO_CD") & "' "
                End If
                sql = sql & "AND TRN_TP_CD='" & txt_ord_tp_cd.Text & "' "

                command.CommandText = sql
                command.ExecuteNonQuery()

                Dim ds As DataSet = Session("DEL_DOC_LN#")
                Dim dt As DataTable = ds.Tables(0)
                Dim dr As DataRow
                Dim curr_store_cd As String = ""
                Dim orig_store_cd As String = ""
                Dim curr_loc_cd As String = ""
                Dim orig_loc_cd As String = ""
                Dim serial_num As String = ""
                Dim pkg_itm_cd As String = ""
                Dim origOrdLn As OrderLine
                Dim chngOrdLn As OrderLine
                Dim updt As soLn_updates
                RemoveInventoryForReplacedItem(command)
                For Each dr In ds.Tables(0).Rows
                    If (ds.Tables(0).Columns.Contains("ISREPLACED") AndAlso dr("ISREPLACED").ToString().Equals("Y")) Then
                        'Remove the old reference from the table, Since this the new Row added, Add_Sales_Order_Line will insert the new line to DB

                        theSalesBiz.RemoveComponent(dr, command)
                    Else
                        'Do Nothing!!
                    End If

                    If dr.RowState = DataRowState.Added AndAlso "Y".Equals(dr("VOID_FLAG", DataRowVersion.Current).ToString) Then
                        ' Do nothing, Do not save add lines that were added and voided before saving (will fail if test on next if)

                    ElseIf dr.RowState = DataRowState.Added OrElse
                            dr("VOID_FLAG", DataRowVersion.Original).ToString <> "Y" Then ' no original state if line added and will error with 'No original state...'

                        curr_store_cd = IIf(String.IsNullOrWhiteSpace(dr("STORE_CD").ToString), "", UCase(dr("STORE_CD").ToString.Trim))
                        curr_loc_cd = IIf(String.IsNullOrWhiteSpace(dr("LOC_CD").ToString), "", UCase(dr("LOC_CD").ToString.Trim))
                        serial_num = IIf(String.IsNullOrWhiteSpace(dr("SERIAL_NUM").ToString), "", UCase(dr("SERIAL_NUM").ToString.Trim))

                        ProcessPOLinks(dr, command)

                        If dr.RowState = DataRowState.Added Then

                            'caches the package item to be used in the SO_LN.PKG_SOURCE column when saving its components
                            If ("PKG" = dr("ITM_TP_CD")) Then
                                pkg_itm_cd = dr("ITM_CD")
                            ElseIf (dr("PACKAGE_PARENT")) & "" = "" Then
                                pkg_itm_cd = ""
                            End If
                            'chngOrdLn = createOrdLn(dr, Tax_Methods.lineTax.Equals(orig_dr("tax_method"))) TODO DSA - after tax fix, modify add line to pass in the object - both here and finalize (or merge these two) so not duplicate anymore

                            If (ConfigurationManager.AppSettings("tax_line").ToString = "Y" Or ("L".Equals(ViewState("TaxMethod")))) AndAlso
                                IsNumeric(dr("TAXABLE_AMT").ToString) AndAlso
                                IsNumeric(dr("CUST_TAX_CHG").ToString) Then

                                tax_basis = CDbl(dr("TAXABLE_AMT").ToString)
                                tax_chg = CDbl(dr("CUST_TAX_CHG").ToString)
                            End If

                            Add_Sales_Order_Line(command, dr, curr_store_cd, curr_loc_cd, serial_num, tax_basis, tax_chg, pkg_itm_cd, Session("SPLIT_SO_LN"))
                            HBCG_Utils.SaveAutoComment(command, txt_del_doc_num.Text, txt_wr_dt.Text, "NLA", dr("itm_cd").ToString, dr("itm_cd").ToString, Session("EMP_CD"), dr("DEL_DOC_LN#"))

                            'MM-11207 - To update the newly added items into INV_XREF for CRM
                            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM AndAlso (Not "Y".Equals(dr("void_flag").ToString)) AndAlso dr("ITM_TP_CD") & "" = "INV" Then
                                HBCG_Utils.AddPendingInventory(command, txt_del_doc_num.Text, dr("DEL_DOC_LN#"), dr("itm_cd").ToString, dr("Qty"), Session("ORIG_DEL_DOC_NUM"))
                            End If

                            ' Check if price has changed while adding new item
                            updt = New soLn_updates
                            SoLnCurrentInvAddProcess(dr, orig_dr, updt, origOrdLn, chngOrdLn, command)

                        ElseIf dr.RowState = DataRowState.Modified Then
                            'Update the SO_LN table since it is modifed. For some reason, it is always modified if not added. So we are doing extra work

                            updt = New soLn_updates

                            SoLnPreInvUpdtProcess(dr, orig_dr, updt, origOrdLn, chngOrdLn, command)

                            ' TODO - we may want to consider making the inventory updt a 2nd atomic trans after the rest except for finalization and void - that way -other changes can take ; - ditto in order_stage
                            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL AndAlso dr("INVENTORY").ToString = "Y" Then

                                updt.inv = updtInventory(dr, chngOrdLn, command)
                            End If
                            ' TODO - this error handling is not used everywhere, sb  - inconsistent and not good  -' debit sets session, credit sets lbl_desc ??? debits don't show msg
                            ' Removing session and referring back to lbl_desc to avoid err
                            'If InStr(Session("err"), "Insufficient") > 0 Then
                            '    lbl_desc.Text = Session("err")
                            'End If
                            If InStr(lbl_desc.Text, "Error") > 0 OrElse InStr(lbl_desc.Text, "Insufficient") > 0 Then
                                Throw New ResUnresException(lbl_desc.Text & "Errors were encountered and the operation could not be completed.")
                            End If

                            ' all SO_LN updates must be applied to chngOrdLn (inv done in calls from updtInventory) and updt flag set accordingly,
                            '   then this call does the actual SO_LN update with all applicable columns

                            ' MM-4240
                            ' When the line has been voided while saving the order we need to update the tax information, 
                            ' If the tax method is line level tax
                            If ("L".Equals(ViewState("TaxMethod"))) Or ("H".Equals(ViewState("TaxMethod"))) Or ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
                                updt.tax = True
                            ElseIf (Convert.ToBoolean(ViewState("TaxExempted"))) Then
                                ' When tax exemption has been given
                                ' I have separated this condition so that we can have additional updates on SO_LN if required in furure
                                updt.tax = True
                            End If
                            '==========================================================================
                            Updt_SoLn(origOrdLn, chngOrdLn, updt, command, False, isSplitDoc)

                            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM Then
                                If "Y".Equals(dr("void_flag").ToString) Then
                                    HBCG_Utils.SubtractInventory(command, txt_del_doc_num.Text, dr("DEL_DOC_LN#"), dr("itm_cd").ToString, origOrdLn.qty, Session("ORIG_DEL_DOC_NUM"))
                                Else
                                    If origOrdLn.qty < chngOrdLn.qty Then
                                        HBCG_Utils.AddPendingInventory(command, txt_del_doc_num.Text, dr("DEL_DOC_LN#"), dr("itm_cd").ToString, chngOrdLn.qty - origOrdLn.qty, Session("ORIG_DEL_DOC_NUM"))
                                    Else
                                        HBCG_Utils.SubtractInventory(command, txt_del_doc_num.Text, dr("DEL_DOC_LN#"), dr("itm_cd").ToString, origOrdLn.qty - chngOrdLn.qty, Session("ORIG_DEL_DOC_NUM"))
                                    End If
                                End If
                            End If
                        End If  'end if dr.RowState = DataRowState.Modified

                        Calc_SoLn_Points(dr, txt_ord_tp_cd.Text, transPointTots)
                    End If
                Next

                If SysPms.allowMultDisc AndAlso AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") Then
                    ProcessSoLnDiscounts(dr("DEL_DOC_NUM"), command)
                End If

            End If   'end If cbo_stat_cd.SelectedValue <> "Change Order Status" And txt_Stat_cd.Text = "O" Then

            'Alice added for request 294 to add approval comments of price changes on Oct 17, 2019 

            If Session("ApprovalComment1_SOM") & "" <> "" AndAlso Session("Approver_emp_cd_SOM") & "" <> "" Then

                HBCG_Utils.SaveAutoComment(command, txt_del_doc_num.Text, txt_wr_dt.Text, "APPCMNT", Session("ApprovalComment1_SOM"), "", Session("Approver_emp_cd_SOM"), "")

                If Session("ApprovalComment2_SOM") & "" <> "" Then
                    HBCG_Utils.SaveAutoComment(command, txt_del_doc_num.Text, txt_wr_dt.Text, "APPCMNT", Session("ApprovalComment2_SOM"), "", Session("Approver_emp_cd_SOM"), "")
                End If
                If Session("ApprovalComment3_SOM") & "" <> "" Then
                    HBCG_Utils.SaveAutoComment(command, txt_del_doc_num.Text, txt_wr_dt.Text, "APPCMNT", Session("ApprovalComment3_SOM"), "", Session("Approver_emp_cd_SOM"), "")
                End If
                Session("ApprovalComment1_SOM") = ""
                Session("ApprovalComment2_SOM") = ""
                Session("ApprovalComment3_SOM") = ""
                Session("Approver_emp_cd_SOM") = ""
            End If


            If Not Session("SPLIT_SO_LN") Is Nothing Then
                Dim splitLnReq As SplitLineRequest
                Dim splitLnReqList As New ArrayList
                splitLnReqList = Session("SPLIT_SO_LN")
                For Each splitLnReq In splitLnReqList
                    SplitLnRelatedTbls(splitLnReq, command)
                Next
            End If

            If txt_Stat_cd.Text = "O" Then
                Dim origTranSched As New TransportationSchedule
                origTranSched.init()
                origTranSched.zoneCd = lbl_curr_zone.Text
                origTranSched.puDelCd = lbl_PD.Text
                origTranSched.puDelDt = FormatDateTime(lbl_curr_dt.Text, DateFormat.ShortDate)
                Dim currTranSched As New TransportationSchedule
                currTranSched.init()
                currTranSched.zoneCd = lbl_new_zone.Text
                currTranSched.puDelCd = lbl_new_tp.Text
                If lbl_new_dt.Text.isNotEmpty Then
                    currTranSched.puDelDt = FormatDateTime(lbl_new_dt.Text, DateFormat.ShortDate)
                End If
                ProcessTransportationUpdt(transPointTots, txt_ord_tp_cd.Text, origTranSched, currTranSched, command)
            End If
            Save_Comments(orig_dt.Rows(0), command)

            '==============Code added for MM-7767===================
            If SysPms.updateTruckDate And txt_pd_display.Text.Equals("D") Then
                If cbo_PD.Value = "D" And Not String.IsNullOrEmpty(lbl_new_dt.Text.Trim()) Then
                    'so this SO is still (D)elivery and a new date has been selected
                    If FormatDateTime(lbl_curr_dt.Text, DateFormat.ShortDate) <> FormatDateTime(lbl_new_dt.Text, DateFormat.ShortDate) _
                        AndAlso Not String.IsNullOrEmpty(lbl_trk_stop.Text.Trim()) Then

                        Dim trkStop() As String = lbl_trk_stop.Text.Trim().Split("/")

                        If trkStop.Length > 1 Then
                            'Comparing the current date with the updated date, both should not same
                            theTMBiz.UpdateTruckDeliverySystem(command, FormatDateTime(lbl_new_dt.Text, DateFormat.ShortDate),
                                                               trkStop(0), trkStop(1), txt_del_doc_num.Text)
                        End If
                    End If
                ElseIf cbo_PD.Value = "P" And SysPms.isDelvSchedEnabled And
                    Not (txt_ord_tp_cd.Text.Equals("CRM") AndAlso Not SysPms.crmAffectDelAvail) And
                    SysPms.enablePickup Then
                    'so this SO was converted from (D)elivery to (P)ickup
                    theTMBiz.ClearTruckDeliverySystem(command, txt_del_doc_num.Text)
                End If
            End If
            '===============Ended code changes for MM-7767==============

            'saves all the pending changes.
            command.Transaction.Commit()

            'prevent Oracle error Sep 16
            command.Parameters.Clear()


            ' Daniela Update So_ln slps info
            If Session("SLSP_CHANGED") = "Y" Then
                'conn.Open()
                'connAtomic.Open()
                command.Transaction = connAtomic.BeginTransaction()

                sql = "UPDATE SO_LN SET so_emp_slsp_cd1= (select so_emp_slsp_cd1 from so where del_doc_num = :DOC_NUM), " +
                        "so_emp_slsp_cd2=(select so_emp_slsp_cd2 from so where del_doc_num = :DOC_NUM), " +
                        "pct_of_sale1=(select pct_of_sale1 from so where del_doc_num = :DOC_NUM), " +
                        "pct_of_sale2=(select pct_of_sale2 from so where del_doc_num = :DOC_NUM) " +
                        "WHERE del_doc_num = :DOC_NUM"

                command.CommandText = sql
                command.Parameters.Add(":DOC_NUM", OracleType.VarChar)
                command.Parameters(":DOC_NUM").Value = txt_del_doc_num.Text

                command.ExecuteNonQuery()
                command.Transaction.Commit()
                Session("SLSP_CHANGED") = Nothing
            End If
            ' Daniela end
            UnreserveInventory()  'removes from ITM_RES all pending reservations

            ' Updating the record since the sale has been committed successfully
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(Convert.ToString(ViewState("del_doc_num")), Convert.ToString(Session("browserID")), " Sale order has been committed")
            End If

            If Session("lucy_ctl_change") = "Y" Then  ' lucy 01-jun-16
                command.Parameters.Clear()
                command.Transaction = connAtomic.BeginTransaction()
                sql = " Update so " +
                 "Set conf_stat_cd='UNC' " +
                 " where del_doc_num ='" & txt_del_doc_num.Text & "' " +
                 " and conf_stat_cd in('INP','CON') "
                command.CommandText = sql
                command.ExecuteNonQuery()
                command.Transaction.Commit()

                lucy_insert_so_comment()   'lucy

                command.Transaction = connAtomic.BeginTransaction()
                sql = " delete from trk_stop " +
                 " where del_doc_num ='" & txt_del_doc_num.Text & "' "

                command.CommandText = sql
                command.ExecuteNonQuery()
                command.Transaction.Commit()
                command.Parameters.Clear()
            End If
            UpPanel.Visible = False

            Session("ORIG_SO") = System.DBNull.Value
            Session("ORIG_SO") = Nothing
            Session("DEL_DOC_LN#") = System.DBNull.Value
            Session("DEL_DOC_LN#") = Nothing
            Session("SPLIT_SO_LN") = Nothing
            Session("MaxCredList") = Nothing
            Session("STORE_ENABLE_ARS") = False
            Session("IsARSStore") = Nothing
            Session("REPLACED") = Nothing
            Session("REPLACED") = Nothing
            Session("UNRESERVE") = Nothing
            Session("SELECTEDSKUINDEX") = Nothing
            Session("UNRESERVE") = Nothing
            Session("mgrApproval") = Nothing
            Session("hidUpdLnNos") = Nothing
            Session.Remove("SaveSalesOrder")
            Session("lucy_ctl_change") = "N"
            Session("add_new_line") = "N"     'lucy 
            Session("OpenIST") = Nothing ' lucy 11-aug-16 clear out the message Items are linked to an IST
            lbl_warning.Text = Nothing       ' lucy , 11-aug-16, clear out the message Items are linked to an IST
            Dim urlSb As New StringBuilder
            urlSb.Append("SalesOrderMaintenance.aspx?del_doc_num=" & txt_del_doc_num.Text & "&query_returned=Y")
            If SystemUtils.isNotEmpty(Session("referrerToSom").ToString) Then

                urlSb.Append("&referrer=" + Session("referrerToSom"))
            End If
            Session("referrerToSom") = Nothing
            Response.Redirect(urlSb.ToString, False)

        Catch resUnresEx As ResUnresException
            Try
                lbl_desc.Text = resUnresEx.Message
                command.Transaction.Rollback()
                ' TODO - if not in order tab, then do not see the error

            Catch ex2 As Exception
                ' This catch block will handle any errors that may have occurred
                ' on the server that would cause the rollback to fail, such as
                ' a closed connection.
                Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType())
                Console.WriteLine("  Message: {0}", ex2.Message)
            End Try

        Catch oraEx As OracleException
            ' Attempt to roll back the transaction.
            Try
                If oraEx.Code = AppConstants.OraErr.RECORD_LOCKED Then
                    lbl_desc.Text = String.Format(Resources.POSErrors.ERR0042, txt_del_doc_num.Text)
                Else
                    lbl_desc.Text = oraEx.Message
                End If
                command.Transaction.Rollback()
                UpPanel.Visible = False
                Exit Sub

            Catch ex2 As Exception
                ' This catch block will handle any errors that may have occurred
                ' on the server that would cause the rollback to fail, such as
                ' a closed connection.
                Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType())
                Console.WriteLine("  Message: {0}", ex2.Message)
            End Try
            Throw

        Catch ex As Exception
            ' TODO - something is causing the process to go into the Catch ex exception block - but all is already committed - ???
            ' Attempt to roll back the transaction.
            Try
                lbl_desc.Text = ex.Message
                command.Transaction.Rollback()
                UpPanel.Visible = False
                Exit Sub

            Catch ex2 As Exception
                ' This catch block will handle any errors that may have occurred
                ' on the server that would cause the rollback to fail, such as
                ' a closed connection.
                Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType())
                Console.WriteLine("  Message: {0}", ex2.Message)
            End Try
            Throw
        Finally
            command.Dispose()
            conn.Close()
            connAtomic.Close()
        End Try
    End Sub
    Private Function SaveComments() As Boolean
        Dim doCommentsSaved As Boolean = False
        Dim orig_ds As DataSet = Session("ORIG_SO")
        Dim orig_dt As DataTable = orig_ds.Tables(0)

        If String.IsNullOrEmpty(Session("scomments") & "") = False OrElse String.IsNullOrEmpty(Session("dcomments") & "") = False OrElse String.IsNullOrEmpty(Session("arcomments") & "") = False Then
            Using connect As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP), cmd As OracleCommand = DisposablesManager.BuildOracleCommand(connect)
                connect.Open()
                Save_Comments(orig_dt.Rows(0), cmd)
                txt_comments.Text = String.Empty
                txt_del_comments.Text = String.Empty
                txt_ar_comments.Text = String.Empty
                GetComments()
                doCommentsSaved = True
            End Using
        End If

        Return doCommentsSaved
    End Function

    ''' <summary>
    ''' Inserts into SO_LN2DISC table, all discounts that were created for the document passed in
    ''' </summary>
    ''' <param name="delDocNum">the document number the lines belong to</param>
    ''' <param name="dbAtomicCommand">a command to execute the DB processing, it's part of the same transaction  as the save process</param>
    Private Sub ProcessSoLnDiscounts(ByVal delDocNum As String, ByRef dbAtomicCommand As OracleCommand)

        If (Not IsNothing(Session("DISC_APPLIED"))) Then

            Dim dtDisc As DataTable = Session("DISC_APPLIED")
            Dim sbSql As StringBuilder = New StringBuilder()
            Dim delDocLnNum As String = String.Empty
            Dim discPct As Object
            Dim discSeqNum As Integer = 1

            '=== Sorts discounts by the lnSeqNum
            Dim discRows = (From row In dtDisc.AsEnumerable() Order By row.Item("LNSEQ_NUM") Ascending Select row)
            dtDisc = discRows.AsDataView().ToTable()

            '=== Deletes all previous discounts
            sbSql.Append("DELETE FROM SO_LN2DISC WHERE DEL_DOC_NUM = :delDocNum")
            dbAtomicCommand.CommandText = sbSql.ToString()
            dbAtomicCommand.Parameters.Clear()
            dbAtomicCommand.Parameters.AddWithValue(":delDocNum", delDocNum)
            dbAtomicCommand.ExecuteNonQuery()

            '=== Inserts ALL non-deleted discounts
            sbSql.Clear()
            sbSql.Append("INSERT INTO SO_LN2DISC (DEL_DOC_NUM, DEL_DOC_LN#, DISC_CD, DISC_AMT, DISC_PCT, DISC_LEVEL, SEQ_NUM) ")
            sbSql.Append("VALUES(:delDocNum, :delDocLnNum, :discCd, :discAmt, :discPct, :discLevel,  :discSeqNum)")

            dbAtomicCommand.CommandText = sbSql.ToString()
            dbAtomicCommand.Parameters.Clear()
            dbAtomicCommand.Parameters.Add(":delDocNum", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":delDocLnNum", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discCd", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discAmt", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discPct", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discLevel", OracleType.VarChar)
            dbAtomicCommand.Parameters.Add(":discSeqNum", OracleType.VarChar)

            For Each dr As DataRow In dtDisc.Rows
                'first line being processed
                If delDocLnNum.isEmpty Then delDocLnNum = dr.Item("LNSEQ_NUM").ToString

                'second to last line being processed
                If (dr.Item("DISC_IS_DELETED")) Then Continue For 'skips deleted discounts

                If delDocLnNum <> dr.Item("LNSEQ_NUM").ToString Then
                    delDocLnNum = dr.Item("LNSEQ_NUM").ToString
                    discSeqNum = 1     'to start a new sequence number for the next order line
                End If

                discPct = DBNull.Value
                If (Not IsDBNull(dr.Item("DISC_PCT"))) Then discPct = dr.Item("DISC_PCT").ToString

                dbAtomicCommand.Parameters(":delDocNum").Value = delDocNum
                dbAtomicCommand.Parameters(":delDocLnNum").Value = delDocLnNum
                dbAtomicCommand.Parameters(":discCd").Value = dr.Item("DISC_CD").ToString
                dbAtomicCommand.Parameters(":discAmt").Value = dr.Item("DISC_AMT").ToString
                dbAtomicCommand.Parameters(":discPct").Value = discPct
                dbAtomicCommand.Parameters(":discLevel").Value = dr.Item("DISC_LEVEL").ToString
                dbAtomicCommand.Parameters(":discSeqNum").Value = discSeqNum
                dbAtomicCommand.ExecuteNonQuery()

                discSeqNum = discSeqNum + 1
            Next
            dbAtomicCommand.Parameters.Clear()
        End If
    End Sub

    ''' <summary>
    ''' Makes sure that PO links are either created, updated, or removed accordingly
    ''' </summary>
    ''' <param name="dr">the datarow being processed</param>
    ''' <param name="dbAtomicCommand">the command to be used for the processing of PO links</param>
    Private Sub ProcessPOLinks(ByVal dr As DataRow, ByRef dbAtomicCommand As OracleCommand)

        'when PO linking is enabled, needs to make sure that PO links are removed or added accordingly
        If (ConfigurationManager.AppSettings("link_po").ToString.Equals("Y")) Then

            Dim docTpCd As String = If(Not String.IsNullOrEmpty(Session("ord_tp_cd")), Session("ord_tp_cd").ToString(), AppConstants.Order.TYPE_SAL)
            Dim poCd As String = If(IsDBNull(dr("PO_CD")), "", dr("PO_CD").ToString())
            Dim poLn As String = If(IsDBNull(dr("PO_LN")), "", dr("PO_LN").ToString())
            Dim lnVoid As Boolean = ("Y" = dr("VOID_FLAG", DataRowVersion.Current).ToString())
            'when a PO link is removed, the PO_CD is cleared but PO_LN is left to save DB hit to find out if there is link that needs removal
            Dim lnkRemoved As Boolean = (String.IsNullOrEmpty(poCd) AndAlso Not String.IsNullOrEmpty(poLn))
            Dim dtPdTransfer As Date
            If lbl_new_dt.Text.isNotEmpty Then
                dtPdTransfer = FormatDateTime(lbl_new_dt.Text, DateFormat.ShortDate)
            End If

            'if line was voided, finds out if PO links exist to remove them.
            If (lnVoid OrElse lnkRemoved) Then
                theSalesBiz.RemovePOLink(dbAtomicCommand, docTpCd, dr("DEL_DOC_NUM").ToString(), dr("DEL_DOC_LN#").ToString(),
                                         dtPdTransfer, dr("QTY").ToString())

            ElseIf (Not String.IsNullOrEmpty(poCd) AndAlso Not String.IsNullOrEmpty(poLn)) Then

                'at this point when a PO link exist, then it is removed and readded, because the PO link info is retrieved
                'in the bindGrid method, which makes it harder to have an ORIG_PO_CD value to be used for this purpose.
                'This block handles the Update (if a PO-Link was replaced for another one) as well as ADD (new link)
                Dim poBiz As POBiz = New POBiz()
                If poBiz.IsPOAvailable(dbAtomicCommand, poCd, poLn, dr("ITM_CD") & "", dr("Qty") & "") = True Then
                    theSalesBiz.AddPOLink(dbAtomicCommand, docTpCd, dr("DEL_DOC_NUM").ToString(), dr("DEL_DOC_LN#").ToString(),
                                      dtPdTransfer, dr("QTY").ToString(), poCd, poLn, True)
                Else
                    'do not process the PO links if the PO is not available .....
                End If

            End If
        End If
    End Sub


    Protected Sub store_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles final_store_cd.SelectedIndexChanged

        If final_store_cd.SelectedValue = "" Then  ' TODO - there is no "" value so this can never happen - or what don't I get (DSA)
            txt_fv_dt.Enabled = False
            final_store_cd.Enabled = False
        Else
            txt_fv_dt.Enabled = True
            final_store_cd.Enabled = True
        End If

    End Sub

    Private Class TransportationPoints

        Property origPointTot As Double
        Property currPointTot As Double
        Property unschedPointTot As Double
        Property schedPointTot As Double

        Friend Sub Init()
            origPointTot = 0
            currPointTot = 0
            unschedPointTot = 0
            schedPointTot = 0
        End Sub
    End Class

    Private Sub Calc_SoLn_Points(ByRef soLn As DataRow, ByVal trnTp As String, ByRef tranPoints As TransportationPoints)
        ' determines the total points to schedule or unschedule from the specific line whether added, voided or quantity changed
        '   also track the total original points and the total new points in case needed for header changes instead
        ' trnTp is the ord_tp_cd - SAL/CRM/EEX/etc.

        If (txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL OrElse txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM OrElse txt_ord_tp_cd.Text = "EEX" OrElse txt_ord_tp_cd.Text = "RES") AndAlso
           soLn("Delivery_points").ToString.isNotEmpty AndAlso
           IsNumeric(soLn("Delivery_points").ToString) AndAlso
           CDbl(soLn("Delivery_points").ToString) > 0 Then

            If soLn.RowState = DataRowState.Added Then

                tranPoints.schedPointTot = tranPoints.schedPointTot + (CDbl(soLn("QTY", DataRowVersion.Current).ToString) * soLn("delivery_points"))
                tranPoints.currPointTot = tranPoints.currPointTot + (CDbl(soLn("QTY", DataRowVersion.Current).ToString) * soLn("delivery_points"))

            ElseIf soLn("VOID_FLAG", DataRowVersion.Original).ToString = "N" Then

                tranPoints.origPointTot = tranPoints.origPointTot + (CDbl(soLn("QTY", DataRowVersion.Original).ToString) * soLn("delivery_points"))

                If soLn("VOID_FLAG", DataRowVersion.Current).ToString = "Y" Then

                    tranPoints.unschedPointTot = tranPoints.unschedPointTot + (CDbl(soLn("QTY", DataRowVersion.Original).ToString) * soLn("delivery_points"))

                Else
                    tranPoints.currPointTot = tranPoints.currPointTot + CDbl(soLn("QTY", DataRowVersion.Current).ToString) * soLn("delivery_points")

                    If CDbl(soLn("QTY", DataRowVersion.Current)) > CDbl(soLn("QTY", DataRowVersion.Original)) Then

                        tranPoints.schedPointTot = tranPoints.schedPointTot + ((soLn("QTY") - soLn("QTY", DataRowVersion.Original)) * soLn("delivery_points"))

                    ElseIf CDbl(soLn("QTY", DataRowVersion.Original)) > CDbl(soLn("QTY", DataRowVersion.Current)) Then

                        tranPoints.unschedPointTot = tranPoints.unschedPointTot + ((soLn("QTY", DataRowVersion.Original) - soLn("QTY")) * soLn("delivery_points"))
                    End If
                End If
            End If
        End If
    End Sub

    Private Class TransportationSchedule

        Property zoneCd As String
        Property puDelCd As String
        Property puDelDt As Date

        Sub init()
            zoneCd = ""
            puDelCd = ""
            puDelDt = FormatDateTime(SysPms.begOfTime, DateFormat.ShortDate)
        End Sub
    End Class

    Private Sub ProcessTransportationUpdt(ByRef tranLnPoints As TransportationPoints, ByVal trnTp As String, ByVal origTranSched As TransportationSchedule,
                                          ByVal currTranSched As TransportationSchedule, ByRef cmd As OracleCommand)
        ' Update the DEL (or PUM - future) based on the changes that have occurred during the update process
        ' trnTp is the ord_tp_cd - SAL/CRM/EEX/etc.

        If txt_Stat_cd.Text = "O" AndAlso
            (trnTp = AppConstants.Order.TYPE_SAL OrElse trnTp = AppConstants.Order.TYPE_CRM OrElse trnTp = "EEX" OrElse trnTp = "RES") Then

            If cbo_stat_cd.SelectedValue = "V" Then ' header voided then unsched orig total - test for VOID must occur first

                theTMBiz.UnschedPuDel(cmd, origTranSched.zoneCd, origTranSched.puDelCd, origTranSched.puDelDt,
                                      trnTp, IIf(SysPms.isDelvByPoints, tranLnPoints.origPointTot, 1))

            ElseIf (currTranSched.zoneCd.isEmpty OrElse origTranSched.zoneCd.Equals(currTranSched.zoneCd)) AndAlso
                (currTranSched.puDelCd.isEmpty OrElse origTranSched.puDelCd.Equals(currTranSched.puDelCd)) AndAlso
                (currTranSched.puDelDt = FormatDateTime(SysPms.begOfTime, DateFormat.ShortDate) OrElse origTranSched.puDelDt = currTranSched.puDelDt) Then
                ' the curr values stay empty unless there is a change
                ' no header changes, unsched or sched points (only) based on line changes; if stops, then still 1 at same place no matter line changes
                ' must not add the test for Points to above because then the last else would have to have check for header changes before doing the header changes below
                If SysPms.isDelvByPoints Then
                    If tranLnPoints.schedPointTot - tranLnPoints.unschedPointTot > 0 Then
                        ' schedule additional new points
                        theTMBiz.SchedPuDel(cmd, origTranSched.zoneCd, origTranSched.puDelCd, origTranSched.puDelDt,
                                            trnTp, tranLnPoints.schedPointTot - tranLnPoints.unschedPointTot)

                    ElseIf tranLnPoints.schedPointTot - tranLnPoints.unschedPointTot < 0 Then

                        theTMBiz.UnschedPuDel(cmd, origTranSched.zoneCd, origTranSched.puDelCd, origTranSched.puDelDt,
                                              trnTp, tranLnPoints.unschedPointTot - tranLnPoints.schedPointTot)
                    End If
                End If
            Else
                ' header change, unschedule the total original points from orig schedule info and reschedule the total new points on new schedule info (or stops)
                theTMBiz.UnschedPuDel(cmd, origTranSched.zoneCd, origTranSched.puDelCd, origTranSched.puDelDt,
                                      trnTp, IIf(SysPms.isDelvByPoints, tranLnPoints.origPointTot, 1))

                theTMBiz.SchedPuDel(cmd, currTranSched.zoneCd, currTranSched.puDelCd, currTranSched.puDelDt,
                                    trnTp, IIf(SysPms.isDelvByPoints, tranLnPoints.currPointTot, 1))
            End If
        End If
    End Sub

    Private Function updtInventory(ByRef dr As DataRow, ByRef chngOrdLn As OrderLine, ByRef Command As OracleCommand) As Boolean

        Dim orig_store_cd As String = dr("STORE_CD", DataRowVersion.Original).ToString.Trim
        Dim orig_loc_cd As String = dr("LOC_CD", DataRowVersion.Original).ToString.Trim
        Dim curr_store_cd As String = IIf(String.IsNullOrWhiteSpace(dr("STORE_CD").ToString), "", UCase(dr("STORE_CD").ToString.Trim))
        Dim curr_loc_cd As String = IIf(String.IsNullOrWhiteSpace(dr("LOC_CD").ToString), "", UCase(dr("LOC_CD").ToString.Trim))
        Dim soLnUpdt As Boolean = False

        If dr("VOID_FLAG", DataRowVersion.Current).ToString = "Y" AndAlso dr("VOID_FLAG", DataRowVersion.Original).ToString = "N" AndAlso orig_store_cd & "" <> "" Then
            ' put inv back before void
            Credit_Inventory(Command, dr("QTY", DataRowVersion.Original), dr("DEL_DOC_NUM"), dr("ITM_CD"), orig_store_cd, orig_loc_cd, dr("DEL_DOC_LN#"), dr("FIFL_DT").ToString,
                             dr("FIFO_CST").ToString, dr("FIFO_DT").ToString, dr("OOC_QTY"), chngOrdLn, dr("SERIAL_NUM", DataRowVersion.Original).ToString,
                             dr("VOID_FLAG", DataRowVersion.Current).ToString, dr("OUT_ID", DataRowVersion.Original).ToString,
                             dr("OUT_CD", DataRowVersion.Original).ToString)
            soLnUpdt = True

        ElseIf dr("VOID_FLAG", DataRowVersion.Current).ToString = "N" Then ' stop from doing any processing except putting back when voiding

            If curr_store_cd & "" = "" AndAlso orig_store_cd & "" <> "" Then  ' if unreserving

                Credit_Inventory(Command, dr("QTY", DataRowVersion.Original), dr("DEL_DOC_NUM"), dr("ITM_CD"), orig_store_cd, orig_loc_cd, dr("DEL_DOC_LN#"), dr("FIFL_DT").ToString,
                                 dr("FIFO_CST").ToString, dr("FIFO_DT").ToString, dr("OOC_QTY"), chngOrdLn, dr("SERIAL_NUM", DataRowVersion.Original).ToString,
                                 dr("VOID_FLAG", DataRowVersion.Current).ToString, dr("OUT_ID", DataRowVersion.Original).ToString,
                                 dr("OUT_CD", DataRowVersion.Original).ToString)
                soLnUpdt = True

            ElseIf curr_store_cd & "" <> "" AndAlso orig_store_cd & "" = "" Then  ' if reserving

                Debit_Inventory(Command, dr("QTY"), dr("DEL_DOC_NUM"), dr("ITM_CD"), curr_store_cd, curr_loc_cd, dr("DEL_DOC_LN#"), lbl_PD.Text, chngOrdLn, 0,
                                dr("SERIAL_NUM").ToString, dr("OUT_ID").ToString)
                soLnUpdt = True

                ' if changing reservation
                ' either because the Store, Location, Serial#, OutletID changed or 
                ' because there is a reservation where the quantity has changed
            ElseIf curr_store_cd <> orig_store_cd OrElse curr_loc_cd <> orig_loc_cd OrElse
                dr("SERIAL_NUM").ToString <> dr("SERIAL_NUM", DataRowVersion.Original).ToString OrElse
                dr("OUT_ID").ToString <> dr("OUT_ID", DataRowVersion.Original).ToString OrElse
                (curr_store_cd.isNotEmpty() AndAlso curr_loc_cd.isNotEmpty() AndAlso dr("QTY", DataRowVersion.Original) <> dr("QTY")) Then

                If (Credit_Inventory(Command, dr("QTY", DataRowVersion.Original), dr("DEL_DOC_NUM"), dr("ITM_CD"), orig_store_cd, orig_loc_cd, dr("DEL_DOC_LN#"), dr("FIFL_DT").ToString,
                                     dr("FIFO_CST").ToString, dr("FIFO_DT").ToString, dr("OOC_QTY"), chngOrdLn, dr("SERIAL_NUM", DataRowVersion.Original).ToString,
                                     dr("VOID_FLAG", DataRowVersion.Current).ToString, dr("OUT_ID", DataRowVersion.Original).ToString,
                                     dr("OUT_CD", DataRowVersion.Original).ToString)) Then

                    Debit_Inventory(Command, dr("QTY"), dr("DEL_DOC_NUM"), dr("ITM_CD"), curr_store_cd, curr_loc_cd, dr("DEL_DOC_LN#"), lbl_PD.Text, chngOrdLn, 0,
                                    dr("SERIAL_NUM").ToString, dr("OUT_ID").ToString)
                    soLnUpdt = True ' may have to watch this for non-api
                End If
            End If
        End If
        If soLnUpdt Then
            Command.Parameters.Clear()
        End If
        Return soLnUpdt
    End Function

    ''' <summary>
    ''' Fires when the 'Add' button is clicked. This adds the entered sku as a new line
    ''' on the retrieved order.
    ''' </summary>
    Public Sub Add_SKU_To_Order(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("add_new_line") = "Y"  'lucy 29-jun-16
        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          ' lucy 25-may-16
            lbl_final.Text = strErr
            Exit Sub
        End If

        Dim footerIndex As Integer = GridView1.Controls(0).Controls.Count - 1
        Dim txt_new_itm_cd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_itm_cd"), TextBox)
        Dim txt_new_store_cd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_store_cd"), TextBox)
        Dim txt_new_loc_cd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_loc_cd"), TextBox)
        Dim txt_new_unit_prc As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_unit_prc"), TextBox)
        Dim txt_new_qty As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_qty"), TextBox)

        SetupForDiscReCalc()

        Dim items() As String = UCase(txt_new_itm_cd.Text).Trim(" ", ",").Split(",").[Select](Function(g) g.Trim()).ToArray()
        'Distint the entry by removing the duplicates
        Dim enteredSKUs As String = String.Join(",", items.AsEnumerable().Distinct())

        lbl_desc.Text = ""
        ' Daniela add multi comp logic
        If txt_new_itm_cd.Text & "" <> "" Then
            txt_new_itm_cd.Text = txt_new_itm_cd.Text.Trim
            Dim usr_flag = LeonsBiz.sec_check_itm(UCase(txt_new_itm_cd.Text), Session("emp_cd"))
            If usr_flag = "N" Then  'multi co
                txt_new_itm_cd.Text = ""
                lbl_desc.Text = "You are not authorized to select that SKU"
                Exit Sub
            End If
        End If

        ' Daniela add Picture URL
        Dim image_URL As String = ""
        Try
            'Dim img_picture As System.Web.UI.WebControls.Image = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("img_picture"), System.Web.UI.WebControls.Image)
            Dim imgZipCd As String = LeonsBiz.GetPostalCode()
            Dim imgLangCode As String = LeonsBiz.GetLangCode()
            image_URL = LeonsBiz.Find_Merchandise_Images(txt_new_itm_cd.Text, imgZipCd, imgLangCode)
            If isEmpty(image_URL) Then
                image_URL = "~/images/image_not_found.jpg"
            End If
        Catch ' Do nothing if picture not found or error
        End Try
        ' End Daniela

        Dim showWarrPopup As Boolean = False

        Dim INSRTED As Boolean = AddLine(enteredSKUs, txt_new_qty.Text, txt_new_unit_prc.Text, txt_new_store_cd.Text, txt_new_loc_cd.Text, image_URL, showWarrPopup)

        If INSRTED Then
            If showWarrPopup Then
                ' Daniela June 15 fix Rel SKU not showing
                ShowWarrantyPopup(String.Empty)
            Else
                If Not String.IsNullOrEmpty(hidEnteredSKUs.Value) Then
                    Dim relatedSku As String = hidEnteredSKUs.Value
                    hidEnteredSKUs.Value = String.Empty

                    ShowRelatedSkuPopUp(relatedSku)
                End If
            End If

            DoDiscReCalcNowOrLater()
            bindgrid()
            doAfterBindgrid()
            calculate_total()
        End If

        'reapplies the discounts (if any) when applicable
    End Sub

    ''' <summary>
    ''' Fires when the 'hpl_find_merch' hyperlink is loaded. Navigation url will be assigned.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub FindMerchandise(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim footerIndex As Integer = GridView1.Controls(0).Controls.Count - 1
        Dim hplSKULookup As HyperLink = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("hpl_find_merch"), HyperLink)
        If Not IsNothing(hplSKULookup) Then
            hplSKULookup.NavigateUrl = "order.aspx?referrer=SalesOrderMaintenance.aspx&del_doc_num=" & Request("del_doc_num") & "&query_returned=" & Request("query_returned")
        End If
    End Sub

    ''' <summary>    ''' Fetches the max line number from the session line dataset and sets in a session variable; 
    ''' initially used by discounting recalculation processing so only done if there are applied discounts and a header discount is included
    ''' </summary>
    Protected Sub SetupForDiscReCalc()

        'Prepares data for later use in discount recalculation (if applicable)
        SessVar.LastLnSeqNum = String.Empty
        If AppConstants.Order.TYPE_SAL = SessVar.ordTpCd AndAlso (Not IsNothing(Session("DISC_APPLIED"))) AndAlso
            (Not IsNothing(Session("DEL_DOC_LN#"))) Then

            Dim hdrDiscCd As String = theSalesBiz.GetHeaderDiscountCode(Session("DISC_APPLIED"), False)
            If hdrDiscCd.isNotEmpty() Then

                Dim soLns As DataSet = Session("DEL_DOC_LN#")
                Dim maxLnNum As Integer = soLns.Tables(0).Compute("MAX([DEL_DOC_LN#])", String.Empty)
                'fetches last lineSeq, to recalculate (if applicable) due to any of the new lines
                SessVar.LastLnSeqNum = maxLnNum
            End If
        End If

    End Sub

    ''' <summary>
    ''' Determines if a warranty may be added later and if so, postpones re-applying the header discount(s); otherwise
    ''' re-applies the discounts here 
    ''' </summary>
    Protected Sub DoDiscReCalcNowOrLater()
        ' if we might add more records from warr or related, then re-calc discount later
        If SessVar.discReCalc.maybeWarr = True Then
            SessVar.discReCalc.needToReCalc = True
        Else
            'The lastSeqNum would have been populated above if found that a HEADER discount exists, 
            'if will reapply it, if ANY of the lines added qualify for the discount.
            If SessVar.LastLnSeqNum.isNotEmpty() Then
                Dim soLnDatSet As DataSet = Session("DEL_DOC_LN#")
                Dim soLnTable As DataTable = soLnDatSet.Tables(0)

                ReapplyDiscounts(soLnTable, ReapplyDiscountRequestDtc.LnChng.Add, SessVar.LastLnSeqNum)
                Session("DEL_DOC_LN#") = soLnDatSet
            End If
        End If  ' TODO - maybe we shouldn't bindgrid yet either
    End Sub

    ''' <summary>
    ''' Processes the adding of a line when it is a CRM/MCR document with an original order. It will validate
    ''' against the original order being referenced for qty, price etc. at the time the line is being added. 
    ''' </summary>
    ''' <param name="maxCrmQty">the max qty allowed</param>
    ''' <param name="maxRetPrc">the max price alllowed</param>
    ''' <param name="origSoLnNum">the original order referenced by the CRM/MCR</param>
    ''' <remarks></remarks>
    Private Sub ProcessCreditLine(ByRef maxCrmQty As Double, ByRef maxRetPrc As Double, ByRef origSoLnNum As Integer)

        Dim footerIndex As Integer = GridView1.Controls(0).Controls.Count - 1
        Dim txt_new_itm_cd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_itm_cd"), TextBox)
        Dim txt_new_store_cd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_store_cd"), TextBox)
        Dim txt_new_loc_cd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_loc_cd"), TextBox)
        Dim txt_new_unit_prc As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_unit_prc"), TextBox)
        Dim txt_new_qty As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_qty"), TextBox)

        Dim tmpExtRet As Double = 0
        Dim maxExtRet As Double = 0
        ' HAVE TO LOOP THRU EXISTING ON CRM/MCR to see if have any credit lines still available to add 
        If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then

            Dim addOK As Boolean = False
            lbl_desc.Text = ""
            Dim soLnDs As DataSet
            Dim soLn As DataRow
            Dim maxRetQtyList As DataSet
            Dim origSoLn As DataRow
            Dim maxCred As New OrderUtils.MaxCredResponse

            If IsNothing(Session("MaxCredList")) Then
                maxRetQtyList = OrderUtils.getMaxCreditListByLn(Session("ORIG_DEL_DOC_NUM"), txt_del_doc_num.Text)
            Else
                maxRetQtyList = Session("MaxCredList")
            End If
            If SystemUtils.dataSetHasRows(maxRetQtyList) AndAlso SystemUtils.dataRowIsNotEmpty(maxRetQtyList.Tables(0).Rows(0)) Then

                soLnDs = Session("DEL_DOC_LN#")

                ' TODO - after first round of testing - 7 lines on SAL, 3 on CRM - seems like maybe should turn around - SAL will always be max number of lines so might limit by sale line match first and not even go there; 
                '      switch sale lines to outside loop for a bit of potential perf improvement
                For Each origSoLn In maxRetQtyList.Tables(0).Rows

                    ' if CRM and same SKU and avail cred and this sale line not already on this doc
                    ' TODO - probably this should allow multiple lines per line so can have diff return credits - not now
                    ' TODO - cannot void and add because maxRetQtyList not refreshed - cannot refresh against db, would have to mod delete process to add to avail (subtract cred) - >
                    ' may be better to allow multiple per line so easier to update  or maybe not???
                    If origSoLn("itm_cd") = txt_new_itm_cd.Text AndAlso
                            origSoLn("thisCredTot").ToString < 0.0001 AndAlso
                            (AppConstants.Order.TYPE_MCR.Equals(Session("ord_tp_cd")) OrElse
                                (AppConstants.Order.TYPE_CRM.Equals(Session("ord_tp_cd")) AndAlso
                                origSoLn("salQty") - origSoLn("crmQty") > 0.0001)) Then
                        '  origSoLn("salTot") - origSoLn("credTot") > 0.0001 Then
                        ' there is a matching SKU line from orig sale that is not on this CRM prior to this transaction and has remaining qty (credit doesn't matter on CRM)
                        addOK = True
                        ' now confirm we haven't added to this order and not saved yet
                        For Each soLn In soLnDs.Tables(0).Rows

                            If (Not "Y".Equals(soLn("void_flag").ToString)) AndAlso origSoLn("lnNum") = soLn("orig_so_ln_num") Then
                                addOK = False
                                Exit For
                            End If
                        Next
                    End If
                    ' if we found an unused line then we can stop looking, now enforce the maximum qty and retail
                    If addOK Then

                        maxCred = OrderUtils.getLnMaxCredit(origSoLn("itm_cd"), origSoLn("lnNum"), origSoLn("salUnitPrc"), Session("ord_tp_cd"), maxRetQtyList)
                        maxCrmQty = maxCred.maxQty   'origSoLn("salQty") - origSoLn("crmQty")
                        maxRetPrc = maxCred.maxUnitRetail  'origSoLn("salTot") - origSoLn("credTot")
                        origSoLnNum = origSoLn("lnNum")

                        If AppConstants.Order.TYPE_CRM.Equals(Session("ord_tp_cd")) Then
                            If IsNumeric(txt_new_qty.Text) AndAlso CDbl(txt_new_qty.Text) > maxCrmQty Then
                                lbl_desc.Text = "Quantity " + txt_new_qty.Text + " entered for SKU " + txt_new_itm_cd.Text +
                                    " is greater than available quantity to return from original document; quantity changed to maximum."
                                txt_new_qty.Text = maxCrmQty
                                lbl_desc.Visible = True
                                addOK = False
                            End If
                            If IsNumeric(txt_new_unit_prc.Text) AndAlso CDbl(txt_new_unit_prc.Text) > maxRetPrc Then
                                If Not addOK Then
                                    lbl_desc.Text = lbl_desc.Text + "; "
                                End If
                                lbl_desc.Text = lbl_desc.Text & "Credit " + FormatCurrency(txt_new_unit_prc.Text, 2) + " entered for SKU " + txt_new_itm_cd.Text +
                                " exceeds the available credit for this line from the original document; credit changed to maximum."
                                txt_new_unit_prc.Text = FormatNumber(maxRetPrc, 2)
                                lbl_desc.Visible = True
                                addOK = False
                            End If

                        Else ' is MCR ' calc and enforce max extended  
                            maxExtRet = Math.Round(maxCrmQty * maxRetPrc, SystemUtils.CurrencyPrecision)
                            If txt_new_qty.Text = 0 Then
                                txt_new_qty.Text = 1
                            End If
                            If txt_new_unit_prc.Text = 0 Then
                                txt_new_unit_prc.Text = maxRetPrc
                            End If
                            If IsNumeric(txt_new_qty.Text) AndAlso IsNumeric(txt_new_unit_prc.Text) Then
                                tmpExtRet = Math.Round(CDbl(txt_new_qty.Text) * CDbl(txt_new_unit_prc.Text), SystemUtils.CurrencyPrecision)

                            ElseIf IsNumeric(txt_new_qty.Text) Then
                                tmpExtRet = Math.Round(CDbl(txt_new_qty.Text) * maxRetPrc, SystemUtils.CurrencyPrecision)

                            ElseIf IsNumeric(txt_new_unit_prc.Text) Then
                                tmpExtRet = Math.Round(CDbl(txt_new_unit_prc.Text) * maxCrmQty, SystemUtils.CurrencyPrecision)
                            End If
                            If tmpExtRet > maxExtRet Then

                                lbl_desc.Text = "Maximum credit available for SKU " + txt_new_itm_cd.Text + " is " + FormatNumber(maxExtRet, 2) + "; values changed to maximums."
                                txt_new_unit_prc.Text = FormatNumber(maxRetPrc, 2)
                                txt_new_qty.Text = maxCrmQty
                                lbl_desc.Visible = True
                                addOK = False
                                Exit Sub
                            End If
                        End If

                        Exit For
                    End If
                Next
            End If

            ' addOK is false for no orig lines to cred doc, none of that item code on orig, no avail qty on CRM line or line already on this doc, either in db or in this transaction
            If Not addOK Then
                If lbl_desc.Text.Length < 0.00001 Then
                    lbl_desc.Text = "SKU " + txt_new_itm_cd.Text + " not on original sale or all original sale lines have been returned in this or previous returns. Unable to return this SKU. "
                    txt_new_itm_cd.Text = ""
                    txt_new_qty.Text = ""
                    txt_new_unit_prc.Text = ""
                    txt_new_store_cd.Text = ""
                    txt_new_loc_cd.Text = ""
                End If
                Exit Sub
            End If
        End If
    End Sub


    ''' <summary>
    ''' Checks if the CRM lines being finalized have warranty links attached on the original order it references.
    ''' If the qty being returned is the last unit for the original sale line, then only will the link on the original
    ''' line will be removed. If the line is a warranty, then all lines attached to that warranty are removed. Otherwise,
    ''' only the link for that particular line is removed. 
    ''' </summary>
    ''' <param name="cmd">the command to use for executing the sql</param>
    Private Sub UnlinkFinalizedCrmLines(ByRef cmd As OracleCommand)

        If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then

            Dim soLnDs As DataSet
            Dim crmLn As DataRow
            Dim maxRetQtyList As DataSet

            maxRetQtyList = OrderUtils.getMaxCreditListByLn(Session("ORIG_DEL_DOC_NUM"), txt_del_doc_num.Text, True)
            If SystemUtils.dataSetHasRows(maxRetQtyList) AndAlso SystemUtils.dataRowIsNotEmpty(maxRetQtyList.Tables(0).Rows(0)) Then

                Dim availableQty As Double = 0
                Dim finalizedQty As Double = -1

                soLnDs = Session("DEL_DOC_LN#")
                For Each origSoLn In maxRetQtyList.Tables(0).Rows

                    ' now for each line on the orig order, see which ones are being returned/finalized on the current CRM order
                    For Each crmLn In soLnDs.Tables(0).Rows
                        '6886 -if the original SO line number (orig_so_ln_num) is empty,then we are assigning del_doc_ln# value of original order of particular line item  to orig_so_ln_num
                        Dim solnnum As String = If(IsDBNull(crmLn("orig_so_ln_num")), String.Empty, crmLn("orig_so_ln_num").ToString)
                        If (Not "Y".Equals(crmLn("void_flag").ToString)) AndAlso
                            CDbl(crmLn("qty")) > 0 AndAlso
                            origSoLn("lnNum") = If(solnnum.ToString.isEmpty, crmLn("DEL_DOC_LN#"), solnnum) AndAlso
                            origSoLn("itm_cd") = crmLn("itm_cd") Then

                            '** Get the original sale qty on the orig order and subtract any existing, finalized return qtys. That will give us how many
                            '** 'available' qtys are left to return. Check if there is still any qty left after subtracting the qty of the line being 
                            '** finalized right now. If there is, it means it's not the last qty being returned and finalized. So no need to unlink                         '
                            '** the warranty/warrantable.   
                            availableQty = origSoLn("salQty") - origSoLn("crmQty")
                            finalizedQty = availableQty - CDbl(crmLn("qty"))

                            If finalizedQty = 0 Then

                                'means that this is the last qty being returned and finalized. Check if this item is a warrantable or warranty
                                If ("Y".Equals(crmLn("WARRANTABLE"))) Then
                                    'UnLinkOriginalSaleLine(crmLn("ORIG_SO_LN_NUM").ToString, Session("ORIG_DEL_DOC_NUM"), False, cmd)
                                    UnLinkOriginalSaleLine(If(solnnum.ToString.isEmpty, crmLn("DEL_DOC_LN#"), solnnum), Session("ORIG_DEL_DOC_NUM"), False, cmd)
                                Else
                                    If (SkuUtils.ITM_TP_WAR.Equals(crmLn("ITM_TP_CD"))) Then
                                        'UnLinkOriginalSaleLine(crmLn("ORIG_SO_LN_NUM").ToString, Session("ORIG_DEL_DOC_NUM"), True, cmd)
                                        UnLinkOriginalSaleLine(If(solnnum.ToString.isEmpty, crmLn("DEL_DOC_LN#"), solnnum), Session("ORIG_DEL_DOC_NUM"), True, cmd)
                                    End If
                                End If
                                Exit For
                            End If
                        End If
                    Next

                Next
            End If
        End If

    End Sub

    ''' <summary>
    ''' Unlinks the original sale line by deleting the warranty link records associated to it. This is when a
    ''' CRM line that references this original line is being returned and finalized and therfore, the links have
    ''' to be broken. The line being returned and finalized could be a warranty or warrantable.
    ''' </summary>
    ''' <param name="origLineNum">the line number of the original line being referenced by the CRM line</param>
    ''' <param name="origDelDocNum">the original order being referenced by the CRM line </param>
    ''' <param name="isWarranty">flag indicating if the CRM line being processed is a warranty or not. </param>
    ''' <param name="cmd">the command object to use for executing the sql</param>
    Private Sub UnLinkOriginalSaleLine(ByVal origLineNum As String, ByVal origDelDocNum As String,
                                       ByVal isWarranty As Boolean, ByRef cmd As OracleCommand)

        Dim sql As New StringBuilder
        Try
            cmd.Parameters.Clear()
            cmd.CommandType = CommandType.Text

            If (origDelDocNum.isNotEmpty) Then

                sql.Append("DELETE FROM so_ln2warr ")
                sql.Append("WHERE del_doc_num = :DEL_DOC_NUM ")

                If (isWarranty) Then
                    sql.Append("AND warr_del_doc_ln# = :LN_NUM ")
                Else
                    sql.Append("AND del_doc_ln# = :LN_NUM ")
                End If

                cmd.CommandText = sql.ToString
                cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                cmd.Parameters(":DEL_DOC_NUM").Value = origDelDocNum
                cmd.Parameters.Add(":LN_NUM", OracleType.Number)
                cmd.Parameters(":LN_NUM").Value = origLineNum
                cmd.ExecuteNonQuery()
            End If
        Catch oex As OracleException
            Throw New Exception("Warranties for Order " + origDelDocNum + "and line#:" + origLineNum + " could not be deleted. Please try again.")
        End Try
    End Sub
    ''' Daniela Adds newItmUrl to the current grid and dataset. It retrieves and sets any related data
    ''' Add url to session
    ''' <summary>
    ''' Adds a new line to the current grid and dataset. It retrieves and sets any related data
    ''' needed for creating and saving a line.
    ''' </summary>
    ''' <param name="enteredItemCd">t</param>
    ''' <param name="newQty"></param>
    ''' <param name="newUnitPrice"></param>
    ''' <param name="newStoreCd"></param>
    ''' <param name="newLocCd"></param>

    Private Function AddLine(ByVal enteredItemCd As String,
                   ByVal newQty As String,
                   ByVal newUnitPrice As String,
                   ByVal newStoreCd As String,
                   ByVal newLocCd As String,
                   ByVal newItmUrl As String,
                   Optional ByRef showwarrpopup As Boolean = False,
                   Optional ByVal processCrms As Boolean = True) As Boolean

        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'When user adds a new line to the sale inside the grid
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Adding a new line on a SALE ")
            End If
        End If


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader
        Dim del_doc_num As String
        Dim ds As New DataSet
        Dim ordLns As DataTable
        Dim dr As DataRow
        Dim curr_ln As Integer = 0

        If Not IsNumeric(newQty) Then newQty = "0"
        If Not IsNumeric(newUnitPrice) Then newUnitPrice = "0.00"

        del_doc_num = Trim(txt_del_doc_num.Text)
        enteredItemCd = Trim(enteredItemCd)

        If enteredItemCd & "" = "" Then
            lbl_desc.Text = "Please type a valid SKU"
            Return False
            Exit Function
        End If

        Dim dsItm As DataSet = New DataSet()
        Dim itms As ArrayList = New ArrayList()
        Dim maxCrmQty As Double = 0
        Dim maxRetPrc As Double = 0
        Dim origSoLnNum As Integer = 0

        For Each newItemCd As String In enteredItemCd.Split(",")
            newItemCd = newItemCd.Trim()
            'Do not allow SKUs that have a prevent sale flag on them
            If AppConstants.Order.TYPE_SAL = txt_ord_tp_cd.Text AndAlso Not theSkuBiz.IsSellable(newItemCd) Then
                'lbl_desc.Text = "The SKU " & newItemCd & " you have selected is no longer available for sale"
                lbl_desc.Text = String.Format(Resources.POSMessages.MSG0018, UCase(newItemCd))
                Return False
                Exit Function
            End If

            '***** if it's a crm/mcr doc with orig order, process it separately to check max. qty, price, etc.
            maxCrmQty = 0
            maxRetPrc = 0
            origSoLnNum = 0

            If processCrms Then
                ProcessCreditLine(maxCrmQty, maxRetPrc, origSoLnNum)
            End If

            If IsNumeric(newQty) Then
                If newQty < 1 Then
                    newQty = 1
                End If
            Else
                newQty = 1
            End If

            curr_ln = 0

            If Not Session("DEL_DOC_LN#") Is Nothing Then
                ds = Session("DEL_DOC_LN#")
                ordLns = ds.Tables(0)
            End If

            For Each dr In ds.Tables(0).Rows
                If IsNumeric(dr("DEL_DOC_LN#")) Then
                    If dr("DEL_DOC_LN#") > curr_ln Then
                        curr_ln = dr("DEL_DOC_LN#")
                    End If
                End If
            Next

            conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()
            If curr_ln < 1 Then
                curr_ln = 1
                sql = "SELECT DEL_DOC_LN# FROM SO_LN WHERE SO_DOC_NUM='" & Left(lbl_so_doc_num.Text, 11) &
                        "' ORDER BY DEL_DOC_LN# DESC"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, conn)
                Try
                    MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                    If MyDataReader2.Read() Then
                        If IsNumeric(MyDataReader2.Item("DEL_DOC_LN#").ToString) Then
                            curr_ln = CInt((MyDataReader2.Item("DEL_DOC_LN#").ToString))
                        End If
                    End If
                Catch
                    conn.Close()
                    Throw
                End Try
                MyDataReader2.Close()
            End If

            itms.Add(newItemCd)
        Next

        dsItm = theSkuBiz.GetItemDetailsWithSortCodesForSOM(itms, txt_store_cd.Text)

        If dsItm.Tables(0).Rows.Count < itms.Count Then
            Dim duplicates = itms.ToArray().GroupBy(Function(p) p).Where(Function(g) g.Count() > 1).[Select](Function(g) g.Key)
            For Each dupStr In duplicates
                Dim dupCnt = itms.ToArray().Where(Function(c) c = dupStr).ToArray().Length
                If dupCnt > 1 Then
                    For a = 0 To dupCnt - 2
                        Dim result() As DataRow = dsItm.Tables(0).Select("ITM_CD = '" + dupStr + "'")
                        dsItm.Tables(0).ImportRow(result(0))
                    Next
                End If
            Next
        End If

        If Not (dsItm.Tables.Count > 0 AndAlso dsItm.Tables(0).Rows.Count = itms.Count) Then
            lbl_desc.Text = String.Format(Resources.POSMessages.MSG0024)
        End If

        Dim PKG_SKUS As String = ""
        Dim PKG_LN As Double = 0
        Dim ret_prc As Double = 0

        Dim focusWarr As String = String.Empty

        curr_ln = curr_ln + 1

        Try
            For Each drNew As DataRow In dsItm.Tables(0).Rows
                If Not Session("DEL_DOC_LN#") Is Nothing Then

                    ret_prc = CDbl(drNew("RET_PRC").ToString)
                    ret_prc = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                         txt_store_cd.Text,
                                                         String.Empty,
                                                         FormatDateTime(Today.Date, DateFormat.ShortDate),
                                                         drNew("ITM_CD").ToString,
                                                         ret_prc)

                    If CDbl(newUnitPrice) = CDbl(0) Then
                        If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then
                            newUnitPrice = maxRetPrc
                        Else
                            newUnitPrice = ret_prc
                        End If
                    End If

                    ' MM-6869, Warranty popup issue(MM-6862) fix, isPrcOverrideRequried flag indicates if manager approval popup is requried to show or not
                    Dim isPrcOverrideRequried = False

                    '-------------------MM-6862---------------
                    If newUnitPrice > 0 AndAlso CDbl(newUnitPrice) <> CDbl(ret_prc) Then
                        Dim footerIndex As Integer = GridView1.Controls(0).Controls.Count - 1
                        Dim maxDiscPct As Decimal = 0, enteredRetPrice As Decimal = 0
                        'Validate the max discuont price
                        If Not String.IsNullOrEmpty(drNew("MAX_DISC_PCNT").ToString) Then
                            If Decimal.TryParse(drNew("MAX_DISC_PCNT").ToString, maxDiscPct) AndAlso Decimal.TryParse(newUnitPrice, enteredRetPrice) Then
                                Dim maxDiscAmt As Decimal = ret_prc - (ret_prc * (maxDiscPct / 100))
                                maxDiscAmt = Math.Round(maxDiscAmt, 2)  'Alice added on June 6, 2019, the issue was found during test 4168
                                If maxDiscAmt > enteredRetPrice Then
                                    lbl_desc.Text = String.Format(Resources.POSErrors.ERR0019, FormatCurrency(maxDiscAmt, 2))
                                    lbl_desc.Visible = True
                                    Dim txt_new_unit_prc As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_unit_prc"), TextBox)
                                    txt_new_unit_prc.Text = FormatNumber(ret_prc)
                                    txt_new_unit_prc.Focus()
                                    Return False
                                End If
                            End If
                        End If

                        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
                        Dim promptForApproval As Boolean = AppConstants.ManagerOverride.BY_LINE = prcOverride OrElse AppConstants.ManagerOverride.BY_ORDER = prcOverride
                        If AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso promptForApproval Then
                            'If order level approval is set
                            If (AppConstants.ManagerOverride.BY_ORDER = prcOverride) Then
                                'save the line number that needs approval
                                If String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
                                    hidUpdLnNos.Value = footerIndex
                                Else
                                    hidUpdLnNos.Value = hidUpdLnNos.Value & ";" & footerIndex
                                End If
                            Else
                                'if line level approval is requred.
                                'save the line number that is added
                                Session("SelectedRow") = footerIndex - 1 & ":" & footerIndex
                                Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = ucMgrOverride.FindControl("txtMgrPwd")
                                If txtMgrPwd IsNot Nothing Then
                                    txtMgrPwd.Focus()
                                End If
                                popupMgrOverride.ShowOnPageLoad = True
                                ' MM-6869, Warranty popup issue(MM-6862) fix, isPrcOverrideRequried flag indicates if manager approval popup is requried to show or not
                                isPrcOverrideRequried = True
                            End If
                        End If
                    End If
                    'get the line number form session after the warrientie sku are selected.
                    If Not String.IsNullOrEmpty(Session("hidUpdLnNos")) Then
                        hidUpdLnNos.Value = Session("hidUpdLnNos")
                        Session("hidUpdLnNos") = String.Empty
                    End If
                    '-----------------END--MM-6862---------------
                    dr = ordLns.NewRow
                    dr("ITM_URL") = newItmUrl ' Daniela added
                    dr("DEL_DOC_NUM") = del_doc_num
                    dr("DEL_DOC_LN#") = curr_ln
                    dr("ITM_CD") = drNew("ITM_CD").ToString
                    dr("COMM_CD") = drNew("COMM_CD").ToString
                    dr("QTY") = newQty
                    dr("VE_CD") = drNew("VE_CD").ToString
                    dr("ITM_TP_CD") = drNew("ITM_TP_CD").ToString
                    dr("SERIAL_TP") = drNew("SERIAL_TP").ToString
                    dr("INVENTORY") = drNew("inventory").ToString
                    dr("WARRANTABLE") = drNew("WARRANTABLE").ToString
                    'Added for the  purpose of discount
                    If (Not IsDBNull(drNew("MAX_DISC_PCNT"))) Then dr("MAX_DISC_PCNT") = CDbl(drNew("MAX_DISC_PCNT").ToString)
                    dr("MNR_CD") = If(IsDBNull(drNew("MNR_CD")), "", drNew("MNR_CD").ToString)
                    dr("REPL_CST") = If(IsNumeric(drNew("REPL_CST")), drNew("REPL_CST"), 0)
                    dr("SHU") = "N"
                    dr("WARR_DAYS") = CDbl(drNew("WARR_DAYS").ToString)
                    dr("TYPECODE") = drNew("ITM_TP_CD").ToString
                    ' MM-5762
                    ' When a new line has been added to the CRM then need to assign the below column IsStoreFromInvXref to FALSE
                    dr("IsStoreFromInvXref") = False

                    If drNew("point_size").ToString.isNotEmpty AndAlso
                        IsNumeric(drNew("point_size").ToString()) AndAlso
                        CDbl(drNew("point_size")) > 0 Then
                        dr("delivery_points") = CDbl(drNew("point_size").ToString)
                    End If
                    If drNew("ITM_TP_CD").ToString = "PKG" And "Y".Equals(ConfigurationManager.AppSettings("package_breakout").ToString + "") Then
                        dr("UNIT_PRC") = 0
                        dr("ORIG_PRC") = 0
                        dr("MANUAL_PRC") = 0
                        dr("itm_ret_prc") = 0
                    Else
                        dr("UNIT_PRC") = newUnitPrice
                        'MM-6862
                        dr("ORIG_PRC") = ret_prc
                        dr("MANUAL_PRC") = newUnitPrice
                        dr("itm_ret_prc") = ret_prc
                    End If

                    If drNew("ITM_TP_CD").ToString = "PKG" Then
                        PKG_SKUS = PKG_SKUS & "'" & drNew("ITM_CD").ToString & "',"
                        PKG_LN = curr_ln
                    ElseIf drNew("ITM_TP_CD").ToString = AppConstants.Sku.TP_INV Then
                        hidEnteredSKUs.Value = hidEnteredSKUs.Value & "," & drNew("ITM_CD")
                    End If

                    dr("VOID_FLAG") = "N"
                    dr("VSN") = drNew("VSN").ToString
                    dr("DES") = drNew("DES").ToString
                    dr("NEW_LINE") = "Y"

                    If drNew("inventory").ToString = "Y" Then
                        dr("PICKED") = "Z"
                    Else
                        dr("PICKED") = "X"
                    End If

                    dr("VOID_FLAG") = "N"

                    If IsNumeric(drNew("SPIFF")) Then
                        If CDbl(drNew("SPIFF")) > 0 Then
                            dr("SPIFF") = drNew("SPIFF")
                        End If
                    End If

                    dr("DROP_CD") = drNew("DROP_CD")
                    dr("DROP_DT") = drNew("DROP_DT")

                    'for a new line in mgmt, default salespersons from the order header values 
                    dr("LN_SLSP1") = lbl_hdr_slsp1_cd.Text
                    dr("LN_SLSP1_PCT") = txt_comm1.Text
                    dr("LN_SLSP2") = lbl_hdr_slsp2_cd.Text
                    dr("LN_SLSP2_PCT") = txt_comm2.Text
                    If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then
                        dr("MAX_CRM_QTY") = maxCrmQty
                        dr("MAX_RET_PRC") = maxRetPrc
                        dr("ORIG_SO_LN_NUM") = origSoLnNum
                    End If
                    dr("SORTCODES") = drNew("SORTCODE").ToString()
                    ordLns.Rows.Add(dr)
                    curr_ln = curr_ln + 1
                End If
                newUnitPrice = 0

                If PKG_SKUS & "" <> "" Then
                    Dim addPkgCmpt As New CreatePkgCmpntLnReq
                    addPkgCmpt.pkgSku = PKG_SKUS
                    'addPkgCmpt.nextLnNum = ds.Tables(0).Rows.Count + 2
                    'This is because of if we add more than 2 pkg and void one PKG, and save changes. When user try to one more PKG and try to save there was no sequence DEL_DOC_LN#.
                    'this change will gives sequence DEL_DOC_LN#  even if user void any component or PKG its self.
                    addPkgCmpt.nextLnNum = curr_ln
                    addPkgCmpt.pkgLnNum = PKG_LN
                    addPkgCmpt.slsp1 = lbl_hdr_slsp1_cd.Text
                    addPkgCmpt.slsp2 = lbl_hdr_slsp2_cd.Text
                    addPkgCmpt.pctOfSale1 = txt_comm1.Text
                    addPkgCmpt.pctOfSale2 = txt_comm2.Text
                    Dim errorMessage As String = String.Empty
                    ' MM-6305 
                    ' When there are no components for the Package SKU then an exception will occur  
                    ' Package Parent item needs to be removed from the TEMP_ITM
                    Dim isWarrantableComps As Boolean = AddPackages(addPkgCmpt, errorMessage)
                    If Not showwarrpopup AndAlso isWarrantableComps Then showwarrpopup = isWarrantableComps

                    If Not (String.IsNullOrWhiteSpace(errorMessage)) Then
                        ' MM-6305
                        ' Need to remove the last entered Package SKU from the dataset
                        lbl_desc.Text = errorMessage
                        Dim count As Integer = ds.Tables(0).Rows.Count
                        ds.Tables(0).Rows(count - 1).Delete()
                        ds.AcceptChanges()
                    Else
                        lbl_desc.Text = String.Empty
                    End If
                    PKG_SKUS = ""
                End If

                If Not showwarrpopup Then
                    showwarrpopup = "Y".Equals(dr("INVENTORY").ToString) _
                                        AndAlso "Y".Equals(dr("WARRANTABLE").ToString) AndAlso SalesUtils.hasWarranty(dr("ITM_CD"))
                End If
            Next
            'Need to show he related SKU popup if the warranty popup is not shown.
            If Not showwarrpopup Then
                'Show the related SKU popup
                Dim relatedSku As String = hidEnteredSKUs.Value
                hidEnteredSKUs.Value = String.Empty
                ShowRelatedSkuPopUp(relatedSku)
            End If

        Catch ex As Exception
            'conn.Close()
            Throw
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        'conn.Close()

        Session("DEL_DOC_LN#") = ds

        Return True
    End Function

    Private Class CreatePkgCmpntLnReq
        Property pkgSku As String
        Property nextLnNum As Double  ' for adding components to line list
        Property pkgLnNum As Double
        Property slsp1 As String
        Property slsp2 As String
        Property pctOfSale1 As Double
        Property pctOfSale2 As Double
    End Class

    ''' <summary>
    ''' Updates the warranty link of the warrantable sku based on the passed-in values. This could be because a new or 
    ''' existing warranty was selected.
    ''' </summary>
    ''' <param name="warrantySku">the warranty sku to be linked</param>
    ''' <param name="warrantySkuLnNum">the line number of the warranty sku to be linked</param>
    ''' <param name="warrantableSkuLnNum">the line number of the warrantable sku to which this warranty needs to be linked</param>
    Private Sub UpdateWarrantyLinks(ByVal warrantySku As String, ByVal warrantySkuLnNum As String, ByVal warrantableSkuLnNum As String)

        Dim totalRows As Integer = GridView1.Items.Count - 1
        Dim dgItem As DataGridItem
        Dim warrantyLineNum As Integer = 0
        Dim selectedWarrantySku As String = ""
        Dim isInventory As Boolean = False
        Dim lineNum As String

        'iterate through all the datagrid rows to find the warrantable lines to link this warranty sku to
        For i As Integer = 0 To totalRows
            dgItem = GridView1.Items(i)

            isInventory = ResolveYnToBooleanEquiv(dgItem.Cells(SoLnGrid.INVENTORY).Text.Trim())
            lineNum = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
            If isInventory AndAlso (lineNum.isNotEmpty AndAlso (lineNum = CType(warrantableSkuLnNum, Integer))) Then

                'means this is the matching warrantable line, whose link info needs to be updated
                dgItem.Cells(SoLnGrid.WARR_BY_LN).Text = warrantySkuLnNum
                dgItem.Cells(SoLnGrid.WARR_BY_SKU).Text = warrantySku
                dgItem.Cells(SoLnGrid.WARR_BY_DOC_NUM).Text = txt_del_doc_num.Text
                dgItem.Cells(SoLnGrid.WARRANTABLE).Text = "Y"

                Dim lbl_warr As Label = DirectCast(dgItem.FindControl("lbl_warr"), Label)
                Dim lbl_warr_sku As Label = DirectCast(dgItem.FindControl("lbl_warr_sku"), Label)
                lbl_warr.Visible = True
                lbl_warr_sku.Visible = True
                Dim cbWarr As CheckBox = DirectCast(dgItem.FindControl("ChkBoxWarr"), CheckBox)
                cbWarr.Checked = True
                cbWarr.Enabled = True

                Exit For
            End If
        Next

        '**** Now update the dataset itself
        Dim soLnDataSet As DataSet = Session("DEL_DOC_LN#")
        Dim soLnDatTbl As DataTable = soLnDataSet.Tables(0)
        Dim soLnRow As DataRow
        For Each soLnRow In soLnDatTbl.Rows

            If soLnRow("DEL_DOC_LN#") = warrantableSkuLnNum Then
                'finds the warrantied row and update the warranty link on it

                soLnRow("WARR_BY_LN") = CInt(warrantySkuLnNum)
                soLnRow("WARR_BY_SKU") = warrantySku
                soLnRow("WARR_BY_DOC_NUM") = txt_del_doc_num.Text
                soLnRow("WARRANTABLE") = "Y"
                Exit For
            End If
        Next
    End Sub


    ''' <summary>
    ''' Called when the order changes are saved. 
    ''' </summary>
    Private Sub Add_Sales_Order_Line(ByRef command As OracleCommand, ByRef soLn As DataRow,
                                     ByVal store_cd As String, ByVal loc_cd As String, ByVal serial_num As String,
                                     ByVal taxable_amt As Double, ByVal tax_chg As Double, ByVal pkg_itm_cd As String, ByVal splitLnReqList As ArrayList)

        ' TODO consider using other columns from datarow - list is long

        Dim line_no As String = soLn("DEL_DOC_LN#")
        Dim isSplitting As Boolean = isSplittingLn(line_no, splitLnReqList)
        Dim itm_cd As String = soLn("itm_cd").ToString
        Dim out_id As String = soLn("OUT_ID").ToString
        Dim delivery_points As Object = soLn("delivery_points")
        Dim disc_amt As String = soLn("DISC_AMT").ToString
        Dim qty As Double = soLn("QTY")
        Dim unit_prc As Double = soLn("UNIT_PRC").ToString
        If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT Then
            Dim filterText As String = String.Empty
            Dim filteredRows As DataRow()
            filterText = "PACKAGEROWID = " + soLn("package_parent").ToString + " AND ITM_CD = '" + soLn("ITM_CD").ToString + "'"
            Dim splitDataItm As DataTable = DirectCast(Web.HttpContext.Current.Session("PKGSPLITDATA"), DataTable)
            If splitDataItm IsNot Nothing AndAlso Not IsDBNull(soLn("package_parent")) AndAlso soLn("package_parent").ToString() <> String.Empty Then
                filteredRows = splitDataItm.Select(filterText)
                If filteredRows.Length > 0 Then
                    unit_prc = filteredRows(0)("RET_PRC")
                End If
            End If
        End If
        Dim lnSlsp1 As String = soLn("LN_SLSP1")
        Dim lnSlsp1Pct As Double = soLn("LN_SLSP1_PCT")
        Dim lnSlsp2 As String = soLn("LN_SLSP2") + ""
        Dim lnSlsp2Pct As Double = 0
        If (Not IsNothing(soLn("LN_SLSP2_PCT"))) AndAlso IsNumeric(soLn("LN_SLSP2_PCT")) Then
            lnSlsp2Pct = soLn("LN_SLSP2_PCT")
        End If
        Dim objSql As OracleCommand
        Dim ItmDatRdr As OracleDataReader
        Dim sql, sql2 As String
        Dim del_doc_num As String = Trim(txt_del_doc_num.Text)
        Dim out_cd As String = ""
        Dim spiffTable As String = If(SysPms.isSpiffBySKU, "itm2spiff_clndr_by_sku", "ITM$SPIFF_CLNDR")

        ' TODO below - send in hte out_cd from inv_res on sale line - have it now, then remove this call and merge
        If out_id & "" <> "" Then
            out_cd = InventoryUtils.GetOutCd(out_id)
        Else
            out_cd = soLn("OUT_CD").ToString
        End If

        Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sqlSB As StringBuilder = New StringBuilder()
        sqlSB.Append("SELECT I.ITM_CD, NVL(SPIFF_AMT, SPIFF) AS SPIFF, COMM_CD, ITM_TP_CD, PKG_CMPNT, INVENTORY, TAX_RESP ")
        sqlSB.Append(" FROM ITM I LEFT JOIN  " + spiffTable + "  sp ON I.ITM_CD = SP.ITM_CD AND ")
        sqlSB.Append("      TO_DATE(sysdate) BETWEEN TO_DATE(EFF_DT) AND TO_DATE(END_DT) ")
        sqlSB.Append(If(SysPms.isSpiffBySKU, " ", (" AND STORE_CD = '" & txt_store_cd.Text & "' ")))
        sqlSB.Append(" WHERE I.ITM_CD= :itmCd ")

        objSql = DisposablesManager.BuildOracleCommand(sqlSB.ToString(), conn)
        objSql.Parameters.Add(":itmCd", OracleType.VarChar)
        objSql.Parameters(":itmCd").Value = itm_cd

        Try
            conn.Open()
            ItmDatRdr = DisposablesManager.BuildOracleDataReader(objSql)

            If ItmDatRdr.Read() Then
                sql = "INSERT INTO SO_LN (DEL_DOC_NUM, DEL_DOC_LN#, ITM_CD, COMM_CD, SO_DOC_NUM, QTY, UNIT_PRC, VOID_FLAG, delivery_points, picked, lv_in_carton, taken_with, set_up, disc_amt "
                sql2 = " VALUES('" & del_doc_num & "'," & line_no & ",'" & ItmDatRdr.Item("ITM_CD").ToString & "','" & ItmDatRdr.Item("COMM_CD").ToString & "'"
                sql2 = sql2 & ",'" & Left(del_doc_num, 11) & "'," & qty & "," & unit_prc & ", 'N', "
                If IsNumeric(delivery_points) AndAlso delivery_points > 0 Then
                    sql2 = sql2 & delivery_points & ", "
                Else
                    sql2 = sql2 & "NULL, "
                End If
                sql2 = sql2 & If(ItmDatRdr.Item("INVENTORY").ToString = "Y", "'Z'", "'X'") & ", NVL('" & soLn("lv_in_carton").ToString & "','N'), 'N', 'N', "
                If IsNumeric(disc_amt) AndAlso disc_amt > 0 Then
                    sql2 = sql2 & disc_amt
                Else
                    sql2 = sql2 & "NULL "
                End If
                sql = sql & ", SO_EMP_SLSP_CD1, PCT_OF_SALE1"
                '***If slspsn and pct info on the line is not empty, use that. Otherwise default the data from the header(SO table)
                If (lnSlsp1.isNotEmpty) Then
                    sql2 = sql2 & ", '" & lnSlsp1 & "','" & lnSlsp1Pct & "'"
                Else
                    sql2 = sql2 & ", '" & txt_slsp1.Text & "','" & txt_comm1.Text & "'"
                End If
                If (Not IsDBNull(soLn("orig_so_ln_num"))) AndAlso (Not "Y".Equals(soLn("void_flag"))) Then
                    sql = sql & ", alt_doc_ln# "
                    sql2 = sql2 & ", '" & soLn("orig_so_ln_num") & "'"
                End If
                'Check if the Row is replaced!, if yes, then consider the PKG_CMPNT of the row
                If soLn.Table.Columns("ISREPLACED") IsNot Nothing Then
                    sql = sql & ", PKG_SOURCE"
                    sql2 = sql2 & ", '" & soLn("PKG_SOURCE").ToString() & "'"
                Else
                    If (Not IsDBNull(ItmDatRdr.Item("PKG_CMPNT")) AndAlso ItmDatRdr.Item("PKG_CMPNT") = "Y") Then
                        sql = sql & ", PKG_SOURCE"
                        sql2 = sql2 & ", '" & pkg_itm_cd & "'"
                    End If
                End If



                ' only fill addon_wr_dt when dt different than SO written BUT NOT when from line splitting
                If Convert.ToDateTime(txt_wr_dt.Text) < Convert.ToDateTime(FormatDateTime(Today.Date, DateFormat.ShortDate)) AndAlso
                     Not (isSplitting) Then ' TODO - this should be transaction dt, not sysdate - if there is a trans date
                    sql = sql & ", ADDON_WR_DT"
                    sql2 = sql2 & ", TO_DATE(SYSDATE,'dd/mm/RRRR')"   ' TODO - this should be transaction dt, not sysdate - if there is a trans date 
                End If

                ' MM-4240
                ' Getting the Tax Responsibility from ITM table for the item
                Dim taxResponsibility As String = ItmDatRdr.Item("TAX_RESP").ToString
                If (String.IsNullOrWhiteSpace(taxResponsibility)) Then
                    ' If no tax responsibility code has been mentioned for the item in ITM table then we should it set it as "C" as
                    ' customer is responsible to pay the tax for this ITEM
                    taxResponsibility = "C"
                End If

                ' MM-4240
                ' When the tax has been exempted 
                If (cbo_tax_exempt.SelectedIndex <> 0) Then ' Or (cbo_stat_cd.SelectedValue = AppConstants.Order.STATUS_VOID)) Then
                    ' Since the tax exemption has been given to the customer we should update SO_LN with no tax information
                    sql = sql & ", CUST_TAX_CHG, TAX_CD, TAX_RESP, TAX_BASIS, store_tax_chg "
                    sql2 = sql2 & ", " & 0 & ", " & "''" & ", " & "''" & ", " & 0 & ", " & 0 & ""

                ElseIf (ConfigurationManager.AppSettings("tax_line").ToString = "Y") Or ("L".Equals(ViewState("TaxMethod"))) And (IsNumeric(tax_chg) And (cbo_tax_exempt.SelectedIndex = 0)) Then ' TODO - this should depend on tax_cd.tax_method also
                    sql = sql & ", CUST_TAX_CHG, TAX_CD, TAX_RESP, TAX_BASIS, store_tax_chg "
                    'sql2 = sql2 & ", " & tax_chg & ",'" & lbl_tax_cd.Text & "', 'C', " & unit_prc & ", 0 "
                    sql2 = sql2 & ", " & tax_chg & ",'" & lbl_tax_cd.Text & "', '" & taxResponsibility & "', " & unit_prc & ", 0 "
                End If

                If lnSlsp2.isNotEmpty Then
                    sql = sql & ", SO_EMP_SLSP_CD2, PCT_OF_SALE2"
                    'sql2 = sql2 & ", '" & txt_slsp2.Text & "','" & txt_comm2.Text & "'"
                    sql2 = sql2 & ", '" & lnSlsp2 & "','" & lnSlsp2Pct & "'"
                End If

                'Adding Commission flags for new lines added to the sale
                If SysPms.commissionOnDelvCharge Then
                    sql = sql & ", COMM_ON_DEL_CHG"
                    sql2 = sql2 & ", 'Y'"
                Else
                    sql = sql & ", COMM_ON_DEL_CHG"
                    sql2 = sql2 & ", 'N'"
                End If
                If SysPms.commissionOnSetupCharge Then
                    sql = sql & ", COMM_ON_SETUP_CHG"
                    sql2 = sql2 & ", 'Y'"
                Else
                    sql = sql & ", COMM_ON_SETUP_CHG"
                    sql2 = sql2 & ", 'N'"
                End If
                If out_id & "" <> "" Then
                    sql = sql & ",OUT_ID_CD"
                    sql2 = sql2 & ",'" & out_id & "' "
                    sql = sql & ",OUT_CD"
                    sql2 = sql2 & ",'" & out_cd & "' "
                ElseIf out_cd & "" <> "" Then
                    sql = sql & ",OUT_CD"
                    sql2 = sql2 & ",'" & out_cd & "' "
                End If
                ' no pu_del_dt on so_ln per DSA

                'if the price was changed, save the price approval code
                If (soLn("PRC_CHG_APP_CD").ToString.isNotEmpty) Then
                    sql = sql & ", PRC_CHG_APP_CD"
                    sql2 = sql2 & ", '" & soLn("PRC_CHG_APP_CD").ToString & "'"
                End If

                If isSplitting Then
                    ' if line is split, it is on the line already, then set from line, otherwise from itm
                    If IsNumeric(soLn("SPIFF")) Then
                        sql = sql & ", SPIFF"
                        sql2 = sql2 & ", " & soLn("SPIFF").ToString
                    End If

                ElseIf IsNumeric(ItmDatRdr.Item("SPIFF")) Then

                    If CDbl(ItmDatRdr.Item("SPIFF")) > 0 Then
                        sql = sql & ", SPIFF"
                        sql2 = sql2 & ", " & ItmDatRdr.Item("SPIFF")
                    End If
                End If
                sql = sql & ")"
                sql2 = sql2 & ")"
                sql = sql & sql2

                command.Parameters.Clear()
                command.CommandText = UCase(sql)
                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()

                '***** if the line is warrantable and has a link, Insert new warranty link info
                Dim isWarrantable As Boolean = ItmDatRdr.Item("INVENTORY").ToString = "Y" AndAlso soLn("WARRANTABLE").ToString = "Y"
                Dim hasWarrantyLinked As Boolean = Not IsDBNull(soLn("WARR_BY_LN")) AndAlso soLn("WARR_BY_LN").ToString.isNotEmpty
                'If (ItmDatRdr.Item("INVENTORY").ToString = "Y") AndAlso soLn("WARRANTABLE").ToString = "Y" AndAlso (Not IsNothing(soLn("WARR_BY_LN"))) Then
                If (isWarrantable AndAlso hasWarrantyLinked) Then

                    Dim soLnWarrObj As New SalesUtils.SoLnWarrDtc
                    soLnWarrObj.docNum = del_doc_num
                    soLnWarrObj.docLnNum = line_no
                    soLnWarrObj.warrDocNum = soLn("WARR_BY_DOC_NUM")
                    soLnWarrObj.warrDocLnNum = soLn("WARR_BY_LN")
                    SalesUtils.InsertSoLnWarr(soLnWarrObj, command)

                End If

                'Update inventory if allocated on the sales order line
                If store_cd & "" <> "" And loc_cd & "" <> "" Then

                    Dim origSoLn As New OrderLine
                    origSoLn.delDocNum = del_doc_num
                    origSoLn.lnNum = line_no
                    origSoLn.itmCd = ItmDatRdr.Item("ITM_CD").ToString
                    Dim chngSoLn As New OrderLine
                    Dim updt As New soLn_updates
                    updt.newLn = True
                    If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then

                        Debit_Inventory(command, qty, del_doc_num, itm_cd, store_cd, loc_cd, line_no, lbl_PD.Text, chngSoLn, 0, serial_num, out_id)
                        updt.inv = True

                    ElseIf cbo_stat_cd.SelectedValue = AppConstants.Order.STATUS_FINAL Then
                        ' only update CRM inventory if finalizing
                        Credit_Inventory(command, qty, del_doc_num, itm_cd, store_cd, loc_cd, line_no, "", "", "", 0, chngSoLn, serial_num, "N", out_id, out_cd)
                        soLn("OUT_ID") = chngSoLn.outId
                        updt.inv = True
                    End If
                    If lbl_desc.Text & "" <> "" Then ' InStr(lbl_desc.Text, "Error") > 0 Then - DSA label_desc is not set yet or doesn't have 
                        'lbl_desc.Text = Session("err") & "Errors were encountered and the operation could not be completed."
                        Throw New ResUnresException(lbl_desc.Text & vbCrLf & "Errors were encountered and the operation could not be completed.")
                    End If
                    Updt_SoLn(origSoLn, chngSoLn, updt, command)
                End If
            End If
            ItmDatRdr.Close()

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
    End Sub

    Public Class SplitLineRequest

        Property soDiscCd As String
        Property origDelDocNum As String
        Property origLnNum As String     ' for temp table, these are line_id

        Property newDelDocNum As String  ' for temp table is session Id
        Property newLnNum As String
        Property newLnQty As Double
        Property newLnStoreCd As String
        Property newLnLocCd As String
        Property newResId As String       'to indicate a new soft-reservation
        Property newLnTax As String
        Property newLnWarrByLine As String     'warranty line seq # that the new line needs to be linked to
        Property newLnWarrBySku As String      'warranty sku# that the new line needs to be linked to
        Property newLnWarrByDocNum As String   'warranty doc# that the new line needs to be linked to

        ' Property newQtyOnOrigLn As Double

    End Class

    Private Shared Sub SplitLnRelatedTbls(ByVal splitLn As SplitLineRequest, ByRef cmd As OracleCommand)

        ' for comm_paid, the last entry is the one that is valid
        Dim sqlSb As New StringBuilder
        sqlSb.Append("INSERT INTO comm_paid ( DEL_DOC_NUM, DEL_DOC_LN, so_emp_slsp_cd1, refund_flag1, pct_of_sale1, so_emp_slsp_cd2, ").Append(
                  " refund_flag2, pct_of_sale2, dt_time)").Append(
                  " SELECT :newDelDocNum, :newlnNum, so_emp_slsp_cd1, refund_flag1, pct_of_sale1, so_emp_slsp_cd2, ").Append(
                  " refund_flag2, pct_of_sale2, SYSDATE ").Append(
                  " FROM comm_paid cp1 WHERE DEL_DOC_NUM = :origDelDocNum AND DEL_DOC_LN = :origlnNum AND dt_time = ").Append(
                  "(SELECT MAX(cp2.dt_time) FROM comm_paid cp2 WHERE cp1.del_doc_num = cp2.del_doc_num ").Append(
                  "AND cp1.del_doc_ln = cp2.del_doc_ln)")
        cmd.CommandText = sqlSb.ToString
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Clear()

        cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
        cmd.Parameters.Add(":origlnNum", OracleType.Number)
        cmd.Parameters.Add(":newlnNum", OracleType.Number)

        cmd.Parameters(":newDelDocNum").Value = splitLn.newDelDocNum
        cmd.Parameters(":origDelDocNum").Value = splitLn.origDelDocNum
        cmd.Parameters(":origlnNum").Value = CInt(splitLn.origLnNum)
        cmd.Parameters(":newlnNum").Value = CInt(splitLn.newLnNum)

        cmd.ExecuteNonQuery()


        ' disc_amt on SO_LN is unit disc; also on SO_LN2DISC so just copy 
        ' seq_num is per so_ln so that also can be copied
        If (Not String.IsNullOrEmpty(splitLn.soDiscCd) AndAlso SysPms.multiDiscCd = splitLn.soDiscCd) Then

            sqlSb.Clear()
            sqlSb.Append("INSERT INTO SO_LN2DISC ( DEL_DOC_NUM, DEL_DOC_LN#, DISC_CD, DISC_AMT, DISC_REASON_CD, DISC_CMNT, DISC_PCT, SEQ_NUM ) ").Append(
                      " SELECT :newDelDocNum, :newlnNum, DISC_CD, DISC_AMT, ").Append(
                      " DISC_REASON_CD, DISC_CMNT, DISC_PCT, SEQ_NUM ").Append(
                      " FROM SO_LN2DISC WHERE DEL_DOC_NUM = :origDelDocNum AND DEL_DOC_LN# = :origlnNum")
            cmd.CommandText = sqlSb.ToString
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Clear()

            cmd.Parameters.Add(":newDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origDelDocNum", OracleType.VarChar)
            cmd.Parameters.Add(":origlnNum", OracleType.Number)
            cmd.Parameters.Add(":newlnNum", OracleType.Number)

            cmd.Parameters(":newDelDocNum").Value = splitLn.newDelDocNum
            cmd.Parameters(":origDelDocNum").Value = splitLn.origDelDocNum
            cmd.Parameters(":origlnNum").Value = CInt(splitLn.origLnNum)
            cmd.Parameters(":newlnNum").Value = CInt(splitLn.newLnNum)

            cmd.ExecuteNonQuery()

        End If

    End Sub

    Private Function isSplittingLn(ByVal currLnNum As String, ByVal splitLnReqList As ArrayList) As Boolean
        'it will return TRUE if all validations pass, and the save process can continue, FALSE otherwise

        Dim retval As Boolean = False
        Dim splitLnReq As SplitLineRequest

        If Not splitLnReqList Is Nothing Then

            For Each splitLnReq In splitLnReqList

                If currLnNum.isNotEmpty AndAlso splitLnReq.newLnNum.isNotEmpty AndAlso
                    currLnNum.Equals(splitLnReq.newLnNum) Then

                    retval = True
                End If
            Next
        End If

        Return retval
    End Function

    Public Sub Insert_ITM_TRN(ByVal qty As Double, ByVal ivc_cd As String, ByVal itm_cd As String, ByVal store_cd As String, ByVal loc_cd As String,
                              ByVal ln_no As Double, ByVal rcv_dt As String, ByVal cst As String, ByVal fifo_dt As String, ByRef cmd As OracleCommand,
                              Optional ByVal ser_num As String = "", Optional ByVal out_id As String = "", Optional ByVal out_cd As String = "")
        'Create the finalization transaction in the ITM_TRN table

        Dim sqlStmtSb As New StringBuilder
        Dim itt_cd As String = ""
        If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then
            itt_cd = "SOF"
        ElseIf txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM Then
            itt_cd = "CMF"
        End If

        sqlStmtSb.Append("INSERT INTO ITM_TRN( ITT_CD, ITM_CD, TRN_DT, QTY, CST, del_doc_num, POSTED_TO_GL, CONF_LABELS, ORIGIN_CD, EMP_CD_OP, ID_CD, NEW_OUT_CD, ")

        If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then
            sqlStmtSb.Append("OLD_STORE_CD, OLD_LOC_CD ")

        ElseIf txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM Then
            sqlStmtSb.Append("NEW_STORE_CD, NEW_LOC_CD ")
        End If

        sqlStmtSb.Append(" ) VALUES(:itt_cd, :itm_cd, TRUNC(SYSDATE) ")  ' TODO when is TRN_DT different than create_dt; create is not truncated but why separate then
        sqlStmtSb.Append(", :qty, :cst, :del_doc_num, 'N','N','POS', :emp_cd, :out_id, :out_cd, :store_cd, :loc_cd  ")

        sqlStmtSb.Append(" )")

        cmd.Parameters.Clear()
        cmd.Parameters.Add(":itt_cd", OracleType.VarChar)
        cmd.Parameters(":itt_cd").Value = itt_cd
        cmd.Parameters.Add(":itm_cd", OracleType.VarChar)
        cmd.Parameters(":itm_cd").Value = itm_cd
        cmd.Parameters.Add(":qty", OracleType.Number)
        cmd.Parameters(":qty").Value = qty
        cmd.Parameters.Add(":cst", OracleType.Number)
        cmd.Parameters(":cst").Value = cst
        cmd.Parameters.Add(":del_doc_num", OracleType.VarChar)
        cmd.Parameters(":del_doc_num").Value = txt_del_doc_num.Text
        cmd.Parameters.Add(":emp_cd", OracleType.VarChar)
        cmd.Parameters(":emp_cd").Value = Session("EMP_CD")
        cmd.Parameters.Add(":store_cd", OracleType.VarChar)
        cmd.Parameters(":store_cd").Value = store_cd
        cmd.Parameters.Add(":loc_cd", OracleType.VarChar)
        cmd.Parameters(":loc_cd").Value = loc_cd

        cmd.Parameters.Add(":out_id", OracleType.VarChar)
        cmd.Parameters.Add(":out_cd", OracleType.VarChar)

        If String.IsNullOrWhiteSpace(out_id) Then
            cmd.Parameters(":out_id").Value = System.DBNull.Value
        Else
            cmd.Parameters(":out_id").Value = out_id
        End If
        If String.IsNullOrWhiteSpace(out_cd) Then
            cmd.Parameters(":out_cd").Value = System.DBNull.Value
        Else
            cmd.Parameters(":out_cd").Value = out_cd
        End If

        cmd.CommandText = sqlStmtSb.ToString
        cmd.CommandType = CommandType.Text
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
        ' TODO - DSA possibly need to do the OOC inserts into ITM_TRN and serial num and rf_id_cd
    End Sub

    Private Function Credit_Inventory(ByRef command As OracleCommand, ByVal qty As Double, ByVal ivc_cd As String,
                                      ByVal itm_cd As String, ByVal store_cd As String, ByVal loc_cd As String,
                                      ByVal ln_no As Double, ByVal rcv_dt As String, ByVal cst As String,
                                      ByVal fifo_dt As String, ByVal ooc_qty As Double, ByRef chngSoLn As OrderLine, Optional ByVal ser_num As String = "",
                                      Optional ByVal void_flag As String = "N", Optional ByVal out_id As String = "",
                                      Optional ByVal out_cd As String = "") As Boolean
        'NOTE the command variable passed in, should be used for ALL operations that insert/update from the DB
        '     so that they become part of an atomic transaction where commit only occurs if all succeeds

        Dim Use_API As Boolean = False
        If HBCG_Utils.Use_New_Logic(AppConstants.NewLogic.INV_API) Then
            Use_API = True
        End If

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql As OracleCommand
        Dim sql As String = ""
        Dim Mydatareader As OracleDataReader
        Dim tax_cd As String = ""

        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()    ' this connection is used thruout the process so do not move

        If Not IsNumeric(cst) Then
            sql = sql & "SELECT REPL_CST FROM ITM WHERE ITM_CD='" & itm_cd & "'"
            sql = UCase(sql)
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)
            Try

                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If Mydatareader.Read Then
                    If IsNumeric(Mydatareader.Item("REPL_CST").ToString) Then
                        cst = CDbl(Mydatareader.Item("REPL_CST").ToString)
                    Else
                        cst = 0
                    End If
                Else
                    cst = 0
                End If
                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

        If Not IsDate(fifo_dt) Then fifo_dt = FormatDateTime(Today.Date, DateFormat.ShortDate)
        If Not IsDate(rcv_dt) Then rcv_dt = FormatDateTime(Today.Date, DateFormat.ShortDate)

        fifo_dt = FormatDateTime(fifo_dt, DateFormat.ShortDate)
        rcv_dt = FormatDateTime(rcv_dt, DateFormat.ShortDate)
        Dim retOutId As String = ""

        If Use_API = True Then

            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM Then

                sql = "SELECT SER_NUM FROM INV_XREF WHERE DISPOSITION='PEN' AND DEL_DOC_NUM='" & ivc_cd & "' AND DEL_DOC_LN#=" & CInt(ln_no)
                sql = UCase(sql)
                objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                    If Mydatareader.Read Then
                        ser_num = Mydatareader.Item("SER_NUM").ToString
                    End If
                    Mydatareader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

                sql = "UPDATE inv_xref set "
                sql = sql & "disposition = 'RES'"
                sql = sql & "WHERE del_doc_num = '" & ivc_cd & "' "
                sql = sql & "AND del_doc_ln# = " & CInt(ln_no)
                command.CommandText = sql
                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()

                command.CommandText = "bt_order_inv_wrapper.return_inv"
                command.CommandType = CommandType.StoredProcedure
                command.Parameters.Clear()

                command.Parameters.Add("tran_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
                command.Parameters.Add("origin_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("emp_cd_op_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("src_tp_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("action_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("cmnt_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("rf_id_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input

                command.Parameters.Add("del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("del_doc_ln#_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("qty_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("loc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input

                command.Parameters.Add("out_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("ser_num_i", OracleType.VarChar, 30).Direction = ParameterDirection.Input
                command.Parameters.Add("ooc_qty_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("fifo_cst_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("comm_cd_i", OracleType.VarChar, 3).Direction = ParameterDirection.Input
                command.Parameters.Add("spiff_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("void_flag_i", OracleType.VarChar).Direction = ParameterDirection.Input

                command.Parameters.Add("unit_prc_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("out_id_cd_io", OracleType.VarChar, 10).Direction = ParameterDirection.InputOutput
                command.Parameters.Add("id_num_io", OracleType.VarChar, 15).Direction = ParameterDirection.InputOutput

                command.Parameters.Add("process_stat_o", OracleType.Number).Direction = ParameterDirection.Output
                command.Parameters.Add("err_cd_o", OracleType.Number).Direction = ParameterDirection.Output
                command.Parameters.Add("err_msg_o", OracleType.VarChar, 512).Direction = ParameterDirection.Output

                command.Parameters("tran_dt_i").Value = Today.Date
                command.Parameters("origin_cd_i").Value = "POS"
                command.Parameters("emp_cd_op_i").Value = HttpContext.Current.Session("emp_cd")
                command.Parameters("src_tp_i").Value = "S"
                command.Parameters("action_cd_i").Value = System.DBNull.Value
                command.Parameters("cmnt_i").Value = System.DBNull.Value
                command.Parameters("rf_id_cd_i").Value = System.DBNull.Value

                command.Parameters("del_doc_num_i").Value = ivc_cd
                command.Parameters("del_doc_ln#_i").Value = CInt(ln_no)
                command.Parameters("itm_cd_i").Value = itm_cd
                command.Parameters("qty_i").Value = qty
                command.Parameters("store_cd_i").Value = store_cd
                command.Parameters("loc_cd_i").Value = loc_cd

                command.Parameters("out_cd_i").Value = out_cd
                command.Parameters("ser_num_i").Value = ser_num
                command.Parameters("ooc_qty_i").Value = ooc_qty
                command.Parameters("fifo_cst_i").Value = cst
                command.Parameters("comm_cd_i").Value = System.DBNull.Value
                command.Parameters("spiff_i").Value = 0
                command.Parameters("void_flag_i").Value = void_flag

                command.Parameters("unit_prc_i").Value = 0
                command.Parameters("out_id_cd_io").Value = out_id
                command.Parameters("id_num_io").Value = System.DBNull.Value

            Else
                'command.CommandText = "bt_order_inv_wrapper.unres_inv_POS" 'revert JDA changes for now April 19, 2017
                command.CommandText = "bt_order_inv_wrapper.unres_inv"
                command.CommandType = CommandType.StoredProcedure
                command.Parameters.Clear()

                command.Parameters.Add("tran_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
                command.Parameters.Add("origin_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("emp_cd_op_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("src_tp_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("action_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("cmnt_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("rf_id_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("del_doc_ln#_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("qty_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("loc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("out_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("ser_num_i", OracleType.VarChar, 30).Direction = ParameterDirection.Input
                command.Parameters.Add("ooc_qty_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("fifo_cst_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("fifo_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
                command.Parameters.Add("fifl_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
                command.Parameters.Add("fill_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
                command.Parameters.Add("comm_cd_i", OracleType.VarChar, 3).Direction = ParameterDirection.Input
                command.Parameters.Add("spiff_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("unit_prc_i", OracleType.Number).Direction = ParameterDirection.Input
                command.Parameters.Add("out_id_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("id_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
                command.Parameters.Add("process_stat_o", OracleType.Number).Direction = ParameterDirection.Output
                command.Parameters.Add("err_cd_o", OracleType.Number).Direction = ParameterDirection.Output
                command.Parameters.Add("err_msg_o", OracleType.VarChar, 512).Direction = ParameterDirection.Output

                command.Parameters("tran_dt_i").Value = FormatDateTime(Today.Date, DateFormat.ShortDate)
                command.Parameters("origin_cd_i").Value = "POS"
                command.Parameters("emp_cd_op_i").Value = HttpContext.Current.Session("emp_cd")
                command.Parameters("src_tp_i").Value = "S"
                command.Parameters("action_cd_i").Value = System.DBNull.Value
                command.Parameters("cmnt_i").Value = System.DBNull.Value
                command.Parameters("rf_id_cd_i").Value = System.DBNull.Value
                command.Parameters("del_doc_num_i").Value = ivc_cd
                command.Parameters("del_doc_ln#_i").Value = CInt(ln_no)
                command.Parameters("itm_cd_i").Value = itm_cd
                command.Parameters("qty_i").Value = qty
                command.Parameters("store_cd_i").Value = store_cd
                command.Parameters("loc_cd_i").Value = loc_cd
                command.Parameters("out_cd_i").Value = out_cd
                command.Parameters("ser_num_i").Value = ser_num
                command.Parameters("ooc_qty_i").Value = ooc_qty
                command.Parameters("fifo_cst_i").Value = cst
                command.Parameters("fifo_dt_i").Value = fifo_dt
                command.Parameters("fifl_dt_i").Value = rcv_dt
                command.Parameters("fill_dt_i").Value = rcv_dt
                command.Parameters("comm_cd_i").Value = DBNull.Value
                command.Parameters("spiff_i").Value = 0
                command.Parameters("unit_prc_i").Value = 0
                command.Parameters("out_id_cd_i").Value = out_id
                command.Parameters("spiff_i").Value = 0
                command.Parameters("id_num_i").Value = System.DBNull.Value
                command.Parameters("process_stat_o").Value = System.DBNull.Value
                command.Parameters("err_cd_o").Value = System.DBNull.Value
                command.Parameters("err_msg_o").Value = System.DBNull.Value
            End If

            Try
                command.ExecuteNonQuery()

                Dim errCd As String = command.Parameters("err_cd_o").Value.ToString
                Dim errMsg As String = command.Parameters("err_msg_o").Value.ToString
                If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM AndAlso command.Parameters("out_id_cd_io").Value.ToString.isNotEmpty Then
                    retOutId = command.Parameters("out_id_cd_io").Value.ToString
                End If
                command.Parameters.Clear()

                If errCd & "" <> "" OrElse errMsg & "" <> "" Then
                    If IsNumeric(errCd.ToString) Then
                        If CDbl(errCd.ToString) = 0 Then
                            'Do Nothing
                        Else
                            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then
                                lbl_desc.Text = lbl_desc.Text & "Error unreserving inventory for SKU " & itm_cd & " in location " & store_cd & "/" & loc_cd & " (" & errMsg & ")" & vbCrLf
                                Session("err") = errMsg
                                Return False
                            Else
                                lbl_desc.Text = lbl_desc.Text & "Error returning inventory for SKU " & itm_cd & " in location " & store_cd & "/" & loc_cd & " (" & errMsg & ")" & vbCrLf
                                Session("err") = errMsg
                                Return False
                            End If
                        End If
                    Else
                        lbl_desc.Text = lbl_desc.Text & "Error unreserving inventory for SKU " & itm_cd & " in location " & store_cd & "/" & loc_cd & " (" & errMsg & ")" & vbCrLf
                        Session("err") = errMsg
                        Return False
                    End If
                End If

            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            '
            'Update the SO_LN table
            '
            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then

                chngSoLn.storeCd = ""
                chngSoLn.locCd = ""
                chngSoLn.picked = "Z"
                chngSoLn.serNum = ""
                chngSoLn.oocQty = "NaN"
                chngSoLn.fifoCst = "NaN"
                chngSoLn.fifoDt = Nothing
                chngSoLn.fiflDt = Nothing
                chngSoLn.fillDt = Nothing
                chngSoLn.outId = ""
                chngSoLn.outCd = ""

            Else
                chngSoLn.storeCd = store_cd
                chngSoLn.locCd = loc_cd
                chngSoLn.picked = "N"
                chngSoLn.serNum = ""
                chngSoLn.fifoCst = cst
                chngSoLn.fifoDt = Nothing
                chngSoLn.oocQty = ooc_qty
                chngSoLn.outId = retOutId
                chngSoLn.outCd = out_cd
            End If

        Else
            Dim Rcv_Date As String = ""
            Dim itm_cst As Double = 0
            Dim itm_qty As Double = 0
            Dim fifo_qty As Double = 0
            Dim inv_exists As Boolean = False

            store_cd = UCase(store_cd)
            loc_cd = UCase(loc_cd)
            rcv_dt = FormatDateTime(rcv_dt, DateFormat.ShortDate)

            sql = "SELECT ITM_FIFL.ITM_CD, ITM_FIFL.QTY, ITM_FIFL.STORE_CD, ITM_FIFL.RCV_DT "
            sql = sql & "FROM ITM_FIFL "
            sql = sql & "WHERE ITM_FIFL.STORE_CD='" & store_cd & "' "
            sql = sql & "AND ITM_FIFL.ITM_CD='" & itm_cd & "' "
            sql = sql & "AND ITM_FIFL.LOC_CD='" & loc_cd & "' "
            sql = sql & "ORDER BY ITM_FIFL.RCV_DT "

            sql = UCase(sql)
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If Mydatareader.Read Then
                    inv_exists = True
                End If
                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            If inv_exists = True Then

                'Update the FIFL table
                sql = "UPDATE ITM_FIFL SET QTY=QTY+" & qty & " WHERE "
                sql = sql & "ITM_FIFL.STORE_CD='" & store_cd & "' "
                sql = sql & "AND ITM_FIFL.ITM_CD='" & itm_cd & "' "
                sql = sql & "AND ITM_FIFL.LOC_CD='" & loc_cd & "' "
                command.CommandText = sql
                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()

                Dim Fifo_Lookup As Boolean = False
                sql = "SELECT ITM_FIFO.QTY as FIFO_QTY "
                sql = sql & "FROM ITM_FIFO WHERE ITM_FIFO.RCV_DT=TO_DATE('" & rcv_dt & "','mm/dd/RRRR') "
                sql = sql & "AND ITM_FIFO.STORE_CD='"
                If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                    sql = sql & store_cd
                Else
                    sql = sql & "00"
                End If
                sql = sql & "' "
                sql = sql & "AND ITM_FIFO.ITM_CD='" & itm_cd & "' "
                sql = sql & "AND ITM_FIFO.CST=" & cst & " "
                sql = sql & "ORDER BY ITM_FIFO.RCV_DT "
                sql = UCase(sql)
                objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                    If Mydatareader.Read Then
                        Fifo_Lookup = True
                    End If
                    Mydatareader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

                If Fifo_Lookup = False Then
                    'Update the FIFO table
                    sql = "INSERT INTO ITM_FIFO (ITM_CD, DIV_CD, STORE_CD, RCV_DT, CST, QTY, UP_CHRG) "
                    sql = sql & "VALUES('" & itm_cd & "', '000', '"
                    If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                        sql = sql & store_cd
                    Else
                        sql = sql & "00"
                    End If
                    sql = sql & "', TO_DATE('" & rcv_dt & "','mm/dd/RRRR'), "
                    sql = sql & cst & "," & qty & ",0)"
                Else
                    'Update the FIFO table
                    sql = "UPDATE ITM_FIFO SET QTY=QTY+" & qty & " WHERE "
                    If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                        sql = sql & "ITM_FIFO.STORE_CD='" & store_cd & "' "
                    Else
                        sql = sql & "ITM_FIFO.STORE_CD='00' "
                    End If
                    sql = sql & "AND ITM_FIFO.CST=" & cst & " "
                    sql = sql & "AND ITM_FIFO.RCV_DT=TO_DATE('" & rcv_dt & "','mm/dd/RRRR') "
                    sql = sql & "AND ITM_FIFO.ITM_CD='" & itm_cd & "' "
                End If
                command.CommandText = sql
                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()

            Else

                'Update the FIFL table
                sql = "INSERT INTO ITM_FIFL (ITM_CD, STORE_CD, LOC_CD, RCV_DT, QTY, LST_ACT_DT) "
                sql = sql & "VALUES('" & itm_cd & "', '"
                sql = sql & store_cd
                sql = sql & "', '" & loc_cd & "', TO_DATE('" & rcv_dt & "','mm/dd/RRRR'), "
                sql = sql & qty & ",SYSDATE)"
                command.CommandText = sql
                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()

                Dim Fifo_Lookup As Boolean = False
                sql = "SELECT ITM_FIFO.QTY as FIFO_QTY "
                sql = sql & "FROM ITM_FIFO WHERE ITM_FIFO.RCV_DT=TO_DATE('" & rcv_dt & "','mm/dd/RRRR') "
                sql = sql & "AND ITM_FIFO.STORE_CD='"
                If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                    sql = sql & store_cd
                Else
                    sql = sql & "00"
                End If
                sql = sql & "' "
                sql = sql & "AND ITM_FIFO.ITM_CD='" & itm_cd & "' "
                sql = sql & "AND ITM_FIFO.CST=" & cst & " "
                sql = sql & "ORDER BY ITM_FIFO.RCV_DT "
                sql = UCase(sql)
                objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                    If Mydatareader.Read Then
                        Fifo_Lookup = True
                    End If
                    Mydatareader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

                If Fifo_Lookup = False Then
                    'Update the FIFO table
                    sql = "INSERT INTO ITM_FIFO (ITM_CD, DIV_CD, STORE_CD, RCV_DT, CST, QTY, UP_CHRG) "
                    sql = sql & "VALUES('" & itm_cd & "', '000', '"
                    If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                        sql = sql & store_cd
                    Else
                        sql = sql & "00"
                    End If
                    sql = sql & "', TO_DATE('" & rcv_dt & "','mm/dd/RRRR'), "
                    sql = sql & cst & "," & qty & ",0)"
                Else
                    'Update the FIFO table
                    sql = "UPDATE ITM_FIFO SET QTY=QTY+" & qty & " WHERE "
                    If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                        sql = sql & "ITM_FIFO.STORE_CD='" & store_cd & "' "
                    Else
                        sql = sql & "ITM_FIFO.STORE_CD='00' "
                    End If
                    sql = sql & "AND ITM_FIFO.CST=" & cst & " "
                    sql = sql & "AND ITM_FIFO.RCV_DT=TO_DATE('" & rcv_dt & "','mm/dd/RRRR') "
                    sql = sql & "AND ITM_FIFO.ITM_CD='" & itm_cd & "' "
                End If
                command.CommandText = sql
                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()

            End If

            Dim ser_tp As String = ""
            Dim bulk_tp_itm As String = ""

            sql = "SELECT SERIAL_TP, BULK_TP_ITM FROM ITM WHERE ITM_CD='" & itm_cd & "'"
            sql = UCase(sql)
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If Mydatareader.Read Then
                    ser_tp = Mydatareader.Item("SERIAL_TP").ToString
                    bulk_tp_itm = Mydatareader.Item("BULK_TP_ITM").ToString
                End If
                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            ser_tp = GetRFInventoryType(ser_tp)

            'Update the INV_XREF tables
            If ConfigurationManager.AppSettings("enable_serialization") = "Y" And ser_tp = "Y" Then
                Try
                    sql = "UPDATE INV_XREF SET DISPOSITION='FIF', "
                    sql = sql & "LAST_ACTN_EMP_CD='" & HttpContext.Current.Session("EMP_CD") & "', "
                    sql = sql & "DEL_DOC_NUM=NULL, "
                    sql = sql & "DEL_DOC_LN#=NULL, "
                    sql = sql & "LAST_ACTN_DT=SYSDATE "
                    sql = sql & "WHERE DEL_DOC_NUM='" & ivc_cd & "' "
                    sql = sql & "AND DEL_DOC_LN#=" & ln_no
                    command.CommandText = sql
                    command.CommandType = CommandType.Text
                    command.ExecuteNonQuery()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            ElseIf ConfigurationManager.AppSettings("xref") = "Y" Then
                If bulk_tp_itm = "N" Then
                    Try
                        sql = "UPDATE INV_XREF SET DISPOSITION='FIF', "
                        sql = sql & "LAST_ACTN_EMP_CD='" & HttpContext.Current.Session("EMP_CD") & "', "
                        sql = sql & "DEL_DOC_NUM=NULL, "
                        sql = sql & "DEL_DOC_LN#=NULL, "
                        sql = sql & "LAST_ACTN_DT=SYSDATE "
                        sql = sql & "WHERE DEL_DOC_NUM='" & ivc_cd & "' "
                        sql = sql & "AND DEL_DOC_LN#=" & ln_no
                        command.CommandText = sql
                        command.CommandType = CommandType.Text
                        command.ExecuteNonQuery()
                    Catch
                        conn.Close()
                        Throw
                    End Try
                End If
            End If

            'Update the SO_LN table
            'If void_flag = "Y" Then
            '    sql = "UPDATE SO_LN SET VOID_FLAG='Y' "
            '    sql = sql & ",VOID_DT=TO_DATE(SYSDATE,'dd/mm/RRRR') "
            '    sql = sql & ",PICKED='V' "
            '    sql = sql & ",FILL_DT=TO_DATE(SYSDATE,'dd/mm/RRRR') "
            '    sql = sql & "WHERE DEL_DOC_LN#=" & ln_no & " AND "
            '    sql = sql & "DEL_DOC_NUM='" & ivc_cd & "' "
            'Else
            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then
                chngSoLn.storeCd = ""
                chngSoLn.locCd = ""
                chngSoLn.picked = "Z"
                chngSoLn.serNum = ""
                chngSoLn.oocQty = "NaN"
                chngSoLn.fifoCst = "NaN"
                chngSoLn.fifoDt = Nothing
                chngSoLn.fiflDt = Nothing
                chngSoLn.fillDt = Nothing
                chngSoLn.outId = ""
                chngSoLn.outCd = ""

                'sql = "UPDATE SO_LN SET STORE_CD=NULL, "
                'sql = sql & "PICKED='Z', "
                'sql = sql & "LOC_CD=NULL, "
                'sql = sql & "OOC_QTY=NULL, "
                'sql = sql & "FIFO_CST=NULL, "
                'sql = sql & "FIFO_DT=NULL, "
                'sql = sql & "FIFL_DT=NULL, "
                'sql = sql & "FILL_DT=NULL "
                'sql = sql & "WHERE DEL_DOC_LN#=" & ln_no & " AND "
                'sql = sql & "DEL_DOC_NUM='" & ivc_cd & "' "
            Else
                'Update the SO_LN table
                chngSoLn.storeCd = store_cd
                chngSoLn.locCd = loc_cd
                chngSoLn.picked = "N"
                chngSoLn.serNum = ""
                chngSoLn.fifoCst = cst
                chngSoLn.fifoDt = fifo_dt
                chngSoLn.oocQty = 1
                chngSoLn.outId = ""
                chngSoLn.outCd = ""
                'sql = "UPDATE SO_LN SET STORE_CD='" & store_cd & "', "
                'sql = sql & "PICKED='Z', "
                'sql = sql & "LOC_CD='" & loc_cd & "', "
                'sql = sql & "FIFO_CST=" & cst & ", "
                'sql = sql & "FIFO_DT=TO_DATE('" & fifo_dt & "','mm/dd/RRRR'), "
                'sql = sql & "FIFL_DT=TO_DATE('" & rcv_dt & "','mm/dd/RRRR'), "
                'sql = sql & "FILL_DT=TO_DATE('" & rcv_dt & "','mm/dd/RRRR') "
                'sql = sql & "WHERE DEL_DOC_LN#=" & ln_no & " AND "
                'sql = sql & "DEL_DOC_NUM='" & ivc_cd & "' "
            End If
            'command.CommandText = sql
            'command.CommandType = CommandType.Text
            'command.ExecuteNonQuery()


            'Update the ITM_TRN table
            sql = "INSERT INTO ITM_TRN(ITT_CD, ITM_CD, TRN_DT,"
            sql = sql & "NEW_STORE_CD, NEW_LOC_CD, "
            sql = sql & "QTY, CST, del_doc_num, "
            sql = sql & "POSTED_TO_GL, CONF_LABELS, ORIGIN_CD, EMP_CD_OP) "
            sql = sql & "VALUES('"
            sql = sql & "UNR"
            sql = sql & "','" & itm_cd & "',TO_DATE('" & Today.Date & "','mm/dd/RRRR')"
            sql = sql & ",'" & store_cd & "','" & loc_cd & "'," & qty & "," & cst & ",'" & txt_del_doc_num.Text & "','N','N','POS'"
            sql = sql & ",'" & Session("EMP_CD") & "')"
            command.CommandText = sql
            command.CommandType = CommandType.Text
            command.ExecuteNonQuery()

        End If
        conn.Close()
        Return True

    End Function

    Private Sub Debit_Inventory(ByRef command As OracleCommand, ByVal qty As Double, ByVal ivc_cd As String, ByVal itm_cd As String,
                                       ByVal store_cd As String, ByVal loc_cd As String, ByVal ln_no As Double,
                                       ByVal p_d As String, ByRef chngSoLn As OrderLine, Optional ByVal fifo_cst As Double = 0, Optional ByVal ser_num As String = "", Optional ByVal out_id As String = "")

        Dim Use_API As Boolean = False
        Dim sql As String = ""
        Dim out_cd As String = ""

        ' Note:  in Dev databases, the version for 11.1 is '11.1bt' so 11.1 always goes into the old code;  
        '   Per Mike, the customer databases are set to the exact correct value so 11.1.9 will work OK at the customer
        If HBCG_Utils.Use_New_Logic(AppConstants.NewLogic.INV_API) Then
            Use_API = True
        End If

        'NOTE: the variable "command" passed in, represents an atomic transaction so that
        '      operations made here that alter the DB are committed or rolled back as part of 
        '      the save of the transaction

        If Use_API = True Then
            If out_id & "" <> "" Then
                out_cd = InventoryUtils.GetOutCd(out_id)
            End If

            sql = "bt_order_inv_wrapper.reserve_inv"
            command.Parameters.Clear()
            command.CommandText = sql
            command.CommandType = CommandType.StoredProcedure

            command.Parameters.Add("pu_del_flg_i", OracleType.VarChar, 1).Direction = ParameterDirection.Input
            command.Parameters.Add("tran_dt_i", OracleType.DateTime).Direction = ParameterDirection.Input
            command.Parameters.Add("origin_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            command.Parameters.Add("emp_cd_op_i", OracleType.VarChar).Direction = ParameterDirection.Input
            command.Parameters.Add("src_tp_i", OracleType.VarChar).Direction = ParameterDirection.Input
            command.Parameters.Add("del_doc_num_i", OracleType.VarChar).Direction = ParameterDirection.Input
            command.Parameters.Add("del_doc_ln#_i", OracleType.Number).Direction = ParameterDirection.Input
            command.Parameters.Add("itm_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            command.Parameters.Add("qty_i", OracleType.Number).Direction = ParameterDirection.Input
            command.Parameters.Add("taken_with_i", OracleType.VarChar).Direction = ParameterDirection.Input
            command.Parameters.Add("store_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            command.Parameters.Add("loc_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            command.Parameters.Add("out_id_cd_i", OracleType.VarChar).Direction = ParameterDirection.Input
            command.Parameters.Add("out_cd_io", OracleType.VarChar, 3).Direction = ParameterDirection.InputOutput
            command.Parameters.Add("id_num_io", OracleType.VarChar, 10).Direction = ParameterDirection.InputOutput
            command.Parameters.Add("ser_num_io", OracleType.VarChar, 30).Direction = ParameterDirection.InputOutput
            command.Parameters.Add("ooc_qty_io", OracleType.Number).Direction = ParameterDirection.InputOutput
            command.Parameters.Add("fifo_cst_io", OracleType.Number).Direction = ParameterDirection.InputOutput
            command.Parameters.Add("comm_cd_io", OracleType.VarChar, 3).Direction = ParameterDirection.InputOutput
            command.Parameters.Add("spiff_io", OracleType.Number).Direction = ParameterDirection.InputOutput
            command.Parameters.Add("picked_o", OracleType.VarChar, 1).Direction = ParameterDirection.Output
            command.Parameters.Add("fill_dt_o", OracleType.DateTime).Direction = ParameterDirection.Output
            command.Parameters.Add("fifl_dt_o", OracleType.DateTime).Direction = ParameterDirection.Output
            command.Parameters.Add("fifo_dt_o", OracleType.DateTime).Direction = ParameterDirection.Output
            command.Parameters.Add("process_stat_o", OracleType.Number).Direction = ParameterDirection.Output
            command.Parameters.Add("err_cd_o", OracleType.Number).Direction = ParameterDirection.Output
            command.Parameters.Add("err_msg_o", OracleType.VarChar, 512).Direction = ParameterDirection.Output

            command.Parameters("pu_del_flg_i").Value = p_d
            command.Parameters("tran_dt_i").Value = FormatDateTime(Today.Date, DateFormat.ShortDate)
            command.Parameters("origin_cd_i").Value = "POS"
            command.Parameters("emp_cd_op_i").Value = HttpContext.Current.Session("emp_cd")
            command.Parameters("src_tp_i").Value = "S"
            command.Parameters("del_doc_num_i").Value = ivc_cd
            command.Parameters("del_doc_ln#_i").Value = CInt(ln_no)
            command.Parameters("itm_cd_i").Value = itm_cd
            command.Parameters("qty_i").Value = qty
            command.Parameters("taken_with_i").Value = "N"
            command.Parameters("store_cd_i").Value = store_cd
            command.Parameters("loc_cd_i").Value = loc_cd
            command.Parameters("out_id_cd_i").Value = out_id
            command.Parameters("out_cd_io").Value = out_cd
            command.Parameters("id_num_io").Value = DBNull.Value
            command.Parameters("ser_num_io").Value = ser_num
            command.Parameters("ooc_qty_io").Value = 0
            command.Parameters("fifo_cst_io").Value = fifo_cst
            command.Parameters("comm_cd_io").Value = DBNull.Value
            command.Parameters("spiff_io").Value = 0
            command.Parameters("picked_o").Value = System.DBNull.Value
            command.Parameters("fill_dt_o").Value = System.DBNull.Value
            command.Parameters("fifl_dt_o").Value = System.DBNull.Value
            command.Parameters("fifo_dt_o").Value = System.DBNull.Value
            command.Parameters("process_stat_o").Value = System.DBNull.Value
            command.Parameters("err_cd_o").Value = System.DBNull.Value
            command.Parameters("err_msg_o").Value = System.DBNull.Value
            command.ExecuteNonQuery()

            Dim xCd = command.Parameters("err_cd_o").Value.ToString
            Dim xMsg = command.Parameters("err_msg_o").Value.ToString + ""
            If command.Parameters("err_cd_o").Value.ToString & "" <> "" Then
                lbl_desc.Text = lbl_desc.Text & "Insufficient inventory for SKU " & itm_cd & " in location " & store_cd & "/" & loc_cd & vbCrLf &
                    " Err Cd: " & command.Parameters("err_cd_o").Value.ToString & "  Err Msg: " & xMsg & vbCrLf
                Session("err") = Session("err") & "Insufficient inventory for SKU " & itm_cd & " in location " & store_cd & "/" & loc_cd & vbCrLf
                Exit Sub
            End If

            fifo_cst = Replace(FormatNumber(command.Parameters("fifo_cst_io").Value, 2), ",", "")

            '
            'Update the SO_LN table
            '
            chngSoLn.storeCd = store_cd
            chngSoLn.locCd = loc_cd
            chngSoLn.picked = "F"
            chngSoLn.serNum = ser_num
            chngSoLn.fifoCst = CDbl(IIf(IsNumeric(fifo_cst), fifo_cst, 0))
            chngSoLn.fifoDt = command.Parameters("fifo_dt_o").Value
            chngSoLn.fiflDt = command.Parameters("fifl_dt_o").Value
            chngSoLn.fillDt = FormatDateTime(Today.Date, DateFormat.ShortDate)
            chngSoLn.oocQty = command.Parameters("ooc_qty_io").Value
            If out_id.isNotEmpty Then
                chngSoLn.outId = out_id
                chngSoLn.outCd = out_cd
            Else
                chngSoLn.outId = ""
                chngSoLn.outCd = ""
            End If

            'sql = "UPDATE SO_LN SET STORE_CD='" & store_cd & "', " & "PICKED='F', " & "LOC_CD='" & loc_cd & "', "
            'sql = sql & IIf(Not IsNumeric(fifo_cst), "FIFO_CST=0, ", "FIFO_CST=" & fifo_cst & ", ") & " SER_NUM='" & ser_num & "', "
            'sql = sql & "FIFO_DT=TO_DATE('" & command.Parameters("fifo_dt_o").Value & "','mm/dd/RRRR'), "
            'sql = sql & "FIFL_DT=TO_DATE('" & command.Parameters("fifl_dt_o").Value & "','mm/dd/RRRR'), "
            'sql = sql & "OOC_QTY=" & command.Parameters("ooc_qty_io").Value & ", "
            'sql = sql & "FILL_DT=TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR') "
            'If out_id & "" <> "" Then
            '    sql = sql & ",OUT_ID_CD='" & out_id & "' "
            '    sql = sql & ",OUT_CD='" & out_cd & "' "
            'End If
            'sql = sql & "WHERE DEL_DOC_LN#=" & ln_no & " AND "
            'sql = sql & "DEL_DOC_NUM='" & ivc_cd & "' "

            'command.Parameters.Clear()
            'command.CommandText = sql
            'command.CommandType = CommandType.Text
            'command.ExecuteNonQuery()

            ''
            ''Update the ITM_TRN table
            ''
            'Try
            '    sql = "INSERT INTO ITM_TRN(ITT_CD, ITM_CD, TRN_DT, OLD_STORE_CD, OLD_LOC_CD, QTY, CST, DEL_DOC_NUM, "
            '    sql = sql & "POSTED_TO_GL, CONF_LABELS, ORIGIN_CD, EMP_CD_OP) "
            '    sql = sql & "VALUES('OOR','" & itm_cd & "',TO_DATE('" & Today.Date & "','mm/dd/RRRR')"
            '    sql = sql & ",'" & store_cd & "','" & loc_cd & "'," & qty & ","
            '    If Not IsNumeric(fifo_cst) Then
            '        sql = sql & "0, "
            '    Else
            '        sql = sql & fifo_cst & ", "
            '    End If
            '    sql = sql & "'" & ivc_cd & "','N','N','PSOE'"
            '    sql = sql & ",'" & HttpContext.Current.Session("EMP_CD") & "')"
            '    command.CommandText = sql
            '    command.CommandType = CommandType.Text
            '    command.ExecuteNonQuery()
            'Catch ex As Exception
            '    Throw
            'End Try

        Else

            Dim Rcv_Date As String = ""
            Dim FIFO_Rcv_Date As String = ""
            Dim itm_cst As Double = 0
            Dim itm_qty As Double = 0
            Dim fifo_qty As Double = 0
            Dim ser_tp As String = ""
            Dim bulk_tp_itm As String = ""

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objsql As OracleCommand
            Dim Mydatareader As OracleDataReader
            command.Parameters.Clear()

            conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            store_cd = UCase(store_cd)
            loc_cd = UCase(loc_cd)

            sql = "SELECT NVL(SERIAL_TP,'N') AS SERIAL_TP, NVL(BULK_TP_ITM,'N') AS BULK_TP_ITM FROM ITM " &
                    "WHERE ITM_CD='" & itm_cd & "'"
            sql = UCase(sql)
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                conn.Open()
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If Mydatareader.Read Then
                    ser_tp = Mydatareader.Item("SERIAL_TP").ToString
                    bulk_tp_itm = Mydatareader.Item("BULK_TP_ITM").ToString
                End If
                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            ser_tp = GetRFInventoryType(ser_tp)

            sql = "SELECT ITM_FIFL.ITM_CD, ITM_FIFL.QTY, ITM_FIFL.STORE_CD, ITM_FIFL.RCV_DT "
            sql = sql & "FROM ITM_FIFL WHERE ITM_FIFL.STORE_CD='" & store_cd & "' "
            sql = sql & "AND ITM_FIFL.QTY >=" & qty & " "
            sql = sql & "AND ITM_FIFL.ITM_CD='" & itm_cd & "' "
            sql = sql & "AND ITM_FIFL.LOC_CD='" & loc_cd & "' "
            sql = sql & "ORDER BY ITM_FIFL.RCV_DT ASC"

            sql = UCase(sql)
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                If Mydatareader.Read Then
                    If IsDate(Mydatareader.Item("RCV_DT").ToString) Then
                        Rcv_Date = FormatDateTime(Mydatareader.Item("RCV_DT").ToString, DateFormat.ShortDate)
                    End If
                    If IsNumeric(Mydatareader.Item("QTY").ToString) Then
                        itm_qty = Mydatareader.Item("QTY").ToString
                    End If
                Else
                    conn.Close()
                    Mydatareader.Close()
                    Session("err") = Session("err") & "Insufficient inventory for SKU " & itm_cd & " in location " & store_cd & "/" & loc_cd & vbCrLf
                    lbl_desc.Text = lbl_desc.Text & "Insufficient inventory for SKU " & itm_cd & " in location " & store_cd & "/" & loc_cd & vbCrLf
                    Exit Sub
                End If
                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            '
            'Update ITM_FIFL
            '
            If itm_qty > qty Then

                'Update the FIFL table
                sql = "UPDATE ITM_FIFL SET QTY=QTY-" & qty & " WHERE "
                sql = sql & "ITM_FIFL.STORE_CD='" & store_cd & "' "
                sql = sql & "AND ITM_FIFL.RCV_DT=TO_DATE('" & Rcv_Date & "','mm/dd/RRRR') "
                sql = sql & "AND ITM_FIFL.ITM_CD='" & itm_cd & "' "
                sql = sql & "AND ITM_FIFL.LOC_CD='" & loc_cd & "' "
                command.CommandText = sql

                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()

            ElseIf itm_qty = qty Then

                'Update the FIFL table
                sql = "DELETE FROM ITM_FIFL WHERE "
                sql = sql & "ITM_FIFL.STORE_CD='" & store_cd & "' "
                sql = sql & "AND ITM_FIFL.RCV_DT=TO_DATE('" & Rcv_Date & "','mm/dd/RRRR') "
                sql = sql & "AND ITM_FIFL.ITM_CD='" & itm_cd & "' "
                sql = sql & "AND ITM_FIFL.LOC_CD='" & loc_cd & "' "
                command.CommandText = sql
                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()

            Else
                'Not enough inventory, we need to EXIT
                conn.Close()
                Exit Sub
            End If

            '
            'UPDATE FIFO...CHANGE HERE
            '
            Dim FIFO_QTY_ONGOING As Double = 0
            Dim FIFO_COST_ONGOING As Double = 0
            Dim Loop_qty As Double = qty
            FIFO_Rcv_Date = ""

            If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                sql = "SELECT ITM_FIFO.ITM_CD, ITM_FIFO.QTY, ITM_FIFO.CST, ITM_FIFO.RCV_DT "
                sql = sql & "FROM ITM_FIFO "
                sql = sql & "WHERE ITM_FIFO.STORE_CD='" & store_cd & "' "
                sql = sql & "AND ITM_FIFO.ITM_CD='" & itm_cd & "' "
                sql = sql & "ORDER BY ITM_FIFO.RCV_DT ASC, CST ASC"
            Else
                sql = "SELECT ITM_FIFO.ITM_CD, ITM_FIFO.QTY, ITM_FIFO.CST, ITM_FIFO.RCV_DT "
                sql = sql & "FROM ITM_FIFO "
                sql = sql & "WHERE ITM_FIFO.STORE_CD='00' "
                sql = sql & "AND ITM_FIFO.ITM_CD='" & itm_cd & "' "
                sql = sql & "ORDER BY ITM_FIFO.RCV_DT ASC, CST ASC"
            End If

            sql = UCase(sql)
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                Do While Mydatareader.Read
                    itm_cst = 0
                    fifo_qty = 0
                    itm_qty = 0

                    If IsNumeric(Mydatareader.Item("QTY").ToString) Then
                        fifo_qty = Mydatareader.Item("QTY").ToString
                    End If
                    If IsNumeric(Mydatareader.Item("QTY").ToString) Then
                        itm_qty = Mydatareader.Item("QTY").ToString
                    End If

                    If Loop_qty > 0 Then
                        If IsNumeric(Mydatareader.Item("CST").ToString) Then
                            itm_cst = Mydatareader.Item("CST").ToString
                        End If
                        If IsDate(Mydatareader.Item("RCV_DT").ToString) Then
                            FIFO_Rcv_Date = FormatDateTime(Mydatareader.Item("RCV_DT").ToString, DateFormat.ShortDate)
                        End If

                        If Loop_qty < itm_qty Then itm_qty = Loop_qty
                        FIFO_COST_ONGOING = FIFO_COST_ONGOING + (itm_cst * itm_qty)
                        FIFO_QTY_ONGOING = FIFO_QTY_ONGOING + itm_qty

                        If fifo_qty > Loop_qty Then
                            sql = "UPDATE ITM_FIFO SET QTY=QTY-" & Loop_qty & " WHERE "
                            If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                                sql = sql & "ITM_FIFO.STORE_CD='" & store_cd & "' "
                            Else
                                sql = sql & "ITM_FIFO.STORE_CD='00' "
                            End If
                            sql = sql & "AND ITM_FIFO.RCV_DT=TO_DATE('" & FIFO_Rcv_Date & "','mm/dd/RRRR') "
                            sql = sql & "AND ITM_FIFO.ITM_CD='" & itm_cd & "' "
                            sql = sql & "AND ITM_FIFO.CST=" & itm_cst & " "
                            command.CommandText = sql
                            command.ExecuteNonQuery()

                        ElseIf fifo_qty <= Loop_qty Then
                            sql = "DELETE FROM ITM_FIFO WHERE "
                            If ConfigurationManager.AppSettings("sys_type") = "STORE" Then
                                sql = sql & "ITM_FIFO.STORE_CD='" & store_cd & "' "
                            Else
                                sql = sql & "ITM_FIFO.STORE_CD='00' "
                            End If
                            sql = sql & "AND ITM_FIFO.RCV_DT=TO_DATE('" & FIFO_Rcv_Date & "','mm/dd/RRRR') "
                            sql = sql & "AND ITM_FIFO.ITM_CD='" & itm_cd & "' "
                            sql = sql & "AND ITM_FIFO.CST=" & itm_cst & " "
                            command.CommandText = sql
                            command.ExecuteNonQuery()

                        End If
                        Loop_qty = Loop_qty - fifo_qty
                    Else
                        Exit Do
                    End If
                Loop
                Mydatareader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            '
            'Update the SO_LN table
            '
            chngSoLn.storeCd = store_cd
            chngSoLn.locCd = loc_cd
            chngSoLn.picked = "F"
            chngSoLn.serNum = ser_num
            If Not IsNumeric(Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "")) Or Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "") = "NaN" Then
                chngSoLn.fifoCst = 0
            Else
                chngSoLn.fifoCst = CDbl(Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", ""))
            End If
            chngSoLn.fifoDt = FIFO_Rcv_Date
            chngSoLn.fiflDt = Rcv_Date
            chngSoLn.fillDt = FormatDateTime(Today.Date, DateFormat.ShortDate)
            ' No outlet or OOC coded here ???
            chngSoLn.oocQty = 1

            '
            'Update the ITM_TRN table
            '
            Try
                sql = "INSERT INTO ITM_TRN(ITT_CD, ITM_CD, TRN_DT, OLD_STORE_CD, OLD_LOC_CD, QTY, CST, DEL_DOC_NUM, "
                sql = sql & "POSTED_TO_GL, CONF_LABELS, ORIGIN_CD, EMP_CD_OP) "
                sql = sql & "VALUES('OOR','" & itm_cd & "',TO_DATE('" & Today.Date & "','mm/dd/RRRR')"
                sql = sql & ",'" & store_cd & "','" & loc_cd & "'," & qty & ","
                If Not IsNumeric(Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "")) Or Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "") = "NaN" Then
                    sql = sql & "0"
                Else
                    sql = sql & Replace(FormatNumber(FIFO_COST_ONGOING / FIFO_QTY_ONGOING, 2), ",", "")
                End If
                sql = sql & ",'" & ivc_cd & "','N','N','POS'"
                sql = sql & ",'" & HttpContext.Current.Session("EMP_CD") & "')"

                command.CommandText = sql
                command.ExecuteNonQuery()

            Catch ex As Exception
                conn.Close()
                Throw
            End Try

            '
            'Update the INV_XREF tables
            '
            If ConfigurationManager.AppSettings("enable_serialization") = "Y" And ser_tp = "Y" Then

                sql = "SELECT INV_XREF.RF_ID_CD, INV_XREF.SER_NUM "
                sql = sql & "FROM INV_XREF, ITM WHERE "
                sql = sql & "INV_XREF.STORE_CD='" & store_cd & "' "
                sql = sql & "AND INV_XREF.ITM_CD='" & itm_cd & "' "
                sql = sql & "AND INV_XREF.LOC_CD='" & loc_cd & "' "
                sql = sql & "AND INV_XREF.DISPOSITION='FIF' "
                If ser_num & "" <> "" Then
                    sql = sql & "AND INV_XREF.SER_NUM='" & ser_num & "' "
                End If
                sql = sql & "ORDER BY INV_XREF.CREATE_DT "

                'Set SQL OBJECT 
                objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                    If Mydatareader.Read Then
                        If ser_num & "" = "" And Mydatareader.Item("SER_NUM").ToString & "" <> "" Then
                            ser_num = Mydatareader.Item("SER_NUM").ToString
                        End If

                        sql = "UPDATE INV_XREF SET DISPOSITION='RES', "
                        sql = sql & "LAST_ACTN_EMP_CD='" & HttpContext.Current.Session("EMP_CD") & "', "
                        sql = sql & "DEL_DOC_NUM='" & ivc_cd & "', "
                        sql = sql & "DEL_DOC_LN#=" & ln_no & ", "
                        sql = sql & "LAST_ACTN_DT=SYSDATE "
                        sql = sql & "WHERE RF_ID_CD='" & Mydatareader.Item("RF_ID_CD").ToString & "'"

                        command.CommandText = sql
                        command.ExecuteNonQuery()
                    End If

                    Mydatareader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

            ElseIf ConfigurationManager.AppSettings("xref") = "Y" Then
                If bulk_tp_itm <> "Y" Then
                    Dim x As Integer = 0
                    sql = "SELECT INV_XREF.RF_ID_CD "
                    sql = sql & "FROM INV_XREF, ITM WHERE "
                    sql = sql & "INV_XREF.STORE_CD='" & store_cd & "' "
                    sql = sql & "AND INV_XREF.ITM_CD='" & itm_cd & "' "
                    sql = sql & "AND INV_XREF.LOC_CD='" & loc_cd & "' "
                    sql = sql & "AND INV_XREF.DISPOSITION='FIF' "
                    sql = sql & "AND ITM.ITM_CD=INV_XREF.ITM_CD "
                    sql = sql & "AND NVL(ITM.BULK_TP_ITM,'N')='N' "
                    sql = sql & "ORDER BY INV_XREF.CREATE_DT "

                    'Set SQL OBJECT 
                    objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                    Try
                        'Execute DataReader 
                        Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                        Do While Mydatareader.Read
                            If x < qty Then
                                sql = "UPDATE INV_XREF SET DISPOSITION='RES', "
                                sql = sql & "LAST_ACTN_EMP_CD='" & HttpContext.Current.Session("EMP_CD") & "', "
                                sql = sql & "DEL_DOC_NUM='" & ivc_cd & "', "
                                sql = sql & "DEL_DOC_LN#=" & ln_no & ", "
                                sql = sql & "LAST_ACTN_DT=SYSDATE "
                                sql = sql & "WHERE RF_ID_CD='" & Mydatareader.Item("RF_ID_CD").ToString & "'"

                                command.CommandText = sql
                                command.ExecuteNonQuery()
                            Else
                                Exit Do
                            End If
                            x = x + 1
                        Loop
                    Catch
                        conn.Close()
                        Throw
                    End Try
                End If

            End If
            conn.Close()
            command.Parameters.Clear()
        End If
    End Sub

    Public Sub Save_Comments(ByRef soDatRow As DataRow, ByRef cmd As OracleCommand)

        Dim MyDatareader As OracleDataReader
        Dim sql As String
        Dim txtCharCount As Integer, y As Integer
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim lngSEQNUM As Integer, lngComments As Integer, strSO_SEQ_NUM As String
        Dim strComments As String, strMidCmnts As String
        strSO_SEQ_NUM = ""
        txtCharCount = 0
        lngSEQNUM = 0
        Dim so_store_cd As String = txt_store_cd.Text  ' store code may or may not be in the del_doc_num as of 12.0
        Dim sqlQueryBuilder As StringBuilder = New StringBuilder
        cmd.CommandType = CommandType.Text

        Try

            If Not Session("scomments") Is Nothing Then
                'Insert Sales "S" Comments
                strComments = Session("scomments").ToString
                lngComments = Len(strComments)
                Dim del_doc_num, cust_cd As String

                del_doc_num = txt_del_doc_num.Text
                cust_cd = txt_Cust_cd.Text

                If lngComments > 0 Then
                    txtCharCount = 1
                    'sql = "Select Max(SO_CMNT.SEQ#), so_cmnt.so_seq_num from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & del_doc_num & "'"
                    'sql = sql & " AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD "
                    'sql = sql & "AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM GROUP BY SO_CMNT.SO_SEQ_NUM"

                    'objsql = DisposablesManager.BuildOracleCommand(sql, conn)
                    'MyDatareader = objsql.ExecuteReader()
                    Dim cmntKey = New OrderUtils.WrDtStoreSeqKey
                    cmntKey.wrDt = soDatRow("SO_WR_DT")
                    cmntKey.storeCd = soDatRow("SO_STORE_CD")
                    cmntKey.seqNum = soDatRow("SO_SEQ_NUM")
                    lngSEQNUM = OrderUtils.getNextSoCmntSeq(cmntKey, cmd)
                    strSO_SEQ_NUM = cmntKey.seqNum

                    ' Daniela 22 Oct, 2014
                    Dim str_wr_dt As String = txt_wr_dt.Text
                    Dim str_del_doc_num = ""
                    Dim str_commentType As String = "S"

                    LeonsBiz.leons_insert_cmnt(str_wr_dt, so_store_cd, strSO_SEQ_NUM, lngSEQNUM, strComments, str_del_doc_num, str_commentType)  'lucy comments

                End If
            End If

            If Not Session("dcomments") Is Nothing Then
                'Insert Sales "D" Comments
                strComments = Session("dcomments").ToString
                lngComments = Len(strComments)
                Dim del_doc_num, cust_cd As String

                del_doc_num = txt_del_doc_num.Text
                cust_cd = txt_Cust_cd.Text

                If lngComments > 0 Then
                    txtCharCount = 1

                    ' Daniela get new seq to prevent dulicate
                    'If lngSEQNUM < 1 Then    ' if there were no sales comments then need to get, otherwise already have
                    Dim cmntKey = New OrderUtils.WrDtStoreSeqKey
                    cmntKey.wrDt = soDatRow("SO_WR_DT")
                    cmntKey.storeCd = soDatRow("SO_STORE_CD")
                    cmntKey.seqNum = soDatRow("SO_SEQ_NUM")
                    lngSEQNUM = OrderUtils.getNextSoCmntSeq(cmntKey, cmd)
                    strSO_SEQ_NUM = cmntKey.seqNum
                    'End If

                    ' Daniela 22 Oct, 2014
                    Dim str_wr_dt As String = txt_wr_dt.Text
                    Dim str_del_doc_num = ""
                    Dim str_commentType As String = "D"
                    LeonsBiz.leons_insert_cmnt(str_wr_dt, so_store_cd, strSO_SEQ_NUM, lngSEQNUM, strComments, str_del_doc_num, str_commentType)  'DB added to D type also

                End If
            End If

            If Not Session("arcomments") Is Nothing Then
                'Insert A/R Comments
                strComments = Session("arcomments").ToString
                lngComments = Len(strComments)

                If lngComments > 0 Then
                    sql = "Select Max(SEQ#) from CUST_CMNT where CUST_CD = '" & txt_Cust_cd.Text & "'"

                    cmd.CommandText = sql
                    MyDatareader = DisposablesManager.BuildOracleDataReader(cmd)


                    If MyDatareader.Read() Then
                        If MyDatareader.IsDBNull(0) = False Then
                            lngSEQNUM = MyDatareader(0)
                            lngSEQNUM = lngSEQNUM + 1
                        Else
                            lngSEQNUM = 1
                        End If
                    Else
                        lngSEQNUM = 1
                    End If

                    txtCharCount = 1

                    Do Until txtCharCount > lngComments

                        strMidCmnts = Mid(strComments, txtCharCount, 50)

                        strMidCmnts = Replace(strMidCmnts, "'", "")

                        sqlQueryBuilder.Length = 0

                        sqlQueryBuilder.Append("insert into CUST_CMNT (CUST_CD, SEQ#, CMNT_DT, TEXT, EMP_CD_OP) Values ('")
                        sqlQueryBuilder.Append(txt_Cust_cd.Text).Append("','").Append(lngSEQNUM).Append("',").Append("TO_DATE('").Append(FormatDateTime(Now(), DateFormat.ShortDate)).Append("','mm/dd/RRRR')").Append(" ,'")
                        sqlQueryBuilder.Append(strMidCmnts).Append("','").Append(Session("EMP_CD")).Append("')")
                        sql = UCase(sqlQueryBuilder.ToString)

                        cmd.CommandText = sql

                        cmd.ExecuteNonQuery()

                        txtCharCount = txtCharCount + 50
                        lngSEQNUM = lngSEQNUM + 1
                    Loop
                End If
            End If

            Session("scomments") = System.DBNull.Value
            Session("dcomments") = System.DBNull.Value
            Session("arcomments") = System.DBNull.Value
            ' Daniela remove the text when done
            txt_ar_comments.Text = ""

            ' Daniela add try
        Catch

            Session("scomments") = System.DBNull.Value
            Session("dcomments") = System.DBNull.Value
            Session("arcomments") = System.DBNull.Value

            'Throw
            Throw New Exception("Errors were encountered saving SOM+ comments.")
        End Try
    End Sub

    Public Function Determine_Taxability(ByVal itm_tp_cd As String)

        Determine_Taxability = "N"

        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        If itm_tp_cd = "PKG" Or itm_tp_cd = "NLN" Then
            Determine_Taxability = "N"
            Exit Function
        End If

        If itm_tp_cd = "INV" Then
            Determine_Taxability = "Y"
            Exit Function
        End If

        sql = "select inventory from itm_tp where itm_tp_cd='" & itm_tp_cd & "' and inventory='Y'"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Determine_Taxability = "Y"
                conn.Close()
                Exit Function
            End If
            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
        End Try

        Dim TAT_QUERY As String = ""
        Select Case itm_tp_cd
            Case "LAB"
                TAT_QUERY = "LAB_TAX"
            Case "FAB"
                TAT_QUERY = "FAB_TAX"
            Case "NLT"
                TAT_QUERY = "NLT_TAX"
            Case "WAR"
                TAT_QUERY = "WAR_TAX"
            Case "PAR"
                TAT_QUERY = "PARTS_TAX"
            Case "MIL"
                TAT_QUERY = "MILEAGE_TAX"
            Case Else
                Determine_Taxability = "N"
                conn.Close()
                Exit Function
        End Select

        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
        sql = sql & "FROM TAT, TAX_CD$TAT "
        sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
        sql = sql & "AND TAX_CD$TAT.TAX_CD='" & lbl_tax_cd.Text & "' "
        sql = sql & "AND TAT." & TAT_QUERY & " = 'Y' "
        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                Determine_Taxability = "Y"
                conn.Close()
                Exit Function
            End If
            MyDataReader.Close()
        Catch
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Function

    Private Function AddPackages(addPkgCmpt As CreatePkgCmpntLnReq, Optional ByRef errorMessage As String = "") As Boolean
        Dim showWarrPopup As Boolean = False
        ' create the object for header and transaction info     
        Dim headerInfo As SoHeaderDtc = OrderUtils.GetSoHeader(Session.SessionID.ToString.Trim,
                                                             addPkgCmpt.slsp1, addPkgCmpt.slsp2,
                                                             addPkgCmpt.pctOfSale1, addPkgCmpt.pctOfSale2,
                                                             Session("tet_cd"), Session("TAX_CD"),
                                                             Session("STORE_ENABLE_ARS"), Session("ZONE_CD"),
                                                             Session("PD"), Session("pd_store_cd"),
                                                             Session("cash_carry"))

        ' TODO - TAX FIX - need to use the correct rates but we need tax date to calc
        '  Call SetTxPcts(tempItmReq.soHdrInf.soWrDt, tempItmReq.soHdrInf.taxCd).itmTpPcts
        If Session("TAX_RATE") IsNot Nothing Then
            Dim taxRate As Double = Session("TAX_RATE")
            headerInfo.taxRates.lin = taxRate
            headerInfo.taxRates.fab = taxRate
            headerInfo.taxRates.lab = taxRate
            headerInfo.taxRates.mil = taxRate
            headerInfo.taxRates.nlt = taxRate
            headerInfo.taxRates.nln = 0.0
            headerInfo.taxRates.par = taxRate
            headerInfo.taxRates.war = taxRate
            headerInfo.taxRates.svc = taxRate
            headerInfo.taxRates.pkg = taxRate
            headerInfo.taxRates.dnr = 0.0
            headerInfo.taxRates.del = taxRate
            headerInfo.taxRates.setup = taxRate
        End If

        ' create pkg components in temp_itm
        Dim tempLines As DataTable = theSalesBiz.AddPkgCmpntsToTempItm(headerInfo,
                                        addPkgCmpt.pkgSku, Session.SessionID.ToString.Trim, txt_store_cd.Text.Trim, errorMessage)

        'If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT Then
        '    theSalesBiz.addSplitDataToSession(pkgHeadderLn, tempLines)
        'End If
        'now that the package components have been inserted into the temp-itm table, retrieve them
        ' and add them to the dataset lines cached in the session.

        ' Daniela get URL
        ' Dim pkgLines As DataSet = theSalesBiz.GetTempItemDataForPkgCmpntsUrl(Session.SessionID.ToString())   25-may-15 lucy commented for compiling
        Dim pkgLines As DataSet = theSalesBiz.GetTempItemDataForPkgCmpnts(Session.SessionID.ToString())
        Dim new_line_no As Double = addPkgCmpt.nextLnNum
        Dim lineDatSet As DataSet = Session("DEL_DOC_LN#")
        Dim lineRow As DataRow
        Dim ItemList As ArrayList = New ArrayList()
        Dim qty As Integer

        For Each line In pkgLines.Tables(0).Rows
            qty = 1
            ItemList.Clear()
            lineRow = lineDatSet.Tables(0).NewRow
            lineRow("DEL_DOC_NUM") = txt_del_doc_num.Text
            'lineRow("ITM_URL") = line("ITM_URL").ToString ' Daniela added
            lineRow("DEL_DOC_LN#") = new_line_no
            lineRow("ITM_CD") = line("ITM_CD").ToString
            ItemList.Add(line("ITM_CD").ToString)
            lineRow("COMM_CD") = line("COMM_CD").ToString
            lineRow("QTY") = qty
            lineRow("UNIT_PRC") = line("RET_PRC").ToString
            lineRow("itm_ret_prc") = line("RET_PRC").ToString
            lineRow("ORIG_PRC") = line("RET_PRC").ToString
            lineRow("MANUAL_PRC") = line("RET_PRC").ToString
            lineRow("VE_CD") = line("VE_CD").ToString
            lineRow("ITM_TP_CD") = line("ITM_TP_CD").ToString
            lineRow("MNR_CD") = line("MNR_CD").ToString
            lineRow("VOID_FLAG") = "N"
            lineRow("VSN") = Replace(line("VSN").ToString, "'", "''")
            lineRow("DES") = Replace(line("DES").ToString, "'", "''")
            lineRow("WARRANTABLE") = line("WARRANTABLE").ToString
            lineRow("INVENTORY") = line("INVENTORY").ToString
            lineRow("delivery_points") = Double.TryParse(line("DEL_PTS").ToString, 0)
            lineRow("NEW_LINE") = "Y"
            lineRow("PACKAGE_PARENT") = addPkgCmpt.pkgLnNum  ' This cannot work for multiple packages if this is the only line number
            If line("RET_PRC").ToString.ToUpper().Equals("INV") Then
                lineRow("PICKED") = "Z"
            Else
                lineRow("PICKED") = "X"
            End If
            lineRow("VOID_FLAG") = "N"
            lineRow("SHU") = "N"
            lineRow("LN_SLSP1") = addPkgCmpt.slsp1
            lineRow("LN_SLSP2") = addPkgCmpt.slsp2
            lineRow("LN_SLSP1_PCT") = addPkgCmpt.pctOfSale1
            lineRow("LN_SLSP2_PCT") = addPkgCmpt.pctOfSale2
            lineRow("SORTCODES") = theSkuBiz.GetItemSortCodesCSV(ItemList)
            lineRow("TYPECODE") = "CMP"     'This is to identify component object'
            lineDatSet.Tables(0).Rows.Add(lineRow)
            new_line_no = new_line_no + 1

            If Integer.TryParse(line("QTY"), qty) AndAlso qty > 1 Then
                For index = 1 To qty - 1
                    Dim newRow As DataRow = lineDatSet.Tables(0).NewRow()
                    newRow.ItemArray = TryCast(lineRow.ItemArray.Clone(), Object())
                    newRow("DEL_DOC_LN#") = new_line_no
                    lineDatSet.Tables(0).Rows.Add(newRow)
                    new_line_no = new_line_no + 1
                Next
            End If

            hidEnteredSKUs.Value = hidEnteredSKUs.Value & "," & line("ITM_CD")
        Next
        Session("DEL_DOC_LN#") = lineDatSet
        ' MM-6305
        ' Once the package components are added to the dataset then these items need to be deleted from the temp_itm table
        ' this was causing an issue when you add a package which has some components these items will be inserted to Temp_Itm, next time add a package sku which does not
        ' have any component(s) but still it was getting previously added package components from temp_itm from the below code which is situated above
        ' Dim pkgLines As DataSet = theSalesBiz.GetTempItemDataForPkgCmpnts(Session.SessionID.ToString())
        If (pkgLines.Tables(0).Rows.Count > 0) Then
            theSalesBiz.DeleteTempItems(Session.SessionID.ToString())
            Dim drFilter() As DataRow = pkgLines.Tables(0).Select("WARRANTABLE = 'Y' AND INVENTORY = 'Y'")
            For Each drFil As DataRow In drFilter
                If Not showWarrPopup Then showWarrPopup = SalesUtils.hasWarranty(drFil("ITM_CD"))
            Next
        End If

        Return showWarrPopup
    End Function

    Protected Sub pmt_btn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles pmt_btn.Click
        ASPxPopupControl2.ContentUrl = "SalesOrderPayments.aspx?del_doc_num=" & txt_del_doc_num.Text & "&ord_tp_cd=" & txt_ord_tp_cd.Text
        ASPxPopupControl2.ShowOnPageLoad = True
    End Sub

    Private Sub RemoveStoreLocationInfo()
        ' MM - 5672
        ' When CRM is about to VOID then removing the store, location fields for the grid
        ' I will update the store and location back to the dataset and it will bind to the grid
        If (AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text) Then
            ' If I have updated the values then I am going to revert it
            ' Some times user only queries a CRM but may save the changes or VOID the CRM in that case
            ' no need to call the below code to revert the changes since I would have not updated the INV_XREF values in these cases
            ' INV_XREF values will only be updated when user would like to Finalize the sales order           
            Dim soLnsDataSet As DataSet = Session("DEL_DOC_LN#")
            ' Resetting the store and location to dataset
            If ((soLnsDataSet) IsNot Nothing) And (soLnsDataSet.Tables(0).Rows.Count > 0) Then
                For Each dataRow As DataRow In soLnsDataSet.Tables(0).Rows
                    ' When changed from Finalize to Void/Change Order Status then we need to disable the
                    ' Store and Location field and need to set IsStoreFromInvXref = False
                    If (dataRow("VOID_FLAG") IsNot DBNull.Value) AndAlso ("N".Equals(dataRow("VOID_FLAG"))) Then
                        dataRow("STORE_CD") = String.Empty
                        dataRow("LOC_CD") = String.Empty
                        dataRow("IsStoreFromInvXref") = False
                    End If
                Next
            End If
            Session("DEL_DOC_LN#") = soLnsDataSet
            bindgrid(soLnsDataSet)
        End If
    End Sub
    Protected Sub cbo_stat_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_stat_cd.SelectedIndexChanged

        Dim strValue As String
        Dim strErr As String = "Y"
        Dim strDelDocNum As String
        strValue = cbo_stat_cd.SelectedValue 'lucy, 11-May-16
        If strValue = "F" Then
            strDelDocNum = txt_del_doc_num.Text.Trim
            strErr = lucy_ctl_final(strDelDocNum)
            If strErr <> "Y" Then
                ' lbl_desc.Text = strErr
                lbl_final.Text = strErr
                Exit Sub
            End If
        End If

        If strValue = "V" Then
            strDelDocNum = txt_del_doc_num.Text.Trim
            strErr = lucy_ctl_void(strDelDocNum)
            If strErr <> "Y" Then
                ' lbl_desc.Text = strErr
                ' strErr = "Change not allowed. Lines have been picked or checked. The user will need to unpick the affected lines for the above changes to be allowed."

                lbl_final.Text = strErr
                Exit Sub
            End If
        End If
        If AppConstants.Order.STATUS_OPEN = txt_Stat_cd.Text Then

            'NOTE: When processing CRMs, reservations can only be made if user has selected
            ' to FINALIZE the order, therefore IF at least one line is found to have a
            ' STORE or LOCATION, unless it is being changed to VOID, it must be FINAL
            If (AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text AndAlso
                AppConstants.Order.STATUS_FINAL <> cbo_stat_cd.SelectedValue AndAlso
                AppConstants.Order.STATUS_VOID <> cbo_stat_cd.SelectedValue AndAlso
                HasStoreOrLocation()) Then
                cbo_stat_cd.SelectedValue = AppConstants.Order.STATUS_FINAL
            End If


            ASPxRoundPanel1.Visible = True
            If cbo_stat_cd.SelectedValue = "C" Then
                final_store_cd.Enabled = False
                txt_fv_dt.Enabled = False
                cbo_void_cause_cd.Enabled = False
                cbo_void_cause_cd.Visible = False
                RemoveStoreLocationInfo()
            ElseIf cbo_stat_cd.SelectedValue = AppConstants.Order.STATUS_VOID Then
                If cbo_related_docs.Visible = True Then
                    Dim dependentRows As Boolean = theSalesBiz.ValidateSalesOrder(txt_del_doc_num.Text.Trim)
                    If dependentRows Then
                        ViewState("SaleVoid") = True
                        Dim errorMsg As String = ""
                        Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
                        ucMsgPopup.DisplayConfirmMsg("This action will Void\Remove the warranty in another portion of the sale", "", Resources.POSMessages.MSG0049)
                        Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
                        msgComponent.ForeColor = System.Drawing.ColorTranslator.FromHtml("#9F6000")
                        msgPopup.ShowOnPageLoad = True
                    Else
                        StatChangeToVoid()
                    End If
                Else
                    StatChangeToVoid()
                End If

            Else
                'MM-7088
                GetStoreLocationData()
            End If
            btn_Save.Enabled = True
        End If
    End Sub
    ''' <summary>
    ''' This method decides whether to show the manager approval popup for order finalization.
    ''' </summary>
    ''' <param name="requestFor">Parameter identify's the popup is for approval of what?</param>

    Private Sub ProcessFinalizationRequest(ByVal requestFor As String)
        Dim allowCODParameter As String = SysPms.allowCODBalance
        Dim isPaidInFull As Boolean = IsSalePaidInFull()
        If isPaidInFull = True Then
            'Do Nothing, Continue with the finalization
            SaveSalesOrder()
        ElseIf allowCODParameter.Equals("W") Then 'Means Need approval, bringup the approval popup
            ucMgrOverride.ApprovalFor = requestFor
            showApprovalPopup(Resources.POSMessages.MSG0063, ORDERSAVE) ' "Approval required for finalizing the Order. Order is not paid in full!")
        ElseIf allowCODParameter.Equals("N") Then ' Never allow finalzation if COD' Check if the order has balance against it
            'Display Error message
            lbl_desc.Text = If(lbl_desc.Text.Trim.Equals(String.Empty), "", "<BR/>") & Resources.POSMessages.MSG0064 ' " The Order must be Paid in Full to finalize."
            cbo_stat_cd.SelectedIndex = 0
        Else
            SaveSalesOrder()
        End If
    End Sub
    Protected Sub StatChangeToVoid()
        ViewState("SaleVoid") = Nothing
        If cbo_void_cause_cd.SelectedValue = "" Then
            PopulateVoidCause()
        End If
        cbo_void_cause_cd.Enabled = True
        cbo_void_cause_cd.Visible = True
        final_store_cd.Enabled = True
        txt_fv_dt.Enabled = True
        RemoveStoreLocationInfo()
    End Sub

    Protected Sub cbo_related_docs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_related_docs.SelectedIndexChanged

        If cbo_related_docs.SelectedValue <> txt_del_doc_num.Text.ToString Then
            'If the current user has write access then perform the below operation
            'If the current has has read-only permission then no need to perform below activity            
            If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
                Dim accessInfo As New AccessControlBiz()
                accessInfo.DeleteRecord("SalesOrderMaintenance.aspx")
            End If
            Response.Redirect("SalesOrderMaintenance.aspx?del_doc_num=" & cbo_related_docs.SelectedValue.ToString & "&query_returned=Y")
        End If

    End Sub

    Protected Sub GridView1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView1.ItemDataBound

        Dim footerIndex As Integer = GridView1.Controls(0).Controls.Count - 1
        Dim txt_new_itm_cd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_itm_cd"), TextBox)
        Dim txt_new_store_cd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_store_cd"), TextBox)
        Dim txt_store_cd As TextBox = CType(e.Item.FindControl("store_cd"), TextBox)
        Dim txt_loc_cd As TextBox = CType(e.Item.FindControl("loc_cd"), TextBox)
        Dim txt_new_loc_cd As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_loc_cd"), TextBox)
        Dim txt_new_unit_prc As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_unit_prc"), TextBox)
        Dim txt_new_qty As TextBox = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("txt_new_qty"), TextBox)
        Dim btn_add_item As Button = DirectCast(GridView1.Controls(0).Controls(footerIndex).FindControl("btn_add_item"), Button)
        Dim lbl_PO As Label = CType(e.Item.FindControl("lbl_PO"), Label)
        Dim lbl_IST As Label = CType(e.Item.FindControl("lbl_IST"), Label)

        If Not IsNothing(txt_new_qty) And txt_Stat_cd.Text <> "O" Then
            txt_new_itm_cd.Enabled = False
            txt_new_store_cd.Enabled = False
            txt_new_loc_cd.Enabled = False
            txt_new_unit_prc.Enabled = False
            txt_new_qty.Enabled = False
            btn_add_item.Enabled = False
        End If
        Dim txt_qty As TextBox = CType(e.Item.FindControl("qty"), TextBox)
        If Not IsNothing(txt_qty) Then
            If e.Item.Cells(SoLnGrid.ITM_TP_CD).Text = "PKG" Then
                txt_qty.Enabled = False
            End If

            Dim unit_prc As TextBox = CType(e.Item.FindControl("unit_prc"), TextBox)
            If txt_Stat_cd.Text <> "O" Then
                unit_prc.Enabled = False
                txt_qty.Enabled = False
            ElseIf IsDate(e.Item.Cells(SoLnGrid.ADDON_WR_DT).Text) Then
                If Convert.ToDateTime(e.Item.Cells(SoLnGrid.ADDON_WR_DT).Text) <= Convert.ToDateTime(txt_SHU.Text) Then
                    unit_prc.Enabled = True
                    txt_qty.Enabled = True
                End If
            ElseIf Convert.ToDateTime(txt_wr_dt.Text) <= Convert.ToDateTime(txt_SHU.Text) And e.Item.Cells(SoLnGrid.NEW_LINE).Text = "N" Then
                unit_prc.Enabled = False
                txt_qty.Enabled = False
            End If
            If e.Item.Cells(SoLnGrid.ITM_TP_CD).Text = "PKG" Then
                unit_prc.Enabled = False
            End If
            If Not IsNothing(txt_store_cd.Text) Then
                If txt_store_cd.Text & "" <> "" Then
                    txt_qty.Enabled = False
                End If
            End If
        End If
        If e.Item.Cells(SoLnGrid.ITM_CD).Text & "" <> "" AndAlso e.Item.Cells(SoLnGrid.ITM_CD).Text <> "&nbsp;" Then
            Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String

            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn2.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand
            Dim TheFile As System.IO.FileInfo
            Dim File_Path As String = ""

            sql = "select major.mjr_cd || '_' || major.des || '/' || minor.mnr_cd || '_' || minor.des || " &
                          "   '/' || item.ve_cd || '_' || item.itm_cd || '_WEB.jpg' " &
                          " from itm item, inv_mnr minor, inv_mjr major, ve vendor where item.mnr_cd = minor.mnr_cd and minor.mjr_cd = major.mjr_cd and item.ve_cd = vendor.ve_cd and item.itm_cd='" & e.Item.Cells(SoLnGrid.ITM_CD).Text & "'"
            'Set SQL OBJECT 
            objsql = DisposablesManager.BuildOracleCommand(sql, conn2)

            Try
                'Execute DataReader 
                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If Mydatareader.Read() Then
                    File_Path = Mydatareader.Item(0).ToString
                    File_Path = Replace(File_Path, " ", "_")
                    TheFile = New System.IO.FileInfo(Server.MapPath("\external_images\") & File_Path)
                End If
                Mydatareader.Close()
            Catch
                conn2.Close()
            End Try
            conn2.Close()

            ' Daniela comment
            Try
                ' Daniela default picture if img_picture found
                If Not Session("DEL_DOC_LN#") Is Nothing Then
                    Dim img_picture As Image = CType(e.Item.FindControl("img_picture"), Image)
                    If isEmpty(img_picture.ImageUrl) Then
                        img_picture.ImageUrl = "~/images/image_not_found.jpg"
                    End If
                End If
                '    'sabrina modified
                '    'If TheFile.Exists Then
                '    '    Dim img_picture As Image = CType(e.Item.FindControl("img_picture"), Image)
                '    '    img_picture.ImageUrl = "~/external_images/" & File_Path
                '    'Else
                '    Dim image_URL As String = HBCG_Utils.Find_Merchandise_Images(e.Item.Cells(SoLnGrid.ITM_CD).Text)
                '    Dim img_picture As Image = CType(e.Item.FindControl("img_picture"), Image)
                '    If image_URL.ToString & "" <> "" Then
                '        img_picture.ImageUrl = image_URL
                '    Else
                '        img_picture.ImageUrl = "~/images/image_not_found.jpg"
                '    End If
                '    'End If
            Catch

            End Try


            If GM_Sec & "" = "" Then GM_Sec = Find_Security("GMP_ACC", Session("emp_cd"))

            Dim img_gm As ImageButton = CType(e.Item.FindControl("img_gm"), ImageButton)
            If Not IsNothing(img_gm) Then
                If GM_Sec = "Y" Then
                    img_gm.Visible = True
                Else
                    img_gm.Visible = False
                End If
            End If
        End If

        'Display IST Information for each line
        If Not IsNothing(lbl_IST) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String

            conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            sql = "select ist.doc_num || ' - Line#' || ist_ln$so_ln.ist_ln# || '  (' || ist.transfer_dt || ')' as ist_desc,ist.doc_num, ist_ln$so_ln.ist_ln#, ist.transfer_dt from ist_ln$so_ln, ist "
            sql = sql & "where ist.ist_wr_dt=IST_LN$SO_LN.IST_WR_DT "
            sql = sql & "and IST.IST_STORE_CD= IST_LN$SO_LN.IST_STORE_CD "
            sql = sql & "and ist.ist_seq_num=IST_LN$SO_LN.IST_SEQ_NUM "
            sql = sql & "and ist.stat_cd='O' "
            sql = sql & "and ist_ln$so_ln.del_doc_num='" & txt_del_doc_num.Text & "' "
            sql = sql & "and ist_ln$so_ln.del_doc_ln#=" & e.Item.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try

                Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
                'IT Request Session("OpenIST")
                If Mydatareader.Read() Then
                    'IT Request 2289
                    If IsNumeric(Session("OpenIST")) Then
                        Session("OpenIST") = Session("OpenIST") + 1
                    Else : Session("OpenIST") = 1
                    End If
                    'End IT Request 2289
                    lbl_IST.Text = "IST: " & Mydatareader.Item("IST_DESC").ToString
                    'If the line is void then we can still finalize if attached to an IST
                    If e.Item.Cells(SoLnGrid.VOID_FLAG).Text = "N" Then
                        Session("ISTfound") = "TRUE"
                    End If
                End If
                Mydatareader.Close()
            Catch
                Throw
                conn.Close()
            End Try
            conn.Close()
        End If

        If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_MCR Or txt_ord_tp_cd.Text = AppConstants.Order.TYPE_MDB Then
            Dim img_picture As ImageButton = CType(e.Item.FindControl("Image1"), ImageButton)
            Dim store_cd As TextBox = CType(e.Item.FindControl("store_cd"), TextBox)
            'Dim loc_cd As TextBox = CType(e.Item.FindControl("loc_cd"), TextBox)

            If Not IsNothing(img_picture) Then
                img_picture.Enabled = False
            End If
            If Not IsNothing(store_cd) Then
                store_cd.Enabled = False
            End If
            If Not IsNothing(txt_loc_cd) Then
                txt_loc_cd.Enabled = False
            End If
        End If

        Dim lblPickStat As Label = CType(e.Item.FindControl("lblPickStat"), Label)
        If Not IsNothing(lblPickStat) Then
            lblPickStat.Visible = False
        End If
        Dim lblMaxCred As Label = CType(e.Item.FindControl("lblMaxCred"), Label)
        If Not IsNothing(lblMaxCred) Then
            lblMaxCred.Visible = False
        End If
        Dim realDataRow As Boolean = True
        If (e.Item.Cells(SoLnGrid.ITM_CD).Text & "").isEmpty OrElse e.Item.Cells(SoLnGrid.ITM_CD).Text = "&nbsp;" Then
            ' using existance of ITM_CD value to determine if this a real data row or header/footer
            realDataRow = False
        End If
        'If the order is open then
        '   1) call the routine to retrieve all the cashiers/salespersons from EMP, EMP_SLSP,
        '      tables and store it in the class-level dataset 'slsp_ds'  &set the values on the drop-down and textfield
        '   2) Process to display RF pick status 
        '   3) process to display max credit/qty avail on CRM/MCR docs

        'Repopulate the drop down boxes based on any previous stored data
        Dim empcd As String = ""
        'Daniela Repopulate the drop down boxes based on any previous stored data
        If Session("slsp1") & "" <> "" Then
            empcd = Session("slsp1")
        Else
            empcd = UCase(Session("emp_cd"))
        End If

        If txt_Stat_cd.Text = "O" Then

            ' TODO - why are we going thru this logic with "&nbsp;" at all - once before lines, once after - that would help perf not to if not needed
            If "Y".Equals(e.Item.Cells(SoLnGrid.INVENTORY).Text) AndAlso (Not IsNothing(lblPickStat)) AndAlso
                txt_store_cd.Text.isNotEmpty AndAlso txt_loc_cd.Text.isNotEmpty AndAlso (Not "Y".Equals(e.Item.Cells(SoLnGrid.NEW_LINE).Text & "")) AndAlso
                realDataRow AndAlso
                "Y".Equals(theSystemBiz.GetSysPm("WHSRF1", 18, 1)) AndAlso "N".Equals(e.Item.Cells(SoLnGrid.INV_PICK_VOID).Text) Then
                Dim s As String = ViewState("PTS_RF")

                'Start MM-7164
                'once warranty popup appears, the view state detail are not available(warranty popup is separate aspx page), this was causing application error
                ' to overcome this, re-initializing  view state again.
                If IsNothing(ViewState("PTS_RF")) Then
                    ViewState("PTS_RF") = theInvBiz.GetPTSRFStatus()
                End If
                'End MM-7164

                ' called routine confirms RF is enabled and is RF store
                lblPickStat.Text = theInvBiz.GetRfPickStatus(txt_del_doc_num.Text, e.Item.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text,
                                                                  e.Item.Cells(SoLnGrid.ITM_CD).Text, txt_store_cd.Text, ViewState("PTS_RF").ToString)
                lblPickStat.Visible = True
            End If

            If realDataRow AndAlso Not "Y".Equals(e.Item.Cells(SoLnGrid.VOID_FLAG).Text) AndAlso OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then
                'If txt_ord_tp_cd.Text = "CRM" Then
                'lblMaxCred.Text = "Max Qty/Credit = " & e.Item.Cells(SoLnGrid.MAX_CRM_QTY).Text + " / " & FormatCurrency(CDbl(e.Item.Cells(SoLnGrid.MAX_RET_PRC).Text), 2)
                lblMaxCred.Text = "Max Qty/Credit = " & e.Item.Cells(SoLnGrid.MAX_CRM_QTY).Text + " / " & If(IsNumeric(e.Item.Cells(SoLnGrid.MAX_RET_PRC).Text), FormatCurrency(CDbl(e.Item.Cells(SoLnGrid.MAX_RET_PRC).Text), 2), e.Item.Cells(SoLnGrid.MAX_RET_PRC).Text)
                lblMaxCred.Visible = True
            End If

            If ChkBoxViewSlspersons.Checked Then
                If slsp_ds.Tables.Count < 1 Then slsp_ds = LeonsBiz.lucy_RetrieveSalespersons(empcd)

                Dim cbo_slsp1 As DropDownList = CType(e.Item.FindControl("cbo_slsp1"), DropDownList)
                Dim cbo_slsp2 As DropDownList = CType(e.Item.FindControl("cbo_slsp2"), DropDownList)
                Dim txt_slsp1_pct As TextBox = CType(e.Item.FindControl("txtLineSlsp1Pct"), TextBox)
                Dim txt_slsp2_pct As TextBox = CType(e.Item.FindControl("txtLineSlsp2Pct"), TextBox)

                'Set the salesperson info
                If Not IsNothing(cbo_slsp1) Then
                    cbo_slsp1.DataSource = slsp_ds
                    cbo_slsp1.DataValueField = "EMP_CD"
                    cbo_slsp1.DataTextField = "FULLNAME"
                    cbo_slsp1.DataBind()
                    'cbo_slsp1.Items.Insert(0, New ListItem("Please Select A Salesperson", "0"))

                    cbo_slsp2.DataSource = slsp_ds
                    cbo_slsp2.DataValueField = "EMP_CD"
                    cbo_slsp2.DataTextField = "FULLNAME"
                    cbo_slsp2.DataBind()
                    'MM-7366
                    cbo_slsp2.Items.Insert(0, New ListItem("Please Select A Salesperson", "0"))

                    If e.Item.Cells(SoLnGrid.LN_SLSP1).Text & "" <> "" Then
                        e.Item.Cells(SoLnGrid.LN_SLSP1).Text = e.Item.Cells(SoLnGrid.LN_SLSP1).Text.Replace("&nbsp;", "")
                        e.Item.Cells(SoLnGrid.LN_SLSP_PCT1).Text = e.Item.Cells(SoLnGrid.LN_SLSP_PCT1).Text.Replace("&nbsp;", "")
                        cbo_slsp1.SelectedValue = e.Item.Cells(SoLnGrid.LN_SLSP1).Text
                        txt_slsp1_pct.Text = FormatNumber(CDbl(e.Item.Cells(SoLnGrid.LN_SLSP_PCT1).Text), 2)
                    End If

                    If e.Item.Cells(SoLnGrid.LN_SLSP2).Text & "" <> "" Then
                        e.Item.Cells(SoLnGrid.LN_SLSP2).Text = e.Item.Cells(SoLnGrid.LN_SLSP2).Text.Replace("&nbsp;", "")
                        e.Item.Cells(SoLnGrid.LN_SLSP_PCT2).Text = e.Item.Cells(SoLnGrid.LN_SLSP_PCT2).Text.Replace("&nbsp;", "")
                        cbo_slsp2.SelectedValue = e.Item.Cells(SoLnGrid.LN_SLSP2).Text
                        txt_slsp2_pct.Text = FormatNumber(CDbl(e.Item.Cells(SoLnGrid.LN_SLSP_PCT2).Text), 2)
                    End If
                End If
            End If
        Else
            If ChkBoxViewSlspersons.Checked Then
                If slsp_ds.Tables.Count < 1 Then slsp_ds = LeonsBiz.lucy_RetrieveSalespersons(empcd)

                Dim cbo_slsp1 As DropDownList = CType(e.Item.FindControl("cbo_slsp1"), DropDownList)
                Dim cbo_slsp2 As DropDownList = CType(e.Item.FindControl("cbo_slsp2"), DropDownList)
                Dim txt_slsp1_pct As TextBox = CType(e.Item.FindControl("txtLineSlsp1Pct"), TextBox)
                Dim txt_slsp2_pct As TextBox = CType(e.Item.FindControl("txtLineSlsp2Pct"), TextBox)

                'Set the salesperson info
                If Not IsNothing(cbo_slsp1) Then
                    cbo_slsp1.DataSource = slsp_ds
                    cbo_slsp1.DataValueField = "EMP_CD"
                    cbo_slsp1.DataTextField = "FULLNAME"
                    cbo_slsp1.DataBind()
                    'cbo_slsp1.Items.Insert(0, New ListItem("Please Select A Salesperson", "0"))


                    cbo_slsp2.DataSource = slsp_ds
                    cbo_slsp2.DataValueField = "EMP_CD"
                    cbo_slsp2.DataTextField = "FULLNAME"
                    cbo_slsp2.DataBind()
                    'MM-7366
                    cbo_slsp2.Items.Insert(0, "Please Select A Salesperson")
                    cbo_slsp2.Items.FindByText("Please Select A Salesperson").Value = ""
                    cbo_slsp2.SelectedIndex = 0

                    If e.Item.Cells(SoLnGrid.LN_SLSP1).Text & "" <> "" Then
                        e.Item.Cells(SoLnGrid.LN_SLSP1).Text = e.Item.Cells(SoLnGrid.LN_SLSP1).Text.Replace("&nbsp;", "")
                        e.Item.Cells(SoLnGrid.LN_SLSP_PCT1).Text = e.Item.Cells(SoLnGrid.LN_SLSP_PCT1).Text.Replace("&nbsp;", "")
                        cbo_slsp1.SelectedValue = e.Item.Cells(SoLnGrid.LN_SLSP1).Text
                        txt_slsp1_pct.Text = e.Item.Cells(SoLnGrid.LN_SLSP_PCT1).Text
                    End If

                    If e.Item.Cells(SoLnGrid.LN_SLSP2).Text & "" <> "" Then
                        e.Item.Cells(SoLnGrid.LN_SLSP2).Text = e.Item.Cells(SoLnGrid.LN_SLSP2).Text.Replace("&nbsp;", "")
                        e.Item.Cells(SoLnGrid.LN_SLSP_PCT2).Text = e.Item.Cells(SoLnGrid.LN_SLSP_PCT2).Text.Replace("&nbsp;", "")
                        cbo_slsp2.SelectedValue = e.Item.Cells(SoLnGrid.LN_SLSP2).Text
                        txt_slsp2_pct.Text = e.Item.Cells(SoLnGrid.LN_SLSP_PCT2).Text
                    End If
                End If
            End If
        End If

        If ChkBoxViewSlspersons.Checked = True Then
            Dim slsp_div As HtmlGenericControl = CType(e.Item.FindControl("slsp_section"), HtmlGenericControl)
            If Not IsNothing(slsp_div) Then
                slsp_div.Visible = ChkBoxViewSlspersons.Checked
            End If
        End If

        Dim lblAvailResult As Label = CType(e.Item.FindControl("lblAvailResults"), Label)
        If chk_calc_avail.Checked = True Then
            Dim store_cd As TextBox = CType(e.Item.FindControl("store_cd"), TextBox)
            'Dim loc_cd As TextBox = CType(e.Item.FindControl("loc_cd"), TextBox)
            If Not IsNothing(store_cd) Then
                If store_cd.Text & "" = "" Then
                    Dim request As CalcAvailRequestDtc = InventoryUtils.GetRequestForCalcAvail(
                                                                    e.Item.Cells(SoLnGrid.ITM_CD).Text,
                                                                    txt_qty.Text,
                                                                    lbl_curr_dt.Text,
                                                                    txt_store_cd.Text,
                                                                    cbo_pd_store_cd.Value,
                                                                    lbl_curr_zone.Text,
                                                                    cbo_PD.Value,
                                                                    txt_del_doc_num.Text,
                                                                    e.Item.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text)
                    lblAvailResult.Text = theInvBiz.CalcAvail(request)
                Else
                    lblAvailResult.Text = "Filled"
                End If
            End If
        End If
        'Applying security to disscount package.
        If Session("ord_tp_cd") = "SAL" Then
            If Not Session("DEL_DOC_LN#") Is Nothing Then
                If e.Item.ItemIndex >= 0 Then
                    Dim packageDtemp As DataTable = CType(Session("DEL_DOC_LN#"), DataSet).Tables(0)
                    Dim FilterText As String
                    Dim pkgDelDocLn As String
                    Dim pkgItm As String
                    Dim pkgCmp As String
                    Dim distinctPackageParent() As DataRow
                    Dim packgeComponetCheck As String = packageDtemp.Rows(e.Item.ItemIndex)("TYPECODE")
                    Dim txtRetPrc As TextBox = CType(e.Item.FindControl("unit_prc"), TextBox)
                    Dim txtQty As TextBox = CType(e.Item.FindControl("qty"), TextBox)
                    Dim btnReplace As ImageButton = CType(e.Item.FindControl("btnReplace"), ImageButton)
                    If packgeComponetCheck = "CMP" Then
                        If Not (packageDtemp.Columns.Contains("ISREPLACED")) Then
                            If e.Item.Cells(SoLnGrid.NEW_LINE).Text = "N" Then
                                pkgItm = e.Item.Cells(SoLnGrid.ITM_CD).Text.ToString().Trim()
                                FilterText = "[ITM_CD]='" + pkgItm + "'"
                                distinctPackageParent = packageDtemp.Select(FilterText)
                                pkgCmp = distinctPackageParent(0)("PKG_SOURCE").ToString
                                FilterText = "[ITM_CD]='" + pkgCmp + "' and [DEL_DOC_LN#] <" + e.Item.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text + ""
                            Else
                                If e.Item.Cells(SoLnGrid.NEW_LINE).Text = "Y" Then
                                    pkgDelDocLn = e.Item.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.ToString().Trim()
                                    FilterText = "[DEL_DOC_LN#]='" + pkgDelDocLn + "'"
                                    distinctPackageParent = packageDtemp.Select(FilterText)
                                    pkgCmp = distinctPackageParent(0)("PACKAGE_PARENT").ToString
                                    FilterText = "[DEL_DOC_LN#]='" + pkgCmp + "' and [DEL_DOC_LN#] <" + e.Item.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text + ""
                                End If
                            End If
                        Else
                            pkgItm = e.Item.Cells(SoLnGrid.ITM_CD).Text.ToString().Trim()
                            FilterText = "[ITM_CD]='" + pkgItm + "'"
                            distinctPackageParent = packageDtemp.Select(FilterText)
                            pkgCmp = distinctPackageParent(0)("PKG_SOURCE").ToString
                            FilterText = "[ITM_CD]='" + pkgCmp + "' and [DEL_DOC_LN#] <" + e.Item.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text + ""
                        End If
                        If Not pkgCmp.Equals("") Then
                            distinctPackageParent = packageDtemp.Select(FilterText)
                        End If
                        Dim prc As Double = 0
                        If distinctPackageParent.Length > 0 Then
                            prc = distinctPackageParent(distinctPackageParent.Length - 1)("UNIT_PRC").ToString
                        End If
                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                        txtRetPrc.Enabled = False
                        txtQty.Enabled = False
                        btnReplace.Enabled = False
                        Dim checkFlag As Boolean
                        'If the user is super user having all access
                        If prc > 0 Then
                            If CheckSecurity("", Session("EMP_CD")) Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = True
                                btnReplace.Enabled = True
                                checkFlag = True
                            Else
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                                btnReplace.Enabled = False
                            End If
                            If Not checkFlag Then
                                'The security "PKGCMPT_CHG" does not allow the changing of a SKU on a line.The line must be deleted or voided and then a new line must be added.
                                'And this security will not allow changes to the qty.
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG, Session("EMP_CD")) Then
                                    txtRetPrc.Enabled = False
                                    txtQty.Enabled = False
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                    btnReplace.Enabled = True
                                End If
                                'This security "PKGCMPT_CHG_QTY" will allow to change the qty alone. 
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG_QTY, Session("EMP_CD")) Then
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    txtQty.Enabled = False
                                End If
                                'This security "SOM_CHG_PRC" allows only to change retail price for any line except package sku if it is explored. 
                                ' This security "PKGCMPT_CHG_PRC" is to change the retail price of the component.This is on top of SOERET.
                                'The user must have ' both to be able to change the component price.
                                If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKGCMPT_CHG_PRC, Session("EMP_CD"))) Then
                                    txtRetPrc.Enabled = False
                                End If
                            End If
                        ElseIf prc = 0 Then
                            If CheckSecurity("", Session("EMP_CD")) Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = True
                                txtQty.Enabled = True
                                btnReplace.Enabled = True
                                checkFlag = True
                            Else
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                                btnReplace.Enabled = False
                            End If
                            If Not checkFlag Then
                                'The security "PKGCMPT_CHG" does not allow the changing of a SKU on a line.The line must be deleted or voided and then a new line must be added.
                                'And this security will not allow changes to the qty.
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG, Session("EMP_CD")) Then
                                    txtRetPrc.Enabled = False
                                    txtQty.Enabled = False
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                    btnReplace.Enabled = True
                                End If
                                'This security "PKGCMPT_CHG_QTY" will allow to change the qty alone. 
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG_QTY, Session("EMP_CD")) Then
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    txtQty.Enabled = True
                                End If
                                'This security "SOM_CHG_PRC" allows only to change retail price for any line except package sku if it is explored. 
                                ' This security "PKGCMPT_CHG_PRC" is to change the retail price of the component.This is on top of SOERET.
                                'The user must have ' both to be able to change the component price.
                                If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKGCMPT_CHG_PRC, Session("EMP_CD"))) Then
                                    txtRetPrc.Enabled = True
                                End If
                            End If
                        End If

                    ElseIf packgeComponetCheck = "PKG" Then
                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                        txtRetPrc.Enabled = False
                        txtQty.Enabled = False
                        btnReplace.Enabled = False
                        Dim checkFlag As Boolean
                        Dim prc As Double = txtRetPrc.Text.Trim
                        If prc > 0 Then
                            If CheckSecurity("", Session("EMP_CD")) Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = True
                                txtQty.Enabled = False
                                checkFlag = True
                            Else
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                                If e.Item.Cells(SoLnGrid.NEW_LINE).Text = "N" Then
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                Else
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                End If
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG_QTY, Session("EMP_CD")) Then
                                    If e.Item.Cells(SoLnGrid.NEW_LINE).Text = "N" Then
                                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    Else
                                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    End If
                                End If
                            End If
                            If Not checkFlag Then
                                'This security "SOM_CHG_PRC" allows only to change retail price for any line except package sku if it is explored. 
                                If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKG_CHG_PRC, Session("EMP_CD"))) Then
                                    txtRetPrc.Enabled = True
                                    txtQty.Enabled = False
                                    If e.Item.Cells(SoLnGrid.NEW_LINE).Text = "N" Then
                                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                    Else
                                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    End If
                                End If
                            End If
                        ElseIf prc = 0 Then
                            If CheckSecurity("", Session("EMP_CD")) Then
                                e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                                checkFlag = True
                            Else
                                txtRetPrc.Enabled = False
                                txtQty.Enabled = False
                                If e.Item.Cells(SoLnGrid.NEW_LINE).Text = "N" Then
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                Else
                                    e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                End If
                                If CheckSecurity(SecurityUtils.PKGCMPT_CHG_QTY, Session("EMP_CD")) Then
                                    If e.Item.Cells(SoLnGrid.NEW_LINE).Text = "N" Then
                                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    Else
                                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    End If
                                End If
                            End If
                            If Not checkFlag Then
                                'This security "SOM_CHG_PRC" allows only to change retail price for any line except package sku if it is explored. 
                                If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) AndAlso (CheckSecurity(SecurityUtils.PKG_CHG_PRC, Session("EMP_CD"))) Then
                                    txtRetPrc.Enabled = False
                                    txtQty.Enabled = False
                                    If e.Item.Cells(SoLnGrid.NEW_LINE).Text = "N" Then
                                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = False
                                    Else
                                        e.Item.Cells(SoLnGrid.DEL_CHKBOX).Enabled = True
                                    End If
                                End If
                            End If
                        End If
                    Else
                        txtRetPrc.Enabled = False
                        If CheckSecurity("", Session("EMP_CD")) Then
                            txtRetPrc.Enabled = True
                        Else
                            txtRetPrc.Enabled = False
                        End If
                        If (CheckSecurity(SecurityUtils.SOM_CHG_PRC, Session("EMP_CD"))) Then
                            txtRetPrc.Enabled = True
                        End If
                    End If
                End If
            End If
        Else
            'Do nothing
        End If
    End Sub


    'Fires when the value of the salesperson1 & 2 drop-down changes
    Protected Sub LineSalespersonChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdateItems As OracleCommand = DisposablesManager.BuildOracleCommand

        conn.Open()

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim txtSlsPct1, txtSlsPct2 As TextBox
        txtSlsPct1 = CType(dgItem.FindControl("txtLineSlsp1Pct"), TextBox)
        txtSlsPct1.Text = FormatNumber(CDbl(txtSlsPct1.Text.Trim), 2)
        txtSlsPct2 = CType(dgItem.FindControl("txtLineSlsp2Pct"), TextBox)
        txtSlsPct2.Text = FormatNumber(CDbl(txtSlsPct2.Text.Trim), 2)
        Dim cboSlsp1, cboSlsp2 As DropDownList
        cboSlsp1 = CType(dgItem.FindControl("cbo_slsp1"), DropDownList)
        cboSlsp2 = CType(dgItem.FindControl("cbo_slsp2"), DropDownList)

        If cboSlsp2.SelectedIndex = 0 Then

            txtSlsPct1.Text = "100.00"
            txtSlsPct2.Text = "0.00"

        ElseIf cboSlsp2.SelectedValue <> "" Then

            'if the second salesperson selected is same as the first, then don't accept it
            If cboSlsp2.SelectedValue = cboSlsp1.SelectedValue Then
                If Not IsNothing(Session("DEL_DOC_LN#")) Then
                    Dim filterText As String = String.Empty
                    Dim filteredRows As DataRow()
                    Dim ds As DataSet = Session("DEL_DOC_LN#")
                    filterText = "[DEL_DOC_LN#] = " + dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.Trim
                    filteredRows = ds.Tables(0).Select(filterText)
                    If filteredRows.Length > 0 Then
                        cboSlsp1.SelectedValue = filteredRows(0)("LN_SLSP1").ToString.Trim
                        cboSlsp2.SelectedValue = filteredRows(0)("LN_SLSP2").ToString.Trim
                    End If
                End If
            Else
                'if the 2 slspersons are different
                'default based on setup param and reset the slsprsn1 pct
                If Not txtSlsPct2.Text.Equals("0.00") Then
                    txtSlsPct1.Text = FormatNumber(CDbl(100 - CDbl(txtSlsPct2.Text.Trim)), 2)
                Else
                    txtSlsPct1.Text = "50.00"
                    txtSlsPct2.Text = "50.00"
                End If
            End If
        End If

        If Not IsNothing(Session("DEL_DOC_LN#")) Then
            'create a local ds that is the same as the cached one and dump updated values into it
            Dim ds As DataSet
            ds = Session("DEL_DOC_LN#")
            Dim dt As DataTable = ds.Tables(0)
            Dim dr As DataRow

            'iterate through all the rows in the ds
            For Each dr In ds.Tables(0).Rows
                'if the line# of the lines in the cached ds and the current row being
                ' modified match, then update those values
                If dr("DEL_DOC_LN#") = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text Then
                    dr("LN_SLSP1") = cboSlsp1.SelectedValue
                    dr("LN_SLSP1_PCT") = txtSlsPct1.Text
                    dr("LN_SLSP2") = cboSlsp2.SelectedValue
                    dr("LN_SLSP2_PCT") = txtSlsPct2.Text
                End If
            Next
            'update the session cache again with the latest
            Session("DEL_DOC_LN#") = ds
        End If

    End Sub


    ''' <summary>
    ''' Fires when the value of the salesperson1 commission percent changes for the order line. It validates
    ''' the percentages entered and accordingly sets the value of the other percent field.
    ''' </summary>
    Protected Sub LineSlsp1PctChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)
        Dim txtSlsPct1, txtSlsPct2 As TextBox
        txtSlsPct1 = CType(sender, TextBox)
        txtSlsPct2 = CType(dgItem.FindControl("txtLineSlsp2Pct"), TextBox)
        Dim cboSlsp1, cboSlsp2 As DropDownList
        cboSlsp1 = CType(dgItem.FindControl("cbo_slsp1"), DropDownList)
        cboSlsp2 = CType(dgItem.FindControl("cbo_slsp2"), DropDownList)

        If Not IsNumeric(txtSlsPct1.Text) Then
            If Not IsNothing(Session("DEL_DOC_LN#")) Then
                Dim filterText As String = String.Empty
                Dim filteredRows As DataRow()
                Dim ds As DataSet = Session("DEL_DOC_LN#")
                filterText = "[DEL_DOC_LN#] = " + dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.Trim
                filteredRows = ds.Tables(0).Select(filterText)
                If filteredRows.Length > 0 Then
                    txtSlsPct1.Text = FormatNumber(CDbl(filteredRows(0)("LN_SLSP1_PCT")), 2)
                Else
                    If txtSlsPct2.Text.isNotEmpty Then
                        txtSlsPct1.Text = FormatNumber(CDbl(100 - CDbl(txtSlsPct2.Text.Trim)), 2)
                    Else
                        txtSlsPct1.Text = "100.00"
                        txtSlsPct2.Text = "0.00"
                    End If
                End If
            End If
        Else
            txtSlsPct1.Text = FormatNumber(CDbl(txtSlsPct1.Text.Trim), 2)
            If Not String.IsNullOrEmpty(txtSlsPct1.Text.Trim) AndAlso (IsNumeric(txtSlsPct1.Text)) Then
                If CDbl(txtSlsPct1.Text) <= 100 And CDbl(txtSlsPct1.Text) > -0.0000001 Then
                    If cboSlsp2.SelectedIndex = 0 Then
                        If Not IsNothing(Session("DEL_DOC_LN#")) Then
                            Dim filterText As String = String.Empty
                            Dim filteredRows As DataRow()
                            Dim ds As DataSet = Session("DEL_DOC_LN#")
                            filterText = "[DEL_DOC_LN#] = " + dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.Trim
                            filteredRows = ds.Tables(0).Select(filterText)
                            If filteredRows.Length > 0 Then
                                txtSlsPct1.Text = FormatNumber(CDbl(filteredRows(0)("LN_SLSP1_PCT")), 2)
                            End If
                        End If
                    Else
                        'valid values, so just default the other pct field based on this one
                        txtSlsPct2.Text = FormatNumber(CDbl(100 - CDbl(txtSlsPct1.Text.Trim)), 2)
                    End If
                Else
                    'means that the pct fell outside the allowed range of 1-100
                    If IsNumeric(txtSlsPct2.Text) Then
                        'default the slsprsn1 pct to (100 - txtSlsPct2)
                        txtSlsPct1.Text = FormatNumber(CDbl(100 - CDbl(txtSlsPct2.Text.Trim)), 2)
                    Else
                        txtSlsPct1.Text = "100.00"
                        txtSlsPct2.Text = "0.00"
                        cboSlsp2.SelectedIndex = 0
                        cboSlsp2.SelectedValue = ""
                    End If
                End If
            Else
                'means percentage 1 entered was non numeric
                txtSlsPct1.Text = "100.00"
                txtSlsPct2.Text = "0.00"
                cboSlsp2.SelectedIndex = 0
                cboSlsp2.SelectedValue = ""
            End If
        End If

        If Not IsNothing(Session("DEL_DOC_LN#")) Then
            'create a local ds that is the same as the cached one and dump updated values into it
            Dim ds As DataSet
            ds = Session("DEL_DOC_LN#")
            Dim dt As DataTable = ds.Tables(0)
            Dim dr As DataRow

            'iterate through all the rows in the ds
            For Each dr In ds.Tables(0).Rows
                'if the line# of the lines in the cached ds and the current row being
                ' modified match, then update those values
                If dr("DEL_DOC_LN#") = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text Then
                    dr("LN_SLSP1") = cboSlsp1.SelectedValue
                    dr("LN_SLSP1_PCT") = txtSlsPct1.Text
                    dr("LN_SLSP2") = cboSlsp2.SelectedValue
                    dr("LN_SLSP2_PCT") = txtSlsPct2.Text
                End If
            Next
            'update the session cache again with the latest
            Session("DEL_DOC_LN#") = ds
        End If

    End Sub

    ''' <summary>
    ''' Fires when the value of the salesperson2 commission percent changes for the order line. It validates
    ''' the percentages entered and accordingly sets the value of the other percent field.
    ''' </summary>
    Protected Sub LineSlsp2PctChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)
        Dim txtSlsPct1, txtSlsPct2 As TextBox
        txtSlsPct1 = CType(dgItem.FindControl("txtLineSlsp1Pct"), TextBox)
        txtSlsPct2 = CType(sender, TextBox)
        Dim cboSlsp1, cboSlsp2 As DropDownList
        cboSlsp1 = CType(dgItem.FindControl("cbo_slsp1"), DropDownList)
        cboSlsp2 = CType(dgItem.FindControl("cbo_slsp2"), DropDownList)

        If Not IsNumeric(txtSlsPct2.Text) Then
            If Not IsNothing(Session("DEL_DOC_LN#")) Then
                Dim filterText As String = String.Empty
                Dim filteredRows As DataRow()
                Dim ds As DataSet = Session("DEL_DOC_LN#")
                filterText = "[DEL_DOC_LN#] = " + dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text.Trim
                filteredRows = ds.Tables(0).Select(filterText)
                If filteredRows.Length > 0 Then
                    txtSlsPct2.Text = FormatNumber(CDbl(filteredRows(0)("LN_SLSP2_PCT")), 2)
                Else
                    If txtSlsPct1.Text.isNotEmpty Then
                        txtSlsPct2.Text = FormatNumber(CDbl(100 - CDbl(txtSlsPct1.Text.Trim)), 2)
                    Else
                        txtSlsPct1.Text = "100.00"
                        txtSlsPct2.Text = "0.00"
                    End If
                End If
            End If
        Else
            txtSlsPct2.Text = FormatNumber(CDbl(txtSlsPct2.Text.Trim), 2)

            If Not String.IsNullOrEmpty(txtSlsPct2.Text.Trim) AndAlso (IsNumeric(txtSlsPct2.Text)) Then
                If Not cboSlsp2.SelectedIndex = 0 Then
                    If CDbl(txtSlsPct2.Text) <= 100 And CDbl(txtSlsPct2.Text) > -0.0000001 Then
                        'valid values, so just default the other pct field based on this one
                        txtSlsPct1.Text = FormatNumber(CDbl(100 - CDbl(txtSlsPct2.Text.Trim)), 2)
                    Else
                        If IsNumeric(txtSlsPct1.Text) Then
                            'default the slsprsn2 pct to (100 - txtSlsPct1)
                            txtSlsPct2.Text = FormatNumber(CDbl(100 - CDbl(txtSlsPct1.Text.Trim)), 2)
                        Else
                            txtSlsPct1.Text = "100.00"
                            txtSlsPct2.Text = "0.00"
                        End If
                    End If
                Else
                    txtSlsPct2.Text = "0.00"
                End If
            Else
                txtSlsPct1.Text = "100.00"
                txtSlsPct2.Text = "0.00"
                Exit Sub
            End If
        End If

        If Not IsNothing(Session("DEL_DOC_LN#")) Then
            'create a local ds that is the same as the cached one and dump updated values into it
            Dim ds As DataSet
            ds = Session("DEL_DOC_LN#")
            Dim dt As DataTable = ds.Tables(0)
            Dim dr As DataRow

            'iterate through all the rows in the ds
            For Each dr In ds.Tables(0).Rows
                'if the line# of the lines in the cached ds and the current row being
                ' modified match, then update those values
                If dr("DEL_DOC_LN#") = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text Then
                    dr("LN_SLSP1") = cboSlsp1.SelectedValue
                    dr("LN_SLSP1_PCT") = txtSlsPct1.Text
                    dr("LN_SLSP2") = cboSlsp2.SelectedValue
                    dr("LN_SLSP2_PCT") = txtSlsPct2.Text
                End If
            Next
            'update the session cache again with the latest
            Session("DEL_DOC_LN#") = ds
        End If

    End Sub


    'Fired when the check box to view the salesperson1,2 details for the line is clicked
    Protected Sub ChkBoxViewSlspersons_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkBoxViewSlspersons.CheckedChanged

        ' Daniela 16 oct 2014 Commented to improve performance 
        'bindgrid()

        ' Daniela only refresh GridView1
        Dim soLnDatSet As DataSet = Session("DEL_DOC_LN#")
        GridView1.DataSource = soLnDatSet
        GridView1.DataBind()

        doAfterBindgrid()
    End Sub

    ''' <summary>
    ''' Displays an alert message window with the message passed-in
    ''' </summary>
    ''' <param name="msg">the message to display</param>
    Private Sub DisplayMessageAlert(ByVal msg As String)

        Dim errorMessage As String = "alert('" + msg + "');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErrorMsg", errorMessage.ToString(), True)

    End Sub

    ''' <summary>
    ''' Determines based on the datarow data passed-in whether any of the line salesperson
    ''' information has changed on the line.
    ''' </summary>
    ''' <param name="dr">the datarow containing order line data</param>
    ''' <returns>True to indicate if the salesperson info has changed.False otherwise.
    ''' </returns>
    Private Function HasLineSlspersonInfoChanged(ByRef dr As DataRow) As Boolean
        Dim rtnVal As Boolean = False

        If (dr("LN_SLSP1", DataRowVersion.Original).ToString <> dr("LN_SLSP1", DataRowVersion.Current).ToString) OrElse
            (dr("LN_SLSP2", DataRowVersion.Original).ToString <> dr("LN_SLSP2", DataRowVersion.Current).ToString) OrElse
            (dr("LN_SLSP1_PCT", DataRowVersion.Original).ToString <> dr("LN_SLSP1_PCT", DataRowVersion.Current).ToString) OrElse
            (dr("LN_SLSP2_PCT", DataRowVersion.Original).ToString <> dr("LN_SLSP2_PCT", DataRowVersion.Current).ToString) Then

            rtnVal = True
        End If

        Return rtnVal
    End Function

    Public Function Finalize_Void_Security()

        Dim approval_found As String = ""
        If Find_Security("SOMV", Session("emp_cd")) = "Y" Then approval_found = approval_found & "V"
        If Find_Security("SOMF", Session("emp_cd")) = "Y" Then approval_found = approval_found & "F"

        Return approval_found

    End Function

    Protected Sub btn_addendum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_addendum.Click
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Printing Finance info ")
            End If
            Finance_Print()
        End If
    End Sub

    Protected Sub lbldelivery_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblDelivery.TextChanged
        'If user has write mode access then continue
        Dim deliveryCharges As Double = 0
        Dim sessionDelCharges As Double = 0
        Double.TryParse(lblDelivery.Text.Trim, deliveryCharges)
        Double.TryParse(Session("DEL_CHG") & "", sessionDelCharges)

        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Changing delivery charges ")
            End If
            If IsNumeric(lblDelivery.Text) AndAlso deliveryCharges >= 0 Then
                lblDelivery.Text = FormatNumber(lblDelivery.Text, 2)
                'Need to check for the syspm Sales #31. Require approval to override default delivery charges in PSOE and SOE? 
                'If this is set to Y then PSOE/SOE in E1 will require an approval code if the user changes the delivery charge
                Dim syspmForApproval As String = SysPms.deliveryChargeApproval
                If syspmForApproval.Equals("Y") AndAlso Not (sessionDelCharges = deliveryCharges) Then
                    'Bring the popup here
                    showApprovalPopup(Resources.POSMessages.MSG0062, DelChargesApproval)
                Else

                End If
            Else
                lblDelivery.Text = FormatNumber(0, 2)
                lbl_desc.Text = lbl_desc.Text & "<BR />" & Resources.POSErrors.ERR0047
            End If
            lblDelivery.Text = FormatNumber(lblDelivery.Text, 2)
            calculate_total()
        End If
    End Sub
    Private Sub hideApprovalPopup(ByVal approvalFor As String)
        Session("SHOWPOPUP") = Nothing
        ucMgrOverride.ApprovalFor = approvalFor
        popupMgrOverride.ShowOnPageLoad = False
        hidUpdLnNos.Value = String.Empty
    End Sub
    Private Sub showApprovalPopup(ByVal message As String, ByVal approvalFor As String)
        popupMgrOverride.ShowOnPageLoad = True
        ucMgrOverride.SetDisplayMessage(message)
        ucMgrOverride.ApprovalFor = approvalFor
        Session("SHOWPOPUP") = True
    End Sub
    Protected Sub lblsetup_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblSetup.TextChanged
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Changing setup charges ")
            End If
            Dim setupCharges As Double = 0
            Double.TryParse(lblSetup.Text.Trim, setupCharges)
            If IsNumeric(lblSetup.Text.Trim) AndAlso setupCharges >= 0 Then
                lblSetup.Text = FormatNumber(lblSetup.Text, 2)
            Else
                lblSetup.Text = FormatNumber(0, 2)
                lbl_desc.Text = lbl_desc.Text & "<BR />" & Resources.POSErrors.ERR0048
            End If
            lblSetup.Text = FormatNumber(lblSetup.Text, 2)
            calculate_total()
        End If
    End Sub

    Protected Sub txt_comm1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_comm1.TextChanged
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Changing sales person1 commission ")
            End If
            Dim ErrorMessage As Boolean = False
            If Not String.IsNullOrEmpty(txt_comm1.Text.Trim) AndAlso (IsNumeric(txt_comm1.Text)) Then
                txt_comm1.Text = FormatNumber(CDbl(txt_comm1.Text.Trim), 2)
                If CDbl(txt_comm1.Text.Trim) <= 100 And CDbl(txt_comm1.Text.Trim) > 0 Then
                    If txt_slsp2.Text.Trim & "" <> "" Then
                        txt_comm2.Text = FormatNumber(CDbl(100 - CDbl(txt_comm1.Text.Trim)), 2)
                    Else
                        txt_comm1.Text = "100.00"
                        txt_comm2.Text = "0.00"
                        lbl_desc.Text = Resources.POSErrors.ERR0034
                        ErrorMessage = True
                    End If
                Else
                    If CDbl(txt_comm1.Text.Trim) > 100 Or CDbl(txt_comm1.Text.Trim) < -0.0000001 Then
                        lbl_desc.Text = Resources.POSErrors.ERR0035
                        ErrorMessage = True
                        txt_comm1.Text = FormatNumber(CDbl(100 - CDbl(txt_comm2.Text)), 2)
                    ElseIf IsNumeric(txt_comm2.Text.Trim) AndAlso CDbl(txt_comm1.Text.Trim) = 0 Then
                        If txt_slsp2.Text.Trim & "" <> "" Then
                            txt_comm2.Text = "100.00"
                        Else
                            txt_comm1.Text = "100.00"
                            txt_comm2.Text = "0.00"
                            lbl_desc.Text = Resources.POSErrors.ERR0034
                            ErrorMessage = True
                        End If
                        'txt_comm2.Text = "100.00"
                    End If
                End If
            Else
                lbl_desc.Text = Resources.POSErrors.ERR0036
                ErrorMessage = True
                If txt_comm2.Text = "0.00" Then
                    txt_comm1.Text = "100.00"
                Else
                    txt_comm1.Text = FormatNumber(CDbl(100 - CDbl(txt_comm2.Text)), 2)
                End If

            End If
            ViewState("COMM1") = HidslsComm1.Value.ToString()
            HidslsComm1.Value = txt_comm1.Text.Trim
            ViewState("COMM2") = HidslsComm2.Value.ToString()
            HidslsComm2.Value = txt_comm2.Text.Trim
            'MM-7366
            If Not (ErrorMessage) Then
                lblSalessplit.Text = Resources.POSMessages.MSG0035
                PopupControlSalespersonConform.ShowOnPageLoad = True
            End If
        End If
    End Sub

    Protected Sub txt_comm2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_comm2.TextChanged
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Changing sales person2 commission ")
            End If
            Dim ErrorMessage As Boolean = False
            If Not String.IsNullOrEmpty(txt_comm2.Text.Trim) AndAlso (IsNumeric(txt_comm2.Text)) Then
                txt_comm2.Text = FormatNumber(CDbl(txt_comm2.Text.Trim), 2)
                If CDbl(txt_comm2.Text.Trim) <= 100 And CDbl(txt_comm2.Text.Trim) > 0 Then
                    If txt_slsp2.Text & "" <> "" Then
                        txt_comm1.Text = FormatNumber(CDbl(100 - CDbl(txt_comm2.Text)), 2)
                    Else
                        txt_comm1.Text = "100.00"
                        txt_comm2.Text = "0.00"
                        lbl_desc.Text = Resources.POSErrors.ERR0034
                        ErrorMessage = True
                    End If
                Else
                    If CDbl(txt_comm2.Text.Trim) > 100 Or CDbl(txt_comm2.Text.Trim) < -0.0000001 Then
                        lbl_desc.Text = Resources.POSErrors.ERR0035
                        ErrorMessage = True
                        txt_comm2.Text = FormatNumber(CDbl(100 - CDbl(txt_comm1.Text)), 2)
                    ElseIf IsNumeric(txt_comm1.Text.Trim) And CDbl(txt_comm2.Text.Trim) = 0 Then
                        txt_comm2.Text = FormatNumber(CDbl(100 - CDbl(txt_comm1.Text)), 2)
                    End If
                End If
            Else
                If txt_slsp2.Text.Trim & "" <> "" Then
                    txt_comm2.Text = FormatNumber(CDbl(100 - CDbl(txt_comm1.Text)), 2)
                    lbl_desc.Text = Resources.POSErrors.ERR0036
                    ErrorMessage = True
                Else
                    txt_comm2.Text = "0.00"
                    txt_comm1.Text = "100.00"
                    txt_slsp2.Text = ""
                    lbl_desc.Text = "A second salesperson must be entered to modify second salesperson commission percentages."
                    ErrorMessage = True
                End If
            End If
            ViewState("COMM1") = HidslsComm1.Value.ToString()
            HidslsComm1.Value = txt_comm1.Text.Trim
            ViewState("COMM2") = HidslsComm2.Value.ToString()
            HidslsComm2.Value = txt_comm2.Text.Trim
            'MM-7366
            If Not (ErrorMessage) Then
                lblSalessplit.Text = Resources.POSMessages.MSG0035
                PopupControlSalespersonConform.ShowOnPageLoad = True
            End If
        End If
    End Sub

    ''' <summary>
    ''' Fires when the user clicks on the 'Save Finance' button on the Finance window. This window is used
    ''' to modify any finance details for an open order. When there are pending order changes or the order
    ''' is not 'OPEN', the details can only be viewed.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub btn_save_fi_co_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save_fi_co.Click
        'If user has write mode access then continue
        'Daniela move up
        Dim str_display As String

        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Saving Finance ")
            End If

            'lucy start
            If CDbl(txt_fi_amt.Text) = 0 Then
                lblErrorMsg.Text = "Amount is required"
                Exit Sub
            ElseIf cbo_finance_company.SelectedIndex = 0 Then
                lblErrorMsg.Text = "Plan is required"
                Exit Sub
            Else
                Dim strfinCo = cbo_finance_company.SelectedValue
                Session("lucy_as_cd") = strfinCo
                Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = ucMgrOverride.FindControl("txtMgrPwd")
                If txtMgrPwd IsNot Nothing Then
                    txtMgrPwd.Focus()
                End If
                If Session("has_orig_fi") = "Y" And Session("skip_data_security") = False Then
                    popupMgrOverride.ShowOnPageLoad = True   'this needs to be uncomment when need to have popup., it works , 02-oct-14
                    lblErrorMsg.Text = "In progress..."

                    Session("from_btn_save") = "N"
                    Session("swipe_insert") = "Y"
                    Session("from_btn_rmv_fi") = "N"

                Else
                    str_display = send_out_fi_request()
                    If str_display = "no designated pinpad " Or str_display = "No designated pinpad" Then

                        lblErrorMsg.Text = "no designated pinpad "
                        ' txt_fi_acct.Text = ""
                        Exit Sub
                    End If
                    If str_display = "Timeout in finance detail" Then

                        lblErrorMsg.Text = str_display
                        ' txt_fi_acct.Text = ""
                        Exit Sub
                    End If

                    ' Daniela Start
                    'If Left(str_display, 5) <> "empty" Then
                    'lucy_update_table() 'not needed, the update is done below
                    'End If

                    If Left(str_display, 5) = "empty" Then
                        ' Set the txt_fi_acct.Text to empty so it will not process in ProcessOfflineFinanceChanges(dbCmd)
                        txt_fi_acct.Text = ""
                        'lucy_update_table()
                    End If
                    ' End Daniela


                End If
            End If

            Dim dbConn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
            Dim proceed As Boolean = False
            Dim soRowid As String = String.Empty

            Try
                ' At this point, unless auth occurs and fails, SO will be updated so let us lock it here
                dbConn.Open()
                dbCmd.Transaction = dbConn.BeginTransaction()

                soRowid = SalesUtils.LockSoRec(txt_del_doc_num.Text, dbCmd)

                If ConfigurationManager.AppSettings("finance") = "N" Then

                    ProcessOfflineFinanceChanges(dbCmd)
                    proceed = True
                Else
                    ' Implies that FInance is configured as a valid MOP in the system
                    ' ** Only if the finance amt is greater than 0, and there were other changes go for auth
                    If (txt_fi_amt.Text.isNotEmpty AndAlso CDbl(txt_fi_amt.Text) > 0 AndAlso Not OnlyFinanceAmtDecreased()) Then
                        If Len(txt_fi_acct.Text) < 20 Then
                            proceed = ProcessFinanceChanges(dbCmd)
                        End If
                    Else
                        'lucy comment all following
                        'means that only the amount decreased
                        ' Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                        ' Try
                        'UPDATE ONLY THE AMT IN SO table
                        ' conn.Open()
                        ' Dim sql As String = "UPDATE SO SET ORIG_FI_AMT= :AMT WHERE DEL_DOC_NUM = :DEL_DOC_NUM"
                        ' Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
                        'cmd.Parameters.Add(":AMT", OracleType.Double)
                        'cmd.Parameters(":AMT").Value = CDbl(txt_fi_amt.Text)
                        'cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                        'cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
                        ' cmd.ExecuteNonQuery()
                        proceed = True
                        'Catch ex As Exception
                        'Throw
                        ' Finally
                        'conn.Close()
                        'End Try
                    End If
                End If


                dbCmd.Transaction.Commit()

            Catch ex As Exception
                dbCmd.Transaction.Rollback()
                If ex.Message = String.Format(Resources.POSErrors.ERR0042, txt_del_doc_num.Text) Then
                    ModifyControls(1) ' disable controls other than Clear button!
                    btn_save_fi_co.Enabled = False
                End If
                lblErrorMsg.Text = ex.Message
                Exit Sub

            Finally
                If Not IsNothing(dbCmd.Transaction) Then
                    dbCmd.Transaction.Dispose()
                End If
                dbCmd.Dispose()
                dbConn.Close()
                dbConn.Dispose()
            End Try

            'if all succeeded, recalculate totals and nullify orig FI info
            If proceed Then
                UpdateAmountsAfterModifyFinance()

                ' Add comment if finance popup information has been changed
                ' before "Session("ORIG_FI_INFO")" set to "Nothing"
                addFinancePopupComments(dbCmd)

                Session("ORIG_FI_INFO") = Nothing
                PopupModifyFinance.ShowOnPageLoad = False
            End If

        End If
    End Sub


    ''' <summary>
    ''' Fires when the user clicks on the 'Exit Without Save' button on the Finance window. This window is used
    ''' to modify any finance details when Finance is not setup as an electronic MOP in the system. The user can modify
    ''' any of the details for an open order and only some when it is not 'OPEN.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_no_save_fi_co_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_no_save_fi_co.Click
        'If user has write mode access then continue
        ' Daniela exit if no doc num
        If isEmpty(ViewState("del_doc_num")) Then
            PopupModifyFinance.ShowOnPageLoad = False
            Exit Sub
        End If
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Exit Without Saving Finance ")
            End If
            ' put back the finance values before went to pop-up

            ' TODO DSA - really don't understand what happens if finance = Y but if exit no save, then restore orig values - for now
            Try
                Dim origFiInfo As New OrigFinanceInfo
                origFiInfo = Session("ORIG_FI_INFO")
                txt_fi_amt.Text = origFiInfo.finAmt
                txt_fi_appr.Text = origFiInfo.approval
                txt_fi_acct.Text = origFiInfo.acct
                cbo_finance_company.SelectedValue = origFiInfo.finCo
                lbl_financed.Text = FormatNumber(txt_fi_amt.Text, 2)

                ' Add comment if finance popup information has been changed
                Dim dbConn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)

                ' No need to check and add finance comment if "Exit w/o changes" is clicked (#ITREQUEST-149)
                'addFinancePopupComments(dbCmd)

                'Session("ORIG_FI_INFO") = origFiInfo
            Catch ' Daniela catch exception to prevent yellow triangle cast error April 27
            End Try
            PopupModifyFinance.ShowOnPageLoad = False
        End If
    End Sub


    ''' <summary>
    ''' Sets the properties such as enabled/disabled for various UI components
    ''' in the change finance popup depending on factors such as the order status,
    ''' system configuration.
    ''' </summary>
    Private Sub SetPropertiesForFinancePopup()

        lblErrorMsg.Text = String.Empty
        ddFinanceType.Enabled = False
        ddFinanceType.SelectedIndex = 0
        txt_fi_amt.Enabled = False
        cbo_finance_company.Enabled = False
        txt_fi_appr.Enabled = False
        txt_fi_acct.Enabled = False
        cbo_promo.Enabled = False
        btn_save_fi_co.Enabled = False
        btn_no_save_fi_co.Enabled = True

        Dim isOpen As Boolean = (txt_Stat_cd.Text = AppConstants.Order.STATUS_OPEN)

        If ConfigurationManager.AppSettings("finance") = "Y" Then
            'if the system is setup to do electronic 'FI', enable finance changes ONLY if it is an open order
            If isOpen AndAlso AllowFinanceChanges() Then

                txt_fi_amt.Enabled = True
                txt_fi_appr.Enabled = True
                cbo_finance_company.Enabled = True
                txt_fi_acct.Enabled = True
                cbo_promo.Enabled = True
                btn_save_fi_co.Enabled = True
            End If
        Else
            'means FI is being done non-electronically, so enable mostly everything
            txt_fi_amt.Enabled = isOpen
            cbo_finance_company.Enabled = isOpen
            txt_fi_appr.Enabled = isOpen
            txt_fi_acct.Enabled = True
            cbo_promo.Enabled = True
            ddFinanceType.Enabled = True
            btn_save_fi_co.Enabled = True

        End If
    End Sub


    ''' <summary>
    ''' Sets the max  finance amount that will be used for validation when modifying the 
    ''' finance amount via the 'Modify Finance' popup.
    ''' </summary>
    Private Sub SetMaxFinanceAmount()

        FinAmtRangeValidator.MaximumValue = CDbl(lblTotal.Text)

    End Sub

    ''' <summary>
    ''' Updates the finance amount and balance amounts after changes
    ''' are made via the 'Modify Finance' window.
    ''' </summary>
    Private Sub UpdateAmountsAfterModifyFinance()
        ' Daniela add try to catch cast errors 
        Try
            Dim origFiInfo As OrigFinanceInfo = Session("ORIG_FI_INFO")
            Dim diff As Double = 0
            If txt_fi_amt.Text = "" Then
                txt_fi_amt.Text = 0
            End If
            If Not IsNothing(origFiInfo) Then
                diff = CDbl(origFiInfo.finAmt) - CDbl(txt_fi_amt.Text)
                lbl_balance.Text = FormatCurrency((CDbl(lbl_balance.Text) + diff), 2)
            End If

            lbl_financed.Text = FormatCurrency(CDbl(txt_fi_amt.Text), 2)
        Catch ex As Exception

        Finally
        End Try

    End Sub

    ''' <summary>
    ''' Determines if the only thing that changed in the 'Change Finance' popup
    ''' was the finance amount.
    ''' </summary>
    Private Function OnlyFinanceAmtDecreased() As Boolean

        Dim rtnVal As Boolean = False
        Dim origFiInfo As OrigFinanceInfo = Session("ORIG_FI_INFO")
        If Not IsNothing(origFiInfo) Then

            If (cbo_finance_company.SelectedValue = origFiInfo.finCo AndAlso
                cbo_promo.SelectedValue = origFiInfo.promoCd AndAlso
                txt_fi_appr.Text = origFiInfo.approval AndAlso
                txt_fi_acct.Text = origFiInfo.acct) Then
                rtnVal = True
            End If

            'means that none of the other FI values changed, so check if the amount changed
            If rtnVal Then
                rtnVal = (CDbl(txt_fi_amt.Text) < CDbl(origFiInfo.finAmt))
            End If
        End If

        Return rtnVal

    End Function


    ''' <summary>
    ''' Retrieves and populates the promotions for a particular finance provider.
    ''' </summary>
    Private Sub GetPromoCodes()

        PaymentUtils.PopulatePromotions(lbl_pmt_tp.Text, cbo_finance_company.SelectedValue, cbo_promo)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String = "SELECT PROMO_CD FROM SO_ASP WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "' AND PROMO_CD IS NOT NULL"
        Try
            conn.Open()
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            Dim reader As OracleDataReader = DisposablesManager.BuildOracleDataReader(cmd)

            If (reader.Read()) Then
                Dim vListItem As ListItem = cbo_promo.Items.FindByValue(reader.Item("PROMO_CD").ToString)
                'if the promo cd on the order is in the list, default it to that, otherwise force user to choose one
                If Not vListItem Is Nothing Then
                    cbo_promo.SelectedValue = reader.Item("PROMO_CD").ToString
                Else
                    cbo_promo.SelectedIndex = 0
                End If
            End If

            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

    End Sub


    ''' <summary>
    ''' Validates that all the required info for a doing a finance authorization is present
    ''' in the 'Change Finance' popup.
    ''' </summary>
    Private Function ValidateFinanceInfo() As Boolean

        Dim rtnVal As Boolean = True
        Dim msg As String = String.Empty

        If (txt_fi_acct.Text.isEmpty) Then
            rtnVal = False
            msg = "You must enter an Account #."
        Else
            ' verify the card/acct number 
            Dim verifyCC As New VerifyCC
            Dim verifyCCTypes As New VerifyCC.TypeValid
            If txt_fi_acct.Text.isEmpty = True Or Len(txt_fi_acct.Text) < 20 Then  'not manually enter
                lucy_get_detail()  'lucy 

                If Not IsDBNull(Session("bnk_crd_num")) Then
                    txt_fi_acct.Text = Session("bnk_crd_num")
                End If

                verifyCCTypes = verifyCC.GetCardInfo(txt_fi_acct.Text)
                If String.IsNullOrEmpty(verifyCCTypes.CCType) Or verifyCCTypes.CCType = "Unknown" Then
                    rtnVal = False
                    msg = "Invalid Account #."
                    txt_fi_acct.Text = ""
                End If

            End If
        End If

        If (rtnVal AndAlso cbo_finance_company.SelectedValue.isEmpty()) Then
            rtnVal = False
            msg = "You must select a Finance company."
        End If

        If (rtnVal AndAlso cbo_promo.SelectedValue.isEmpty()) Then
            rtnVal = False
            msg = "You must select a promotion code."
        End If

        If (Not rtnVal) Then
            lblErrorMsg.Text = msg & " Please enter a valid value to continue."
        Else
            lblErrorMsg.Text = ""
        End If

        'If (Not rtnVal) Then
        'TODO -AXK cannot display either a tru Dx popup control or this jscript message box 'coz the Finance popup disappears
        ' after the message i
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ERROR", "alert('" + msg + "\n Please enter a value to continue." + "');", True)
        'End If
        Return rtnVal
    End Function


    ''' <summary>
    ''' Checks if the approval code in the 'Change Finance' popup changed or not. If 
    ''' it did change, then an entry is inserted into the SO comment table.
    ''' </summary>
    Private Sub CheckForFinanceApprovalChanges()

        Dim origFiInfo As OrigFinanceInfo = Session("ORIG_FI_INFO")
        If (Not IsNothing(origFiInfo)) Then
            If txt_fi_appr.Text <> origFiInfo.approval Then

                Dim origSoDatRow As DataRow = Session("ORIG_SO").Tables(0).Rows(0)
                Dim cmntKey = New OrderUtils.WrDtStoreSeqKey
                cmntKey.wrDt = txt_wr_dt.Text
                cmntKey.storeCd = txt_store_cd.Text
                cmntKey.seqNum = origSoDatRow("SO_SEQ_NUM")

                InsertSOComment(String.Format("FI approval code changed from {0} to {1}", origFiInfo.approval, txt_fi_appr.Text),
                                "S",
                                cmntKey)
            End If
        End If

    End Sub

    ''' <summary>
    ''' Processes any changes made  the 'Change Finance' popup when the system is configured 
    ''' to NOT support any electronic Finance MOP.
    ''' </summary>
    Private Sub ProcessOfflineFinanceChanges(ByRef dbCmd As OracleCommand)

        'Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String


        ' *** First determine if the approval code changed and if it did, create a comment in the SO_CMNT table
        ' *** This needs to happend regardless of whether the system is configured for Finance or not.
        CheckForFinanceApprovalChanges()

        If (txt_fi_acct.Text.isNotEmpty) Then

            sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & txt_fi_acct.Text & "', MOP_CD='FI' WHERE TRN_TP_CD='" & txt_ord_tp_cd.Text & "' "
            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then
                sql = sql & "AND IVC_CD='" & txt_del_doc_num.Text & "'"
            Else
                sql = sql & "AND ADJ_IVC_CD='" & txt_del_doc_num.Text & "'"
            End If
            dbCmd.CommandText = sql
            dbCmd.ExecuteNonQuery()
            ' Daniela fix amount bug April 23
            sql = "UPDATE SO SET ORIG_FI_AMT=" & CDbl(txt_fi_amt.Text) & ", APPROVAL_CD='" & txt_fi_appr.Text & "', FIN_CUST_CD='" & cbo_finance_company.SelectedValue & "' WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
            ' sql = "UPDATE SO SET ORIG_FI_AMT=" & txt_fi_amt.Text & ", APPROVAL_CD='" & txt_fi_appr.Text & "', FIN_CUST_CD='" & cbo_finance_company.SelectedValue & "' WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
            dbCmd.CommandText = sql
            dbCmd.ExecuteNonQuery()

            ' this is a different connection to extract the data
            Dim reader As OracleDataReader
            Dim conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            sql = "SELECT * FROM SO_ASP WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            conn.Open()
            Dim rate_info_array As Array = Nothing
            Dim intro_rate_array As Array = Nothing
            Dim during_promo_apr As String = String.Empty
            Dim during_promo_apr_flag As String = String.Empty
            Dim after_promo_apr As String = String.Empty
            Dim after_promo_apr_flag As String = String.Empty
            Dim promo_duration As String = String.Empty
            Dim aspPromoCd As String = String.Empty
            Try
                reader = DisposablesManager.BuildOracleDataReader(cmd)

                ''get  first the ASP_PROMO info like promo des, etc.
                Dim promoDes As String = String.Empty
                Dim promoDes3 As String = String.Empty
                Dim ds As DataSet = GetAspPromoDetails(cbo_finance_company.SelectedValue, cbo_promo.SelectedValue)
                For Each dr In ds.Tables(0).Rows
                    If Not IsDBNull(dr("des")) Then promoDes = dr("des")
                    If Not IsDBNull(dr("promo_des3")) Then promoDes3 = dr("promo_des3")
                    If Not IsDBNull(dr("as_promo_cd")) Then aspPromoCd = dr("as_promo_cd")
                Next

                If IsNumeric(during_promo_apr) Then
                    during_promo_apr = during_promo_apr / 10000 & "% "
                End If
                If IsNumeric(after_promo_apr) Then
                    after_promo_apr = after_promo_apr / 10000 & "% "
                End If
                If (reader.Read()) Then
                    sql = "UPDATE SO_ASP SET PROMO_CD='" & cbo_promo.SelectedValue & "', AS_PROMO_CD='" & aspPromoCd & "', "
                    sql = sql & "PROMO_DES='" & promoDes & "' WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"

                    dbCmd.CommandText = sql
                    dbCmd.ExecuteNonQuery()
                Else
                    sql = "INSERT INTO SO_ASP (" &
                                "DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD,  PROMO_DES, PROMO_DES1, " &
                                "PROMO_DES2, PROMO_DES3, PROMO_APR, PROMO_APR_FLAG, " &
                                "ACCT_APR, ACCT_APR_FLAG)" &
                            " VALUES('" & txt_del_doc_num.Text & "', 'Y', '" & cbo_promo.SelectedValue & "','" & aspPromoCd & "','" & promoDes & "',NULL,'" &
                                                   promo_duration & "','" & promoDes3 & "','" & during_promo_apr & "','" & during_promo_apr_flag & "','" &
                                                   after_promo_apr & "','" & after_promo_apr_flag & "')"
                    sql = UCase(sql)
                    dbCmd.CommandText = sql
                    dbCmd.ExecuteNonQuery()
                End If

                reader.Close()
                conn.Close()
                conn.Dispose()
            Catch ex As Exception
                If Not reader.IsClosed Then
                    reader.Close()
                End If
                conn.Close()
                conn.Dispose()
                Throw
            End Try
        Else
            'means the account number was empty
            sql = "UPDATE AR_TRN SET BNK_CRD_NUM=NULL, MOP_CD=NULL WHERE TRN_TP_CD='" & txt_ord_tp_cd.Text & "' "
            If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then
                sql = sql & "AND IVC_CD='" & txt_del_doc_num.Text & "'"
            Else
                sql = sql & "AND ADJ_IVC_CD='" & txt_del_doc_num.Text & "'"
            End If
            dbCmd.CommandText = sql
            dbCmd.ExecuteNonQuery()

            sql = "UPDATE SO SET ORIG_FI_AMT=NULL, APPROVAL_CD=NULL, FIN_CUST_CD=NULL WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
            dbCmd.CommandText = sql
            dbCmd.ExecuteNonQuery()

            sql = "DELETE FROM SO_ASP WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
            dbCmd.CommandText = sql
            dbCmd.ExecuteNonQuery()
            txt_fi_amt.Text = 0
            txt_fi_appr.Text = ""
            cbo_finance_company.SelectedValue = ""
        End If
        lbl_financed.Text = FormatNumber(txt_fi_amt.Text, 2)

    End Sub


    ''' <summary>
    ''' Processes any changes made  the 'Change Finance' popup when the system is configured 
    ''' to do electronic Finance transactions. This will actually create, request and process an
    ''' electronic authorization and if successful, will update the related and relevant tables.
    ''' It performs any validation necessary for doing authorization and also prints an addendum,if
    ''' successful.
    ''' </summary>
    ''' <param name="dbCmd">existing command with locked SO record for continued processing of updates</param>
    ''' <returns>a flag indicating that the electronic auth was successful.</returns>
    Private Function ProcessFinanceChanges(ByRef dbCmd As OracleCommand) As Boolean

        Dim rtnVal As Boolean = False
        Dim SDC_Fields As String() = Nothing
        Dim SDC_BreakOut As String() = Nothing
        Dim merchantId As String = String.Empty
        Dim authFailed As Boolean = False
        Dim Auth_No As String = ""
        Dim Trans_Id As String = ""
        Dim Host_Response_Cd As String = ""
        Dim Protobase_Response_Msg As String = ""
        Dim PB_Response_Cd As String = ""
        Dim Host_Response_Msg As String = ""
        Dim Host_Merchant_ID As String = ""
        Dim bnk_crd_num As String = ""
        Dim SDC_Response As String = ""
        Dim exp_dt As String = ""
        Dim ref_num As String = ""
        Dim transport As String = ""
        Dim plan_desc As String = ""
        Dim rate_info As String = ""
        Dim intro_rate As String = ""

        If (ValidateFinanceInfo()) Then

            ' *** First determine if the approval code changed and if it did, create a comment in the SO_CMNT table
            CheckForFinanceApprovalChanges()

            ' Check if there is a terminal id & transport(only GE & WF suported right now) for the written store of the sale.
            Dim ds As DataSet = GetFinanceAuthDetails(cbo_finance_company.SelectedValue, txt_store_cd.Text)
            For Each dr In ds.Tables(0).Rows
                merchantId = dr("TERMINAL_ID")
                transport = dr("TRANSPORT")
            Next

            If merchantId.isNotEmpty AndAlso (transport = "GE_182" Or transport = "WF_148") AndAlso txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then

                'In this case, the finance auth is always going to be a 'SALE' auth, never a refund auth here. So, Process through PBase Instance                           
                'Create a unique payment/reference id for the auth request - has to be a 7-digit number. Use current datetime + random number
                'Dim id As String = Right(txt_del_doc_num.Text, 5) + DateTime.Now.Millisecond.ToString
                Dim randomGen As Random = New Random()
                Dim id As String = Right((Now.Ticks + randomGen.Next).ToString, 7)
                Dim authInput As String = SD_Utils.BuildPLAuthTransaction(id, merchantId,
                                                                           txt_del_doc_num.Text,
                                                                           SD_Utils.AUTH_ONLY,
                                                                           txt_fi_acct.Text,
                                                                           txt_fi_amt.Text, "",
                                                                           txt_fi_appr.Text, "",
                                                                           cbo_promo.SelectedValue)
                SDC_Response = SD_Submit_Query(SD_Utils.AUTH_ONLY, authInput)
                If SDC_Response.isEmpty OrElse InStr(SDC_Response, " this stream") > 0 OrElse InStr(SDC_Response, "Protobase is not responding") > 0 Then
                    UpdatePaymentHoldingForFinance(id, AppConstants.Order.TYPE_SAL, "Protobase server not responding", "", "")
                    authFailed = True
                    theSystemBiz.SaveAuditLogComment("PAYMENT_FAILED", "Sale " & txt_del_doc_num.Text & " finance transaction for " & txt_fi_amt.Text & " failed.  Protobase server did not respond for card " & Right(txt_fi_acct.Text, 4))
                    btn_save_fi_co.Enabled = False
                Else
                    btn_save_fi_co.Enabled = True
                    Dim sep(3) As Char
                    sep(0) = Chr(10)
                    sep(1) = Chr(12)
                    SDC_Fields = SDC_Response.Split(sep)
                    Dim s As String
                    Dim MsgArray As Array
                    For Each s In SDC_Fields
                        If InStr(s, ",") > 0 Then
                            MsgArray = Split(s, ",")
                            Select Case MsgArray(0)
                                Case "0003"
                                    Trans_Id = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "0004"
                                    If (MsgArray(1).ToString.isNotEmpty) Then
                                        exp_dt = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                        exp_dt = SystemUtils.MonthLastDay(Left(exp_dt, 2) & "/1/" & Right(exp_dt, 2))
                                    End If
                                Case "0006"
                                    Auth_No = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "0007"
                                    ref_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "1003"
                                    PB_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "1004"
                                    Host_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "1008"
                                    bnk_crd_num = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                    bnk_crd_num = Right(bnk_crd_num.Trim, 4)
                                Case "1009"
                                    Host_Response_Cd = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "1010"
                                    Protobase_Response_Msg = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "3319"
                                    plan_desc = ""
                                    For seq_num = LBound(MsgArray) To UBound(MsgArray)
                                        If seq_num > 0 Then
                                            plan_desc = plan_desc & MsgArray(seq_num) & ","
                                        End If
                                    Next
                                    plan_desc = Left(plan_desc, Len(plan_desc) - 1)
                                Case "3322"
                                    rate_info = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                                Case "3325"
                                    intro_rate = Left(MsgArray(1), Len(MsgArray(1)) - 1)
                            End Select
                        End If
                    Next s

                    ''''''Check what kind of response was received
                    If (PB_Response_Cd = "0" OrElse PB_Response_Cd = "0000") Then
                        theSystemBiz.SaveAuditLogComment("PAYMENT_SUCCESS", "Sale " & txt_del_doc_num.Text & " finance transaction for " & txt_fi_amt.Text & " succeeded for card " & Right(txt_fi_acct.Text, 4))
                    Else
                        ' means it could be a "0060" or any other kind of failure
                        'Update_Payment_Holding(id, Host_Response_Msg, Host_Response_Cd, Protobase_Response_Msg, "DEP", txt_del_doc_num.Text)
                        UpdatePaymentHoldingForFinance(id, AppConstants.Order.TYPE_SAL, Protobase_Response_Msg, Host_Response_Msg, Host_Response_Cd)
                        theSystemBiz.SaveAuditLogComment("PAYMENT_FAILED", "Sale " & txt_del_doc_num.Text & " finance transaction for " & txt_fi_amt.Text & " was declined for card " &
                                               Right(txt_fi_acct.Text, 4) & ". " & Host_Response_Msg & " - " & Host_Response_Cd & " - " & Protobase_Response_Msg)
                        authFailed = True
                        btn_save_fi_co.Enabled = False
                    End If
                End If

                ' ** if the FI auth was successful, then save/update the various tables and print the addendum
                If Not authFailed Then
                    SaveAndPrintFinanceAuthInfo(ref_num, Auth_No, plan_desc, intro_rate, rate_info, dbCmd)
                Else
                    ASPxPopupControl2.ContentUrl = "Payment_Hold.aspx?del_doc_num=" & txt_del_doc_num.Text
                    ASPxPopupControl2.ShowOnPageLoad = True
                End If
            Else
                authFailed = True
                'ClientScript.RegisterStartupScript(Me.GetType(), "Setup Error", "alert('" + "No Merchant/terminal ID defined for " & cbo_finance_company.SelectedValue & ".\n Please contact the System Administrator." + "');", True)
                lblErrorMsg.Text = String.Format("No Merchant/terminal ID defined in the system for {0}, and store {1}. ", cbo_finance_company.SelectedValue, txt_store_cd.Text)
                Exit Function
            End If

            rtnVal = (Not authFailed)
        End If  'end if validation passed

        Return rtnVal

    End Function

    ''' <summary>
    ''' Saves the information that was receieved as result of a successful electronic finance authorization
    ''' due to changes made  the 'Change Finance' popup. It also issues a printing request. 
    ''' </summary>
    ''' <param name="refNum">the unique reference number referenced in the authorization</param>
    ''' <param name="authNum">the auth number</param>
    ''' <param name="planDesc">the plan description that contains details such as the promo details</param>
    ''' <param name="introRate">the during promo information</param>
    ''' <param name="rateInfo">the after promo information</param>
    Private Sub SaveAndPrintFinanceAuthInfo(ByVal refNum As String, ByVal authNum As String,
                                                ByVal planDesc As String, ByVal introRate As String,
                                                ByVal rateInfo As String, ByRef dbCmd As OracleCommand)



        Dim sql As String


        ' *** Since this is an existing order, update all the related info in various tables
        ' **** Update the **AR_TRN** table with the ref_num,etc. used for the FIN auth request 
        sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & txt_fi_acct.Text & "', MOP_CD='FI', " &
              " REF_NUM = '" & refNum & "'" & ", HOST_REF_NUM = '" & refNum & "'" &
              " WHERE TRN_TP_CD='" & txt_ord_tp_cd.Text & "' " &
              " AND IVC_CD='" & txt_del_doc_num.Text & "'"
        dbCmd.CommandText = sql
        dbCmd.ExecuteNonQuery()

        ' **** Update the **SO** table with the new finance info
        If authNum.isEmpty Then
            If txt_fi_appr.Text.isNotEmpty Then authNum = txt_fi_appr.Text
        End If
        sql = "UPDATE SO SET ORIG_FI_AMT=" & txt_fi_amt.Text & ", APPROVAL_CD='" & authNum &
              "', FIN_CUST_CD='" & cbo_finance_company.SelectedValue &
              "' WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
        sql = UCase(sql)
        dbCmd.CommandText = sql
        dbCmd.ExecuteNonQuery()
        ' *** Update the asp response info when a response is received for a finance auth
        Dim responseCd, responseTypeCd As String

        'retrieve the auth provider response code info from the db
        Dim ds As DataSet = GetAspResponseCodeDetails(cbo_finance_company.SelectedValue)
        For Each dr In ds.Tables(0).Rows
            responseCd = dr("resp_cd")
            responseTypeCd = dr("resp_tp_cd")
            If IsDBNull(responseCd) Then  'lucy 
                responseCd = "MANUAL"
            End If
        Next
        sql = "UPDATE SO_ASP_RESP SET AS_CD='" & cbo_finance_company.SelectedValue & "', RESP_CD='" & responseCd &
               "', RESP_TP_CD='" & responseTypeCd & "', RESP_DT_TIME= sysdate " &
               "WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
        sql = UCase(sql)
        dbCmd.CommandText = sql
        dbCmd.ExecuteNonQuery()
        '' *** Update or insert into the local Settlement_Terms and E1 SO_ASP tables 
        SaveFinancePromoInfo(planDesc, introRate, rateInfo)

        ' **** FInally print the FI addendum, since the auth was successful 
        PrintAddendumAfterFinanceChanges()
    End Sub

    ''' <summary>
    ''' Saves the Finance promotion details received as result of a successful electronic finance authorization
    ''' due to changes made  the 'Change Finance' popup. 
    ''' </summary>
    ''' <param name="plan_desc">the plan description that contains details such as the promo details</param>
    ''' <param name="intro_rate">the during promo information</param>
    ''' <param name="rate_info">the after promo information</param>
    Private Sub SaveFinancePromoInfo(ByVal plan_desc As String, ByVal intro_rate As String, ByVal rate_info As String)

        Dim fin_co = cbo_finance_company.SelectedValue.ToString
        Dim rate_info_array As Array = Nothing
        Dim intro_rate_array As Array = Nothing
        Dim during_promo_apr As String = String.Empty
        Dim during_promo_apr_flag As String = String.Empty
        Dim after_promo_apr As String = String.Empty
        Dim after_promo_apr_flag As String = String.Empty
        Dim promo_duration As String = String.Empty
        Dim aspPromoCd As String = String.Empty
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim reader As OracleDataReader

        Try
            If plan_desc.isNotEmpty Then
                'this is the promo/during promo info
                If InStr(intro_rate, ";") > 0 Then
                    intro_rate_array = Split(intro_rate, ";")
                    If (Not intro_rate Is Nothing) Then
                        during_promo_apr_flag = intro_rate_array(0)
                        during_promo_apr = intro_rate_array(1)
                        promo_duration = intro_rate_array(4)
                    End If
                End If

                'this is the account/after promo info 
                If InStr(rate_info, ";") > 0 Then
                    rate_info_array = Split(rate_info, ";")
                    If (Not rate_info_array Is Nothing) Then
                        after_promo_apr_flag = rate_info_array(4)
                        after_promo_apr = rate_info_array(6)
                    End If
                End If

                conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                conn.Open()
                sql = "INSERT INTO SETTLEMENT_TERMS (DEL_DOC_NUM, ORD_TP_CD, RATE_DEL_RATE_TYPE, RATE_DEL_APR, INTRO_RATE, INTRO_APR, INTRO_DURATION, PLAN_DESC) "
                sql = sql & "VALUES (:DEL_DOC_NUM, :ORD_TP_CD, :RATE_DEL_RATE_TYPE, :RATE_DEL_APR, :INTRO_RATE, :INTRO_APR, :INTRO_DURATION, :PLAN_DESC) "
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)

                cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
                cmd.Parameters.Add(":ORD_TP_CD", OracleType.VarChar)
                cmd.Parameters.Add(":RATE_DEL_RATE_TYPE", OracleType.VarChar)
                cmd.Parameters.Add(":RATE_DEL_APR", OracleType.VarChar)
                cmd.Parameters.Add(":INTRO_RATE", OracleType.VarChar)
                cmd.Parameters.Add(":INTRO_APR", OracleType.VarChar)
                cmd.Parameters.Add(":INTRO_DURATION", OracleType.VarChar)
                cmd.Parameters.Add(":PLAN_DESC", OracleType.VarChar)

                cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
                cmd.Parameters(":ORD_TP_CD").Value = txt_ord_tp_cd.Text
                cmd.Parameters(":RATE_DEL_RATE_TYPE").Value = after_promo_apr_flag  'rate_info_array(4).ToString
                cmd.Parameters(":RATE_DEL_APR").Value = after_promo_apr   'rate_info_array(6).ToString
                cmd.Parameters(":INTRO_RATE").Value = during_promo_apr_flag      'intro_rate_array(0).ToString
                cmd.Parameters(":INTRO_APR").Value = during_promo_apr         'intro_rate_array(1).ToString
                cmd.Parameters(":INTRO_DURATION").Value = promo_duration 'intro_rate_array(4).ToString
                cmd.Parameters(":PLAN_DESC").Value = plan_desc.trimString(0, 50)
                cmd.ExecuteNonQuery()
            Else
                theSystemBiz.SaveAuditLogComment("FINANCE_PROMO_FAILURE", "Sales Order " & txt_del_doc_num.Text & " did not retrieve promotional terms")
            End If
            conn.Close()

            ' ****** Save/update the SO_ASP table **************
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()
            sql = "SELECT * FROM SO_ASP WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            reader = DisposablesManager.BuildOracleDataReader(cmd)


            ''get  first the ASP_PROMO info like promo des, etc.
            Dim promoDes As String = String.Empty
            Dim promoDes3 As String = String.Empty
            Dim ds As DataSet = GetAspPromoDetails(cbo_finance_company.SelectedValue, cbo_promo.SelectedValue)
            For Each dr In ds.Tables(0).Rows
                If Not IsDBNull(dr("des")) Then promoDes = dr("des")
                If Not IsDBNull(dr("promo_des3")) Then promoDes3 = dr("promo_des3")
                If Not IsDBNull(dr("as_promo_cd")) Then aspPromoCd = dr("as_promo_cd")
            Next

            '****************** Insert/Update the finance auth details into SO_ASP table, dpending on if exists or not *****************
            If (reader.Read()) Then
                sql = "UPDATE SO_ASP SET PROMO_CD='" & cbo_promo.SelectedValue & "', AS_PROMO_CD='" & aspPromoCd & "', " &
                         "PROMO_DES='" & promoDes & "', " & "PROMO_DES2='" & promo_duration & "', " & "PROMO_DES3='" & promoDes3 & "', " &
                         "PROMO_APR='" & during_promo_apr & "', " & "PROMO_APR_FLAG='" & during_promo_apr_flag & "', " &
                         "ACCT_APR='" & after_promo_apr & "', " & "ACCT_APR_FLAG='" & after_promo_apr_flag & "' " &
                         " WHERE DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                cmd.ExecuteNonQuery()
            Else
                sql = "INSERT INTO SO_ASP (" &
                            "DEL_DOC_NUM, MANUAL, PROMO_CD, AS_PROMO_CD,  PROMO_DES, PROMO_DES1, " &
                            "PROMO_DES2, PROMO_DES3, PROMO_APR, PROMO_APR_FLAG, " &
                            "ACCT_APR, ACCT_APR_FLAG)" &
                        " VALUES('" & txt_del_doc_num.Text & "', 'Y', '" & cbo_promo.SelectedValue & "','" & aspPromoCd & "','" & promoDes & "',NULL,'" &
                                               promo_duration & "','" & promoDes3 & "','" & during_promo_apr & "','" & during_promo_apr_flag & "','" &
                                               after_promo_apr & "','" & after_promo_apr_flag & "')"
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                cmd.ExecuteNonQuery()
            End If

            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

    End Sub

    ''' <summary>
    ''' Inserts a record into the SO comment table
    ''' </summary>
    ''' <param name="comment">the text or comment</param>
    ''' <param name="cmntType">the type of comment</param>
    ''' <param name="cmntKey">the object containing info needed to generate the comemnt sequence.</param>
    Private Sub InsertSOComment(ByVal comment As String, ByVal cmntType As String, ByRef cmntKey As OrderUtils.WrDtStoreSeqKey)

        Dim rtnVal As String = String.Empty
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        Dim intSeqNum As Integer = OrderUtils.getNextSoCmntSeq(cmntKey)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As New StringBuilder
        Dim reader As OracleDataReader

        sql.Append(" INSERT INTO SO_CMNT (SO_WR_DT, ")
        sql.Append("                      SO_STORE_CD, ")
        sql.Append("                      SO_SEQ_NUM, ")
        sql.Append("                      SEQ#, ")
        sql.Append("                      DT, ")
        sql.Append("                      TEXT, ")
        sql.Append("                      DEL_DOC_NUM, ")
        sql.Append("                      CMNT_TYPE, ")
        sql.Append("                      PERM) ")
        sql.Append("      VALUES( :WR_DT, ")
        sql.Append("              :STORE_CD, ")
        sql.Append("              :SEQ_NUM, ")
        sql.Append("              :SEQ#, ")
        sql.Append("              TRUNC(sysdate), ")
        sql.Append("              :CMNT, ")
        sql.Append("              :DEL_DOC_NUM, ")
        sql.Append("              :TYPE, ")
        sql.Append("              'Y') ")

        Try
            cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
            cmd.Parameters.Add(":WR_DT", OracleType.DateTime)
            cmd.Parameters(":WR_DT").Value = Date.ParseExact(FormatDateTime(txt_wr_dt.Text, DateFormat.ShortDate), "d", System.Globalization.CultureInfo.CurrentCulture)
            cmd.Parameters.Add(":STORE_CD", OracleType.VarChar)
            cmd.Parameters(":STORE_CD").Value = txt_store_cd.Text
            cmd.Parameters.Add(":SEQ_NUM", OracleType.VarChar)
            cmd.Parameters(":SEQ_NUM").Value = cmntKey.seqNum
            cmd.Parameters.Add(":SEQ#", OracleType.Number)
            cmd.Parameters(":SEQ#").Value = intSeqNum
            cmd.Parameters.Add(":CMNT", OracleType.VarChar)
            cmd.Parameters(":CMNT").Value = comment
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
            cmd.Parameters.Add(":TYPE", OracleType.VarChar)
            cmd.Parameters(":TYPE").Value = cmntType
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
    End Sub


    ''' <summary>
    ''' Prints an addendum after any finance changes have been made to ensure the customer
    ''' has the updated details.
    ''' </summary>
    Private Sub PrintAddendumAfterFinanceChanges()

        Dim invoice As String = String.Empty
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim sql As New StringBuilder
        Dim reader As OracleDataReader
        Dim UpPanel As UpdatePanel = Master.FindControl("UpdatePanel1")

        conn.Open()
        sql.Append(" SELECT INVOICE_NAME ")
        sql.Append("   FROM PROMO_ADDENDUMS ")
        sql.Append("  WHERE PROMO_CD = :PROMO_CD ")
        sql.Append("  AND AS_CD = :AS_CD ")

        cmd = DisposablesManager.BuildOracleCommand(sql.ToString, conn)
        cmd.Parameters.Add(":PROMO_CD", OracleType.VarChar)
        cmd.Parameters(":PROMO_CD").Value = cbo_promo.SelectedValue
        cmd.Parameters.Add(":AS_CD", OracleType.VarChar)
        cmd.Parameters(":AS_CD").Value = cbo_finance_company.SelectedValue

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            If reader.Read Then
                invoice = reader.Item("INVOICE_NAME").ToString
            End If
            reader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        If invoice.isEmpty Then
            lbl_desc.Text = "No finance invoice name found for:" & cbo_finance_company.SelectedValue
            Exit Sub
        End If

        ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "AnyScriptNameYouLike8", "window.open('FI_Addendums/GEMONEY.aspx?DEL_DOC_NUM=" & txt_del_doc_num.Text.ToString & "&UFM=" & invoice & "','frmFinance8" & "','height=700px,width=800px,top=20,left=20,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

    End Sub

    ''' <summary>
    ''' Inserts a record in the Payment Holding table whenever a FINANCE authorization
    ''' request fails. This is the place where all failed auths are stored and are retrieved
    ''' when the Payment Holding popup is displayed for possible resubmission.
    ''' </summary>
    ''' <param name="id">the unique reference id for the auth</param>
    ''' <param name="tranTypeCd">the type of transaction- in this case is "SAL"</param>
    ''' <param name="pbResponseMsg">the message returned from Protbase after an auth request</param>
    ''' <param name="hostResponseMsg">the message returned from the auth host</param>
    ''' <param name="hostResponseCd">the code returned after an auth request</param>
    ''' <remarks></remarks>
    Private Sub UpdatePaymentHoldingForFinance(ByVal id As String,
                                               ByVal tranTypeCd As String,
                                               ByVal pbResponseMsg As String,
                                               ByVal hostResponseMsg As String,
                                               ByVal hostResponseCd As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        Dim sql As String

        sql = "INSERT INTO PAYMENT_HOLDING (ROW_ID, DEL_DOC_NUM, MOP_CD, AMT, DES, EXP_DT, MOP_TP, "
        sql = sql & "FIN_CO, FIN_PROMO_CD, FI_ACCT_NUM, APPROVAL_CD, TRN_TP_CD, "
        sql = sql & "PB_RESPONSE_MSG, HOST_RESPONSE_MSG, HOST_RESPONSE_CD, DL_STATE, DL_LICENSE_NO) "
        sql = sql & "VALUES(:ROW_ID, :DEL_DOC_NUM, :MOP_CD, :AMT, :DES, :EXP_DT, :MOP_TP, "
        sql = sql & ":FIN_CO, :FIN_PROMO_CD, :FI_ACCT_NUM, :APPROVAL_CD, :TRN_TP_CD, "
        sql = sql & ":PB_RESPONSE, :HOST_RESPONSE_MSG, :HOST_RESPONSE_CD, :DL_STATE, :DL_LICENSE_NO) "

        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        cmd.Parameters.Add(":ROW_ID", OracleType.Number)
        cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
        cmd.Parameters.Add(":MOP_CD", OracleType.VarChar)
        cmd.Parameters.Add(":AMT", OracleType.Number)
        cmd.Parameters.Add(":DES", OracleType.VarChar)
        cmd.Parameters.Add(":EXP_DT", OracleType.VarChar)
        cmd.Parameters.Add(":MOP_TP", OracleType.VarChar)
        cmd.Parameters.Add(":FIN_CO", OracleType.VarChar)
        cmd.Parameters.Add(":FIN_PROMO_CD", OracleType.VarChar)
        cmd.Parameters.Add(":FI_ACCT_NUM", OracleType.VarChar)
        cmd.Parameters.Add(":APPROVAL_CD", OracleType.VarChar)
        cmd.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
        cmd.Parameters.Add(":PB_RESPONSE", OracleType.VarChar)
        cmd.Parameters.Add(":HOST_RESPONSE_MSG", OracleType.VarChar)
        cmd.Parameters.Add(":HOST_RESPONSE_CD", OracleType.VarChar)
        cmd.Parameters.Add(":DL_STATE", OracleType.VarChar)
        cmd.Parameters.Add(":DL_LICENSE_NO", OracleType.VarChar)


        cmd.Parameters(":ROW_ID").Value = CDbl(id)
        cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
        cmd.Parameters(":MOP_CD").Value = PaymentUtils.Payment.TP_FINANCE
        cmd.Parameters(":AMT").Value = CDbl(txt_fi_amt.Text)
        cmd.Parameters(":DES").Value = cbo_finance_company.SelectedItem.Text
        cmd.Parameters(":EXP_DT").Value = ""
        cmd.Parameters(":MOP_TP").Value = PaymentUtils.Payment.TP_FINANCE
        cmd.Parameters(":FIN_CO").Value = cbo_finance_company.SelectedValue
        cmd.Parameters(":FIN_PROMO_CD").Value = cbo_promo.SelectedValue
        'acct# is encrypted because the Payment_Hold page will expect it and will decrypt it
        cmd.Parameters(":FI_ACCT_NUM").Value = AppUtils.Encrypt(txt_fi_acct.Text, "CrOcOdIlE")
        cmd.Parameters(":APPROVAL_CD").Value = txt_fi_appr.Text
        cmd.Parameters(":TRN_TP_CD").Value = tranTypeCd
        cmd.Parameters(":PB_RESPONSE").Value = pbResponseMsg
        cmd.Parameters(":HOST_RESPONSE_MSG").Value = hostResponseMsg
        cmd.Parameters(":HOST_RESPONSE_CD").Value = hostResponseCd
        cmd.Parameters(":DL_STATE").Value = ""
        cmd.Parameters(":DL_LICENSE_NO").Value = ""

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
    End Sub

    ''' <summary>
    ''' Determines if finance changes are allowed or not based on pending order changes.
    ''' </summary>
    ''' <returns>a flag to indicate if the original finance info can be modified or not.</returns>
    Private Function AllowFinanceChanges() As Boolean
        Dim rtnVal As Boolean = True

        If (Session("ORIG_SO_TOTAL") <> "") Then

            rtnVal = CDbl(lblTotal.Text) = CDbl(Session("ORIG_SO_TOTAL"))
        End If

        Return rtnVal
    End Function

    Private Class OrigFinanceInfo
        ' original finance related info before entry to and any changes in the finance pop-up; used to restore if exit w/o saving
        Property finCo As String  ' from so.fin_cust_cd or asp.as_cd 
        Property promoCd As String
        Property finAmt As String
        Property acct As String
        Property approval As String
    End Class

    Protected Sub cbo_finance_company_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_finance_company.SelectedIndexChanged


        If cbo_finance_company.SelectedValue.ToString.isNotEmpty Then
            '  GetPromoCodes()
            txt_fi_appr.Text = String.Empty
            txt_fi_acct.Text = String.Empty    'lucy print
            txt_exp_dt.Text = String.Empty ' daniela added
        End If
    End Sub

    ''' <summary>
    ''' Fires when user clicks on the icon button that brings up the 'Modify Finance' popup.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub btnModifyFinance_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        'Feb 22 disable buttons if Finalized or Void
        If txt_Stat_cd.Text.Equals("F") Or txt_Stat_cd.Text.Equals("V") Then
            btn_save_fi_co.Enabled = False
            btn_lucy_save.Enabled = False
            btn_lucy_rmv_fi.Enabled = False
        End If

        ' Daniela mask card num
        If isNotEmpty(txt_fi_acct.Text) And txt_fi_acct.Text.Length = 16 Then
            Session("lucy_bnk_card") = txt_fi_acct.Text
            txt_fi_acct.Text = String.Format("************" + txt_fi_acct.Text.Substring(12, 4))
        End If

        'lucy
        Dim str_co_grp_cd As String
        Dim str_co_cd As String
        Dim str_sup_user_flag As String
        Dim str_flag As String
        Dim empcd As String = Session("emp_cd")
        'Feb 22 Avoid top sql
        If (isEmpty(Session("str_sup_user_flag"))) Then
            str_flag = LeonsBiz.lucy_check_sup_user(UCase(empcd), str_co_grp_cd, str_co_cd, str_sup_user_flag)
        Else
            str_sup_user_flag = Session("str_sup_user_flag")
        End If

        If str_sup_user_flag = "N" Then
            Session("skip_data_security") = SystemUtils.skipDataSecurity(empcd)
        Else
            Session("skip_data_security") = True
        End If

        If Not (cbo_finance_company.Items.Count > 0) Then
            PaymentUtils.PopulateFinanceProviders(txt_store_cd.Text, cbo_finance_company)
        End If
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim promoCd As String = ""
        Dim providerCd As String = ""

        If cbo_finance_company.SelectedIndex = 0 Then
            Add_Finance_Company() 'lucy add

        End If

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        'sql = "SELECT FIN_CUST_CD, PROMO_CD FROM SO_ASP, SO " &
        '       "WHERE SO.DEL_DOC_NUM=SO_ASP.DEL_DOC_NUM AND SO.DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"
        ' lucy add approval_cd
        sql = "SELECT FIN_CUST_CD, PROMO_CD ,so.approval_cd FROM SO_ASP, SO " &
                      "WHERE SO.DEL_DOC_NUM=SO_ASP.DEL_DOC_NUM AND SO.DEL_DOC_NUM='" & txt_del_doc_num.Text & "'"

        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            conn.Open()
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            If MyDataReader2.Read Then
                promoCd = MyDataReader2.Item("PROMO_CD").ToString
                providerCd = MyDataReader2.Item("FIN_CUST_CD").ToString
                txt_fi_appr.Text = MyDataReader2.Item("approval_cd").ToString   ' lucy 

            End If
            MyDataReader2.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If Not IsDBNull(providerCd) AndAlso providerCd.isNotEmpty Then
            If cbo_finance_company.SelectedIndex = 0 Then
                Add_Finance_Company() 'lucy  

            End If
            cbo_finance_company.SelectedValue = providerCd
        End If
        GetPromoCodes()
        If Not IsDBNull(promoCd) AndAlso promoCd.isNotEmpty Then
            cbo_promo.SelectedValue = promoCd
        End If

        Dim origFiInfo As New OrigFinanceInfo
        origFiInfo.finCo = cbo_finance_company.SelectedValue
        origFiInfo.promoCd = cbo_promo.SelectedValue
        origFiInfo.finAmt = lbl_financed.Text
        origFiInfo.acct = txt_fi_acct.Text
        origFiInfo.approval = txt_fi_appr.Text
        Session("ORIG_FI_INFO") = origFiInfo

        'if changes are not allowed because of pending changes, display a message
        If Not AllowFinanceChanges() Then
            Dim msg As String = "Finance information can only be viewed when the order has pending changes."
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "alert('" + msg + "');", True)
        End If
        ' *** set the proper state of components
        SetPropertiesForFinancePopup()

        'set the max. range for the finance amount validator
        SetMaxFinanceAmount()
        PopupModifyFinance.ShowOnPageLoad = True

        ' DB Oct 14, 2014
        If IsDBNull(txt_fi_appr.Text) = False Then
            Dim int_len As Integer
            int_len = Len(txt_fi_appr.Text)
            If int_len >= 2 Then
                Session("has_orig_fi") = "Y"   ' lucy
                txt_fi_appr.Enabled = False
            End If
        End If

        'Feb 22 disable buttons if Finalized or Void again if changed
        If txt_Stat_cd.Text.Equals("F") Or txt_Stat_cd.Text.Equals("V") Then
            btn_save_fi_co.Enabled = False
            btn_lucy_save.Enabled = False
            btn_lucy_rmv_fi.Enabled = False
        End If

    End Sub

    Protected Sub txt_fi_acct_TextChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_fi_acct.TextChanged

        If IsSwiped(txt_fi_acct.Text) Then
            'get the parsed acct# and exp date from the swiped track info 
            Dim swipedData = txt_fi_acct.Text
            txt_fi_acct.Text = GetAccountNumber(swipedData)
            'txt_fi_exp_dt.Text = GetExpirationDate(swipedData)
        End If

    End Sub


    Protected Sub btnPrintPickReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim UpPanel As UpdatePanel
        UpPanel = Master.FindControl("UpdatePanel1")
        ScriptManager.RegisterClientScriptBlock(UpPanel, UpPanel.GetType(), "Asegf43tg", "window.open('Invoices/PICR.aspx?DEL_DOC_NUM=" & txt_del_doc_num.Text & "','frdfgf43tg','height=700px,width=700px,top=50,left=50,status=no,toolbar=no,menubar=no,resizable=yes,scrollbars=1');", True)

    End Sub

    ''' <summary>
    ''' Fires when user clicks on the Tax Code link button that brings up the 'Update Tax' popup.
    ''' </summary>
    Protected Sub lnkModifyTax_Click(sender As Object, e As EventArgs) Handles lnkModifyTax.Click
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user makes the line VOID
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Voiding a line ")
            End If
            If (Not (Convert.ToBoolean(ViewState("CanModifyTax")))) Then
                Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
                ucMsgPopup.DisplayErrorMsg(Resources.POSMessages.MSG0009)
                msgPopup.ShowOnPageLoad = True
            Else

                Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
                hidIsInvokeManually.Value = "true"

                Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
                Dim hidZipCD As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)

                hidTaxDt.Value = txt_wr_dt.Text.Trim()
                If SecurityUtils.hasSecurity("SOMTAXC", Session("EMP_CD")) Then
                    hidZipCD.Value = String.Empty
                Else
                    If Session("PD") = "P" Then
                        If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                            Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                            hidZipCD.Value = store.addr.postalCd
                        End If
                    Else
                        hidZipCD.Value = txt_zip.Text
                    End If
                End If
                ucTaxUpdate.LoadTaxData()
                PopupControlTax.ShowOnPageLoad = True
            End If
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If
        Dim sm As ScriptManager = DirectCast(Master.FindControl("Scriptmanager1"), ScriptManager)
        sm.EnablePartialRendering = False
    End Sub

    Protected Sub txt_zip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If Not (AppConstants.Order.TYPE_CRM.Equals(txt_ord_tp_cd.Text) AndAlso Session("ORIG_DEL_DOC_NUM") + "" <> "") Then
            'Added the validation to avoid the tax popup in case of search.
            If Not String.IsNullOrEmpty(txt_del_doc_num.Text) Then
                determine_tax(txt_zip.Text)
            End If
        End If

        Update_City_State()
        'Added the validation to avoid the calcualtion in case of search.
        If Not String.IsNullOrEmpty(txt_del_doc_num.Text) Then
            calculate_total()
        End If
    End Sub

    Public Function determine_tax(ByVal zip_cd As String, Optional ByVal conversion As String = "N") As Boolean
        Dim isTaxPopUpDisplayed As Boolean = False
        If cbo_tax_exempt.Value & "" = "" Then
            Dim sql As String = ""
            Dim dbConnection As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            Dim dbReader As OracleDataReader

            If Session("PD") = "P" Then
                If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                    Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                    zip_cd = store.addr.postalCd
                End If
            Else
                zip_cd = txt_zip.Text
            End If
            zip_cd = UCase(zip_cd)

            Dim Use_Written As String = "N"
            If zip_cd.isNotEmpty Then
                sql = "SELECT USE_WR_ST_TAX_ON_PU,USE_WR_ST_TAX_ON_DEL FROM ZIP WHERE ZIP_CD='" & zip_cd & "'"
                dbCommand.CommandText = sql

                Try
                    dbConnection.Open()
                    dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                    If dbReader.Read() Then
                        If Session("PD") = "P" Then
                            Use_Written = dbReader.Item("USE_WR_ST_TAX_ON_PU").ToString
                        Else
                            Use_Written = dbReader.Item("USE_WR_ST_TAX_ON_DEL").ToString
                        End If
                    End If
                    dbReader.Close()

                    If Use_Written = "Y" Then
                        sql = "SELECT ZIP_CD FROM STORE WHERE STORE_CD='" & Session("store_cd") & "'"
                        dbCommand.CommandText = sql

                        dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

                        If dbReader.Read() Then
                            zip_cd = dbReader.Item("ZIP_CD")
                        End If
                        dbReader.Close()
                    End If
                Catch ex As Exception
                    Throw
                Finally
                    dbConnection.Close()
                End Try

                zip_cd = UCase(zip_cd)
                Dim MyTable As New DataTable
                Dim ds As DataSet = theSalesBiz.GetTaxCodesByZip(zip_cd)
                MyTable = ds.Tables(0)

                If (MyTable.Rows.Count = 1) Then
                    'if the ZIp code is mapped to only one tax code irr-respective of security the application will assign that tax code and no promt for tax.
                    Session("TAX_RATE") = MyTable.Rows(0).Item("SumofPCT").ToString
                    Session("TAX_CD") = MyTable.Rows(0).Item("TAX_CD").ToString
                    '** The 'manual' tax cd is the tax, before the user consciously modifies it( via the tax icon button). 
                    '** Since the system is auto assigning the tax, maunal code should be set to nothing.
                    Session("MANUAL_TAX_CD") = Nothing

                    lbl_tax_rate.Text = Session("TAX_RATE")
                    lbl_tax_cd.Text = Session("TAX_CD")
                    ' Added the below code as a part of MM-4240
                    ' The below code was adding an "-" symbol in the txtTaxCd textbox so making this change
                    If Not (String.IsNullOrWhiteSpace(Session("TAX_CD"))) Then
                        txtTaxCd.Text = Session("TAX_CD") & " - " & MyTable.Rows(0).Item("DES").ToString
                    Else
                        txtTaxCd.Text = String.Empty
                    End If
                ElseIf SecurityUtils.hasSecurity("SOMTAXC", Session("EMP_CD")) Then
                    Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
                    Dim hidZipCD As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
                    hidZipCD.Value = String.Empty
                    hidTaxDt.Value = txt_wr_dt.Text.Trim()
                    ucTaxUpdate.LoadTaxData()
                    PopupControlTax.ShowOnPageLoad = True
                Else
                    'MM-7026
                    'if logged in user do not have security then we need to displsy security based on the zip.
                    Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
                    hidIsInvokeManually.Value = "true"

                    Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)
                    Dim hidZipCD As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
                    hidTaxDt.Value = txt_wr_dt.Text.Trim()
                    If Session("PD") = "P" Then
                        If Not HttpContext.Current.Session("pd_store_cd") Is Nothing Then
                            Dim store As StoreData = theSalesBiz.GetStoreInfo(HttpContext.Current.Session("pd_store_cd").ToString)
                            hidZipCD.Value = store.addr.postalCd
                        End If
                    Else
                        hidZipCD.Value = zip_cd
                    End If
                    ucTaxUpdate.LoadTaxData()
                    PopupControlTax.ShowOnPageLoad = True
                    isTaxPopUpDisplayed = True
                End If
            End If
        Else
            lbl_tax_rate.Text = ""
            lbl_tax_cd.Text = ""
            lblTax.Text = "0.00"
        End If
        Return isTaxPopUpDisplayed

    End Function

    Public Sub Update_City_State()

        If Not String.IsNullOrEmpty(txt_zip.Text) Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objsql As OracleCommand
            Dim MyDataReader As OracleDataReader
            Dim sql As String

            conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            conn.Open()

            sql = "SELECT CITY, ST_CD FROM ZIP WHERE ZIP_CD='" & txt_zip.Text & "'"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objsql)

                'Store Values in String Variables 
                If MyDataReader.Read() Then
                    txt_city.Text = MyDataReader.Item("CITY").ToString
                    cbo_State.SelectedValue = MyDataReader.Item("ST_CD").ToString
                    conn.Close()
                End If
            Catch
                conn.Close()
                Throw
            End Try
        End If

    End Sub

    Protected Sub ASPxGridView4_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles ASPxGridView4.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then Return
        If e.GetValue("SER_NUM").ToString() & "" <> "" Then
            Dim hpl_serial As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(ASPxGridView4.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_serial"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
            If Not IsNothing(hpl_serial) Then
                hpl_serial.Text = e.GetValue("SER_NUM").ToString()
                hpl_serial.NavigateUrl = "SalesOrderMaintenance.aspx?ser_num=" & HttpUtility.UrlEncode(e.GetValue("SER_NUM").ToString()) & "&STORE_CD=" & HttpUtility.UrlEncode(lbl_ser_store_cd.Text) & "&ROW_ID=" & lbl_ser_row_id.Text & "&DEL_DOC_NUM=" & txt_del_doc_num.Text & "&query_returned=Y"
            End If
        End If

    End Sub '

    Private Sub GetComments()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim DEL_DOC_NUM, cust_cd As String

        DEL_DOC_NUM = Request("del_doc_num")
        cust_cd = Request("cust_cd")
        txt_del_comments.Text = ""

        If Not Session("scomments") Is Nothing Then
            txt_comments.Text = Session("scomments").ToString
        End If
        If Not Session("dcomments") Is Nothing Then
            txt_del_comments.Text = Session("dcomments").ToString
        End If
        If Not Session("arcomments") Is Nothing Then
            txt_ar_comments.Text = Session("arcomments").ToString
        End If

        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()
        Dim sqlQuery As New StringBuilder
        'sqlQuery.Append("Select TO_CHAR(SO_CMNT.DT, 'MM/DD/YYYY') AS DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & DEL_DOC_NUM & "'")
        'sqlQuery.Append(" AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD  ")
        'sqlQuery.Append(" AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM AND SO_CMNT.DEL_DOC_NUM=SO.del_doc_num  ORDER BY SEQ# ASC ")

        ' Daniela show all comments even for missing del_doc_num 
        sqlQuery.Append("Select TO_CHAR(SO_CMNT.DT, 'MM/DD/YYYY') AS DT, SO_CMNT.TEXT, SO_CMNT.CMNT_TYPE from SO_CMNT, SO Where SO.DEL_DOC_NUM = '" & DEL_DOC_NUM & "'")
        sqlQuery.Append(" AND SO_CMNT.SO_WR_DT=SO.SO_WR_DT AND SO_CMNT.SO_STORE_CD=SO.SO_STORE_CD  ")
        sqlQuery.Append(" AND SO_CMNT.SO_SEQ_NUM=SO.SO_SEQ_NUM ORDER BY SEQ# ASC, DT")


        sql = UCase(sqlQuery.ToString)

        Dim ds As DataSet = New DataSet
        Dim oAdp As OracleDataAdapter

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Dim curr_dt As String = ""
        Dim curr_tp As String = ""
        Dim dt As DataTable = ds.Tables.Add("SO_CMNT")
        Dim dr As DataRow

        Try
            ASPxGridView1.DataSource = ds
            ASPxGridView1.DataBind()
        Catch ex As Exception

        End Try

        ds.Clear()

        Dim ds3 As New DataSet
        sql = "SELECT SEQ#, TO_CHAR(CMNT_DT, 'MM/DD/YYYY') AS DT, TEXT FROM CUST_CMNT WHERE CUST_CD='" & txt_Cust_cd.Text & "' ORDER BY SEQ#"
        sql = UCase(sql)

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds3)

        Try
            ASPxGridView2.DataSource = ds3
            ASPxGridView2.DataBind()
        Catch ex As Exception

        End Try
        conn.Close()

    End Sub

    Protected Sub btn_save2_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("scomments") = txt_comments.Text
        Session("dcomments") = txt_del_comments.Text
        Session("arcomments") = txt_ar_comments.Text

    End Sub

    Protected Sub MyDataGrid_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles MyDataGrid.ItemDataBound

        Dim txt_fld_value As TextBox
        txt_fld_value = CType(e.Item.FindControl("txt_fld_value"), TextBox)
        If Not txt_fld_value Is Nothing Then
            If e.Item.Cells(2).Text & "" <> "" And e.Item.Cells(2).Text <> "&nbsp;" Then
                txt_fld_value.Visible = True
                txt_fld_value.MaxLength = e.Item.Cells(1).Text
                Select Case e.Item.Cells(0).Text
                    Case "USR_FLD_1"
                        txt_fld_value.Text = Session("USR_FLD_1")
                    Case "USR_FLD_2"
                        txt_fld_value.Text = Session("USR_FLD_2")
                    Case "USR_FLD_3"
                        txt_fld_value.Text = Session("USR_FLD_3")
                    Case "USR_FLD_4"
                        txt_fld_value.Text = Session("USR_FLD_4")
                    Case "USR_FLD_5"
                        txt_fld_value.Text = Session("USR_FLD_5")
                End Select
            End If
            If txt_Stat_cd.Text <> "O" AndAlso btn_Lookup.Visible = False Then
                txt_fld_value.Enabled = False
            End If
        End If

    End Sub

    Protected Sub UDF_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim txt_fld_value As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(txt_fld_value.NamingContainer, DataGridItem)
        Select Case dgItem.Cells(0).Text
            Case "USR_FLD_1"
                Session("USR_FLD_1") = txt_fld_value.Text
            Case "USR_FLD_2"
                Session("USR_FLD_2") = txt_fld_value.Text
            Case "USR_FLD_3"
                Session("USR_FLD_3") = txt_fld_value.Text
            Case "USR_FLD_4"
                Session("USR_FLD_4") = txt_fld_value.Text
            Case "USR_FLD_5"
                Session("USR_FLD_5") = txt_fld_value.Text
        End Select

    End Sub

    Protected Sub txt_comments_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_comments.TextChanged

        Session("scomments") = txt_comments.Text
        GetComments()
        txt_del_comments.Focus()

    End Sub

    Protected Sub txt_del_comments_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_del_comments.TextChanged

        Session("dcomments") = txt_del_comments.Text
        GetComments()
        txt_ar_comments.Focus()

    End Sub

    Protected Sub txt_ar_comments_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_ar_comments.TextChanged

        Session("arcomments") = txt_ar_comments.Text
        GetComments()

    End Sub

    ''' <summary>
    ''' Returns a flag indicating if the order is an 'open', sale order
    ''' </summary>
    ''' <returns></returns>
    Private Function IsOpenSaleOrder() As Boolean

        Return (txt_del_doc_num.Text.isNotEmpty AndAlso
                AppConstants.Order.TYPE_SAL = txt_ord_tp_cd.Text.Trim AndAlso
                AppConstants.Order.STATUS_OPEN = txt_Stat_cd.Text)

    End Function

    ''' <summary>
    ''' Checks if for an open, sale order that has 'ARS' enabled for its written store, avail. dt info 
    ''' should be recalculated. If availability is recalculated, then the line display is also updated.
    ''' </summary>
    Private Sub CheckForAvailDtUpdate()
        If (IsOpenSaleOrder() OrElse AppConstants.Order.TYPE_CRM = txt_ord_tp_cd.Text) AndAlso Session("STORE_ENABLE_ARS") Then

            'For an open order that has 'ARS' enabled for its written store, recalculate avail. dt info if:
            ' 1. the order's TM when it was originally retrieved is different than what it is now 
            ' 2. the TM is had not changed, but curr and new zone are difft. Curr zone will always be populated, new may or may not be
            Dim zoneChanged As Boolean = lbl_curr_zone.Text.isNotEmpty AndAlso lbl_new_zone.Text.isNotEmpty AndAlso Not (lbl_curr_zone.Text.Equals(lbl_new_zone.Text))
            Dim recalcAvail As Boolean = Not txt_pd_display.Text.Equals(Session("PD")) OrElse zoneChanged

            If recalcAvail Or ASPxPageControl1.ActiveTabIndex = 5 Then

                Dim isInventory As Boolean
                Dim lblAvailResults As Label
                Dim totalRows As Integer = GridView1.Items.Count - 1
                Dim dgItem As DataGridItem
                Dim soLnRowDetails As DataRow = Nothing
                'reset the viewstate 
                ViewState("MaxAvailDdate") = String.Empty
                Dim storeCode As String = String.Empty
                Dim locationCode As String = String.Empty
                Dim isOutletLoc As Boolean = False
                Dim isShowroomLoc As Boolean = False
                Dim availDt As DateTime = Today.Date
                Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
                dbConnection.Open()
                Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConnection)

                Try
                    For row As Integer = 0 To totalRows
                        dgItem = GridView1.Items(row)
                        'if the line is voided, dont consider the availability date of the item.
                        If (CType(dgItem.FindControl("ChkBoxVoid"), CheckBox)).Checked Then
                            Continue For
                        End If

                        isInventory = ResolveYnToBooleanEquiv(dgItem.Cells(SoLnGrid.INVENTORY).Text.Trim())

                        ' update the current values into the datarow being reviewed
                        soLnRowDetails = getSoLnDatTblRow(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text)

                        Dim txtQty As TextBox = CType(dgItem.FindControl("qty"), TextBox)
                        isOutletLoc = False
                        isShowroomLoc = False
                        storeCode = CType(dgItem.FindControl("store_cd"), TextBox).Text
                        locationCode = CType(dgItem.FindControl("loc_cd"), TextBox).Text
                        If (storeCode.isNotEmpty AndAlso locationCode.isNotEmpty) Then
                            isOutletLoc = theInvBiz.IsOutletLocation(storeCode, locationCode, dbCommand)
                        End If
                        ' DB Jan 11 2018
                        If (Not isOutletLoc And (storeCode.isNotEmpty AndAlso locationCode.isNotEmpty)) Then
                            isShowroomLoc = LeonsBiz.IsShowroomLocation(storeCode, locationCode)
                        End If

                        If (isOutletLoc) Then
                            availDt = Today.Date
                            If Not (Session("pd_store_cd").Equals(storeCode)) Then
                                If (IsNothing(ViewState("zoneLeadDays")) AndAlso String.IsNullOrWhiteSpace(ViewState("zoneLeadDays"))) Then
                                    ViewState("zoneLeadDays") = theTMBiz.GetZoneLeadDays(SessVar.zoneCd)
                                End If
                                availDt = availDt.AddDays(ViewState("zoneLeadDays"))
                            End If
                        Else
                            'Daniela Nov 10 enable CRM dates 
                            'If (Session("IsARSStore")) Then availDt = GetAvailDate(soLnRowDetails)
                            ' DB Jan 11 2018 showroom excluded
                            If (Session("IsARSStore") AndAlso AppConstants.Order.TYPE_CRM <> txt_ord_tp_cd.Text And Not isShowroomLoc) Then
                                availDt = GetAvailDate(soLnRowDetails)
                            End If
                        End If
                        ' DB Jan 11 2018 showroom added
                        If (isShowroomLoc And Session("IsARSStore")) Then
                            availDt = Today.Date
                            If Not (Session("pd_store_cd").Equals(storeCode)) Then
                                If (IsNothing(ViewState("zoneLeadDays")) AndAlso String.IsNullOrWhiteSpace(ViewState("zoneLeadDays"))) Then
                                    ViewState("zoneLeadDays") = theTMBiz.GetZoneLeadDays(SessVar.zoneCd)
                                End If
                                availDt = availDt.AddDays(ViewState("zoneLeadDays"))
                            End If
                        End If

                        If (Not String.IsNullOrWhiteSpace(ViewState("MaxAvailDdate"))) AndAlso IsDate(ViewState("MaxAvailDdate")) Then
                            If (Convert.ToDateTime(ViewState("MaxAvailDdate")) < availDt) Then
                                ViewState("MaxAvailDdate") = availDt
                            End If
                        Else
                            ViewState("MaxAvailDdate") = availDt
                        End If

                        lblAvailResults = CType(dgItem.FindControl("lblAvailResults"), Label)
                        Dim lbl_poDes As Label = CType(dgItem.FindControl("lbl_PO"), Label)
                        If (Session("IsARSStore")) Then
                            If Not (availDt = Date.MinValue) And isInventory Then
                                lblAvailResults.Visible = True
                                lblAvailResults.Text = Resources.POSMessages.MSG0002 & FormatDateTime(availDt, DateFormat.ShortDate)
                            Else
                                ' Daniela fix 
                                lblAvailResults = CType(dgItem.FindControl("lblAvailResults"), Label)
                                ' End Daniela fix  
                                lblAvailResults.Visible = False
                            End If
                        Else
                            ViewState("MaxAvailDdate") = Today.Date
                            lblAvailResults.Visible = False
                        End If
                        If IsNothing(lbl_poDes) = False AndAlso Not lbl_poDes.Text.Trim().Equals(String.Empty) Then
                            'Means the line item is linked to PO, hide the item available available date field.
                            'Check if the availabale date is less thna the PO arrival date, if true, then display the available date and consider this date as available date.
                            lblAvailResults.Visible = False

                        Else
                            'Do Nothing
                        End If
                    Next
                    dbConnection.Close()
                Catch ex As Exception
                    Throw
                Finally
                    dbConnection.Dispose()
                    dbCommand.Dispose()
                End Try
            End If
        End If
    End Sub

    ''' <summary>
    ''' This fires when the change is in the process of changing but hasn't changed yet. 
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    Private Sub ASPxPageControl1_ActiveTabChanging(source As Object, e As DevExpress.Web.ASPxTabControl.TabControlCancelEventArgs) Handles ASPxPageControl1.ActiveTabChanging

        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
            PromptForSummaryPriceMgrOverride()
            ViewState("EventName") = "PageControlActiveTabChange"
            ViewState("NewPageIndex") = DirectCast(e.Tab, DevExpress.Web.ASPxTabControl.TabPage).Index
        Else
            PageControlActiveTabChange(e)
        End If
        cboTargetDate.Value = String.Empty
    End Sub

    Private Sub PageControlActiveTabChange(e As DevExpress.Web.ASPxTabControl.TabControlCancelEventArgs)

        Dim idx As Integer = 0
        If Integer.TryParse(ViewState("NewPageIndex"), idx) Then
            'DirectCast(e.Tab, DevExpress.Web.ASPxTabControl.TabPage).Index = idx
            ASPxPageControl1.ActiveTabIndex = idx
        End If



        If txt_Cust_cd.Text.isNotEmpty Then

            If ASPxPageControl1.ActiveTabIndex = 5 Then  'pickup/delivery TAB
                If IsOpenSaleOrder() AndAlso (Session("STORE_ENABLE_ARS") OrElse SysPms.isPriorityFillByZone) AndAlso
                    (IsNothing(Session("ZONE_CD")) OrElse Session("ZONE_CD").ToString.isEmpty) Then
                    'if no zone found and user is trying to navigate away from the del/pkp tab, display error
                    lblTMError.Text = Resources.POSErrors.ERR0004
                    e.Cancel = True
                End If
            End If
        End If

        'MM-5661
        If ASPxPageControl1.ActiveTabIndex = 2 Then  'Discount TAB
            If IsOpenSaleOrder() Then

                Dim discountGrid As DevExpress.Web.ASPxGridView.ASPxGridView = ucDiscount.FindControl("grdDiscItems")
                Dim discountRowCount As Integer = discountGrid.VisibleRowCount
                'MM-9870
                If (SysPms.discWithAppSec = "W") AndAlso (Not IsNothing(ViewState("IsDiscountAdded")) AndAlso ViewState("IsDiscountAdded") AndAlso (Session("mgrApproval") <> "Y") AndAlso (discountRowCount > 0) AndAlso (AppConstants.ManagerOverride.BY_ORDER.Equals(SalesUtils.GetPriceMgrOverride()))) Then
                    If Not IsNothing(Session("IsCanceled")) AndAlso Session("IsCanceled").Equals(True) Then
                        ucDiscount.HideManagerPopUp()
                    Else
                        ucDiscount.ShowManagerPopUp()
                        e.Cancel = True
                    End If
                End If
            End If
        End If

    End Sub

    ''' <summary>
    ''' Executes when the tab changes by the user clicking on a different one.
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    Protected Sub ASPxPageControl1_ActiveTabChanged(ByVal source As Object, ByVal e As DevExpress.Web.ASPxTabControl.TabControlEventArgs) Handles ASPxPageControl1.ActiveTabChanged
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                Dim result As Boolean = accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicking on different tabs ")
                'If Not (accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicking on different tabs ")) Then
                If Not (result) Then
                    ModifyControls(1)
                    Exit Sub
                End If
            End If
            'shows or hides depending on the TAB selection made
            Dim isOrderLnsTabSelected As Boolean = (ASPxPageControl1.ActiveTabIndex = 1)
            ChkBoxViewSlspersons.Visible = isOrderLnsTabSelected
            chk_calc_avail.Visible = isOrderLnsTabSelected
            chk_reserve_all.Visible = isOrderLnsTabSelected
            If ASPxPageControl1.ActiveTabIndex = 4 Then
                PopulateUDF()
            End If

            If txt_Cust_cd.Text.isNotEmpty Then

                If ASPxPageControl1.ActiveTabIndex = 1 Then
                    'this is the LINE DETAILS tab
                    CheckForAvailDtUpdate()
                ElseIf ASPxPageControl1.ActiveTabIndex = 2 Then
                    ucDiscount.ReqSource = "SOM"
                    ucDiscount.LoadDiscount(txt_Stat_cd.Text)
                ElseIf ASPxPageControl1.ActiveTabIndex = 3 Then
                    GetComments()
                    'ElseIf ASPxPageControl1.ActiveTabIndex = 4 Then
                    '    Populate_UDF()
                ElseIf ASPxPageControl1.ActiveTabIndex = 5 Then
                    'this is the PICKUP/DELIVERY tab         
                    CheckForAvailDtUpdate()
                    If Session("PD").Equals(AppConstants.DELIVERY) Then
                        'DisableDeliveryDates()
                        'Commented the above and added the call to grid_del_date_Binddata bscause of calcuating the points for the sale
                        grid_del_date_Binddata()
                    Else
                        setPickupDate()
                    End If
                    If Not IsPostBack Then
                        lbl_new_tp.Text = ""
                        lblTMError.Text = ""
                        Dim zoneReqd = theInvBiz.RequireZoneCd(lbl_curr_tp.Text,
                                                               Session("STORE_ENABLE_ARS"),
                                                               txt_ord_tp_cd.Text)
                        If zoneReqd Then
                            UpdateZone(If(AppConstants.DELIVERY = lbl_curr_tp.Text,
                                       txt_zip.Text,
                                       Session("ZONE_ZIP_CD")), Session("emp_init"))
                        ElseIf (AppConstants.PICKUP = lbl_curr_tp.Text) Then
                            Calendar1.Visible = True
                        End If
                    End If

                    btn_Save.Enabled = True
                    If ConfigurationManager.AppSettings("delivery_options").ToString = "TIERED" Then
                        lbl_new_charges.Text = lbl_curr_charges.Text
                    End If
                End If
            End If
        End If
    End Sub


    ''' <summary>
    ''' Fires when the Transportation Method(PICKUP/DELIVERY) d.d. value changes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub cbo_PD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_PD.SelectedIndexChanged

        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          ' lucy 25-may-16
            lbl_final.Text = strErr
            Exit Sub
        End If
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'Clear the error message from previous actions
            lblTMError.Text = String.Empty
            'When user changes to pickup/delivery from the dropdown inside the Pickup/Delivery tab        
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Changed to Pickup/Delivery")
            End If
            Session("PD") = cbo_PD.Value
            ResetStoreInfo()
            lbl_new_tp.Text = cbo_PD.Value
            lbl_new_zone.Text = String.Empty

            'Condition added to mandate the Pickup/Delivery date if the P/D changed and zone code is required to make correct reservation
            If (theInvBiz.RequireZoneCd(AppConstants.PICKUP, Session("STORE_ENABLE_ARS"), txt_ord_tp_cd.Text.Trim())) Then
                lbl_new_dt.Text = String.Empty
            End If
            If (AppConstants.DELIVERY.Equals(cbo_PD.Value)) Then

                '*** means TM is being changed to a 'Delivery'
                UpdateZone(txt_zip.Text, Session("emp_init"))

                If Not (AppConstants.Order.TYPE_CRM.Equals(txt_ord_tp_cd.Text) AndAlso Session("ORIG_DEL_DOC_NUM") + "" <> "") Then
                    determine_tax(txt_zip.Text)
                End If

                calculate_total()
                'show the 'change zone' button since it is being changed to a delv
                ASPxButton2.Visible = True
                DisableDeliveryDates()

            ElseIf (AppConstants.PICKUP.Equals(cbo_PD.Value)) Then

                '*** means TM is being changed to a 'PICKUP'
                lbl_pageinfo.Text = ""
                lblDesc.Text = ""
                Session("del_chg") = System.DBNull.Value.ToString
                Session("del_dt") = System.DBNull.Value.ToString
                lbl_new_charges.Text = ""
                lbl_new_dt.Text = ""
                lblDelivery.Text = "0.00"


                If (theInvBiz.RequireZoneCd(AppConstants.PICKUP, Session("STORE_ENABLE_ARS"), txt_ord_tp_cd.Text.Trim())) Then
                    UpdateZone(Session("ZONE_ZIP_CD"), Session("emp_init"))
                Else
                    Calendar1.Visible = True
                    GridView2.Visible = False
                    navdt_table.Visible = False
                    grid_del_date.Visible = False
                    nav_table.Visible = False
                    btnPrev.Enabled = False
                    btnLast.Enabled = False
                    btnNext.Enabled = False
                    btnFirst.Enabled = False
                    btnprev2.Enabled = False
                    btnlast2.Enabled = False
                    btnnext2.Enabled = False
                    btnfirst2.Enabled = False
                    btnfirst2.Visible = False
                    btnprev2.Visible = False
                    btnnext2.Visible = False
                    btnlast2.Visible = False
                End If

                If Not (AppConstants.Order.TYPE_CRM.Equals(txt_ord_tp_cd.Text) AndAlso Session("ORIG_DEL_DOC_NUM") + "" <> "") Then
                    determine_tax(txt_zip.Text)
                End If
                CheckForAvailDtUpdate()
                calculate_total()
                ASPxButton2.Visible = False  'hide the 'change zone'  button
                setPickupDate()

            End If
        End If
    End Sub

    'Executes when the value in the store d.d. changes on the TM tab
    Protected Sub PD_Store_Update(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          'lucy 25-may-16
            lbl_final.Text = strErr
            Exit Sub
        End If

        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'Clear the error message from previous actions
            lblTMError.Text = String.Empty
            'When user changes to pickup/delivery from the dropdown inside the Pickup/Delivery tab        
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Changed Pickup/Delivery store")
            End If
            Dim isTaxPopUpDisplayed As Boolean = False
            If cbo_pd_store_cd.Value & "" <> "" Then
                'After the selection of the store, it's details need to be updated. then only the proper zones are populated for the selected store.
                ResetStoreInfo(cbo_pd_store_cd.Value.ToString().Trim().ToUpper())
                Session("pd_store_cd") = cbo_pd_store_cd.Value
                lbl_new_tp.Text = cbo_PD.Value
                lbl_new_zone.Text = lbl_curr_zone.Text
                lbl_new_charges.Text = lbl_curr_charges.Text
                lbl_new_dt.Text = lbl_curr_dt.Text
                lbl_new_pd_store.Text = cbo_pd_store_cd.Value
                'Condition added to mandate the Pickup/Delivery date if the P/D changed and zone code is required to make correct reservation
                If (theInvBiz.RequireZoneCd(AppConstants.PICKUP, Session("STORE_ENABLE_ARS"), txt_ord_tp_cd.Text.Trim())) Then
                    lbl_new_dt.Text = String.Empty
                End If
                'instead of passing the Customer's ZIP (txt_zip.Text), passes blank so the app
                'fetches the proper tax rate, using the ZIP for the store selected 
                ' if we are processing a return to original document, then we already have tax from orig, even store change doesn't impact
                If Not (AppConstants.Order.TYPE_CRM.Equals(txt_ord_tp_cd.Text) AndAlso Session("ORIG_DEL_DOC_NUM") + "" <> "") Then
                    isTaxPopUpDisplayed = determine_tax("")
                End If

                If Not (isTaxPopUpDisplayed) Then
                    calculate_total()
                End If

                If (theInvBiz.RequireZoneCd(cbo_PD.Value, Session("STORE_ENABLE_ARS"), txt_ord_tp_cd.Text.Trim())) Then
                    UpdateZone(Session("ZONE_ZIP_CD"), Session("emp_init"))
                    lbl_new_zone.Text = Session("ZONE_CD")
                ElseIf (cbo_PD.Value = "P" AndAlso Session("STORE_ENABLE_ARS") = False) Then
                    GridView2.Visible = False
                End If
                If (AppConstants.PICKUP = Session("PD")) Then
                    setPickupDate()
                Else
                    DisableDeliveryDates()
                End If

            End If
            ' Added as a part of MM-7088
            ' TO enable or disable the store, location fields when the store code has been changed
            If (AppConstants.Order.TYPE_CRM = (Session("ord_tp_cd")) AndAlso AppConstants.Order.STATUS_FINAL = cbo_stat_cd.SelectedValue) Then
                StoreLocationActivity()
            End If
        End If
    End Sub

    ' Added as a part of MM-7088
    ''' <summary>
    ''' Enabling/Disabling the store, location field in SOM+ at the time of CRM finalization
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub StoreLocationActivity()
        Dim isInventory As Boolean = False
        Dim soLnsDataSet As DataSet = Session("DEL_DOC_LN#")
        Dim soLnsDatTbl As DataTable = soLnsDataSet.Tables(0)
        'Dim itmTypeCode As String = String.Empty
        'show a msg based on if ars store and cannot do reservations        

        For Each soLnRow As DataGridItem In GridView1.Items
            'Finds the row in the DATASET to get a hold of the complete details for the line
            Dim soLnRowDetails As DataRow = Nothing
            For Each lnRow In soLnsDatTbl.Rows
                If lnRow("DEL_DOC_LN#") = soLnRow.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text Then
                    soLnRowDetails = lnRow
                    'itmTypeCode = soLnRowDetails.Item("TYPECODE").ToString
                    Exit For
                End If
            Next

            'STORE and LOCATION should be enabled on the following conditions: 
            '1) line has an INVENTORY sku  AND
            '2) it is an open Sale Line OR
            '   2.1) If it is an ARS store, then also it has to be paid in full
            '3) it is NOT linked to a PO
            '3) it is a CRM being finalized
            Dim linkedToPO As Boolean = Not IsNothing(soLnRowDetails) AndAlso
                                        Not IsDBNull(soLnRowDetails("PO_CD")) AndAlso
                                        Not String.IsNullOrEmpty(soLnRowDetails("PO_CD"))
            ' While finalizing the CRM store code and location code needs to disbled so I have removed the condtion "isFinalizingCRM"
            ' MM - 5672
            ' When CRM is about to finalize then disabling the store, location fields inside the grid

            isInventory = ResolveYnToBooleanEquiv(soLnRow.Cells(SoLnGrid.INVENTORY).Text.Trim())

            Dim isStoreFromInvXref As Boolean = Convert.ToBoolean(soLnRow.Cells(SoLnGrid.IsStoreFromInvXref).Text.Trim())
            Dim isStoreHasValue As Boolean = False
            Dim getStoreCode As TextBox = CType(soLnRow.FindControl("store_cd"), TextBox)
            If (((getStoreCode) IsNot Nothing) AndAlso (Not String.IsNullOrWhiteSpace(getStoreCode.Text))) Then
                isStoreHasValue = True
            End If

            ' MM - 946 / 5762
            ' For Non Inventory type SKU's store and location fields will be disabled
            If (Not isInventory) Then
                soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = False
                soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = False
            ElseIf (isInventory AndAlso (Not linkedToPO) AndAlso (isStoreFromInvXref) AndAlso (isStoreHasValue)) Then
                soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = False
                soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = False
            ElseIf (SysPms.isRFEnabled AndAlso theInvBiz.IsRfStore(Session("pd_store_cd"))) Then
                soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = False
                soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = False
            Else
                soLnRow.Cells(SoLnGrid.STORE_CD).Enabled = True
                soLnRow.Cells(SoLnGrid.LOC_CD).Enabled = True
            End If
        Next
    End Sub

    ''' <summary>
    ''' Resets the store in the pickup/delivery store d.d. to be the one based on the
    ''' written store code, when the order was originally retrieved. This typically happens when
    ''' the TM like the pickup/delivery is changed. Therefore, any intermediate change to the store
    ''' is overwritten.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ResetStoreInfo()

        If txt_store_cd.Text.isNotEmpty Then
            Session("store_cd") = txt_store_cd.Text
            Try
                Dim store As StoreData = theSalesBiz.GetStoreInfo(txt_store_cd.Text.ToString.Trim)
                If Session("PD") = "P" Then
                    cbo_pd_store_cd.Value = If(store.puStoreCd.isEmpty, Session("store_cd"), store.puStoreCd)
                    Session("ZONE_ZIP_CD") = store.addr.postalCd
                Else
                    cbo_pd_store_cd.Value = store.shipToStoreCd
                End If
                'MCCL LSS change Nov 24
                'Session("CO_CD") = store.coCd
                Session("pd_store_cd") = cbo_pd_store_cd.Value
                lbl_new_pd_store.Text = cbo_pd_store_cd.Value

            Catch ex As Exception
                Throw
            End Try
        End If

    End Sub
    ''' <summary>
    ''' Resets the particular store in the pickup/delivery store d.d. to be the one based on the
    ''' written store code, when the order was originally retrieved. This typically happens when
    ''' the TM like the pickup/delivery is changed. Therefore, any intermediate change to the store
    ''' is overwritten.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ResetStoreInfo(StoreCD As String)

        If txt_store_cd.Text.isNotEmpty Then
            Session("store_cd") = StoreCD
            Try
                Dim store As StoreData = theSalesBiz.GetStoreInfo(StoreCD)
                If Session("PD") = "P" Then
                    Session("STORE_ENABLE_ARS") = store.enableARS
                    Session("ZONE_ZIP_CD") = store.addr.postalCd
                Else
                    'cbo_pd_store_cd.Value = store.shipToStoreCd
                    lblTMError.Text = String.Empty
                End If
                'MCCL LSS change Nov 24
                'Session("CO_CD") = store.coCd
                Session("pd_store_cd") = cbo_pd_store_cd.Value
                lbl_new_pd_store.Text = cbo_pd_store_cd.Value

            Catch ex As Exception
                Throw
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Retrieves the ZONES in the system, if no zones are found for the postal code passed in,
    ''' then ALL zones may be retrieved if ConfigurationManager.AppSettings("open_zones") is Y.
    ''' </summary>
    ''' <param name="postalCd">the postal code to retrieve ZONES for</param>
    ''' <returns>number of rows placed in the GridView2 dataset</returns>
    Private Function UpdateZonesDisplay(ByVal postalCd As String, ByVal empInit As String) As Integer
        Dim numRows As Integer = 0
        Try
            'Dim dsZones As DataSet = theTMBiz.GetZoneCodesForZip(postalCd)

            Dim dsZones As DataSet = theTMBiz.lucy_GetZoneCodesForZip(postalCd, Session("CO_CD"))

            ' if NO zones are found, it may have to fetch them all
            If (dsZones.Tables(0).Rows.Count = 0 AndAlso
                ConfigurationManager.AppSettings("open_zones").ToString = "Y") Then

                'dsZones = theTMBiz.GetZoneCodes()
                dsZones = theTMBiz.GetZoneCodes(empInit) ' Daniela Feb 16, 25-may-15 lucy comment for compiling
            End If
            numRows = dsZones.Tables(0).Rows.Count

            GridView2.DataSource = dsZones.Tables(0).DefaultView
            GridView2.DataBind()

        Catch ex As Exception
            Throw
        End Try
        Return numRows

    End Function

    ''' <summary>
    ''' Sets the ZONE and its related values, using the values found in
    ''' the datarow passed in
    ''' </summary>
    ''' <param name="zoneRow">details for the zone selected</param>
    Private Sub SetZoneAndRelatedValues(ByVal zoneRow As GridViewRow)

        Dim zone As String = Server.HtmlDecode(zoneRow.Cells(0).Text)
        Session("ZONE_CD") = zone
        ViewState("zoneLeadDays") = Nothing
        lbl_new_zone.Text = zone
        lbl_new_pd_store.Text = cbo_pd_store_cd.Value
        lbl_new_tp.Text = cbo_PD.Value

        If (AppConstants.PICKUP = cbo_PD.Value) Then
            Calendar1.Visible = True
            setPickupDate()
            Dim MaxDelDate As Date = Convert.ToDateTime(ViewState("MaxAvailDdate").ToString())
            If MaxDelDate < Date.Today Then
                MaxDelDate = Date.Today
            Else
                MaxDelDate = Convert.ToDateTime(ViewState("MaxAvailDdate").ToString())
            End If
            If (Convert.ToDateTime(lbl_curr_dt.Text) >= MaxDelDate) Then
                lbl_new_dt.Text = lbl_curr_dt.Text
                Calendar1.SelectedDate = lbl_new_dt.Text
            End If
        ElseIf (AppConstants.DELIVERY = cbo_PD.Value) Then

            Dim delvStore As String = Server.HtmlDecode(zoneRow.Cells(2).Text)
            Dim defDelChg As String = Server.HtmlDecode(zoneRow.Cells(3).Text)
            Dim defSetChg As String = Server.HtmlDecode(zoneRow.Cells(4).Text)

            SetCharges(defDelChg, defSetChg)

            'STORE should be set based on the ZCM setup when present
            SetDeliveryStore(delvStore)

            lbl_new_dt.Text = ""
            grid_del_date.Visible = True
            navdt_table.Visible = True
            Dim IsChangedToDelivery As Boolean = True
            grid_del_date_Binddata(IsChangedToDelivery)
        End If

    End Sub

    ''' <summary>
    ''' Updates the ZONE display accordingly, for Deliveries (or pickups when applicable)
    ''' </summary>
    Private Sub UpdateZone(ByVal postalCd As String, ByVal empInit As String)

        Session("ZONE_CD") = Nothing
        ViewState("zoneLeadDays") = Nothing
        lblTMError.Text = String.Empty
        Calendar1.Visible = False       'pkup dates calendar
        grid_del_date.Visible = False   'delv dates grid
        navdt_table.Visible = False     'delv dates navigation buttons
        GridView2.Visible = False       'zones grid
        GridView2.PageIndex = 0         'displays first page upon initial retrieval
        nav_table.Visible = False       'zones navigation buttons

        Dim numRows As Integer = UpdateZonesDisplay(postalCd, empInit) ' Daniela
        If (numRows = 0) Then
            lbl_pageinfo.Text = ""

            ' Daniela Nov 11
            'lblDesc.Text = "Sorry, no zones were found for the specified zip code.  Please re-enter"
            lblDesc.Text = "Sorry, no zones were found for the specified zip code.  Please re-enter or use the postal code of the pickup or delivery store."

            lblDesc.Visible = True

        ElseIf (numRows = 1) Then
            'ONE ZONE found
            SetZoneAndRelatedValues(GridView2.Rows(0))
        Else
            'MULTIPLE ZONES found, therefore displays grid to force user to choose
            lblTMError.Text = Resources.POSMessages.MSG0003
            nav_table.Visible = True
            GridView2.Visible = True
            SetZonesNavigation()
        End If
    End Sub

    ''' <summary>
    ''' Enables / Disables the buttons used for ZONE navigation
    ''' </summary>
    Private Sub SetZonesNavigation()
        lbl_pageinfo.Text = "Displaying Page " + CStr(GridView2.PageIndex + 1) + " of " + CStr(GridView2.PageCount)
        lbl_pageinfo.Visible = True

        Dim multiPage As Boolean = (GridView2.PageCount > 1)
        Dim atFirstPage As Boolean = (GridView2.PageIndex = 0)
        Dim atLastPage As Boolean = (GridView2.PageIndex = (GridView2.PageCount - 1))

        btnFirst.Visible = True
        btnPrev.Visible = True
        btnNext.Visible = True
        btnLast.Visible = True

        btnFirst.Enabled = multiPage AndAlso Not atFirstPage
        btnPrev.Enabled = multiPage AndAlso Not atFirstPage
        btnNext.Enabled = multiPage AndAlso Not atLastPage
        btnLast.Enabled = multiPage AndAlso Not atLastPage
    End Sub

    ''' <summary>
    ''' Sets the DELIVERY/SETUP charges passed in
    ''' </summary>
    ''' <param name="delvChg">the delivery charge to be used</param>
    ''' <param name="setupChg">the setup charge to be used</param>
    Private Sub SetCharges(ByVal delvChg As String, ByVal setupChg As String)

        Dim newChgs As String = If(ConfigurationManager.AppSettings("delivery_options").ToString = "TIERED",
                                  lbl_curr_charges.Text,
                                  delvChg)

        lbl_new_charges.Text = If(IsNumeric(newChgs), FormatNumber(CDbl(newChgs), 2), FormatNumber(0, 2))
        lblDelivery.Text = lbl_new_charges.Text
        hidDeliveryCharges.Value = lbl_new_charges.Text
        lblSetup.Text = If(IsNumeric(setupChg), FormatNumber(CDbl(setupChg), 2), FormatNumber(0, 2))

    End Sub

    ''' <summary>
    ''' The DELIVERY STORE needs to be the one defined for the Zone in use, 
    '''  (when available) as defined in the ZCM E1 screen
    ''' </summary>
    ''' <param name="zcmStore">Delivery store defined for the ZONE in use</param>
    ''' <remarks></remarks>
    Private Sub SetDeliveryStore(ByVal zcmStore As String)
        If (Not IsDBNull(zcmStore) AndAlso zcmStore.isNotEmpty()) Then
            cbo_pd_store_cd.SelectedItem = cbo_pd_store_cd.Items.FindByValue(zcmStore)
        Else
            Dim storeObj As StoreData = theSalesBiz.GetStoreInfo(txt_store_cd.Text.Trim)
            If Not IsNothing(storeObj) AndAlso storeObj.shipToStoreCd.isNotEmpty Then
                cbo_pd_store_cd.SelectedItem = cbo_pd_store_cd.Items.FindByValue(storeObj.shipToStoreCd.Trim)
            Else
                cbo_pd_store_cd.SelectedItem = cbo_pd_store_cd.Items.FindByValue(txt_store_cd.Text.Trim)
            End If
        End If

        Session("pd_store_cd") = cbo_pd_store_cd.Value
        lbl_new_pd_store.Text = cbo_pd_store_cd.Value
        Try
            Dim store As StoreData = theSalesBiz.GetStoreInfo(lbl_new_pd_store.Text.Trim)
            'MCCL LSS change Nov 24, 2017
            'Session("CO_CD") = store.coCd
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Executes when the user clicks on an editable component- in this case the 'SELECT'  button- in the ZONES grid.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Currently executes the "SELECT" on th ZONEs table</remarks>
    Protected Sub GridView2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView2.SelectedIndexChanged
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " User clicked SELECT button on Zone grid ")
            End If
            lblTMError.Text = String.Empty
            GridView2.Visible = False
            nav_table.Visible = False
            'reset the page index to 0 when the zone is changed.
            If Not IsNothing(grid_del_date) AndAlso grid_del_date.PageCount >= 0 Then
                grid_del_date.CurrentPageIndex = 0
            End If
            SetZoneAndRelatedValues(GridView2.SelectedRow)

            btn_Save.Enabled = True
            calculate_total()
            DisableDeliveryDates()
        End If
    End Sub

    ''' <summary>
    ''' Updates the delivery dates grid with data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub grid_del_date_Binddata(Optional ByVal IsSelectedByZone As Boolean = False)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim CurrentDate As DateTime = DateTime.Today

        lblDesc.Text = ""
        grid_del_date.DataSource = ""
        grid_del_date.Visible = True
        navdt_table.Visible = True
        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        If SysPms.isDelvByPoints Then
            sql = "SELECT SUM(DELIVERY_POINTS * QTY) As Point_CNT FROM SO_LN WHERE DEL_DOC_NUM='" & Request("del_doc_num") & "' AND VOID_FLAG='N'"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            Try
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    pt_cnt = MyDataReader.Item("Point_CNT").ToString
                Else
                    pt_cnt = "0"
                End If

                MyDataReader.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        Else
            pt_cnt = 1
        End If

        'If IsNothing(Session("DeliveryDates")) Then
        Dim transport As TransportationBiz = New TransportationBiz()
        Try
            Dim zone As String = String.Empty
            If lbl_new_zone.Text & "" <> "" Then
                zone = lbl_new_zone.Text.Trim()
            Else
                zone = lbl_curr_zone.Text.Trim()
            End If
            ds = transport.GetDeliveryDates(zone, CurrentDate)
            Session("DeliveryDates") = ds
        Catch ex As Exception
            conn.Close()
            Throw
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        'Else
        '    ds = Session("DeliveryDates")
        'End If
        dv = ds.Tables(0).DefaultView
        If IsSelectedByZone Then
            Dim result() As DataRow = ds.Tables(0).Select("DEL_DT =#" + DateTime.Parse(lbl_curr_dt.Text).ToString("MM/dd/yyyy") + "#")
            If result.Count > 0 Then
                Dim MaxAvailDate As DateTime
                Dim DeliveryDate As DateTime
                CheckForAvailDtUpdate()
                If (theTMBiz.GetDisableDeliveryDates(pt_cnt, result(0).ItemArray(5).ToString(), result(0).ItemArray(6).ToString().Trim())) Then
                    If ViewState("MaxAvailDdate") IsNot Nothing AndAlso ViewState("MaxAvailDdate").ToString().Trim().isNotEmpty Then
                        MaxAvailDate = Convert.ToDateTime(ViewState("MaxAvailDdate").ToString())
                    End If
                    If result(0).ItemArray(1).ToString() <> String.Empty Then
                        Try
                            DeliveryDate = Convert.ToDateTime(result(0).ItemArray(0).ToString())
                        Catch ex As Exception
                        End Try
                    Else
                        'default it to DeliveryDate -1 .
                        DeliveryDate = MaxAvailDate.AddDays(-1)
                    End If
                    If DeliveryDate < MaxAvailDate Then
                        'do nothing
                    Else
                        lbl_new_dt.Text = lbl_curr_dt.Text
                    End If

                End If
            End If
        End If
        Try
            If SystemUtils.dataSetHasRows(ds) Then
                Dim MaxDate As DateTime
                Dim MinDate As DateTime
                Dim totalrows As Integer = 0
                grid_del_date.DataSource = dv
                grid_del_date.DataBind()
                navdt_table.Visible = True
                grid_del_date.Visible = True
                totalrows = ds.Tables(0).Rows.Count()
                If IsDate(ds.Tables(0).Rows(totalrows - 1)("DEL_DT")) Then
                    MaxDate = Convert.ToDateTime(ds.Tables(0).Rows(totalrows - 1)("DEL_DT"))
                Else
                    MaxDate = Today
                End If
                If IsDate(ds.Tables(0).Rows(0)("DEL_DT")) Then
                    MinDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("DEL_DT"))
                Else
                    MinDate = Today
                End If
                If totalrows > grid_del_date.PageSize Then
                    cboTargetDate.Enabled = True
                Else
                    cboTargetDate.Enabled = False
                End If
                setCalanderMinAndMaxDays(MinDate.AddDays(-1), MaxDate)
                cboTargetDate.Visible = True
                lblFilterDate.Visible = True
            Else
                navdt_table.Visible = False
                grid_del_date.Visible = False
                cboTargetDate.Visible = False
                lblFilterDate.Visible = False
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try


        lbl_pageinfo.Text = "Displaying Page " + CStr(GridView2.PageIndex + 1) + " of " + CStr(GridView2.PageCount)
        If grid_del_date.PageCount = 0 Then
            lbldesc2.Text = "Sorry, no available delivery dates were found for the specified zone.  Please <a href=""Delivery.aspx"">try again</a>.  <br><br>If you click on the above link and continue to get this message, the zip code selected for your customer has no eligible delivery dates."
            lbldesc2.Visible = True
        Else
            lbldesc2.Text = String.Empty
            lbldesc2.Visible = False

            ' make all the buttons visible if page count is more than 0
            btnfirst2.Enabled = True
            btnprev2.Enabled = True
            btnnext2.Enabled = True
            btnlast2.Enabled = True
            btnfirst2.Visible = True
            btnprev2.Visible = True
            btnnext2.Visible = True
            btnlast2.Visible = True
            lbl_pageinfo.Visible = True

            If grid_del_date.PageCount = 1 Then
                ' only 1 page of data
                btnfirst2.Enabled = False
                btnprev2.Enabled = False
                btnnext2.Enabled = False
                btnlast2.Enabled = False
            Else
                If grid_del_date.CurrentPageIndex = 0 Then
                    ' first page
                    btnfirst2.Enabled = False
                    btnprev2.Enabled = False
                Else
                    If grid_del_date.CurrentPageIndex = (grid_del_date.PageCount - 1) Then
                        ' last page
                        btnnext2.Enabled = False
                        btnlast2.Enabled = False
                    End If
                End If
            End If
        End If
        DisableDeliveryDates()

    End Sub

    ''' <summary>
    ''' Executes when user clicks on any of the navigation buttons(like Next, Previous, etc.)
    ''' at the bottom of the DELIVERY DATES grid.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Shows only for a delivery</remarks>
    Public Sub PageButtonClick2(ByVal sender As Object, ByVal e As EventArgs)
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " User clicked on Delivery dates grid ")
            End If
            Dim strArg As String
            strArg = sender.CommandArgument

            Select Case strArg
                Case "Next"
                    If grid_del_date.CurrentPageIndex < (grid_del_date.PageCount - 1) Then
                        grid_del_date.CurrentPageIndex += 1
                    End If
                Case "Prev"
                    If grid_del_date.CurrentPageIndex > 0 Then
                        grid_del_date.CurrentPageIndex -= 1
                    End If
                Case "Last"
                    grid_del_date.CurrentPageIndex = grid_del_date.PageCount - 1
                Case Else
                    grid_del_date.CurrentPageIndex = 0
            End Select
            cboTargetDate.Value = String.Empty
            grid_del_date_Binddata()
        End If
    End Sub

    ''' <summary>
    ''' Executes when user clicks on any of the navigation buttons(like Next, Previous, etc.)
    ''' at the bottom of the ZONES grid.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Shows for a delivery/pickup whenever there are multiple zones </remarks>
    Protected Sub PageButtonClick(ByVal sender As Object, ByVal e As EventArgs)
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicked on zones grid navigation button ")
            End If
            Dim strArg As String = sender.CommandArgument
            Select Case strArg
                Case "Next"
                    If GridView2.PageIndex < (GridView2.PageCount - 1) Then
                        GridView2.PageIndex += 1
                    End If
                Case "Prev"
                    If GridView2.PageIndex > 0 Then
                        GridView2.PageIndex -= 1
                    End If
                Case "Last"
                    GridView2.PageIndex = GridView2.PageCount - 1
                Case Else
                    GridView2.PageIndex = 0
            End Select

            UpdateZonesDisplay(If(AppConstants.PICKUP = cbo_PD.Value, Session("ZONE_ZIP_CD"), txt_zip.Text), Session("emp_init"))
            SetZonesNavigation()
        End If
    End Sub

    ''' <summary>
    ''' Disables  the select button in the delivery dates grid under certain circumstances.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DisableDeliveryDates()
        Dim dec As Boolean = True
        Dim intRow As Integer = 0
        Dim intRows As Integer = grid_del_date.Items.Count - 1
        Dim GridItem As DataGridItem
        Dim remainingPtsStps As String   ' remaining points or stops
        Dim MaxAvailDate As DateTime
        Dim DeliveryDate As DateTime

        CheckForAvailDtUpdate()
        For intRow = 0 To intRows
            GridItem = grid_del_date.Items(intRow)

            remainingPtsStps = GridItem.Cells(4).Text().Trim()

            'the pt_cnt is the total delivery point for this delivery line and was set during the binding of the delv date grid
            If pt_cnt & "" = "" Then pt_cnt = "0"

            If IsNumeric(remainingPtsStps) Then
                GridItem.Cells(5).Enabled = theTMBiz.GetDisableDeliveryDates(pt_cnt, remainingPtsStps, GridItem.Cells(6).Text().Trim())
            End If

            'Logic to Disable the button if based on max availablity date
            'Assumption is made that always GridItem.Cells(0) will have Date field
            'Disable only if ARS enable and also AndAlso Not Session("ord_tp_cd") = Nothing AndAlso Session("ord_tp_cd").ToString().ToUpper().Equals("SAL")
            'Session("STORE_ENABLE_ARS") = "True" AndAlso Not Session("ord_tp_cd") = Nothing AndAlso Session("ord_tp_cd").ToString().ToUpper().Equals("SAL")
            If Session("STORE_ENABLE_ARS") = "True" AndAlso Not Session("ord_tp_cd") = Nothing AndAlso Session("ord_tp_cd").ToString().ToUpper().Equals("SAL") Then
                If ViewState("MaxAvailDdate") IsNot Nothing AndAlso ViewState("MaxAvailDdate").ToString().Trim().isNotEmpty Then
                    MaxAvailDate = Convert.ToDateTime(ViewState("MaxAvailDdate").ToString())
                    If GridItem.Cells(1).Text <> String.Empty Then
                        Try
                            DeliveryDate = Convert.ToDateTime(GridItem.Cells(0).Text)
                        Catch ex As Exception
                            dec = False
                        End Try
                    Else
                        'default it to DeliveryDate -1 .
                        DeliveryDate = MaxAvailDate.AddDays(-1)
                    End If

                    'if delivery date is < available date then disable the button
                    If DeliveryDate < MaxAvailDate Then
                        GridItem.Cells(5).Enabled = False
                    Else
                        'Do nothing
                    End If
                Else
                    dec = False
                    'if no available dates found, Disable the Button on that row.
                    GridItem.Cells(5).Enabled = False
                End If
            Else
                'Do Nothing
            End If
        Next

        If dec = False AndAlso ASPxPageControl1.ActiveTabIndex = 5 Then
            ASPlblError.Text = "Available dates are not found for item(s)"
        Else
            ASPlblError.Text = String.Empty
        End If

    End Sub

    ''' <summary>
    ''' Executes when the user clicks on any editable component in the DELIVERY dates grid/table. In this
    ''' case it is the 'Select' of the date that selects that row.
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub grid_del_date_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grid_del_date.ItemCommand
        'If user has write mode access then continue
        ' lucy  29-jun-16
        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          'lucy 25-may-16
            lbl_final.Text = strErr
            Exit Sub
        End If
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicked select button on delivery grid ")
            End If
            lbl_new_dt.Text = e.Item.Cells(0).Text
            lbl_new_pd_store.Text = cbo_pd_store_cd.Value
            lbl_new_tp.Text = "D"
            If lbl_new_zone.Text & "" = "" Then
                lbl_new_zone.Text = lbl_curr_zone.Text
                lbl_new_charges.Text = lbl_curr_charges.Text
            End If
            If ConfigurationManager.AppSettings("delivery_options").ToString = "TIERED" Then
                lbl_new_charges.Text = lbl_curr_charges.Text
            End If
            cboTargetDate.Visible = True
            lblFilterDate.Visible = True
            lblFilterDate.Value = String.Empty
        End If
    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          'lucy 25-may-16
            lbl_final.Text = strErr
            Exit Sub
        End If

        'If user has write mode access then continue
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicked on calendar ")
            End If
            lbl_new_dt.Text = Calendar1.SelectedDate
            lbl_new_pd_store.Text = cbo_pd_store_cd.Value
            If String.IsNullOrEmpty(lbl_new_zone.Text) Then
                lbl_new_zone.Text = lbl_curr_zone.Text
            End If
            lbl_new_tp.Text = "P"
            If ConfigurationManager.AppSettings("delivery_options").ToString = "TIERED" Then
                lbl_new_charges.Text = lbl_curr_charges.Text
            End If
            calculate_total()
        End If
    End Sub

    'fires when the 'Change Zone' button on the pickup/delv tab is clicked
    Protected Sub ASPxButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASPxButton2.Click
        Dim strErr As String = lucy_ctl_change(txt_del_doc_num.Text.Trim)

        If strErr <> "Y" Then          'lucy 25-may-16
            lbl_final.Text = strErr
            Exit Sub
        End If

        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicked on Change Zone button ")
            End If
            UpdateZone(txt_zip.Text, Session("emp_init"))
        End If
        cboTargetDate.Value = String.Empty
    End Sub

    Protected Sub chk_email_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If user has write mode access then continue
        'If (Convert.ToInt32(Session("provideAccess")) = 0) Then
        '    'While user trying to see zone details
        '    If Not (String.IsNullOrEmpty(Session("browserID"))) Then
        '        accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicked on Email Invoice ")
        '    End If
        '    If chk_email.Checked = True Then

        '        ' May 24 IT Request 2919
        '        ' Daniela comment Nov 10
        '        'btnConfigureEmail.Enabled = True

        '        'ASPxPopupControl6.ShowOnPageLoad = True
        '    Else
        '        btnConfigureEmail.Enabled = False
        '    End If
        'End If
        ' May 24 IT Request 2919
        If chk_email.Checked = True Then
            Session("email_checked") = "Y"
        Else
            'disable email even if checked in Cust_edit
            Session("email_checked") = ""
        End If
    End Sub

    Protected Sub ASPxButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ASPxPopupControl10.ShowOnPageLoad = False
        Response.Redirect("SalesOrderMaintenance.aspx?ROW_ID=" & lbl_ser_row_id.Text & "&DEL_DOC_NUM=" & txt_del_doc_num.Text & "&query_returned=Y")
    End Sub

    Protected Sub btnConfigureEmail_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfigureEmail.Click
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicked on Configure Email button ")
            End If
            ASPxPopupControl6.ContentUrl = "Email_Templates.aspx?ord_tp_cd=" & txt_ord_tp_cd.Text & "&slsp1=" & txt_slsp1.Text & "&slsp2=" & txt_slsp2.Text & "&cust_cd=" & txt_Cust_cd.Text & "&del_doc_num=" & txt_del_doc_num.Text
            ASPxPopupControl6.ShowOnPageLoad = True
        End If
    End Sub

    Protected Sub cbo_tax_exempt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_tax_exempt.SelectedIndexChanged
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Changed the Tax exemption code ")
            End If
            txt_exempt_id.Text = String.Empty
            If cbo_tax_exempt.SelectedIndex <> 0 Then
                txtTaxCd.Text = ""
                Session("TAX_CD") = Nothing
                ViewState("TaxMethod") = Nothing
                ViewState("TaxExempted") = True
                txt_exempt_id.Text = String.Empty
                lbl_tax_cd.Text = ""
                determine_tax(txt_zip.Text)   ' TODO - consider - if they change this, then all bets are off
                calculate_total()
                txt_exempt_id.Focus()
            Else
                Session("TAX_CD") = txtTaxCd.Text
                ViewState("TaxExempted") = False
                'Added the below code as a part of MM-4240
                If (Not (Convert.ToBoolean(ViewState("CanModifyTax")))) Then
                    Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupMsg")
                    ucMsgPopup.DisplayErrorMsg(Resources.POSMessages.MSG0009)
                    msgPopup.ShowOnPageLoad = True
                Else
                    Dim hidIsInvokeManually As HiddenField = CType(ucTaxUpdate.FindControl("hidIsInvokeManually"), HiddenField)
                    hidIsInvokeManually.Value = "true"
                    Dim hidTaxDt As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDt"), HiddenField)

                    If SecurityUtils.hasSecurity("SOMTAXC", Session("EMP_CD")) Then
                        Dim hidZip As HiddenField = CType(ucTaxUpdate.FindControl("hidZip"), HiddenField)
                        hidZip.Value = String.Empty
                    End If
                    hidTaxDt.Value = txt_wr_dt.Text.Trim()
                    ucTaxUpdate.LoadTaxData()
                    PopupControlTax.ShowOnPageLoad = True
                End If
            End If
        End If
    End Sub


    Protected Sub txt_slsp1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Daniela
        Session("SLSP_CHANGED") = "Y"

        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Changing Sales Person 1 ")
            End If
            Dim errorMessage1 As Boolean = False
            If btn_Save.Enabled = True Then  ' maybe this means if a change is allowed but then should not allow change

                Dim slsp_cd As String = ""
                If (txt_slsp1.Text.Trim + "").isNotEmpty Then
                    If txt_slsp1.Text.Trim.ToUpper.Trim = txt_slsp2.Text.ToUpper.Trim Then
                        lbl_desc.Text = Resources.POSMessages.MSG0037
                        errorMessage1 = True
                        txt_slsp1.Text = lbl_desc.Text.ToUpper
                    Else
                        txt_slsp1.Text = txt_slsp1.Text.ToUpper
                        slsp_cd = theSystemBiz.GetEmployeeCode(txt_slsp1.Text)
                        ViewState("LBL_HDR_SLSP1_CD") = lbl_hdr_slsp1_cd.Text
                        lbl_hdr_slsp1_cd.Text = slsp_cd
                    End If
                Else
                    txt_slsp1.Text = lbl_desc.Text.ToUpper
                    lbl_desc.Text = "Salesperson1 is manidatory"
                    errorMessage1 = True
                End If

                If slsp_cd.isEmpty OrElse SalesUtils.Validate_Slsp(slsp_cd) = False Then
                    If Not errorMessage1 AndAlso lbl_desc.Text.isEmpty Then
                        ' put back the original slsp before change
                        lbl_desc.Text = Resources.POSErrors.ERR0037
                        errorMessage1 = True
                    End If
                    Dim ordSlsps As OrderSalespersons = SalesUtils.GetSoHdrSlsps(txt_del_doc_num.Text)
                    If ordSlsps.slsp1.Id.isNotEmpty Then
                        txt_slsp1.Text = ordSlsps.slsp1.Id
                        ViewState("LBL_HDR_SLSP1_CD") = lbl_hdr_slsp1_cd.Text
                        lbl_hdr_slsp1_cd.Text = ordSlsps.slsp1.Cd
                    Else
                        errorMessage1 = True
                    End If
                    'txt_slsp1.Text = MyDataReader.Item("SO_EMP_SLSP_CD1").ToString
                End If
            End If
            'MM -7366
            If Not (errorMessage1) Then
                lblSalessplit.Text = Resources.POSMessages.MSG0035
                PopupControlSalespersonConform.ShowOnPageLoad = True
            End If
        End If
    End Sub

    Protected Sub txt_slsp2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Daniela
        Session("SLSP_CHANGED") = "Y"

        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Changing Sales Person 2 ")
            End If
            Dim errorMessage2 As Boolean = False
            If txt_slsp2.Text.Trim & "" = "" Then
                txt_comm2.Text = "0.00"
                txt_comm1.Text = "100.00"
                txt_slsp2.Text = ""
                ViewState("LBL_HDR_SLSP2_CD") = lbl_hdr_slsp2_cd.Text
                lbl_hdr_slsp2_cd.Text = ""

            Else
                txt_slsp2.Text = txt_slsp2.Text.ToUpper
                If btn_Save.Enabled = True Then  ' only enabled if change allowed

                    Dim slsp_cd As String = ""
                    If (txt_slsp2.Text + "").isNotEmpty Then
                        If (txt_slsp2.Text.Trim.Equals(txt_slsp1.Text.Trim)) Then
                            lbl_desc.Text = Resources.POSMessages.MSG0037
                            errorMessage2 = True
                            txt_slsp2.Text = lbl_hdr_slsp2_cd.Text.ToUpper
                        Else
                            slsp_cd = theSystemBiz.GetEmployeeCode(txt_slsp2.Text)
                            ViewState("LBL_HDR_SLSP2_CD") = lbl_hdr_slsp2_cd.Text
                            lbl_hdr_slsp2_cd.Text = slsp_cd
                        End If
                    End If

                    If slsp_cd.isEmpty OrElse SalesUtils.Validate_Slsp(slsp_cd) = False Then

                        Dim ordSlsps As OrderSalespersons = SalesUtils.GetSoHdrSlsps(txt_del_doc_num.Text)
                        'If ordSlsps.slsp2.Id.isNotEmpty Then
                        txt_slsp2.Text = ordSlsps.slsp2.Id
                        ViewState("LBL_HDR_SLSP2_CD") = lbl_hdr_slsp2_cd.Text
                        lbl_hdr_slsp2_cd.Text = ordSlsps.slsp2.Cd
                        If txt_slsp2.Text.Trim.isEmpty Then
                            txt_comm1.Text = "100.00"
                            txt_comm2.Text = "0.00"
                            ' put back the original slsp before change
                        End If
                        lbl_desc.Text = Resources.POSErrors.ERR0037
                        errorMessage2 = True
                    Else
                        setHdrSlspComm()
                    End If
                Else
                    setHdrSlspComm()
                End If
            End If
            ViewState("COMM1") = HidslsComm1.Value.ToString()
            HidslsComm1.Value = txt_comm1.Text.Trim
            ViewState("COMM2") = HidslsComm2.Value.ToString()
            HidslsComm2.Value = txt_comm2.Text.Trim
            'MM-7366
            If Not (errorMessage2) Then
                lblSalessplit.Text = Resources.POSMessages.MSG0035
                PopupControlSalespersonConform.ShowOnPageLoad = True
            End If
        End If
    End Sub

    Protected Sub setHdrSlspComm()
        If txt_comm2.Text.Trim & "" = "" OrElse txt_comm2.Text.Trim = "0.00" Then
            If Not IsNumeric(ConfigurationManager.AppSettings("slsp2")) Then
                'for non numeric values, automatically default sls pct  to 50%
                txt_comm1.Text = "50.00"
                txt_comm2.Text = "50.00"
            Else
                'default based on setup param and reset the slsprsn1 pct
                txt_comm2.Text = FormatNumber(CDbl(ConfigurationManager.AppSettings("slsp2")), 2)
                txt_comm1.Text = FormatNumber(CDbl(100 - CDbl(txt_comm2.Text)), 2)
            End If
        Else
            txt_comm1.Text = FormatNumber(CDbl(100 - CDbl(txt_comm2.Text)), 2)
        End If
    End Sub

    Public Sub Package_Explosion()

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim retprc As Double
        Dim thistransaction As OracleTransaction
        Dim dropped_SKUS As String
        Dim dropped_dialog As String
        Dim User_Questions As String
        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim objSql8 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        Dim MyDataReader8 As OracleDataReader
        Dim MyDataReader10 As OracleDataReader
        Dim qty As Double = 1
        Dim PKG_GO As Boolean
        Dim itm_cd As String = ""
        Dim package_retail As Double = 0
        Dim row_id As Double = 0
        PKG_GO = False
        User_Questions = ""
        dropped_SKUS = ""
        dropped_dialog = ""

        objConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        objConnection.Open()
        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn.Open()

        sql = "SELECT SO_LN.ITM_CD, SO_LN.DEL_DOC_LN#, SO_LN.UNIT_PRC from itm, so_ln where DEL_DOC_NUM='" & txt_del_doc_num.Text & "' AND ITM.ITM_CD=SO_LN.ITM_CD AND ITM.ITM_TP_CD='PKG' AND SO_LN.UNIT_PRC > 0 ORDER BY DEL_DOC_LN#"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        MyDataReader10 = DisposablesManager.BuildOracleDataReader(objSql)

        Do While MyDataReader10.Read()
            itm_cd = MyDataReader10.Item("ITM_CD").ToString
            package_retail = MyDataReader10.Item("UNIT_PRC").ToString
            row_id = MyDataReader10.Item("DEL_DOC_LN#").ToString

            'If we are to populate the retail amounts, we need to calculate based on cost percentages
            sql = "SELECT ITM_CD, REPL_CST, PKG_ITM_CD from itm, package where pkg_itm_cd='" & itm_cd & "' "
            sql = sql & "and package.cmpnt_itm_cd=itm.itm_cd order by pkg_itm_cd, seq"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read()
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "insert into PACKAGE_BREAKOUT (session_id,PKG_SKU,COMPONENT_SKU,COST,ROW_ID) values ('" & Session.SessionID.ToString.Trim & "','" & MyDataReader.Item("PKG_ITM_CD").ToString & "','" & MyDataReader.Item("ITM_CD").ToString & "'," & MyDataReader.Item("REPL_CST").ToString & "," & row_id & ")"
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()

            'Insert new retail price
            thistransaction = objConnection.BeginTransaction
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_RETAIL=" & package_retail & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & itm_cd & "' AND ROW_ID=" & row_id
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()

            sql = "SELECT pkg_ITM_CD, SUM(REPL_CST) As TOTAL_COST from itm, package where pkg_itm_cd='" & itm_cd & "' and package.cmpnt_itm_cd=itm.itm_cd GROUP BY pkg_ITM_CD"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Calculate the total cost
            Do While MyDataReader.Read()
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_COST=" & MyDataReader.Item("TOTAL_COST").ToString & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & MyDataReader.Item("PKG_ITM_CD").ToString & "' AND ROW_ID=" & row_id
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()
            Dim PERCENT_OF_TOTAL As Double = 0
            Dim NEW_RETAIL As Double = 0
            Dim TOTAL_NEW_RETAIL As Double = 0
            Dim TOTAL_RETAIL As Double = 0
            Dim LAST_SKU As String = ""
            Dim LAST_PKG_SKU As String = ""

            If PKG_GO = True Then
                'Calculate the percentage based on cost/total cost
                sql = "SELECT * FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & itm_cd & "' AND ROW_ID=" & row_id & " ORDER BY PKG_SKU"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Dim pkg_qty As Double
                Do While MyDataReader2.Read()
                    sql = "SELECT NVL(SUM(QTY),0) AS SUM_QTY FROM PACKAGE WHERE PKG_ITM_CD='" & MyDataReader2.Item("PKG_SKU").ToString & "' AND CMPNT_ITM_CD='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "'"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    If MyDataReader.Read Then
                        If IsNumeric(MyDataReader.Item("SUM_QTY").ToString) Then
                            pkg_qty = CDbl(MyDataReader.Item("SUM_QTY").ToString)
                        Else
                            pkg_qty = 0
                        End If
                    Else
                        pkg_qty = 0
                    End If

                    If IsNumeric(MyDataReader2.Item("COST").ToString) And IsNumeric(MyDataReader2.Item("TOTAL_COST").ToString) Then
                        PERCENT_OF_TOTAL = CDbl(MyDataReader2.Item("COST").ToString) / CDbl(MyDataReader2.Item("TOTAL_COST").ToString)
                    Else
                        PERCENT_OF_TOTAL = CDbl(0)
                    End If
                    If PERCENT_OF_TOTAL.ToString = "NaN" Then PERCENT_OF_TOTAL = 0
                    NEW_RETAIL = Math.Round(PERCENT_OF_TOTAL * CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString), 2, MidpointRounding.AwayFromZero)
                    TOTAL_NEW_RETAIL = TOTAL_NEW_RETAIL + NEW_RETAIL
                    TOTAL_RETAIL = CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                    LAST_SKU = MyDataReader2.Item("COMPONENT_SKU").ToString
                    LAST_PKG_SKU = MyDataReader2.Item("PKG_SKU").ToString
                    thistransaction = objConnection.BeginTransaction
                    With cmdInsertItems
                        .Transaction = thistransaction
                        .Connection = objConnection
                        .CommandText = "UPDATE PACKAGE_BREAKOUT SET PERCENT_OF_TOTAL = " & PERCENT_OF_TOTAL & ", NEW_RETAIL=" & Math.Round(NEW_RETAIL / pkg_qty, 2, MidpointRounding.AwayFromZero) & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & MyDataReader2.Item("PKG_SKU").ToString & "' AND COMPONENT_SKU='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "' AND ROW_ID=" & row_id
                    End With
                    cmdInsertItems.ExecuteNonQuery()
                    thistransaction.Commit()
                Loop

                MyDataReader2.Close()
                objSql2.Dispose()
            End If

            Dim ds2 As DataSet
            ds2 = Session("DEL_DOC_LN#")
            Dim FINAL_RETAIL_TOTAL As Double = 0

            If Not IsNothing(Session("DEL_DOC_LN#")) Then

                sql = "SELECT NEW_RETAIL, COMPONENT_SKU, ROW_ID FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & itm_cd & "'"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read()
                    retprc = MyDataReader2.Item("NEW_RETAIL")
                    If Not IsNumeric(retprc) Then retprc = 0

                    Dim dt As DataTable = ds2.Tables(0)
                    Dim dr As DataRow

                    For Each dr In ds2.Tables(0).Rows
                        If dr.RowState = DataRowState.Unchanged Then
                            If IsNumeric(dr("DEL_DOC_LN#")) Then
                                If dr("DEL_DOC_LN#") > row_id Then
                                    If dr("UNIT_PRC") = 0 Then
                                        If MyDataReader2.Item("COMPONENT_SKU").ToString = dr("ITM_CD").ToString Then
                                            If TOTAL_RETAIL <> TOTAL_NEW_RETAIL And CDbl(dr("QTY")) = 1 And retprc > (TOTAL_NEW_RETAIL - TOTAL_RETAIL) Then
                                                retprc = retprc - (TOTAL_NEW_RETAIL - TOTAL_RETAIL)
                                                TOTAL_NEW_RETAIL = TOTAL_RETAIL
                                            End If

                                            dr("TAXABLE_AMT") = retprc / CDbl(dr("QTY").ToString)
                                            dr("UNIT_PRC") = retprc / CDbl(dr("QTY").ToString)
                                            If retprc / CDbl(dr("QTY").ToString) > 0 Then
                                                FINAL_RETAIL_TOTAL = FINAL_RETAIL_TOTAL + retprc / CDbl(dr("QTY").ToString)
                                                LAST_SKU = dr("ITM_CD").ToString
                                            End If
                                            dr("PACKAGE_PARENT") = row_id
                                        End If
                                    ElseIf dr("ITM_TP_CD").ToString = "FAB" Or dr("ITM_TP_CD").ToString = "WAR" Then
                                        'Do nothing, let the process continue running
                                    Else
                                        Exit For
                                    End If
                                ElseIf dr("DEL_DOC_LN#") = row_id Then
                                    dr("TAXABLE_AMT") = 0
                                    dr("UNIT_PRC") = 0
                                End If
                            End If
                        End If
                    Next
                Loop
                If TOTAL_RETAIL <> FINAL_RETAIL_TOTAL Then
                    For Each dr In ds2.Tables(0).Rows
                        If LAST_SKU = dr("ITM_CD").ToString And CDbl(dr("QTY").ToString) = 1 Then
                            dr("TAXABLE_AMT") = CDbl(dr("TAXABLE_AMT").ToString) - (FINAL_RETAIL_TOTAL - TOTAL_RETAIL)
                            dr("UNIT_PRC") = CDbl(dr("UNIT_PRC").ToString) - (FINAL_RETAIL_TOTAL - TOTAL_RETAIL)
                            Exit For
                        End If
                    Next
                End If
            End If

            MyDataReader2.Close()
            objSql2.Dispose()

            Session("DEL_DOC_LN#") = ds2

            'We're done, delete out all of the records to avoid duplication
            thistransaction = objConnection.BeginTransaction
            sql = "DELETE FROM PACKAGE_BREAKOUT WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "'"
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()

        Loop

        conn.Close()
        objConnection.Close()

    End Sub


    Protected Sub ASPxGridView5_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles ASPxGridView5.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If e.GetValue("ID_CD").ToString() & "" <> "" Then

            Dim hpl_out As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(ASPxGridView5.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_out"), DevExpress.Web.ASPxEditors.ASPxHyperLink)

            If Not IsNothing(hpl_out) Then

                hpl_out.Text = e.GetValue("ID_CD").ToString()
                hpl_out.NavigateUrl = "SalesOrderMaintenance.aspx?OUT_ID=" & HttpUtility.UrlEncode(e.GetValue("ID_CD").ToString()) & "&STORE_CD=" & HttpUtility.UrlEncode(lbl_out_store_cd.Text) &
                                      "&OUT_CD=" & HttpUtility.UrlEncode(e.GetValue("OUT_CD").ToString()) & "&SPIFF=" & HttpUtility.UrlEncode(e.GetValue("SPIFF").ToString()) &
                                      "&ROW_ID=" & lbl_out_row_id.Text & "&DEL_DOC_NUM=" & txt_del_doc_num.Text & "&query_returned=Y"
            End If
        End If

    End Sub

    Protected Sub ASPxGridView14_HtmlRowCreated(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles ASPxGridView14.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If e.GetValue("OUT_CD").ToString() & "" <> "" Then

            Dim hpl_outCd As DevExpress.Web.ASPxEditors.ASPxHyperLink = TryCast(ASPxGridView14.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "hpl_outCd"), DevExpress.Web.ASPxEditors.ASPxHyperLink)

            If Not IsNothing(hpl_outCd) Then

                hpl_outCd.Text = e.GetValue("OUT_CD").ToString()
                Dim outCd As String = e.GetValue("OUT_CD").ToString()
                hpl_outCd.NavigateUrl = "SalesOrderMaintenance.aspx?OUT_CD=" & HttpUtility.UrlEncode(e.GetValue("OUT_CD").ToString()) &
                                      "&ROW_ID=" & lbl_out_cd_row_id.Text & "&DEL_DOC_NUM=" & txt_del_doc_num.Text & "&query_returned=Y" &
                                      "&STORE_CD=&LOC_CD="   'had to add this otherwise Store/Loc are overriden in the LOAD event  
            End If
        End If

    End Sub

    Protected Sub chk_reserve_all_CheckedChanged(sender As Object, e As System.EventArgs) Handles chk_reserve_all.CheckedChanged
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicked on Reserve Lines ")
            End If
            Try
                If (chk_reserve_all.Checked) Then
                    ReserveInventory()
                    chk_reserve_all.Checked = False   'resets the value for next time
                Else
                    If isRsrvLnCalled = False Then
                        lbl_desc.Text = String.Empty
                    End If

                End If
                If splitErrMsg.Length > 0 AndAlso isRsrvLnCalled = False Then
                    lbl_desc.Text = splitErrMsg.ToString()
                End If
            Catch oracleException As OracleException
                Select Case oracleException.Code
                    Case 20101
                        lbl_desc.Text = HBCG_Utils.FormatOracleException(oracleException.Message.ToString())
                End Select
            Catch ex As Exception
                lbl_desc.Text = ex.Message.ToString()
            End Try
        End If
        splitErrMsg.Length = 0 'dispose this 
        isRsrvLnCalled = True
    End Sub

    Protected Sub chk_calc_avail_CheckedChanged(sender As Object, e As System.EventArgs) Handles chk_calc_avail.CheckedChanged
        'If user has write mode access then continue
        If (Convert.ToInt32(Session("provideAccess")) = 0) Then
            'While user trying to see zone details
            If Not (String.IsNullOrEmpty(Session("browserID"))) Then
                accessInfo.UpdateRequest(ViewState("del_doc_num"), Session("browserID").ToString(), " Clicked on Calculate Availability ")
            End If
            bindgrid()
            doAfterBindgrid()
        End If
    End Sub

    Protected Sub btn_return_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_return.Click
        Session("OpenIST") = Nothing ' lucy 11-aug-16
        ' otherwise this button should not be visible
        If Session("referrerToSom").ToString = "salesbookofbusiness.aspx" Then
            'If the current user has write access then perform the below operation
            'If the current has has read-only permission then no need to perform below activity            
            If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
                Dim accessInfo As New AccessControlBiz()
                accessInfo.DeleteRecord("SalesOrderMaintenance.aspx")
            End If
            Response.Redirect("salesbookofbusiness.aspx?returnFrom=SOM&query_returned=N")

        End If
        If Session("referrerToSom").ToString = "salesbookofbusinessMgment.aspx" Then    'lucy, 20-may-16
            'If the current user has write access then perform the below operation
            'If the current has has read-only permission then no need to perform below activity            
            If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
                Dim accessInfo As New AccessControlBiz()
                accessInfo.DeleteRecord("SalesOrderMaintenance.aspx")
            End If
            Response.Redirect("salesbookofbusinessMgment.aspx?returnFrom=SOM&query_returned=N")
        End If
        If Session("referrerToSom").ToString = "SalesConfirmation.aspx" Then
            'If the current user has write access then perform the below operation
            'If the current has has read-only permission then no need to perform below activity            
            If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
                Dim accessInfo As New AccessControlBiz()
                accessInfo.DeleteRecord("SalesOrderMaintenance.aspx")
            End If
            Response.Redirect("SalesConfirmation.aspx?returnFrom=SOM&query_returned=N")
        End If
        If Session("referrerToSom").ToString = "CommCustomerInquiry.aspx" Then      'lucy 19-aug-16
            'If the current user has write access then perform the below operation
            'If the current has has read-only permission then no need to perform below activity            
            If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
                Dim accessInfo As New AccessControlBiz()
                accessInfo.DeleteRecord("SalesOrderMaintenance.aspx")
            End If
            Response.Redirect("CommCustomerInquiry.aspx?returnFrom=SOM&query_returned=N")
        End If
    End Sub

    ''' <summary>
    '''  Responds as the subscriber to the summary price change approval user control 'WasPCApproved' event. It will hide the popup
    ''' and update the UI, the dataset and recalculate amounts based on the new price info.  
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucSummaryPrcChange_WasPCApproved(sender As Object, e As EventArgs) Handles ucSummaryPrcChange.WasPCApproved





        'means that the approval for all the line price changes was successful, so update the lines in the grid and temp_itm          
        Dim popupPriceChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")
        Dim modifiedLnNums = hidUpdLnNos.Value.ToString().Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries).Distinct()

        'get the approver's emp_cd to set on the line                            
        Dim hidAppEmpCd As HiddenField = CType(popupPriceChangeAppr.FindControl("hidAppEmpCd"), HiddenField)
        Dim approverEmpCd As String = hidAppEmpCd.Value.ToString
        Dim soLnDatSet As DataSet = Session("DEL_DOC_LN#")
        Dim soLnTable As DataTable = soLnDatSet.Tables(0)

        'Alice added on Oct 17, 2019 for request 294
        'Get the approver's comments     
        Dim txtCommentL1 As TextBox = CType(popupPriceChangeAppr.FindControl("txtCommentL1"), TextBox)
        Dim txtCommentL2 As TextBox = CType(popupPriceChangeAppr.FindControl("txtCommentL2"), TextBox)
        Dim txtCommentL3 As TextBox = CType(popupPriceChangeAppr.FindControl("txtCommentL3"), TextBox)
        Dim commentL1 As String = txtCommentL1.Text.Trim()
        Dim commentL2 As String = txtCommentL2.Text.Trim()
        Dim commentL3 As String = txtCommentL3.Text.Trim()
        Session("ApprovalComment1_SOM") = commentL1
        Session("ApprovalComment2_SOM") = commentL2
        Session("ApprovalComment3_SOM") = commentL3
        Session("Approver_emp_cd_SOM") = approverEmpCd

        If (modifiedLnNums.Any) Then
            'see if this line was one of the lines modified 

            For Each id As Integer In modifiedLnNums
                Dim dr As DataRow = OrderUtils.GetOrderLine(soLnDatSet, id)
                'update the approver emp cd for each line
                If dr IsNot Nothing Then
                    dr.Item("prc_chg_app_cd") = approverEmpCd
                    dr.Item("manual_prc") = dr.Item("unit_prc")

                End If
                calculate_total()
            Next



            'reapplies the discounts (if any) when applicable, and updates the session data

            ReapplyDiscounts(soLnDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)

            Session("DEL_DOC_LN#") = soLnDatSet
            GridView1.DataSource = soLnDatSet
            GridView1.DataBind()
            doAfterBindgrid()

            calculate_total()
        End If

        popupPriceChangeAppr.ShowOnPageLoad = False
        DoAfterSummaryPriceChangeApproval()

    End Sub

    ''' <summary>
    '''  Responds as the subscriber to the summary price change approval user control 'WasPCCanceled' event. It will hide the popup
    ''' and update the UI, the dataset and reset amounts based on the original price info.  
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucSummaryPrcChange_WasPCCanceled(sender As Object, e As EventArgs) Handles ucSummaryPrcChange.WasPCCanceled

        'Needs to undo the Grid changes
        Dim txtRetPrc As TextBox
        Dim delDocLnNum As Integer
        Dim popupPriceChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")
        Dim modifiedLnNums = hidUpdLnNos.Value.ToString().Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries).Distinct()
        Dim soLnDatSet As DataSet = Session("DEL_DOC_LN#")
        'MM-9870
        'Updating 'N' in the Approved clolumn if click on the cancel in the Manager Approval Screen
        Dim discountGrid As DevExpress.Web.ASPxGridView.ASPxGridView = ucDiscount.FindControl("grdDiscItems")
        Dim discountRowCount As Integer = discountGrid.VisibleRowCount
        If (SysPms.discWithAppSec = "W") AndAlso (Not IsNothing(ViewState("IsDiscountAdded")) AndAlso ViewState("IsDiscountAdded") AndAlso (Session("mgrApproval") <> "Y") AndAlso (discountRowCount > 0) AndAlso (AppConstants.ManagerOverride.BY_ORDER.Equals(SalesUtils.GetPriceMgrOverride()))) Then
            If Not IsNothing(Session("discountsApplied")) Then
                Dim discountsApplied As DataTable = Session("discountsApplied")
                Dim drs = (From u In discountsApplied.AsEnumerable()
                           Where u.Field(Of String)("Approved") = "A"
                           Select u)
                For Each Row As DataRow In drs
                    If Row("DISC_IS_EXISTING") AndAlso Row("Approved").ToString.Equals("A") Then
                        Row("Approved") = "Y"
                    Else
                        Row("Approved") = "N"
                    End If
                Next
                Session("discountsApplied") = discountsApplied
            End If
        End If
        'MM-9870
        If (Not IsNothing(soLnDatSet) AndAlso modifiedLnNums.Any) OrElse Not IsNothing(Session("IsCanceled")) AndAlso Session("IsCanceled").Equals(True) Then
            'see if this line was one of the lines modified 
            For Each id As Integer In modifiedLnNums

                'get the actual dataset row for each line whose price was modified and also get the display grid row
                Dim dr As DataRow = OrderUtils.GetOrderLine(Session("DEL_DOC_LN#"), id)
                If dr IsNot Nothing Then
                    For Each dgItem As DataGridItem In GridView1.Items
                        delDocLnNum = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
                        If delDocLnNum = CInt(id) Then
                            ' reset the orig price on the display field
                            txtRetPrc = CType(dgItem.FindControl("unit_prc"), TextBox)
                            'MM-6862
                            txtRetPrc.Text = FormatNumber(dr.Item("ORIG_PRC"))
                            Exit For
                        End If
                    Next
                    dr("UNIT_PRC") = CDbl(txtRetPrc.Text)
                End If
            Next

            'reapplies the discounts (if any) when applicable, and updates the session data
            'MM-9870
            If (SysPms.discWithAppSec = "W") AndAlso (Not IsNothing(ViewState("IsDiscountAdded")) AndAlso ViewState("IsDiscountAdded") AndAlso (Session("mgrApproval") <> "Y") AndAlso (discountRowCount > 0) AndAlso (AppConstants.ManagerOverride.BY_ORDER.Equals(SalesUtils.GetPriceMgrOverride()))) Then
                If Not IsNothing(Session("IsCanceled")) AndAlso Session("IsCanceled").Equals(True) Then
                    ReapplyDiscounts(soLnDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Void, "", True)
                    If discountGrid.VisibleRowCount = 0 Then
                        ' If discountsApplied grid is not containing any rows Hiding the discounts gridview and  Successful label message is hiding 
                        ucDiscount.FindControl("grdDiscItems").Visible = False
                        ucDiscount.FindControl("lblMessage").Visible = False
                    Else
                        ucDiscount.FindControl("lblMessage").Visible = False
                    End If
                Else
                    ReapplyDiscounts(soLnDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)
                End If
            Else
                ReapplyDiscounts(soLnDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)
            End If
            Session("DEL_DOC_LN#") = soLnDatSet
            GridView1.DataSource = soLnDatSet
            GridView1.DataBind()
            doAfterBindgrid()

            calculate_total()
        End If

        popupPriceChangeAppr.ShowOnPageLoad = False

        DoAfterSummaryPriceChangeApproval()
        ucDiscount.HideManagerPopUp()

    End Sub


    ''' <summary>
    ''' Executes when the manager override is successfully carried out via the popup. Any updates to the price
    ''' on the line and other specific updates take place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasApproved(sender As Object, e As EventArgs) Handles ucMgrOverride.wasApproved
        If ucMgrOverride.ApprovalFor = ORDERSAVE Then
            SaveSalesOrder()
            hideApprovalPopup(ORDERSAVE)
            Exit Sub
        End If
        If ucMgrOverride.ApprovalFor = DelChargesApproval Then
            calculate_total()
            hideApprovalPopup(DelChargesApproval)
            'if the Zone is changed and delivery charges are also modified, then change the new delivery charges as well
            If Not lbl_new_charges.Text.Trim.Equals(String.Empty) Then
                lbl_new_charges.Text = lblDelivery.Text
            Else
                'Do Nothing
            End If

            Exit Sub
        End If
        If Session("SelectedRow") IsNot Nothing Then

            Dim modifiedRow As String() = Session("SelectedRow").ToString.Split(":")
            Dim dgItem As DataGridItem = CType(GridView1.Items(CType(modifiedRow(0), Integer)), DataGridItem)
            If dgItem IsNot Nothing Then

                Dim txtRetPrc As TextBox = CType(dgItem.FindControl("unit_prc"), TextBox)
                'MM-9146
                If Not IsNothing(Session("EditedRetPrc")) AndAlso CInt(Session("EditedRetPrc").ToString) <> CInt(txtRetPrc.Text) Then
                    txtRetPrc.Text = Session("EditedRetPrc").ToString
                    Session("EditedRetPrc") = Nothing
                Else
                    Session("EditedRetPrc") = Nothing
                End If
                If txtRetPrc IsNot Nothing Then

                    Dim hidAppEmpCd As HiddenField = CType(ucMgrOverride.FindControl("hidAppEmpCd"), HiddenField)
                    Dim approverEmpCd As String = hidAppEmpCd.Value.ToString
                    Dim soLnDatSet As DataSet = Session("DEL_DOC_LN#")

                    '**** Since the approval passed, set the manual price behind the scenes on the datagrid and dataset
                    'update the approver emp cd for each line
                    Dim dr As DataRow = OrderUtils.GetOrderLine(soLnDatSet, modifiedRow(1))
                    If dr IsNot Nothing Then
                        dr.Item("prc_chg_app_cd") = approverEmpCd
                        dr.Item("manual_prc") = txtRetPrc.Text
                        dr.Item("unit_prc") = txtRetPrc.Text
                    End If

                    'reapplies the discounts (if any) when applicable
                    ReapplyDiscounts(soLnDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)

                    Session("DEL_DOC_LN#") = soLnDatSet
                    GridView1.DataSource = soLnDatSet
                    GridView1.DataBind()
                    doAfterBindgrid()

                    calculate_total()
                End If
            End If
        End If

        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
        End If

        popupMgrOverride.ShowOnPageLoad = False
        hidUpdLnNos.Value = String.Empty

        'MM-6862
        'If Not String.IsNullOrEmpty(ViewState("hasWarrenties")) Then
        '    ASPxPopupControl9.ContentUrl = ViewState("hasWarrenties")
        '    ASPxPopupControl9.ShowOnPageLoad = True
        '    ViewState("hasWarrenties") = String.Empty
        'End If
    End Sub

    ''' <summary>
    ''' Gets fired when the manager override fails. Any resetting of values takes place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasCanceled(sender As Object, e As EventArgs) Handles ucMgrOverride.wasCanceled
        If ucMgrOverride.ApprovalFor = ORDERSAVE Then
            cbo_stat_cd.SelectedIndex = 0
            hideApprovalPopup(ORDERSAVE)
            Exit Sub
        End If
        If ucMgrOverride.ApprovalFor = DelChargesApproval Then
            lblDelivery.Text = hidDeliveryCharges.Value
            'if the Zone is changed and delivery charges are also modified, then change the new delivery charges as well
            If Not lbl_new_charges.Text.Trim.Equals(String.Empty) Then
                lbl_new_charges.Text = lblDelivery.Text
            Else
                'Do Nothing
            End If
            hideApprovalPopup(DelChargesApproval)
            calculate_total()
            Exit Sub
        End If

        If Session("SelectedRow") IsNot Nothing Then

            Dim soLnDatSet As DataSet = Session("DEL_DOC_LN#")
            Dim modifiedRow As String() = Session("SelectedRow").ToString.Split(":")
            Dim dgItem As DataGridItem = CType(GridView1.Items(CType(modifiedRow(0), Integer)), DataGridItem)
            If dgItem IsNot Nothing Then

                'get the price field for the line being changed
                Dim txtRetPrc As TextBox = CType(dgItem.FindControl("unit_prc"), TextBox)
                If txtRetPrc IsNot Nothing Then

                    '**** Since the approval was aborted, reset the display fields value to the manual price
                    ' *** The datatrow unit price does not have to be reset since it was never updated before the mgr appr popup was displayed
                    Dim dr As DataRow = OrderUtils.GetOrderLine(soLnDatSet, modifiedRow(1))
                    If dr IsNot Nothing Then
                        txtRetPrc.Text = FormatNumber(dr.Item("ORIG_PRC"))
                        'MM-6862
                        dr("UNIT_PRC") = txtRetPrc.Text
                    End If
                End If
            End If

            'reapplies the discounts (if any) when applicable, and updates the session data
            ReapplyDiscounts(soLnDatSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Prc)

            Session("DEL_DOC_LN#") = soLnDatSet
            GridView1.DataSource = soLnDatSet
            GridView1.DataBind()
            doAfterBindgrid()

            calculate_total()
        End If
        If (Not IsNothing(Session("SHOWPOPUP")) AndAlso Session("SHOWPOPUP") = True) Then
            Session("SHOWPOPUP") = Nothing
        End If
        popupMgrOverride.ShowOnPageLoad = False
        hidUpdLnNos.Value = String.Empty

        'MM-6862
        'If Not String.IsNullOrEmpty(ViewState("hasWarrenties")) Then
        '    ASPxPopupControl9.ContentUrl = ViewState("hasWarrenties")
        '    ASPxPopupControl9.ShowOnPageLoad = True
        '    ViewState("hasWarrenties") = String.Empty
        'End If
    End Sub


    ''' <summary>
    ''' Executes after the summary price approval popup has been dismissed.
    ''' It will redirect to the intended page or location before the approval 
    ''' popup was displayed, clear out the modified lines list, etc.
    ''' </summary>
    Private Sub DoAfterSummaryPriceChangeApproval()

        hidUpdLnNos.Value = String.Empty

        If Not String.IsNullOrEmpty(hidDestUrl.Value) Then
            Response.Redirect(hidDestUrl.Value)
        ElseIf Not String.IsNullOrEmpty(ViewState("EventName")) Then

            Select Case ViewState("EventName").ToString()
                Case "PageControlActiveTabChange"
                    AddHandler ASPxPageControl1.ActiveTabChanging, AddressOf ASPxPageControl1_ActiveTabChanging
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Determines if the price on any lines on the order has been modified and if needed, prompts for a 
    ''' manager approval via the summary price approval popup. 
    ''' </summary>
    Private Sub PromptForSummaryPriceMgrOverride()

        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()
        If AppConstants.ManagerOverride.BY_ORDER = prcOverride Then

            'get the unique, not null value of all rows whose price was modified
            Dim modifiedLnNums = hidUpdLnNos.Value.ToString().Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries).Distinct()

            If (Not IsNothing(Session("DEL_DOC_LN#")) AndAlso modifiedLnNums.Any) Then

                Dim delDocLnNum As Integer = 0
                Dim lblDesc As Label
                Dim tboxQty As TextBox

                Dim table As DataTable = SalesUtils.GetOrderLinesTableNew() 'Alice replace GetOrderLinesTable() to GetOrderLinesTableNew() on May 24, 2019 for request 4168
                For Each dgItem As DataGridItem In GridView1.Items

                    delDocLnNum = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
                    For Each id As String In modifiedLnNums
                        If delDocLnNum = CInt(id) Then
                            'gather the needed info to build the data table to pass to the price summary grid
                            Dim dr As DataRow = OrderUtils.GetOrderLine(Session("DEL_DOC_LN#"), delDocLnNum)
                            If dr IsNot Nothing Then
                                lblDesc = CType(dgItem.FindControl("Label2"), Label)
                                tboxQty = CType(dgItem.FindControl("qty"), TextBox)
                                SalesUtils.AddOrderLineRow(table,
                                                           dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text,
                                                           dgItem.Cells(SoLnGrid.ITM_CD).Text,
                                                           lblDesc.Text,
                                                           CDbl(tboxQty.Text),
                                                           dr("ORIG_PRC"),
                                                           dr("UNIT_PRC"),
                                CType(dgItem.FindControl("lbl_max"), Label).Text)
                            End If
                            Exit For
                        End If
                    Next
                Next

                'pass this datatable to the new summ price approval popup & display it.
                If table IsNot Nothing AndAlso table.Rows.Count > 0 Then
                    ucSummaryPrcChange.BindData(table)
                    Dim popupPrcChangeAppr As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucSummaryPrcChange.FindControl("popupPriceChangeAppr")
                    popupPrcChangeAppr.ShowOnPageLoad = True
                End If
            End If
        End If

    End Sub

    Private Sub setPickupDate()

        Dim MaxDelDate As Date
        CheckForAvailDtUpdate()
        If (Session("STORE_ENABLE_ARS")) AndAlso Not Session("ord_tp_cd") = Nothing AndAlso Session("ord_tp_cd").ToString().ToUpper().Equals("SAL") Then
            If ViewState("MaxAvailDdate") IsNot Nothing AndAlso ViewState("MaxAvailDdate").ToString() <> String.Empty Then
                MaxDelDate = Convert.ToDateTime(ViewState("MaxAvailDdate").ToString())
                If MaxDelDate < Date.Today Then
                    MaxDelDate = Date.Today
                Else
                    MaxDelDate = Convert.ToDateTime(ViewState("MaxAvailDdate").ToString())
                End If

                Calendar1.MinDate = MaxDelDate
                ASPlblError.Text = String.Empty
            Else
                Calendar1.Enabled = False
                ASPlblError.Text = "Available dates are not found for item(s)"
            End If
        Else
            If ViewState("MaxAvailDdate") IsNot Nothing AndAlso ViewState("MaxAvailDdate").ToString() <> String.Empty Then
                MaxDelDate = Convert.ToDateTime(ViewState("MaxAvailDdate").ToString())
                Calendar1.MinDate = MaxDelDate
                ASPlblError.Text = String.Empty
            Else
                Calendar1.Enabled = False
                ASPlblError.Text = "Available dates are not found for item(s)"
            End If
        End If

    End Sub

    Protected Sub btnSORW_Click(sender As Object, e As EventArgs) Handles btnSORW.Click

        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
            PromptForSummaryPriceMgrOverride()
        Else
            popupSaleRW.ContentUrl = "RedirectingToSalesRewrite.aspx?RELATED_SKU=" & txt_del_doc_num.Text & ""
            popupSaleRW.ShowOnPageLoad = True
        End If

    End Sub
    'Alice added on Jan 08, 2019 for request 2260
    Protected Sub btnEEX_Click(sender As Object, e As EventArgs) Handles btnEEX.Click

        If Not String.IsNullOrEmpty(hidUpdLnNos.Value.ToString()) Then
            PromptForSummaryPriceMgrOverride()
        Else
            popupSaleEEX.ContentUrl = "RedirectingToSalesExchange.aspx?RELATED_SO=" & txt_del_doc_num.Text & ""
            popupSaleEEX.ShowOnPageLoad = True
        End If

    End Sub

    ''' <summary>
    ''' Checks if the logged in employee has the corresponding securities to make changes to 
    ''' the confirmation status code.
    ''' </summary>
    Private Sub ApplyConfirmationCodesSecurities()
        If (Not IsNothing(Session("emp_cd")) AndAlso Not IsNothing(ddlConfStaCd.SelectedItem)) Then

            If (CheckSecurity(SecurityUtils.OVERRIDE_CONFIRMATION, Session("emp_cd"))) Then
                'since user has security to override the code, nothing else needs to be done
            Else
                'needs to check that user has Updateable security and code is updateable
                If (CheckSecurity(SecurityUtils.UPDATE_CONFIRMATION, Session("emp_cd"))) Then
                    Dim isCodeChangeable As Boolean = False
                    Dim dt As DataTable = Session("ConfCodes")
                    Dim drFitered() As DataRow = dt.Select("Code = '" & ddlConfStaCd.SelectedItem.Value & "'")
                    isCodeChangeable = "Y" = drFitered(0)("IS_CHANGEABLE")
                    'user has security but if the code is not changeable, disallow changes
                    ddlConfStaCd.Enabled = isCodeChangeable
                Else
                    'since user doesn't have security to change, disallow changes
                    ddlConfStaCd.Enabled = False
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Responds as the subscriber to the tax update user control event. It will hide the popup
    ''' and update the UI and recalculate amounts based on the new tax info.
    ''' </summary>
    ''' <param name="sender"></param>
    Protected Sub ucTaxUpdate_TaxUpdated(sender As Object, e As EventArgs) Handles ucTaxUpdate.TaxUpdated

        PopupControlTax.ShowOnPageLoad = False
        Dim hidTaxCd As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxCd"), HiddenField)
        Dim hidTaxDesc As HiddenField = CType(ucTaxUpdate.FindControl("hidTaxDesc"), HiddenField)
        ViewState("TaxMethod") = (CType(ucTaxUpdate.FindControl("hidTaxMethod1"), HiddenField)).Value
        lbl_tax_cd.Text = hidTaxCd.Value
        ' Newly selected tax_cd should be added to session
        Session("TAX_CD") = lbl_tax_cd.Text
        ' Added the below code as a part of MM-4240
        ' The below code was adding an "-" symbol in the txtTaxCd textbox so making this change
        If Not (String.IsNullOrWhiteSpace(hidTaxCd.Value)) Then
            txtTaxCd.Text = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(hidTaxCd.Value.ToString() & " - " & hidTaxDesc.Value.ToString()))
        Else
            txtTaxCd.Text = String.Empty
        End If

        ViewState("TaxMethod") = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(ViewState("TaxMethod")))
        'When tax has been selected
        ViewState("TaxExempted") = False
        cbo_tax_exempt.SelectedIndex = 0
        txt_exempt_id.Text = String.Empty

        calculate_total()

    End Sub

    Protected Sub ucDiscount_DiscountApplied(sender As Object, e As EventArgs) Handles ucDiscount.DiscountApplied

        SessVar.Remove(SessVar.discReCalcVarNm)
        'MM-5661
        ViewState("IsDiscountAdded") = True
        bindgrid()
        doAfterBindgrid()
        calculate_total()
    End Sub

    'MM-5661
    Protected Sub ucDiscount_CancelledManagerApproval(sender As Object, e As EventArgs) Handles ucDiscount.CancelledManagerApproval
        Session("mgrApproval") = "N"
        lbl_desc.Text = Resources.POSMessages.MSG0038
    End Sub


    Private Sub updateSOLNonUponCommit()

        Dim tmpItmAllTbl As New DataTable

        ' create the object for header and transaction info   
        Dim slsp1 As String = lbl_hdr_slsp1_cd.Text
        Dim slsp2 As String = lbl_hdr_slsp2_cd.Text
        Dim pctOfSale1 As Double = txt_comm1.Text
        Dim pctOfSale2 As Double = IIf(SystemUtils.isNumber(txt_comm2.Text), txt_comm2.Text, 0)
        Dim ds As DataSet = Session("DEL_DOC_LN#")

        Try
            Dim headerInfo As SoHeaderDtc = OrderUtils.GetSoHeader(Session.SessionID.ToString.Trim,
                                                              slsp1, slsp2, pctOfSale1, IIf(IsNothing(pctOfSale2), 0, pctOfSale2),
                                                             Session("tet_cd"), Session("TAX_CD"),
                                                             Session("STORE_ENABLE_ARS"), Session("ZONE_CD"),
                                                             Session("PD"), Session("pd_store_cd"),
                                                             Session("cash_carry"))

            If (Not IsNothing(Session("TAX_CD")) AndAlso Not String.IsNullOrEmpty(Session("TAX_CD"))) Then

                headerInfo.writtenDt = txt_wr_dt.Text ' tax date is always written date even for add on lines
                Dim taxPcts As TaxUtils.taxPcts = TaxUtils.SetTaxPcts(headerInfo.writtenDt, headerInfo.taxCd)

                headerInfo.taxRates = taxPcts.itmTpPcts  'SetTxPcts(headerInfo.soWrDt, headerInfo.taxCd).itmTpPcts
            End If

            If AppConstants.PkgSplit.ON_COMMIT = ConfigurationManager.AppSettings("package_breakout").ToString Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If dr.Item("ITM_TP_CD").ToString = "PKG" AndAlso dr.Item("UNIT_PRC") > 0 Then
                        ' create pkg components in temp_itm
                        Dim tempLines As DataTable = theSalesBiz.UpdatePkgCmpntsToSOLNonUponCommit(
                                                        headerInfo,
                                                        dr.Item("ITM_CD").ToString,
                                                        Session.SessionID.ToString.Trim,
                                                        Request("del_doc_num"),
                                                        ds)
                    End If
                Next
            End If

        Catch ex As Exception
            Dim err As String = ex.ToString
        End Try
        If Not (Err.ToString) Is Nothing And Not (Err.ToString = String.Empty) Then
            Session("DEL_DOC_LN#") = Session("DEL_DOC_LNOLD#")
        End If
    End Sub

    'event that handles the popup call back
    Protected Sub ReplaceSKU(sender As Object, e As EventArgs) Handles SKU1.EventItemSelected

        Dim selectedSku As DataTable = SKU1.SelectedSKU
        'Response.Write(selectedSku)
        ASPxPopupSKU.Visible = False
        ASPxPopupSKU.ShowOnPageLoad = False
        Dim ds As DataSet = CType(Session("DEL_DOC_LN#"), DataSet)
        Dim dt As DataTable = ds.Tables(0)
        If Not dt.Columns.Contains("ISREPLACED") Then
            dt.Columns.Add("ISREPLACED", Type.GetType("System.String"))
        End If
        Dim selectedRow As Integer = CType(Session("SELECTEDSKUINDEX"), Integer)
        Dim toBeReplace As DataTable = New DataTable()
        Dim NewItemrecord As DataRow = dt.NewRow()
        'Dim record As DataRow
        Dim replaceRow As DataRow = dt.Rows(selectedRow)
        'NewItemrecord.ItemArray = replaceRow.ItemArray
        Dim record As DataRow
        Dim toBeUnReserved As DataSet = New DataSet()
        Dim FilterText As String
        If Session("REPLACED") Is Nothing Then
            'Copy the contents from view state
            toBeReplace.Columns.Add("OldSku", Type.GetType("System.String"))
            toBeReplace.Columns.Add("NewSku", Type.GetType("System.String"))
            toBeReplace.Columns.Add("LINE", Type.GetType("System.String"))
            'Create an empty dataset which. this dataset will have all the items to be un reserved
            toBeUnReserved = ds.Copy()
            toBeUnReserved.Clear()
        Else
            toBeReplace = CType(Session("REPLACED"), DataTable)
            toBeUnReserved = CType(Session("UNRESERVE"), DataSet)
        End If

        Dim xchng As DataRow = toBeReplace.NewRow()
        'Dim drFiltered As DataRow() = toBeReplace.Select("OldSku = '" & selectedSku.Rows(0)("ITM_CD").ToString() & "' and LINE = '" + record("DEL_DOC_LN#").ToString() + "'")
        Dim drFiltered As DataRow() = toBeReplace.Select("LINE = '" + replaceRow("DEL_DOC_LN#").ToString() + "'")
        If drFiltered.Length = 0 Then
            'Add a new row
            xchng("OldSku") = replaceRow("ITM_CD").ToString()
            xchng("NewSku") = selectedSku.Rows(0)("ITM_CD").ToString()
            xchng("LINE") = replaceRow("DEL_DOC_LN#").ToString()
            toBeReplace.Rows.Add(xchng)
            toBeUnReserved.Tables(0).ImportRow(replaceRow)
            theSalesBiz.UnreserveLine(replaceRow)
        Else
            'toBeUnReserved.AcceptChanges()
        End If
        Session("UNRESERVE") = toBeUnReserved
        Session("REPLACED") = toBeReplace

        Dim itmDetails = getNewLineForReplace(selectedSku.Rows(0)("ITM_CD").ToString())

        itmDetails("ORIG_PRC") = replaceRow("UNIT_PRC")
        itmDetails("MANUAL_PRC") = replaceRow("UNIT_PRC")
        itmDetails("ITM_RET_PRC") = replaceRow("ITM_RET_PRC")
        itmDetails("DEL_DOC_NUM") = replaceRow("DEL_DOC_NUM")
        itmDetails("DEL_DOC_LN#") = replaceRow("DEL_DOC_LN#")
        itmDetails("RES_ID") = ""
        itmDetails("STORE_CD") = ""
        itmDetails("LOC_CD") = ""
        itmDetails("QTY") = replaceRow("QTY")
        itmDetails("PKG_SOURCE") = replaceRow("PKG_SOURCE")
        itmDetails("SHU") = "N"
        itmDetails("DISC_AMT") = replaceRow("DISC_AMT")
        itmDetails("UNIT_PRC") = replaceRow("UNIT_PRC") - replaceRow("DISC_AMT")
        itmDetails("ISREPLACED") = "Y"
        dt.Rows.RemoveAt(CType(Session("SELECTEDSKUINDEX"), Integer))
        dt.Rows.InsertAt(itmDetails, CType(Session("SELECTEDSKUINDEX"), Integer))

        Session("DEL_DOC_LN#") = ds
        GridView1.DataSource = ds
        GridView1.DataBind()
        bindgrid()
        doAfterBindgrid()
        ViewState("ReplaceInvoked") = False
    End Sub

    'Perform all the task as done adding a new line.
    Private Function getNewLineForReplace(ByVal itemcd As String) As DataRow

        Dim dsItm As DataSet = New DataSet()
        Dim itms As ArrayList = New ArrayList()
        itms.Add(itemcd)
        dsItm = theSkuBiz.GetItemDetailsWithSortCodesForSOM(itms, txt_store_cd.Text)
        Dim ds As DataSet = CType(Session("DEL_DOC_LN#"), DataSet)
        Dim dt As DataTable = ds.Tables(0)
        If Not dt.Columns.Contains("ISREPLACED") Then
            dt.Columns.Add("ISREPLACED", Type.GetType("System.String"))
        End If

        Dim ret_prc As Double
        Dim drNew As DataRow = dsItm.Tables(0).Rows(0)
        Dim dr As DataRow
        Dim newUnitPrice As Double = 0
        Dim maxRetPrc As Double = 0
        Dim maxCrmQty As Double = 0
        Dim origSoLnNum As Integer = 0
        ProcessCreditLine(maxCrmQty, maxRetPrc, origSoLnNum)

        If Not Session("DEL_DOC_LN#") Is Nothing Then

            ret_prc = CDbl(drNew("RET_PRC").ToString)
            ret_prc = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                 txt_store_cd.Text,
                                                 String.Empty,
                                                 FormatDateTime(Today.Date, DateFormat.ShortDate),
                                                 drNew("ITM_CD").ToString,
                                                 ret_prc)

            If CDbl(newUnitPrice) = CDbl(0) Then
                If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then
                    newUnitPrice = maxRetPrc
                Else
                    newUnitPrice = ret_prc
                End If
            End If

            dr = dt.NewRow
            dr("ITM_CD") = drNew("ITM_CD").ToString
            dr("COMM_CD") = drNew("COMM_CD").ToString
            dr("VE_CD") = drNew("VE_CD").ToString
            dr("ITM_TP_CD") = drNew("ITM_TP_CD").ToString
            dr("SERIAL_TP") = drNew("SERIAL_TP").ToString
            dr("INVENTORY") = drNew("inventory").ToString
            dr("WARRANTABLE") = drNew("WARRANTABLE").ToString
            'Added for the  purpose of discount
            If (Not IsDBNull(drNew("MAX_DISC_PCNT"))) Then dr("MAX_DISC_PCNT") = CDbl(drNew("MAX_DISC_PCNT").ToString)
            dr("MNR_CD") = If(IsDBNull(drNew("MNR_CD")), "", drNew("MNR_CD").ToString)
            dr("WARR_DAYS") = CDbl(drNew("WARR_DAYS").ToString)
            dr("TYPECODE") = drNew("ITM_TP_CD").ToString
            dr("IsStoreFromInvXref") = False
            If drNew("point_size").ToString.isNotEmpty AndAlso
                IsNumeric(drNew("point_size").ToString()) AndAlso
                CDbl(drNew("point_size")) > 0 Then
                dr("delivery_points") = CDbl(drNew("point_size").ToString)
            End If
            dr("UNIT_PRC") = newUnitPrice
            dr("ORIG_PRC") = newUnitPrice
            dr("ITM_RET_PRC") = newUnitPrice
            dr("MANUAL_PRC") = newUnitPrice
            dr("VOID_FLAG") = "N"
            dr("VSN") = drNew("VSN").ToString
            dr("DES") = drNew("DES").ToString
            dr("NEW_LINE") = "Y"
            dr("TYPECODE") = "CMP"
            If drNew("inventory").ToString = "Y" Then
                dr("PICKED") = "Z"
            Else
                dr("PICKED") = "X"
            End If
            dr("VOID_FLAG") = "N"
            If IsNumeric(drNew("SPIFF")) Then
                If CDbl(drNew("SPIFF")) > 0 Then
                    dr("SPIFF") = drNew("SPIFF")
                End If
            End If
            dr("DROP_CD") = drNew("DROP_CD")
            dr("DROP_DT") = drNew("DROP_DT")

            'for a new line in mgmt, default salespersons from the order header values 
            dr("LN_SLSP1") = lbl_hdr_slsp1_cd.Text
            dr("LN_SLSP1_PCT") = txt_comm1.Text
            dr("LN_SLSP2") = lbl_hdr_slsp2_cd.Text
            dr("LN_SLSP2_PCT") = txt_comm2.Text
            If OrderUtils.isCredDocWithOrigSal(Session("ord_tp_cd"), Session("orig_del_doc_num")) Then
                dr("MAX_CRM_QTY") = maxCrmQty
                dr("MAX_RET_PRC") = maxRetPrc
                dr("ORIG_SO_LN_NUM") = origSoLnNum
            End If
            dr("SORTCODES") = drNew("SORTCODE").ToString()
        End If
        Return dr
        'Next
    End Function

    Private Function RemoveInventoryForReplacedItem(command As OracleCommand)
        Dim updt As soLn_updates = New soLn_updates
        Dim curr_store_cd As String = ""
        Dim orig_store_cd As String = ""
        Dim curr_loc_cd As String = ""
        Dim orig_loc_cd As String = ""
        Dim serial_num As String = ""
        Dim pkg_itm_cd As String = ""
        Dim origOrdLn As OrderLine
        Dim chngOrdLn As OrderLine
        Dim orig_dr As DataRow
        Dim cst As String = String.Empty
        Dim orig_ds As DataSet = Session("ORIG_SO")
        orig_dr = orig_ds.Tables(0).Rows(0)
        Dim dsUnReserve As DataSet = DirectCast(Session("UNRESERVE"), DataSet)
        If Not dsUnReserve Is Nothing AndAlso dsUnReserve.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In dsUnReserve.Tables(0).Rows
                theSalesBiz.UnreserveLine(dr)
                dr("VOID_FLAG") = "Y"
                ProcessPOLinks(dr, command)
                cst = If(IsDBNull(dr("FIFO_CST")), "", dr("FIFO_CST"))
                SoLnPreInvUpdtProcess(dr, orig_dr, updt, origOrdLn, chngOrdLn, command)
                If dr("INVENTORY").ToString = "Y" Then
                    If txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM And dr("VOID_FLAG", DataRowVersion.Current).ToString = "N" And curr_store_cd & "" <> "" Then
                        Credit_Inventory(command, dr("QTY"), dr("DEL_DOC_NUM"), dr("ITM_CD"), curr_store_cd, curr_loc_cd, dr("DEL_DOC_LN#"), dr("FIFL_DT").ToString, dr("FIFO_CST").ToString,
                                         dr("FIFO_DT").ToString, 0, chngOrdLn, dr("SERIAL_NUM").ToString, dr("VOID_FLAG", DataRowVersion.Current).ToString, dr("OUT_ID").ToString, dr("OUT_CD").ToString)
                        updt.inv = True

                    ElseIf txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL Then
                        ' reserve, unreserve, or both as needed
                        updt.inv = updtInventory(dr, chngOrdLn, command)

                    End If
                    If lbl_desc.Text & "" <> "" Then ' InStr(lbl_desc.Text, "Error") > 0 Then - DSA label_desc is not set yet or doesn't have 
                        'lbl_desc.Text = Session("err") & "Errors were encountered and the operation could not be completed."
                        Throw New ResUnresException(lbl_desc.Text & vbCrLf & "Errors were encountered and the operation could not be completed.")

                    ElseIf dr("VOID_FLAG", DataRowVersion.Current).ToString = "N" AndAlso curr_store_cd & "" <> "" AndAlso
                         (txt_ord_tp_cd.Text = AppConstants.Order.TYPE_SAL OrElse txt_ord_tp_cd.Text = AppConstants.Order.TYPE_CRM) Then

                        Insert_ITM_TRN(dr("QTY"), dr("DEL_DOC_NUM"), dr("ITM_CD"), curr_store_cd, curr_loc_cd, dr("DEL_DOC_LN#"), dr("FIFL_DT").ToString,
                                       cst.ToString, dr("FIFO_DT").ToString, command, dr("SERIAL_NUM").ToString, chngOrdLn.outId, dr("OUT_CD").ToString)
                    ElseIf (dr("VOID_FLAG", DataRowVersion.Current).ToString = "Y") Then
                        If ("L".Equals(ViewState("TaxMethod"))) Or ("H".Equals(ViewState("TaxMethod"))) Or ConfigurationManager.AppSettings("tax_line").ToString = "Y" Then
                            updt.tax = True
                        ElseIf (Convert.ToBoolean(ViewState("TaxExempted"))) Then
                            updt.tax = True
                        End If
                    End If
                End If
                Updt_SoLn(origOrdLn, chngOrdLn, updt, command)
            Next
        End If
    End Function

    Protected Sub HandleCancel(sender As Object, e As EventArgs) Handles SKU1.EventHandleCancel
        ASPxPopupSKU.Visible = False
        ASPxPopupSKU.ShowOnPageLoad = False
        ViewState("ReplaceInvoked") = False
    End Sub
    'MM-7366
    Protected Sub ASPxButton_Yes_Click(sender As Object, e As EventArgs)
        Dim ds As DataSet = Session("DEL_DOC_LN#")
        Dim dsORIG As DataSet = Session("ORIG_SO")
        Dim dr As DataRow
        Dim SLSP1 As String = String.Empty
        Dim SLSP2 As String = String.Empty

        Try
            PopupControlSalespersonConform.ShowOnPageLoad = False
            dsORIG.Tables(0).Rows(0)("SLSP1") = txt_slsp1.Text.Trim
            dsORIG.Tables(0).Rows(0)("SLSP2") = txt_slsp2.Text.Trim
            dsORIG.Tables(0).Rows(0)("PCT_OF_SALE1") = txt_comm1.Text.Trim

            If Not txt_comm2.Text = String.Empty Then
                dsORIG.Tables(0).Rows(0)("PCT_OF_SALE2") = txt_comm2.Text.Trim
            Else
                dsORIG.Tables(0).Rows(0)("PCT_OF_SALE2") = 0.0
            End If
            For Each dr In ds.Tables(0).Rows
                If dr("VOID_FLAG") = "N" Then
                    UpdateSalesPersonInfo(dr)
                End If
            Next
            Session("DEL_DOC_LN#") = ds
            GridView1.DataSource = Session("DEL_DOC_LN#")
            GridView1.DataBind()
            doAfterBindgrid()
        Catch ex As Exception

        End Try
    End Sub
    'MM-7366
    Protected Sub ASPxButton_No_Click(sender As Object, e As EventArgs)
        Dim dsORIG As DataSet = Session("ORIG_SO")
        Dim SLSP1 As String = String.Empty
        Dim SLSP2 As String = String.Empty
        Try
            PopupControlSalespersonConform.ShowOnPageLoad = False
            dsORIG.Tables(0).Rows(0)("SLSP1") = txt_slsp1.Text.Trim
            dsORIG.Tables(0).Rows(0)("SLSP2") = txt_slsp2.Text.Trim
            dsORIG.Tables(0).Rows(0)("PCT_OF_SALE1") = txt_comm1.Text.Trim
            If Not txt_comm2.Text = String.Empty Then
                dsORIG.Tables(0).Rows(0)("PCT_OF_SALE2") = txt_comm2.Text.Trim
            Else
                dsORIG.Tables(0).Rows(0)("PCT_OF_SALE2") = 0.0
            End If
        Catch ex As Exception

        End Try
    End Sub

    'MM-7366   
    Private Sub UpdateSalesPersonInfo(ByRef datarow As DataRow)

        salesInfo = ViewState("STORESALESPERSONINFO")
        Dim updateSalesPerson1 As Boolean = False
        Dim updateSalesPerson2 As Boolean = False
        Dim updateSalesPercentage As Boolean = False
        Dim updateBothSalesPerson As Boolean = False
        'Dim MyDataReader As OracleDataReader = Nothing

        Dim sls1 As String = salesInfo(1)
        ' Dim sls2 As String = salesInfo(2)
        Dim sls2 As String = If(String.IsNullOrEmpty(salesInfo(2)), "", salesInfo(2))
        Dim sls3 As String = salesInfo(3)
        Dim sls4 As String = salesInfo(4)

        'Dim sql As String
        Try
            If (salesInfo(1) = datarow("LN_SLSP1")) AndAlso ((salesInfo(2) = datarow("LN_SLSP2").ToString)) Then
                'This is to update the sales person with NEW VALUE
                updateBothSalesPerson = False
                If (lbl_hdr_slsp1_cd.Text.Equals(datarow("LN_SLSP1"))) AndAlso (lbl_hdr_slsp2_cd.Text.Equals(datarow("LN_SLSP2").ToString)) Then
                    'This is to update the sales person with NEW VALUE
                    updateBothSalesPerson = False
                Else
                    Dim slsp1 = If(Not String.IsNullOrEmpty(ViewState("LBL_HDR_SLSP1_CD")), ViewState("LBL_HDR_SLSP1_CD").ToString(), "")
                    Dim slsp2 = If(Not String.IsNullOrEmpty(ViewState("LBL_HDR_SLSP2_CD")), ViewState("LBL_HDR_SLSP2_CD").ToString(), "")
                    If lbl_hdr_slsp1_cd.Text.Equals(datarow("LN_SLSP1")) Then
                        'If the old SALES person1 code is same as current then no need to update
                        updateSalesPerson1 = False
                    Else
                        If (slsp1.ToString = (datarow("LN_SLSP1").ToString)) AndAlso (ViewState("COMM1").ToString = datarow("LN_SLSP1_PCT")) Then
                            updateSalesPerson1 = True
                        End If
                        'updateSalesPerson1 = True
                    End If

                    If lbl_hdr_slsp2_cd.Text.Equals(datarow("LN_SLSP2").ToString) Then
                        'If the old SALES person2 code is same as current then no need to update
                        updateSalesPerson2 = False
                    Else
                        If (slsp2.ToString = (datarow("LN_SLSP2").ToString)) AndAlso (ViewState("COMM2").ToString = datarow("LN_SLSP2_PCT")) Then
                            updateSalesPerson2 = True
                        End If
                        'updateSalesPerson2 = True
                    End If
                    If (updateSalesPerson1 AndAlso updateSalesPerson2) Then
                        updateBothSalesPerson = True
                    End If
                End If
            Else
                Dim slsp1 = If(Not String.IsNullOrEmpty(ViewState("LBL_HDR_SLSP1_CD")), ViewState("LBL_HDR_SLSP1_CD").ToString(), "")
                Dim slsp2 = If(Not String.IsNullOrEmpty(ViewState("LBL_HDR_SLSP2_CD")), ViewState("LBL_HDR_SLSP2_CD").ToString(), "")
                If (salesInfo(2) = String.Empty) AndAlso (slsp1.ToString = (datarow("LN_SLSP1").ToString)) AndAlso (ViewState("COMM1").ToString = datarow("LN_SLSP1_PCT")) Then
                    updateSalesPerson1 = True
                ElseIf (slsp1.ToString = (datarow("LN_SLSP1").ToString)) AndAlso (ViewState("COMM1").ToString = datarow("LN_SLSP1_PCT")) Then
                    updateSalesPerson1 = True
                End If
                If (salesInfo(2) = String.Empty) AndAlso (slsp2.ToString = (datarow("LN_SLSP2").ToString)) AndAlso (ViewState("COMM2").ToString = datarow("LN_SLSP2_PCT")) Then
                    updateSalesPerson2 = True
                ElseIf (slsp2.ToString = (datarow("LN_SLSP2").ToString)) AndAlso (ViewState("COMM2").ToString = datarow("LN_SLSP2_PCT")) Then
                    updateSalesPerson2 = True
                End If
                If (updateSalesPerson1 AndAlso updateSalesPerson2) Then
                    updateBothSalesPerson = True
                End If
            End If

            If (salesInfo(3) = datarow("LN_SLSP1_PCT")) AndAlso (salesInfo(4) = datarow("LN_SLSP2_PCT")) Then
                'If the both sales percentage is same then need to update with new VALUES
                updateSalesPercentage = False
                If (txt_comm1.Text = datarow("LN_SLSP1_PCT")) AndAlso (txt_comm2.Text = datarow("LN_SLSP2_PCT")) Then
                    updateSalesPercentage = False
                Else
                    updateSalesPercentage = True
                End If
            ElseIf (salesInfo(2) = String.Empty) Then
                If ((ViewState("COMM1").ToString = datarow("LN_SLSP1_PCT")) AndAlso (ViewState("COMM2").ToString = datarow("LN_SLSP2_PCT"))) Then
                    updateSalesPercentage = True
                End If
            Else
                If ((ViewState("COMM1").ToString = datarow("LN_SLSP1_PCT")) AndAlso (ViewState("COMM2").ToString = datarow("LN_SLSP2_PCT"))) Then
                    updateSalesPercentage = True
                End If
            End If

            'To updates both sales people percentage and employee codes on the sales order lines
            If (updateBothSalesPerson AndAlso updateSalesPercentage) Then
                ' UPDATE SO_LN for SLSP1, SLSP2, EMPCD1, EMPCD2
                'UpdateSalesPersonInfoonLine(dbCommand, lbl_hdr_slsp1_cd.Text, lbl_hdr_slsp2_cd.Text, Convert.ToDouble(txt_comm1.Text), Convert.ToDouble(txt_comm2.Text), delDocNumber, delDocLineNumber)
                datarow("LN_SLSP1") = lbl_hdr_slsp1_cd.Text
                datarow("LN_SLSP2") = lbl_hdr_slsp2_cd.Text
                datarow("LN_SLSP1_PCT") = Convert.ToDouble(txt_comm1.Text)
                datarow("LN_SLSP2_PCT") = Convert.ToDouble(txt_comm2.Text)

                'To update both sales people percentage and Sales person1 employee code on the sales order lines
            ElseIf (updateSalesPercentage AndAlso updateSalesPerson1) Then
                'UpdateSalesPersonAndPercentageonLine(dbCommand, 1, lbl_hdr_slsp1_cd.Text, Convert.ToDouble(txt_comm1.Text), Convert.ToDouble(txt_comm2.Text), delDocNumber, delDocLineNumber)
                datarow("LN_SLSP1") = lbl_hdr_slsp1_cd.Text
                datarow("LN_SLSP1_PCT") = Convert.ToDouble(txt_comm1.Text)
                datarow("LN_SLSP2_PCT") = Convert.ToDouble(txt_comm2.Text)

                'To update both sales people percentage and Sales person2 employee code on the sales order lines
            ElseIf (updateSalesPercentage AndAlso updateSalesPerson2) Then
                'UpdateSalesPersonAndPercentageonLine(dbCommand, 2, lbl_hdr_slsp2_cd.Text, Convert.ToDouble(txt_comm1.Text), Convert.ToDouble(txt_comm2.Text), delDocNumber, delDocLineNumber)                
                datarow("LN_SLSP2") = lbl_hdr_slsp2_cd.Text
                datarow("LN_SLSP1_PCT") = Convert.ToDouble(txt_comm1.Text)
                datarow("LN_SLSP2_PCT") = Convert.ToDouble(txt_comm2.Text)

                'To update only sales percentage both the sales person should be same
            ElseIf updateSalesPercentage AndAlso ((lbl_hdr_slsp1_cd.Text.Equals(datarow("LN_SLSP1"))) AndAlso (lbl_hdr_slsp2_cd.Text.Equals(datarow("LN_SLSP2")))) Then
                'UpdateSalesPercentageonLine(dbCommand, Convert.ToDouble(txt_comm1.Text), Convert.ToDouble(txt_comm2.Text), delDocNumber, delDocLineNumber)
                datarow("LN_SLSP1_PCT") = Convert.ToDouble(txt_comm1.Text)
                datarow("LN_SLSP2_PCT") = Convert.ToDouble(txt_comm2.Text)

                'To update both sales people employee codes on the sales order lines
            ElseIf updateBothSalesPerson Then
                'UpdateSalesPersonEmployeeCodeonLine(dbCommand, 0, lbl_hdr_slsp1_cd.Text, delDocNumber, delDocLineNumber, lbl_hdr_slsp2_cd.Text)
                datarow("LN_SLSP1") = lbl_hdr_slsp1_cd.Text
                datarow("LN_SLSP2") = lbl_hdr_slsp2_cd.Text

                'To update Sales Person1 employee code on the sales order lines
            ElseIf updateSalesPerson1 Then
                'UpdateSalesPersonEmployeeCodeonLine(dbCommand, 1, lbl_hdr_slsp1_cd.Text, delDocNumber, delDocLineNumber)
                datarow("LN_SLSP1") = lbl_hdr_slsp1_cd.Text

                'To update Sales Person2 employee code on the sales order lines
            ElseIf updateSalesPerson2 Then
                'UpdateSalesPersonEmployeeCodeonLine(dbCommand, 2, lbl_hdr_slsp2_cd.Text, delDocNumber, delDocLineNumber)
                datarow("LN_SLSP2") = lbl_hdr_slsp2_cd.Text
                'To update both sales people percentage on the sales order lines                    
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub
    'MM-7366
    Private Sub StoreSalesPersonInfo()
        salesInfo.Add(1, lbl_hdr_slsp1_cd.Text)
        salesInfo.Add(2, lbl_hdr_slsp2_cd.Text)
        salesInfo.Add(3, txt_comm1.Text)
        salesInfo.Add(4, txt_comm2.Text)
        ViewState("STORESALESPERSONINFO") = salesInfo
    End Sub

    Private Sub ShowWarrantyPopup(focusWarr As String)
        Dim itmCd As String
        Dim isInventory As Boolean
        Dim isWarrantable As Boolean
        Dim isVoided As Boolean

        Dim dtWarrantable As New DataTable
        dtWarrantable.Columns.Add("ROW_ID", GetType(String))
        dtWarrantable.Columns.Add("ITM_CD", GetType(String))
        dtWarrantable.Columns.Add("ITM_TP_CD", GetType(String))
        dtWarrantable.Columns.Add("WARR_ROW_ID", GetType(String))
        dtWarrantable.Columns.Add("WARR_ITM_CD", GetType(String))
        dtWarrantable.Columns.Add("WARR_ITM_TEXT", GetType(String))
        dtWarrantable.Columns.Add("IS_PREV_SEL", GetType(Boolean))
        Dim drWarrantable As DataRow

        Dim dtSOMItems As DataTable

        If Not IsNothing(Session("DEL_DOC_LN#")) Then
            Dim dsSOMItems As DataSet = CType(Session("DEL_DOC_LN#"), DataSet)
            If dsSOMItems.Tables.Count > 0 Then dtSOMItems = dsSOMItems.Tables(0)
            Dim objMerchBiz As MerchBiz = New MerchBiz()

            For Each dr As DataRow In dtSOMItems.Rows
                itmCd = dr("ITM_CD")
                isInventory = IIf("Y".Equals(dr.Item("INVENTORY") + ""), True, False)
                isWarrantable = IIf("Y".Equals(dr.Item("WARRANTABLE") + ""), True, False)
                isVoided = IIf("Y".Equals(dr.Item("VOID_FLAG") + ""), True, False)

                If isInventory AndAlso isWarrantable AndAlso (Not isVoided) AndAlso
                    Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL Then
                    If SalesUtils.hasWarranty(itmCd) Then
                        If SysPms.isOne2ManyWarr Then
                            Dim where As String = "VOID_FLAG ='N' AND ITM_TP_CD= '" & SkuUtils.ITM_TP_WAR & "'"
                            Dim warrantyRows() As DataRow = dtSOMItems.Select(where)
                            Dim tempWarrObj As TempWarrDtc

                            For i As Integer = 0 To warrantyRows.GetUpperBound(0)
                                Dim warrantyRow As DataRow = (warrantyRows(i))

                                If (objMerchBiz.IsValWarrForItm(itmCd, warrantyRow("ITM_CD").ToString)) Then
                                    tempWarrObj = New TempWarrDtc()
                                    tempWarrObj.sessId = warrantyRow("DEL_DOC_NUM")
                                    tempWarrObj.itmCd = warrantyRow("ITM_CD").ToString
                                    tempWarrObj.lnNum = warrantyRow("DEL_DOC_LN#").ToString
                                    tempWarrObj.retPrc = CDbl(warrantyRow("unit_prc"))
                                    tempWarrObj.vsn = warrantyRow("VSN").ToString
                                    tempWarrObj.days = warrantyRow("WARR_DAYS")
                                    HBCG_Utils.CreateTempWarr(tempWarrObj)
                                End If
                            Next i
                        End If

                        drWarrantable = dtWarrantable.NewRow()
                        drWarrantable("ROW_ID") = dr("DEL_DOC_LN#")
                        drWarrantable("ITM_CD") = itmCd
                        drWarrantable("ITM_TP_CD") = dr("ITM_TP_CD")
                        drWarrantable("WARR_ROW_ID") = dr("WARR_BY_LN")
                        drWarrantable("WARR_ITM_CD") = dr("WARR_BY_SKU")

                        If dtWarrantable.Rows.Count > 0 Then
                            Dim filterExp As String = "IS_PREV_SEL = FALSE AND WARR_ROW_ID = '" & dr("DEL_DOC_LN#") & "' AND WARR_ITM_CD = '" & dr("WARR_BY_SKU") & "'"
                            Dim drFiltered As DataRow() = dtWarrantable.Select(filterExp)
                            drWarrantable("IS_PREV_SEL") = drFiltered.Length > 0
                        Else
                            drWarrantable("IS_PREV_SEL") = False
                        End If

                        dtWarrantable.Rows.Add(drWarrantable)
                    End If
                End If
            Next
        End If

        If dtWarrantable.Rows.Count > 0 Then
            Dim _popupWarranties As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucWarranties.FindControl("popupWarranties")
            _popupWarranties.Modal = True

            Dim _hidDelDocNum As HiddenField = _popupWarranties.FindControl("hidDelDocNum")
            _hidDelDocNum.Value = txt_del_doc_num.Text

            Dim _hidFocusItem As HiddenField = _popupWarranties.FindControl("hidFocusItem")
            _hidFocusItem.Value = IIf(String.IsNullOrEmpty(focusWarr), GetCurrentLineNum(), focusWarr)

            _popupWarranties.ShowOnPageLoad = True
            ucWarranties.LoadWarranties(dtWarrantable) ', dtWarrantedItem, "SOM"
        ElseIf Not String.IsNullOrEmpty(hidEnteredSKUs.Value) Then
            Dim relatedSku As String = hidEnteredSKUs.Value
            hidEnteredSKUs.Value = String.Empty

            ShowRelatedSkuPopUp(relatedSku)
        End If
    End Sub

    Protected Sub ucWarranties_WarrantyAdded(sender As Object, e As EventArgs) Handles ucWarranties.WarrantyAdded
        If Not IsNothing(Session("WARRINFO")) Then
            Dim dtWarrInfo As DataTable = CType(Session("WARRINFO"), DataTable)
            Dim sortExp As String = "WARR_ROW_ID, IS_PREV_SEL"
            Dim dt As DataTable = dtWarrInfo.Select(Nothing, sortExp).CopyToDataTable()

            Dim soLnDataSet As New DataSet
            Dim soLnDatTbl As New DataTable
            Dim drSoLn As DataRow()

            If Not IsNothing(Session("DEL_DOC_LN#")) Then
                soLnDataSet = Session("DEL_DOC_LN#")
                If soLnDataSet.Tables.Count > 0 Then
                    soLnDatTbl = soLnDataSet.Tables(0)
                End If
            End If

            Dim warrRowIdSeqNum As String = String.Empty
            Dim warrItmCd As String
            Dim drWarrRowId() As DataRow

            SetupForDiscReCalc()

            For Each dr As DataRow In dt.Rows
                warrItmCd = String.Empty
                'Need to fill the WARR_ROW_ID from warrRowIdSeqNum
                If Not IsDBNull(dr("IS_PREV_SEL")) AndAlso dr("IS_PREV_SEL") = True Then
                    warrItmCd = dr("WARR_ITM_CD")
                    drWarrRowId = dt.Select("IS_PREV_SEL = FALSE AND WARR_ITM_CD = '" + warrItmCd + "'")

                    If drWarrRowId.Length > 0 Then
                        dr("WARR_ROW_ID") = drWarrRowId(drWarrRowId.Length - 1)("WARR_ROW_ID")
                    Else
                        dr("WARR_ROW_ID") = String.Empty
                    End If
                End If

                warrRowIdSeqNum = String.Empty

                '***Check for Warranties being added or not after returning from warranties popup *******
                If Not IsDBNull(dr("WARR_ITM_CD")) AndAlso Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                    If String.IsNullOrEmpty(dr("WARR_ROW_ID")) Then
                        '*** Implies that a new warranty sku is being added after the popup selection. In this case a new 
                        '*** line for the warranty sku needs to be added. Then, the link info on the warrantable sku 
                        '*** also needs to be updated.
                        If Not String.IsNullOrEmpty(dr("WARR_ITM_CD")) Then
                            '--means a warrantable sku was selected
                            AddLine(dr("WARR_ITM_CD"), 0, 0, "", "", False)
                            warrRowIdSeqNum = GetCurrentLineNum()
                            dr("WARR_ROW_ID") = warrRowIdSeqNum
                        End If
                    End If
                End If

                drSoLn = soLnDatTbl.Select("[DEL_DOC_LN#] = " + dr("ROW_ID"))

                If drSoLn.Length > 0 Then
                    drSoLn(0)("WARR_BY_LN") = dr("WARR_ROW_ID")
                    drSoLn(0)("WARR_BY_DOC_NUM") = drSoLn(0)("DEL_DOC_NUM")
                    drSoLn(0)("WARR_BY_SKU") = dr("WARR_ITM_CD")
                End If

                drSoLn = Nothing
            Next

            'Remove the voided Warranties
            If soLnDatTbl.Rows.Count > 0 Then
                Dim drWarSKUs As DataRow() = soLnDatTbl.Select("TYPECODE = '" & AppConstants.Sku.TP_WAR & "'")

                For Each dr As DataRow In drWarSKUs
                    If soLnDatTbl.Select("WARR_BY_LN = '" & dr("DEL_DOC_LN#") & "'").Length < 1 Then
                        Dim drWarRow As DataRow() = soLnDatTbl.Select("[DEL_DOC_LN#] = '" & dr("DEL_DOC_LN#") & "'")
                        If drWarRow.Length > 0 Then
                            For Each drWar As DataRow In drWarRow
                                UpdateDataRowLinkInfo(dr("DEL_DOC_LN#"), True)
                            Next
                        End If
                    End If
                Next
            End If

            Session("DEL_DOC_LN#") = soLnDataSet
            DoDiscReCalcNowOrLater()
            bindgrid()
            doAfterBindgrid()
            calculate_total()
        End If

        Session.Remove("WARRINFO")

        If Not String.IsNullOrEmpty(hidEnteredSKUs.Value) Then
            Dim relatedSku As String = hidEnteredSKUs.Value
            hidEnteredSKUs.Value = String.Empty

            ShowRelatedSkuPopUp(relatedSku)
        End If
    End Sub

    Protected Sub ucWarranties_NoWarrantyAdded(sender As Object, e As EventArgs) Handles ucWarranties.NoWarrantyAdded
        If Not String.IsNullOrEmpty(hidEnteredSKUs.Value) Then
            Dim relatedSku As String = hidEnteredSKUs.Value
            hidEnteredSKUs.Value = String.Empty

            ShowRelatedSkuPopUp(relatedSku)
        End If
    End Sub

    Private Sub ShowRelatedSkuPopUp(itemcd As String)
        If Not String.IsNullOrEmpty(itemcd) And Session("ord_tp_cd") = AppConstants.Order.TYPE_SAL Then
            ucRelatedSku.LoadRelatedSKUData(itemcd.Replace("'", String.Empty).TrimStart(",").TrimEnd(","))
        End If
    End Sub



    Protected Sub ucRelatedSku_AddSelection(sender As Object, e As EventArgs, selectedRelSKUs As List(Of Object)) Handles ucRelatedSku.AddSelection
        Dim skus As String = String.Empty

        For Each obj As Object In selectedRelSKUs
            If Not IsNothing(obj) AndAlso Not String.IsNullOrEmpty(obj) Then
                skus = skus & "," & UCase(obj.ToString())
            End If
        Next

        If Not String.IsNullOrEmpty(skus) Then
            SetupForDiscReCalc()

            Dim showWarrPopup As Boolean = False

            Dim INSRTED As Boolean = AddLine(skus.TrimStart(",").TrimEnd(","), String.Empty, String.Empty, String.Empty, String.Empty, showWarrPopup)

            If INSRTED Then
                If showWarrPopup Then ShowWarrantyPopup(String.Empty)
                DoDiscReCalcNowOrLater()
                bindgrid()
                doAfterBindgrid()
                calculate_total()
            End If
        End If
    End Sub


    ''' <summary>
    ''' VOID THE LINE
    ''' </summary>
    ''' <param name="dgItem"></param>
    ''' <remarks></remarks>
    Protected Sub VoidLine(ByRef dgItem As DataGridItem)

        Dim textdata As CheckBox = dgItem.FindControl("ChkBoxVoid")
        Dim ds As DataSet = Session("DEL_DOC_LN#")
        Dim dr As DataRow
        Dim processVoid As Boolean = False
        Dim checkForWarrantyLinks As Boolean = False
        Dim checkForWarrantableLinks As Boolean = False
        'is the warr_by_ln# for a warrantable or the del_docLn# for a warranty
        Dim lineNumToCheckForLinks As String = String.Empty
        'is the line num of the line being voided
        Dim lineNumToVoid As String = String.Empty
        'Dim pkgParentLine As String = String.Empty
        Dim typeCode As String = String.Empty
        Dim doneWithPkg As Boolean = False
        Dim lineToVoid As Integer = Convert.ToInt32(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text)
        ViewState("VoidRow") = Nothing
        Dim delDocLN As Integer = 0
        Dim lc As Integer = 0

        'need to get the actual row position of the DEL_DOC_LN# in dataset
        For Each dr In ds.Tables(0).Rows
            If lineToVoid = Convert.ToInt32(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text) Then
                lineNumToVoid = lc
                typeCode = dr("typecode")
                Exit For
            End If
            lc += 1
        Next

        ' TODO - consider updating the max credit line info when line deleted on CRM/MCR so void/add works
        ' processing thru the lines so that related pkg/components can also be voided 
        For Each dr In ds.Tables(0).Rows
            If typeCode = dr("typecode") AndAlso dr("typecode") = "PKG" AndAlso doneWithPkg = False AndAlso dr("DEL_DOC_LN#") >= lineToVoid Then
                doneWithPkg = True
            ElseIf typeCode = dr("typecode") AndAlso dr("typecode") = "PKG" AndAlso doneWithPkg = True AndAlso dr("DEL_DOC_LN#") >= lineToVoid Then
                Exit For
            Else
                'Nothing
            End If


            processVoid = False
            If (dgItem.Cells(SoLnGrid.NEW_LINE).Text = "N" AndAlso dr("ITM_TP_CD").ToString = "PKG") Then
                If (IsDBNull(dr("PKG_SOURCE")) AndAlso dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text = dr("DEL_DOC_LN#")) Then
                    delDocLN = dr("DEL_DOC_LN#")
                End If
            ElseIf (dgItem.Cells(SoLnGrid.NEW_LINE).Text = "Y" AndAlso dr("ITM_TP_CD").ToString = "PKG") Then
                If (IsDBNull(dr("PACKAGE_PARENT")) AndAlso dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text = dr("DEL_DOC_LN#")) Then
                    delDocLN = dr("DEL_DOC_LN#")
                End If
            End If
            If ((dr("DEL_DOC_LN#") = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text) OrElse
                (Not IsDBNull(dr("PKG_SOURCE")) AndAlso dr("PKG_SOURCE") = dgItem.Cells(SoLnGrid.ITM_CD).Text AndAlso Not (dr("DEL_DOC_LN#") < dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text) AndAlso dr("DEL_DOC_LN#") > delDocLN) OrElse
                (Not IsDBNull(dr("PACKAGE_PARENT")) AndAlso dr("PACKAGE_PARENT") = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text AndAlso Not (dr("DEL_DOC_LN#") < dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text) AndAlso dr("DEL_DOC_LN#") > delDocLN) OrElse
                dr("PACKAGE_PARENT").ToString = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text) Then

                lineNumToVoid = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
                processVoid = textdata.Checked
                If ((Not IsDBNull(dr("PKG_SOURCE")) AndAlso dr("PKG_SOURCE") = dgItem.Cells(SoLnGrid.ITM_CD).Text AndAlso dr("DEL_DOC_LN#") > delDocLN) OrElse
                 (Not IsDBNull(dr("PACKAGE_PARENT")) AndAlso dr("PACKAGE_PARENT") = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text AndAlso dr("DEL_DOC_LN#") > delDocLN)) Then
                    processVoid = True
                    'pkgParentLine = dr("PACKAGE_PARENT").ToString()
                    textdata.Checked = True
                    lineNumToVoid = dr("DEL_DOC_LN#").ToString
                Else
                    If Not (textdata.Checked) Then dr("VOID_FLAG") = "N"
                End If
                ' confirm void allowed if conditional discount minor line
                If Not IsNothing(Session("DISC_APPLIED")) Then

                    Dim ordLn2Disc As DataTable = Session("DISC_APPLIED")
                    If SystemUtils.dataTblIsNotEmpty(ordLn2Disc) AndAlso Not theSalesBiz.CanBeVoided(txt_wr_dt.Text, dr, ds.Tables(0), ordLn2Disc) Then

                        processVoid = False
                        lbl_desc.Text = Resources.POSErrors.ERR0022
                        lbl_desc.Visible = True
                        textdata.Checked = False
                    End If
                End If
            End If
            'Dim removedPrice As Double = 0.0
            If (processVoid) Then
                Dim isInventory As Boolean = ResolveYnToBooleanEquiv(dgItem.Cells(SoLnGrid.INVENTORY).Text.Trim())
                'If ConfigurationManager.AppSettings("package_breakout").ToString().ToUpper() = AppConstants.PkgSplit.ON_COMMIT AndAlso (Not String.IsNullOrEmpty(dr("PACKAGE_PARENT").ToString)) Then
                '    removedPrice = theSalesBiz.updateSplitItmShareOnDelete(dr("PACKAGE_PARENT").ToString(), dr("ITM_CD").ToString)
                '    Dim filterText As String = String.Empty
                '    Dim filteredRows As DataRow()
                '    filterText = "[DEL_DOC_LN#] = " + dr("PACKAGE_PARENT").ToString
                '    filteredRows = ds.Tables(0).Select(filterText)
                '    filteredRows(0)("UNIT_PRC") = CDbl(filteredRows(0)("UNIT_PRC")) - CDbl(removedPrice)
                'End If
                ' ---- REMOVE PENDING (SOFT) RESERVATIONS WHEN PRESENT
                If (isInventory) Then
                    theSalesBiz.UnreserveLine(dr)
                End If

                '*** Now consider any warranties and warrantables to void
                If isInventory Then
                    Dim warrByLineNum As String = String.Empty
                    If (dgItem.Cells(SoLnGrid.WARR_BY_LN).Text.isNotEmpty) Then warrByLineNum = dgItem.Cells(SoLnGrid.WARR_BY_LN).Text
                    If dgItem.Cells(SoLnGrid.WARRANTABLE).Text.isNotEmpty AndAlso "Y".Equals(dgItem.Cells(SoLnGrid.WARRANTABLE).Text) AndAlso warrByLineNum.isNotEmpty AndAlso
                        (dgItem.Cells(SoLnGrid.WARR_BY_DOC_NUM).Text) = dr("DEL_DOC_NUM").ToString Then
                        'This check is added to stop the new SKU to getting voided automatically, when old SKU  is checked to void ,after old SKU associated warranty SKU is moved to the new order. (MM-7172)
                        'If this is a warrantable sku line and has a warranty attached to it, then need to find out if the warranty can be voided
                        lineNumToCheckForLinks = warrByLineNum
                        checkForWarrantyLinks = True
                    Else
                        '*** means a REGULAR inventoriable line with no warranties attached to it is being voided, so just void
                        dr("VOID_FLAG") = "Y"
                        ' MM-4240
                        ' Tax information has been removed to the VOIDED lineSoLnGrid.UNIT_PRC
                        dr("CUST_TAX_CHG") = CDbl(0)
                        dr("TAXABLE_AMT") = CDbl(0)
                        dr("TAX_BASIS") = CDbl(0)
                        dr("TAX_RESP") = ""
                        dr("TAX_CD") = ""
                    End If
                Else
                    If dgItem.Cells(SoLnGrid.ITM_TP_CD).Text.Equals("WAR") Then
                        'means this is a warranty.  If this is linked to any other lines, we need to break the link to this warranty before voiding the warranty
                        lineNumToCheckForLinks = dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text
                        checkForWarrantableLinks = True
                        dr("VOID_FLAG") = "Y"
                    Else
                        dr("VOID_FLAG") = "Y"
                    End If

                End If

                'Find the linked lines and determine what needs to be voided 
                If (textdata.Checked AndAlso (checkForWarrantyLinks OrElse checkForWarrantableLinks)) Then

                    Dim linkedLines = GetLinkedLineNumbers(lineNumToCheckForLinks)
                    If linkedLines.Count > 1 Then
                        'means that there are multiple warrantables to warranties

                        If (checkForWarrantyLinks) Then
                            ' means that the warrantable has other lines linked to it's warranty. So void THIS warrantable line based on line#, 
                            ' But leave the OTHER LINE lines alone- just delink them
                            UpdateDataRowLinkInfo(lineNumToVoid, True)

                        ElseIf (checkForWarrantableLinks) Then
                            'if warranty was being voided there are other lines linked to this warranty, then VOID WARRANTY & delink the OTHER lines 

                            UpdateDataRowLinkInfo(lineNumToVoid, True)
                            For Each ln As String In linkedLines
                                UpdateDataRowLinkInfo(ln)
                            Next
                        End If
                    ElseIf linkedLines.Count = 1 Then
                        'means that this is the only warrantable line that has a warranty linked to it.Void it and nullify the link info

                        If (checkForWarrantyLinks) Then
                            'if the warrantable was being voided, this list would have that same warranted line in it- that line needs to be voided & the warranty voided
                            UpdateDataRowLinkInfo(linkedLines.Item(0), True)      'is the warrantable line to be voided
                            UpdateDataRowLinkInfo(lineNumToCheckForLinks, True)   'is the warranty line to be voided
                        Else
                            'if the warranty was being voided, this list would have the only warranted line linked in it- that line's links needs to be nullified & the warranty voided
                            UpdateDataRowLinkInfo(lineNumToVoid, True)       'is the warranty line to be voided
                            UpdateDataRowLinkInfo(linkedLines.Item(0))       'is the warrantable line to be de-linked
                        End If
                    Else
                        'means that this is a warranty being voided and it is not linked to anything else. 
                        If checkForWarrantableLinks Then UpdateDataRowLinkInfo(lineNumToVoid, True)
                    End If
                End If
            End If
        Next

        'reapplies the discounts (if any) when applicable
        ReapplyDiscounts(ds.Tables(0), ReapplyDiscountRequestDtc.LnChng.Void)

        Session("DEL_DOC_LN#") = ds
        bindgrid()
        doAfterBindgrid()
        calculate_total()

    End Sub

    ''' <summary>
    ''' Remove the warranty on the line
    ''' </summary>
    ''' <param name="dgItem"></param>
    ''' <remarks></remarks>
    Protected Sub RemoveWarranty(ByRef dgItem As DataGridItem)

        Dim chkWarr As CheckBox = dgItem.FindControl("ChkBoxWarr")
        Dim soLnDataSet As DataSet = Session("DEL_DOC_LN#")
        Dim soLnDatTbl As DataTable = soLnDataSet.Tables(0)
        Dim soLnRow As DataRow
        ViewState("WarrRow") = Nothing
        If chkWarr.Checked = True Then
            ' need to check the one to one or one to many warr syspm, pop-up the warranty window & update links
            If SysPms.isOne2ManyWarr OrElse 1 = CInt(dgItem.Cells(SoLnGrid.QTY).Text) Then

                SessVar.discReCalc.needToReCalc = True
                'Prepares data for later use in discount recalculation (if applicable)
                SetupForDiscReCalc()  ' CheckWarrClicked for adding a warranty to a line and is new warranty line

                If SalesUtils.hasWarranty(dgItem.Cells(SoLnGrid.ITM_CD).Text) Then
                    ShowWarrantyPopup(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text)
                End If
            Else
                lbl_desc.Text = "Quantity must equal 1 when a warranty is attached to a SKU."
                chkWarr.Checked = False
            End If
        Else  ' remove warranty link
            '*******************************************************
            'TODO:AXK revisit this to use common methods like updateDataRowLinkInfo
            Dim currWarrItm As String = dgItem.Cells(SoLnGrid.WARR_BY_SKU).Text
            Dim linkedLines As New ArrayList()
            Dim currWarrLnNum As Integer = 0

            ' can have a warranty SKU without a Warranty line# (if doc was split or the warranty added later, just means warrantable not on this doc)
            If dgItem.Cells(SoLnGrid.WARR_BY_LN).Text.isNotEmpty AndAlso dgItem.Cells(SoLnGrid.WARR_BY_LN).Text <> "&nbsp;" Then

                currWarrLnNum = CInt(dgItem.Cells(SoLnGrid.WARR_BY_LN).Text)

                'see how many warrantables are attached to this warranty line num
                linkedLines = GetLinkedLineNumbers(dgItem.Cells(SoLnGrid.WARR_BY_LN).Text)

                'update the datagrid cells- unlink/clear the values
                dgItem.Cells(SoLnGrid.WARR_BY_LN).Text = ""
                dgItem.Cells(SoLnGrid.WARR_BY_SKU).Text = ""
                dgItem.Cells(SoLnGrid.WARR_BY_DOC_NUM).Text = ""

            End If

            'this list includes the warrantable line being unchecked, that is why check if count>1
            Dim hasOtherLinks As Boolean = (linkedLines.Count > 1)

            'find the warrantied row in the dataset and remove the warranty link from it;
            UpdateDataRowLinkInfo(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text)

            If (Not hasOtherLinks) AndAlso currWarrLnNum > 0 Then

                ' now iterate to find the warranty row to void/remove it
                For Each soLnRow In soLnDatTbl.Rows
                    If soLnRow("DEL_DOC_LN#") = currWarrLnNum Then
                        ' delete row if not existing and void if existing  
                        ' TODO DSA - see how this works on existing vs new
                        If Not theSalesBiz.CheckWarrLink(soLnRow("DEL_DOC_NUM"), False) Then
                            soLnRow("VOID_FLAG") = "Y"
                        End If
                        ReapplyDiscounts(soLnDataSet.Tables(0), ReapplyDiscountRequestDtc.LnChng.Void, String.Empty)
                        Exit For 'no need to keep on iterating, updated the correct row already
                    End If
                Next
            End If

        End If
        Session("DEL_DOC_LN#") = soLnDataSet
        bindgrid()
        doAfterBindgrid()
        calculate_total()
    End Sub

    ''' <summary>
    ''' validating the split order
    ''' </summary>
    ''' <param name="dgItem"></param>
    ''' <remarks></remarks>
    Protected Sub ValidateSplitOrder(ByRef dgItem As DataGridItem)
        Dim errorMsg As String = ""
        Dim checkForWarrantyLinks As Boolean = False

        If dgItem.Cells(SoLnGrid.ITM_TP_CD).Text.Equals("WAR") Then
            errorMsg = theSalesBiz.ValidateWarrReference(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text, txt_del_doc_num.Text, True)
        Else
            If Not IsNothing(dgItem.Cells(SoLnGrid.WARR_BY_LN).Text) AndAlso (Not dgItem.Cells(SoLnGrid.WARR_BY_LN).Text.isEmpty) Then
                errorMsg = theSalesBiz.ValidateWarrReference(dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text, txt_del_doc_num.Text, False)
            End If
        End If

        If Not errorMsg.isEmpty Then
            Dim msgPopup As DevExpress.Web.ASPxPopupControl.ASPxPopupControl = ucMsgPopup.FindControl("popupmsg")
            ucMsgPopup.DisplayConfirmMsg(errorMsg, "", Resources.POSMessages.MSG0049)
            Dim msgComponent As DevExpress.Web.ASPxEditors.ASPxMemo = CType(msgPopup.FindControl("txtmsg"), DevExpress.Web.ASPxEditors.ASPxMemo)
            msgComponent.ForeColor = System.Drawing.ColorTranslator.FromHtml("#9F6000")
            msgPopup.ShowOnPageLoad = True
        ElseIf Not IsNothing(ViewState("VoidRow")) AndAlso Not String.IsNullOrEmpty(ViewState("VoidRow")) Then
            VoidLine(dgItem)
        ElseIf Not IsNothing(ViewState("WarrRow")) AndAlso Not String.IsNullOrEmpty(ViewState("WarrRow")) Then
            RemoveWarranty(dgItem)
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <param name="choice"></param>
    ''' <remarks></remarks>
    Protected Sub ucMsgPopup_Choice(sender As Object, e As EventArgs, choice As Usercontrols_MessagePopup.MessageResponse) Handles ucMsgPopup.Choice
        Dim dgItem As DataGridItem = Nothing
        Dim chkdata As CheckBox
        If ucMsgPopup.MessageResponse.YES_OK_SELECTION = choice Then
            If Not IsNothing(ViewState("VoidRow")) AndAlso Not String.IsNullOrEmpty(ViewState("VoidRow")) Then
                dgItem = GridView1.Items(ViewState("VoidRow"))
                VoidLine(dgItem)
            ElseIf Not IsNothing(ViewState("WarrRow")) AndAlso Not String.IsNullOrEmpty(ViewState("WarrRow")) Then
                dgItem = GridView1.Items(ViewState("WarrRow"))
                RemoveWarranty(dgItem)
            ElseIf Not IsNothing(ViewState("SaleVoid")) AndAlso Not String.IsNullOrEmpty(ViewState("SaleVoid")) Then
                StatChangeToVoid()
            End If
        Else
            If Not IsNothing(ViewState("VoidRow")) AndAlso Not String.IsNullOrEmpty(ViewState("VoidRow")) Then
                dgItem = GridView1.Items(ViewState("VoidRow"))
                chkdata = dgItem.FindControl("ChkBoxVoid")
                chkdata.Checked = False
                ViewState("VoidRow") = Nothing
            ElseIf Not IsNothing(ViewState("WarrRow")) AndAlso Not String.IsNullOrEmpty(ViewState("WarrRow")) Then
                dgItem = GridView1.Items(ViewState("WarrRow"))
                chkdata = dgItem.FindControl("ChkBoxWarr")
                chkdata.Checked = True
                ViewState("WarrRow") = Nothing
            ElseIf Not IsNothing(ViewState("SaleVoid")) AndAlso Not String.IsNullOrEmpty(ViewState("SaleVoid")) Then
                cbo_stat_cd.SelectedValue = "C"
                ViewState("SaleVoid") = Nothing
            End If
        End If
    End Sub

    ''' <summary>
    ''' Method to get which button caused the postback
    ''' </summary>
    ''' <returns>name of the control</returns>
    ''' <remarks></remarks>
    Private Function GetPostBackControlName() As String
        Dim control As Control = Nothing
        Dim result As String = String.Empty

        'first we will check the "__EVENTTARGET" because if post back made by the controls 
        'which used "_doPostBack" function also available in Request.Form collection. 

        Dim ctrlname As String = Page.Request.Params("__EVENTTARGET")
        If ctrlname IsNot Nothing AndAlso ctrlname <> [String].Empty Then
            control = Page.FindControl(ctrlname)
        Else
            ' if __EVENTTARGET is null, the control is a button type and we need to 
            ' iterate over the form collection to find it

            Dim ctrlStr As String = [String].Empty
            Dim c As Control = Nothing
            For Each ctl As String In Page.Request.Form

                If ctl.EndsWith(".x") OrElse ctl.EndsWith(".y") Then
                    ctrlStr = ctl.Substring(0, ctl.Length - 2)
                    c = Page.FindControl(ctrlStr)
                Else
                    c = Page.FindControl(ctl)
                End If

                If TypeOf c Is DevExpress.Web.ASPxEditors.ASPxButton Then 'OrElse TypeOf c Is System.Web.UI.WebControls.ImageButton Then
                    control = c
                    Exit For
                End If
            Next
        End If
        If Not control Is Nothing Then
            result = control.ID
        End If
        Return result
    End Function

    Private Sub ModifyControls(ByVal provideAccess As Integer)
        If (provideAccess = 0) Then
            ' Do the steps when the doc is on Write_Mode
            ' This session has been created to utilise for MM-1594. This session variable is required when user changes the tax
            ' information on the document then it will open the tax_update page from there it will be redirected to SOM+ or SORW+
            ' based on the Del_Doc_Num page will be opened
            Session("som_del_doc_num") = txt_del_doc_num.Text.Trim()

            'Need to prepare a list of Documents to be deleted on logout            
            Dim usersInfoList As List(Of AccessControlDtc) = New List(Of AccessControlDtc)

            If Not (Session("deleteBrowserID")) Is Nothing Then
                usersInfoList = TryCast(Session("deleteBrowserID"), List(Of AccessControlDtc))
                Dim accessInfo As AccessControlDtc
                accessInfo = usersInfoList.Find(Function(x) (x.BrowserId = Convert.ToString(Session("browserID")))) ' AndAlso x.EmployeeInitials = Convert.ToString(Session("EMP_INIT")))) ' AndAlso x.ScreenName = Convert.ToString(thisProgramName)))
                If (accessInfo) Is Nothing Then
                    usersInfoList.Add(New AccessControlDtc(Convert.ToString(Session("EMP_INIT")), thisProgramName, Convert.ToString(Session("browserID"))))
                    Session("deleteBrowserID") = usersInfoList
                End If
            Else
                usersInfoList.Add(New AccessControlDtc(Convert.ToString(Session("EMP_INIT")), thisProgramName, Convert.ToString(Session("browserID"))))
                Session("deleteBrowserID") = usersInfoList
            End If

        Else
            '' To disable the  bottom part of the page            
            Dim submitFieldControls2 = Master.FindControl("ctl00$arpMain$ContentPlaceHolder2").Controls
            accessInfo.DisableFields(submitFieldControls2)

            'display the Message            
            lbl_desc.Text = String.Format(Resources.POSMessages.MSG0054, Convert.ToString(ViewState("LockedBy")))

            ' Need to Enable the Del_Doc_Num and Clear button so that you can requery the new document
            txt_del_doc_num.Visible = True
            txt_del_doc_num.Enabled = False
            btn_Clear.Visible = True
            btn_Clear.Enabled = True
            cbo_stat_cd.Enabled = False
            lnkModifyTax.Enabled = False
            ddlConfStaCd.Enabled = False
            'Disable all the tab controls
            pnlOrderHeader.Enabled = False
            pnlItemDetails.Enabled = False
            pnlDiscounts.Enabled = False
            pnlComments.Enabled = False
            pnlPickDel.Enabled = False
            pnlMiscellaneous.Enabled = False

            'Disable all butttons
            btn_Lookup.Enabled = False
            btn_Save.Enabled = False
            btn_payments.Enabled = False
            btn_invoice.Enabled = False
            btnPrintPickReport.Enabled = False
            btnSORW.Enabled = False
            btn_addendum.Enabled = False
            btn_return.Enabled = False
            btnConfigureEmail.Enabled = False
            chk_email.Enabled = False
            'Daniela reprint
            btn_reprint.Enabled = False
        End If
    End Sub

    Private Sub DeleteRecordonButtonClick()
        ' If the user has write access mode then as soon as user queries the SAL# it will be added to the dictionary 
        ' so need to delete this record on clicking the clear button, if user has read-only access then no need to perform the below check
        If Not (txt_Stat_cd.Text.Equals("F") Or txt_Stat_cd.Text.Equals("V")) Then
            If (Convert.ToInt32(Session("provideAccess")) = 0) Then
                ' Deleting the record from the table to clear the SAL entry
                If Not (String.IsNullOrEmpty(Convert.ToString(Session("browserID")))) Then
                    'Deletes the record from the database                    
                    Dim recordCount As Integer = accessInfo.DeleteRequest(Convert.ToString(Session("som_del_doc_num")), Convert.ToString(Session("browserID")))
                    If (recordCount >= 1) Then
                        Dim usersInfoList As New List(Of AccessControlDtc)
                        usersInfoList = TryCast(HttpContext.Current.Session("deleteBrowserID"), List(Of AccessControlDtc))
                        If Not (usersInfoList) Is Nothing Then
                            Dim accessInfo As AccessControlDtc
                            accessInfo = usersInfoList.Find(Function(x) (x.BrowserId = Convert.ToString(Session("browserID"))))
                            If Not (accessInfo) Is Nothing Then
                                usersInfoList.RemoveAll(Function(a) a.BrowserId = Convert.ToString(accessInfo.BrowserId))
                                HttpContext.Current.Session("deleteBrowserID") = usersInfoList
                            Else
                                Exit Sub
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Line level manager approval popup display.
    ''' </summary>
    ''' <param name="dgItem"></param>
    ''' <remarks></remarks>
    Private Sub MgrByLineApprovalPopup(ByRef dgItem As DataGridItem, ByVal editedRetPrc As String)
        'means that Mgr Override is by Line OR that the GM is below the configured margin, so approval is needed
        Session("SelectedRow") = dgItem.ItemIndex & ":" & dgItem.Cells(SoLnGrid.DEL_DOC_LN_NUM).Text

        Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = ucMgrOverride.FindControl("txtMgrPwd")

        If txtMgrPwd IsNot Nothing Then
            txtMgrPwd.Focus()
        End If
        popupMgrOverride.ShowOnPageLoad = True
        Session("SHOWPOPUP") = True
        Session("EditedRetPrc") = editedRetPrc
    End Sub

    ''' <summary>
    '''  Remove the rowid from the hidden field.
    ''' </summary>
    ''' <param name="rowId"></param>
    ''' <remarks></remarks>
    Public Sub RemoveUpdLn(ByVal rowId As String)
        Dim seperator As Char = ";"c
        Dim sArr() As String = hidUpdLnNos.Value.ToString().Split(seperator)
        If sArr.Contains(rowId) Then
            Dim index As Integer = hidUpdLnNos.Value.LastIndexOf(seperator)
            hidUpdLnNos.Value = hidUpdLnNos.Value.Substring(0, index)
        End If
    End Sub
    ''' <summary>
    ''' Enable the Comment section/tab controls when sale is finilized or void
    ''' </summary>
    ''' <param name="isEnable"></param>
    ''' <remarks></remarks>
    Public Sub SetCommntSection(ByVal isEnable As Boolean)
        If txt_Stat_cd.Text = AppConstants.Order.STATUS_VOID Or txt_Stat_cd.Text = AppConstants.Order.STATUS_FINAL Then
            btn_Save.Enabled = isEnable
            txt_comments.Enabled = isEnable
            txt_del_comments.Enabled = isEnable
            txt_ar_comments.Enabled = isEnable
            btn_Save.Text = Resources.POSMessages.MSG0056
        End If
    End Sub
    Protected Sub cboTargetDatechanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rowsFound As Boolean = True
        Dim DeliveryDates As DataSet = Session("DeliveryDates")
        cboTargetDate.Visible = True
        lblFilterDate.Visible = True
        'Bind all the data by default
        grid_del_date.DataSource = DeliveryDates
        'Enable navigation button by default
        btnFirst.Enabled = False
        btnPrev.Enabled = False
        btnNext.Enabled = True
        btnLast.Enabled = True
        If cboTargetDate.Value & "" = String.Empty Then
            'Binds all tthe data 

        Else
            'filter out the rows and bind only the matching data
            Dim dt As DataTable = DeliveryDates.Tables(0)
            Dim qry = (From row In dt.AsEnumerable() Where row.Field(Of Date?)("DEL_DT") = Convert.ToDateTime(cboTargetDate.Value.ToString()) Select row).Distinct()
            'disable the prev and first buttons by defult
            btnFirst.Enabled = False
            btnPrev.Enabled = False
            ASPlblError.Text = String.Empty
            'if any rows found for the selected date
            If qry.Any() Then
                Dim filteredDates As DataTable = qry.CopyToDataTable()
                If filteredDates.Rows.Count > 1 Then
                    For lc As Integer = 1 To filteredDates.Rows.Count - 1
                        filteredDates.Rows.RemoveAt(lc)
                    Next
                    filteredDates.AcceptChanges()
                End If
                grid_del_date.DataSource = filteredDates
                btnFirst.Enabled = False
                btnNext.Enabled = False
                btnPrev.Enabled = False
                btnLast.Enabled = False
            Else
                rowsFound = False
                btnFirst.Enabled = False
                btnNext.Enabled = False
                If DeliveryDates.Tables(0).Rows.Count > grid_del_date.PageSize Then
                    btnNext.Enabled = True
                    btnLast.Enabled = True
                Else
                    btnNext.Enabled = False
                    btnLast.Enabled = False
                End If
            End If
        End If
        grid_del_date.CurrentPageIndex = 0
        grid_del_date.DataBind()
        DisableDeliveryDates()
        If rowsFound Then
            'Do Nothing
        Else
            ASPlblError.Text = "No delivery dates found for the selected date - " & cboTargetDate.Value
            cboTargetDate.Value = String.Empty
        End If
    End Sub
    Private Sub setCalanderMinAndMaxDays(ByVal startDate As Date, ByVal endDate As Date)
        cboTargetDate.MinDate = startDate
        If endDate = Today Then
            'Do nothing, dont set the end Date

        Else
            cboTargetDate.MaxDate = endDate
        End If
    End Sub

    Protected Sub ClearCache()
        ''mm -sep 16,2016 - backbutton concern
        Session.Abandon()
        Dim nextpage As String = "Logout.aspx"
        Response.Write("<script language=javascript>")

        Response.Write("{")
        Response.Write(" var Backlen=history.length;")

        Response.Write(" history.go(-Backlen);")
        Response.Write(" window.location.href='" + nextpage + "'; ")

        Response.Write("}")
        Response.Write("</script>")
    End Sub

    Protected Sub lucy_update_table_not_fi(ByRef cmd As OracleCommand)
        'Daniela add parameter cmd


        ' Dim strfinCo = cbo_finance_company.SelectedValue
        Dim str_app_cd As String

        Try
            'UPDATE ONLY THE AMT IN SO table
            Dim sql As String = "UPDATE SO SET approval_cd =:approval_cd WHERE DEL_DOC_NUM = :DEL_DOC_NUM"
            cmd.CommandText = sql
            ' cmd.Parameters.Add(":AMT", OracleType.Double)
            'cmd.Parameters(":AMT").Value = CDbl(txt_fi_amt.Text)
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
            ' cmd.Parameters.Add(":strfinco", OracleType.VarChar)
            ' cmd.Parameters(":strfinco").Value = strfinCo
            If IsDBNull(txt_fi_appr.Text) = False And isEmpty(txt_fi_appr.Text) = False Then
                str_app_cd = txt_fi_appr.Text
            Else
                str_app_cd = ""
            End If
            cmd.Parameters.Add(":approval_cd", OracleType.VarChar)
            cmd.Parameters(":approval_cd").Value = str_app_cd
            cmd.ExecuteNonQuery()

            ' Add comment if finance popup information has been changed
            addFinancePopupComments(cmd)

        Catch ex As Exception
            Throw
        Finally
            cmd.Cancel()
            cmd.Dispose()
        End Try
        'lucy_insert_to_ar_trn() ' should be only one SAL for the same IVC_CD

        PopupModifyFinance.ShowOnPageLoad = False

    End Sub
    Public Function lucy_ctl_final(ByRef p_del_doc_num As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_err As String = ""
        Dim p_id As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_ctl_final.pos_main"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_del_doc_num", OracleType.VarChar)).Value = p_del_doc_num
        myCMD.Parameters.Add("p_err", OracleType.VarChar, 200).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()


            If IsDBNull(myCMD.Parameters("p_err").Value) = False Then   ' allow final if str_err=Y
                str_err = myCMD.Parameters("p_err").Value
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return str_err
    End Function

    Public Function lucy_ctl_void(ByRef p_del_doc_num As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_err As String = ""
        Dim p_id As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_ctl_change.pos_main"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_del_doc_num", OracleType.VarChar)).Value = p_del_doc_num
        myCMD.Parameters.Add("p_err", OracleType.VarChar, 200).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()


            If IsDBNull(myCMD.Parameters("p_err").Value) = False Then   ' allow final if str_err=Y
                str_err = myCMD.Parameters("p_err").Value
            End If

            If str_err <> "Y" Then
                str_err = "Void not allowed. Lines have been picked or checked. The user will need to unpick the affected lines for the above changes to be allowed."

                btn_Save.Enabled = False
            Else
                btn_Save.Enabled = True
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return str_err
    End Function
    Protected Sub lucy_insert_so_comment()
        'lucy 28-jun-16

        Dim sql As String



        Dim str_del_doc_num As String = txt_del_doc_num.Text


        ' Dim orig_store_cd As String = Right(left_del, 2)
        Dim emp_init As String = txt_slsp1.Text
        Dim emp_cd As String = Session("emp_cd")
        Dim cmd As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Exception
        Dim conn As OracleConnection
        Dim str_insert As String
        Dim str_trk As String
        Dim str_stop As String



        '  Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim lngSEQNUM As Integer, lngComments As Integer, strSO_SEQ_NUM As String
        Dim strComments As String
        strSO_SEQ_NUM = ""

        lngSEQNUM = 0
        Dim so_store_cd As String = txt_store_cd.Text  ' store code may or may not be in the del_doc_num as of 12.0
        Dim sqlQueryBuilder As StringBuilder = New StringBuilder
        '  cmd.CommandType = CommandType.Text
        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        sql = "select trk_num,stop# from trk_stop where del_doc_num='" & str_del_doc_num & "'"
        Try

            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.ExecuteNonQuery()

            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)


            If MyDataReader.Read() Then
                str_stop = MyDataReader.Item("stop#").ToString
                str_trk = MyDataReader.Item("trk_num").ToString


                Dim cmntKey = New OrderUtils.WrDtStoreSeqKey

                Dim str_wr_dt As String = txt_wr_dt.Text

                cmntKey.wrDt = str_wr_dt
                cmntKey.storeCd = so_store_cd



                cmntKey.seqNum = Right(str_del_doc_num, 4)
                lngSEQNUM = OrderUtils.getNextSoCmntSeq(cmntKey, cmd)
                strSO_SEQ_NUM = cmntKey.seqNum



                strComments = "TRUCK/STOP " + str_trk + "," + str_stop + " DELETED BY " + emp_cd + "."


                Dim str_commentType As String = " "

                LeonsBiz.leons_insert_cmnt(str_wr_dt, so_store_cd, strSO_SEQ_NUM, lngSEQNUM, strComments, str_del_doc_num, str_commentType)



            End If

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
    End Sub

    Public Function lucy_ctl_change(ByRef p_del_doc_num As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_err As String = ""
        Dim p_id As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_ctl_change.pos_main"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_del_doc_num", OracleType.VarChar)).Value = p_del_doc_num
        myCMD.Parameters.Add("p_err", OracleType.VarChar, 200).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        If Session("lucy_ctl_change") <> "Y" And Session("add_new_line") = "Y" Then
            Session("lucy_ctl_change") = "N"
        End If

        Try

            myCMD.ExecuteNonQuery()


            If IsDBNull(myCMD.Parameters("p_err").Value) = False Then   ' allow final if str_err=Y
                str_err = myCMD.Parameters("p_err").Value
            End If

            If str_err <> "Y" Then
                Session("lucy_ctl_change") = "N"
                str_err = "Change not allowed. Lines have been picked or checked. The user will need to unpick the affected lines for the above changes to be allowed."
                btn_Save.Enabled = False
            Else
                btn_Save.Enabled = True
                If Session("lucy_ctl_change") <> "Y" And Session("add_new_line") = "Y" Then
                    Session("lucy_ctl_change") = "N"
                Else
                    Session("lucy_ctl_change") = "Y"
                End If

            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return str_err
    End Function
    Protected Sub lucy_update_table_not_fi()
        'lucy


        ' Dim strfinCo = cbo_finance_company.SelectedValue
        Dim str_app_cd As String

        Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Try
            'UPDATE ONLY THE AMT IN SO table
            conn.Open()
            Dim sql As String = "UPDATE SO SET approval_cd =:approval_cd WHERE DEL_DOC_NUM = :DEL_DOC_NUM"
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            ' cmd.Parameters.Add(":AMT", OracleType.Double)
            'cmd.Parameters(":AMT").Value = CDbl(txt_fi_amt.Text)
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
            ' cmd.Parameters.Add(":strfinco", OracleType.VarChar)
            ' cmd.Parameters(":strfinco").Value = strfinCo
            If IsDBNull(txt_fi_appr.Text) = False And isEmpty(txt_fi_appr.Text) = False Then
                str_app_cd = txt_fi_appr.Text
            Else
                str_app_cd = ""
            End If
            cmd.Parameters.Add(":approval_cd", OracleType.VarChar)
            cmd.Parameters(":approval_cd").Value = str_app_cd
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
        'lucy_insert_to_ar_trn() ' should be only one SAL for the same IVC_CD

        PopupModifyFinance.ShowOnPageLoad = False

    End Sub


    Protected Sub lucy_update_table(ByRef cmd As OracleCommand)
        'lucy

        Dim strfinCo = cbo_finance_company.SelectedValue
        Dim str_app_cd As String

        Try
            'UPDATE ONLY THE AMT IN SO table

            Dim sql As String = "UPDATE SO SET ORIG_FI_AMT= :AMT,fin_cust_cd= :strfinCo ,approval_cd =:approval_cd WHERE DEL_DOC_NUM = :DEL_DOC_NUM"

            cmd.CommandText = sql

            cmd.Parameters.Add(":AMT", OracleType.Double)
            cmd.Parameters(":AMT").Value = CDbl(txt_fi_amt.Text)
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
            cmd.Parameters.Add(":strfinco", OracleType.VarChar)
            cmd.Parameters(":strfinco").Value = strfinCo
            If IsDBNull(txt_fi_appr.Text) = False And isEmpty(txt_fi_appr.Text) = False Then
                str_app_cd = txt_fi_appr.Text
            Else
                str_app_cd = ""
            End If
            cmd.Parameters.Add(":approval_cd", OracleType.VarChar)
            cmd.Parameters(":approval_cd").Value = str_app_cd
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally

        End Try
        ' lucy_insert_to_ar_trn() ' should be only one SAL for the same IVC_CD
        If Session("from_btn_rmv_fi") <> "Y" Then   'only uodate so when removing fi transaction
            lucy_update_ar_trn()
            lucy_insert_to_so_asp()
        End If
        UpdateAmountsAfterModifyFinance()

        ' Add comment if finance popup information has been changed
        ' Before "Session("ORIG_FI_INFO")" set to "Nothing"
        addFinancePopupComments(cmd)

        Session("ORIG_FI_INFO") = Nothing
        PopupModifyFinance.ShowOnPageLoad = False

    End Sub



    Protected Sub lucy_update_table()
        'lucy

        Dim strfinCo = cbo_finance_company.SelectedValue
        Dim str_app_cd As String

        Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Try
            'UPDATE ONLY THE AMT IN SO table
            conn.Open()
            Dim sql As String = "UPDATE SO SET ORIG_FI_AMT= :AMT,fin_cust_cd= :strfinCo ,approval_cd =:approval_cd WHERE DEL_DOC_NUM = :DEL_DOC_NUM"
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.Parameters.Add(":AMT", OracleType.Double)
            cmd.Parameters(":AMT").Value = CDbl(txt_fi_amt.Text)
            cmd.Parameters.Add(":DEL_DOC_NUM", OracleType.VarChar)
            cmd.Parameters(":DEL_DOC_NUM").Value = txt_del_doc_num.Text
            cmd.Parameters.Add(":strfinco", OracleType.VarChar)
            cmd.Parameters(":strfinco").Value = strfinCo
            If IsDBNull(txt_fi_appr.Text) = False And isEmpty(txt_fi_appr.Text) = False Then
                str_app_cd = txt_fi_appr.Text
            Else
                str_app_cd = ""
            End If
            cmd.Parameters.Add(":approval_cd", OracleType.VarChar)
            cmd.Parameters(":approval_cd").Value = str_app_cd
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
        ' lucy_insert_to_ar_trn() ' should be only one SAL for the same IVC_CD
        If Session("from_btn_rmv_fi") <> "Y" Then   'only uodate so when removing fi transaction
            lucy_update_ar_trn()
            lucy_insert_to_so_asp()
        End If
        UpdateAmountsAfterModifyFinance()
        Session("ORIG_FI_INFO") = Nothing
        PopupModifyFinance.ShowOnPageLoad = False

    End Sub

    Protected Sub lucy_update_ar_trn()
        'lucy
        Dim sql, sql2 As String
        Dim order_tp As String = "SAL"
        ' Dim dbl_amt As Double = CDbl(txt_fi_amt.Text)
        Dim dbl_amt As Double = Replace(CDbl(lblTotal.Text), ",", "")
        Dim del_doc_num As String = txt_del_doc_num.Text
        Dim bnk_crd_num As String = "FI"
        Dim Auth_No As String = txt_fi_appr.Text
        Dim left_del As String = Left(txt_del_doc_num.Text, 7)
        Dim orig_store_cd As String = Right(left_del, 2)
        Dim emp_init As String = LeonsBiz.GetEmpInit(Session("emp_cd"))
        Dim emp_cd As String = Session("emp_cd")

        ' should not update sal amt

        'sql = "update AR_TRN set mop_cd='FI', amt=" & dbl_amt & ",post_dt=TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'),pmt_store='" & Session("home_store_cd") & "',EMP_CD_CSHR='" & emp_init & "' "
        sql = "update AR_TRN set mop_cd='FI',  post_dt=TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR'),pmt_store='" & Session("home_store_cd") & "',EMP_CD_CSHR='" & emp_init & "' "

        If isEmpty(Session("lucy_bnk_card")) = False And IsDBNull(Session("lucy_bnk_card")) = False Then
            'lucy_get_detail()
            sql = sql & ", BNK_CRD_NUM='" & Session("lucy_bnk_card") & "'"
        End If
        ' Daniela Apr 23
        If IsDBNull(Session("lucy_exp_dt")) = False And IsNumeric(Session("lucy_exp_dt")) = True Then
            'lucy_get_detail()
            If Len(Session("lucy_exp_dt")) = 4 Then ' from front end
                'sql = sql & ", EXP_DT=TO_DATE('" & Session("lucy_exp_dt") & "','mm/dd/RRRR') "
                sql = sql & ", EXP_DT=TO_DATE('" & Session("lucy_exp_dt") & "','MMYY') "
            Else
                sql = sql & ", EXP_DT=TO_DATE('" & Session("lucy_exp_dt") & "','mm/dd/RRRR') "
            End If
        End If

        ' If Card_Type & "" <> "" Then
        'sql = sql & ", REF_NUM"
        ' sql2 = sql2 & ", '" & Card_Type & "' "
        ' End If
        ' If dbReader.Item("CHK_NUM").ToString() & "" <> "" Then
        'sql = sql & ", CHK_NUM"
        ' sql2 = sql2 & ", '" & dbReader.Item("CHK_NUM").ToString() & "' "
        ' End If
        '  Dim expDt As String = Session("exp_dt")
        ' If IsDate(expDt) Then
        'sql = sql & ", EXP_DT=TO_DATE('" & expDt & "','mm/dd/RRRR')"

        ' End If



        If Auth_No & "" <> "" Then
            sql = sql & ", APP_CD='" & Auth_No & "'"

        End If
        sql = sql & " where ivc_cd='" & del_doc_num & "' and trn_tp_cd='SAL'"
        sql = UCase(sql)

        'If ref_num & "" <> "" Then
        'sql = sql & ", HOST_REF_NUM"
        ' sql2 = sql2 & ",'" & ref_num & "'"
        ' End If
        ' If Trans_Id & "" <> "" Then
        'sql = sql & ", ACCT_NUM"
        'Overwrite the transId(which is the unique ID returned by PB) with the acct# for a 
        'FI 'DP' type of auth as a workaround to be used for the Leons Refund to FI DP spec.
        ' If dbReader.Item("MOP_TP").ToString = "FI" AndAlso isFinanceDP Then
        'Trans_Id = "ID:" & Decrypt(dbReader.Item("FI_ACCT_NUM").ToString, "CrOcOdIlE")
        ' End If
        ' sql2 = sql2 & ",'" & Trans_Id & "'"
        ' End If

        '  sql = sql & ")"
        ' sql2 = sql2 & ")"


        Dim conn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Try
            'UPDATE ONLY THE AMT IN SO table
            conn.Open()
            Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
    End Sub



    Protected Sub lucy_insert_to_so_asp()
        '  Dim dbAtomicConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        ' Dim dbAtomicCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbAtomicConnection)

        '  Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        '   Dim dbCommand As OracleCommand

        Dim sql, sql2 As String

        ' Dim dbl_amt As Double = CDbl(txt_fi_amt.Text)
        Dim dbl_amt As Double = Replace(CDbl(lblTotal.Text), ",", "")
        Dim del_doc_num As String = txt_del_doc_num.Text
        Dim bnk_crd_num As String = "FI"
        Dim Auth_No As String = txt_fi_appr.Text
        Dim left_del As String = Left(txt_del_doc_num.Text, 7)
        Dim orig_store_cd As String = Right(left_del, 2)
        Dim emp_init As String = txt_slsp1.Text
        Dim emp_cd As String = Session("emp_cd")
        Dim cmd As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Exception
        Dim conn As OracleConnection
        Dim str_insert As String

        conn = GetConn(SystemUtils.Connection_Constants.CONN_ERP)

        sql = "select del_doc_num from so_asp where del_doc_num='" & del_doc_num & "'"
        Try

            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            cmd.ExecuteNonQuery()

            MyDataReader = DisposablesManager.BuildOracleDataReader(cmd)


            If MyDataReader.Read() Then
                str_insert = "N"
            Else
                str_insert = "Y"
            End If


            If str_insert = "Y" Then
                sql = "INSERT INTO so_asp (del_doc_num,EMP_SLSP_CD"
                sql2 = " VALUES('" & del_doc_num & "','" & emp_init & "'"


                sql = UCase(sql)
                sql2 = UCase(sql2)

                sql = sql & ")"
                sql2 = sql2 & ")"
                sql = sql & sql2


                ' Try
                'UPDATE ONLY THE AMT IN SO table
                'conn.Open()
                ' Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                cmd.ExecuteNonQuery()
            End If
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
    End Sub

    Protected Sub btn_lucy_save_Click(sender As Object, e As EventArgs) Handles btn_lucy_save.Click
        ' Daniela save card num and exp date
        If isNotEmpty(txt_fi_acct.Text) And txt_fi_acct.Text.Length = 16 And InStr(txt_fi_acct.Text, "*") = 0 Then
            Session("lucy_bnk_card") = txt_fi_acct.Text
        End If
        If isNotEmpty(txt_exp_dt.Text) And txt_exp_dt.Text.Length = 4 Then
            Session("lucy_exp_dt") = txt_exp_dt.Text
        End If
        'lucy copied from btn_save_fi_co_Click
        Dim str_has_orig_fi As String = Session("has_orig_fi")
        Dim str_skip_data_sec As String = Session("skip_data_security")
        Dim strfinCo = cbo_finance_company.SelectedValue
        Session("lucy_as_cd") = strfinCo

        Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = ucMgrOverride.FindControl("txtMgrPwd")
        If txtMgrPwd IsNot Nothing Then
            txtMgrPwd.Focus()
        End If
        ' If Session("has_orig_fi") = "Y" And Session("skip_data_security") = False Then 'lucy comment   on 20-jan-15 as per Dave
        'Daniela Start
        Dim dbConn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
        Dim proceed As Boolean = False
        Dim soRowid As String = String.Empty

        Try
            ' At this point, unless auth occurs and fails, SO will be updated so let us lock it here
            dbConn.Open()
            dbCmd.Transaction = dbConn.BeginTransaction()

            soRowid = SalesUtils.LockSoRec(txt_del_doc_num.Text, dbCmd)
            If Session("skip_data_security") = False Then
                popupMgrOverride.ShowOnPageLoad = True
                lblErrorMsg.Text = "In progress..."
                Session("from_btn_save") = "Y"
                Session("from_btn_rmv_fi") = "N"
                Session("swipe_insert") = "N"
                'ElseIf Session("has_orig_fi") = "Y" And cbo_finance_company.SelectedIndex > 0 And CDbl(txt_fi_amt.Text) > 0 Then
            ElseIf cbo_finance_company.SelectedIndex > 0 And CDbl(txt_fi_amt.Text) > 0 Then ' Daniela May 04 as per Donna update needed if no Session("has_orig_fi")
                lucy_update_table(dbCmd)  'this needs to be commentted when need to have popup., it works, 02-oct-14, the function is added to ucMgrOverride_wasApproved
            Else
                lucy_update_table_not_fi(dbCmd)  'not a finance sales as per dave 20-Jan-15
            End If
            dbCmd.Transaction.Commit()

        Catch ex As Exception
            dbCmd.Transaction.Rollback()
            If ex.Message = String.Format(Resources.POSErrors.ERR0042, txt_del_doc_num.Text) Then
                ModifyControls(1) ' disable controls other than Clear button!
                btn_save_fi_co.Enabled = False
            End If
            lblErrorMsg.Text = ex.Message
            Exit Sub

        Finally
            If Not IsNothing(dbCmd.Transaction) Then
                dbCmd.Transaction.Dispose()
            End If
            dbCmd.Dispose()
            dbConn.Close()
            dbConn.Dispose()
        End Try
        'Daniela end        

        ' Daniela reset session
        Session("lucy_bnk_card") = Nothing
        Session("lucy_exp_dt") = Nothing
        Session("lucy_as_cd") = Nothing

    End Sub

    Protected Sub btn_lucy_rmv_fi_Click(sender As Object, e As EventArgs) Handles btn_lucy_rmv_fi.Click
        'lucy copied from btn_save_fi_co_Click
        'Daniela Sept 22, 2015
        Dim dbConn As OracleConnection = GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand("", dbConn)
        Dim proceed As Boolean = False
        Dim soRowid As String = String.Empty
        If Not IsDBNull(txt_fi_appr.Text) And
            (Len(txt_fi_appr.Text) = 6 Or Len(txt_fi_appr.Text) = 7) Then 'sabrina add chk for 7

            If Session("has_orig_fi") = "Y" And Session("skip_data_security") = False Then

                Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = ucMgrOverride.FindControl("txtMgrPwd")
                If txtMgrPwd IsNot Nothing Then
                    txtMgrPwd.Focus()
                End If

                popupMgrOverride.ShowOnPageLoad = True   'this needs to be uncomment when need to have popup., it works , 02-oct-14
                lblErrorMsg.Text = "In progress..."

                Session("from_btn_rmv_fi") = "Y"
                Session("from_btn_save") = "N"
                Session("swipe_insert") = "N"
            Else
                Dim str_display As String
                If isNotEmpty(txt_fi_acct.Text) Then ' Daniela do not send out request without bank card because it will fail
                    str_display = send_out_void_fi_request()

                    If Left(str_display, 7) <> "APPROVE" Then 'sabrina add
                        'sabrina comment out If str_display = "no designated pinpad " Then
                        'sabrina comment out lblErrorMsg.Text = "no designated pinpad "
                        ' txt_fi_acct.Text = ""
                        lblErrorMsg.Text = str_display 'sabrina add
                        Exit Sub
                    End If
                End If
                'If Left(str_display, 5) <> "empty" Then
                txt_fi_appr.Text = ""  'sabrina add (commented out in the send_out_void_fi_request)
                'Daniela comment out not needed lucy_update_table() will update approval code
                'lucy_update_so()  'sabrina add (commented out in the send_out_void_fi_request)

                'Daniela start Sept 22, 2015

                Try
                    ' At this point, unless auth occurs and fails, SO will be updated so let us lock it here
                    dbConn.Open()
                    dbCmd.Transaction = dbConn.BeginTransaction()

                    soRowid = SalesUtils.LockSoRec(txt_del_doc_num.Text, dbCmd)

                    lucy_update_table(dbCmd)

                    dbCmd.Transaction.Commit()

                Catch ex As Exception
                    dbCmd.Transaction.Rollback()
                    If ex.Message = String.Format(Resources.POSErrors.ERR0042, txt_del_doc_num.Text) Then
                        ModifyControls(1) ' disable controls other than Clear button!
                        btn_save_fi_co.Enabled = False
                    End If
                    lblErrorMsg.Text = ex.Message
                    Exit Sub

                Finally
                    If Not IsNothing(dbCmd.Transaction) Then
                        dbCmd.Transaction.Dispose()
                    End If
                    dbCmd.Dispose()
                    dbConn.Close()
                    dbConn.Dispose()
                End Try

                'Daniela end Sept 22, 2015
                'sabrina
                ' Daniela added new parameter to reuse 
                ' Daniela May 5 only print if there is acct#
                If isNotEmpty(txt_fi_acct.Text) Then
                    'Daniela french UFM
                    'Dim cult As String = UICulture
                    Dim ufm As String = "VAR"
                    'If InStr(cult.ToLower, "fr") >= 1 Then
                    ufm = "VAR" 'Did not change for now Donna to confirm if french needed
                    'End If
                    sy_print_salesbill(txt_del_doc_num.Text, Session("emp_cd"), ufm)
                End If

            End If
        Else 'sabrina add
            ' Daniela May 5 allow remove if no acct#
            If isNotEmpty(txt_fi_acct.Text) Then
                lblErrorMsg.Text = "Invalid Authorization#" 'sabrina add
                Exit Sub 'sabrina add
            Else
                txt_fi_appr.Text = ""
                'Daniela commented out
                'lucy_update_so()
                'lucy_update_table()
                'Daniela start Sept 22, 2015

                Try
                    ' At this point, unless auth occurs and fails, SO will be updated so let us lock it here
                    dbConn.Open()
                    dbCmd.Transaction = dbConn.BeginTransaction()

                    soRowid = SalesUtils.LockSoRec(txt_del_doc_num.Text, dbCmd)

                    lucy_update_table(dbCmd)

                    dbCmd.Transaction.Commit()

                Catch ex As Exception
                    dbCmd.Transaction.Rollback()
                    If ex.Message = String.Format(Resources.POSErrors.ERR0042, txt_del_doc_num.Text) Then
                        ModifyControls(1) ' disable controls other than Clear button!
                        btn_save_fi_co.Enabled = False
                    End If
                    lblErrorMsg.Text = ex.Message
                    Exit Sub

                Finally
                    If Not IsNothing(dbCmd.Transaction) Then
                        dbCmd.Transaction.Dispose()
                    End If
                    dbCmd.Dispose()
                    dbConn.Close()
                    dbConn.Dispose()
                End Try

                'Daniela end Sept 22, 2015

            End If
        End If
        ' send_out_void_fi_request()
        '  UpdateAmountsAfterModifyFinance()
        ' Session("ORIG_FI_INFO") = Nothing
        ' PopupModifyFinance.ShowOnPageLoad = False

    End Sub

    Private Sub addFinancePopupComments(ByRef cmd As OracleCommand)
        ' Do not add comment if finance popup has not been clicked
        If Session("ORIG_FI_INFO") Is Nothing Then
            Return
        End If

        ' Get original finance information from session created on finance icon click
        Dim orgFinanceProviderCode As String = Session("ORIG_FI_INFO").finCo ' Finance provider code
        Dim orgFinanceAuthorizationCode As String = Session("ORIG_FI_INFO").approval ' Finance authorization code

        ' Get current finance information from popup input fields
        Dim currFinanceProviderCode = cbo_finance_company.SelectedValue
        Dim currFinanceAuthorizationCode = txt_fi_appr.Text.ToString

        ' Add comment if original finance authorization code (Cached on page load) has changed
        If orgFinanceAuthorizationCode <> currFinanceAuthorizationCode Then
            HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "FAC", orgFinanceAuthorizationCode, currFinanceAuthorizationCode, Session("emp_cd"))
        End If

        ' Add comment if original finance provider code (Cached on page load) has changed
        If orgFinanceProviderCode <> currFinanceProviderCode Then
            HBCG_Utils.SaveAutoComment(cmd, txt_del_doc_num.Text, txt_wr_dt.Text, "FCC", orgFinanceProviderCode, currFinanceProviderCode, Session("emp_cd"))
        End If
    End Sub
End Class
