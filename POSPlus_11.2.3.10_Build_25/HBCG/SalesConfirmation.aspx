﻿<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="True" CodeFile="SalesConfirmation.aspx.vb" Inherits="SalesConfirmation" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <script type="text/javascript">
            $(document).ready(function () {
                $(document).on("contextmenu", function () {
                    return false;
                });
            });

            window.name = "ActiveTab"
    </script>

      <%-- MM-19550 --%>
<script type="text/javascript" language="javascript">
    window.onload = function () {
        var GetDocumentScrollTop = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollTop > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageYOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollTop || document.body.scrollTop;
                return document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        };
        var _aspxGetDocumentScrollTop = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageYOffset;
                else
                    return document.documentElement.scrollTop || document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        }
        if (window._aspxGetDocumentScrollTop) {
            window._aspxGetDocumentScrollTop = _aspxGetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = _aspxGetDocumentScrollTop;
        } else {
            window.ASPx.GetDocumentScrollTop = GetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = GetDocumentScrollTop;
        }
        /* Begin -> Correct ScrollLeft  */
        var GetDocumentScrollLeft = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollLeft > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageXOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
                return document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        };
        var _aspxGetDocumentScrollLeft = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageXOffset;
                else
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        }
        if (window._aspxGetDocumentScrollLeft) {
            window._aspxGetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
        } else {
            window.ASPx.GetDocumentScrollLeft = GetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = GetDocumentScrollLeft;
        }
        /* End -> Correct ScrollLeft  */
    };
</script>

    <style type="text/css">
        .hide {
            display: none;
        }

        .aspGridViewLines {
            color: #003300;
            font-size: 11px;
            font-family: Tahoma;
            font-weight: normal;
        }

        .aspGridViewHdr {
            color: #003300;
            font-size: 11px;
            font-family: Tahoma;
            font-weight: bold;
        }

        .devExpressLinkBreakFix {
            display: inline-table;
        }
        .btnInline {

            display: inline-table;
        }
    </style>
    <table class="aspGridViewLines" style="position: relative; width: 100%; left: 0px; top: 0px;">
        <tr valign="middle" >

            <td colspan="6" valign="middle" >
                <asp:Label ID="lbl_ZoneCd" runat="server" Text="<%$ Resources:LibResources, Label685 %>" Width="110px" CssClass="aspGridViewLines">
            </asp:Label>


                <%--<asp:DropDownList ID="StoreCds" runat="server" AutoPostBack="True" Width="300px" CssClass="aspGridViewLines">
                </asp:DropDownList>--%>
                <dx:ASPxComboBox ID="ZoneCdFrom" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" />
                <dx:ASPxComboBox ID="ZoneCdTo" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" />
                <asp:Label ID="lbl_or" runat="server" Text="<%$ Resources:LibResources, Label949 %>" Width="15px" CssClass="aspGridViewLines">
                    </asp:Label>
                <asp:Label ID="lbl_ZoneGrp" runat="server" Text="<%$ Resources:LibResources, Label951 %>" Width="80px" CssClass="aspGridViewLines">
                    </asp:Label>
                <dx:ASPxComboBox ID="ZoneGroup" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="10" AutoPostBack="false" EnableViewState="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="middle" ><asp:Label ID="lbl_StoreCd" runat="server" Text="<%$ Resources:LibResources, Label573 %>" Width="110px" CssClass="aspGridViewLines">
            </asp:Label>
                <dx:ASPxComboBox ID="StoreCds" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" />
            </td>
        </tr>
        <tr>
            <td valign="middle" colspan="3"> 
                <asp:Label ID="lbl_DelPick" runat="server" Text="<%$ Resources:LibResources, Label920 %>" Width="110px" CssClass="aspGridViewLines"></asp:Label>               
                <dx:ASPxComboBox ID="DelPick" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true">
                    <Items>
                        <dx:ListEditItem Value="D" Text="D - Delivery" />
                        <dx:ListEditItem Value="P" Text="P - Pickup" />
                    </Items>
                </dx:ASPxComboBox>                
            </td>
           <%-- <td>
                <asp:Label ID="lbl_gTot" runat="server" Text="<%$ Resources:LibResources, Label224 %>" CssClass="aspGridViewLines" Width="85px">
                </asp:Label>
                <asp:Label ID="lbl_gTotVal" runat="server" Text="" CssClass="aspGridViewLines" Width="110px">
                </asp:Label>
            </td>--%>
        </tr>

        <tr id="TRConfStatusFrom" runat="server">
            <td colspan="3" valign="middle" >
				<div style="display: inline;">
				<asp:Label ID="lbl_DelPickDt" runat="server" Text="<%$ Resources:LibResources, Label921 %>" Width="110px" CssClass="aspGridViewLines"></asp:Label>
                <%--<asp:DropDownList ID="ddlConfStatusFrom" runat="server" CssClass="aspGridViewLines" AutoPostBack="false">
                </asp:DropDownList>--%>
				<dx:ASPxDateEdit ID="DelPickDtFrom" runat="server" NullText = "MM/dd/yyyy" CssClass="devExpressLinkBreakFix" EditFormatString="MM/dd/yyyy">
				</dx:ASPxDateEdit>
                <dx:ASPxDateEdit ID="DelPickDtTo" runat="server" NullText = "MM/dd/yyyy" CssClass="devExpressLinkBreakFix">
				</dx:ASPxDateEdit>
				</div>
            </td>
        </tr>
        <tr id="TRConfStatusTo" runat="server" >

            <td colspan="2" valign="middle" >
                <asp:Label ID="lbl_SalesOrder" runat="server" Text="<%$ Resources:LibResources, Label511 %>" Width="110px" CssClass="aspGridViewLines">
                             </asp:Label>                
                <dx:ASPxTextBox ID="txt_SalesOrder" runat="server" CssClass="btnInline"/>
            </td>

        </tr>
        <tr id="TR1" runat="server" >

            <td colspan="4" valign="middle" >
                <asp:Label ID="lbl_pickUpDelSt" runat="server" Text="<%$ Resources:LibResources, Label922 %>" Width="110px" CssClass="aspGridViewLines"></asp:Label>
                <dx:ASPxComboBox ID="PickUpDelSt" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" SelectedIndex="-1"/>
                <asp:Label ID="lb_cust" runat="server" Text="<%$ Resources:LibResources, Label132 %>"></asp:Label>
                <dx:ASPxTextBox ID="txt_CustCd" runat="server" CssClass="btnInline">                   
                </dx:ASPxTextBox>         
            </td>

        </tr>
         <tr id="TR2" runat="server" >

            <td colspan="2" valign="middle" ><asp:Label ID="lbl_ConfSt" runat="server" Text="<%$ Resources:LibResources, Label923 %>" Width="110px" CssClass="aspGridViewLines"></asp:Label>
                <dx:ASPxComboBox ID="CustTps" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" SelectedIndex="-1">
                 <Items>
                        <dx:ListEditItem Value="CON" Text="CONFIRM" />
                        <dx:ListEditItem Value="UNC" Text="UNCONFIRM" />
                        <dx:ListEditItem Value="INP" Text="IN-PROGRESS" />
                        <dx:ListEditItem Value="ALL" Text="ALL" />
                    </Items>
                </dx:ASPxComboBox>  
            </td>

        </tr>

         <tr id="TR3" runat="server" >

            <td colspan="2" valign="middle" ><asp:Label ID="lbl_IsPaid" runat="server" Text="<%$ Resources:LibResources, Label948 %>" Width="110px" CssClass="aspGridViewLines"></asp:Label>
                <dx:ASPxComboBox ID="isPaid" runat="server" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDownList" CssClass="devExpressLinkBreakFix"
                    CallbackPageSize="15" AutoPostBack="false" EnableViewState="true" SelectedIndex="-1">
                 <Items>
                        <dx:ListEditItem Value="Y" Text="Yes" />
                        <dx:ListEditItem Value="N" Text="No" />                        
                    </Items>
                </dx:ASPxComboBox>  
            </td>

        </tr>

        <tr>
            <td><dx:ASPxButton ID="btn_LookUp" runat="server" Text="<%$ Resources:LibResources, Label278 %>"></dx:ASPxButton></td>
            <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Text="<%$ Resources:LibResources, Label83 %>"></dx:ASPxButton>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="lbl_msg" runat="server" Text="" CssClass="aspGridViewLines" ForeColor="Red" Width="98%">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <br /><asp:Label ID="lbl_asOf" runat="server" Text="" CssClass="aspGridViewLines" >
                </asp:Label>
            </td>
        </tr>
    </table>
    <br />
    
    <%--OnRowEditing="DocGrid_RowEditing"         
        OnRowCancelingEdit="DocGrid_RowCancelingEdit" 
        OnRowUpdating="DocGrid_RowUpdating"
        OnPageIndexChanging="DocGrid_PageIndexChanging"--%>

    <asp:GridView ID="DocGrid" Style="position: relative; top: 1px; left: 1px;" runat="server" OnRowDataBound="OnRowDataBound" 
        OnRowEditing="DocGrid_RowEditing" OnEditCommand="DocGrid_ItemCommand"   
        AutoGenerateColumns="False" CellPadding="3" DataKeyField="del_doc_num"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" AutoGenerateSelectButton="False"
        Font-Underline="False" Height="156px" Width="99%"  >
        <AlternatingRowStyle BackColor="Beige" BorderColor="Green"></AlternatingRowStyle>
        <HeaderStyle HorizontalAlign="Center" Font-Underline="False" CssClass="aspGridViewHdr"></HeaderStyle>
        <Columns>
           <asp:HyperLinkField DataNavigateUrlFields="del_doc_num" Target="_self" DataTextField="del_doc_num" HeaderText="<%$ Resources:LibResources, Label511 %>"
                DataNavigateUrlFormatString="salesordermaintenance.aspx?referrer=SalesConfirmation.aspx&del_doc_num={0}&query_returned=Y">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
            </asp:HyperLinkField>
		<asp:TemplateField>
		    <ItemTemplate>
                    <asp:ImageButton ID="Image1" runat="server" ImageUrl="images/icons/DocumentsBlack.PNG" OnClick="ButtonSoDetails_Click"
                        Height="20" Width="20" CommandName="so_details" ToolTip="SO Details" AlternateText="SO Details" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.del_doc_num") %>'/>                    
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
       </asp:TemplateField>
            <asp:BoundField DataField="del_doc_num" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label511 %>" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
			<%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="Image1" runat="server" ImageUrl="images/icons/DocumentsBlack.PNG"
                        Height="20" Width="20" CommandName="so_detail" ToolTip="SO Details" AlternateText="SO Details" />
                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
            </asp:TemplateField>--%>
            <asp:BoundField DataField="zone_cd" Visible="true"  ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label685 %>">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="ORD_TP_CD" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label944 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="CUST_CD" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label130 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="CUST_NAME" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label138 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" Font-Size="XX-Small"/>
            </asp:BoundField>
            <asp:BoundField DataField="SHIP_TO_H_PHONE" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label229 %>" Visible="True" >
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <%--<asp:BoundField DataField="SHIP_TO_ADDRESS" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label950 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" Font-Size="XX-Small"/>
            </asp:BoundField>--%>
            <asp:TemplateField HeaderText="<%$ Resources:LibResources, Label950 %>">
				  <ItemTemplate>
                    <asp:Label ID="SHIP_TO_ADDRESS" runat="server" AutoPostBack="true" 
                        Text='<%# DataBinder.Eval(Container, "DataItem.SHIP_TO_ADDRESS") %>' Font-Size="XX-Small" Wrap="False"></asp:Label>
					<asp:ImageButton ID="AddressDetails" runat="server" Height="15px" ImageUrl="~/images/icons/find.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.SHIP_TO_ADDR2") %>'			
                            CommandName="SHIP2" Width="10px" ToolTip="<%$ Resources:LibResources, Label19 %>" OnClick="AddressDetails_Click"/>
                </ItemTemplate>
                 <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" Font-Size="XX-Small"/>
            </asp:TemplateField>
			<asp:BoundField DataField="mult_ords" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label324 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="PICK_DEL_FLAG" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label945 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
             <asp:BoundField DataField="pu_del_dt" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label394 %>" Visible="True" DataFormatString="{0:MM/dd/yyyy}">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="res" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label472 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="TKM" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label946 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="INV_AVAIL" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label244 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="SPEC_ATTR" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label947 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="PAID_DEL_DOC" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label948 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
            <asp:BoundField DataField="CONF_STAT_CD" ReadOnly="True" HeaderText="<%$ Resources:LibResources, Label101 %>" Visible="False">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" Wrap="False" CssClass="aspGridViewLines" />
            </asp:BoundField>
          <%--  <asp:BoundField DataField="CONF_STAT_CD" ReadOnly="False" HeaderText="<%$ Resources:LibResources, Label101 %>" Visible="True">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Center" Wrap="False" CssClass="aspGridViewLines" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>--%>
            <asp:TemplateField HeaderText="<%$ Resources:LibResources, Label101 %>" ControlStyle-Font-Size="XX-Small">
            <ItemTemplate>
                <asp:Label ID="lblConfStatus" runat="server" Text='<%# Eval("CONF_STAT_CD")%>' Visible = "false" />
                <asp:DropDownList ID="ddConfStatus" runat="server" AutoPostBack="true" DataTextField="CONF_STAT_CD" DataValueField="CONF_STAT_CD" Font-Size="XX-Small" Enabled="True"  ReadOnly="False">
                    <asp:ListItem Value="CON">CONFIRM</asp:ListItem>
                    <asp:ListItem Value="UNC">UNCONFIRM</asp:ListItem>
                    <asp:ListItem Value="INP">IN-PROGRESS</asp:ListItem>
                </asp:DropDownList>
            </ItemTemplate>
		   </asp:TemplateField>
            <asp:BoundField DataField="row_proc" ShowHeader="true" ItemStyle-HorizontalAlign="Left" Visible="false" ControlStyle-Width="0">
                <ItemStyle CssClass="hide" Width="0" />
                <HeaderStyle Width="0" />
            </asp:BoundField>
            <asp:CommandField ShowSelectButton="True" SelectText="<%$ Resources:LibResources, Label520 %>"/>
          <%--   <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update" />  
                    </ItemTemplate>                      
                </asp:TemplateField>  --%>
        </Columns>
    </asp:GridView>
    <dxpc:ASPxPopupControl ID="SODetailPopup" runat="server" CloseAction="CloseButton"
        HeaderText="SO Detail" Height="104px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="317px">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Label ID="lbl_sodtl" runat="server"></asp:Label>
						<asp:DataGrid ID="SODetail" runat="server" AllowSorting="False" AlternatingItemStyle-BackColor="Beige"
                                AutoGenerateColumns="False" CellPadding="3" DataKeyField="DEL_DOC_LN#" Height="16px"
                                PagerStyle-HorizontalAlign="Right" PagerStyle-Mode="NumericPages"
                                Width="99%" CssClass="style5">
                                <Columns>
                                    <asp:BoundColumn DataField="qty" HeaderText="<%$ Resources:LibResources, Label447 %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="item" HeaderText="<%$ Resources:LibResources, Label550 %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="DES" HeaderText="DES">
									<ItemStyle Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="ITM_TP_CD" HeaderText="<%$ Resources:LibResources, Label964 %>"></asp:BoundColumn>
									<asp:BoundColumn DataField="DROP_CD" HeaderText="<%$ Resources:LibResources, Label965 %>"></asp:BoundColumn>
									<asp:BoundColumn DataField="ITM_SRT_CD" HeaderText="ATTR"></asp:BoundColumn>
									<asp:BoundColumn DataField="OUT_CD" HeaderText="<%$ Resources:LibResources, Label750 %>"></asp:BoundColumn>
									<asp:BoundColumn DataField="STORE_CD" HeaderText="RSV"></asp:BoundColumn>
									<asp:BoundColumn DataField="ZONE_PRI_AV_QTY" HeaderText="<%$ Resources:LibResources, Label961 %>"></asp:BoundColumn>
									<asp:BoundColumn DataField="POIST_LINK_ARR_DT" HeaderText="<%$ Resources:LibResources, Label963 %>">
										<ItemStyle Wrap="False"></ItemStyle>
									</asp:BoundColumn>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Mode="NumericPages" />
                                <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" ForeColor="Black" CssClass="style5" />
                            </asp:DataGrid>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
   <dxpc:ASPxPopupControl ID="PopupAddress" runat="server" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label968 %>" Height="104px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="317px">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Label ID="lbl_gm" runat="server"></asp:Label>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    &nbsp;
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    &nbsp;<asp:Image ID="img_warn" runat="server" ImageUrl="images/warning.jpg" Visible="False" />
    &nbsp;
    <dx:ASPxLabel ID="lbl_Warning" Text="" runat="server" Width="100%">
    </dx:ASPxLabel>
</asp:Content>

