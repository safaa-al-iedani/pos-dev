﻿
Partial Class RedirectingToSalesRewrite
    Inherits POSBasePage

    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Proceed.Click

        'If the current user has write access then perform the below operation
        'If the current has has read-only permission then no need to perform below activity            
        If (Not IsNothing(Session("provideAccess"))) AndAlso (Session("provideAccess") = 0) AndAlso (Not (String.IsNullOrWhiteSpace(Convert.ToString(Session("EMP_INIT"))))) Then
            Dim accessInfo As New AccessControlBiz()
            accessInfo.DeleteRecord("SalesOrderMaintenance.aspx")
        End If

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup6();")
        If Request("RELATED_SKU") <> "" Then
            Response.Write("parentWindow.location.href='SalesRewrite.aspx?RELATED_SKU=" & Request("RELATED_SKU") & "';" & vbCrLf)
        Else
            Response.Write("close();")
        End If
        Response.Write("</script>")

    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup6();")
        Response.Write("close();")
        Response.Write("</script>")

    End Sub
End Class
