Imports System.Data.OracleClient

Partial Class User_Defined_Fields
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim oAdp As OracleDataAdapter
            Dim ds As DataSet
            ds = New DataSet

            Dim MyDataReader As OracleDataReader

            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                                ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If

            sql = "SELECT FLD_NAME, FLD_TITLE, FLD_LENGTH, FLD_DEF_VAL FROM USR_FLD WHERE TBL_NAME='SO'"

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)

            Try
                'Open Connection 
                conn.Open()
                'Execute DataReader 
                MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                'Store Values in String Variables 
                If (MyDataReader.Read()) Then
                    MyDataGrid.DataSource = ds
                    MyDataGrid.DataBind()
                End If

                'Close Connection 
                MyDataReader.Close()
                conn.Close()
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End If

    End Sub

    Protected Sub UDF_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim txt_fld_value As TextBox = CType(sender, TextBox)
        Dim dgItem As DataGridItem = CType(txt_fld_value.NamingContainer, DataGridItem)
        Select Case dgItem.Cells(0).Text
            Case "USR_FLD_1"
                Session("USR_FLD_1") = txt_fld_value.Text
            Case "USR_FLD_2"
                Session("USR_FLD_2") = txt_fld_value.Text
            Case "USR_FLD_3"
                Session("USR_FLD_3") = txt_fld_value.Text
            Case "USR_FLD_4"
                Session("USR_FLD_4") = txt_fld_value.Text
            Case "USR_FLD_5"
                Session("USR_FLD_5") = txt_fld_value.Text
        End Select

    End Sub

    Protected Sub MyDataGrid_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles MyDataGrid.ItemDataBound

        Dim txt_fld_value As TextBox
        txt_fld_value = CType(e.Item.FindControl("txt_fld_value"), TextBox)
        If Not txt_fld_value Is Nothing Then
            If e.Item.Cells(2).Text & "" <> "" And e.Item.Cells(2).Text <> "&nbsp;" Then
                txt_fld_value.Visible = True
                txt_fld_value.MaxLength = e.Item.Cells(1).Text
                Select Case e.Item.Cells(0).Text
                    Case "USR_FLD_1"
                        txt_fld_value.Text = Session("USR_FLD_1")
                    Case "USR_FLD_2"
                        txt_fld_value.Text = Session("USR_FLD_2")
                    Case "USR_FLD_3"
                        txt_fld_value.Text = Session("USR_FLD_3")
                    Case "USR_FLD_4"
                        txt_fld_value.Text = Session("USR_FLD_4")
                    Case "USR_FLD_5"
                        txt_fld_value.Text = Session("USR_FLD_5")
                End Select
            End If
        End If

    End Sub
End Class
