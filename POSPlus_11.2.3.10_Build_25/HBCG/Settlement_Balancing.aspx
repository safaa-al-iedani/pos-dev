<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Settlement_Balancing.aspx.vb" Inherits="Settlement_Balancing" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="95%" bgcolor="White">
        <tr>
            <td height="100%" align="left" valign="middle">
                <div style="padding-left: 18px; padding-top: 10px" id="main_body" runat="server">
                    <table border="0" cellpadding="0" cellspacing="0" width="95%" class="style5">
                        <tr>
                            <td align="left" valign="middle">
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Balancing">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0" width="95%" class="style5">
                        <tr>
                            <td align="left" valign="middle" style="height: 28px; width: 451px;">
                                &nbsp;<dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Store:">
                                </dx:ASPxLabel>
                                <asp:DropDownList ID="store_cd" runat="server" AutoPostBack="True" CssClass="style5"
                                    Width="292px">
                                </asp:DropDownList>
                                <br />
                                <br />
                                &nbsp;
                            </td>
                            <td align="right" valign="middle" style="width: 602px; height: 28px;">
                                <table>
                                    <tr>
                                        <td style="width: 123px">
                                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Daily Total:">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            $
                                            <dx:ASPxLabel ID="lbl_daily_total" runat="server" Text="0.00">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 123px">
                                            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Amount to Settle:">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            $
                                            <dx:ASPxLabel ID="lbl_amt_settled" runat="server" Text="0.00">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 123px">
                                            <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Difference:">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            $
                                            <dx:ASPxLabel ID="lbl_difference" runat="server" Text="0.00">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 123px">
                                            <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="# of Transactions:">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            &nbsp; &nbsp;<dx:ASPxLabel ID="lbl_tran_count" runat="server" Text="0">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr />
                                &nbsp;
                                <dx:ASPxLabel ID="lbl_grid_msg" runat="server" Text="" Width="100%">
                                </dx:ASPxLabel>
                                <br />
                                <br />
                                <asp:DataGrid ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    CssClass="style5" Height="200px" OnSortCommand="MyDataGrid_Sort" Width="100%">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="SelectThis" runat="server" AutoPostBack="true" OnCheckedChanged="Update_Checked_Items" />
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="WR_DT" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Date"
                                            SortExpression="WR_DT">
                                            <HeaderStyle Height="10px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CUST_CD" HeaderText="Bill to Name" SortExpression="CUST_CD">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="BNK_CRD_NUM" HeaderText="Account Number" SortExpression="BNK_CRD_NUM"
                                            Visible="True"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="BNK_CRD_NUM" HeaderText="Account Number" SortExpression="BNK_CRD_NUM"
                                            Visible="False"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="STORE_CD" HeaderText="Store" SortExpression="STORE_CD"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="AMT" HeaderText="Amount" SortExpression="AMT" DataFormatString="{0:C2}">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="APP_CD" HeaderText="Auth Code" SortExpression="APP_CD"
                                            Visible="False"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="APP_CD" HeaderText="Auth Code" SortExpression="APP_CD"
                                            Visible="True"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="REF_NUM" HeaderText="Plan Code" SortExpression="REF_NUM"
                                            Visible="False"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="REF_NUM" HeaderText="Plan Code" SortExpression="REF_NUM"
                                            Visible="True"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="MEDIUM_TP_CD" HeaderText="Type" SortExpression="MEDIUM_TP_CD">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DEL_DOC_NUM" HeaderText="Sales Order" SortExpression="DEL_DOC_NUM">
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:Button ID="Button1" runat="server" CssClass="style5" Text="Remove" Visible="False" />
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="MEDIUM_TP_CD" HeaderText="Sales Order" SortExpression="ORD_TP_CD"
                                            Visible="False"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="STAT_CD" HeaderText="Sales Order" SortExpression="STAT_CD"
                                            Visible="False"></asp:BoundColumn>
                                    </Columns>
                                    <AlternatingItemStyle BackColor="Beige" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Size="X-Small" Font-Strikeout="False" Font-Underline="False" />
                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Size="X-Small"
                                        Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                                    <EditItemStyle CssClass="style5" />
                                    <ItemStyle CssClass="style5" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Size="X-Small" Font-Strikeout="False" Font-Underline="False" />
                                    <SelectedItemStyle BackColor="Silver" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" />
                                </asp:DataGrid><br />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" valign="middle" style="height: 19px">
                <dx:ASPxLabel ID="lbl_header" runat="server" Text="" ForeColor="Red">
                </dx:ASPxLabel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
