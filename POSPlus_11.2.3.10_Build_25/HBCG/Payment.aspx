<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Payment.aspx.vb" Inherits="Payment"
    MasterPageFile="~/Regular.master" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.v13.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript">
        function SelectAndClosePopup1() {
            ASPxPopupControl1.Hide();
        }
        function SelectAndClosePopup2() {
            ASPxPopupControl2.Hide();
        }
        function SelectAndClosePopup4() {
            ASPxPopupControl4.Hide();
        }
        function stopEnterKey(evt) {
            evt = (evt) ? evt : event;
            var charCode = evt.which || evt.charCode || evt.keyCode || 0;
            if (charCode == 13) {
                evt.preventDefault();
                return false;
            }
        }
    </script>

      <script language="javascript">
          function disableButton(id) {
              // Daniela B 20140722 Disable the 2 buttons to prevent double click
              try {
                  var a = document.getElementById(id);
                  a.style.display = 'none';
                  var b = document.getElementById('ctl00_ASPxRoundPanel4_ContentPlaceHolder1_ASPxPopupControl2_lucy_btn_new_cust');
                  b.style.display = 'none';
              } catch (err) {
                  // daniela return true in case there is no lucy_btn_new_cust
                  return true;
              }
              return true;
          }

    </script>


    <table>
        <tr>
            <td>
                <b>
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="<%$ Resources:LibResources, Label292 %>">
                    </dx:ASPxLabel>
                </b>
            </td>
            <td style="width: 18px">
                <b>
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="<%$ Resources:LibResources, Label28 %>">
                    </dx:ASPxLabel>
                </b>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxComboBox ID="mop_drp" runat="server" AutoPostBack="True" Width="339px"
                    IncrementalFilteringMode ="StartsWith">
                </dx:ASPxComboBox>
            </td>
            <td style="width: 18px">
                <dx:ASPxTextBox ID="TextBox1" runat="server" Width="75px" onkeydown="javascript:stopEnterKey(event)">
                </dx:ASPxTextBox>
            </td>
            <td align="right">
                <dx:ASPxButton ID="btn_add" runat="server" Text="<%$ Resources:LibResources, Label17 %>">
                </dx:ASPxButton>
            </td>
        
                      <%--Lucy added  Nov, 2019 for request 214--%>
                        <td>
                            <dx:ASPxButton ID="btn_add_spc" runat="server" Text="Add_SPC#"  ClientIDMode="Static"   >
                            </dx:ASPxButton>
                        </td>
            </tr>

        <tr>
            <td colspan="3" valign="top" align="right">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:Label ID="lbl_pmt_amt" runat="server" Visible="False"></asp:Label>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator"
                                Width="126px"></asp:CustomValidator>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_dp" runat="server" Width="26px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxButton runat="server" ID="btn_calc_dep" Text="<%$ Resources:LibResources, Label54 %>">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr runat="server" id="PO_Table">
            <td colspan="2" valign="top" align="left">
                <asp:Panel ID="CashBreakdownPanel" runat="server">
                    <table cellpadding="2" runat="server" id="cashBreakdownSection" visible="false"><%-- Cash Breakdown section  --%>
                        <tr>
                            <td colspan="8" style="white-space: nowrap;">
                                <asp:Label ID="cashBreakdownError" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8" style="white-space: nowrap;">
                                <b><asp:Label ID="lbl_cash_breakdown" runat="server" Text="<%$ Resources:LibResources, Label1014 %>"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">
                                <asp:Label ID="lbl_cash_breakdown_coins" runat="server" Text="<%$ Resources:LibResources, Label1017 %>"></asp:Label>
                            </td>
                            <td align="right">=</td>
                            <td align="right" style="white-space:nowrap;">
                                <asp:TextBox ID="txt_cash_breakdown_coins_total" runat="server" Width="113px" AutoPostBack="True" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txt_cash_breakdown_5" runat="server"  Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                            </td>
                            <td align="center">x</td>
                            <td align="right">$5</td>
                            <td align="right">=</td>
                            <td align="right">
                                <asp:TextBox ID="txt_cash_breakdown_5_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txt_cash_breakdown_10" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                            </td>
                            <td align="center">x</td>
                            <td align="right">$10</td>
                            <td align="right">=</td>
                            <td align="right">
                                <asp:TextBox ID="txt_cash_breakdown_10_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txt_cash_breakdown_20" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                            </td>
                            <td align="center">x</td>
                            <td align="right">$20</td>
                            <td align="right">=</td>
                            <td align="right">
                                <asp:TextBox ID="txt_cash_breakdown_20_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td align="right" style="white-space: nowrap;">
                                <b><asp:Label ID="lbl_cash_breakdown_total" runat="server" Text="<%$ Resources:LibResources, Label1015 %>"></asp:Label>:</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_cash_breakdown_total" runat="server" Width="113px" AutoPostBack="True" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txt_cash_breakdown_50" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                            </td>
                            <td align="center">x</td>
                            <td align="right">$50</td>
                            <td align="right">=</td>
                            <td align="right">
                                <asp:TextBox ID="txt_cash_breakdown_50_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txt_cash_breakdown_100" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                            </td>
                            <td align="center">x</td>
                            <td align="right">$100</td>
                            <td align="right">=</td>
                            <td align="right">
                                <asp:TextBox ID="txt_cash_breakdown_100_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txt_cash_breakdown_1000" runat="server" Width="75px" AutoPostBack="True" MaxLength="15" OnTextChanged="updated_cash_breakdown" onkeydown="javascript:stopEnterKey(event)"></asp:TextBox>
                            </td>
                            <td align="center">x</td>
                            <td align="right">$1000</td>
                            <td align="right">=</td>
                            <td align="right">
                                <asp:TextBox ID="txt_cash_breakdown_1000_total" runat="server" Width="113px" AutoPostBack="False" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td align="right" style="white-space: nowrap;">
                                <b><asp:Label ID="lbl_cash_breakdown_change" runat="server" Text="<%$ Resources:LibResources, Label1016 %>"></asp:Label>:</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_cash_breakdown_change" runat="server" Width="113px" AutoPostBack="True" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="right">
                <table>
                    <tr>
                        <td valign="top" style="white-space: nowrap;">
                            <asp:Label ID="lbl_po" runat="server" Text="<%$ Resources:LibResources, Label139 %>"></asp:Label>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txt_po" runat="server" Width="113px" AutoPostBack="True" MaxLength="15">
							</dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <asp:Label ID="lucy_lbl_aprv" runat="server" Text="lucy_lbl_aprv"></asp:Label>
                  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            <td colspan="3" align="right">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="<%$ Resources:LibResources, Label92 %>">
                </dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl_cod" runat="server" Text="">
                </dx:ASPxLabel>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="vld_price" runat="server" ErrorMessage="CustomValidator"
        Visible="false">
    </asp:CustomValidator>
    <asp:DataGrid ID="Gridview1" Style="position: relative; top: 9px;" runat="server"
        OnCancelCommand="DoItemCancel" OnEditCommand="DoItemEdit" OnUpdateCommand="DoItemUpdate"
        OnDeleteCommand="DoItemDelete" AutoGenerateColumns="False" CellPadding="2" DataKeyField="row_id"
        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
        Font-Underline="False" Height="95px" Width="98%" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left">
        <alternatingitemstyle backcolor="Beige" font-italic="False" font-strikeout="False"
            font-underline="False" font-overline="False" font-bold="False"></alternatingitemstyle>
        <headerstyle font-italic="False" font-strikeout="False" font-underline="False" font-overline="False"
            font-bold="True" height="20px" horizontalalign="Left"></headerstyle>
        <columns>
            <asp:BoundColumn DataField="mop_cd" HeaderText="SKU" ReadOnly="True" Visible="False">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="des" HeaderText="Payment Type" ReadOnly="True">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="amt" HeaderText="<%$ Resources:LibResources, Label28 %>" DataFormatString="{0:0.00}">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:BoundColumn>
            <asp:BoundColumn DataField="row_id" HeaderText="ID" Visible="False"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Electronic?">
                <ItemTemplate>

                    <%-- lucy remove  
                    <asp:Image ID="Image1" runat="server" ImageUrl="images/icons/tick.png" Height="20"
                        Width="20" /> --%>

                </ItemTemplate>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" />
            </asp:TemplateColumn>
            <asp:EditCommandColumn CancelText="&lt;img src='images/icons/Undo24.gif' border='0'&gt;"
                UpdateText="&lt;img src='images/icons/Save24.gif' border='0'&gt;" EditText="&lt;img src='images/icons/Edit24.gif' border='0'&gt;"
                HeaderText="Edit">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:EditCommandColumn>
            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/icons/Trashcan_30.png' border='0'&gt;"
                HeaderText="Delete">
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </asp:ButtonColumn>
            <asp:BoundColumn DataField="mop_tp" HeaderText="TP" Visible="False"></asp:BoundColumn>
        </columns>
        <itemstyle verticalalign="Top" />
    </asp:DataGrid>&nbsp;
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="ASPxPopupControl1" CloseAction="CloseButton"
        HeaderText="Credit Card Payments" Height="300px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="500px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" ClientInstanceName="ASPxPopupControl2" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label205%>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="450px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlFinanceDetails" runat="server" DefaultButton="btn_save_fi_co">
                      <%--Daniela B 20140722 <asp:Button ID="btn_save_fi_co" runat="server" CssClass="style5" Text="Click to Swipe/Insert " Visible="False" ForeColor="Fuchsia"/>--%> 
                 <asp:Button ID="btn_save_fi_co" runat="server" CssClass="style5" Text="<%$ Resources:LibResources, Label90 %>" Visible="False" ForeColor="Fuchsia"  OnClientClick="return disableButton(this.id);"/>
                 <asp:Button ID="lucy_btn_new_cust" runat="server" Text="<%$ Resources:LibResources, Label331 %>" Visible="False" />
                <table class="style5">
                    <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="style5">
                                <asp:ListItem Value="O" Text="<%$ Resources:LibResources, Label359 %>"></asp:ListItem>
                                <asp:ListItem Value="R">Revolving</asp:ListItem>
                                <asp:ListItem Value="I">Installment</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_finance_company" runat="server" CssClass="style5" Width="318px"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                         
                        <td>
                            <asp:DropDownList ID="cbo_promo" runat="server" CssClass="style5" Width="318px" AutoPostBack="True" Visible="False">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_fi_acct" runat="server" CssClass="style5"   OnTextChanged="txt_fi_acct_TextChanged"></asp:TextBox>
                            &nbsp; &nbsp; <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label192 %>" />:
                            <asp:TextBox ID="txt_fi_exp_dt" runat="server" CssClass="style5" MaxLength="4" Width="40px"  OnTextChanged="txt_fi_exp_dt_OnTextChanged"></asp:TextBox>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label997 %>" />
                        </td>
                    </tr>
                    <tr><%--
                        <td>
                            &nbsp;</td> --%>
                        
                       
                            <%--sabrina R1999 --%>
                            <td><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:LibResources, Label874 %>"/>
                            </td>
                         <td>
                            <asp:TextBox ID="txt_fi_appr" runat="server" CssClass="style5" MaxLength="10" Visible="False"></asp:TextBox>
                            &nbsp; &nbsp; &nbsp;
                            <asp:TextBox ID="txt_fi_sec_cd" runat="server" CssClass="style5" MaxLength="10" Width="33px" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:TextBox ID="txt_fi_dl_st" runat="server" CssClass="style5" MaxLength="2" Width="20px" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:TextBox ID="txt_fi_dl_no" runat="server" CssClass="style5" MaxLength="30" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                </table>

                      <asp:Label ID="lucy_lbl_pay" runat="server" Text=" "></asp:Label>

                <br />
                
                <asp:Label ID="lbl_pmt_tp" runat="server" Visible="False" CssClass="style5"></asp:Label>
                <asp:Label ID="lbl_fi_warning" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                    
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl3" runat="server" CloseAction="CloseButton"
        HeaderText="Check Details" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="494px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <table class="style5">
                    <tr>
                        <td>
                            Check #:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_check" runat="server" CssClass="style5" MaxLength="6"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Bank Routing #:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="TextBox2" runat="server" Width="194px" CssClass="style5" MaxLength="9"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="TextBox3" runat="server" Width="194px" CssClass="style5" MaxLength="25"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Type:
                        </td>
                        <td align="left">
                            &nbsp;<asp:DropDownList ID="DropDownList2" runat="server" CssClass="style5">
                                <asp:ListItem Selected="True" Value="01">Personal</asp:ListItem>
                                <asp:ListItem Value="90">Business</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Approval #:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_appr" runat="server" CssClass="style5" MaxLength="10"></asp:TextBox>
                           
                        </td>
                    </tr>
                    <tr>
                        <td>
                            DL State:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_dl_state" runat="server" CssClass="style5" MaxLength="2" Width="20px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            DL #:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_dl_number" runat="server" CssClass="style5" MaxLength="30"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lbl_chk_warnings" runat="server" CssClass="style5" Font-Bold="True"
                    ForeColor="Red"></asp:Label>
                <br />
                <asp:Button ID="btn_save_chk" runat="server" CssClass="style5" Text="Save Check Details" />
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl4" runat="server" ClientInstanceName="ASPxPopupControl4" CloseAction="CloseButton"
        HeaderText="Use Existing Card?" Height="207px" Modal="True" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="168px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <table>
                    <tr>
                        <td valign="middle" align="center">
                            <img src="images/icons/Symbol-Information.png" style="width: 174px; height: 174px" />
                        </td>
                        <td>
                            &nbsp;&nbsp;
                        </td>
                        <td valign="middle" align="center" style="width: 413px">
                            Would you like to charge the payment to a card on file?
                            <br />
                            <br />
                            <asp:DataGrid ID="MyDataGrid" runat="server" Width="413" BorderColor="Black" CellPadding="3"
                                AutoGenerateColumns="False" DataKeyField="acct_num" Height="16px" PageSize="7"
                                AlternatingItemStyle-BackColor="Beige" CssClass="style5">
                                <Columns>
                                    <asp:BoundColumn HeaderText="Card Number" DataField="BNK_CRD_NUM" />
                                    <asp:BoundColumn HeaderText="Expiration Date" DataField="EXP_DT" DataFormatString="{0:MM/dd/yyyy}" />
                                    <asp:BoundColumn HeaderText="ACCT_NUM" DataField="ACCT_NUM" Visible="false" />
                                    <asp:BoundColumn HeaderText="MOP_CD" DataField="MOP_CD" Visible="false" />
                                    <asp:BoundColumn HeaderText="DES" DataField="DES" Visible="false" />
                                    <asp:TemplateColumn HeaderText="Amount">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="txt_amt_transfer" runat="server" Width="75" CssClass="style5"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:ButtonColumn ButtonType="PushButton" CommandName="Select" Text="Select" ItemStyle-CssClass="style5">
<ItemStyle CssClass="style5"></ItemStyle>
                                    </asp:ButtonColumn>
                                </Columns>
                                <AlternatingItemStyle BackColor="Beige"></AlternatingItemStyle>
                                <HeaderStyle Font-Bold="True" ForeColor="Black" />
                                <EditItemStyle CssClass="style5" />
                                <ItemStyle CssClass="style5" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                <dx:ASPxLabel ID="lbl_card_on_file" runat="server" Width="100%">
                </dx:ASPxLabel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>
    <br />
    <dxpc:ASPxPopupControl ID="ASPxPopupControl9" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="Gift Card" Height="64px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="494px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                <table class="style5">
                    <tr>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Card Number:">
                            </dx:ASPxLabel>
                            &nbsp;
                        </td>
                        <%-- sabrina - add enabled=false --%>
                        <td align="left">
                            <asp:TextBox ID="txt_gc" runat="server" AutoPostBack="true" Width="194px" CssClass="style5"
                                OnTextChanged="txt_gc_TextChanged" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click">
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btn_Clear" runat="server" Text="Clear">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            <dx:ASPxLabel ID="ASPxLabel15" runat="server" Width="100%">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    <br />
   
     <%--Alice added on Oct 03, 2018 for request 4115--%>
     <dxpc:ASPxPopupControl ID="ASPxPopupControl8" runat="server" ClientInstanceName="ASPxPopupControl8" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label205%>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="450px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlFinanceDetails_flx" runat="server" DefaultButton="btn_save_flx">
                  
                    <table class="style5">
                      <tr>
                        <td>
                          
                        </td>
                        <td align ="right" >
                           <asp:Button ID="btn_save_flx" runat="server" Width="150px" Text="<%$ Resources:LibResources, Label520 %>" />
                        </td>
                     </tr>
                      <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList3" runat="server" CssClass="style5">
                                <asp:ListItem Value="O" Text="<%$ Resources:LibResources, Label359 %>"></asp:ListItem>
                                <asp:ListItem Value="R" Text="<%$ Resources:LibResources, Label1010 %>"></asp:ListItem>
                                <asp:ListItem Value="I" Text="<%$ Resources:LibResources, Label1011 %>"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_finance_company_flx" runat="server" CssClass="style5" Width="318px"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                  
                   
                    <tr>
                            <td><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:LibResources, Label874 %>"/>
                            </td>
                             <td>
                                <asp:TextBox ID="txt_fi_appr_flx" runat="server" CssClass="style5" MaxLength="10" Visible="False"></asp:TextBox>
                            </td>
                    </tr>
    
                </table>
                        
                <br />
                
                <asp:Label ID="lbl_pmt_tp_flx" runat="server" Visible="False" CssClass="style5"></asp:Label>
                <asp:Label ID="lbl_fi_warning_flx" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                    
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>


    <%--Alice added on May 28, 2019 for request 235--%>
     <dxpc:ASPxPopupControl ID="ASPxPopupControl11" runat="server" ClientInstanceName="ASPxPopupControl11" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label1024%>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="450px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlFinanceDetails_fm" runat="server" DefaultButton="btn_save_fm_co">
                 <%--<asp:Button ID="btn_save_fm_co" runat="server" CssClass="style5" Text="<%$ Resources:LibResources, Label90 %>" Visible="False" ForeColor="Fuchsia"  />--%>
                    <asp:Button ID="btn_save_fm_co" runat="server"  Text="<%$ Resources:LibResources, Label90 %>" ForeColor="Fuchsia" />
                    <%--<asp:Button ID="Button1" runat="server" Text="Button" />     --%>        
                    <table class="style5">
                   
                      <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList5" runat="server" CssClass="style5">
                                <asp:ListItem Value="O" Text="<%$ Resources:LibResources, Label359 %>"></asp:ListItem>
                                <asp:ListItem Value="R" Text="<%$ Resources:LibResources, Label1010 %>"></asp:ListItem>
                                <asp:ListItem Value="I" Text="<%$ Resources:LibResources, Label1011 %>"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_finance_company_fm" runat="server" CssClass="style5" Width="318px"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                  
                   <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label5 %>" />:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_fm_acct" runat="server" CssClass="style5" AutoPostBack ="true"   OnTextChanged="txt_fm_acct_TextChanged"></asp:TextBox>
                            &nbsp; &nbsp; <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label192 %>" />:
                            <asp:TextBox ID="txt_fm_exp" runat="server" CssClass="style5" AutoPostBack ="true"  MaxLength="4" Width="40px"  OnTextChanged="txt_fm_exp_OnTextChanged"></asp:TextBox>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label997 %>" />
                        </td>
                    </tr>

                    <tr>
                            <td><asp:Literal ID="lit_fm" runat="server" Text="<%$ Resources:LibResources, Label874 %>"/>
                            </td>
                             <td>
                                <asp:TextBox ID="txt_fm_appr" runat="server" CssClass="style5" MaxLength="10" AutoPostBack ="true" OnTextChanged="txt_fm_appr_OnTextChanged"  Visible="False"></asp:TextBox>
                            </td>
                    </tr>
    
                </table>
                        
                <br />
                
                <asp:Label ID="lbl_pmt_tp_fm" runat="server" Visible="False" CssClass="style5"></asp:Label>
                <asp:Label ID="lbl_fm_warning" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                    
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>


     <%--Alice added on Dec 12, 2018 for request 4394--%>
     <dxpc:ASPxPopupControl ID="ASPxPopupControl10" runat="server" ClientInstanceName="ASPxPopupControl10" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label205%>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="450px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Panel ID="PanelChina" runat="server" DefaultButton="btn_save_china">
                  
                    <table class="style5" style=" width:400px; ">
                      <tr>
                        <td width="120px">
                            &nbsp;
                        </td>
                          <td width="200px">
                            &nbsp;
                        </td>
                        <td align ="right" >
                           <asp:Button ID="btn_save_china" runat="server" Width="120px" Text="<%$ Resources:LibResources, Label520 %>" />
                        </td>
                     </tr>
                      <tr>
                        <td width="120px">
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:
                        </td>
                        <td width="200px" >
                            <asp:DropDownList ID="DropDownList4" runat="server" CssClass="style5">
                                <asp:ListItem Value="O" Text="<%$ Resources:LibResources, Label359 %>"></asp:ListItem>
                                <asp:ListItem Value="R" Text="<%$ Resources:LibResources, Label1010 %>"></asp:ListItem>
                                <asp:ListItem Value="I" Text="<%$ Resources:LibResources, Label1011 %>"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                          <td></td>
                    </tr>
  
                    <tr>
                            <td width="120px"><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:LibResources, Label874 %>"/>
                            </td>
                             <td width="200px">
                                <asp:TextBox ID="txt_fi_appr_china" runat="server" CssClass="style5" MaxLength="10" Visible="False"></asp:TextBox>
                            </td>
                        <td></td>
                    </tr>
                        <tr>
                            <td colspan ="3">
                               <span style ="color :red"> <asp:Label ID="Label2" runat="server" Text="<%$ Resources:LibResources, Label1012 %>"></asp:Label></span>
                              
                            </td>
                        </tr>
                  
                </table>
                                       
                <br />
                
                <asp:Label ID="lbl_pmt_tp_china" runat="server" Visible="False" CssClass="style5"></asp:Label>
                <asp:Label ID="lbl_fi_warning_china" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                    
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>


    <%--Lucy added on Nov, 2019  Req-214--%>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl_spc_num" runat="server" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label1202 %>"  Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Height="30px" Width="460px"
        ClientInstanceName="ASPxPopupControl1">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server" Height="30px" Width="550px">
               <table>
                 <tr>    
                   <td align="left">
                      <asp:TextBox ID="txt_spc_num" Width="280px" runat="server"   ToolTip="<%$ Resources:LibResources, Label1205 %>"   > </asp:TextBox>
                   </td>
                 </tr> 
              </table>
          
             <table>
               <tr>                 
                 <td align="left">
                    <dx:ASPxButton ID="btn_spc_save" runat="server" Text="<%$ Resources:LibResources, Label520 %>"  Width="80px" Visible="true" OnClick="btn_spc_save_Click"   ></dx:ASPxButton>
                 </td>
                 <td align="left">
                    <dx:ASPxButton ID="btn_spc_clear" runat="server" Text="<%$ Resources:LibResources, Label82 %>"  Width="80px" Visible="true" OnClick="btn_spc_clear_Click"   > </dx:ASPxButton>
                 </td>
                 <td align="left">
                  <dx:ASPxButton ID="btn_spc_exit" runat="server" Text="<%$ Resources:LibResources, Label1201 %>"  Width="80px" Visible="true" OnClick="btn_spc_exit_Click"  ></dx:ASPxButton>
                 </td>
                </tr>
                <tr>  
                  <td align="left" colspan="3" >
                     <asp:Label ID="lbl_msg" runat="server" BackColor="#FF6600" Width="180px" ForeColor="#333333"  ></asp:Label>
                  </td>
                </tr>
              </table>
           </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>

       <dxpc:ASPxPopupControl ID="ASPxPopupControl12" runat="server" ClientInstanceName="ASPxPopupControl12" CloseAction="CloseButton"
        HeaderText="<%$ Resources:LibResources, Label205%>" Height="64px" Modal="True" PopupAction="None" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" Width="450px">
        <ContentCollection>
            <dxpc:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlFinanceDetails_esr" runat="server" DefaultButton="btn_new_cust_esr">
                     <asp:Button ID="btn_new_cust_esr" runat="server" Text="<%$ Resources:LibResources, Label331 %>" />
                <table class="style5">
                    <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label3 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList6" runat="server" CssClass="style5">
                                <asp:ListItem Value="O" Text="<%$ Resources:LibResources, Label359 %>"></asp:ListItem>
                                <asp:ListItem Value="R">Revolving</asp:ListItem>
                                <asp:ListItem Value="I">Installment</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal runat="server" Text="<%$ Resources:LibResources, Label204 %>" />:
                        </td>
                        <td>
                            <asp:DropDownList ID="cbo_finance_company_esr" runat="server" CssClass="style5" Width="318px"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                  
                   
                </table>

                    <br />
                
                 <asp:Label ID="lbl_pmt_tp_esr" runat="server" Visible="False" CssClass="style5"></asp:Label>
                <asp:Label ID="lbl_fi_warning_esr" runat="server" CssClass="style5" ForeColor="Red"></asp:Label>
                    
                </asp:Panel>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
    </dxpc:ASPxPopupControl>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
      <table style="width:550px">
            <tr>
                <td style="width:390px; margin-left:5px">
       
                       <span style="color:red"> <asp:Literal ID="litPo" runat="server" Text="<%$ Resources:LibResources, Label1000 %>" Visible="false" ></asp:Literal></span>
                </td>
                <td>
                        <asp:RadioButtonList ID="rbtPo" runat="server" RepeatDirection ="Horizontal" AutoPostBack ="true" Visible="false" ForeColor="red" >
                            <asp:ListItem Value="YES" Selected ="True" Text="<%$ Resources:LibResources, Label674 %>"></asp:ListItem>
                            <asp:ListItem Value ="NO" Text="<%$ Resources:LibResources, Label1001 %>"></asp:ListItem>
                        </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    <br />
    <dx:ASPxLabel ID="lbl_Error" runat="server" Text="" ForeColor="Red">
    </dx:ASPxLabel>
    <br />
    <br />
</asp:Content>
