﻿Imports System.Collections.Generic
Imports System.Data.OracleClient
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports Renci.SshNet
Imports Renci.SshNet.Common
Imports Renci.SshNet.Messages
Imports Renci.SshNet.Channels
Imports Renci.SshNet.Sftp
Partial Class paymentreversal
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_msg.Text = ""

        Session("payment_reversal") = "Y"

        If cust_cd.Text & "" = "" Then   'sabrina R1910
            If Session("cust_cd") & "" <> "" Then
                cust_cd.Text = Session("cust_cd")
            End If
        End If


        If Not IsPostBack Then
            ASPxPopuppaymentreversal.ShowOnPageLoad = False
            Gridview1.DataSource = ""
            Gridview1.DataBind()
        End If


    End Sub
    Public Function gettermid() As String

        Dim v_term_id As String = ""

        If Not Session("clientip") Is Nothing Then
            v_term_id = LeonsBiz.get_term_id(Session("clientip"))
        Else
            v_term_id = LeonsBiz.get_term_id(Request.ServerVariables("REMOTE_ADDR"))
        End If

        Return v_term_id


    End Function
    Public Function getpmtstore() As String

        Dim pmtStore As String = ""

        If Not Session("clientip") Is Nothing Then
            pmtStore = LeonsBiz.GetPaymentStore(Session("clientip"))
        Else ' if not go global then default to remote ip
            pmtStore = LeonsBiz.GetPaymentStore(Request.ServerVariables("REMOTE_ADDR"))
        End If
        If pmtStore & "" = "" Then
            pmtStore = Session("home_store_cd")
        End If

        Return pmtStore

    End Function
    Public Function getdeldocnum() As String
        Dim v_doc As String = ""

        If session("pmt_store_cd") & "" = "" Then
            session("pmt_store_cd") = Session("home_store_cd")
        End If
        If Session("tran_dt") & "" = "" Then
            Session("tran_dt") = Today.Date
        End If

        If Not IsDate(Session("tran_dt")) Then Session("tran_dt") = Today.Date
        Dim sy_soKeyInfo As OrderUtils.SalesOrderKeys = OrderUtils.Get_Next_DocNumInfo(session("pmt_store_cd").ToString, Session("tran_dt"))

        If Not (sy_soKeyInfo Is Nothing OrElse sy_soKeyInfo.soDocNum Is Nothing OrElse sy_soKeyInfo.soDocNum.delDocNum Is Nothing) Then
            If Len(sy_soKeyInfo.soDocNum.delDocNum) <> 11 Then
                Return v_doc
                Exit Function
            End If
        End If

        v_doc = sy_soKeyInfo.soDocNum.delDocNum
        Return v_doc

    End Function
    Protected Sub lookup_payment()


        If sy_chk_security_level1() & "" <> "Y" And sy_chk_security_level2() & "" <> "Y" Then
            lbl_msg.Text = "You are NOT AUTHORIZED to use this function - please exit the screen"
            Session("payment_reversal") = ""
            Exit Sub
        End If

        If cust_cd.Text & "" = "" Then
            lbl_msg.Text = "Enter a valid Customer Code to proceed"
            Exit Sub
        End If

        If LeonsBiz.GetCustomerName(cust_cd.Text) = "" & "" Then
            lbl_msg.Text = "Invalid Customer Code - please try again "
            cust_cd.Focus()
            Exit Sub
        End If

        cust_cd.Text = UCase(cust_cd.Text)
        Session("cust_cd") = UCase(cust_cd.Text)

        If from_dt.SelectedDate.Date = DateTime.MinValue Then
            lbl_msg.Text = "Select From Date to proceed"
            Exit Sub
        End If

        If to_dt.SelectedDate.Date = DateTime.MinValue Then
            lbl_msg.Text = "Select To Date to proceed"
            Exit Sub
        End If

        If FormatDateTime(from_dt.SelectedDate, DateFormat.ShortDate) > FormatDateTime(DateTime.Today, DateFormat.ShortDate) Then
            lbl_msg.Text = "Future From-Date not allowed - please try again "
            Exit Sub
        End If

        If FormatDateTime(from_dt.SelectedDate, DateFormat.ShortDate) < FormatDateTime(to_dt.SelectedDate, DateFormat.ShortDate) Then
            If sy_chk_security_level1() & "" <> "Y" Then
                lbl_msg.Text = "You are only authorized for SAME DAY reversals - please try again "
                Exit Sub
            End If
        End If

        If FormatDateTime(from_dt.SelectedDate, DateFormat.ShortDate) > FormatDateTime(to_dt.SelectedDate, DateFormat.ShortDate) Then
            lbl_msg.Text = "FROM-DATE cannot be greater than TO-DATE - please try again "
            Exit Sub
        End If


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim pymtGItm As DataGridItem
        ds = New DataSet

        Gridview1.DataSource = ""

        'Gridview2.Visible = True

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        'sabrina R1910 - add bnk_crd_num
        ' " AND A.IVC_CD = 'BRICK PAYMENT' " &
        '  " AND A.TRN_TP_CD <> 'BCP' " &
        ' "               AND B.AMT     = A.AMT     )" &
        objSql.CommandText = "SELECT a.IVC_CD IVC_CD, a.TRN_TP_CD TRN_TP_CD, a.MOP_CD MOP_CD, " &
                             " decode(a.mop_cd,'CS','n/a','************'||nvl(SUBSTR(A.BNK_CRD_NUM,13,4),'----')) DB_CARD_NO, " &
                             " A.POST_DT POST_DT, A.AMT AMT,a.des TD_CARD_NO " &
                             " from ar_TRN A " &
                             " where A.cust_cd = :CUST_CD " &
                             " AND A.IVC_CD = 'BRICK PAYMENT' " &
                             " AND A.CO_CD = '" & Session("CO_CD") & "'" &
                             " AND A.TRN_TP_CD = 'PMT' " &
                             " and trunc(A.post_dt) between TO_DATE('" & FormatDateTime(from_dt.SelectedDate.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')" &
                             " AND TO_DATE('" & FormatDateTime(to_dt.SelectedDate.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')" &
                             " AND A.DES IS NOT NULL " &
                             " AND EXISTS (SELECT 'X' FROM AR_TRN B " &
                             "               WHERE B.TRN_TP_CD = 'BCP' " &
                             "               AND B.IVC_CD = 'BRICK PAYMENT' " &
                             "               AND B.CUST_CD = A.CUST_CD " &
                             "               AND B.POST_DT = A.POST_DT " &
                             "               AND B.AMT >= A.AMT)" &
                             " order by post_dt,ivc_cd "

        objSql.Parameters.Add(":CUST_CD", OracleType.VarChar)
        objSql.Parameters(":CUST_CD").Value = Session("cust_cd")
        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()


                Dim i As Integer
                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg.Text = "No Brick Card Payment transactions found for this customer and date range"
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub
    Public Sub print_receipt(ByVal p_delDocNum As String, ByRef p_emp_cd As String, ByRef p_doc_seq_num As String)

        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim v_count As Integer = 1

        Dim co_cd As String = Session("CO_CD")
        Dim ufm As String = "LPO"
        Dim grpCd As String = ""
        'Daniela french UFM
        Dim cult As String = UICulture

        If isNotEmpty(co_cd) Then
            grpCd = LeonsBiz.GetGroupCode(co_cd)
            If grpCd = "BRK" Then
                If InStr(cult.ToLower, "fr") >= 1 Then
                    ufm = "BFC"
                Else
                    ufm = "BRC"
                End If
            End If

        End If

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()


        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_tenderretail_util.print_instpmt_rcpt"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_doc", OracleType.Char)).Value = p_delDocNum
        myCMD.Parameters.Add(New OracleParameter("p_signOnUsrCd", OracleType.Char)).Value = p_emp_cd
        myCMD.Parameters.Add(New OracleParameter("p_doc_seq_num", OracleType.Char)).Value = p_doc_seq_num
        myCMD.Parameters.Add(New OracleParameter("p_pmt_str", OracleType.Char)).Value = Session("pmt_store_cd")
        myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.Char)).Value = Session("cust_cd")

        'Daniela add french
        'Dim cult As String = UICulture
        Dim lang As String = "E"
        If InStr(cult.ToLower, "fr") >= 1 Then
            lang = "F"
        End If
        myCMD.Parameters.Add(New OracleParameter("p_lang", OracleType.Char)).Value = lang
        'End Daniela

        Try

            'MyDA.Fill(Ds)
            myCMD.ExecuteNonQuery()

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString         
        Finally 'Daniela
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
        End Try
    End Sub
    Public Function total_refund_amt(ByVal p_cust_cd As String, ByVal p_amt As String, ByVal p_mop As String) As Double


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select nvl(sum(ar.amt),0) V_AMT "
        sql = sql & " from ar_trn ar  "
        sql = sql & " where ar.trn_tp_cd = 'BCR' "           'sabrina R1910 check for BCR instead of R
        sql = sql & " and ar.mop_cd = '" & p_mop & "'"
        sql = sql & " and ar.co_cd = '" & Session("CO_CD") & "'"
        sql = sql & " and ar.cust_cd = '" & p_cust_cd & "'"
        sql = sql & " and ar.des = '" & Session("td_card_no") & "'"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_AMT").ToString
            Else
                Return 0
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function
    Public Function total_pmt_amt(ByVal p_cust_cd As String, ByVal p_mop As String) As Double


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select NVL(SUM(AR.AMT),0) V_AMT "
        sql = sql & " from ar_trn ar  "
        sql = sql & " where ar.trn_tp_cd = 'PMT' "           'sabrina R1910 check for BCR instead of R
        sql = sql & " and ar.mop_cd = '" & p_mop & "'"
        sql = sql & " and ar.co_cd = '" & Session("CO_CD") & "'"
        sql = sql & " and ar.cust_cd = '" & p_cust_cd & "'"
        sql = sql & " and ar.des = '" & Session("td_card_no") & "'"
        sql = sql & " and ar.ivc_cd = 'BRICK PAYMENT' "
        sql = sql & " and trunc(ar.post_dt) between TO_DATE('" & FormatDateTime(from_dt.SelectedDate.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')"
        sql = sql & " and TO_DATE('" & FormatDateTime(to_dt.SelectedDate.ToString, DateFormat.ShortDate) & "','mm/dd/RRRR')"
        sql = sql & " and exists (SELECT 'X' FROM AR_TRN B "
        sql = sql & "               WHERE B.TRN_TP_CD = 'BCP' "
        sql = sql & "               AND B.IVC_CD = 'BRICK PAYMENT' "
        sql = sql & "               AND B.CUST_CD = ar.CUST_CD "
        sql = sql & "               AND B.POST_DT = ar.POST_DT "
        sql = sql & "               AND B.AMT >= ar.AMT)"


        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_AMT").ToString
            Else
                Return 0
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function
    Public Function check_if_reversed_already(ByVal p_cust_cd As String, ByVal p_amt As String, ByVal p_mop As String) As String
        'sabrina CNCREFUND add
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select 'Y' as V_EXISTS "
        sql = sql & " from ar_trn ar  "
        sql = sql & " where ar.trn_tp_cd = 'BCR' "           'sabrina R1910 check for BCR instead of R
        sql = sql & " and ar.mop_cd = '" & p_mop & "'"
        sql = sql & " and ar.co_cd = '" & Session("CO_CD") & "'"
        sql = sql & " and ar.cust_cd = '" & p_cust_cd & "'"
        sql = sql & " and nvl(ar.amt,0) = " & p_amt


        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_EXISTS").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()

    End Function

    Protected Sub search_btn_Click(sender As Object, e As EventArgs) Handles search_btn.Click
        lookup_payment()
    End Sub
    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) 'Handles Gridview1.DeleteCommand

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim v_mop_cd As String = Left(e.Item.Cells(2).Text, 4)
        Dim v_refund_amt As String = Left(e.Item.Cells(5).Text, 4)   'sabrina R1910
        Dim v_td_card_no As String = Left(e.Item.Cells(6).Text, 16)  'sabrina R1910
        Dim v_dc_card_no As String = Left(e.Item.Cells(3).Text, 4)  'sabrina R1910
        Dim v_cust_cd As String = UCase(Session("cust_cd"))
        Dim v_total_refund As Double
        Dim v_total_pmt As Double


        Session("refund_amt") = v_refund_amt
        Session("td_card_no") = v_td_card_no
        Session("dc_card_no") = "************" & v_dc_card_no

        If v_mop_cd & "" <> "" Then
            If (v_mop_cd = "CS" Or v_mop_cd = "DC") Then
                Session("mop_cd") = v_mop_cd
            Else
                lbl_msg.Text = "Invalid MOP - Only Cash or Debit Card can be processed"
                Exit Sub
            End If
        Else
            lbl_msg.Text = "Invalid MOP - Only Cash or Debit Card can be processed"
            Exit Sub
        End If

        payr_amt.Text = v_refund_amt

        v_total_refund = total_refund_amt(v_cust_cd, v_refund_amt, v_mop_cd)
        v_total_pmt = total_pmt_amt(v_cust_cd, v_mop_cd)

        If v_total_refund >= v_total_pmt Then
            If v_total_pmt - v_total_refund <= CDbl(v_refund_amt) Then
                lbl_msg.Text = "Cannot proceed - Total Refund amount of $" & v_total_refund & " already exists - refund of $" & v_refund_amt & " not allowed"
                Exit Sub
            End If
        End If




        Dim pmtStore As String = getpmtstore()
        Session("pmt_store_cd") = pmtStore

        Dim v_store As StoreData = theSalesBiz.GetStoreInfo(UCase(Session("HOME_STORE_CD").ToString))
        Session("CO_CD") = v_store.coCd
        Session("dc_auth_no") = ""

        Dim str_del_doc_num As String = getdeldocnum()
        If str_del_doc_num & "" = "" Then
            lbl_msg.Text = "<font color=red>Could not create order, please try again</font>"
            Exit Sub
        End If

        Dim doc_seq_num As String = Right(str_del_doc_num, 4)
        Session("del_doc_num") = str_del_doc_num
        Session("doc_seq_num") = Right(str_del_doc_num, 4)

        Dim v_cash_auth As String = ""
        Dim v_crn As String = ""

        If v_mop_cd = "CS" Then
            'sabrina R1910 dim v_mop As String = ""
            If sy_chk_security_level1() & "" <> "Y" Then  'add R type only for store level sec.
                add_ar_Trn(v_cash_auth, v_crn, "R", v_crn, v_mop_cd)  'sabrina R1910
            End If

            process_td_payment_reversal()
        Else
            If sy_chk_security_level1() & "" <> "Y" Then   'sabrina R1910 - popup for store level sec.
                commit_btn.Enabled = True
                ASPxPopuppaymentreversal.ShowOnPageLoad = True

            Else

                process_td_payment_reversal()

            End If

        End If


    End Sub
    Public Sub add_ar_Trn(ByVal p_aut As String, ByVal p_crn As String, ByVal p_trn_tp_cd As String, ByVal p_des As String, ByVal p_mop As String)
        Dim merchant_id As String = ""
        Dim sql As String

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim cmdAddRecords As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim v_amt As Double

        Dim v_emp_init As String = ""
        Dim v_csh_dwr As String = ""

        v_emp_init = LeonsBiz.GetEmpInit(Session("emp_cd"))
        v_csh_dwr = LeonsBiz.get_csh_dwr(v_emp_init, Session("CO_CD"))

        Dim pmtStore As String = getpmtstore()


        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "insert into ar_trn (CO_CD,CUST_CD,MOP_CD,EMP_CD_CSHR,EMP_CD_OP,ORIGIN_STORE,CSH_DWR_CD,TRN_TP_CD,IVC_CD,"
        sql = sql & "BNK_CRD_NUM,DOC_SEQ_NUM,AMT,POST_DT,STAT_CD,AR_TP,APP_CD,DES,PMT_STORE,ORIGIN_CD,CREATE_DT,AR_TRN_PK) "
        sql = sql & " values (:CO_CD,:CUST_CD,:MOP_CD,:EMP_CD_CSHR,:EMP_CD_OP,:ORIGIN_STORE,:CSH_DWR_CD,:TRN_TP_CD,:IVC_CD,"
        sql = sql & ":BNK_CRD_NUM,:DOC_SEQ_NUM,:AMT,TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'),"
        sql = sql & ":STAT_CD,:AR_TP,:APP_CD,:DES,:PMT_STORE,:ORIGIN_CD,TO_DATE('" & FormatDateTime(Today.Date, DateFormat.ShortDate) & "','mm/dd/RRRR'),"
        sql = sql & " seq_ar_trn.nextval)"

        cmdAddRecords.Parameters.Clear()

        Try
            If Not IsNothing(Session.SessionID.ToString.Trim) Then
                With cmdAddRecords
                    .Connection = conn
                    .CommandText = sql.ToString

                    cmdAddRecords.Parameters.Add(":CO_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CO_CD").Value = Session("CO_CD")

                    cmdAddRecords.Parameters.Add(":CUST_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CUST_CD").Value = Session("cust_cd")

                    cmdAddRecords.Parameters.Add(":MOP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":MOP_CD").Value = p_mop

                    cmdAddRecords.Parameters.Add(":EMP_CD_CSHR", OracleType.VarChar)
                    cmdAddRecords.Parameters(":EMP_CD_CSHR").Value = Session("emp_cd").ToString.Trim

                    cmdAddRecords.Parameters.Add(":EMP_CD_OP", OracleType.VarChar)
                    cmdAddRecords.Parameters(":EMP_CD_OP").Value = Session("emp_cd").ToString.Trim

                    cmdAddRecords.Parameters.Add(":ORIGIN_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters(":ORIGIN_STORE").Value = Session("HOME_STORE_CD").ToString.Trim

                    cmdAddRecords.Parameters.Add(":CSH_DWR_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":CSH_DWR_CD").Value = v_csh_dwr

                    cmdAddRecords.Parameters.Add(":TRN_TP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":TRN_TP_CD").Value = p_trn_tp_cd

                    cmdAddRecords.Parameters.Add(":IVC_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":IVC_CD").Value = "BRICK PAYMENT"

                    cmdAddRecords.Parameters.Add(":BNK_CRD_NUM", OracleType.VarChar)
                    cmdAddRecords.Parameters(":BNK_CRD_NUM").Value = Session("dc_card_no")

                    cmdAddRecords.Parameters.Add(":DOC_SEQ_NUM", OracleType.VarChar)
                    cmdAddRecords.Parameters(":DOC_SEQ_NUM").Value = Session("doc_seq_num")

                    If IsNumeric(Session("refund_amt")) Then
                        v_amt = CDbl(Session("refund_amt"))
                    Else
                        v_amt = 0
                    End If

                    cmdAddRecords.Parameters.Add(":AMT", OracleType.Number)
                    cmdAddRecords.Parameters(":AMT").Value = v_amt

                    cmdAddRecords.Parameters.Add(":STAT_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":STAT_CD").Value = "T"

                    cmdAddRecords.Parameters.Add(":AR_TP", OracleType.VarChar)
                    cmdAddRecords.Parameters(":AR_TP").Value = "O"

                    cmdAddRecords.Parameters.Add(":APP_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":APP_CD").Value = p_aut

                    cmdAddRecords.Parameters.Add(":DES", OracleType.VarChar)
                    cmdAddRecords.Parameters(":DES").Value = p_des

                    cmdAddRecords.Parameters.Add(":PMT_STORE", OracleType.VarChar)
                    cmdAddRecords.Parameters(":PMT_STORE").Value = pmtStore

                    cmdAddRecords.Parameters.Add(":ORIGIN_CD", OracleType.VarChar)
                    cmdAddRecords.Parameters(":ORIGIN_CD").Value = "RBP"

                End With
                cmdAddRecords.ExecuteNonQuery()

            End If
            conn.Close()

        Catch ex As Exception
            conn.Close()
            Throw ex
        Finally
            conn.Close()
        End Try

    End Sub

    Public Sub process_td_payment_reversal()
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_ack As String

        Dim v_sub_type As String
        Dim v_card_num As String = debit_card_no.Text
        Dim v_cust_cd As String = cust_cd.Text
        Dim v_expire_date As String = ""
        Dim v_amt As String = CDbl(payr_amt.Text)
        Dim v_auth_no As String = ""
        Dim v_financetp As String = " "
        Dim l_filename As String
        Dim l_printcmd As String
        Dim str_s_or_m As String = ""
        Dim str_correction As String = "Y"  'this indicates payment reversal/correction
        Dim str_pmt_tp = "PMT"
        Dim sessionid As String = Session.SessionID.ToString.Trim

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()
        v_sub_type = "8"

        Dim v_term_id As String = gettermid
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()
        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_brk_pos1.sendsubTp8_td"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = Session("td_card_no")
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = "1249"
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_financetp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = "PMT"
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = ""
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = ""
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = ""
            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = v_cust_cd
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = "BRICK PAYMENT"

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

            myCMD.ExecuteNonQuery()

            l_dsp = myCMD.Parameters("p_dsp").Value

            If InStr(myCMD.Parameters("p_dsp").Value, "Approved") > 0 Then
                If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                    l_aut = myCMD.Parameters("p_aut").Value
                Else
                    l_aut = "empty"
                End If
                l_ack = myCMD.Parameters("p_ackInd").Value
                l_res = myCMD.Parameters("p_res").Value
                l_dsp = myCMD.Parameters("p_dsp").Value

                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                End If

                l_crn = myCMD.Parameters("p_crn").Value
                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value

                Session("str_print_cmd") = l_printcmd

                add_ar_Trn(l_aut, Session("dc_card_no"), "BCR", l_crn, Session("mop_cd")) 'sabrina R1910 
                print_receipt("BRICK PAYMENT", Session("emp_cd"), Session("doc_seq_num"))
                ASPxPopuppaymentreversal.ShowOnPageLoad = False
                lbl_msg.Text = "Payment Reversal is complete - Authorization# " & l_aut & Session("dc_auth_no")
                Session("payment_reversal") = ""
            Else
                lbl_msg.Text = "Payment Reversal failed - " & l_dsp
            End If

        Catch
            Throw
        Finally
            Try 'Daniela close
                myCMD.Cancel()
                myCMD.Dispose()
                objConnection.Close()
                objConnection.Dispose()
            Catch
            End Try
        End Try

    End Sub
    Public Sub process_debit_card_refund(ByVal p_amt As String)
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()
        Dim l_res As String
        Dim l_dsp As String
        Dim l_crn As String
        Dim l_crt As String
        Dim l_exp As String
        Dim l_tr2 As String
        Dim l_aut As String
        Dim l_rct As String
        Dim l_ack As String

        Dim v_dis_line_1 As String
        Dim v_dis_line_2 As String
        Dim v_reserve As String
        Dim v_term_id As String

        Dim v_rpt_name As String
        Dim v_trans_type As String
        Dim v_sub_type As String = "8"
        Dim v_card_num As String
        Dim v_expire_date As String
        Dim v_amt As String = p_amt
        Dim v_trans_no As String
        Dim v_auth_num As String
        Dim v_track2 As String
        Dim v_auth_no As String
        Dim v_oth As String
        Dim v_length As Integer
        Dim v_response As String
        Dim v_mop_cd As String = "DC"
        Dim v_private_lbl_finance_tp As String = ""
        Dim l_filename As String
        Dim l_printcmd As String
        Dim v_count As Integer
        Dim str_sp_approved As String = "N"
        Dim str_auth As String
        Dim str_app_cd As String
        Dim str_s_or_m As String = ""
        Dim str_correction As String = ""
        Dim str_pmt_tp As String = "R"


        v_term_id = gettermid()
        If v_term_id = "empty" Then
            popup_msg.Text = " Error - No Designated Pinpad"
            Exit Sub
        End If

        Dim sessionid As String = Session.SessionID.ToString.Trim


        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString  'Lucy
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_tenderretail_pos.sendsubtp8"
            myCMD.CommandType = CommandType.StoredProcedure
            myCMD.Parameters.Add(New OracleParameter("p_crdnum", OracleType.VarChar)).Value = ""
            myCMD.Parameters.Add(New OracleParameter("p_expDt", OracleType.VarChar)).Value = ""
            myCMD.Parameters.Add(New OracleParameter("p_amt", OracleType.VarChar)).Value = v_amt
            myCMD.Parameters.Add(New OracleParameter("p_termID", OracleType.VarChar)).Value = v_term_id
            myCMD.Parameters.Add(New OracleParameter("p_subTp", OracleType.VarChar)).Value = v_sub_type
            myCMD.Parameters.Add(New OracleParameter("p_financetp", OracleType.VarChar)).Value = v_private_lbl_finance_tp
            myCMD.Parameters.Add(New OracleParameter("p_refund_ind", OracleType.VarChar)).Value = str_pmt_tp
            myCMD.Parameters.Add(New OracleParameter("p_swipe_ind", OracleType.VarChar)).Value = str_s_or_m
            myCMD.Parameters.Add(New OracleParameter("p_mop_cd", OracleType.VarChar)).Value = v_mop_cd
            myCMD.Parameters.Add(New OracleParameter("p_correction", OracleType.VarChar)).Value = str_correction
            myCMD.Parameters.Add(New OracleParameter("p_app_cd", OracleType.VarChar)).Value = ""
            myCMD.Parameters.Add(New OracleParameter("p_orig_auth", OracleType.VarChar)).Value = ""

            myCMD.Parameters.Add(New OracleParameter("p_sessionid", OracleType.VarChar)).Value = sessionid
            myCMD.Parameters.Add(New OracleParameter("p_cust_cd", OracleType.VarChar)).Value = Session("cust_cd")
            myCMD.Parameters.Add(New OracleParameter("p_doc_num", OracleType.VarChar)).Value = "BRICK PAYMENT"

            myCMD.Parameters.Add("p_ackInd", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_authNo", OracleType.VarChar, 20).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_transTp", OracleType.VarChar, 10).Direction = ParameterDirection.Output


            myCMD.Parameters.Add("p_aut", OracleType.VarChar, 10).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_dsp", OracleType.VarChar, 200).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_crn", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_exp", OracleType.VarChar, 11).Direction = ParameterDirection.Output

            myCMD.Parameters.Add("p_res", OracleType.VarChar, 10).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_tr2", OracleType.VarChar, 100).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_outfile", OracleType.VarChar, 50).Direction = ParameterDirection.Output
            myCMD.Parameters.Add("p_cmd", OracleType.VarChar, 200).Direction = ParameterDirection.Output

            myCMD.ExecuteNonQuery()

        Catch ex As Exception

            popup_msg.Text = "Reversal unsuccessful - please contact Accounting for Assistance" & ex.ToString
            objConnection.Close()
            Exit Sub

            ' Throw
        End Try

        l_res = myCMD.Parameters("p_res").Value
        v_auth_no = myCMD.Parameters("p_authNo").Value  'this is auth_no, not the approved auth, it is 10 spaces or combination of auth.txt or app_cd with spaces
        v_trans_no = myCMD.Parameters("p_transTp").Value
        l_dsp = myCMD.Parameters("p_dsp").Value

        If Left(l_res, 5) = "1[49]" Or Left(l_res, 5) = "1[50]" Then
            str_sp_approved = "Y"

            If IsDBNull(myCMD.Parameters("p_aut").Value) = False Then
                l_aut = myCMD.Parameters("p_aut").Value
            Else
                l_aut = "empty"  ' this is approved auth, using empty to replace null value
            End If

            Session("dc_auth_no") = l_aut

            If l_aut <> "empty" Then
                l_ack = myCMD.Parameters("p_ackInd").Value
                If IsDBNull(myCMD.Parameters("p_tr2").Value) = False Then    'swipe
                    l_tr2 = myCMD.Parameters("p_tr2").Value
                    debit_card_no.Text = Left(l_tr2, 16)
                    l_exp = myCMD.Parameters("p_exp").Value
                End If

                l_filename = myCMD.Parameters("p_outfile").Value
                l_printcmd = myCMD.Parameters("p_cmd").Value

            End If

            If sy_chk_security_level1() & "" <> "Y" Then 'add R type only for store user sec.
                add_ar_Trn(l_aut, debit_card_no.Text, "R", Session("td_card_no"), Session("mop_cd"))
            End If

            process_td_payment_reversal()


        Else
            popup_msg.Text = "Reversal unsuccessful - please contact Accounting for Assistance " & l_dsp
        End If
        'Daniela close
        Try
            myCMD.Cancel()
            myCMD.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        Catch
        End Try

    End Sub
    Function sy_chk_security_level1() As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoInsert('" & Session("emp_cd") & "','paymentreversal.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_RES").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Function
    Function sy_chk_security_level2() As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()

        sql = "select std_web_security.isOKtoupdate('" & Session("emp_cd") & "','paymentreversal.aspx') V_RES from dual "

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            If MyDatareader2.Read Then
                Return MyDatareader2.Item("V_RES").ToString
            Else
                Return "N"
            End If

        Catch
            conn.Close()
            Throw
        End Try

        conn.Close()
    End Function

    Protected Sub commit_btn_Click(sender As Object, e As EventArgs) Handles commit_btn.Click

        process_debit_card_refund(payr_amt.Text)

    End Sub

 
End Class
