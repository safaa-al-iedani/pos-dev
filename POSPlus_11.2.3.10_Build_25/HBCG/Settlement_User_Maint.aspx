<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Settlement_User_Maint.aspx.vb" Inherits="Settlement_User_Maint" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="95%" bgcolor="White">
        <tr>
            <td height="100%" align="center" valign="middle">
                <div style="padding-left: 18px; padding-top: 10px" id="main_body" runat="server">
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="User Maintenance">
                    </dx:ASPxLabel>
                    <dx:ASPxComboBox ID="cbo_new_users" runat="server" IncrementalFilteringMode ="StartsWith">
                    </dx:ASPxComboBox>
                    <dx:ASPxButton ID="btn_add_user" runat="server" Text="Add User">
                    </dx:ASPxButton>
                    <br />
                    <dx:ASPxLabel ID="lbl_errors" runat="server" Text="" ForeColor="Red">
                    </dx:ASPxLabel>
                    <br />
                    <asp:DataGrid ID="Gridview1" runat="server" OnDeleteCommand="DoItemDelete" AutoGenerateColumns="False"
                        CellPadding="2" DataKeyField="emp_cd" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                        Font-Strikeout="False" Font-Underline="False" Height="144px" Width="643px" HeaderStyle-Height="20px"
                        HeaderStyle-HorizontalAlign="Left" CssClass="style5">
                        <AlternatingItemStyle BackColor="Beige" Font-Italic="False" Font-Strikeout="False"
                            Font-Underline="False" Font-Overline="False" Font-Bold="False"></AlternatingItemStyle>
                        <HeaderStyle Font-Italic="False" Font-Strikeout="False" Font-Underline="False" Font-Overline="False"
                            Font-Bold="False" Height="20px" HorizontalAlign="Center" CssClass="style5"></HeaderStyle>
                        <Columns>
                            <asp:BoundColumn DataField="emp_cd" HeaderText="User ID" ReadOnly="True">
                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" />
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="User Maint">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="Update_User" />
                                </ItemTemplate>
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Promo Maint">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="True" OnCheckedChanged="Update_Promo" />
                                </ItemTemplate>
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Admin Access">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox3" runat="server" AutoPostBack="True" OnCheckedChanged="Update_Admin" />
                                </ItemTemplate>
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MOP Maint">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox4" runat="server" AutoPostBack="True" OnCheckedChanged="Update_MOP" />
                                </ItemTemplate>
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="USER_MAINT" Visible="False" />
                            <asp:BoundColumn DataField="PROMO_MAINT" Visible="False" />
                            <asp:BoundColumn DataField="ADMIN_ACCESS" Visible="False" />
                            <asp:BoundColumn DataField="MOP_MAINT" Visible="False" />
                            <asp:ButtonColumn CommandName="Delete" Text="&lt;img src='images/Trashcan_30.png' border='0'&gt;">
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" VerticalAlign="Middle" CssClass="style5" HorizontalAlign="Center" />
                            </asp:ButtonColumn>
                        </Columns>
                        <ItemStyle VerticalAlign="Top" CssClass="style5" Font-Bold="False" Font-Italic="False"
                            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle" style="height: 19px">
                <asp:Label ID="lbl_header" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
