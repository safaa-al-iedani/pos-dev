'Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient
Imports System.Linq


Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization

Partial Class CommRate
    Inherits POSBasePage
    Public Const gs_version As String = "Version 2.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen.
    '   2.0:
    '   (1) French implementation
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : Page Init Event Handler
    ''' Loads store Group/Store combo box based on logged on users company group code.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init





        populate_search_table()


        btn_search.Enabled = True
        btn_clr_screen.Enabled = True

        btn_update.Visible = False
    End Sub

    Protected Sub populate_search_table()



        DropDown_table.Items.Add("PROMOTION")
        DropDown_table.Items.Add("REGULAR")
        '  DropDown_table.Items.Add("BOTH")


        DropDownDt.Items.Add("Current")
        DropDownDt.Items.Add("Expired")
        DropDownDt.Items.Add("Both")

    End Sub
    
    



    Protected Sub click_btn_search(ByVal sender As Object, ByVal e As System.EventArgs)



        lbl_warning.Text = Nothing



        lbl_msg.Text = Nothing

        reload()
        btn_update.Enabled = True


    End Sub

    Private Sub populate_txt_box()

        Dim dgItem As DataGridItem
        Dim drpdwn As TextBox
     
        Dim l_txtRowId As TextBox
        Dim l_txt_prod_grp As TextBox
        Dim l_txt_min_pcnt As TextBox
        Dim l_txt_start_date As TextBox
        Dim l_txt_end_date As TextBox


        Dim txtRowId As String
        Dim strYes As String
        Dim txProd_grp As String
        Dim txtMin_pcnt As String
        Dim txtSart_date As String
        Dim txtEnd_date As String
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)
        Dim dt_end_date As Date
        Dim dt_start_date As Date
        'iterate through all the rows  
        For i As Integer = 0 To GV_SRR.Items.Count - 1
            dgItem = GV_SRR.Items(i)


            l_txtRowId = CType(GV_SRR.Items(i).FindControl("rowId"), TextBox)
            l_txt_prod_grp = CType(GV_SRR.Items(i).FindControl("prod_grp"), TextBox)
            l_txt_min_pcnt = CType(GV_SRR.Items(i).FindControl("txt_min_pcnt"), TextBox)
            l_txt_start_date = CType(GV_SRR.Items(i).FindControl("txt_start_date"), TextBox)
            l_txt_start_date = CType(GV_SRR.Items(i).FindControl("txt_start_date"), TextBox)
            l_txt_end_date = CType(GV_SRR.Items(i).FindControl("txt_end_date"), TextBox)

            txtRowId = dgItem.Cells(0).Text
            txProd_grp = dgItem.Cells(1).Text
            txtMin_pcnt = dgItem.Cells(2).Text


            txtSart_date = dgItem.Cells(3).Text
            txtEnd_date = dgItem.Cells(4).Text
 

            If isEmpty(txtMin_pcnt) Then
                txtMin_pcnt = Nothing
            End If

            If isEmpty(txtSart_date) Then
                txtSart_date = Nothing
            End If

            If isEmpty(txtEnd_date) Then
                txtEnd_date = Nothing
            End If

            l_txt_min_pcnt.Text = txtMin_pcnt
            l_txt_start_date.Text = txtSart_date
            l_txt_end_date.Text = txtEnd_date

            If IsDate(txtSart_date) Then
                l_txt_start_date.Text = FormatDateTime(txtSart_date, DateFormat.ShortDate)
                dt_start_date = l_txt_start_date.Text
                If dt_start_date <= l_today Then
                    l_txt_min_pcnt.Enabled = False
                    l_txt_start_date.Enabled = False
                End If
            End If
            If IsDate(txtEnd_date) Then
                l_txt_end_date.Text = FormatDateTime(txtEnd_date, DateFormat.ShortDate)
                dt_end_date = l_txt_end_date.Text
                If dt_end_date < l_today Then
                    l_txt_min_pcnt.Enabled = False
                    l_txt_start_date.Enabled = False
                    l_txt_end_date.Enabled = False
                
                ElseIf dt_end_date < dt_start_date Then
                    ' l_txt_end_date.Text = FormatDateTime("12/31/2049", DateFormat.ShortDate)
                    lbl_msg.Text = "Start date must be ealier than end Date"

                End If
                If dt_end_date = l_max_dt And (txProd_grp = "1FURN" Or txProd_grp = "2APPL" Or txProd_grp = "3ELEC" Or txProd_grp = "4MATT") Then
                    l_txt_end_date.Enabled = True
                    l_txt_min_pcnt.Enabled = True
                    ' l_txt_start_date.Enabled = False
                End If
            End If
            ' strYes = check_past_date(txtRowId)


        Next




    End Sub
    Public Function check_date(ByRef p_start_date As Date, ByRef p_end_date As Date)


        Dim txtStart_date As Date
        Dim txtEnd_date As Date
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_str As String = "Y"
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)
        txtStart_date = p_start_date
        txtEnd_date = p_end_date

        lbl_warning.Text = Nothing

        If Not (isEmpty(txtStart_date)) And Not (IsDate(txtStart_date)) Then
            If txtStart_date < l_today Then
                lbl_warning.Text = "Past date is invalid "

                l_str = "N"
            ElseIf txtStart_date >= l_max_dt Then
                lbl_warning.Text = "Start date is invalid "
                btn_update.Enabled = False
                l_str = "N"
            End If
        End If
        If (isEmpty(txtEnd_date)) Or Not (IsDate(txtEnd_date)) Then

            lbl_warning.Text = "Invalid date"
            btn_update.Enabled = False
            l_str = "N"
        ElseIf (IsDate(txtEnd_date)) Then


            If txtEnd_date < l_today Then
                lbl_warning.Text = "Past date is invalid "
                btn_update.Enabled = False
                l_str = "N"
            ElseIf txtEnd_date > l_max_dt Then
                lbl_warning.Text = "End date is invalid "
                btn_update.Enabled = False
                l_str = "N"
            ElseIf txtEnd_date < txtStart_date Then
                lbl_warning.Text = "End date cannot be earlier than start date "
                btn_update.Enabled = False
                l_str = "N"
            
            End If


        End If
        Return l_str
    End Function
    Protected Sub txt_start_date_TextChanged(sender As Object, e As EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim l_txt_start_date As TextBox
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_tmrw As Date = l_today.AddDays(1)
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)
        Dim txtStart_date As Date
        Dim l_txt_end_date As TextBox



        Dim txtEnd_date As Date

        lbl_warning.Text = Nothing
     

        l_txt_start_date = CType(dgItem.FindControl("txt_start_date"), TextBox)
        If Not isEmpty(l_txt_start_date.Text) And IsDate(l_txt_start_date.Text) Then
            txtStart_date = l_txt_start_date.Text

        Else
            lbl_warning.Text = "Invalid date"
            Exit Sub
        End If


        If Not (isEmpty(txtStart_date)) Then
            If Not (IsDate(txtStart_date)) Then

                lbl_warning.Text = "Invalid date"
                Exit Sub
            ElseIf txtStart_date < l_today Then
                lbl_warning.Text = "Past date is invalid "
                Exit Sub
            ElseIf txtStart_date >= l_max_dt Then
                lbl_warning.Text = "Start date is invalid "
                Exit Sub
            End If
        

        End If

    End Sub

    Protected Sub txt_end_date_TextChanged(sender As Object, e As EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim l_txt_end_date As TextBox
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)


        Dim txtEnd_date As String
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_tmrw As Date = l_today.AddDays(1)
        Dim l_txt_start_date As TextBox
        Dim txtStart_date As String
        Dim dtStart_date As Date
        Dim dtEnd_date As Date
        Dim strYes As String




        l_txt_end_date = CType(dgItem.FindControl("txt_end_date"), TextBox)

        txtEnd_date = l_txt_end_date.Text


        If Not isEmpty(l_txt_end_date.Text) And IsDate(l_txt_end_date.Text) Then
            txtEnd_date = l_txt_end_date.Text
            dtEnd_date = txtEnd_date
        Else
            lbl_warning.Text = "Invalid date"
            Exit Sub
        End If
        
        If txtEnd_date < l_today Then
            lbl_warning.Text = "Past date is invalid "
            Exit Sub
        ElseIf txtEnd_date > l_max_dt Then
            lbl_warning.Text = "End date is invalid "
            Exit Sub
        End If
    End Sub

    Protected Sub txt_min_pcnt_TextChanged(sender As Object, e As EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)

        Dim l_txt_min_pcnt As TextBox
      

        Dim txtMin_pcnt As String
       
      
        l_txt_min_pcnt = CType(dgItem.FindControl("txt_min_pcnt"), TextBox)

        txtMin_pcnt = l_txt_min_pcnt.Text


      

        lbl_warning.Text = Nothing
        If (Not isEmpty(txtMin_pcnt)) Then
            If Not (IsNumeric(txtMin_pcnt)) Then

                lbl_warning.Text = txtMin_pcnt + "is invalid "
                Exit Sub
            ElseIf txtMin_pcnt > 100 Or txtMin_pcnt < 0 Then
                lbl_warning.Text = txtMin_pcnt + "is invalid "
                Exit Sub
            Else
                btn_update.Enabled = True
            End If
        Else
            btn_update.Enabled = True
        End If

    End Sub
    Protected Sub text_min_pcnt_TextChanged(sender As Object, e As EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)





        Dim l_min_pcnt As String

        Dim txtMin_pcnt As Double
        l_min_pcnt = text_min_pcnt.Text

        txtMin_pcnt = l_min_pcnt


        lbl_warning.Text = Nothing
        If (Not isEmpty(txtMin_pcnt)) Then
            If Not (IsNumeric(txtMin_pcnt)) Then

                lbl_warning.Text = txtMin_pcnt + "is invalid "

            ElseIf txtMin_pcnt > 100 Or txtMin_pcnt < 0 Then
                lbl_warning.Text = txtMin_pcnt + "is invalid "

            Else
                btn_update.Enabled = True
            End If
        Else
            btn_update.Enabled = True
        End If

    End Sub
    Protected Sub reload()

        Dim l_id As String




        Dim l_prod_grp As String
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)

        ' Dim DDL As DropDownList = CType(e.Item.FindControl("dropdownstore"), DropDownList)

     
        'If isEmpty(l_prod_grp) Then
        'lbl_msg.Text = "Please enter a value to prod grp"
        ' Exit Sub
        '  End If

        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            ds = New DataSet
            GV_SRR.DataSource = ""

            Dim l_date_r As String
            l_date_r = DropDownDt.SelectedItem.Value

            l_prod_grp = LTrim(RTrim(Text_prod_grp.Text))
            Dim l_table As String
            l_table = DropDown_table.SelectedItem.Value

            sqlString = New StringBuilder("SELECT  ")
            sqlString.Append(" rowid,prod_grp ,min_percent, start_date, end_date")
            If l_table = "REGULAR" Then
                sqlString.Append(" FROM drgm ")
            Else
                sqlString.Append(" FROM drgmp ")
            End If
            Dim str_min_pcnt As String

            str_min_pcnt = text_min_pcnt.Text

            Dim l_min_pcnt As Double
            If l_date_r = "Current" Then
                sqlString.Append(" WHERE end_date >= TO_DATE('" & FormatDateTime(l_today, 2) & "','mm/dd/yyyy') ")
            ElseIf l_date_r = "Expired" Then
                sqlString.Append(" WHERE end_date < TO_DATE('" & FormatDateTime(l_today, 2) & "','mm/dd/yyyy') ")
            Else
                sqlString.Append(" WHERE end_date =end_date ")
            End If

            If Len(l_prod_grp) > 0 Then

                sqlString.Append(" and prod_grp like '" & UCase(l_prod_grp) & "'")
                
            End If

            If SystemUtils.isNumber(str_min_pcnt) Then

                l_min_pcnt = str_min_pcnt

                sqlString.Append(" and min_percent='" & l_min_pcnt & "'")
            End If

            Dim l_eff_dt As Date
            Dim l_end_dt As Date




            l_eff_dt = can_eff_date.SelectedDate

            l_end_dt = can_end_date.SelectedDate


            If l_eff_dt <> DateTime.MinValue And IsDate(l_eff_dt) Then


                sqlString.Append("  and start_date =TO_DATE('" & FormatDateTime(l_eff_dt, 2) & "','mm/dd/yyyy') ")


            End If

            If l_end_dt <> DateTime.MinValue And IsDate(l_end_dt) Then

                sqlString.Append("  and end_date =TO_DATE('" & FormatDateTime(l_end_dt, 2) & "','mm/dd/yyyy') ")


            End If


            sqlString.Append(" order by prod_grp,start_date, end_date,min_percent")
            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()

            '   If Not isEmpty(l_prod_grp) Or Not IsDBNull(l_prod_grp) Then
            'sqlString.Append(" WHERE prod_grp=l_prod_grp")
            ' objcmd.Parameters.Add(":l_prod_grp", OracleType.VarChar)
            ' objcmd.Parameters(":l_prod_grp").Value = l_prod_grp
            ' End If
            '  If CheckBox_end_dt.Checked = True Then
            'sqlString.Append(" and trunc(end_date) ='31-DEC-2049' ")
            '   End If

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


            objAdaptor.Fill(ds)


            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count




            If (MyDataReader.Read()) Then
                GV_SRR.Enabled = True
                GV_SRR.Visible = True
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    Dim l_va As String
                    Dim l_st As String






                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("prod_grp")) Then
                            err_cnt = 1
                        End If

                        
                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)
                btn_update.Visible = True



            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()
                lbl_msg.Text = "No data found."
                GV_SRR.Enabled = False
                GV_SRR.Visible = False
                btn_update.Visible = False
            Else
                If l_table = "REGULAR" Then
                    GV_SRR.Caption = "*** REGULAR *** "
                Else
                    GV_SRR.Caption = "*** PROMOTION ***"

                End If
                populate_txt_box()

            End If

            MyDataReader.Close()
            conn.Close()


        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try


    End Sub
    
    Public Function check_prod_grp(ByRef p_prod_grp As String)


        Dim txt_prod_grp As String


        Dim strY As String
        Dim sql As String
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader





        txt_prod_grp = UCase(LTrim(RTrim(p_prod_grp)))


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()


        sql = "select prod_grp_cd from inv_mjr where mjr_cd='" & txt_prod_grp & "' or prod_grp_cd='" & txt_prod_grp & "' "


        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()



        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                strY = "Y"

            Else

                lbl_warning.Text = txt_prod_grp + " is invalid  "

                strY = "N"

            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try
        Return strY
    End Function
    Private Function get_orig_record(ByRef p_rowid As String, ByRef p_old_start_dt As Date, ByRef p_old_end_dt As Date, ByRef p_old_min_pcnt As Double)

        Dim l_rowid As String
        Dim l_prod_grp As String

        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_start_dt As Date
        Dim l_end_dt As Date
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtProd_grp As String = "Y"


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append(" start_date,end_date,min_percent ")

        Dim l_table As String
        l_table = DropDown_table.SelectedItem.Value


        If l_table = "REGULAR" Then
            sqlString.Append(" FROM drgm ")
        Else
            sqlString.Append(" FROM drgmp ")
        End If


        sqlString.Append(" WHERE rowid = :l_rowid ")








        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()


        If Not isEmpty(p_rowid) Then
            objcmd.Parameters.Add(":l_rowid", OracleType.VarChar)
            objcmd.Parameters(":l_rowid").Value = p_rowid
        End If



        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                txtProd_grp = "Y"


                p_old_end_dt = MyDataReader.Item("end_date").ToString

                p_old_start_dt = MyDataReader.Item("start_date").ToString

                p_old_min_pcnt = MyDataReader.Item("min_percent").ToString

            Else

                txtProd_grp = "N"


            End If
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw ex

        End Try
        Return txtProd_grp
    End Function
    Private Function check_dup_record(ByRef p_rowid As String, ByRef p_prod_grp As String, ByRef p_eff_dt As Date, ByRef p_end_dt As Date, ByRef p_count As Integer, ByRef p_old_end_dt As Date, ByRef p_old_start_dt As Date, ByRef p_old_min_pcnt As Double)

        Dim l_rowid As String
        Dim l_prod_grp As String

        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_start_dt As Date
        Dim l_end_dt As Date
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtProd_grp As String = "Y"


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append(" rowid ,start_date,end_date,min_percent ")

        Dim l_table As String
        l_table = DropDown_table.SelectedItem.Value


        If l_table = "REGULAR" Then
            sqlString.Append(" FROM drgm ")
        Else
            sqlString.Append(" FROM drgmp ")
        End If


        sqlString.Append(" WHERE prod_grp = :l_prod_grp ")
        sqlString.Append(" and  end_date >= :l_today ")




        If Not isEmpty(p_rowid) Then
            sqlString.Append(" and  rowid <>  : l_rowid ")
        End If

        If Not isEmpty(p_eff_dt) Then
            sqlString.Append("  and ( ")
            sqlString.Append(" ( start_date <=  : l_start_dt  and end_date >= :l_start_dt )")
        End If

        If Not isEmpty(p_end_dt) Then
            sqlString.Append(" or  (start_date <=  : l_end_dt  and end_date >= :l_end_dt) ")
            sqlString.Append("  )   ")
        End If


        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()

        objcmd.Parameters.Add(":l_today", OracleType.DateTime)
        objcmd.Parameters(":l_today").Value = l_today

        objcmd.Parameters.Add(":l_prod_grp", OracleType.VarChar)
        objcmd.Parameters(":l_prod_grp").Value = p_prod_grp

        objcmd.Parameters.Add(":l_end_dt", OracleType.DateTime)
        objcmd.Parameters(":l_end_dt").Value = p_end_dt

        If Not isEmpty(p_rowid) Then
            objcmd.Parameters.Add(":l_rowid", OracleType.VarChar)
            objcmd.Parameters(":l_rowid").Value = p_rowid
        End If

        If Not isEmpty(p_eff_dt) Then
            objcmd.Parameters.Add(":l_start_dt", OracleType.DateTime)
            objcmd.Parameters(":l_start_dt").Value = p_eff_dt
        End If

        If Not isEmpty(p_end_dt) Then
            objcmd.Parameters.Add(":l_end_dt", OracleType.DateTime)
            objcmd.Parameters(":l_end_dt").Value = p_end_dt
        End If


        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                txtProd_grp = "N"
                p_count = 1

                l_rowid = MyDataReader.Item("rowid").ToString
                p_old_end_dt = MyDataReader.Item("end_date").ToString
                p_old_start_dt = MyDataReader.Item("start_date").ToString
                p_old_min_pcnt = MyDataReader.Item("min_percent").ToString
                p_rowid = l_rowid

                lbl_warning.Text = "Similar record existed"

            Else
                p_count = 0
                txtProd_grp = "Y"


            End If
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw ex

        End Try
        Return txtProd_grp
    End Function
    Private Function get_min_pcnt_of_max_dt(ByRef p_prod_grp As String, ByRef p_min_pcnt As Double)

        Dim l_rowid As String
        Dim l_prod_grp As String

        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_start_dt As Date
        Dim l_end_dt As Date

        Dim l_orig_rowid As String
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)

        Dim l_tmrw As Date = l_today.AddDays(1) 'tomoorow

        Dim l_ystd As Date = l_today.AddDays(-1) 'yesterday
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtProd_grp As String = "Y"


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append("   min_percent ")

        Dim l_table As String
        l_table = DropDown_table.SelectedItem.Value


        If l_table = "REGULAR" Then
            sqlString.Append(" FROM drgm ")
        Else
            sqlString.Append(" FROM drgmp ")
        End If


        sqlString.Append(" WHERE prod_grp = :l_prod_grp ")
        sqlString.Append(" and  end_date = :l_max_dt ")








        '   sqlString.Append("  ) group by rowid ")

        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()

        objcmd.Parameters.Add(":l_max_dt", OracleType.DateTime)
        objcmd.Parameters(":l_max_dt").Value = l_max_dt

        objcmd.Parameters.Add(":l_prod_grp", OracleType.VarChar)
        objcmd.Parameters(":l_prod_grp").Value = p_prod_grp



        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then

                p_min_pcnt = MyDataReader.Item("min_percent").ToString


            End If
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw ex

        End Try
        Return txtProd_grp
    End Function

    Private Function check_change(ByRef p_rowid As String, ByRef p_prod_grp As String, ByRef p_eff_dt As Date, ByRef p_end_dt As Date, ByRef p_min_pcnt As Double)

        Dim l_rowid As String
        Dim l_prod_grp As String
        Dim l_count As Integer
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_start_dt As Date
        Dim l_end_dt As Date

        Dim l_orig_rowid As String
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)

        Dim l_tmrw As Date = l_today.AddDays(1) 'tomoorow

        Dim l_ystd As Date = l_today.AddDays(-1) 'yesterday
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtProd_grp As String = "Y"


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append("   count(*) as count ")

        Dim l_table As String
        l_table = DropDown_table.SelectedItem.Value


        If l_table = "REGULAR" Then
            sqlString.Append(" FROM drgm ")
        Else
            sqlString.Append(" FROM drgmp ")
        End If


        sqlString.Append(" WHERE   rowid =  : l_rowid ")
        sqlString.Append(" and ( min_percent <>   : l_min_pcent ")
        sqlString.Append(" or  start_date <>   : l_start_dt ")
        sqlString.Append(" or end_date <>   : l_end_dt ) ")

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()



        If Not isEmpty(p_min_pcnt) Then
            objcmd.Parameters.Add(":l_min_pcent", OracleType.Double)
            objcmd.Parameters(":l_min_pcent").Value = p_min_pcnt
            l_orig_rowid = p_rowid
        End If


        If Not isEmpty(p_rowid) Then
            objcmd.Parameters.Add(":l_rowid", OracleType.VarChar)
            objcmd.Parameters(":l_rowid").Value = p_rowid
            l_orig_rowid = p_rowid
        End If

        If Not isEmpty(p_eff_dt) Then
            objcmd.Parameters.Add(":l_start_dt", OracleType.DateTime)
            objcmd.Parameters(":l_start_dt").Value = p_eff_dt
        End If

        If Not isEmpty(p_end_dt) Then
            objcmd.Parameters.Add(":l_end_dt", OracleType.DateTime)
            objcmd.Parameters(":l_end_dt").Value = p_end_dt
        End If


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then

                l_count = MyDataReader.Item("count").ToString
                If l_count > 0 Then
                    txtProd_grp = "Y"   ' record changed
                Else

                    txtProd_grp = "N"   ' record not changed
                End If
            Else

                txtProd_grp = "N"
             
            End If
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw ex

        End Try
        Return txtProd_grp
    End Function

    Private Function check_dup_count(ByRef p_rowid As String, ByRef p_prod_grp As String, ByRef p_eff_dt As Date, ByRef p_end_dt As Date, ByRef p_count As Integer, ByRef p_old_start_dt As Date, ByRef p_old_end_dt As Date, ByRef p_old_min_pcnt As Double)


        Dim l_rowid As String
        Dim l_prod_grp As String

        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_start_dt As Date
        Dim l_end_dt As Date

        Dim l_orig_rowid As String
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)

        Dim l_tmrw As Date = l_today.AddDays(1) 'tomoorow

        Dim l_ystd As Date = l_today.AddDays(-1) 'yesterday
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtProd_grp As String = "Y"


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append("   count(*) as count ")

        Dim l_table As String
        l_table = DropDown_table.SelectedItem.Value


        If l_table = "REGULAR" Then
            sqlString.Append(" FROM drgm ")
        Else
            sqlString.Append(" FROM drgmp ")
        End If


        sqlString.Append(" WHERE prod_grp = :l_prod_grp ")
        sqlString.Append(" and  end_date >= :l_today ")





        If Not isEmpty(p_rowid) Then
            sqlString.Append(" and  rowid <>  : l_rowid ")
        End If


        

        If Not isEmpty(p_eff_dt) And Not isEmpty(p_end_dt) Then
            sqlString.Append("  and ( ")
            sqlString.Append(" (   start_date <=:l_start_dt and end_date >= :l_start_dt )")  ' record before this one
            sqlString.Append(" or  ( start_date <=  :l_end_dt  and end_date >= :l_end_dt )") ' record the same range

        End If
        sqlString.Append("  ) ")

        '   sqlString.Append("  ) group by rowid ")

        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()

        objcmd.Parameters.Add(":l_today", OracleType.DateTime)
        objcmd.Parameters(":l_today").Value = l_today

        objcmd.Parameters.Add(":l_prod_grp", OracleType.VarChar)
        objcmd.Parameters(":l_prod_grp").Value = p_prod_grp

        objcmd.Parameters.Add(":l_end_dt", OracleType.DateTime)
        objcmd.Parameters(":l_end_dt").Value = p_end_dt

        If Not isEmpty(p_rowid) Then
            objcmd.Parameters.Add(":l_rowid", OracleType.VarChar)
            objcmd.Parameters(":l_rowid").Value = p_rowid
            l_orig_rowid = p_rowid
        End If

        If Not isEmpty(p_eff_dt) Then
            objcmd.Parameters.Add(":l_start_dt", OracleType.DateTime)
            objcmd.Parameters(":l_start_dt").Value = p_eff_dt
        End If

        If Not isEmpty(p_end_dt) Then
            objcmd.Parameters.Add(":l_end_dt", OracleType.DateTime)
            objcmd.Parameters(":l_end_dt").Value = p_end_dt
        End If


        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then

                p_count = MyDataReader.Item("count").ToString
                If p_count > 1 Then
                    txtProd_grp = "N"
                    lbl_warning.Text = "Similar record existed"

                ElseIf p_count = 1 Then

                    txtProd_grp = check_dup_record(p_rowid, p_prod_grp, p_eff_dt, p_end_dt, p_count, p_old_end_dt, p_old_start_dt, p_old_min_pcnt)  ' get rowid if there is one

                    If p_old_end_dt = l_max_dt Or p_end_dt = l_max_dt Then ' it is ok to continue
                        txtProd_grp = "Y"
                    Else ' if old end_Date is not max date, cannot do
                        txtProd_grp = "N"
                    End If

                Else ' 0 record
                    p_count = 0
                    If Not isEmpty(p_rowid) Then
                        txtProd_grp = get_orig_record(p_rowid, p_old_start_dt, p_old_end_dt, p_old_min_pcnt)  ' get original end_date
                    End If
                    txtProd_grp = "Y"

                    End If
            Else ' 0 record
                    p_count = 0
                    If Not isEmpty(p_rowid) Then ' from update_record
                        txtProd_grp = get_orig_record(p_rowid, p_old_start_dt, p_old_end_dt, p_old_min_pcnt)  ' get original end_date 
                    End If
                    txtProd_grp = "Y"
                End If
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw ex

        End Try
        Return txtProd_grp
    End Function
                


    Private Function end_prev_record(ByRef p_rowid As String, ByRef p_eff_dt As Date, ByRef p_end_dt As Date, ByRef p_min_pcnt As Double, ByRef p_old_eff_dt As Date, ByRef p_old_end_dt As Date, ByRef p_old_min_pcnt As Double)

        Dim l_rowid As String
        Dim l_prod_grp As String

        Dim l_count As Integer
        Dim l_start_dt As Date
        Dim l_end_dt As Date
        Dim old_start_dt As Date
        Dim old_end_dt As Date
        Dim new_end_dt As Date
        Dim new_start_dt As Date
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtProd_grp As String = "Y"
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)

        Dim l_tmrw As Date = l_today.AddDays(1) 'tomoorow

        Dim l_ystd As Date = l_today.AddDays(-1) 'yesterday
        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append("  prod_grp,start_date,end_date,min_percent ")

        Dim l_table As String
        l_table = DropDown_table.SelectedItem.Value


        If l_table = "REGULAR" Then
            sqlString.Append(" FROM drgm ")
        Else
            sqlString.Append(" FROM drgmp ")
        End If



        If Not isEmpty(p_rowid) Then
            sqlString.Append(" WHERE  rowid =  : l_rowid ")
        End If

        If Not isEmpty(p_eff_dt) Then
            sqlString.Append("  and ( ")
            sqlString.Append("  start_date <>  : l_start_dt   ")
        End If

        If Not isEmpty(p_end_dt) Then
            sqlString.Append("  or end_date <> :l_end_dt ")

        End If


        If Not isEmpty(p_min_pcnt) Then
            sqlString.Append(" or  min_percent <> :l_min_pcnt ")

        End If
        sqlString.Append(" ) ")
        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()






        If Not isEmpty(p_rowid) Then
            objcmd.Parameters.Add(":l_rowid", OracleType.VarChar)
            objcmd.Parameters(":l_rowid").Value = p_rowid
        End If

        If Not isEmpty(p_eff_dt) Then
            objcmd.Parameters.Add(":l_start_dt", OracleType.DateTime)
            objcmd.Parameters(":l_start_dt").Value = p_eff_dt
        End If

        If Not isEmpty(p_end_dt) Then
            objcmd.Parameters.Add(":l_end_dt", OracleType.DateTime)
            objcmd.Parameters(":l_end_dt").Value = p_end_dt
        End If
        If Not isEmpty(p_min_pcnt) Then
            objcmd.Parameters.Add(":l_min_pcnt", OracleType.Number)
            objcmd.Parameters(":l_min_pcnt").Value = p_min_pcnt
        End If

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                txtProd_grp = "Y"
                old_start_dt = MyDataReader.Item("start_date").ToString  ' original date
                old_end_dt = MyDataReader.Item("end_date").ToString
                p_old_eff_dt = old_start_dt
                p_old_end_dt = old_end_dt
                p_old_min_pcnt = MyDataReader.Item("min_percent").ToString
                l_prod_grp = MyDataReader.Item("prod_grp").ToString
                If p_old_min_pcnt = p_min_pcnt Then
                    If p_old_eff_dt <= p_eff_dt And p_old_end_dt >= p_end_dt Then ' within the old range, no need to do anything
                        lbl_msg.Text = "Similar record existed"
                        txtProd_grp = "no new record"
                        Return txtProd_grp
                        Exit Function
                    ElseIf p_old_eff_dt >= p_eff_dt And p_old_eff_dt >= l_today And p_old_end_dt >= l_today And p_old_end_dt <= p_end_dt And p_old_end_dt <> l_max_dt Then  ' new start date is earlier, new end date is later, modify the old one, no need to insert new one

                        update_prev_record(p_rowid, p_eff_dt, p_end_dt, l_prod_grp, l_count) ' there is also date chcking to make sure no duplicated date range after modify
                        lbl_msg.Text = "similar record existed"
                        txtProd_grp = "no new record"
                        Return txtProd_grp
                        Exit Function
                    ElseIf p_old_eff_dt < l_today And p_old_end_dt >= l_today And p_old_end_dt <= p_end_dt And p_old_end_dt <> l_max_dt Then  'new end date is later, modify the old one, no need to insert new one
                        lbl_msg.Text = "Similar record existed"
                        update_prev_record(p_rowid, p_eff_dt, p_end_dt, l_prod_grp, l_count)
                        txtProd_grp = "no new record"
                        Return txtProd_grp
                        Exit Function
                    Else
                        lbl_msg.Text = "Similar record existed"
                        txtProd_grp = "no new record"
                        Return txtProd_grp
                        Exit Function
                    End If
                ElseIf p_old_min_pcnt <> p_min_pcnt Then

                    If p_old_end_dt <= p_end_dt And p_end_dt >= l_today And p_end_dt <= l_max_dt Then ' always use old start date  
                        If p_eff_dt >= l_tmrw Then
                            new_end_dt = p_eff_dt.AddDays(-1)
                            update_prev_record(p_rowid, p_old_eff_dt, new_end_dt, l_prod_grp, l_count)
                            If l_count = 0 Then  ' no duplicates
                                new_start_dt = p_eff_dt.AddDays(1)
                                If p_end_dt < l_max_dt Then
                                    insert_max_dt_record(l_prod_grp, p_old_min_pcnt, new_start_dt, l_max_dt)     ' need to insert previous record with max date, start date needs to be calculated again
                                End If
                            End If
                        End If
                    ElseIf p_old_end_dt > p_end_dt And p_end_dt >= l_today And p_end_dt <> l_max_dt Then
                        If p_old_end_dt > p_end_dt Then
                            new_start_dt = p_end_dt.AddDays(1)
                            update_prev_record(p_rowid, new_start_dt, p_old_end_dt, l_prod_grp, l_count)
                            If l_count = 0 Then  ' no duplicates

                                If p_old_end_dt < l_max_dt Then
                                    new_start_dt = p_old_end_dt.AddDays(1)
                                    insert_max_dt_record(l_prod_grp, p_old_min_pcnt, new_start_dt, l_max_dt)     ' need to insert previous record with max date, start date is one day later than new end date
                                ElseIf p_old_end_dt = l_max_dt Then
                                    If p_eff_dt > old_start_dt Then   ' the record with max_dt has start_date earlier than new inserted record
                                        new_end_dt = p_eff_dt.AddDays(-1)
                                        insert_to_table(l_prod_grp, p_old_min_pcnt, old_start_dt, new_end_dt)
                                    End If

                                    insert_to_table(l_prod_grp, p_min_pcnt, p_eff_dt, p_end_dt)
                                End If
                            End If
                            ' ElseIf p_old_eff_dt < p_end_dt And p_eff_dt >= l_tmrw And p_old_end_dt = l_max_dt Then
                            '  new_start_dt = p_end_dt.AddDays(1)
                            ' update_prev_record(p_rowid, new_start_dt, l_max_dt, l_prod_grp, l_count) ' update previous end_date=max_dt with a new start_date
                            ' If l_count = 0 Then  ' no duplicates

                            'If p_end_dt < l_max_dt Then

                            'insert_to_table(l_prod_grp, p_min_pcnt, p_eff_dt, p_end_dt)     ' need to insert previous record with max date, start date is one day later than new end date
                            'End If
                            ' End If
                        ElseIf new_start_dt = l_today Then
                            new_start_dt = l_today
                            'cannot start today
                            update_prev_record(p_rowid, p_old_eff_dt, new_end_dt, l_prod_grp, l_count)
                            If l_count = 0 Then  ' no duplicates
                                new_start_dt = p_end_dt.AddDays(1)
                                If p_end_dt < l_max_dt Then
                                    insert_max_dt_record(l_prod_grp, p_old_min_pcnt, new_start_dt, l_max_dt)  'need to insert previous record with max date, start date needs to be calculated again
                                End If
                            End If
                        End If

                    End If
                End If
            Else
                txtProd_grp = "N"
            End If


            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw ex

        End Try
        Return txtProd_grp
    End Function
    Private Function check_past_date(ByRef p_rowid As String)

        Dim l_rowid As String


        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)

        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtProd_grp As String = "Y"


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append("  prod_grp ")


        Dim l_table As String
        l_table = DropDown_table.SelectedItem.Value


        If l_table = "REGULAR" Then
            sqlString.Append(" FROM drgm ")
        Else
            sqlString.Append(" FROM drgmp ")
        End If

        sqlString.Append(" WHERE  rowid =  : l_rowid ")
        sqlString.Append(" and   start_date <:l_today ")



        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()


        If Not isEmpty(p_rowid) Then
            objcmd.Parameters.Add(":l_rowid", OracleType.VarChar)
            objcmd.Parameters(":l_rowid").Value = p_rowid
        End If

        objcmd.Parameters.Add(":l_today", OracleType.DateTime)
        objcmd.Parameters(":l_today").Value = l_today

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                txtProd_grp = "N"
                ' lbl_warning.Text = "Past record cannot be deleted"
            Else
                txtProd_grp = "Y"


            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try
        Return txtProd_grp
    End Function

    Private Function check_allow_delete(ByRef p_prod_grp As String, ByRef p_rowid As String)

        Dim l_rowid As String
        Dim l_prod_grp As String
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)

        Dim l_start_dt As Date
        Dim l_end_dt As Date
        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtProd_grp As String = "Y"


        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append("  prod_grp ")

        Dim l_table As String
        l_table = DropDown_table.SelectedItem.Value


        If l_table = "REGULAR" Then
            sqlString.Append(" FROM drgm ")
        Else
            sqlString.Append(" FROM drgmp ")
        End If

        sqlString.Append(" WHERE prod_grp = :l_prod_grp ")

        If Not isEmpty(p_rowid) Then
            sqlString.Append(" and  rowid <>  :l_rowid ")
        End If

        sqlString.Append("  and end_date >= :l_today ")




        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()


        objcmd.Parameters.Add(":l_prod_grp", OracleType.VarChar)
        objcmd.Parameters(":l_prod_grp").Value = p_prod_grp



        If Not isEmpty(p_rowid) Then
            objcmd.Parameters.Add(":l_rowid", OracleType.VarChar)
            objcmd.Parameters(":l_rowid").Value = p_rowid
        End If




        objcmd.Parameters.Add(":l_today", OracleType.DateTime)
        objcmd.Parameters(":l_today").Value = l_today



        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                txtProd_grp = "N"

            Else
                txtProd_grp = "Y"

            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try
        Return txtProd_grp
    End Function


    Private Function cp_check_allow_delete(ByRef p_prod_grp As String, ByRef p_rowid As String)



        Dim l_rowid As String
        Dim l_prod_grp As String


        Dim l_start_dt As Date
        Dim l_end_dt As Date


        Dim sqlString As StringBuilder
        Dim objcmd As OracleCommand
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand


        Dim MyDataReader As OracleDataReader


        Dim txtProd_grp As String = "Y"
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)

        lbl_warning.Text = Nothing

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString


        conn.Open()

        sqlString = New StringBuilder("SELECT  ")
        sqlString.Append(" prod_grp ")


        Dim l_table As String
        l_table = DropDown_table.SelectedItem.Value


        If l_table = "REGULAR" Then
            sqlString.Append(" FROM drgm ")
        Else
            sqlString.Append(" FROM drgmp ")
        End If

        sqlString.Append(" WHERE  prod_grp =  :l_prod_grp ")
        sqlString.Append(" and  rowid <>  :l_rowid ")
        sqlString.Append(" and end_date >=  :l_today ")
        ' sqlString.Append(" having  count(prod_grp) >0 ")


        objcmd = DisposablesManager.BuildOracleCommand()
        objcmd.Connection = conn

        objcmd.CommandText = sqlString.ToString()
        objcmd.Parameters.Clear()




        If Not isEmpty(p_prod_grp) Then
            objcmd.Parameters.Add(":l_prod_grp", OracleType.VarChar)
            objcmd.Parameters(":l_prod_grp").Value = p_prod_grp
        End If

        objcmd.Parameters.Add(":l_rowid", OracleType.VarChar)
        objcmd.Parameters(":l_rowid").Value = p_rowid

        objcmd.Parameters.Add(":l_today", OracleType.DateTime)
        objcmd.Parameters(":l_today").Value = l_today

        Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)


        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                txtProd_grp = "N"
                ' lbl_warning.Text = "Past record cannot be deleted"
            Else
                txtProd_grp = "Y"


            End If
            MyDataReader.Close()
            conn.Close()
        Catch
            conn.Close()
            Throw

        End Try
        Return txtProd_grp
    End Function

    Protected Function get_prod_grp(ByRef p_rowid As String, ByRef p_prod_grp As String, ByRef p_min_pcent As Double, ByRef p_start_dt As Date, ByRef p_end_dt As Date, ByRef p_max_rowid As String, ByRef p_max_start_dt As Date, ByRef p_count As Integer)
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        lbl_msg.Text = Nothing
        lbl_warning.Text = Nothing

        Dim dgItem As DataGridItem
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim txtRowId As String

        Dim txtProd_grp As String
        Dim l_max_start_dt As Date = l_today
        Dim l_start_dt As Date
        Dim l_end_dt As Date

        'iterate through all the rows  
        p_count = 0
        For i As Integer = 0 To GV_SRR.Items.Count - 1

            dgItem = GV_SRR.Items(i)
          
            txtRowId = dgItem.Cells(0).Text
            If txtRowId = p_rowid Then    'this is the record to be deleted
                txtProd_grp = dgItem.Cells(1).Text
                p_prod_grp = txtProd_grp
                p_min_pcent = dgItem.Cells(2).Text
                p_start_dt = dgItem.Cells(3).Text
                p_end_dt = dgItem.Cells(4).Text
                ' Return txtProd_grp

            End If
        Next

        For i As Integer = 0 To GV_SRR.Items.Count - 1
            dgItem = GV_SRR.Items(i)

            txtRowId = dgItem.Cells(0).Text
            If dgItem.Cells(1).Text = p_prod_grp Then
                l_start_dt = dgItem.Cells(3).Text
                l_end_dt = dgItem.Cells(4).Text
            End If
            If txtProd_grp = p_prod_grp And l_start_dt >= l_today And l_end_dt >= l_today Then ' count the record has future start_date
                p_count = p_count + 1
                If l_start_dt > l_max_start_dt Then
                    l_max_start_dt = l_start_dt
                End If
            End If
            If p_prod_grp = dgItem.Cells(1).Text And dgItem.Cells(4).Text = l_max_dt Then

                p_min_pcent = dgItem.Cells(2).Text   ' this is used to replace deleted record
                p_max_rowid = dgItem.Cells(0).Text
                p_max_start_dt = dgItem.Cells(3).Text
                ' Return txtProd_grp
                Exit Function
            End If
        Next

        If p_max_start_dt <= l_max_start_dt Then  ' do not want to update if the record with max_dt is not the last one of this prod_grp
            p_max_rowid = Nothing
        End If
        '  Return txtProd_grp

    End Function

   
    Protected Sub update_record()


        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)

        Dim l_tmrw As Date = l_today.AddDays(1) 'tomoorow
        Dim l_ystd As Date = l_today.AddDays(-1) 'yesterday
        Dim l_count As Integer
        Dim l_no As Integer = 0
        Dim l_u_no As Integer = 0
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String


        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        lbl_msg.Text = Nothing
        lbl_warning.Text = Nothing

        Dim dgItem As DataGridItem
        Dim drpdwn As DropDownList



        Dim l_txt_prod_grp As TextBox
        Dim l_txt_min_pcnt As TextBox
        Dim l_txt_start_date As TextBox
        Dim l_txt_end_date As TextBox

        Dim txtRowId As String
        Dim old_rowid As String

        Dim txtProd_grp As String
        Dim txtMin_pcnt As String
        Dim txtStart_date As String
        Dim txtEnd_date As String


        Dim dblMin_pcnt As Double
        Dim dtStart_date As Date
        Dim dtEnd_date As Date
        Dim old_dblMin_pcnt As Double
        Dim old_dtStart_date As Date
        Dim old_dtEnd_date As Date
        Dim l_end_date As Date
        Dim l_new_start_dt As Date
        Dim l_new_end_dt As Date
        Dim strYes As String = "N"

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()


            'iterate through all the rows  
            For i As Integer = 0 To GV_SRR.Items.Count - 1
                lbl_warning.Text = Nothing
                dgItem = GV_SRR.Items(i)

                txtRowId = dgItem.Cells(0).Text
                txtProd_grp = dgItem.Cells(1).Text
                l_txt_min_pcnt = CType(GV_SRR.Items(i).FindControl("txt_min_pcnt"), TextBox)
                l_txt_start_date = CType(GV_SRR.Items(i).FindControl("txt_start_date"), TextBox)
                l_txt_end_date = CType(GV_SRR.Items(i).FindControl("txt_end_date"), TextBox)




                txtMin_pcnt = l_txt_min_pcnt.Text

                If Not isEmpty(l_txt_start_date.Text) And IsDate(l_txt_start_date.Text) Then
                    txtStart_date = l_txt_start_date.Text
                    dtStart_date = txtStart_date
                Else

                    dtStart_date = l_tmrw
                End If

                If Not isEmpty(l_txt_end_date.Text) And IsDate(l_txt_end_date.Text) Then

                    txtEnd_date = l_txt_end_date.Text
                    dtEnd_date = txtEnd_date
                Else

                    dtEnd_date = dtStart_date
                End If

                If IsNumeric(txtMin_pcnt) And 0 <= txtMin_pcnt <= 100 Then

                    dblMin_pcnt = txtMin_pcnt
                Else
                    lbl_warning.Text = txtMin_pcnt + " is invalid"

                    Exit Sub
                End If
                If dtStart_date = DateTime.MinValue Then
                    dtStart_date = l_tmrw
                End If
                If dtEnd_date = DateTime.MinValue Then
                    dtEnd_date = dtStart_date
                End If


                old_dtStart_date = DateTime.MinValue
                old_dtEnd_date = DateTime.MinValue
                old_dblMin_pcnt = Nothing

                strYes = check_change(txtRowId, txtProd_grp, dtStart_date, dtEnd_date, dblMin_pcnt)  ' change if the record is changed

                If strYes = "Y" Then ' record changed
                    strYes = check_date(dtStart_date, dtEnd_date)
                    If strYes <> "Y" Then
                        l_no = l_no + 1

                    ElseIf strYes = "Y" Then
                        strYes = check_dup_count(txtRowId, txtProd_grp, dtStart_date, dtEnd_date, l_count, old_dtStart_date, old_dtEnd_date, old_dblMin_pcnt)  ' if only one active, return the rowid, if count=0 , recturn the orginal information
                        If strYes = "N" Then ' more than one active record

                            lbl_warning.Text = "A record cannot be updated to Prod_grp=" + txtProd_grp + " Min_percent=" + txtMin_pcnt + " Start_date= " + dtStart_date + " End_date=   " + dtEnd_date + "  , similar record existed"
                            ' Exit Sub
                            l_no = l_no + 1
                        Else

                            If txtProd_grp = "1FURN" Or txtProd_grp = "2APPL" Or txtProd_grp = "3ELEC" Or txtProd_grp = "4MATT" Then    ' update record


                                If l_count = 1 And old_dtEnd_date = l_max_dt And old_dblMin_pcnt <> dblMin_pcnt Then ' only one active record with end_date=max_dt
                                    old_rowid = txtRowId  ' old_rowid is the one with max_dt
                                    txtRowId = dgItem.Cells(0).Text
                                    update_each_record(txtRowId, dblMin_pcnt, dtStart_date, dtEnd_date) ' update the one changed
                                    l_u_no = l_u_no + 1
                                    dtStart_date = dtEnd_date.AddDays(1)
                                    update_each_record(old_rowid, old_dblMin_pcnt, dtStart_date, l_max_dt) ' update the one with end_date=max_dt
                                    l_u_no = l_u_no + 1
                                    ' strYes = end_prev_record(txtRowId, dtStart_date, dtEnd_date, dblMin_pcnt, old_dtStart_date, old_dtEnd_date, old_dblMin_pcnt)
                                ElseIf l_count = 1 And old_dtEnd_date <> l_max_dt And dtEnd_date = l_max_dt And dtStart_date <= old_dtEnd_date Then ' only one active record with end_date <> max_dt, the updated one to be the one with max_dt
                                    dtStart_date = old_dtEnd_date.AddDays(1)
                                    txtRowId = dgItem.Cells(0).Text
                                    '   update_each_record(txtRowId, dblMin_pcnt, dtStart_date, l_max_dt)
                                ElseIf l_count = 0 And (old_dtStart_date <> dtStart_date) Or (dtEnd_date <> old_dtEnd_date) Or (old_dblMin_pcnt <> dblMin_pcnt) Then ' no similar record
                                    If dtEnd_date <> l_max_dt And old_dtEnd_date <> l_max_dt And old_dtEnd_date > dtEnd_date And old_dblMin_pcnt <> dblMin_pcnt Then ' need new record to cover the gap of end date ????
                                        l_new_start_dt = dtEnd_date.AddDays(-1)
                                        l_new_end_dt = old_dtEnd_date
                                        insert_to_table(txtProd_grp, txtMin_pcnt, l_new_start_dt, l_new_end_dt)  ' insert a new record with new end date to cover the gap of end date

                                        update_each_record(txtRowId, dblMin_pcnt, dtStart_date, dtEnd_date)
                                        l_u_no = l_u_no + 1
                                    ElseIf dtEnd_date <> l_max_dt And old_dtEnd_date <> l_max_dt Then ' not the one with max_dt
                                        '  l_new_start_dt = dtEnd_date.AddDays(-1)
                                        '  l_new_end_dt = old_dtEnd_date
                                        '   insert_to_table(txtProd_grp, txtMin_pcnt, l_new_start_dt, l_new_end_dt)  ' insert a new record with new end date to cover the gap of end date

                                        update_each_record(txtRowId, dblMin_pcnt, dtStart_date, dtEnd_date)
                                        l_u_no = l_u_no + 1
                                        If dtEnd_date < old_dtEnd_date Then ' cover the gap with min of max_dt, end earlier
                                            get_min_pcnt_of_max_dt(txtProd_grp, dblMin_pcnt)
                                            dtStart_date = dtEnd_date.AddDays(1)
                                            dtEnd_date = old_dtEnd_date
                                            insert_to_table(txtProd_grp, dblMin_pcnt, dtStart_date, dtEnd_date)
                                            l_u_no = l_u_no + 1
                                        ElseIf dtStart_date > old_dtStart_date Then ' cover the gap with min of max_dt, start later
                                            get_min_pcnt_of_max_dt(txtProd_grp, dblMin_pcnt)
                                            dtEnd_date = dtStart_date.AddDays(-1)
                                            dtStart_date = old_dtStart_date
                                            insert_to_table(txtProd_grp, dblMin_pcnt, dtStart_date, dtEnd_date)
                                            l_u_no = l_u_no + 1
                                        End If
                                    ElseIf old_dtEnd_date = l_max_dt And old_dtEnd_date <> dtEnd_date Then ' change the one end with max_dt  
                                        If dtStart_date = old_dtStart_date Then

                                            insert_to_table(txtProd_grp, old_dblMin_pcnt, old_dtStart_date, dtEnd_date) ' keep the old min_percent
                                            l_u_no = l_u_no + 1
                                            dtStart_date = dtEnd_date.AddDays(1)
                                            dtEnd_date = l_max_dt

                                            update_each_record(txtRowId, dblMin_pcnt, dtStart_date, dtEnd_date)

                                            l_u_no = l_u_no + 1
                                        ElseIf dtStart_date > old_dtStart_date And old_dblMin_pcnt <> dblMin_pcnt Then
                                            l_end_date = dtStart_date.AddDays(-1)
                                            insert_to_table(txtProd_grp, old_dblMin_pcnt, old_dtStart_date, l_end_date) ' keep the old min_percent and old_start_dt
                                            l_u_no = l_u_no + 1
                                            update_each_record(txtRowId, dblMin_pcnt, dtStart_date, dtEnd_date)
                                            l_u_no = l_u_no + 1
                                            dtStart_date = dtEnd_date.AddDays(1)
                                            insert_to_table(txtProd_grp, old_dblMin_pcnt, dtStart_date, old_dtEnd_date) ' keep the old min_percent and max_dt
                                            l_u_no = l_u_no + 1
                                        ElseIf dtStart_date > old_dtStart_date And old_dblMin_pcnt = dblMin_pcnt Then
                                           
                                            l_no = l_no + 1
                                            
                                        Else
                                            update_each_record(txtRowId, dblMin_pcnt, dtStart_date, dtEnd_date)
                                            l_u_no = l_u_no + 1
                                        End If
                                    ElseIf old_dtEnd_date = l_max_dt And old_dtEnd_date = dtEnd_date Then ' change the one end with max_dt 
                                        If old_dblMin_pcnt <> dblMin_pcnt Or dtStart_date < old_dtStart_date Then
                                            update_each_record(txtRowId, dblMin_pcnt, dtStart_date, dtEnd_date)

                                            l_u_no = l_u_no + 1
                                        ElseIf old_dblMin_pcnt = dblMin_pcnt And dtStart_date >= old_dtStart_date Then
                                            l_no = l_no + 1
                                        ElseIf old_dblMin_pcnt <> dblMin_pcnt And dtStart_date > old_dtStart_date Then
                                            l_end_date = dtStart_date.AddDays(-1)
                                            insert_to_table(txtProd_grp, old_dblMin_pcnt, old_dtStart_date, l_end_date) ' keep the old min_percent and old_start_dt
                                            l_u_no = l_u_no + 1
                                            update_each_record(txtRowId, dblMin_pcnt, dtStart_date, dtEnd_date)
                                            l_u_no = l_u_no + 1
                                            dtStart_date = dtEnd_date.AddDays(1)
                                            insert_to_table(txtProd_grp, old_dblMin_pcnt, dtStart_date, old_dtEnd_date) ' keep the old min_percent and max_dt
                                            l_u_no = l_u_no + 1
                                        End If
                                    Else
                                        l_no = l_no + 1
                                    End If

                                    '  lbl_msg.Text = "The record is updated"
                                ElseIf l_count = 0 And (old_dtStart_date = dtStart_date) And (dtEnd_date = old_dtEnd_date) And (old_dblMin_pcnt = dblMin_pcnt) Then ' no similar record
                                    l_no = l_no + 1
                                Else

                                    l_no = l_no
                                    ' lbl_warning.Text = "A record cannot be updated to Prod_grp=" + txtProd_grp + " Min_percent=" + txtMin_pcnt + " Start_date= " + dtStart_date + " End_date=   " + dtEnd_date + " , similar record existed"
                                    ' Exit Sub
                                End If 'end count
                            Else  ' other major
                                If l_count = 0 And (old_dtStart_date <> dtStart_date) Or (dtEnd_date <> old_dtEnd_date) Or (old_dblMin_pcnt <> dblMin_pcnt) Then ' no similar record
                                    update_each_record(txtRowId, dblMin_pcnt, dtStart_date, dtEnd_date)
                                    l_u_no = l_u_no + 1
                                    '  lbl_msg.Text = "The record is updated"
                                End If
                            End If ' end prod_grp
                        End If ' end strYes
                    Else
                        l_no = l_no + 1
                    End If ' end check_date
                End If  ' record changed

            Next
            '  clear_screen()
            reload()
            btn_update.Enabled = True
            If l_u_no > 0 Then
                Dim str_u_no As String = l_u_no
                lbl_warning.Text = str_u_no + " record(s) updated"
            End If
            If l_no > 0 Then
                Dim str_no As String = l_no
                lbl_warning.Text = str_no + " record(s) not updated "
            End If


            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try






    End Sub

    Public Sub insert_to_table(ByRef p_prod_grp As String, ByRef p_min_pcnt As Double, ByRef p_eff_dt As Date, ByRef p_end_dt As Date)



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)
        Dim l_tmrw As Date = l_today.AddDays(1) 'tomoorow
        Dim l_ystd As Date = l_today.AddDays(-1) 'yesterday

        Dim l_eff_dt As Date = p_eff_dt
        Dim l_end_dt As Date = p_end_dt

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            lbl_msg.Text = Nothing

            Dim l_table As String
            l_table = DropDown_table.SelectedItem.Value

            If isEmpty(l_eff_dt) Then
                lbl_warning.Text = "Start date is required"
                Exit Sub

            End If
            If isEmpty(l_end_dt) Then
                lbl_warning.Text = "End date is invalid"
                Exit Sub
            End If

            If l_table = "REGULAR" Then
                sql = "Insert into drgm "
            Else
                sql = "Insert into drgmp "
            End If



            sql = sql & " (prod_grp,min_percent,start_date,end_date  ) "
            sql = sql & " select upper(:prod_grp),:min_percent,:start_date ,:end_date "
            sql = sql & " from dual"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":prod_grp", OracleType.VarChar)
            objsql.Parameters(":prod_grp").Value = p_prod_grp

            objsql.Parameters.Add(":min_percent", OracleType.Double)
            objsql.Parameters(":min_percent").Value = p_min_pcnt
            objsql.Parameters.Add(":start_date", OracleType.DateTime)
            objsql.Parameters(":start_date").Value = l_eff_dt
            objsql.Parameters.Add(":end_date", OracleType.DateTime)
            objsql.Parameters(":end_date").Value = l_end_dt
            'Execute DataReader 
            objsql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try

    End Sub

    Public Sub insert_max_dt_record(ByRef p_prod_grp As String, ByRef p_min_pcnt As Double, ByRef p_eff_dt As Date, ByRef p_end_dt As Date)



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_eff_dt As Date = p_eff_dt
        Dim l_end_dt As Date = p_end_dt

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim Mydatareader As OracleDataReader
            Dim objsql As OracleCommand

            lbl_msg.Text = Nothing

            Dim l_table As String
            l_table = DropDown_table.SelectedItem.Value


            If l_table = "REGULAR" Then
                sql = "Insert into drgm "
            Else
                sql = "Insert into drgmp "
            End If

            sql = sql & " (prod_grp,min_percent,start_date,end_date  ) "
            sql = sql & " select upper(:prod_grp),:min_percent,:start_date,:end_date "

            sql = sql & " from dual"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":prod_grp", OracleType.VarChar)
            objsql.Parameters(":prod_grp").Value = p_prod_grp

            objsql.Parameters.Add(":min_percent", OracleType.Double)
            objsql.Parameters(":min_percent").Value = p_min_pcnt
            objsql.Parameters.Add(":start_date", OracleType.DateTime)
            objsql.Parameters(":start_date").Value = l_eff_dt
            objsql.Parameters.Add(":end_date", OracleType.DateTime)
            objsql.Parameters(":end_date").Value = l_end_dt
            'Execute DataReader 
            objsql.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try

    End Sub


    Protected Sub click_btn_insert(ByVal sender As Object, ByVal e As System.EventArgs)



        Dim l_id As String
        Dim l_count As Integer

        Dim l_prod_grp As String

        Dim str_min_pcnt As String
        Dim l_min_pcnt As String
        Dim l_start_dt As Date
        Dim l_end_dt As Date
        Dim l_old_min_pcnt As Double
        Dim l_old_start_dt As Date
        Dim l_old_end_dt As Date

        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)

        Dim l_tmrw As Date = l_today.AddDays(1) 'tomoorow

        Dim l_ystd As Date = l_today.AddDays(-1) 'yesterday


        lbl_msg.Text = Nothing
        lbl_warning.Text = Nothing
        Dim l_rowid As String = Nothing

        l_prod_grp = UCase(LTrim(RTrim(Text_prod_grp.Text)))

        If Len(l_prod_grp) = 0 Then
            lbl_msg.Text = "Please enter a prod_grp value  "
            Exit Sub
        End If

        l_start_dt = can_eff_date.SelectedDate


        If can_eff_date.SelectedDate.Date = DateTime.MinValue Then
            l_start_dt = l_tmrw
        ElseIf l_start_dt <= l_today Then
            lbl_warning.Text = "Start date must be greater than today"
            Exit Sub
        ElseIf l_start_dt >= l_max_dt Then
            lbl_warning.Text = "Start date is invalid"
            Exit Sub
        End If


        l_end_dt = can_end_date.SelectedDate


        If can_end_date.SelectedDate.Date = DateTime.MinValue Then
            If l_prod_grp = "1FURN" Or l_prod_grp = "2APPL" Or l_prod_grp = "3ELEC" Or l_prod_grp = "4MATT" Then
                lbl_warning.Text = "End date is required"
                Exit Sub
            Else
                l_end_dt = FormatDateTime("12/31/2049", DateFormat.ShortDate)
            End If
        ElseIf l_end_dt <= l_today Then
            lbl_warning.Text = "End date must be greater than today"
            Exit Sub
        ElseIf l_end_dt > l_max_dt Then
            lbl_warning.Text = "End date is invalid"
            Exit Sub
        End If







        str_min_pcnt = text_min_pcnt.Text
        If isEmpty(str_min_pcnt) Then
            lbl_msg.Text = "Please enter a correct min percent  "
            Exit Sub
        End If

        l_min_pcnt = str_min_pcnt

        If Len(l_min_pcnt) = 0 Or l_min_pcnt < 0 Or l_min_pcnt > 100 Then
            lbl_msg.Text = "Please enter a correct min percent  "
            Exit Sub
        End If

        Dim strYes As String = "N"
        strYes = check_prod_grp(l_prod_grp)
        If strYes <> "Y" Then
            lbl_msg.Text = "Please enter a correct prod_grp value  "
            Exit Sub
        End If
        strYes = "N"
        strYes = check_date(l_start_dt, l_end_dt)
        If strYes = "N" Then
            Exit Sub


        ElseIf strYes = "Y" Then


            strYes = check_dup_count(l_rowid, l_prod_grp, l_start_dt, l_end_dt, l_count, l_old_start_dt, l_old_end_dt, l_old_min_pcnt)  ' if only one active , then return the rowid
            If strYes = "N" Then ' more than one active record
                lbl_warning.Text = "Similar record existed"
                Exit Sub
            End If
            If l_prod_grp = "1FURN" Or l_prod_grp = "2APPL" Or l_prod_grp = "3ELEC" Or l_prod_grp = "4MATT" Then   '   insert record
                If l_count = 1 Then ' only one active record
                    strYes = end_prev_record(l_rowid, l_start_dt, l_end_dt, l_min_pcnt, l_old_start_dt, l_old_end_dt, l_old_min_pcnt)
                    If strYes = "no new record" And lbl_msg.Text = "Similar record existed" Then

                        Exit Sub

                    ElseIf strYes = "Y" Then  'normal, no error
                        If l_end_dt <> l_max_dt And l_old_start_dt <> l_start_dt And l_start_dt >= l_tmrw Then
                            lbl_warning.Text = "do nothing" ' 06-Apr-17
                            '   insert_to_table(l_prod_grp, l_min_pcnt, l_start_dt, l_end_dt)   , 06-Apr-17
                            ' ElseIf l_end_dt <> l_max_dt And l_old_end_dt = l_max_dt And l_start_dt >= l_tmrw Then  ' the change is made to the record with end_date=max_dt

                            '  insert_to_table(l_prod_grp, l_min_pcnt, l_start_dt, l_end_dt)
                        ElseIf l_end_dt = l_max_dt And l_old_end_dt = l_max_dt And l_start_dt >= l_old_start_dt Then


                            lbl_warning.Text = "do nothing" ' 06-Apr-17
                        
                        End If
                    End If  ' end_prev_record
                ElseIf l_count = 0 Then ' no similar record
                    insert_to_table(l_prod_grp, l_min_pcnt, l_start_dt, l_end_dt)

                End If 'end no of record
            Else  ' other major
                If l_count = 0 Then
                    insert_to_table(l_prod_grp, l_min_pcnt, l_start_dt, l_end_dt)
                ElseIf l_count > 0 Then
                    lbl_warning.Text = "Similar record existed"
                    Exit Sub
                End If
            End If ' end prod_grp
        End If ' end check_date

        clear_screen()
        reload()
        lbl_warning.Text = "Record inserted"
    End Sub







    Protected Sub update_each_record(ByRef p_rowid As String,
                                      ByRef p_min_pcnt As Double, ByRef p_start_date As Date,
                                     ByRef p_end_date As Date)



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim l_str As String
        Dim objsql As OracleCommand

        lbl_msg.Text = Nothing

        Dim str_r As String = "N"

        Dim txtRowId As String

        Dim txtProd_grp As String

        Dim txMin_pcnt As Double
        Dim txtStart_date As Date
        Dim txtEnd_date As Date
        Dim l_Start_date As Date

        '  l_str = check_past_date(p_rowid)
        ' If l_str <> "Y" Then
        ' lbl_msg.Text = "Past record cannot be updated"
        'Exit Sub
        ' Else
        ' str_r = "Y"
        ' End If

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim l_table As String
            l_table = DropDown_table.SelectedItem.Value


            If l_table = "REGULAR" Then
                sql = "update drgm "
            Else
                sql = "update drgmp "
            End If


            ' sql = sql & " set min_percent='" & p_min_pcnt & "'"
            sql = sql & " set min_percent=:txtMin_pcnt "
            ' start_date='" & p_start_date & "', end_date='" & p_end_date & "'"
            sql = sql & "  , start_date=:txtStart_date, end_date=:txtEnd_date"
            sql = sql & " where rowid ='" & p_rowid & "'"
            ' sql = sql & " where rowid =:txtRowId"
            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            '  objsql.Parameters.Add(":txtRowId", OracleType.VarChar)
            '  objsql.Parameters(":txtRowId").Value = p_rowid




            objsql.Parameters.Add(":txtMin_pcnt", OracleType.Double)
            objsql.Parameters(":txtMin_pcnt").Value = p_min_pcnt
            objsql.Parameters.Add(":txtStart_date", OracleType.DateTime)
            objsql.Parameters(":txtStart_date").Value = p_start_date
            objsql.Parameters.Add(":txtEnd_date", OracleType.DateTime)
            objsql.Parameters(":txtEnd_date").Value = p_end_date
            objsql.ExecuteNonQuery()
            ' lbl_msg.Text = " Records updated "
            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try


    End Sub
    Protected Sub update_prev_record(ByRef p_rowid As String, ByRef p_start_date As Date, ByRef p_end_date As Date, ByRef p_prod_grp As String, ByRef p_count As Integer)



        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim l_str As String
        Dim objsql As OracleCommand

        lbl_msg.Text = Nothing
        Dim l_today As Date = FormatDateTime(Today, DateFormat.ShortDate)
        Dim l_tmrw As Date = l_today.AddDays(1) 'tomoorow
        Dim l_ystd As Date = l_today.AddDays(-1) 'yesterday
        Dim l_old_end_dt As Date
        Dim l_old_start_dt As Date
        Dim l_old_min_pcnt As Double
        Dim str_r As String = "N"

        Dim txtRowId As String




        Dim txtEnd_date As Date



        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()

            Dim l_table As String
            l_table = DropDown_table.SelectedItem.Value

            str_r = check_dup_record(p_rowid, p_prod_grp, p_start_date, p_end_date, p_count, l_old_end_dt, l_old_start_dt, l_old_min_pcnt)
            If p_count > 0 Then
                lbl_msg.Text = "Similar record exited, cannot modify"
                Exit Sub

            End If

            If l_table = "REGULAR" Then
                sql = "update drgm "
            Else
                sql = "update drgmp "
            End If


            sql = sql & "  set end_date=:txtEnd_date, start_date=:txtStart_date   "
            sql = sql & " where rowid ='" & p_rowid & "'"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)


            objsql.Parameters.Add(":txtEnd_date", OracleType.DateTime)
            objsql.Parameters(":txtEnd_date").Value = p_end_date

            objsql.Parameters.Add(":txtStart_date", OracleType.DateTime)
            objsql.Parameters(":txtStart_date").Value = p_start_date

            objsql.ExecuteNonQuery()
            ' lbl_msg.Text = " Records updated "
            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try


    End Sub
    Public Sub click_btn_update(ByVal sender As Object, ByVal e As System.EventArgs)




        lbl_warning.Text = Nothing

        update_record()



    End Sub




    Protected Sub doitemdelete(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GV_SRR.DeleteCommand

        Dim txtRowid As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim l_str As String
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim oldRowid As String
        Dim l_act_count As Integer
        Dim l_prod_grp As String
        Dim dt_start_dt As Date
        Dim dt_end_dt As Date
        Dim dt_max_start_dt As Date
        Dim l_max_rowid As String
        Dim l_min_pcnt As Double
        lbl_msg.Text = Nothing
        txtRowid = GV_SRR.DataKeys(e.Item.ItemIndex)
        l_str = check_past_date(txtRowid)
        If l_str <> "Y" Then
            lbl_msg.Text = "Past record cannot be deleted"
            Exit Sub

        End If
        get_prod_grp(txtRowid, l_prod_grp, l_min_pcnt, dt_start_dt, dt_end_dt, l_max_rowid, dt_max_start_dt, l_act_count)
        ' If l_prod_grp in ('1FURN','2APPL','3ELEC','4MATT')  then
        If l_prod_grp = "1FURN" Or l_prod_grp = "2APPL" Or l_prod_grp = "3ELEC" Or l_prod_grp = "4MATT" Then   ' these 4 records must have in the table
            If dt_end_dt >= l_max_dt Then
                lbl_msg.Text = "The record with maxmium date is going to be deleted"
                '  Exit Sub
            Else
                If l_act_count <= 2 And Not isEmpty(l_max_rowid) And dt_start_dt < dt_max_start_dt Then
                    update_each_record(l_max_rowid, l_min_pcnt, dt_start_dt, l_max_dt) ' update the one changed with the min_percent from the record with max_dt
                    '  lbl_msg.Text = "The record cannot be deleted, the min percent is replaced with the one of max time"
                    reload()
                End If

                
            End If

            Try
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()
                Dim l_table As String
                l_table = DropDown_table.SelectedItem.Value


                If l_table = "REGULAR" Then
                    sql = "delete from drgm "
                Else
                    sql = "delete from drgmp "
                End If

                sql = sql & " where rowid =:txtRowId"

                objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                objsql.Parameters.Add(":txtRowId", OracleType.VarChar)
                objsql.Parameters(":txtRowId").Value = txtRowid


                'Execute DataReader 

                objsql.ExecuteNonQuery()




                lbl_msg.Text = "one record deleted, please click search again to view it"

                conn.Close()
            Catch ex As Exception
                Throw ex
                conn.Close()
            End Try

            GV_SRR.Enabled = False
            GV_SRR.Visible = False

            '  btn_update.Visible = False

            ' reload()      'for some reason, reload will cause another delete
        End If

    End Sub
    Protected Sub delete_record(ByVal p_rowid As String)

        Dim txtRowid As String


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim l_str As String
        Dim l_max_dt As Date = FormatDateTime("12/31/2049", DateFormat.ShortDate)
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand
        Dim oldRowid As String

        Dim l_prod_grp As String
        Dim dt_end_dt As Date
        lbl_msg.Text = Nothing

        l_str = check_past_date(p_rowid)
        If l_str <> "Y" Then
            lbl_msg.Text = "Past record cannot be deleted"
            Exit Sub

        End If


        If l_prod_grp = "1FURN" Or l_prod_grp = "2APPL" Or l_prod_grp = "3ELEC" Or l_prod_grp = "4MATT" Then   ' these 4 records must have in the table
            If dt_end_dt >= l_max_dt Then
                lbl_msg.Text = "The record with maxmium date cannot be deleted"
                Exit Sub
            End If

            l_str = check_allow_delete(l_prod_grp, p_rowid)
            If l_str = "Y" Then   ' no more than record exists
                lbl_msg.Text = "The record cannot be deleted"
                Exit Sub
            End If
        End If

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            conn.Open()
            Dim l_table As String
            l_table = DropDown_table.SelectedItem.Value


            If l_table = "REGULAR" Then
                sql = "delete from drgm "
            Else
                sql = "delete from drgmp "
            End If

            sql = sql & " where rowid =:txtRowId"

            objsql = DisposablesManager.BuildOracleCommand(sql, conn)

            objsql.Parameters.Add(":txtRowId", OracleType.VarChar)
            objsql.Parameters(":txtRowId").Value = p_rowid


            'Execute DataReader 

            objsql.ExecuteNonQuery()




            lbl_msg.Text = "one record deleted, please click search again to view it"

            conn.Close()
        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try

        GV_SRR.Enabled = False
        GV_SRR.Visible = False

        btn_update.Visible = False

        ' reload()      'for some reason, reload will cause another delete


    End Sub
    Protected Sub clear_screen()
        Dim l_id As String



        GV_SRR.Enabled = False
        GV_SRR.Visible = False

        btn_update.Visible = False




        ' Text_prod_grp.Text = Nothing
        text_min_pcnt.Text = Nothing

        can_eff_date.SelectedDate = DateTime.MinValue
        can_end_date.SelectedDate = DateTime.MinValue


    End Sub








    ''' Description    : To Clear Grid, search criteria and remove sessions
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btn_clr_screen_Click(sender As Object, e As EventArgs)
        Dim l_id As String



        GV_SRR.Enabled = False
        GV_SRR.Visible = False

        btn_update.Visible = False




        Text_prod_grp.Text = Nothing
        text_min_pcnt.Text = Nothing

        can_eff_date.SelectedDate = DateTime.MinValue
        can_end_date.SelectedDate = DateTime.MinValue

        lbl_warning.Text = ""
        lbl_msg.Text = ""
    End Sub










End Class