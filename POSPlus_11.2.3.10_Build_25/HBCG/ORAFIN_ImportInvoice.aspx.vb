Imports System.Collections.Generic
Imports HBCG_Utils
Imports World_Gift_Utils
Imports SD_Utils
Imports System.Data.OracleClient
Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Linq
Imports DevExpress.Web.Data
Imports System.Xml
Imports System.Threading
Imports System.Globalization
Imports AppUtils
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxTreeList
Partial Class ORAFIN_ImportInvoice
    Inherits POSBasePage
    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
    Private theSalesBiz As SalesBiz = New SalesBiz()
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim v_name As String = ""
        lbl_msg.Text = ""
        lbl_msg2.Text = ""

        
        If Not IsPostBack Then
            populate_co_cd()
            populate_ve_cd()

            ASPxPopupORAFIN_ImportInvoice.ShowOnPageLoad() = False
        Else
            If ASPxPopupORAFIN_ImportInvoice.ShowOnPageLoad = True Then

                populate_gridview1()

            End If
        End If


    End Sub
    Public Sub Populate_co_cd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_co_cd.Items.Clear()

        sql = " select distinct c.co_cd co_cd, c.co_cd||' - '||c.des name "
        sql = sql & " from co c, store s "
        sql = sql & " where std_multi_co2.isvalidstr('" & Session("emp_init") & "', s.store_cd) = 'Y' "
        sql = sql & " order by 1 "

        srch_co_cd.Items.Insert(0, "-")
        srch_co_cd.Items.FindByText("-").Value = ""
        srch_co_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_co_cd
                .DataSource = ds
                .DataValueField = "co_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

       

    End Sub
    Public Sub Populate_ve_cd()

        Dim connErp As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdGetCodes As OracleCommand = DisposablesManager.BuildOracleCommand
        Dim ds As New DataSet
        Dim sql As String

        srch_ve_cd.Items.Clear()

        sql = " select ve_cd ve_cd, ve_cd||' - '||ve_name name from ve v "
        sql = sql & " where std_multi_co2.isvalidvdr('" & Session("emp_init") & "', v.ve_cd) = 'Y' "
        sql = sql & " order by 1 "

        srch_ve_cd.Items.Insert(0, "-")
        srch_ve_cd.Items.FindByText("-").Value = ""
        srch_ve_cd.SelectedIndex = 0

        Try
            With cmdGetCodes
                .Connection = connErp
                .CommandText = sql
            End With

            connErp.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetCodes)
            oAdp.Fill(ds)

            With srch_ve_cd
                .DataSource = ds
                .DataValueField = "ve_cd"
                .DataTextField = "name"
                .DataBind()
            End With

            connErp.Close()

        Catch ex As Exception
            connErp.Close()
            Throw
        End Try

    End Sub
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        ASPxPopupORAFIN_ImportInvoice.ShowOnPageLoad() = False

        GridView1.DataSource = ""
        GridView1.DataBind()
        GridView1.PageIndex = 0

        srch_co_cd.SelectedIndex = 0
        srch_ve_cd.SelectedIndex = 0

    End Sub
    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click

        Dim v_msg As String = ""

        If srch_co_cd.SelectedIndex < 1 Then
            lbl_msg.Text = "CO_CD must be selected to continue"
            Exit Sub
        End If


        GridView1.PageIndex = 0
        populate_gridview1()

        ASPxPopupORAFIN_ImportInvoice.ShowOnPageLoad() = True
        If GridView1.VisibleRowCount < 1 Then
            lbl_msg.Text = "No Data to display"
        End If

    End Sub
    Function getSrchValue(ByVal p_srch As String) As String

        Dim v_value As String = ""

        Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim dbCommand As OracleCommand
        Dim dbReader As OracleDataReader
        Dim v_mpc As String = ""

        Dim sql As String = "SELECT upper(replace(replace('" & p_srch & "','*',null),'%',null)) VAL from dual "

        Try
            dbConnection.Open()
            dbCommand = DisposablesManager.BuildOracleCommand(sql, dbConnection)
            dbReader = DisposablesManager.BuildOracleDataReader(dbCommand)

            If dbReader.Read() Then
                v_value = dbReader.Item("VAL")
            Else
                v_value = ""
            End If
        Catch
            v_value = ""
            'do nothing
        End Try

        dbReader.Close()
        dbCommand.Dispose()
        dbConnection.Close()

        If v_value & "" = "" Or isEmpty(v_value) Then
            Return ""
        Else
            Return v_value
        End If

    End Function
    Protected Sub populate_gridview1()


        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim v_ivc As String = ""
        Dim v_po As String = ""

        If srch_ivc_cd.Text & "" = "" Then
            v_ivc = ""
        Else
            v_ivc = getSrchValue(srch_ivc_cd.Text)
        End If
        If srch_po_cd.Text & "" = "" Then
            v_po = ""
        Else
            v_po = getSrchValue(srch_po_cd.Text)
        End If

        Dim v_common_cond As String = "  and nvl(x.stat_cd,'x') = 'O' " &
                              "  and s.co_cd = '" & srch_co_cd.SelectedItem.Value.ToString() & "'" &
                              "  and trunc(x.cre_dt) < trunc(sysdate) " &
                              "  and std_multi_co2.isvalidstr('" & Session("EMP_INIT") & "', s.store_cd) = 'Y' " &
                              "  and std_orafin_ivc.isPrePaidVe(s.store_cd,p.ve_cd) <> 'Y' " &
                              "  and std_orafin_ivc.ivcExists(s.co_cd,p.ve_cd,x.invoicenumber) = 'Y' "
        GridView1.DataSource = ""

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        ds = New DataSet

        objSql.CommandText = " select s.co_cd CO_CD " &
                             " ,p.ve_cd VE_CD " &
                             " ,x.invoicenumber IVC_CD " &
                             " ,x.purchaseordernumber PO_CD " &
                             " ,nvl(x.netinvoiceamount,0) IVC_AMT " &
                             " ,std_orafin_ivc.getMatchDt(s.co_cd,p.ve_cd,x.invoicenumber) MATCH_DT " &
                             " ,std_orafin_ivc.getEmpCdOp(s.co_cd,p.ve_cd,x.invoicenumber) EMP_CD_OP " &
                             " from xml810_hdr x, store s, po p " &
                             "  where p.po_cd = x.purchaseordernumber " &
                             "    and nvl(p.store_cd,'x') = s.store_cd " &
                             "    and x.invoicenumber like " & " '" & v_ivc & "" & "%'" &
                             "    and x.purchaseordernumber like " & "'" & v_po & "" & "%'" &
                             "    and p.ve_cd = decode('" & srch_ve_cd.SelectedItem.Value.ToString() & "',null,p.ve_cd,'" & srch_ve_cd.SelectedItem.Value.ToString() & "')" &
                             v_common_cond &
                             " order by s.co_cd,p.ve_cd,x.invoicenumber,x.purchaseordernumber "

        objSql.Connection = conn

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        Try
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count
            If numrows > 0 Then
                btn_deselect.Visible = True
                btn_forceMatch.Visible = True
            Else
                btn_deselect.Visible = False
                btn_forceMatch.Visible = False
            End If
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                GridView1.DataSource = ds
                GridView1.DataBind()
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GridView1.DataSource = ds
                GridView1.DataBind()
                lbl_msg.Text = "No data to display"
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        Try
            conn.Close()
        Catch ex As Exception

        End Try

    End Sub
   
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    Protected Sub ForceMatchIvc(ByVal p_co_cd As String, ByVal p_ve_cd As String, ByVal p_ivc_cd As String)

        Dim hasError As Boolean = False
        Dim errorMsg As String = ""

        Dim connstring As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        connstring = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connstring)
        objConnection.Open()
        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()


        Try
            myCMD.Connection = objConnection
            myCMD.CommandText = "std_orafin_ivc.ForceMatchIvc"
            myCMD.CommandType = CommandType.StoredProcedure

            myCMD.Parameters.Add(New OracleParameter("p_emp_init", OracleType.VarChar)).Value = Session("EMP_INIT")
            myCMD.Parameters.Add(New OracleParameter("p_co_cd", OracleType.VarChar)).Value = p_co_cd
            myCMD.Parameters.Add(New OracleParameter("p_ve_cd", OracleType.VarChar)).Value = p_ve_cd
            myCMD.Parameters.Add(New OracleParameter("p_ivc_cd", OracleType.VarChar)).Value = p_ivc_cd

            myCMD.Parameters.Add("p_stat", OracleType.VarChar, 200).Direction = ParameterDirection.Output


            myCMD.ExecuteNonQuery()

            objConnection.Close()
        Catch
            objConnection.Close()
            Throw New Exception("Error in call to std_orafin_ivc.ForceMatchIvc")
            Exit Sub
        End Try

        If myCMD.Parameters("p_stat").Value.ToString & "" = "" Then
            populate_gridview1()
            lbl_msg2.Text = "Update is done"
        Else
            lbl_msg2.Text = myCMD.Parameters("p_stat").Value.ToString & ""
        End If

    End Sub
    Protected Sub btn_deselect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_deselect.Click

        Try
            GridView1.Selection.UnselectAll()
        Catch ex As Exception

        End Try
        
    End Sub
    Protected Sub btn_forceMatch_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_forceMatch.Click

        Dim rows As Integer = GridView1.VisibleRowCount
        Dim i As Integer
        For i = 0 To rows - 1

            Try

                If GridView1.Selection.IsRowSelected(i) Then
                    Dim v_co_cd As String = GridView1.GetRowValues(i, "CO_CD").ToString() & ""
                    Dim v_ve_cd As String = GridView1.GetRowValues(i, "VE_CD").ToString() & ""
                    Dim v_ivc_cd As String = GridView1.GetRowValues(i, "IVC_CD").ToString() & ""
                    ForceMatchIvc(v_co_cd, v_ve_cd, v_ivc_cd)
                    GridView1.Selection.UnselectRow(i)
                End If
            Catch ex As Exception

            End Try
        Next







    End Sub
    Protected Sub ASPxGridView1_CommandButtonInitialize(ByVal sender As Object, ByVal e As ASPxGridViewCommandButtonEventArgs)

        If e.VisibleIndex < 0 Then
            Exit Sub
        End If

        If GridView1.GetRowValues(e.VisibleIndex, "MATCH_DT") & "" <> "" Then
            e.Visible = False
        Else
            e.Visible = True
        End If

    End Sub


End Class
