Imports System.Data.OracleClient

Partial Class CommCustomerInquiry
    Inherits POSBasePage
    Dim running_balance2 As Double = 0
    Dim running_balance As Double = 0

    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder") = SortOrder

    End Function

    Sub MyDataGrid_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        MyDataGrid.CurrentPageIndex = 0 'To sort from top
        BindData(SortOrder(E.SortExpression).ToString()) 'Rebind our DataGrid

    End Sub

    Sub BindData(ByVal SortField As String)

        running_balance = 0
        'Setup Session Cache for different users         
        Dim Source As DataView
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim objConnect As OracleConnection
        Dim myDataAdapter As OracleDataAdapter
        Dim str_cust As String = Request("cust_cd")
        Dim returnFrom As String = Request("returnFrom")   ' 24-aug-16

        If Not String.IsNullOrWhiteSpace(returnFrom) Then   ' 24-aug-14

            If "SOM".Equals(returnFrom) Then
                str_cust = Session("cust_cd_saved")
            End If
        End If
        Dim str_emp_init As String = LeonsBiz.GetEmpInit(Session("EMP_CD"))
        Dim credit_limit As Double = get_limit(str_cust)
        Dim net_ots As Double = get_net_ots(txt_co_cd.Text, str_cust)
        Dim fi_amt As Double = get_fi(str_emp_init, str_cust)

        objConnect = DisposablesManager.BuildOracleConnection

        If credit_limit > 0 Then   'lucy 18-aug-16

            objConnect.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

            sql = "select a.ar_trn_pk, a.ivc_cd, a.adj_ivc_cd, a.post_dt, a.trn_tp_cd, "
            'sabrina R1999 DCS
            'sql = sql & "a.mop_cd, a.stat_cd, nvl(DECODE(T.DC_CD,'C',-A.AMT,'D',A.AMT,0),0) AS AMT,  " ' Daniela comment
            sql = sql & "a.mop_cd, a.stat_cd, nvl(DECODE(T.DC_CD,'C',-A.AMT,'D',A.AMT,0),0) AS AMT, decode(a.mop_cd,'TDF', a.des,'DCS',a.des, '') plan "
            sql = sql & "from "
            sql = sql & "ar_trn_tp t "
            sql = sql & ", ar_trn a "
            sql = sql & "where  a.trn_tp_cd = t.trn_tp_cd (+) "
            ' sql = sql & "and a.cust_cd='" & Request("cust_cd") & "' "
            sql = sql & "and a.cust_cd='" & str_cust & "' "
            If txt_co_cd.Text & "" <> "" Then
                sql = sql & "and a.co_cd='" & txt_co_cd.Text & "' "
            End If
            myDataAdapter = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
            ds = New DataSet()
            sAdp = DisposablesManager.BuildOracleDataAdapter(sql, objConnect)
            sAdp.Fill(ds)
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count

            myDataAdapter.Fill(ds, "MyDataGrid")

            'Assign sort expression to Session              
            Session("SortOrder") = SortField

            'Setup DataView for Sorting              
            Source = ds.Tables(0).DefaultView

            'Insert DataView into Session           
            Session("dgCache") = Source

            If numrows = 0 Then
                MyDataGrid.Visible = False
                lbl_Warning.Visible = True
                lbl_Warning.Text = "No records were found matching your search criteria." & vbCrLf
                btnSubmit.Visible = True
            Else
                Source.Sort = SortField
                MyDataGrid.DataSource = Source
                MyDataGrid.DataBind()
                lbl_Warning.Visible = False
            End If
            'Close connection           
            objConnect.Close()
            txt_acct_balance.Text = FormatCurrency(running_balance, 2)
            txt_credit_limit.Text = FormatCurrency(credit_limit, 2)   'lucy 18-aug-16
            txt_net_open_to_spend.Text = FormatCurrency(net_ots, 2)   'lucy 18-aug-16
            txt_open_fi_amt.Text = FormatCurrency(fi_amt, 2)   'lucy 18-aug-16

        End If
    End Sub

    Public Sub get_cust_info_from_db()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim custcd As String
        Dim warning_list As String
        warning_list = ""
        custcd = UCase(Request.QueryString("cust_cd"))

        Dim returnFrom As String = Request("returnFrom")   ' 24-aug-16
        If Not String.IsNullOrWhiteSpace(returnFrom) Then   ' 24-aug-14


            If "SOM".Equals(returnFrom) Then
                custcd = Session("cust_cd_saved")
            End If
        End If

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Try
            sql = "SELECT FNAME, LNAME, CUST_CD, HOME_PHONE, BUS_PHONE, ADDR1,ADDR2, CITY, ST_CD, ZIP_CD, CUST_TP_CD, EMAIL_ADDR, CORP_NAME FROM CUST WHERE cust_cd = :CUSTCD"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.Parameters.Add(":CUSTCD", OracleType.VarChar)
            objSql.Parameters(":CUSTCD").Value = custcd

            conn.Open()
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read() Then
                If MyDataReader.Item("FNAME") & "" <> "" Then
                    txtFName.Text = MyDataReader.Item("FNAME")
                End If
                If MyDataReader.Item("LNAME") & "" <> "" Then
                    txtLName.Text = MyDataReader.Item("LNAME")
                End If
                If MyDataReader.Item("HOME_PHONE") & "" <> "" Then
                    txtHomePhone.Text = MyDataReader.Item("HOME_PHONE")
                End If
                If MyDataReader.Item("BUS_PHONE") & "" <> "" Then
                    txtBusPhone.Text = MyDataReader.Item("BUS_PHONE")
                End If
                If MyDataReader.Item("CUST_CD") & "" <> "" Then
                    txtCustCode.Text = MyDataReader.Item("CUST_CD")
                End If
                If MyDataReader.Item("ADDR1") & "" <> "" Then
                    txtAddr.Text = MyDataReader.Item("ADDR1")
                End If
                If MyDataReader.Item("city") & "" <> "" Then
                    txt_city.Text = MyDataReader.Item("city")
                End If
                If MyDataReader.Item("st_cd") & "" <> "" Then
                    txt_state.Text = MyDataReader.Item("st_cd")
                End If
                If MyDataReader.Item("zip_cd") & "" <> "" Then
                    txt_zip.Text = MyDataReader.Item("zip_cd")
                End If
                If MyDataReader.Item("CORP_NAME") & "" <> "" Then
                    txt_company.Text = MyDataReader.Item("CORP_NAME")
                End If
            End If
            'Close Connection 
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        'Close Connection 
        conn.Close()

        sql = "SELECT DES FROM CUST_WARN, CUST2CUST_WARN WHERE CUST_WARN.CUST_WARN_CD=CUST2CUST_WARN.CUST_WARN_CD AND CUST2CUST_WARN.CUST_CD=:CUSTCD"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.Parameters.Add(":CUSTCD", OracleType.VarChar)
        objSql.Parameters(":CUSTCD").Value = custcd
        'Add Parameters 
        'objSql.Parameters.AddWithValue("@productid", productid)

        Try
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read()
                warning_list = warning_list & "- " & MyDataReader.Item("des") & vbCrLf
            Loop
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        If warning_list & "" <> "" Then
            img_warn.Visible = True
            lbl_Warning.Visible = True
            lbl_Warning.Text = "PLEASE REVIEW"
            img_warn.ToolTip = warning_list
        Else
            lbl_Warning.Visible = False
            img_warn.Visible = False
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim CatchMe As Boolean

        CustTable.Visible = False
        CatchMe = False
        If txtHomePhone.Text & "" = "" And txt_company.Text & "" = "" And txt_city.Text & "" = "" And txt_state.Text & "" = "" And txt_zip.Text & "" = "" And txtAddr.Text & "" = "" And txtCustCode.Text & "" = "" And txtFName.Text & "" = "" And txtFName.Text & "" = "" And txtLName.Text & "" = "" Then
            CatchMe = True
        End If

        Session("lname") = UCase(txtLName.Text)
        Session("fname") = UCase(txtFName.Text)
        Session("addr") = UCase(txtAddr.Text)
        Session("custcd") = UCase(txtCustCode.Text)
        Session("bus_phone") = UCase(txtBusPhone.Text)
        Session("home_phone") = UCase(txtHomePhone.Text)
        Session("cust_city") = UCase(txt_city.Text)
        Session("cust_state") = UCase(txt_state.Text)
        Session("cust_zip") = UCase(txt_zip.Text)
        Session("corp_name") = UCase(txt_company.Text)

        If CatchMe = True Then
            lbl_errors.Visible = True
            lbl_errors.ForeColor = Color.Red
            lbl_errors.Text = "You must enter at least one search criteria!"
            CustTable.Visible = True
        Else
            Response.Redirect("Commprs.aspx?referrer=CommCustomerInquiry.aspx")  'lucy 18-aug-16, change prs.aspx to CommPrs.aspx which is copied and modified from prs.aspx
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Try
                txt_co_cd.Text = Session("CO_CD")
            Catch ex As Exception
                'Do Nothing
            End Try
            Dim returnFrom As String = Request("returnFrom")   ' 24-aug-16

            If Not String.IsNullOrWhiteSpace(returnFrom) Then


                If "SOM".Equals(returnFrom) Then
                    txtCustCode.Text = Session("cust_cd_saved")
                    get_cust_info_from_db()
                    BindData(Session("SortOrder_saved"))
                    btnSubmit.Visible = False
                End If
            Else


                If Request("cust_cd") & "" <> "" Then
                    Dim str_cust As String = Request("cust_cd")
                    Dim credit_limit As Double = get_limit(str_cust)



                    If IsNothing(Session("SortOrder")) Or IsDBNull(Session("SortOrder")) Then
                        get_cust_info_from_db()

                        BindData("post_dt asc")
                        btnSubmit.Visible = False
                    Else
                        get_cust_info_from_db()
                        BindData(Session("SortOrder"))
                        btnSubmit.Visible = False
                    End If

                End If
            End If        ' 24-aug-16
        End If

    End Sub

    Private Function IsAlternateDoc(ByVal docType As String) As Boolean
        Return AppConstants.Order.TYPE_CRM = docType OrElse
               AppConstants.Order.TYPE_MDB = docType OrElse
               AppConstants.Order.TYPE_MCR = docType
    End Function

    Protected Sub MyDataGrid_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles MyDataGrid.ItemDataBound

        If IsNumeric(e.Item.Cells(5).Text) Then
            Dim txt_qty As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_balance"), DevExpress.Web.ASPxEditors.ASPxLabel)
            txt_qty.Text = FormatNumber(running_balance + e.Item.Cells(5).Text, 2)
            running_balance = FormatNumber(running_balance + e.Item.Cells(5).Text, 2)
        End If
        ' Daniela comment add new column change
        'If Len(e.Item.Cells(12).Text) > 10 Then
        '    Dim delDocNum As String = e.Item.Cells(12).Text  'del_doc_num value
        '    'the hyperlink displays the original del-doc-num value, however when the document type
        '    'is an MCR/MDB/CRM and there is an available adj_ivc_cd, it will redirect to that instead
        '    If (Not String.IsNullOrEmpty(e.Item.Cells(13).Text) AndAlso IsAlternateDoc(e.Item.Cells(14).Text)) Then
        '        delDocNum = e.Item.Cells(13).Text
        '    End If
        '    Dim hpl_ivc As DevExpress.Web.ASPxEditors.ASPxHyperLink = CType(e.Item.FindControl("Hyperlink1"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
        '    hpl_ivc.NavigateUrl = "~/SalesOrderMaintenance.aspx?query_returned=Y&del_doc_num=" & delDocNum
        '    hpl_ivc.Text = e.Item.Cells(12).Text
        'End If
        If Len(e.Item.Cells(14).Text) > 10 Then
            Dim delDocNum As String = e.Item.Cells(14).Text  'del_doc_num value
            'the hyperlink displays the original del-doc-num value, however when the document type
            'is an MCR/MDB/CRM and there is an available adj_ivc_cd, it will redirect to that instead
            If (Not String.IsNullOrEmpty(e.Item.Cells(15).Text) AndAlso IsAlternateDoc(e.Item.Cells(16).Text)) Then
                delDocNum = e.Item.Cells(15).Text
            End If
            Dim hpl_ivc As DevExpress.Web.ASPxEditors.ASPxHyperLink = CType(e.Item.FindControl("Hyperlink1"), DevExpress.Web.ASPxEditors.ASPxHyperLink)
            '  hpl_ivc.NavigateUrl = "~/SalesOrderMaintenance.aspx?query_returned=Y&del_doc_num=" & delDocNum   'lucy need to check SOM+ 18-aug-16
            hpl_ivc.NavigateUrl = "~/SalesOrderMaintenance.aspx?referrer=CommCustomerInquiry.aspx&query_returned=Y&del_doc_num=" & delDocNum   ' 24-aug-16
            Session("cust_cd_saved") = txtCustCode.Text  ' 24-aug-16
            Session("SortOrder_saved") = Session("SortOrder")
            ' DataNavigateUrlFormatString="salesordermaintenance.aspx?referrer=salesbookofbusinessMgment.aspx&del_doc_num={0}&query_returned=Y">
            ' Daniela disable link for not SO
            If (e.Item.Cells(2).Text <> "SAL" AndAlso
                   e.Item.Cells(2).Text <> "MCR" AndAlso
                   e.Item.Cells(2).Text <> "MDC" AndAlso
                   e.Item.Cells(2).Text <> "CRM") Then
                hpl_ivc.Enabled = False
            End If
            hpl_ivc.Text = e.Item.Cells(14).Text
        End If
        If IsDate(e.Item.Cells(1).Text) Then
            Dim lbl_date As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_date"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_date) Then
                lbl_date.Text = FormatDateTime(e.Item.Cells(1).Text, DateFormat.ShortDate)
            End If
        End If
        If e.Item.Cells(2).Text & "" <> "" Then
            Dim lbl_trans As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_trans"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_trans) Then
                lbl_trans.Text = e.Item.Cells(2).Text
            End If
        End If
        If e.Item.Cells(3).Text & "" <> "" Then
            Dim lbl_status As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_status"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_status) Then
                lbl_status.Text = e.Item.Cells(3).Text
            End If
        End If
        If e.Item.Cells(4).Text & "" <> "" Then
            Dim lbl_mop As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_mop"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_mop) Then
                If e.Item.Cells(4).Text = "&nbsp;" Then
                    lbl_mop.Text = ""
                Else
                    lbl_mop.Text = e.Item.Cells(4).Text
                End If
            End If
        End If
        If e.Item.Cells(5).Text & "" <> "" Then
            Dim lbl_amt As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_amt"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_amt) Then
                lbl_amt.Text = e.Item.Cells(5).Text
            End If
        End If
        ' Daniela added
        If e.Item.Cells(6).Text & "" <> "" Then
            Dim lbl_plan As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_plan"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_plan) Then
                If e.Item.Cells(6).Text = "&nbsp;" Then
                    lbl_plan.Text = ""
                Else
                    lbl_plan.Text = e.Item.Cells(6).Text
                End If
            End If
        End If ' end Daniela

    End Sub

    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        txtFName.Text = ""
        txtLName.Text = ""
        txtHomePhone.Text = ""
        txtBusPhone.Text = ""
        txtCustCode.Text = ""
        txtAddr.Text = ""
        txt_company.Text = ""
        txt_city.Text = ""
        txt_state.Text = ""
        txt_zip.Text = ""
        txt_acct_balance.Text = "$0.00"
        btnSubmit.Visible = True
        MyDataGrid.Visible = False
        txt_credit_limit.Text = Nothing
        txt_net_open_to_spend.Text = Nothing
        txt_open_fi_amt.Text = Nothing

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Public Function get_limit(ByRef p_cust_cd As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_id As String = ""
        Dim p_id As String
        Dim p_amt As Double

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_otsr.getCRLimit"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_cus", OracleType.VarChar)).Value = p_cust_cd
        
        myCMD.Parameters.Add("p_amt", OracleType.Number, 50).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_amt").Value) = False Then
                p_amt = myCMD.Parameters("p_amt").Value
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return p_amt
    End Function

    Public Function get_net_ots(ByRef p_co_cd As String, ByRef p_cust_cd As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_id As String = ""
        Dim p_id As String
        Dim p_amt As Double

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_otsr.getNetOTS"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_coCd", OracleType.VarChar)).Value = p_co_cd
        myCMD.Parameters.Add(New OracleParameter("p_cus", OracleType.VarChar)).Value = p_cust_cd
        myCMD.Parameters.Add("p_amt", OracleType.Number, 50).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_amt").Value) = False Then
                p_amt = myCMD.Parameters("p_amt").Value
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return p_amt
    End Function

    Public Function get_fi(ByRef p_emp_init As String, ByRef p_cust_cd As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_id As String = ""
        Dim p_id As String
        Dim p_amt As Double

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_otsr.get7DaysFi"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_empInit", OracleType.VarChar)).Value = p_emp_init
        myCMD.Parameters.Add(New OracleParameter("p_cus", OracleType.VarChar)).Value = p_cust_cd
        myCMD.Parameters.Add("p_amt", OracleType.Number, 50).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_amt").Value) = False Then
                p_amt = myCMD.Parameters("p_amt").Value
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally
            objConnection.Close()

        End Try
        Return p_amt
    End Function

End Class
