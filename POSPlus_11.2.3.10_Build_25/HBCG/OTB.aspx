<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OTB.aspx.vb" Inherits="OTB" %>

<%@ Register Assembly="DevExpress.XtraReports.v13.2.Web, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body class="style5" leftmargin="30" topmargin="30" style="margin-top: 1px; margin-left: 30px">
    <form id="form1" runat="server">
        <div>
            &nbsp;<br />
            <br />
            <table style="position: relative; top: 0px" class="style5">
             <tr>
                   <td  style="width: 209px">
                        <asp:Label ID="lblProvider" runat="server" Text="Provider:"></asp:Label>
                    </td>
                    <td  style="height: 21px; width: 238px;">                                               
                        <asp:DropDownList ID="cboProviders" runat="server" AutoPostBack="True" 
                            AppendDataBoundItems="True" CssClass="style5" Width="145px">
                        </asp:DropDownList>
                    </td>
               </tr>
                <tr>
                    <td nowrap style="width: 209px">
                        Account Number:
                    </td>
                    <td colspan="3" style="width: 238px">
                        <asp:TextBox ID="txt_account" runat="server" Width="140px" CssClass="style5"  MaxLength="20"></asp:TextBox>
                        <br /><asp:RequiredFieldValidator ID="RequiredAccountValidator" runat="server" ErrorMessage="Account Number is required." ControlToValidate="txt_account"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>                        
                </tr>

                <tr>
                    <td nowrap style="width: 209px">
                        Social Security Number:
                    </td>
                    <td colspan="3" style="width: 238px">
                        <asp:TextBox ID="txt_ssn" runat="server" Width="140px" CssClass="style5" MaxLength="9"></asp:TextBox>
                        <br /><asp:RegularExpressionValidator ID="SSNExpressionValidator" runat="server" ValidationExpression="\d{3}\d{2}\d{4}"  ControlToValidate="txt_ssn"  ErrorMessage="Social Security # must consist of 9 numeric characters."  Display="Dynamic"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 209px">
                        Zip Code:</td>
                    <td colspan="3" style="width: 238px">
                        <asp:TextBox ID="txt_zip" runat="server" Width="140px" CssClass="style5" 
                            MaxLength="5"></asp:TextBox>
                        <br /><asp:RegularExpressionValidator id="PostalCodeExpressionValidator" runat="server" ValidationExpression="\d{5}"  ErrorMessage="Zip Code must be 5 digits." ControlToValidate="txt_zip" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <asp:Button ID="btn_save_exit" runat="server" Text="View Open to Buy" Width="144px"
            CssClass="style5" />
        
        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <br /><br />
        <asp:Label ID="Label1" runat="server" 
            Text="*The Account Number and at least one other field is required." ></asp:Label>
        <br />
        <br />
        <br /><br />
        <asp:Label ID="lbl_response" runat="server" Width="452px"></asp:Label><br />
        <br />

    </form>
</body>
</html>
