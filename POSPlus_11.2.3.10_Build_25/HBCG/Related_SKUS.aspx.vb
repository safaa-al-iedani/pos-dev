Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.OracleClient
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Microsoft.VisualBasic

Partial Class Related_SKUS
    Inherits POSBasePage

    Private theSkuBiz As SKUBiz = New SKUBiz()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim SKUS As String = Request("itm_cd")

            If SKUS & "" = "" Then
                lbl_Desc.Text = "No related SKUS were found for this order."
                GridView1.Visible = False
            Else
                lbl_Desc.Text = "The following items compliment your selection.  Would you like to add them to your order?"
                bindgrid()
            End If
        End If
    End Sub

    Public Sub Add_Packages(ByVal PKG_SKUS As String)

        Dim cmdInsertItems As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim objConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim MyTable As DataTable
        Dim sql As String
        Dim sAdp As OracleDataAdapter
        Dim ds As DataSet
        Dim numrows As Integer
        Dim loop1 As Integer
        Dim sku As String = ""
        Dim VeCd As String = ""
        Dim mnrcd As String = ""
        Dim catcd As String = ""
        Dim spiff As String = ""
        Dim point_size As String = ""
        Dim retprc As Double
        Dim adv_prc As Double
        Dim cost As Double
        Dim qty As Double
        Dim vsn As String = ""
        Dim comm_cd As String = ""
        Dim des As String = ""
        Dim siz As String = ""
        Dim finish As String = ""
        Dim cover As String = ""
        Dim grade As String = ""
        Dim prom_prc As String = ""
        Dim stylecd As String = ""
        Dim itm_tp_cd As String = ""
        Dim treatable As String = ""
        Dim tax_cd As String = ""
        Dim tax_pct As String = ""
        Dim taxable_amt As String = ""

        Dim thistransaction As OracleTransaction
        Dim dropped_SKUS As String
        Dim dropped_dialog As String
        Dim User_Questions As String
        Dim objSql As OracleCommand
        Dim objSql2 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim MyDataReader2 As OracleDataReader
        User_Questions = ""
        dropped_SKUS = ""
        dropped_dialog = ""

        objConnection.Open()

        conn.Open()
        'Populate the table for any package SKUS that were created
        Dim PKG_GO As Boolean
        PKG_GO = False
        If PKG_SKUS & "" <> "" Then

            If Right(PKG_SKUS, 1) = "," Then PKG_SKUS = Left(PKG_SKUS, Len(PKG_SKUS) - 1)

            'If we are to populate the retail amounts, we need to calculate based on cost percentages
            'If ConfigurationManager.AppSettings("package_breakout").ToString = "Y" Then
            sql = "SELECT ITM_CD, REPL_CST, PKG_ITM_CD from itm, package where pkg_itm_cd in (" & PKG_SKUS & ") "
            sql = sql & "and package.cmpnt_itm_cd=itm.itm_cd order by pkg_itm_cd, seq"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read()
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "insert into PACKAGE_BREAKOUT (session_id,PKG_SKU,COMPONENT_SKU,COST) values ('" & Session.SessionID.ToString.Trim & "','" & MyDataReader.Item("PKG_ITM_CD").ToString & "','" & MyDataReader.Item("ITM_CD").ToString & "'," & MyDataReader.Item("REPL_CST").ToString & ")"
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()
            sql = "SELECT ITM_CD, RET_PRC, ADV_PRC from itm where itm_cd in (" & PKG_SKUS & ") "
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read()

                retprc = If(IsNumeric(MyDataReader.Item("RET_PRC")), CDbl(MyDataReader.Item("RET_PRC")), 0)
                adv_prc = If(IsNumeric(MyDataReader.Item("ADV_PRC")), CDbl(MyDataReader.Item("ADV_PRC")), 0)

                retprc = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                     SessVar.wrStoreCd,
                                                     SessVar.custTpPrcCd,
                                                     Session("tran_dt"),
                                                     MyDataReader.Item("ITM_CD"), retprc)

                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_RETAIL=" & retprc & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & MyDataReader.Item("ITM_CD") & "'"
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()
            sql = "SELECT pkg_ITM_CD, SUM(REPL_CST) As TOTAL_COST from itm, package where pkg_itm_cd in (" & PKG_SKUS & ") and package.cmpnt_itm_cd=itm.itm_cd GROUP BY pkg_ITM_CD"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Calculate the total cost
            Do While MyDataReader.Read()
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = "UPDATE PACKAGE_BREAKOUT SET TOTAL_COST=" & MyDataReader.Item("TOTAL_COST") & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU = '" & MyDataReader.Item("PKG_ITM_CD") & "'"
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()
                PKG_GO = True
            Loop
            MyDataReader.Close()
            If PKG_GO = True Then
                Dim PERCENT_OF_TOTAL As Double
                Dim NEW_RETAIL As Double
                Dim TOTAL_NEW_RETAIL As Double
                Dim TOTAL_RETAIL As Double
                Dim LAST_SKU As String
                Dim LAST_PKG_SKU As String

                'Calculate the percentage based on cost/total cost
                sql = "SELECT * FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' and PKG_SKU in (" & PKG_SKUS & ") ORDER BY PKG_SKU"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                Do While MyDataReader2.Read()
                    PERCENT_OF_TOTAL = CDbl(MyDataReader2.Item("COST").ToString) / CDbl(MyDataReader2.Item("TOTAL_COST").ToString)
                    NEW_RETAIL = PERCENT_OF_TOTAL * CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                    TOTAL_NEW_RETAIL = TOTAL_NEW_RETAIL + NEW_RETAIL
                    TOTAL_RETAIL = CDbl(MyDataReader2.Item("TOTAL_RETAIL").ToString)
                    LAST_SKU = MyDataReader2.Item("COMPONENT_SKU").ToString
                    LAST_PKG_SKU = MyDataReader2.Item("PKG_SKU").ToString
                    thistransaction = objConnection.BeginTransaction
                    If PERCENT_OF_TOTAL.ToString = "NaN" Then PERCENT_OF_TOTAL = 0
                    With cmdInsertItems
                        .Transaction = thistransaction
                        .Connection = objConnection
                        .CommandText = "UPDATE PACKAGE_BREAKOUT SET PERCENT_OF_TOTAL = " & PERCENT_OF_TOTAL & ", NEW_RETAIL=" & Math.Round(NEW_RETAIL, 2, MidpointRounding.AwayFromZero) & " WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND PKG_SKU='" & MyDataReader2.Item("PKG_SKU").ToString & "' AND COMPONENT_SKU='" & MyDataReader2.Item("COMPONENT_SKU").ToString & "'"
                    End With
                    cmdInsertItems.ExecuteNonQuery()
                    thistransaction.Commit()
                Loop

                MyDataReader2.Close()
                objSql2.Dispose()
            End If

            ' End If
            sql = "SELECT ITM_CD, VE_CD, MNR_CD, CAT_CD, package.PKG_ITM_CD, package.RET_PRC, PACKAGE.QTY, VSN, DES, SIZ, FINISH, COVER, GRADE, STYLE_CD, ITM_TP_CD, TREATABLE, ITM.DROP_CD, ITM.DROP_DT, ITM.USER_QUESTIONS, POINT_SIZE, REPL_CST, COMM_CD, SPIFF from itm, package where pkg_itm_cd in (" & PKG_SKUS & ") "
            sql = sql & "and package.cmpnt_itm_cd=itm.itm_cd order by pkg_itm_cd, seq"
            sAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
            ds = New DataSet
            sAdp.Fill(ds)
            MyTable = New DataTable
            MyTable = ds.Tables(0)
            numrows = MyTable.Rows.Count
            Dim Session_String As String = ""
            Dim PKG_ITM_CD As String = ""

            Session_String = Session.SessionID.ToString()

            For loop1 = 0 To numrows - 1
                PKG_ITM_CD = MyTable.Rows(loop1).Item("PKG_ITM_CD").ToString
                sku = MyTable.Rows(loop1).Item("ITM_CD").ToString
                dropped_SKUS = MyTable.Rows(loop1).Item("DROP_DT").ToString
                If MyTable.Rows(loop1).Item("USER_QUESTIONS") & "" <> "" Then
                    User_Questions = User_Questions & MyTable.Rows(loop1).Item("USER_QUESTIONS").ToString & "<br />"
                End If
                If dropped_SKUS & "" <> "" Then
                    If FormatDateTime(dropped_SKUS, DateFormat.ShortDate) < Now() Then
                        dropped_dialog = dropped_dialog & "SKU " & sku & "(" & MyTable.Rows(loop1).Item("VSN").ToString & ") was dropped on " & dropped_SKUS & "<br />"
                    End If
                End If
                VeCd = MyTable.Rows(loop1).Item("VE_CD").ToString
                mnrcd = MyTable.Rows(loop1).Item("MNR_CD").ToString
                catcd = MyTable.Rows(loop1).Item("CAT_CD").ToString
                itm_tp_cd = MyTable.Rows(loop1).Item("ITM_TP_CD").ToString
                treatable = MyTable.Rows(loop1).Item("TREATABLE").ToString
                If MyTable.Rows(loop1).Item("REPL_CST") & "" <> "" Then
                    cost = CDbl(MyTable.Rows(loop1).Item("REPL_CST").ToString)
                Else
                    cost = 0
                End If
                If IsNumeric(MyTable.Rows(loop1).Item("QTY").ToString) Then
                    qty = MyTable.Rows(loop1).Item("QTY").ToString
                Else
                    qty = 1
                End If
                sql = "SELECT NEW_RETAIL FROM PACKAGE_BREAKOUT WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND COMPONENT_SKU='" & sku & "' AND PKG_SKU='" & MyTable.Rows(loop1).Item("PKG_ITM_CD").ToString & "'"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                If ConfigurationManager.AppSettings("package_breakout").ToString = "Y" Then
                    If MyDataReader2.Read() Then
                        retprc = MyDataReader2.Item("NEW_RETAIL")
                        taxable_amt = MyDataReader2.Item("NEW_RETAIL")
                    Else
                        retprc = 0
                        taxable_amt = 0
                    End If
                Else
                    If MyDataReader2.Read() Then
                        taxable_amt = MyDataReader2.Item("NEW_RETAIL")
                    Else
                        taxable_amt = 0
                    End If
                    If IsNumeric(MyTable.Rows(loop1).Item("RET_PRC")) Then
                        retprc = CDbl(MyTable.Rows(loop1).Item("RET_PRC"))
                    Else
                        retprc = 0
                    End If
                End If
                If IsNumeric(retprc) And IsNumeric(qty) Then
                    retprc = FormatNumber((retprc / qty), 2)
                End If
                If IsNumeric(taxable_amt) And IsNumeric(qty) Then
                    taxable_amt = FormatNumber((taxable_amt / qty), 2)
                End If
                MyDataReader2.Close()
                objSql2.Dispose()
                vsn = MyTable.Rows(loop1).Item("VSN").ToString
                If MyTable.Rows(loop1).Item("COMM_CD") & "" <> "" Then
                    comm_cd = MyTable.Rows(loop1).Item("COMM_CD").ToString
                Else
                    comm_cd = ""
                End If
                If MyTable.Rows(loop1).Item("DES").ToString & "" <> "" Then
                    des = MyTable.Rows(loop1).Item("DES").ToString
                End If
                If MyTable.Rows(loop1).Item("POINT_SIZE").ToString & "" <> "" Then
                    point_size = MyTable.Rows(loop1).Item("POINT_SIZE").ToString
                End If
                If MyTable.Rows(loop1).Item("SPIFF").ToString & "" <> "" Then
                    If IsNumeric(MyTable.Rows(loop1).Item("SPIFF").ToString) Then
                        spiff = MyTable.Rows(loop1).Item("SPIFF").ToString
                    Else
                        spiff = 0
                    End If
                Else
                    spiff = 0
                End If
                If point_size & "" = "" Then point_size = "0"

                If itm_tp_cd = "INV" Then
                    Check_Warranty(sku)
                End If

                prom_prc = FindPromotions(sku)
                If IsNumeric(prom_prc) Then
                    If CDbl(prom_prc) < retprc Then
                        retprc = CDbl(prom_prc)
                    End If
                End If
                'Determine if the select item should be taxed.
                If IsTaxable(itm_tp_cd) AndAlso String.IsNullOrEmpty(Session("tet_cd")) Then
                    tax_cd = Session("TAX_CD")
                    tax_pct = Session("TAX_RATE")
                Else
                    tax_cd = 0
                    tax_pct = 0
                    taxable_amt = 0
                End If

                'Insert the row_id for the package parent SKU
                Dim row_id As String = ""
                sql = "SELECT ROW_ID FROM TEMP_ITM WHERE SESSION_ID = '" & Session.SessionID.ToString.Trim & "' AND ITM_CD='" & PKG_ITM_CD & "' AND PACKAGE_PARENT IS NULL ORDER BY ROW_ID DESC"
                objSql2 = DisposablesManager.BuildOracleCommand(sql, objConnection)
                MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

                If MyDataReader2.Read() Then
                    row_id = MyDataReader2.Item("ROW_ID")
                End If

                If Not IsNumeric(tax_pct) Then tax_pct = 0
                Dim Calc_Ret As Double

                Calc_Ret = taxable_amt * (tax_pct / 100)

                If itm_tp_cd = "PKG" And ConfigurationManager.AppSettings("package_breakout").ToString = "Y" Then retprc = 0


                sql = "INSERT INTO temp_itm (session_id,ITM_CD,VE_CD,MNR_CD,CAT_CD,RET_PRC,ORIG_PRC, MANUAL_PRC,VSN,DES,SIZ,FINISH,COVER,GRADE," &
                    " STYLE_CD,ITM_tp_CD,TREATABLE,DEL_PTS, COST, COMM_CD, tax_cd, tax_pct, taxable_amt, SPIFF, QTY, PACKAGE_PARENT "
                sql = sql & ", SLSP1, SLSP1_PCT, TAX_AMT"
                If Session("slsp2") & "" <> "" Then
                    sql = sql & ", SLSP2, SLSP2_PCT"
                End If
                sql = sql & ")  values ('" & Session.SessionID.ToString.Trim & "','" & sku & "','" & VeCd & "','" & mnrcd & "','" & catcd & "'," & retprc & "," & retprc & retprc & ",'" &
                      Replace(vsn, "'", "''") & "','" & Replace(des, "'", "''") & "','" & Replace(siz, "'", "''") & "','" & Replace(finish, "'", "''") & "','" &
                      Replace(cover, "'", "''") & "','" & Replace(grade, "'", "''") & "','" & stylecd & "','" & itm_tp_cd & "','" & treatable & "'," & point_size & "," & cost & ",'" &
                      comm_cd & "','" & tax_cd & "'," & tax_pct & "," & taxable_amt & "," & spiff & "," & qty & "," & row_id
                sql = sql & ",'" & Session("slsp1") & "'," & Session("pct_1") & ",ROUND(" & Calc_Ret & ",2)"
                If Session("slsp2") & "" <> "" Then
                    sql = sql & ",'" & Session("slsp2") & "'," & Session("pct_2")
                End If
                sql = sql & ")"
                thistransaction = objConnection.BeginTransaction
                With cmdInsertItems
                    .Transaction = thistransaction
                    .Connection = objConnection
                    .CommandText = sql
                End With
                cmdInsertItems.ExecuteNonQuery()
                thistransaction.Commit()

            Next

            'We're done, delete out all of the records to avoid duplication
            thistransaction = objConnection.BeginTransaction

            sql = "DELETE FROM PACKAGE_BREAKOUT WHERE SESSION_ID='" & Session.SessionID.ToString.Trim & "'"
            With cmdInsertItems
                .Transaction = thistransaction
                .Connection = objConnection
                .CommandText = sql
            End With
            cmdInsertItems.ExecuteNonQuery()
            thistransaction.Commit()

        End If

        conn.Close()
        objConnection.Close()

    End Sub

    Private Sub Check_Warranty(ByVal itmCd As String)

        Dim warrantyFound As Boolean = False
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim oAdp As OracleDataAdapter
        Dim dset As New DataSet
        Dim dtbl As DataTable
        Dim sql As String

        sql = "SELECT WAR_ITM_CD, WAR_RET_PRC, ITM.VSN  || ' (' || ITM.WARR_DAYS || ' DAYS) ' AS VSN FROM ITM, ITM2ITM_WARR "
        sql = sql & "WHERE(ITM2ITM_WARR.WAR_ITM_CD = ITM.ITM_CD) AND ITM2ITM_WARR.ITM_CD IN('" & itmCd & "') ORDER BY ITM.WARR_DAYS"

        Try
            conn.Open()
            oAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
            oAdp.Fill(dset)
            dtbl = dset.Tables(0)

            For idx As Integer = 0 To dtbl.Rows.Count - 1
                If dtbl.Rows(idx).Item("war_itm_cd").ToString & "" <> "" Then
                    warrantyFound = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            Throw ex
        Finally
            conn.Close()
        End Try

        If warrantyFound = True Then
            Dim scrpt As String = "<script type='text/javascript'>window.open('Warranties.aspx?itm_cd=" & itmCd & "','frmTest" & itmCd & "','height=390px,width=560px,top=50,left=50,status=no,toolbar=no,menubar=no,scrollbars=yes');</script>"
            ClientScript.RegisterStartupScript(Me.GetType(), "AvisoScript" & itmCd, scrpt)
        End If

    End Sub

    Private Function FindPromotions(ByVal sku As String) As String

        Dim promoPrice As String = ""
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim dbReader As OracleDataReader
        Dim sql As String

        'checks on the Store
        sql = "SELECT PRC FROM PROM_PRC_TP, STORE_PROM_PRC WHERE PROM_PRC_TP.PRC_CD=STORE_PROM_PRC.PRC_CD "
        sql = sql & "AND ITM_CD='" & sku & "' AND STORE_CD='" & Session("store_cd") & "' AND EFF_DT<=TO_DATE('"
        sql = sql & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') AND END_DT>=TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') "
        sql = sql & "ORDER BY PRC"

        Try
            conn.Open()
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)
            dbReader = DisposablesManager.BuildOracleDataReader(cmd)

            If (dbReader.Read) Then
                promoPrice = dbReader.Item("PRC").ToString
            Else
                'checks on the Store Group
                sql = "SELECT PRC FROM PROM_PRC_TP, STORE_GRP_PROM_PRC  WHERE PROM_PRC_TP.PRC_CD=STORE_GRP_PROM_PRC .PRC_CD "
                sql = sql & "AND ITM_CD='" & sku & "' AND STORE_GRP_CD IN (SELECT STORE_GRP_CD FROM STORE_GRP$STORE WHERE STORE_CD='" & Session("store_cd") & "') AND EFF_DT<=TO_DATE('"
                sql = sql & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') AND END_DT>=TO_DATE('" & FormatDateTime(Today.Date, 2) & "','mm/dd/RRRR') "
                sql = sql & "ORDER BY PRC"
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                dbReader = DisposablesManager.BuildOracleDataReader(cmd)

                If (dbReader.Read) Then
                    promoPrice = dbReader.Item("PRC").ToString
                End If
            End If
            dbReader.Close()
        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try
    End Function


    Private Function IsTaxable(ByVal itmTpCd As String) As Boolean

        Dim needsToBeTaxed As Boolean = False

        If ("PKG".Equals(itmTpCd) OrElse "NLN".Equals(itmTpCd)) Then
            needsToBeTaxed = False

        ElseIf ("INV".Equals(itmTpCd)) Then
            needsToBeTaxed = True

        Else
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim cmd As OracleCommand
            Dim dbReader As OracleDataReader
            Dim sql As String = "select inventory from itm_tp where itm_tp_cd='" & itmTpCd & "' and inventory='Y'"

            Try
                conn.Open()
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                dbReader = DisposablesManager.BuildOracleDataReader(cmd)

                If (dbReader.Read) Then
                    needsToBeTaxed = True

                Else
                    ' verifies additional types
                    Dim tatQuery As String = String.Empty
                    Select Case itmTpCd
                        Case "LAB"
                            tatQuery = "LAB_TAX"
                        Case "FAB"
                            tatQuery = "FAB_TAX"
                        Case "NLT"
                            tatQuery = "NLT_TAX"
                        Case "WAR"
                            tatQuery = "WAR_TAX"
                        Case "PAR"
                            tatQuery = "PARTS_TAX"
                        Case "MIL"
                            tatQuery = "MILEAGE_TAX"
                    End Select

                    If (Not String.IsNullOrEmpty(tatQuery)) Then
                        sql = "SELECT TAX_CD$TAT.TAX_CD, Sum(TAT.PCT) AS SumOfPCT "
                        sql = sql & "FROM TAT, TAX_CD$TAT "
                        sql = sql & "WHERE TAT.EFF_DT <= SYSDATE And NVL(TAT.END_DT, TO_DATE('12/31/2049','mm/dd/RRRR')) >= SYSDATE "
                        sql = sql & "AND TAT.TAT_CD = TAX_CD$TAT.TAT_CD "
                        sql = sql & "AND TAX_CD$TAT.TAX_CD='" & Session("TAX_CD") & "' "
                        sql = sql & "AND TAT." & tatQuery & " = 'Y' "
                        sql = sql & "GROUP BY TAX_CD$TAT.TAX_CD "

                        cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                        dbReader = DisposablesManager.BuildOracleDataReader(cmd)

                        If (dbReader.Read) Then
                            needsToBeTaxed = True
                        End If
                    End If
                End If
                dbReader.Close()

            Catch ex As Exception
                Throw
            Finally
                conn.Close()
            End Try
        End If

        Return needsToBeTaxed
    End Function

    Private Sub ClearDataGrid(ByVal sender As Object, ByVal e As EventArgs)

        'Don't add any related SKUS to the order
        If Request("LEAD") = "TRUE" Then
            Response.Redirect("order.aspx?LEAD=TRUE")
        ElseIf Request("referrer") & "" <> "" Then
            Response.Redirect(Request("referrer"))
        Else
            Response.Redirect("order.aspx")
        End If
    End Sub

    Private Sub bindgrid()

        GridView1.DataSource = ""
        GridView1.Visible = True

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand
        Dim oAdp As OracleDataAdapter
        Dim dbReader As OracleDataReader
        Dim dset As New DataSet
        Dim sql As String
        Dim minorStr As String = String.Empty
        Dim majorStr As String = String.Empty
        Dim SKUS As String = Request("itm_cd")
        SKUS = Replace(SKUS, ",", "','")
        Dim spiffTable As String = If(SysPms.isSpiffBySKU, "itm2spiff_clndr_by_sku", "ITM$SPIFF_CLNDR")
        Dim sqlSB As StringBuilder = New StringBuilder()

        sqlSB.Append(" SELECT ITM2RSKU.RELATED_ITM_CD AS REL_SKU, ITM.ITM_TP_CD, ITM.DES, ITM.VSN, ITM.RET_PRC, ITM.VE_CD, ")
        sqlSB.Append(" ITM.MNR_CD, ITM.CAT_CD, ITM.SIZ, ITM.FINISH, ITM.COVER, ITM.GRADE, ITM.STYLE_CD, ")
        sqlSB.Append(" ITM.TREATABLE, ITM.FRAME_TP_ITM, ITM.DROP_CD, ITM.DROP_DT, ITM.USER_QUESTIONS, ")
        sqlSB.Append(" ITM.POINT_SIZE, ITM.REPL_CST, ITM.COMM_CD, NVL(SPIFF_AMT, SPIFF) AS SPIFF ")
        sqlSB.Append(" FROM ITM join ITM2RSKU on ITM2RSKU.RELATED_ITM_CD=ITM.ITM_CD  ")
        sqlSB.Append(" LEFT JOIN  " + spiffTable + " sp ON ITM2RSKU.RELATED_ITM_CD = SP.ITM_CD AND TO_DATE(sysdate)  BETWEEN TO_DATE(EFF_DT) AND TO_DATE(END_DT)  ")
        sqlSB.Append(If(SysPms.isSpiffBySKU, " ", (" AND STORE_CD = '" & Session("store_cd") & "' ")))
        sqlSB.Append(" WHERE  ")
        sqlSB.Append(" ITM2RSKU.ITM_CD IN('" & SKUS & "') ")
        sqlSB.Append(" ORDER BY 1")

        Try
            conn.Open()

            '=== Retrieves Related SKUs first
            oAdp = DisposablesManager.BuildOracleDataAdapter(sqlSB.ToString(), conn)
            oAdp.Fill(dset)

            '=== Finds out additional Related SKUs (if any) using the MINOR
            sql = "SELECT ITM.MNR_CD, INV_MNR.MJR_CD FROM ITM, INV_MNR WHERE INV_MNR.MNR_CD=ITM.MNR_CD AND ITM.ITM_CD IN('" & SKUS & "') GROUP BY INV_MNR.MJR_CD, ITM.MNR_CD"
            cmd = DisposablesManager.BuildOracleCommand(sql, conn)

            dbReader = DisposablesManager.BuildOracleDataReader(cmd)

            Do While dbReader.Read()
                minorStr = minorStr & "'" & dbReader.Item("MNR_CD").ToString & "',"
                majorStr = majorStr & "'" & dbReader.Item("MJR_CD").ToString & "',"
            Loop

            If (Not String.IsNullOrEmpty(minorStr)) Then
                SKUS = String.Empty
                sql = "SELECT ITM_CD FROM RELATED_SKUS WHERE REL_TP='MNR' AND REL_CD IN(" & Left(minorStr, Len(minorStr) - 1) & ") "
                sql = sql & "OR REL_TP='MJR' AND REL_CD IN(" & Left(majorStr, Len(majorStr) - 1) & ") GROUP BY ITM_CD"
                cmd = DisposablesManager.BuildOracleCommand(sql, conn)

                dbReader = DisposablesManager.BuildOracleDataReader(cmd)

                Do While dbReader.Read()
                    SKUS = SKUS & dbReader.Item("ITM_CD").ToString & ","
                Loop

                If (Not String.IsNullOrEmpty(SKUS)) Then

                    SKUS = Left(SKUS, Len(SKUS) - 1)
                    SKUS = Replace(SKUS, ",", "','")

                    Dim dset2 As New DataSet
                    sql = "SELECT ITM.ITM_CD AS REL_SKU, ITM.ITM_TP_CD, ITM.DES, ITM.VSN, ITM.RET_PRC, ITM.VE_CD, "
                    sql = sql & " ITM.MNR_CD, ITM.CAT_CD, ITM.SIZ, ITM.FINISH, ITM.COVER, ITM.GRADE, ITM.STYLE_CD, "
                    sql = sql & " ITM.TREATABLE, ITM.FRAME_TP_ITM, ITM.DROP_CD, ITM.DROP_DT, ITM.USER_QUESTIONS, "
                    sql = sql & " ITM.POINT_SIZE, ITM.REPL_CST, ITM.COMM_CD, ITM.SPIFF "
                    sql = sql & " FROM ITM "
                    sql = sql & " WHERE ITM_CD IN('" & SKUS & "') ORDER BY ITM.ITM_CD"

                    oAdp = DisposablesManager.BuildOracleDataAdapter(sql, conn)
                    oAdp.Fill(dset2, "ITM")

                    If dset.Tables(0).Rows.Count = 0 Then
                        dset = dset2
                    Else
                        dset.Merge(dset2)
                    End If
                End If
            End If
            dbReader.Close()

        Catch ex As Exception
            Throw
        Finally
            conn.Close()
        End Try

        ' Updates the Retail Price, by calling the Pricing API, so that correct pricing is displayed/used
        If (dset.Tables(0).Rows.Count > 0) Then
            Dim retPrice As Double
            Dim relSkusTbl As DataTable = dset.Tables(0)
            For Each dRow As DataRow In relSkusTbl.Rows
                retPrice = If(IsNumeric(dRow("RET_PRC")), CDbl(dRow("RET_PRC")), 0)
                retPrice = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                     SessVar.wrStoreCd,
                                                     SessVar.custTpPrcCd,
                                                     Session("tran_dt"),
                                                     dRow("REL_SKU"),
                                                     retPrice)
                dRow("RET_PRC") = retPrice
            Next
        End If

        GridView1.DataSource = dset
        GridView1.DataBind()

    End Sub

    Protected Sub NoThanks_Click(sender As Object, e As System.EventArgs) Handles NoThanks.Click

        Response.Write("<script language='javascript'>" & vbCrLf)
        Response.Write("var parentWindow = window.parent; ")
        Response.Write("parentWindow.SelectAndClosePopup1();")
        If Request("LEAD") = "TRUE" Then
            If Not (Request("preventMessage") Is Nothing) Then
                Response.Write("parentWindow.location.href='order_detail.aspx?LEAD=TRUE&itemID=" & Request("itemID") & "&messageID=" & Request("messageID") & "&preventMessage=" & Request("preventMessage") & "';" & vbCrLf)
            Else
                Response.Write("parentWindow.location.href='order_detail.aspx?LEAD=TRUE';" & vbCrLf)
            End If
        ElseIf Request("referrer") & "" <> "" Then
            If Not (Request("preventMessage") Is Nothing) Then
                Response.Write("parentWindow.location.href='" & Request("referrer") & "?preventMessage=" & Request("preventMessage") & "';" & vbCrLf)
            Else
                Response.Write("parentWindow.location.href='" & Request("referrer") & "';" & vbCrLf)
            End If
        Else
            If Not (Request("preventMessage") Is Nothing) Then
                Response.Write("parentWindow.location.href='order_detail.aspx?preventMessage=" & Request("preventMessage") & "';" & vbCrLf)
            Else
                Response.Write("parentWindow.location.href='order_detail.aspx';" & vbCrLf)
            End If
        End If
        Response.Write("</script>")
    End Sub

    Protected Sub Confirm_Click(sender As Object, e As EventArgs) Handles Confirm.Click

        Dim cBoxComponent As CheckBox
        Dim needToAddSku As Boolean = False

        ' --- finds out if the user checked any SKUs to be added
        For Each dgItem As DataGridItem In GridView1.Items
            cBoxComponent = dgItem.FindControl("SelectThis")
            If cBoxComponent.Checked Then
                needToAddSku = True
                Exit For
            End If
        Next

        '=== if user selected Related SKUs to be added
        If needToAddSku Then
            Dim dbConnection As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim dbCommand As OracleCommand = DisposablesManager.BuildOracleCommand(dbConnection)
            Dim sql As String

            Dim pkgSkus As String = String.Empty
            Dim cellValue As String = String.Empty
            Dim itmCd, vsn, des, itmTpCd, veCd, mnrCd, catCd, siz, finish, cover, grade, styleCd, treatable, pointSize, commCd, taxCd As String
            Dim calcRetPrc, retPrc, repCost, spiff, taxPct, taxableAmt As Double

            Try
                dbConnection.Open()

                For Each dgItem As DataGridItem In GridView1.Items
                    cBoxComponent = dgItem.FindControl("SelectThis")
                    If cBoxComponent.Checked Then

                        itmCd = dgItem.Cells(1).Text().Trim
                        vsn = Server.HtmlDecode(dgItem.Cells(2).Text()).Trim
                        des = Server.HtmlDecode(dgItem.Cells(3).Text()).Trim
                        retPrc = CDbl(dgItem.Cells(4).Text().Trim)
                        itmTpCd = dgItem.Cells(5).Text().Trim
                        veCd = Server.HtmlDecode(dgItem.Cells(6).Text()).Trim
                        mnrCd = Server.HtmlDecode(dgItem.Cells(7).Text()).Trim
                        catCd = Server.HtmlDecode(dgItem.Cells(8).Text()).Trim
                        siz = Server.HtmlDecode(dgItem.Cells(9).Text()).Trim
                        finish = Server.HtmlDecode(dgItem.Cells(10).Text()).Trim
                        cover = Server.HtmlDecode(dgItem.Cells(11).Text()).Trim
                        grade = Server.HtmlDecode(dgItem.Cells(12).Text()).Trim
                        styleCd = Server.HtmlDecode(dgItem.Cells(13).Text()).Trim
                        treatable = dgItem.Cells(14).Text().Trim

                        cellValue = Server.HtmlDecode(dgItem.Cells(15).Text()).Trim
                        pointSize = If(String.IsNullOrEmpty(cellValue), "0", cellValue)

                        cellValue = Server.HtmlDecode(dgItem.Cells(16).Text()).Trim
                        repCost = If(String.IsNullOrEmpty(cellValue), 0, CDbl(cellValue))

                        commCd = Server.HtmlDecode(dgItem.Cells(17).Text()).Trim

                        cellValue = Server.HtmlDecode(dgItem.Cells(18).Text()).Trim
                        spiff = If(Not String.IsNullOrEmpty(cellValue) AndAlso IsNumeric(cellValue), CDbl(cellValue), 0)

                        'Determine if the select item should be taxed.
                        If IsTaxable(itmCd) AndAlso String.IsNullOrEmpty(Session("tet_cd")) Then
                            taxCd = Session("TAX_CD")
                            taxPct = If(IsNumeric(Session("TAX_RATE")), Session("TAX_RATE"), 0)
                            taxableAmt = retPrc
                        Else
                            taxCd = ""
                            taxPct = 0
                            taxableAmt = 0
                        End If
                        calcRetPrc = taxableAmt * (taxPct / 100)

                        If ("PKG".Equals(itmTpCd)) Then
                            pkgSkus = pkgSkus & "'" & itmCd & "',"

                            If (ConfigurationManager.AppSettings("package_breakout").ToString = "Y") Then retPrc = 0
                        End If

                        sql = "INSERT INTO temp_itm (session_id, ITM_CD, VE_CD, MNR_CD, CAT_CD, RET_PRC, ORIG_PRC, MANUAL_PRC, VSN,DES, SIZ, FINISH, COVER,GRADE, " &
                              "STYLE_CD, ITM_TP_CD, TREATABLE, DEL_PTS, COST, COMM_CD, TAX_CD, TAX_PCT, TAXABLE_AMT, SPIFF, SLSP1, SLSP1_PCT, TAX_AMT "

                        If Session("slsp2") & "" <> "" Then sql = sql & ", SLSP2, SLSP2_PCT"
                        sql = sql & ")  VALUES ('" & Session.SessionID.ToString.Trim & "','" & itmCd & "','" & veCd & "','" & mnrCd & "','" &
                              catCd & "'," & retPrc & "," & retPrc & "," & retPrc & ",'" & Replace(vsn, "'", "''") & "','" & Replace(des, "'", "''") & "','" &
                              Replace(siz, "'", "''") & "','" & Replace(finish, "'", "''") & "','" & Replace(cover, "'", "''") & "','" &
                              Replace(grade, "'", "''") & "','" & styleCd & "','" & itmTpCd & "','" & treatable & "'," & pointSize & "," &
                              repCost & ",'" & commCd & "','" & taxCd & "'," & taxPct & "," & taxableAmt & "," & spiff & ",'" &
                              Session("slsp1") & "'," & Session("pct_1") & ",ROUND(" & calcRetPrc & ",2)"
                        If Session("slsp2") & "" <> "" Then sql = sql & ",'" & Session("slsp2") & "'," & Session("pct_2")
                        sql = sql & ")"

                        dbCommand.CommandText = sql
                        dbCommand.ExecuteNonQuery()
                        If pkgSkus & "" <> "" Then
                            Add_Packages(pkgSkus)
                            pkgSkus = String.Empty
                        End If
                    End If
                Next

                Response.Write("<script language='javascript'>" & vbCrLf)
                Response.Write("var parentWindow = window.parent; ")
                Response.Write("parentWindow.SelectAndClosePopup1();")
                If Request("LEAD") = "TRUE" Then
                    If Not (Request("preventMessage") Is Nothing) Then
                        Response.Write("parentWindow.location.href='order_detail.aspx?LEAD=TRUE&itemID=" & Request("itemID") & "&messageID=" & Request("messageID") & "&preventMessage=" & Request("preventMessage") & "';" & vbCrLf)
                    Else
                        Response.Write("parentWindow.location.href='order_detail.aspx?LEAD=TRUE';" & vbCrLf)
                    End If
                ElseIf Request("referrer") & "" <> "" Then
                    If Not (Request("preventMessage") Is Nothing) Then
                        Response.Write("parentWindow.location.href='" & Request("referrer") & "?preventMessage=" & Request("preventMessage") & "';" & vbCrLf)
                    Else
                        Response.Write("parentWindow.location.href='" & Request("referrer") & "';" & vbCrLf)
                    End If
                Else
                    If Not (Request("preventMessage") Is Nothing) Then
                        Response.Write("parentWindow.location.href='order_detail.aspx?preventMessage=" & Request("preventMessage") & "';" & vbCrLf)
                    Else
                        Response.Write("parentWindow.location.href='order_detail.aspx';" & vbCrLf)
                    End If
                End If
                Response.Write("</script>")

            Catch ex As Exception
                Throw
            Finally
                dbCommand.Dispose()
                dbConnection.Close()
            End Try
        End If
    End Sub
End Class
