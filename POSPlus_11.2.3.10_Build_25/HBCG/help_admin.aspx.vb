Imports System.Data.OracleClient
Imports System.IO
Imports HBCG_Utils

Partial Class help_admin
    Inherits POSBasePage

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataadapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds = New DataSet

        Gridview1.DataSource = ""
        'CustTable.Visible = False

        Gridview1.Visible = True

        conn.Open()

        sql = "SELECT * FROM HELP_FILES order by html_file"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If
        If Not IsPostBack Then
            bindgrid()
        End If

    End Sub

    'Public Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

    '    Dim conn As New OracleConnection
    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As OracleDataReader

    '    conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '    conn.Open()

    '    sql = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

    '    'Set SQL OBJECT 
    '    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

    '    Try
    '        MyDataReader = objSql.ExecuteReader
    '        If (MyDataReader.Read()) Then
    '            If MyDataReader.Item("EMP_CD") & "" <> "" Then
    '                Authorize_User = True
    '            Else
    '                Authorize_User = False
    '            End If
    '        Else
    '            Authorize_User = False
    '        End If
    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try

    'End Function

    'Public Sub test(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Gridview1.ItemDataBound

    '    Dim myGrid As DropDownList = e.Item.Cells(0).FindControl("drpHtmlFile")
    '    Dim arrFiles() As String = Directory.GetFileSystemEntries((Request.ServerVariables("APPL_PHYSICAL_PATH") + "help\"))
    '    Dim i As Integer = 0
    '    Dim strFileName As String
    '    Dim conn As New OracleConnection
    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As OracleDataReader
    '    conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '    conn.Open()
    '    'Set SQL OBJECT 

    '    If (myGrid Is Nothing) Then

    '    Else
    '        Do While (i < arrFiles.GetUpperBound(0))
    '            'get the file info
    '            Dim thisfile As FileInfo = New FileInfo(arrFiles(i))
    '            'add the item to the list
    '            myGrid.Items.Add((thisfile.Name))
    '            i = (i + 1)
    '        Loop
    '        strFileName = e.Item.Cells(1).Text
    '        sql = "SELECT help_file FROM help_files WHERE html_file ='" & strFileName & "' "
    '        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
    '        MyDataReader = objSql.ExecuteReader
    '        'Set SQL OBJECT 
    '        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
    '        If (MyDataReader.Read()) Then
    '            If MyDataReader.Item("help_file") & "" <> "" Then
    '                myGrid.Text = MyDataReader.Item("help_file")
    '            End If
    '            MyDataReader.Close()
    '            conn.Close()
    '        End If

    '    End If

    'End Sub

    Protected Sub DoItemSave(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim txt_desc As TextBox
        Dim str_desc As String

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdate As OracleCommand = DisposablesManager.BuildOracleCommand


        lblError.Text = ""
        txt_desc = e.Item.FindControl("txt_desc")
        str_desc = txt_desc.Text.ToString

        ' Update TempValue to DB
        conn.Open()

        With cmdUpdate
            .Connection = conn
            .CommandText = "UPDATE help_files SET des = '" & str_desc & "' WHERE help_id =" & e.Item.Cells(3).Text
            .ExecuteNonQuery()
        End With

        cmdUpdate.Dispose()
        conn.Close()

    End Sub

    Protected Sub Gridview1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.ItemCommand

        If e.CommandName.ToString <> "Delete" Then

            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim sql As String
            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader

            conn.Open()

            sql = "SELECT HELP_BODY FROM HELP_FILES WHERE HELP_ID =" & e.Item.Cells(3).Text
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            If MyDataReader.Read() Then
                WebHtmlEditor1.Html = MyDataReader.Item("HELP_BODY").ToString
            End If
            Gridview1.Visible = False
            WebHtmlEditor1.Visible = True
            btn_save.Visible = True
            btn_cancel.Visible = True
            lbl_help_id.Text = e.Item.Cells(3).Text
            conn.Close()
        End If

    End Sub

    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click

        WebHtmlEditor1.Visible = False
        WebHtmlEditor1.Html = ""
        Gridview1.Visible = True
        btn_save.Visible = False
        btn_cancel.Visible = False
        lbl_help_id.Text = ""

    End Sub

    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand

        conn.Open()

        sql = "UPDATE HELP_FILES SET HELP_BODY='" & WebHtmlEditor1.Html & "' WHERE HELP_ID = " & lbl_help_id.Text
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        objSql.ExecuteNonQuery()

        objSql.Dispose()
        conn.Close()

        WebHtmlEditor1.Visible = False
        WebHtmlEditor1.Html = ""
        Gridview1.Visible = True
        btn_save.Visible = False
        btn_cancel.Visible = False
        lbl_help_id.Text = ""

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
