'Imports DevExpress.Web.ASPxGridView
Imports System.Data.OracleClient
Imports System.Linq


Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization

Partial Class SRRTD_DETAIL
    Inherits POSBasePage
    Public Const gs_version As String = "Version 2.0"
    '------------------------------------------------------------------------------------------------------------------------------------
    '   VERSION HISTORY
    '   1.0:    Initial version of screen.
    '   2.0:
    '   (1) French implementation
    '------------------------------------------------------------------------------------------------------------------------------------

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    ''' <summary>
    ''' Created by     : KUMARAN
    ''' Dreated Date   : Jun 2015
    ''' Description    : Page Init Event Handler
    ''' Loads store Group/Store combo box based on logged on users company group code.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        Dim l_store_grp As String = Session("srrtd_store_grp")
        Dim l_w_d As String = Session("srrtd_w_d")
        Dim l_itm_tp_cd As String = Session("srrtd_itm_tp_cd")
        Dim l_inv_tp_cd As String = Session("srrtd_inv_tp_cd")

        drpdwn_store.Items.Add(l_store_grp)
        drpdwn_itm_tp.Items.Add(l_itm_tp_cd)
        drpdwn_w_d.Items.Add(l_w_d)
        drpdwn_inv_tp.Items.Add(l_inv_tp_cd)
         
        

        btn_first_page.Enabled = True
        btn_back_to_total.Enabled = True
        show_stores()
    End Sub
    Public Function calc_sale(ByRef p_store_grp_cd As String, ByRef p_w_d As String, ByRef p_inv_tp_cd As String, ByRef p_itm_tp_cd As String)
        ' Lucy created
        Dim connString As String
        Dim objConnection As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim x As Exception
        Dim Ds As New DataSet()

        Dim str_id As String = ""
        Dim p_id As String

        connString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        objConnection = DisposablesManager.BuildOracleConnection(connString)
        objConnection.Open()

        Dim myCMD As OracleCommand = DisposablesManager.BuildOracleCommand()

        myCMD.Connection = objConnection
        myCMD.CommandText = "std_srrtd.pos_input"
        myCMD.CommandType = CommandType.StoredProcedure

        myCMD.Parameters.Add(New OracleParameter("p_store_grp_cd", OracleType.VarChar)).Value = p_store_grp_cd
        myCMD.Parameters.Add(New OracleParameter("p_w_d", OracleType.VarChar)).Value = p_w_d
        myCMD.Parameters.Add(New OracleParameter("p_inv_tp_cd", OracleType.VarChar)).Value = p_inv_tp_cd
        myCMD.Parameters.Add(New OracleParameter("p_itm_tp_cd", OracleType.VarChar)).Value = p_itm_tp_cd
        myCMD.Parameters.Add("p_id", OracleType.Number, 50).Direction = ParameterDirection.ReturnValue

        ' Dim MyDA As New OracleDataAdapter(myCMD)

        Try

            myCMD.ExecuteNonQuery()

            If IsDBNull(myCMD.Parameters("p_id").Value) = False Then
                str_id = myCMD.Parameters("p_id").Value
            End If

        Catch x
            ' lbl_Lucy_Test.Text = x.Message.ToString

            objConnection.Close()
        Finally ' Daniela added
            objConnection.Close()

        End Try
        Return str_id
    End Function
    Protected Sub click_btn_first_page(ByVal sender As Object, ByVal e As System.EventArgs)

        lbl_msg.Text = " "
        show_stores()
    End Sub
    Protected Sub click_btn_back_to_total(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("srrtd_from_detail") = "Y"
        Response.Redirect("srrtd.aspx")
    End Sub



    Protected Sub show_stores()

        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_w_d As String
        Dim l_inv_tp_cd As String
        Dim l_itm_tp_cd As String
        Dim l_out_net_mgn As Double

        'If IsNumeric(l_id) Then
        'Exit Sub

        ' End If



        ' Dim DDL As DropDownList = CType(e.Item.FindControl("dropdownstore"), DropDownList)


     

        l_id = Session("srrtd_id")


        Try

            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            sqlString = New StringBuilder("SELECT  ")
            'Landed Cost Logic Starts
            sqlString.Append(" id,s_f,store_cd,GROSS_SALE , VOID_SALE, NET_SALE, NET_MGN ")
            sqlString.Append(" ,furn_mix, furn_net_mgn ")
            sqlString.Append(" ,matt_mix, matt_net_mgn ")
            sqlString.Append(" ,appl_mix, appl_net_mgn ")
            sqlString.Append(" ,elec_mix, elec_net_mgn ")
            sqlString.Append(" ,out_net_sale, out_net_mgn, out_net_qty ")
            sqlString.Append("FROM srrtd_output ")
            sqlString.Append("WHERE id =:l_id and store_cd <>'ALL'")
            sqlString.Append("  order by rec_no")

            ds = New DataSet
            GV_SRR.DataSource = ""

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()
            objcmd.Parameters.Add(":l_id", OracleType.VarChar)
            objcmd.Parameters(":l_id").Value = l_id

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


            objAdaptor.Fill(ds)


            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count




            If (MyDataReader.Read()) Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    Dim l_va As String
                    Dim l_st As String
                    If Not IsDBNull(ds.Tables(0).Rows(i)("s_f")) Then
                        l_va = ds.Tables(0).Rows(i)("s_f")
                    Else
                        l_va = ""
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i)("store_cd")) Then

                        l_st = ds.Tables(0).Rows(i)("store_cd")
                    Else
                        l_st = ""
                    End If
                    If l_va = "+" And l_st = "ALL" Then
                        GV_SRR.Columns(0).Visible = True

                    Else
                        GV_SRR.Columns(0).Visible = False
                    End If
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("gross_sale")) Then
                            err_cnt = 1
                        End If


                    End If

                    ' this three lines are used to debug values
                    '  If isNotEmpty(ds.Tables(0).Rows(i)("out_net_mgn")) Then
                    'l_out_net_mgn = ds.Tables(0).Rows(i)("out_net_mgn")
                    ' End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()
                lbl_msg.Text = "No data found."


            End If

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try

        Session("orig_page_count") = GV_SRR.PageCount

        ' If GV_SRR.PageCount = 1 Then
        btn_first_page.Visible = False
        '  End If
        lbl_warning.Text = "Page " & "1 of " & _
         GV_SRR.PageCount.ToString() & "."
    End Sub


    Sub GV_SRR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GV_SRR.CurrentPageIndex = e.NewPageIndex

        ' GV_SRR.DataSource = CType(Cache("Data"), DataTable)
        ' GV_SRR.DataBind()
    End Sub

    'Sub GridView_PageIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
    Sub GridView_PageIndexChanged(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
        Dim currentPage As Integer = e.NewPageIndex
        Dim orgin_page_count As Integer = Session("orig_page_count")
        Dim current_page_count = GV_SRR.PageCount
        ' Call a helper method to display the current page number 
        ' when the user navigates to a different page.
        ' DisplayCurrentPage()
        Dim real_page_no As Integer
        ' because the way do_page_load doing, it is loosing the pages prior to the currentPage, so need to add the missing count no of pages
        real_page_no = (orgin_page_count - current_page_count) + currentPage
        ' DisplayCurrentPage(currentPage)
        DisplayCurrentPage(real_page_no)
        do_page_load(real_page_no)

       
    End Sub
    Sub DisplayCurrentPage(ByRef p_currentPage As Integer)

        ' Calculate the current page number.
        Dim currentPage As Integer = p_currentPage + 1
        Dim orgin_page_count As Integer = Session("orig_page_count")
        ' Display the current page number. 
        lbl_warning.Text = "Page " & currentPage.ToString() & " of " & _
         orgin_page_count.ToString() & "."
 
        '  If orgin_page_count = currentPage Then
        'btn_first_page.Visible = False
        'Else
        btn_first_page.Visible = True
        ' End If


    End Sub


    Sub do_page_load(ByRef p_page As Integer)
        Dim l_id As String
        Dim l_store_grp_cd As String
        Dim l_w_d As String
        Dim l_inv_tp_cd As String
        Dim l_itm_tp_cd As String

        ' this does not work, for non reason , this  btn_search_Click routine does twice after click the button
        If IsNumeric(l_id) Then
            Exit Sub

        End If



        ' Dim DDL As DropDownList = CType(e.Item.FindControl("dropdownstore"), DropDownList)


        ' l_store_grp_cd = drpdwn_store.SelectedItem.Value    'this is desc
        '  dw_st.SelectedIndex = drpdwn_store.SelectedIndex    ' get the same index
        '  l_store_grp_cd = dw_st.SelectedItem.Value           ' this is store_grp_cd/store_cd
        ' l_w_d = drpdwn_w_d.SelectedItem.Value
        '  l_inv_tp_cd = drpdwn_inv_tp.SelectedItem.Value
        '  l_itm_tp_cd = drpdwn_itm_tp.SelectedItem.Value

        l_id = Session("srrtd_id")
        ' l_id = calc_sale(l_store_grp_cd, l_w_d, l_inv_tp_cd, l_itm_tp_cd)

        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sqlString As StringBuilder
            Dim objcmd As OracleCommand
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim err_cnt As Integer = 0
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            conn.Open()
            sqlString = New StringBuilder("SELECT  ")
            'Landed Cost Logic Starts
            sqlString.Append(" id,s_f,store_cd,GROSS_SALE , VOID_SALE, NET_SALE, NET_MGN ")
            sqlString.Append(" ,furn_mix, furn_net_mgn ")
            sqlString.Append(" ,matt_mix, matt_net_mgn ")
            sqlString.Append(" ,appl_mix, appl_net_mgn ")
            sqlString.Append(" ,elec_mix, elec_net_mgn ")
            sqlString.Append(" ,out_net_sale, out_net_mgn, out_net_qty ")
            sqlString.Append("FROM srrtd_output ")
            sqlString.Append("WHERE id =:l_id and store_cd <>'ALL'")
            sqlString.Append(" and rec_no > :p_page *300")                   ' here p_page * 10 is because pagesize=10, if pagesize=15, it will be p_page*15
            sqlString.Append(" order by rec_no")

            ds = New DataSet
            GV_SRR.DataSource = ""

            objcmd = DisposablesManager.BuildOracleCommand()
            objcmd.Connection = conn

            objcmd.CommandText = sqlString.ToString()
            objcmd.Parameters.Clear()
            objcmd.Parameters.Add(":p_page", OracleType.VarChar)
            objcmd.Parameters(":p_page").Value = p_page
            objcmd.Parameters.Add(":l_id", OracleType.VarChar)
            objcmd.Parameters(":l_id").Value = l_id

            Dim objAdaptor As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(objcmd)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objcmd)


            objAdaptor.Fill(ds)


            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count




            If (MyDataReader.Read()) Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()

                Dim i As Integer

                For i = 0 To numrows - 1

                    ds.Tables(0).NewRow()
                    Dim l_va As String
                    Dim l_st As String
                    ' If Not IsDBNull(ds.Tables(0).Rows(i)("s_f")) Then
                    'l_va = ds.Tables(0).Rows(i)("s_f")
                    '  Else
                    ' l_va = ""
                    'End If
                    If Not IsDBNull(ds.Tables(0).Rows(i)("store_cd")) Then

                        l_st = ds.Tables(0).Rows(i)("store_cd")
                    Else
                        l_st = ""
                    End If
                    ' If l_va = "+" And l_st = "ALL" Then
                    'GV_SRR.Columns(0).Visible = True

                    ' Else
                    ' GV_SRR.Columns(0).Visible = False
                    ' End If
                    If i > 0 And err_cnt = 0 Then
                        If isNotEmpty(ds.Tables(0).Rows(i)("gross_sale")) Then
                            err_cnt = 1
                        End If


                    End If

                Next
                ds.Tables(0).NewRow()
                ds.Tables(0).Rows.InsertAt(ds.Tables(0).NewRow, 0)

            End If
            If ds.Tables(0).Rows.Count = 0 Then
                GV_SRR.DataSource = ds
                GV_SRR.DataBind()
                lbl_msg.Text = "No data found."


            End If

            MyDataReader.Close()
            conn.Close()

        Catch ex As Exception
            lbl_msg.Text = "System Error. " & ex.Message
        End Try

    End Sub

 




End Class