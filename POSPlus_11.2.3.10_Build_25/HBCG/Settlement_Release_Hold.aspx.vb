Imports System.Data.OracleClient
Imports System.Data

Partial Class Settlement_Release_Hold
    Inherits POSBasePage
    Public ChkdItems As String
    Public SortField As String = ""
    Public ChkBxIndex As String = ""
    Public BxChkd As Boolean = False
    Public CheckedItems As ArrayList
    Public Results() As String
    Public CheckBox As System.Web.UI.WebControls.CheckBox
    Public dgItem As DataGridItem
    Public itemIds As String = ""
    Public sel_itms() As String
    Public itmcd As String

   Private Class TranGrid
        Public Const X0 As Integer = 0
        Public Const WR_DT As Integer = 1 ' not db
        Public Const CUST_CD As Integer = 2 ' not db
        Public Const X3 As Integer = 3  ' not db
        Public Const BNK_CRD_NUM As Integer = 4 ' x
        Public Const STORE_CODE As Integer = 5  ' not db
        Public Const AMT As Integer = 6   'x
        Public Const X7 As Integer = 7   ' not db
        Public Const APPROVAL_CD As Integer = 8   'x
        Public Const X9 As Integer = 9  ' no db
        Public Const PROMO_CD As Integer = 10  ' x   OR ref_num
        Public Const X11 As Integer = 11 ' not db
        Public Const X12 As Integer = 12 ' OR TRN_TP_CD/ORD_TP_CD  ' not db
        Public Const X13 As Integer = 13 'not db
        Public Const REMOVE_BTN As Integer = 14
        Public Const MEDIUM_TP_CD As Integer = 15
        Public Const STAT_CD As Integer = 16
        'Public Const DEL_DOC_NUM As Integer = 17
    End Class
    Public Sub bindgrid(ByVal SortField As String)

        lbl_tran_amt.Text = 0
        lbl_tran_count.Text = 0

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim MyTable As DataTable
        Dim numrows As Integer
        Dim myDataAdapter As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter

        Dim Source As DataView
        ds = New DataSet
        Dim Do_Union As Boolean = False
        Dim store_group As String = Retrieve_Stores(store_cd.Value)

        If store_group & "" = "" And store_cd.Value & "" <> "" Then
            lbl_grid_msg.Text = "No records found for requested store"
            GridView1.Visible = False
            Exit Sub
        End If

        GridView1.DataSource = ""

        GridView1.Visible = True
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString

        'Open Connection 
        conn.Open()

        sql = "SELECT WR_DT, SO.CUST_CD, ASP_TRN.MEDIUM_TP_CD, CUST.FNAME, CUST.LNAME, ASP_TRN.BNK_CRD_NUM, ASP_TRN.STAT_CD, ASP_TRN.STORE_CD, "
        sql = sql & "DECODE(ASP_TRN.MEDIUM_TP_CD,'SAL',ASP_TRN.AMT,'MDB',ASP_TRN.AMT,'CRM',-ASP_TRN.AMT,'MCR',-ASP_TRN.AMT,ASP_TRN.AMT) AS AMT, ASP_TRN.APP_CD, ASP_TRN.REF_NUM, 'TEST' AS PROMO_CD, ASP_TRN.AS_TRN_TP, ASP_TRN.DEL_DOC_NUM FROM ASP_TRN, SO, CUST "
        sql = sql & "WHERE ASP_TRN.DEL_DOC_NUM=SO.DEL_DOC_NUM(+) "
        sql = sql & "AND SO.CUST_CD=CUST.CUST_CD "
        If chk_errors.Checked = True Then
            sql = sql & " AND ASP_TRN.STAT_CD='E' "
        ElseIf chk_deleted.Checked = True Then
            sql = sql & " AND ASP_TRN.STAT_CD='D' "
        Else
            sql = sql & " AND ASP_TRN.STAT_CD ='H' "
            Do_Union = True
        End If
        If Not String.IsNullOrEmpty(store_group) Then
            sql = sql & "AND ASP_TRN.STORE_CD IN(" & store_group & ") "
        End If
        If Do_Union = True Then
            sql = sql & "UNION "
            sql = sql & "SELECT WR_DT, SO.CUST_CD, ASP_TRN.MEDIUM_TP_CD, CUST.FNAME, CUST.LNAME, ASP_TRN.BNK_CRD_NUM, ASP_TRN.STAT_CD, ASP_TRN.STORE_CD, "
            sql = sql & "DECODE(ASP_TRN.MEDIUM_TP_CD,'SAL',ASP_TRN.AMT,'MDB',ASP_TRN.AMT,'CRM',-ASP_TRN.AMT,'MCR',-ASP_TRN.AMT,ASP_TRN.AMT) AS AMT, "
            sql = sql & "ASP_TRN.APP_CD, ASP_TRN.REF_NUM, SO_ASP.PROMO_CD, ASP_TRN.AS_TRN_TP, ASP_TRN.DEL_DOC_NUM FROM ASP_TRN, SO, CUST, SO_ASP "
            sql = sql & "WHERE ASP_TRN.DEL_DOC_NUM=SO.DEL_DOC_NUM(+) AND SO_ASP.DEL_DOC_NUM(+)=ASP_TRN.DEL_DOC_NUM "
            sql = sql & "AND SO.CUST_CD=CUST.CUST_CD "
            sql = sql & " AND ASP_TRN.STAT_CD IS NULL "   ' for testing:   AND ROWNUM < 11 "
        End If
        If Not String.IsNullOrEmpty(store_group) Then
            sql = sql & "AND ASP_TRN.STORE_CD IN(" & store_group & ") "
        End If
        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        dv = ds.Tables(0).DefaultView
        MyTable = New DataTable
        MyTable = ds.Tables(0)
        numrows = MyTable.Rows.Count

        'Assign sort expression to Session              
        Session("SortOrder") = SortField

        'Setup DataView for Sorting              
        Source = ds.Tables(0).DefaultView

        'Insert DataView into Session           
        Session("dgCache") = Source

        If numrows > 0 Then
            lbl_grid_msg.Text = ""
            GridView1.Visible = True
            Source.Sort = SortField
            GridView1.DataSource = Source
            GridView1.DataBind()
            lbl_tran_amt.Text = 0
            lbl_tran_count.Text = 0

            'Loop through DataGrid Items            
            For Each dgItem In GridView1.Items

                'Retrieve key value of each record based on DataGrids DataKeyField property    
                CheckBox = dgItem.FindControl("SelectThis")

                If CheckBox.Checked Then
                    If IsNumeric(dgItem.Cells(6).Text.ToString) Then
                        lbl_tran_amt.Text = lbl_tran_amt.Text + CDbl(dgItem.Cells(6).Text.ToString)
                        lbl_tran_count.Text = lbl_tran_count.Text + 1
                    End If
                End If
            Next

        Else
            lbl_grid_msg.Text = "No records found for requested store"
            GridView1.Visible = False
        End If

        'Close Connection 
        conn.Close()

    End Sub

    Public Shared Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn.Open()

        sql = "SELECT EMP_CD FROM SETTLEMENT_ADMIN WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                If MyDataReader.Item("EMP_CD") & "" <> "" Then
                    Authorize_User = True
                Else
                    Authorize_User = False
                End If
            Else
                Authorize_User = False
            End If
            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            store_cd_populate()
            If Authorize_User(Session("EMP_CD"), "ADMIN_ACCESS") = False Then
                store_cd.Value = Session("HOME_STORE_CD")
                store_cd.Enabled = False
            End If
            bindgrid("WR_DT asc")
        End If

    End Sub

    Public Sub Update_Checked_Items(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ck1 As CheckBox = CType(sender, CheckBox)
        Dim dgItem As DataGridItem = CType(ck1.NamingContainer, DataGridItem)
        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
        conn.Open()
        Dim objsql2 As OracleCommand
        Dim sql As String

        If ck1.Checked = True Then
            sql = "UPDATE ASP_TRN SET STAT_CD=NULL WHERE WR_DT=TO_DATE('" & dgItem.Cells(1).Text & "','mm/dd/yyyy') AND CUST_CD='" & dgItem.Cells(2).Text & "' "
            sql = sql & "AND STORE_CD='" & dgItem.Cells(5).Text & "' AND DEL_DOC_NUM='" & dgItem.Cells(13).Text & "' AND MEDIUM_TP_CD='"
            sql = sql & dgItem.Cells(12).Text & "' AND AMT=ABS(" & CDbl(dgItem.Cells(6).Text) & ")"
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql2.ExecuteNonQuery()
        Else
            sql = "UPDATE ASP_TRN SET STAT_CD='H' WHERE WR_DT=TO_DATE('" & dgItem.Cells(1).Text & "','mm/dd/yyyy') AND CUST_CD='" & dgItem.Cells(2).Text & "' "
            sql = sql & "AND STORE_CD='" & dgItem.Cells(5).Text & "' AND DEL_DOC_NUM='" & dgItem.Cells(13).Text & "' AND MEDIUM_TP_CD='"
            sql = sql & dgItem.Cells(12).Text & "' AND AMT=ABS(" & CDbl(dgItem.Cells(6).Text) & ")"
            objsql2 = DisposablesManager.BuildOracleCommand(sql, conn)
            objsql2.ExecuteNonQuery()
        End If
        conn.Close()

        GetCheckBoxValues(sender, e)

    End Sub

    Public Sub GetCheckBoxValues(ByVal sender As Object, ByVal e As System.EventArgs)

        lbl_tran_amt.Text = 0
        lbl_tran_count.Text = 0

        'Loop through DataGrid Items            
        For Each dgItem In GridView1.Items

            'Retrieve key value of each record based on DataGrids DataKeyField property    
            CheckBox = dgItem.FindControl("SelectThis")

            If CheckBox.Checked Then
                If IsNumeric(dgItem.Cells(6).Text.ToString) Then
                    lbl_tran_amt.Text = lbl_tran_amt.Text + CDbl(dgItem.Cells(6).Text.ToString)
                    lbl_tran_count.Text = lbl_tran_count.Text + 1
                End If
            End If
        Next

    End Sub

    Public Sub store_cd_populate()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim ds As New DataSet
        Dim cmdGetStores As OracleCommand
        cmdGetStores = DisposablesManager.BuildOracleCommand

        Dim SQL As String

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        SQL = "SELECT STORE_CD, STORE_NAME, STORE_CD || ' - ' || STORE_NAME AS FULL_DESC FROM STORE ORDER BY STORE_CD"

        Try
            With cmdGetStores
                .Connection = conn
                .CommandText = SQL
            End With

            conn.Open()
            Dim oAdp = DisposablesManager.BuildOracleDataAdapter(cmdGetStores)
            oAdp.Fill(ds)

            With store_cd
                .DataSource = ds
                .ValueField = "STORE_CD"
                .TextField = "FULL_DESC"
                .DataBind()
            End With

            store_cd.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem("Please Select A Store", ""))
            store_cd.Items.FindByText("Please Select A Store").Value = ""

        Catch ex As Exception
            conn.Close()
            Throw
        End Try
        conn.Close()

    End Sub

    Protected Sub store_cd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles store_cd.SelectedIndexChanged

        bindgrid(Session("SortOrder"))

    End Sub

    Sub MyDataGrid_Sort(ByVal Sender As Object, ByVal E As DataGridSortCommandEventArgs)

        GridView1.CurrentPageIndex = 0 'To sort from top
        bindgrid(SortOrder(E.SortExpression).ToString()) 'Rebind our DataGrid
        'To retain checkbox on sorting          
        'RePopulateCheckBoxes()

    End Sub

    Sub RePopulateCheckBoxes()

        Dim dgItem As DataGridItem
        CheckedItems = New ArrayList

        'If Session("CheckedItems") & "" <> "" Then
        If Not IsDBNull(Session("CheckedItems")) Then
            If Not IsNothing(Session("CheckedITems")) Then
                CheckedItems = Session("CheckedItems")
            End If
        End If

        If Not IsNothing(CheckedItems) Then
            'Loop through DataGrid Items              
            For Each dgItem In GridView1.Items
                ChkBxIndex = GridView1.DataKeys(dgItem.ItemIndex)

                'Repopulate DataGrid with items found in Session              
                If CheckedItems.Contains(ChkBxIndex) Then
                    CheckBox = CType(dgItem.FindControl("SelectThis"), CheckBox)
                    CheckBox.Checked = True
                End If
            Next

            'Copy ArrayList to a new array          
            Results = CheckedItems.ToArray(GetType(String))

            'Concatenate ArrayList with comma to properly send for deletion          
            itemIds = String.Join(",", Results)
        End If

    End Sub

    Function SortOrder(ByVal Field As String) As String

        Dim so As String = Session("SortOrder")

        If Field = so Then
            SortOrder = Replace(Field, "asc", "desc")
        ElseIf Field <> so Then
            SortOrder = Replace(Field, "desc", "asc")
        Else
            SortOrder = Replace(Field, "asc", "desc")
        End If

        'Maintain persistent sort order         
        Session("SortOrder") = SortOrder

    End Function

    Protected Sub GridView1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridView1.ItemCommand

        If e.CommandName.ToString <> "Sort" Then
            Dim del_doc_num As String = e.Item.Cells(13).Text
            Dim medium_tp_cd As String = e.Item.Cells(12).Text
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()

            If chk_deleted.Checked = True Then
                sql = "UPDATE ASP_TRN SET STAT_CD='H' WHERE AS_CD='GEMONEY' AND DEL_DOC_NUM='" & del_doc_num & "' AND MEDIUM_TP_CD='" & medium_tp_cd & "' AND AMT=ABS(" & CDbl(e.Item.Cells(6).Text) & ")"
            Else
                sql = "UPDATE ASP_TRN SET STAT_CD='D' WHERE AS_CD='GEMONEY' AND DEL_DOC_NUM='" & del_doc_num & "' AND MEDIUM_TP_CD='" & medium_tp_cd & "' AND AMT=ABS(" & CDbl(e.Item.Cells(6).Text) & ")"
            End If
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()

            conn.Close()
            bindgrid(Session("SortOrder"))
        End If

    End Sub

    Protected Sub GridView1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView1.ItemDataBound

        Dim CheckBox1 As CheckBox = CType(e.Item.FindControl("SelectThis"), CheckBox)
        Dim Button1 As DevExpress.Web.ASPxEditors.ASPxButton = CType(e.Item.FindControl("Button1"), DevExpress.Web.ASPxEditors.ASPxButton)
        If chk_deleted.Checked = True And Not IsNothing(Button1) Then
            Button1.Text = "Add Back"
        End If
        If Not IsNothing(CheckBox1) Then
            If e.Item.Cells(16).Text = "&nbsp;" Then
                If Not IsNothing(CheckBox1) Then
                    CheckBox1.Checked = True
                End If
            End If
            If Len(e.Item.Cells(4).Text) <> 16 Then
                e.Item.Cells(4).ForeColor = Drawing.Color.Red
                Dim txt_acct_num As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(e.Item.FindControl("txt_bnk_crd_num"), DevExpress.Web.ASPxEditors.ASPxTextBox)
                If Not IsNothing(txt_acct_num) Then
                    txt_acct_num.ForeColor = Drawing.Color.Red
                End If
                If Not IsNothing(CheckBox1) Then
                    CheckBox1.Enabled = False
                End If
            End If

            If Len(e.Item.Cells(8).Text) <> 6 Or Not IsNumeric(e.Item.Cells(8).Text) Then
                If e.Item.Cells(12).Text = "MDB" Or e.Item.Cells(12).Text = "SAL" Then
                    e.Item.Cells(8).ForeColor = Drawing.Color.Red
                    Dim txt_auth_cd As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(e.Item.FindControl("txt_auth_cd"), DevExpress.Web.ASPxEditors.ASPxTextBox)
                    If Not IsNothing(txt_auth_cd) Then
                        txt_auth_cd.ForeColor = Drawing.Color.Red
                    End If
                    If Not IsNothing(CheckBox1) Then
                        CheckBox1.Enabled = False
                    End If
                End If
            End If

            If String.IsNullOrEmpty(Len(e.Item.Cells(10).Text)) Then
                e.Item.Cells(10).ForeColor = Drawing.Color.Red
                Dim txt_promo_cd As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(e.Item.FindControl("txt_promo_cd"), DevExpress.Web.ASPxEditors.ASPxTextBox)
                If Not IsNothing(txt_promo_cd) Then
                    txt_promo_cd.ForeColor = Drawing.Color.Red
                End If
                If Not IsNothing(CheckBox1) Then
                    CheckBox1.Enabled = False
                End If
            End If

            Dim txt_amt As DevExpress.Web.ASPxEditors.ASPxTextBox = CType(e.Item.FindControl("txt_amt"), DevExpress.Web.ASPxEditors.ASPxTextBox)
            If IsNumeric(e.Item.Cells(6).Text) Then
                txt_amt.Text = FormatNumber(e.Item.Cells(6).Text, 2)
                If chk_errors.Checked = True Then
                    txt_amt.ForeColor = Drawing.Color.Red
                End If
            Else
                If Not IsNothing(CheckBox1) Then
                    CheckBox1.Enabled = False
                End If
            End If

            If Authorize_User(Session("EMP_CD"), "ADMIN_ACCESS") = False Then
                If Not IsNothing(txt_amt) Then
                    txt_amt.ReadOnly = True
                End If
            End If

        End If

    End Sub

    Protected Sub chk_errors_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_errors.CheckedChanged

        If chk_errors.Checked = True Then
            chk_deleted.Enabled = False
        Else
            chk_deleted.Enabled = True
        End If
        bindgrid(Session("SortOrder"))

    End Sub

    Protected Sub chk_deleted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_deleted.CheckedChanged

        If chk_deleted.Checked = True Then
            chk_errors.Enabled = False
        Else
            chk_errors.Enabled = True
        End If
        bindgrid(Session("SortOrder"))

    End Sub

    Public Sub BNK_CRD_UPDATE(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)
        If IsNumeric(sender.text) And Len(sender.text) = 16 Then
            Dim myVerify As New VerifyCC
            Dim myTypeValid As New VerifyCC.TypeValid
            myTypeValid = myVerify.GetCardInfo(sender.text)

            If String.IsNullOrEmpty(myTypeValid.CCType) Or myTypeValid.CCType = "Unknown" Then
                lbl_grid_msg.Text = "You've entered an invalid account number."
                Dim new_bnk_crd As String = dgItem.Cells(4).Text
                If new_bnk_crd = "&nbsp;" Then new_bnk_crd = ""
                sender.text = new_bnk_crd
                Exit Sub
            End If
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim cmdGetStores As OracleCommand
            cmdGetStores = DisposablesManager.BuildOracleCommand


            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If
            conn.Open()

            sql = "UPDATE ASP_TRN SET BNK_CRD_NUM='" & sender.text & "' WHERE WR_DT=TO_DATE('" & dgItem.Cells(1).Text & "','mm/dd/yyyy') AND CUST_CD='" & dgItem.Cells(2).Text & "' "
            sql = sql & "AND STORE_CD='" & dgItem.Cells(5).Text & "' AND DEL_DOC_NUM='" & dgItem.Cells(13).Text & "' AND MEDIUM_TP_CD='"
            sql = sql & dgItem.Cells(12).Text & "' AND AMT=ABS(" & CDbl(dgItem.Cells(6).Text) & ")"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()
            If dgItem.Cells(12).Text = "SAL" Then
                sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & sender.text & "' WHERE IVC_CD='" & dgItem.Cells(13).Text & "' AND TRN_TP_CD='" & dgItem.Cells(12).Text & "'"
            Else
                sql = "UPDATE AR_TRN SET BNK_CRD_NUM='" & sender.text & "' WHERE ADJ_IVC_CD='" & dgItem.Cells(13).Text & "' AND TRN_TP_CD='" & dgItem.Cells(12).Text & "'"
            End If
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()
            sender.text = "XXXXXXXXXXXX" & Right(sender.text, 4)
            sender.ForeColor = Drawing.Color.Black
            conn.Close()
        Else
            lbl_grid_msg.Text = "You've entered an invalid account number."
            Dim new_bnk_crd As String = dgItem.Cells(4).Text
            If new_bnk_crd = "&nbsp;" Then new_bnk_crd = ""
            sender.text = new_bnk_crd
        End If

    End Sub

    Public Sub PROMO_CD_UPDATE(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)
        If IsNumeric(sender.text) And Len(sender.text) = 3 Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim cmdGetStores As OracleCommand
            cmdGetStores = DisposablesManager.BuildOracleCommand


            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If
            conn.Open()
            sql = "UPDATE ASP_TRN SET REF_NUM='" & sender.text & "' WHERE WR_DT=TO_DATE('" & dgItem.Cells(1).Text & "','mm/dd/yyyy') AND CUST_CD='" & dgItem.Cells(2).Text & "' "
            sql = sql & "AND STORE_CD='" & dgItem.Cells(5).Text & "' AND DEL_DOC_NUM='" & dgItem.Cells(13).Text & "' AND MEDIUM_TP_CD='"
            sql = sql & dgItem.Cells(12).Text & "' AND AMT=ABS(" & CDbl(dgItem.Cells(6).Text) & ")"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()
            sender.ForeColor = Drawing.Color.Black
            conn.Close()
        Else
            lbl_grid_msg.Text = "You've entered an invalid promo code."
            sender.text = dgItem.Cells(10).Text
        End If

    End Sub

    Public Sub AUTH_CD_UPDATE(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)
        If IsNumeric(sender.text) And Len(sender.text) = 6 Then
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim sql As String
            Dim objSql As OracleCommand
            Dim cmdGetStores As OracleCommand
            cmdGetStores = DisposablesManager.BuildOracleCommand


            If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                Throw New Exception("Connection Error")
            Else
                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
            End If
            conn.Open()

            sql = "UPDATE ASP_TRN SET APP_CD='" & sender.text & "' WHERE WR_DT=TO_DATE('" & dgItem.Cells(1).Text & "','mm/dd/yyyy') AND CUST_CD='" & dgItem.Cells(2).Text & "' "
            sql = sql & "AND STORE_CD='" & dgItem.Cells(5).Text & "' AND DEL_DOC_NUM='" & dgItem.Cells(13).Text & "' AND MEDIUM_TP_CD='"
            sql = sql & dgItem.Cells(12).Text & "' AND AMT=ABS(" & CDbl(dgItem.Cells(6).Text) & ")"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()
            sender.ForeColor = Drawing.Color.Black
            conn.Close()
        Else
            lbl_grid_msg.Text = "You've entered an invalid approval code."
            sender.text = dgItem.Cells(8).Text
        End If

    End Sub

    Public Sub AMT_UPDATE(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim dgItem As DataGridItem = CType(sender.NamingContainer, DataGridItem)
        If IsNumeric(sender.text) Then

            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim sql As String
            Dim objSql As OracleCommand
            '   WARNING - SEE NOTE BELOW ABOVE LockSoRec FOR USE OF THIS dbCmd
            Dim dbCmd As OracleCommand = DisposablesManager.BuildOracleCommand(conn)

            Try
                conn.Open()

                Dim docNum As String = dgItem.Cells(13).Text   ' sb dgItem.Cells(TranGrid.DEL_DOC_NUM).Text

                If dgItem.Cells(12).Text = "GDR" Then
                    sql = "UPDATE ASP_TRN SET AMT=" & CDbl(sender.text) & " WHERE WR_DT=TO_DATE('" & dgItem.Cells(1).Text & "','mm/dd/yyyy') AND CUST_CD='" & dgItem.Cells(2).Text & "' "
                    sql = sql & "AND STORE_CD='" & dgItem.Cells(5).Text & "' AND DEL_DOC_NUM='" & dgItem.Cells(13).Text & "' AND MEDIUM_TP_CD='"
                    sql = sql & dgItem.Cells(12).Text & "' AND AMT=" & CDbl(dgItem.Cells(6).Text)

                Else
                    ' NOTE THAT SINCE THIS DBCMD IS NOT USING A TRANSACTION, THE LOCK IS ONLY GOOD FOR ONE UPDATE
                    '    TO THE COMMAND;  AT THIS POINT, THE CODE IS SETUP FOR JUST SO UPDATE USING THIS COMMAND
                    '    WHICH LOCKS THE SO;  AFTER UPDATE OF SO, IT IS NO LONGER LOCKED;  TO USE FOR MULTIPLE
                    '    UPDATES, HAVE TO CHANGE TO USE TRANSACTION BUT NEED TO REWORK PER PAYMENT THEN SO DO NOT
                    '    SAVE PART OF A PAYMENT (PART UPDATE IN TRANSACTION, PART NOT IN TRANSACTION) AND THAT DO NOT
                    '    ALLOW RE_AUTH'ing/SALE AGAINST SAME PAYMENT TWICE WHICH WOULD OCCUR IF COMPLETED A BC PAYMENT
                    '    AND THEN FAILED ON FINANCE BUT DID NOT DELETE THE PAYMENT ENTRY.
                    Try
                        Dim soRowid As String = SalesUtils.LockSoRec(docNum, dbCmd)

                    Catch ex As Exception
                        If ex.Message = String.Format(Resources.POSErrors.ERR0042, docNum) Then
                            lbl_grid_msg.Text = ex.Message
                            sender.text = dgItem.Cells(6).Text
                            Exit Sub
                        Else
                            Throw ex
                        End If
                    End Try

                    ' FYI - this stuff doesn't work - the values are almost all empty
                    sql = "UPDATE ASP_TRN SET AMT=ABS(" & CDbl(sender.text) & ") WHERE WR_DT=TO_DATE('" & dgItem.Cells(1).Text & "','mm/dd/yyyy') AND CUST_CD='" & dgItem.Cells(2).Text & "' "
                    sql = sql & "AND STORE_CD='" & dgItem.Cells(5).Text & "' AND DEL_DOC_NUM='" & dgItem.Cells(13).Text & "' AND MEDIUM_TP_CD='"
                    sql = sql & dgItem.Cells(12).Text & "' AND AMT=ABS(" & CDbl(dgItem.Cells(6).Text) & ")"
                    objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql.ExecuteNonQuery()

                    sql = "UPDATE SO SET ORIG_FI_AMT=" & CDbl(sender.text) & " WHERE DEL_DOC_NUM='" & dgItem.Cells(13).Text & "' "
                    sql = sql & " AND ORD_TP_CD='" & dgItem.Cells(12).Text & "'"
                    ' NOTE - DO NOT REUSE dbCmd w/o reviewing above note - used for SO locking
                    dbCmd.Parameters.Clear()
                    dbCmd.CommandText = sql
                    dbCmd.ExecuteNonQuery()
                End If
                sender.ForeColor = Drawing.Color.Black
                dgItem.Cells(6).Text = CDbl(sender.text)
                lbl_grid_msg.Text = ""
            Finally
                dbCmd.Dispose()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        Else
            lbl_grid_msg.Text = "You've entered an invalid amount."
            sender.text = dgItem.Cells(6).Text
        End If

    End Sub

    Public Shared Function Retrieve_Stores(ByVal store_cd As String)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim output_store As String = ""

        conn.Open()

        sql = "select store_cd from payment_xref where merchant_id in (select merchant_id from payment_xref where store_cd='"
        sql = sql & store_cd & "') group by store_cd"

        Dim objSql2 As OracleCommand = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim MyDatareader2 As OracleDataReader

        Try
            MyDatareader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDatareader2.Read
                output_store = output_store & "'" & MyDatareader2.Item("STORE_CD").ToString & "',"
            Loop
        Catch
            conn.Close()
            Throw
        End Try

        If output_store & "" = "" Then Return ""
        output_store = Left(output_store, Len(output_store) - 1)

        conn.Close()
        Return output_store

    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

    End Sub
End Class

Public Class VerifyCC
    Public Structure TypeValid
        Public CCType As String
        Public CCValid As Boolean
    End Structure

    Public Function GetCardInfo(ByVal CCN As String) As TypeValid
        Dim i, w, x, y
        y = 0
        CCN = Replace(Replace(Replace(CStr(CCN), "-", ""), " ", ""), ".", "")

        w = 2 * (Len(CCN) Mod 2)
        For i = Len(CCN) - 1 To 1 Step -1
            x = Mid(CCN, i, 1)
            If IsNumeric(x) Then
                Select Case (i Mod 2) + w
                    Case 0, 3
                        y = y + CInt(x)
                    Case 1, 2
                        x = CInt(x) * 2
                        If x > 9 Then

                            y = y + (x \ 10) + (x - 10)
                        Else
                            y = y + x
                        End If
                End Select
            End If
        Next

        y = 10 - (y Mod 10)
        If y > 9 Then y = 0
        GetCardInfo.CCValid = (CStr(y) = Right(CCN, 1))
        If GetCardInfo.CCValid = True Then
            Select Case Left(CCN, 1)
                Case "3"
                    GetCardInfo.CCType = "AMEX/Diners Club/JCB"
                Case "4"
                    GetCardInfo.CCType = "VISA"
                Case "5"
                    GetCardInfo.CCType = "MasterCard"
                Case "6"
                    GetCardInfo.CCType = "Discover"
                Case Else
                    GetCardInfo.CCType = "Unknown"
            End Select
        Else
            GetCardInfo.CCType = "Unknown"
        End If

    End Function
End Class
