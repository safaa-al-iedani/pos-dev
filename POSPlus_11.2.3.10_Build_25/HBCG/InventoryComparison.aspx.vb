Imports System.Data
Imports System.Data.OracleClient
Imports System.IO

Partial Class InventoryComparison
    Inherits POSBasePage
    Dim picstring, vsn, itm_cd, des, ret_prc, avail, treatable, volume, weight, finish, size, cover, grade, setup, SKUSTRING As String

    Public Sub Add_Comparisons()

        Dim SKU As String = Request("SKU")

        If Session("Comparisons") & "" <> "" Then
            SKU = Session("Comparisons")
            Session("Comparisons") = SKU
        ElseIf SKU & "" <> "" Then
            Session("Comparisons") = SKU
        End If
        Dim x As Integer
        Dim HTML As String

        SKU = Replace(SKU, ",,", ",")
        If Len(SKU) = 1 And InStr(SKU, ",") > 0 Then
            SKU = ""
        End If
        If SKU & "" = "" Then
            main_body.Visible = False
        Else
            main_body.Visible = True
            Dim SKUArray As Array

            SKUArray = Split(SKU, ",")

            HTML = "<table cellpadding=0 cellspacing=1 border=0>"
            picstring = "<tr><td valign=top>&nbsp;</td>"
            vsn = "<tr><td width=175 valign=top>Vendor Stock No</td>"
            itm_cd = "<tr><td valign=top>SKU</td>"
            des = "<tr><td valign=top>Description</td>"
            ret_prc = "<tr><td valign=top>Retail</td>"
            avail = "<tr><td valign=top>Availability</td>"
            treatable = "<tr><td valign=top>Treatable</td>"
            volume = "<tr><td valign=top>Volume</td>"
            weight = "<tr><td valign=top>Weight</td>"
            finish = "<tr><td valign=top>Finish</td>"
            size = "<tr><td valign=top>Size</td>"
            cover = "<tr><td valign=top>Cover</td>"
            grade = "<tr><td valign=top>Grade</td>"
            setup = "<tr><td valign=top>Setup Req'd</td>"
            SKUSTRING = "<tr><td valign=top>Remove</td>"

            Dim Find_String As String = ""

            For x = LBound(SKUArray) To UBound(SKUArray)
                If InStr(Find_String, SKUArray(x)) < 2 Then
                    Compare_Items(SKUArray(x))
                End If
                Find_String = Find_String & "^^" & SKUArray(x)
            Next
            HTML = HTML & picstring & "</tr>"
            HTML = HTML & vsn & "</tr>"
            HTML = HTML & itm_cd & "</tr>"
            HTML = HTML & des & "</tr>"
            HTML = HTML & ret_prc & "</tr>"
            HTML = HTML & avail & "</tr>"
            HTML = HTML & treatable & "</tr>"
            HTML = HTML & volume & "</tr>"
            HTML = HTML & weight & "</tr>"
            HTML = HTML & finish & "</tr>"
            HTML = HTML & size & "</tr>"
            HTML = HTML & cover & "</tr>"
            HTML = HTML & grade & "</tr>"
            HTML = HTML & setup & "</tr>"
            HTML = HTML & SKUSTRING & "</tr>"
            HTML = HTML & "</table>"
            main_body.InnerHtml = HTML
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lbl_error.Text = ""

        Dim SKU As String = ""

        If Session("Comparisons") & "" = "" Then
            SKU = Request("SKU")
        Else
            SKU = Session("Comparisons")
        End If

        If Not IsPostBack Then
            If Request("REMOVE_ITEM") & "" <> "" Then
                SKU = Replace(UCase(SKU), UCase(Request("REMOVE_ITEM")), "")
                SKU = Replace(SKU, ",,", ",")
                Session("Comparisons") = SKU
            End If
            Add_Comparisons()
        End If

        hpl_find_merch.NavigateUrl = "order.aspx?referrer=InventoryComparison.aspx&SKU=" & SKU

    End Sub

    Public Function FileExists(ByVal FileFullPath As String) _
      As Boolean

        Dim f As New IO.FileInfo(FileFullPath)
        Return f.Exists

    End Function

    Public Sub Compare_Items(ByVal SKU As String)

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim MyDataReader As OracleDataReader
        Dim sql As String
        Dim strstcd As String = ""
        Dim POArray As String = ""
        Dim Stock_Availability As String = ""
        Dim x As Double
        Dim y As Double
        Dim picture As String = ""

        Page.Title = ConfigurationManager.AppSettings("app_title").ToString & " Merchandise Comparison"

        conn2 = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn2.Open()

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        picture = ""
        If FileExists(Server.MapPath("~/merch_images") + "\" & SKU & ".gif") Then
            picture = "<img src=""merch_images/" & SKU & ".gif"" height=100 width=133>"
        ElseIf FileExists(Server.MapPath("~/merch_images") + "\" & SKU & ".jpg") Then
            picture = "<img src=""merch_images/" & SKU & ".jpg"" height=100 width=133>"
        ElseIf FileExists(Server.MapPath("~/merch_images") + "\" & SKU & ".png") Then
            picture = "<img src=""merch_images/" & SKU & ".png"" height=100 width=133>"
        Else
            picture = "<img src=""images/image_not_found.jpg"" height=100 width=133>"
        End If

        sql = "SELECT * FROM ITM WHERE ITM.ITM_CD='" & UCase(SKU) & "'"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Dim bgcolor As String = " bgcolor=beige"

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then
                picstring = picstring & "<td align=center valign=middle>" & picture & "</td><td bgcolor=white width=1></td>"
                vsn = vsn & "<td align=left width=160 valign=top" & bgcolor & ">" & MyDataReader.Item("VSN").ToString & "</td><td bgcolor=white></td>"
                itm_cd = itm_cd & "<td align=left valign=top>" & MyDataReader.Item("ITM_CD").ToString & "</td><td bgcolor=white></td>"
                des = des & "<td align=left valign=top width=160" & bgcolor & ">" & MyDataReader.Item("DES").ToString & "</td><td bgcolor=white></td>"
                ret_prc = ret_prc & "<td align=right valign=top>" & FormatCurrency(CDbl(MyDataReader.Item("RET_PRC").ToString), 2) & "</td><td bgcolor=white></td>"
                avail = avail & "<td align=center" & bgcolor & "><b>XXXXXDDDDDXXXXX</b></td><td bgcolor=white></td>"
                treatable = treatable & "<td align=left valign=top>" & MyDataReader.Item("TREATABLE").ToString & "</td><td bgcolor=white></td>"
                volume = volume & "<td align=left valign=top" & bgcolor & ">" & MyDataReader.Item("VOL").ToString & "</td><td bgcolor=white></td>"
                weight = weight & "<td align=left valign=top>" & MyDataReader.Item("WEIGHT").ToString & "</td><td bgcolor=white></td>"
                finish = finish & "<td align=left valign=top" & bgcolor & ">" & MyDataReader.Item("FINISH").ToString & "</td><td bgcolor=white></td>"
                size = size & "<td align=left valign=top>" & MyDataReader.Item("SIZ").ToString & "</td><td bgcolor=white></td>"
                cover = cover & "<td align=left valign=top" & bgcolor & ">" & MyDataReader.Item("COVER").ToString & "</td><td bgcolor=white></td>"
                grade = grade & "<td align=left valign=top>" & MyDataReader.Item("GRADE").ToString & "</td><td bgcolor=white></td>"
                setup = setup & "<td align=left valign=top" & bgcolor & ">" & MyDataReader.Item("SETUP_REQ").ToString & "</td><td bgcolor=white></td>"
                SKUSTRING = SKUSTRING & "<td align=center valign=middle><a href=InventoryComparison.aspx?SKU=" & Request("SKU") & "&REMOVE_ITEM=" & MyDataReader.Item("ITM_CD").ToString & "><img src=""images/stop.jpg"" border=0></a></td><td bgcolor=white></td>"

                If MyDataReader.Item("ITM_TP_CD") = "PKG" Then
                    avail = Replace(avail, "XXXXXDDDDDXXXXX", "INFORMATION UNAVAILABLE")
                End If
            Else
                lbl_error.Text = lbl_error.Text & "SKU " & UCase(SKU) & " was not found" & vbCrLf
                Session("comparisons") = Replace(Session("comparisons"), SKU & ",", "")
            End If
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        objSql.Dispose()

        sql = "SELECT STORE.SHIP_TO_STORE_CD FROM EMP, STORE WHERE EMP.EMP_CD='" & Session("EMP_CD") & "' AND EMP.HOME_STORE_CD = STORE.STORE_CD"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If MyDataReader.Read Then
                strstcd = MyDataReader.Item("SHIP_TO_STORE_CD").ToString
            End If
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        objSql.Dispose()

        sql = "SELECT ITM_FIFL.STORE_CD, ITM_FIFL.LOC_CD, ITM_FIFL.QTY "
        sql = sql & "FROM ITM_FIFL"
        'If ConfigurationManager.AppSettings("pfm") = "Y" Then
        '    sql = sql & ", LOC"
        'End If
        sql = sql & " WHERE ITM_FIFL.ITM_CD='" & SKU & "' " 'AND ITM_FIFL.STORE_CD='" & strstcd & "' "
        'If ConfigurationManager.AppSettings("pfm") = "Y" Then
        '    sql = sql & "AND LOC.LOC_CD=ITM_FIFL.LOC_CD AND LOC.PICK = 'Y' AND LOC.STORE_CD=ITM_FIFL.STORE_CD "
        'End If
        sql = sql & "ORDER BY ITM_FIFL.LOC_CD"

        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read
                x = x + CDbl(MyDataReader.Item("QTY").ToString)
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn2.Close()
            conn.Close()
            Throw
        End Try

        objSql.Dispose()

        ''Get Purchase Order Information

        sql = "SELECT a.PO_CD, b.LN#, b.ARRIVAL_DT, b.QTY_ORD As Total_Incoming "
        sql = sql & "FROM PO a, PO_LN b "
        sql = sql & "WHERE a.STAT_CD = 'O' AND a.PO_CD = b.PO_CD AND b.ITM_CD = '" & SKU & "' " ' AND b.DEST_STORE_CD = '" & strstcd & "'
        sql = sql & "ORDER BY ARRIVAL_DT ASC"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        y = 0
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Dim POArray
            Dim n
            Dim pccount
            n = 1
            Do While MyDataReader.Read()
                For pccount = 1 To CDbl(MyDataReader.Item("Total_Incoming"))
                    If MyDataReader.Item("Arrival_dt").ToString & "" <> "" Then
                        POArray = POArray & "|" & MyDataReader.Item("Arrival_dt").ToString
                        n = n + 1
                    End If
                Next
                If IsNumeric(MyDataReader.Item("Arrival_dt").ToString) Then
                    y = y + CDbl(MyDataReader.Item("Total_Incoming").ToString)
                End If
            Loop
            MyDataReader.Close()
        Catch ex As Exception

        End Try

        objSql.Dispose()

        'Get Sales Order Information

        Dim z As Double
        sql = "SELECT SO.DEL_DOC_NUM, SO.SO_WR_DT, SO_LN.QTY, SO.PU_DEL_DT FROM SO, SO_LN WHERE SO.DEL_DOC_NUM=SO_LN.DEL_DOC_NUM AND SO_LN.STORE_CD IS NULL AND SO_LN.VOID_FLAG='N' "
        sql = sql & "AND SO.STAT_CD='O' AND SO.ORD_TP_CD='SAL' AND PU_DEL_STORE_CD='" & strstcd & "' AND SO_LN.ITM_CD='" & SKU
        sql = sql & "' ORDER BY SO.SO_WR_DT ASC"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        z = 0
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read()
                z = z + CDbl(MyDataReader.Item("QTY").ToString)
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        objSql.Dispose()

        Dim POSplit
        POSplit = Split(POArray, "|")
        Dim Avail_dt As String

        If x - z > 0 Then
            Stock_Availability = "   STOCK AVAILABLE   "
            Avail_dt = DateTime.Today
        ElseIf (x + y) - z > 0 Then
            If z - x > y Then
                Stock_Availability = " ALL PURCHASE ORDERS RESERVED "
                Avail_dt = DateTime.Today.AddDays(30)
            Else
                Stock_Availability = " EST AVAILABILITY : " & POSplit((z - x) + 1)
                Avail_dt = POSplit((z - x) + 1)
            End If
        Else
            Stock_Availability = "NO STOCK AVAILABLE"
            Avail_dt = DateTime.Today.AddDays(30)
        End If

        conn.Close()
        conn2.Close()

        avail = Replace(avail, "XXXXXDDDDDXXXXX", Stock_Availability)

    End Sub

    Protected Sub btn_add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add.Click

        lbl_error.Text = ""
        Dim SKU As String = TextBox1.Text
        If Session("Comparisons") & "" <> "" Then
            SKU = Session("Comparisons") & "," & SKU
            Session("Comparisons") = SKU
        Else
            Session("Comparisons") = SKU
        End If
        Add_Comparisons()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
