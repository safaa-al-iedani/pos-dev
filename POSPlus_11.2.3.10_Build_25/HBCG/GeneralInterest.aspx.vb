Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class GeneralInterest
    Inherits POSBasePage

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataadapter
        Dim mytable As DataTable
        Dim numrows As Integer

        ds = New DataSet

        grd_gen_int.DataSource = ""

        conn.Open()

        sql = "SELECT GEN_CAT_ID, DES, MJR_CD, MNR_CD, DECODE(CUSTOM,'Y',1,0) AS CUSTOM, DECODE(DELETED,NULL,1,0) AS DELETED FROM RELATIONSHIP_GEN_INT ORDER BY DES"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                grd_gen_int.DataSource = ds
                grd_gen_int.DataBind()
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    'Public Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

    '    Dim conn As New OracleConnection
    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As OracleDataReader

    '    conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '    conn.Open()

    '    sql = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

    '    'Set SQL OBJECT 
    '    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

    '    Try
    '        MyDataReader = objSql.ExecuteReader
    '        If (MyDataReader.Read()) Then
    '            If MyDataReader.Item("EMP_CD") & "" <> "" Then
    '                Authorize_User = True
    '            Else
    '                Authorize_User = False
    '            End If
    '        Else
    '            Authorize_User = False
    '        End If
    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try

    'End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("SYSPM", Session("EMP_CD")) = "N" Then
            admin_form.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If
        If Not IsPostBack Then
            bindgrid()
        End If

    End Sub

    Protected Sub btn_add_majors_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add_majors.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataadapter

        ds = New DataSet

        grd_gen_int.DataSource = ""

        conn.Open()

        sql = "SELECT MJR_CD FROM RELATIONSHIP_GEN_INT"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds, "GEN_INT")
        Dim myView As DataView
        myView = ds.Tables("GEN_INT").DefaultView

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As OracleCommand
        Dim objSql3 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Integer = 0
        Dim Add_Row As Boolean = False

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT INV_MJR.MJR_CD, INV_MJR.DES FROM INV_MJR ORDER BY INV_MJR.MJR_CD"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Open Connection 
            conn2.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql2)

            'Store Values in String Variables 
            Do While MyDataReader.Read
                Add_Row = True
                myView.RowFilter = "MJR_CD='" & MyDataReader.Item("MJR_CD").ToString & "'"
                For x = 0 To myView.Count - 1
                    If myView(0).Item("MJR_CD").ToString = MyDataReader.Item("MJR_CD").ToString Then
                        Add_Row = False
                        Exit For
                    End If
                Next
                If Add_Row = True Then
                    sql = "INSERT INTO RELATIONSHIP_GEN_INT (MJR_CD, DES) "
                    sql = sql & "VALUES('" & MyDataReader.Item("MJR_CD").ToString & "',"
                    sql = sql & "'" & Replace(MyDataReader.Item("DES").ToString, "'", "''") & "')"
                    objSql3 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql3.ExecuteNonQuery()
                End If
            Loop
            'Close Connection 
            MyDataReader.Close()
            conn2.Close()
        Catch ex As Exception
            conn2.Close()
            conn.Close()
            Throw
        End Try

        conn.Close()
        bindgrid()

    End Sub

    Protected Sub btn_add_minors_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_add_minors.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim ds As DataSet
        Dim oAdp As OracleDataadapter

        ds = New DataSet

        grd_gen_int.DataSource = ""

        conn.Open()

        sql = "SELECT MNR_CD FROM RELATIONSHIP_GEN_INT"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds, "GEN_INT")
        Dim myView As DataView
        myView = ds.Tables("GEN_INT").DefaultView

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As OracleCommand
        Dim objSql3 As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim x As Integer = 0
        Dim Add_Row As Boolean = False

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT MJR_CD, MNR_CD, DES FROM INV_MNR ORDER BY MNR_CD"

        'Set SQL OBJECT 
        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Open Connection 
            conn2.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql2)

            'Store Values in String Variables 
            Do While MyDataReader.Read
                Add_Row = True
                myView.RowFilter = "MNR_CD='" & MyDataReader.Item("MNR_CD").ToString & "'"
                For x = 0 To myView.Count - 1
                    If myView(0).Item("MNR_CD").ToString = MyDataReader.Item("MNR_CD").ToString Then
                        Add_Row = False
                        Exit For
                    End If
                Next
                If Add_Row = True Then
                    sql = "INSERT INTO RELATIONSHIP_GEN_INT (MJR_CD, MNR_CD, DES) "
                    sql = sql & "VALUES('" & MyDataReader.Item("MJR_CD").ToString & "',"
                    sql = sql & "'" & MyDataReader.Item("MNR_CD").ToString & "',"
                    sql = sql & "'" & Replace(MyDataReader.Item("DES").ToString, "'", "''") & "')"
                    objSql3 = DisposablesManager.BuildOracleCommand(sql, conn)
                    objSql3.ExecuteNonQuery()
                End If
            Loop
            'Close Connection 
            MyDataReader.Close()
            conn2.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try

        conn.Close()
        bindgrid()

    End Sub

    Protected Sub btn_new_custom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_new_custom.Click

        If String.IsNullOrEmpty(txt_custom_add.Text) Then
            lbl_error.Text = "You must enter a description"
        Else
            Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
            Dim sql As String
            Dim objSql As OracleCommand

            conn.Open()

            sql = "INSERT INTO RELATIONSHIP_GEN_INT (DES, CUSTOM) "
            sql = sql & "VALUES('" & txt_custom_add.Text.ToString & "','Y')"
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            objSql.ExecuteNonQuery()

            'Close Connection 
            conn.Close()

            bindgrid()
        End If

    End Sub

    'Protected Sub grd_gen_int_DeleteRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles grd_gen_int.DeleteRow

    '    If Not String.IsNullOrEmpty(e.Row.Cells.FromKey("GEN_CAT_ID").Value().ToString) Then

    '        Dim conn As New OracleConnection
    '        Dim sql As String
    '        Dim objSql As OracleCommand
    '        conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '        conn.Open()

    '        sql = "DELETE FROM RELATIONSHIP_GEN_INT "
    '        sql = sql & "WHERE GEN_CAT_ID=" & e.Row.Cells.FromKey("GEN_CAT_ID").Value().ToString
    '        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
    '        objSql.ExecuteNonQuery()

    '        'Close Connection 
    '        conn.Close()

    '    End If

    'End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub grd_gen_int_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_gen_int.ItemDataBound

        If e.Item.Cells(4).Text & "" <> "" Then
            Dim ASPxCheckBox1 As DevExpress.Web.ASPxEditors.ASPxCheckBox = CType(e.Item.FindControl("ASPxCheckBox1"), DevExpress.Web.ASPxEditors.ASPxCheckBox)
            If Not IsNothing(ASPxCheckBox1) Then
                ASPxCheckBox1.Checked = e.Item.Cells(4).Text
            End If
        End If
        If e.Item.Cells(10).Text & "" <> "" Then
            Dim ASPxCheckBox2 As DevExpress.Web.ASPxEditors.ASPxCheckBox = CType(e.Item.FindControl("ASPxCheckBox2"), DevExpress.Web.ASPxEditors.ASPxCheckBox)
            If Not IsNothing(ASPxCheckBox2) Then
                ASPxCheckBox2.Checked = e.Item.Cells(10).Text
            End If
        End If
        If e.Item.Cells(1).Text & "" <> "" Then
            Dim lbl_desc As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_desc"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_desc) Then
                lbl_desc.Text = e.Item.Cells(1).Text
            End If
        End If
        If e.Item.Cells(2).Text & "" <> "" Then
            Dim lbl_mjr_cd As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_mjr_cd"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_mjr_cd) Then
                lbl_mjr_cd.Text = e.Item.Cells(2).Text
            End If
        End If
        If e.Item.Cells(3).Text & "" <> "" And e.Item.Cells(3).Text <> "&nbsp;" Then
            Dim lbl_mnr_cd As DevExpress.Web.ASPxEditors.ASPxLabel = CType(e.Item.FindControl("lbl_mnr_cd"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_mnr_cd) Then
                lbl_mnr_cd.Text = e.Item.Cells(3).Text
            End If
        End If

    End Sub

    Protected Sub Custom_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim chk_carton As DevExpress.Web.ASPxEditors.ASPxCheckBox = CType(sender, DevExpress.Web.ASPxEditors.ASPxCheckBox)
        Dim dgItem As DataGridItem = CType(chk_carton.NamingContainer, DataGridItem)
        If chk_carton.Checked = True Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update RELATIONSHIP_GEN_INT set CUSTOM = 'Y' where GEN_CAT_ID=" & dgItem.Cells(0).Text
            End With
        Else
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update RELATIONSHIP_GEN_INT set CUSTOM = 'N' where GEN_CAT_ID=" & dgItem.Cells(0).Text
            End With
        End If
        cmdDeleteItems.ExecuteNonQuery()
        conn.Close()

    End Sub

    Protected Sub Deleted_Update(ByVal sender As Object, ByVal e As EventArgs)

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdDeleteItems As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()

        Dim chk_carton As DevExpress.Web.ASPxEditors.ASPxCheckBox = CType(sender, DevExpress.Web.ASPxEditors.ASPxCheckBox)
        Dim dgItem As DataGridItem = CType(chk_carton.NamingContainer, DataGridItem)
        If chk_carton.Checked = True Then
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update RELATIONSHIP_GEN_INT set DELETED = 'Y' where GEN_CAT_ID=" & dgItem.Cells(0).Text
            End With
        Else
            With cmdDeleteItems
                .Connection = conn
                .CommandText = "update RELATIONSHIP_GEN_INT set DELETED = 'N' where GEN_CAT_ID=" & dgItem.Cells(0).Text
            End With
        End If
        cmdDeleteItems.ExecuteNonQuery()
        conn.Close()

    End Sub

End Class
