Imports System.Data.OracleClient
Imports HBCG_Utils

Partial Class Picture_Upload
    Inherits POSBasePage

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.AppendHeader("refresh", SysPms.appTimeOut + ";url=timeout.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("ITEM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If

        img_logo.ImageUrl = "~/" & ConfigurationManager.AppSettings("company_logo").ToString

        If Session("EMP_CD") & "" = "" Then
            Response.Redirect("login.aspx")
        End If

        If ConfigurationManager.AppSettings("system_mode") = "TRAIN" Then
            lbl_header.Text = "* TRAIN MODE *"
            lbl_header.ForeColor = Color.Red
        End If

        If Not IsPostBack Then
            Dim sql As String
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

            Dim objSql As OracleCommand
            Dim MyDataReader As OracleDataReader

            If Session("EMP_FNAME") & "" = "" Or Session("EMP_LNAME") & "" = "" Then
                If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                    ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
                    Throw New Exception("Connection Error")
                Else
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                End If

                sql = "SELECT FNAME, LNAME, EMP_CD, HOME_STORE_CD FROM EMP WHERE EMP_CD='" & UCase(Session("EMP_CD")) & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Open Connection 
                    conn.Open()
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        Session("EMP_CD") = MyDataReader.Item("EMP_CD").ToString
                        Session("EMP_FNAME") = MyDataReader.Item("FNAME").ToString
                        Session("EMP_LNAME") = MyDataReader.Item("LNAME").ToString
                        Session("HOME_STORE_CD") = MyDataReader.Item("HOME_STORE_CD").ToString
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try

                sql = "SELECT SHIP_TO_STORE_CD FROM STORE WHERE STORE_CD='" & UCase(Session("HOME_STORE_CD")) & "'"

                'Set SQL OBJECT 
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                    'Store Values in String Variables 
                    If (MyDataReader.Read()) Then
                        Session("pd_store_cd") = MyDataReader.Item("SHIP_TO_STORE_CD").ToString
                    End If
                    'Close Connection 
                    MyDataReader.Close()
                    conn.Close()
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            End If
            If Session("EMP_FNAME") & "" <> "" And Session("EMP_LNAME") & "" <> "" Then
                lbl_Header1.Text = "Welcome " & Left(Session("EMP_FNAME"), 1) & LCase(Right(Session("EMP_FNAME"), Len(Session("EMP_FNAME")) - 1)) & " " & Left(Session("EMP_LNAME"), 1) & LCase(Right(Session("EMP_LNAME"), Len(Session("EMP_LNAME")) - 1))
            End If
            If Session("HOME_STORE_CD") & "" <> "" Then
                lbl_Header2.Text = "Store Code: " & Session("HOME_STORE_CD")
            End If
            If Request("D") = "D" Then
                lblTabID.Text = Session("MP")
            End If
            If Request("D") = "I" Then
                lblTabID.Text = Session("IP")
            End If

            If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink1").ToString) Then
                ASPxMenu1.RootItem.Items(4).Text = ConfigurationManager.AppSettings("link1_desc").ToString
                ASPxMenu1.RootItem.Items(4).NavigateUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink1").ToString
                ASPxMenu1.RootItem.Items(4).Target = "_new"
            End If
            If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Hyperlink2").ToString) Then
                ASPxMenu1.RootItem.Items(5).Text = ConfigurationManager.AppSettings("link2_desc").ToString
                ASPxMenu1.RootItem.Items(5).NavigateUrl = "http://" & ConfigurationManager.AppSettings("Hyperlink2").ToString
                ASPxMenu1.RootItem.Items(5).Target = "_new"
            End If
        End If

        lblTabID.Text = "IMAGE UPLOAD"
        Page.Title = ConfigurationManager.AppSettings("app_title").ToString & "Image Upload"

    End Sub

    'Public Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

    '    Dim conn As New OracleConnection
    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As oracleDataReader

    '    conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '    conn.Open()

    '    sql = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

    '    'Set SQL OBJECT 
    '    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

    '    Try
    '        MyDataReader = objSql.ExecuteReader
    '        If (MyDataReader.Read()) Then
    '            If MyDataReader.Item("EMP_CD") & "" <> "" Then
    '                Authorize_User = True
    '            Else
    '                Authorize_User = False
    '            End If
    '        Else
    '            Authorize_User = False
    '        End If
    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try

    'End Function

    Public Sub Upload_Click(ByVal Sender As Object, ByVal e As EventArgs)

        ' Display properties of the uploaded file
        FileName.InnerHtml = MyFile.PostedFile.FileName
        FileContent.InnerHtml = MyFile.PostedFile.ContentType
        FileSize.InnerHtml = MyFile.PostedFile.ContentLength
        UploadDetails.Visible = True

        'Grab the file name from its fully qualified path at client 
        Dim strFileName As String = MyFile.PostedFile.FileName

        ' only the attched file name not its path
        Dim c As String = System.IO.Path.GetFileName(strFileName)

        'Save uploaded file to server at LOCAL PATH
        Try
            MyFile.PostedFile.SaveAs(Server.Mappath("~/merch_images") + "\" + c)
            Span1.InnerHtml = "Your file successfully uploaded as: " & Server.Mappath("~/merch_images") + "\" + c
        Catch Exp As Exception
            Span1.InnerHtml = "An Error occured. Please check the attached file"
            UploadDetails.Visible = False
            Span2.Visible = False
        End Try
    End Sub

End Class
