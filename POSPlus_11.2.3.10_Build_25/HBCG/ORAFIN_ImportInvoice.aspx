<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false" 
    CodeFile="ORAFIN_ImportInvoice.aspx.vb" Inherits="ORAFIN_ImportInvoice" meta:resourcekey="PageResource1" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div id="Div1" runat="server">  
   
     <%-- =============== HEADING ==========================================   --%> 
    <table  width="100%" class="style5">
      <tr>
      <td align="center" > 
      <asp:Label ID="Label1" runat="server" Text="Force Match Invoice" Font-Bold="True" ></asp:Label>
      </td>
      </tr>
     </table>
       <br />
      <%-- ===============MESSAGES =========lbl_msg ==========================================   --%> 

     <table>
         <tr>
             <td>
     <dx:ASPxLabel ID="lbl_msg" Font-Bold="True" Font-Size="small" ForeColor="Blue" BackColor="White" runat="server"
        Text="" Width="100%"> 
     </dx:ASPxLabel>
            </td>
            </tr>
     </table>
  <%-- ============================================   --%> 
    <asp:Panel ID="Panel5" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >
     <table>
          
         <tr>
           <td style="text-align:right"> <dx:ASPxLabel ID="ASPxLabel5" Text="CO CD" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
           <td> <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" 
                Width  ="300px" ID="srch_co_cd" autopostback="true" >                   
                </asp:dropdownlist>   </td>
           </tr>
         <tr></tr>
          <tr></tr>
           
         <tr>
           <td style="text-align:right"> <dx:ASPxLabel ID="ASPxLabel2" Text="VE CD" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
           <td> <asp:dropdownlist runat="server" Style="position: relative" AppendDataBoundItems="true" 
                Width  ="300px" ID="srch_ve_cd">                   
                </asp:dropdownlist>   </td>
          </tr>
         <tr></tr>
          <tr></tr>
         
                <tr>
           
           <td style="text-align:right"> <dx:ASPxLabel ID="ASPxLabel6" Text="IVC CD" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
           <td>  <asp:TextBox ID="srch_ivc_cd" style='text-transform:uppercase' runat="server"></asp:TextBox>  </td>
          </tr>
          <tr>
           
           <td style="text-align:right"> <dx:ASPxLabel ID="ASPxLabel1" Text="PO CD" Font-Bold="true" runat="server" meta:resourcekey="Label1"> </dx:ASPxLabel> </td>
           <td> <asp:TextBox ID="srch_po_cd" style='text-transform:uppercase' runat="server"></asp:TextBox>     </td>
          </tr>
             
          <tr></tr>
          <tr></tr>    
          </table>
    <br />
  
        
      <table>
          <%-- SEARCH BUTTON --%>
            <tr>
                <td style="width: 60px;">
                <dx:ASPxButton ID="btn_search" runat="server" Font-Bold="true" Text="<%$ Resources:LibResources, Label526 %>">
                </dx:ASPxButton>
           </td>   
                         
         <%-- CLEAR BUTTON --%>
           <td>
                <dx:ASPxButton ID="btn_Clear" runat="server" Font-Bold="true" Text="<%$ Resources:LibResources, Label82 %>">
                </dx:ASPxButton>
           </td> 
       </tr>
       </table>
       </asp:Panel>
    <br />
    <br />
  
<%-- -------------------------------------------------- ----------------------------------------------------- --%>
<%-- -------------------------------------------------- ----------------------------------------------------- --%>
<%-- ===========================================================================   --%>  
 <dxpc:ASPxPopupControl ID="ASPxPopupORAFIN_ImportInvoice" runat="server" AllowDragging="True"
        CloseAction="CloseButton" HeaderText="*** Force Match Invoices ***" Font-Bold="true" Height="80px" PopupAction="None"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="750px"
        CssClass="style5">
        <ModalBackgroundStyle BackColor="#E0E0E0">
        </ModalBackgroundStyle>
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" DefaultButton="btnSubmit" runat="server">
        
         <br />
         <br />
    
<%-- ===== --%>
    <asp:Panel ID="Panel1" runat="server" BorderStyle="solid" BackColor="WhiteSmoke" BorderWidth="1px" >  

         <table>
         <tr>
             <td>
     <dx:ASPxLabel ID="lbl_msg2" Font-Bold="True" Font-Size="small" ForeColor="Blue" BackColor="White" runat="server"
        Text="" Width="100%"> 
     </dx:ASPxLabel>
            </td>
            </tr>
     </table>
     <%-- ----------------------------------------------------------------------- --%>  
     <%-- -------------------UNMATCHED GRIDVIEW --------------------------------- --%>  
     <%-- ----------------------------------------------------------------------- --%>         
         <dx:ASPxGridView ID="GridView1" Settings-ShowFooter="true" runat="server" 
            KeyFieldName="CO_CD;VE_CD;IVC_CD;PO_CD;IVC_AMT" AutoGenerateColumns="false" 
             Width="98%" OnCommandButtonInitialize="ASPxGridView1_CommandButtonInitialize" ClientInstanceName="GridView1" >
             <SettingsBehavior AllowSelectByRowClick="true" AllowSelectSingleRowOnly="false"/> 
     
      <Columns>
         <dx:GridViewDataTextColumn Caption="COCD" FieldName="CO_CD" VisibleIndex="2">
                <PropertiesTextEdit Width="100"  ReadOnlyStyle-BackColor="Snow" MaxLength="10"></PropertiesTextEdit>
             <CellStyle Wrap="False" /> 
           </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Vendor" FieldName="VE_CD"  VisibleIndex="3">
                <PropertiesTextEdit Width="100"  ReadOnlyStyle-BackColor="Snow" MaxLength="10"></PropertiesTextEdit>
             <CellStyle Wrap="False" />                 
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Invoice#" FieldName="IVC_CD" VisibleIndex="4">
                <PropertiesTextEdit Width="150"  ReadOnlyStyle-BackColor="Snow" MaxLength="10"></PropertiesTextEdit>
             <CellStyle Wrap="False" />                 
           </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="PO CD" FieldName="PO_CD" VisibleIndex="5">
               <PropertiesTextEdit Width="400"  ReadOnlyStyle-BackColor="Snow" MaxLength="10"></PropertiesTextEdit>
             <CellStyle Wrap="False" />                 
          </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn Caption="Invoice Amount" FieldName="IVC_AMT" VisibleIndex="6">
               <PropertiesTextEdit Width="150"  ReadOnlyStyle-BackColor="Snow" MaxLength="10"></PropertiesTextEdit>
             <CellStyle Wrap="False" />                 
          </dx:GridViewDataTextColumn>
                           
         <dx:GridViewDataTextColumn Caption="Match DT" FieldName="MATCH_DT" ReadOnly="true" Visible="true" VisibleIndex="13">
           <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" DisplayFormatString="MM/dd/yyyy"  Width="100"></PropertiesTextEdit>
         <CellStyle Wrap="False" />   
         </dx:GridViewDataTextColumn>
        
         <dx:GridViewDataTextColumn Caption="EMP CD" FieldName="EMP_CD_OP" ReadOnly="true" Visible="true" VisibleIndex="14">
             <PropertiesTextEdit ReadOnlyStyle-BackColor="Snow" Width="100"></PropertiesTextEdit>
        <CellStyle Wrap="False" />   
              </dx:GridViewDataTextColumn>
       
          <dx:GridViewCommandColumn Caption="ForceMatch" ShowSelectCheckbox="true" ShowEditButton="false" ShowNewButtonInHeader="false" ShowDeleteButton="false" VisibleIndex="21">
         </dx:GridViewCommandColumn>        
     
      </Columns>    
        
        <SettingsBehavior  AllowSort="False" />
        <SettingsPager  Position="Bottom" Visible="true" Mode="ShowPager"  PageSize="10"  />
    </dx:ASPxGridView>
     
<table >
     <tr>
        
       <td>
        <asp:Button ID="btn_forceMatch" runat="server" Font-Bold="true" Text="Force Match" />

        <asp:Button ID="btn_deselect" runat="server" Font-Bold="true" Text="DeSelect All" />
     </td>
     </tr>
</table>
    </asp:Panel>

</dxpc:PopupControlContentControl>
</ContentCollection>
</dxpc:ASPxPopupControl>

 
    <br />
   </div>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
