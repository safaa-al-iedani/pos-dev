Imports System.Data.OracleClient
Imports System.IO
Imports HBCG_Utils

Partial Class Quote_Admin
    Inherits POSBasePage

    Public Sub bindgrid()

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer

        Dim dirInfo As New DirectoryInfo(Request.ServerVariables("APPL_PHYSICAL_PATH") & "Invoices\")

        cbo_default_file.Items.Clear()
        cbo_default_file.Items.Insert(0, "Please Select A File")
        cbo_default_file.Items.FindByText("Please Select A File").Value = ""
        cbo_default_file.SelectedIndex = 0

        With cbo_default_file
            .DataSource = dirInfo.GetFiles("*.aspx")
            .DataValueField = "Name"
            .DataTextField = "Name"
            .DataBind()
        End With

        ds = New DataSet

        Gridview1.DataSource = ""
        'CustTable.Visible = False

        Gridview1.Visible = True

        conn.Open()

        sql = "SELECT * FROM QUOTE_ADMIN order by STORE_CD"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                If MyDataReader.Item("DEFAULT_FILE").ToString & "" <> "" Then
                    cbo_default_file.SelectedValue = MyDataReader.Item("DEFAULT_FILE").ToString
                End If
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Find_Security("OLPM", Session("EMP_CD")) = "N" Then
            frmsubmit.InnerHtml = "We're sorry, you don't have access to this page.  If you feel you've reached this page in error, please contact your system administrator."
            Exit Sub
        End If

        If Not IsPostBack Then
            'Locate stores added to the system and automatically add them to the the table
            Find_New_Stores()
            bindgrid()
        End If

    End Sub

    Public Sub Find_New_Stores()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim sql As String
        Dim store_listing As String = ""
        Dim default_invoice As String = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        End If

        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objSql2 As OracleCommand
        Dim MyDataReader2 As OracleDataReader

        conn2 = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        conn2.Open()

        sql = "select store_cd, default_file from quote_admin order by store_cd"

        objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
        Try
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objSql2)

            Do While MyDataReader2.Read
                store_listing = store_listing & MyDataReader2.Item("STORE_CD").ToString & "|"
                default_invoice = MyDataReader2.Item("DEFAULT_FILE").ToString
            Loop
            MyDataReader2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

        If default_invoice & "" = "" Then default_invoice = "Invoice.aspx"

        conn.Open()
        sql = "SELECT STORE_CD FROM STORE ORDER BY STORE_CD"
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        Try
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            Do While MyDataReader.Read
                If InStr(UCase(store_listing), UCase(MyDataReader.Item("STORE_CD").ToString)) < 1 Then
                    sql = "INSERT INTO QUOTE_ADMIN (STORE_CD, DEFAULT_FILE) VALUES('" & MyDataReader.Item("STORE_CD").ToString & "','" & default_invoice & "')"
                    objSql2 = DisposablesManager.BuildOracleCommand(sql, conn2)
                    objSql2.ExecuteNonQuery()
                    lbl_msg.Visible = True
                    lbl_msg.Text = lbl_msg.Text & MyDataReader.Item("STORE_CD").ToString & "<br />"
                End If
            Loop
            MyDataReader.Close()
        Catch ex As Exception
            conn.Close()
            conn2.Close()
            Throw
        End Try
        conn.Close()

        conn2.Close()

    End Sub

    'Public Sub Upload_Click(ByVal Sender As Object, ByVal e As EventArgs)

    '    ' Display properties of the uploaded file
    '    FileName.InnerHtml = MyFile.PostedFile.FileName
    '    FileContent.InnerHtml = MyFile.PostedFile.ContentType
    '    FileSize.InnerHtml = MyFile.PostedFile.ContentLength
    '    UploadDetails.Visible = True

    '    'Grab the file name from its fully qualified path at client 
    '    Dim strFileName As String = MyFile.PostedFile.FileName

    '    ' only the attched file name not its path
    '    Dim c As String = System.IO.Path.GetFileName(strFileName)

    '    'Save uploaded file to server at LOCAL PATH
    '    Try
    '        MyFile.PostedFile.SaveAs(Request.ServerVariables("APPL_PHYSICAL_PATH") + "Invoices\" + c)
    '        Span1.InnerHtml = "Your file successfully uploaded as: " & _
    '                          Request.ServerVariables("APPL_PHYSICAL_PATH") & "Invoices\" & c
    '    Catch Exp As Exception
    '        Span1.InnerHtml = "An Error occured. Please check the attached file"
    '        UploadDetails.Visible = False
    '        Span2.Visible = False
    '    End Try

    'End Sub


    'Public Function Authorize_User(ByVal emp_cd As String, ByVal pagename As String)

    '    Dim conn As New OracleConnection
    '    Dim sql As String
    '    Dim objSql As OracleCommand
    '    Dim MyDataReader As OracleDataReader

    '    conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("LOCAL").ConnectionString)
    '    conn.Open()

    '    sql = "SELECT EMP_CD FROM USER_ACCESS WHERE EMP_CD='" & emp_cd & "' AND " & pagename & "='Y'"

    '    'Set SQL OBJECT 
    '    objSql = DisposablesManager.BuildOracleCommand(sql, conn)

    '    Try
    '        MyDataReader = objSql.ExecuteReader
    '        If (MyDataReader.Read()) Then
    '            If MyDataReader.Item("EMP_CD") & "" <> "" Then
    '                Authorize_User = True
    '            Else
    '                Authorize_User = False
    '            End If
    '        Else
    '            Authorize_User = False
    '        End If
    '        'Close Connection 
    '        MyDataReader.Close()
    '        conn.Close()
    '    Catch ex As Exception
    '        conn.Close()
    '        Throw
    '    End Try

    'End Function

    Protected Sub PopulateFiles(ByVal sender As Object, ByVal e As DataGridItemEventArgs)

        Dim myGrid As DropDownList = e.Item.Cells(1).FindControl("drpInvFile")
        Dim arrFiles() As String = Directory.GetFileSystemEntries((Request.ServerVariables("APPL_PHYSICAL_PATH") + "Invoices\"))
        Dim i As Integer = 0
        Dim strStCd As String
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        conn.Open()
        'Set SQL OBJECT 

        If (myGrid Is Nothing) Then

        Else
            Do While (i < arrFiles.GetUpperBound(0))
                'get the file info
                Dim thisfile As FileInfo = New FileInfo(arrFiles(i))
                'add the item to the list
                myGrid.Items.Add((thisfile.Name))
                i = (i + 1)
            Loop
            strStCd = e.Item.Cells(0).Text
            sql = "SELECT quote_file FROM quote_admin WHERE store_cd ='" & strStCd & "' "
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Set SQL OBJECT 
            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            If (MyDataReader.Read()) Then
                If MyDataReader.Item("quote_file") & "" <> "" Then
                    myGrid.Text = MyDataReader.Item("quote_file")
                End If
                MyDataReader.Close()
            End If
        End If
        conn.Close()

    End Sub

    Protected Sub DoItemSave(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Gridview1.DeleteCommand

        Dim drpfile As DropDownList
        Dim strFileName As String
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdate As OracleCommand = DisposablesManager.BuildOracleCommand


        drpfile = e.Item.FindControl("drpInvFile")

        strFileName = drpfile.SelectedItem.Value

        ' Update TempValue to DB
        conn.Open()
        With cmdUpdate
            .Connection = conn
            .CommandText = "UPDATE quote_admin SET quote_file = '" & strFileName & "'  WHERE store_cd ='" & e.Item.Cells(0).Text & "' "
            .ExecuteNonQuery()
        End With

        cmdUpdate.Dispose()
        conn.Close()

    End Sub

    Protected Sub btn_default_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_default.Click

        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmdUpdate As OracleCommand = DisposablesManager.BuildOracleCommand


        conn.Open()
        With cmdUpdate
            .Connection = conn
            .CommandText = "UPDATE quote_admin SET default_file = '" & cbo_default_file.SelectedValue.ToString & "'"
            .ExecuteNonQuery()
        End With
        cmdUpdate.Dispose()
        conn.Close()

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
End Class
