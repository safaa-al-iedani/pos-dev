<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="ItemPriceQuery.aspx.vb" Inherits="ItemPriceQuery" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table border="0" style="position: relative; width: 1100px; left: 0px; top: 0px;">
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_sku" runat="server" meta:resourcekey="lbl_sku">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_sku" runat="server" Style="position: relative" Width="100px" MaxLength="9"></asp:TextBox>
            </td>
             <td class="class_tdlbl" colspan="1" align="left">
                <dx:ASPxLabel ID="lbl_iic" runat="server"  meta:resourcekey="lbl_iic" >
                </dx:ASPxLabel>
            </td>
             <td class="class_tdtxt" colspan="1" align="left">
                <asp:ListBox ID="lst_iic_desc" runat="server" Style="position: relative" Width="100px" Height ="55">
                </asp:ListBox>
            </td>
           <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_des" runat="server" meta:resourcekey="lbl_des">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="2">
                <asp:TextBox ID="txt_des" runat="server" Width="490px" MaxLength="50"></asp:TextBox>
                <asp:TextBox ID="txt_ret_prc" runat="server" Width="10px" MaxLength="50"  Visible="false"></asp:TextBox>
            </td> 
        </tr>
        <tr>
            <td class="class_tdlbl" style="width: 100px" align="right">
                <dx:ASPxLabel ID="lbl_store_cd" runat="server" meta:resourcekey="lbl_store_cd" Width="130px">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdddlb" style="width: 400px" colspan ="3">
                     <asp:DropDownList ID="txt_store_cd" runat="server" Style="position: relative; top: 0px; left: 0px;" AppendDataBoundItems="true">
                     </asp:DropDownList>
            </td>

            <td class="class_tdlbl" style="width: 100px" align="right">
                <dx:ASPxLabel ID="lbl_as_of_date" runat="server" meta:resourcekey="lbl_as_of_date">
                </dx:ASPxLabel>
            </td>

            <td class="class_tddtpkr" style="width: 210px">
                <span class="innerdp">
                <dx:ASPxDateEdit ID="dateEdit" runat="server" Width="200" EditFormat="Custom" DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" 
                    AutoPostBack="True">
                </dx:ASPxDateEdit>
                </span>

            </td>
            <td class="class_tdbtn" style="width: 290px" align="right">
                <dx:ASPxButton ID="btn_Lookup" runat="server" meta:resourcekey="lbl_btn_lookup_prices">
                </dx:ASPxButton>
                &nbsp;
                <dx:ASPxButton ID="btn_Clear" runat="server" meta:resourcekey="lbl_btn_clear_screen">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>

    
    <table border="0" style="position: relative; width: 1100px; left: 0px; top: 0px;">
        <tr><td colspan="7">
        <hr />
        </td></tr>

        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_ve_name" runat="server" meta:resourcekey="lbl_ve_name">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_ve_name" runat="server" Width="310px" MaxLength="40"></asp:TextBox>
            </td>         
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_vsn" runat="server" meta:resourcekey="lbl_vsn">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="4">
                <asp:TextBox ID="txt_vsn" runat="server" Width="585px" MaxLength="100"></asp:TextBox>
            </td> 
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_major_des" runat="server" meta:resourcekey="lbl_major_des">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_major_des" runat="server" Width="310px" MaxLength="50"></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_minor_des" runat="server" meta:resourcekey="lbl_minor_des">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="4">
                <asp:TextBox ID="txt_minor_des" runat="server" Width="585px" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_itm_tp_cd" runat="server" meta:resourcekey="lbl_itm_tp_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_itm_tp_cd" runat="server" Width="110px" MaxLength="10"></asp:TextBox>
                <dx:ASPxLabel ID="lbl_pkg_split_method" runat="server" meta:resourcekey="lbl_pkg_split_method" Visible="false">
                </dx:ASPxLabel>
                <asp:TextBox ID="txt_pkg_split_method" runat="server" Width="110px" MaxLength="10" Visible="false"></asp:TextBox>

            </td>

            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_drop_cd" runat="server" meta:resourcekey="lbl_drop_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_drop_cd" runat="server" Width="320px" MaxLength="20"></asp:TextBox>
            </td>
            <td class="class_tdtxt" colspan="3" align="right">
                <dx:ASPxLabel ID="lbl_itm_srt_cd" runat="server" meta:resourcekey="lbl_itm_srt_cd">
                </dx:ASPxLabel>
                &nbsp;
                <asp:DropDownList ID="txt_itm_srt_cd" runat="server" Style="position: relative" AppendDataBoundItems="False">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="ASPxLabel_cat_cd" runat="server" meta:resourcekey="lbl_cat_cd">
                </dx:ASPxLabel>
            </td>
<%--'JAM v2.1: combine category code and description

            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_cat_cd" runat="server" Width="290px" MaxLength="50"></asp:TextBox>
            </td>
--%>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_cat_des" runat="server" Width="310px" MaxLength="100"></asp:TextBox>
            </td>

            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_dimensions" runat="server" meta:resourcekey="lbl_dimensions">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="4">&nbsp;
                <dx:ASPxLabel ID="lbl_width" runat="server" meta:resourcekey="lbl_width">
                </dx:ASPxLabel>
                <asp:TextBox ID="txt_width" runat="server" Width="40px" MaxLength="5"></asp:TextBox>

                <dx:ASPxLabel ID="lbl_depth" runat="server" meta:resourcekey="lbl_depth">
                </dx:ASPxLabel>
                <asp:TextBox ID="txt_depth" runat="server" Width="40px" MaxLength="5"></asp:TextBox>

                <dx:ASPxLabel ID="lbl_height" runat="server" meta:resourcekey="lbl_height">
                </dx:ASPxLabel>
                <asp:TextBox ID="txt_height" runat="server" Width="40px" MaxLength="5"></asp:TextBox>

                <dx:ASPxLabel ID="lbl_weight" runat="server" meta:resourcekey="lbl_weight">
                </dx:ASPxLabel>
                <asp:TextBox ID="txt_weight" runat="server" Width="40px" MaxLength="5"></asp:TextBox>


                <asp:TextBox ID="txt_cat_cd" runat="server" Width="40px" MaxLength="5" Visible="false"></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_style_cd" runat="server" meta:resourcekey="lbl_style_cd">
                </dx:ASPxLabel>
            </td>
<%--'JAM v2.1: combine style code and description
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_style_cd" runat="server" Width="290px" MaxLength="50"></asp:TextBox>
            </td>
--%>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_style_des" runat="server" Width="310px" MaxLength="100"></asp:TextBox>
            </td>
            <td class="class_tdtxt" colspan="5">&nbsp;
                <asp:TextBox ID="txt_style_cd" runat="server" Width="290px" MaxLength="50" Visible="false"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="class_tdlbl" style="width: 100px" align="right">
                <dx:ASPxLabel ID="lbl_comm_cd" runat="server" meta:resourcekey="lbl_comm_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" style="width: 300px">
                <asp:TextBox ID="txt_comm_cd" runat="server" Width="310px" MaxLength="50"></asp:TextBox>
            </td>
            <td class="class_tdlbl" style="width: 100px" align="right">
                <dx:ASPxLabel ID="lbl_related_itm_cd" runat="server" meta:resourcekey="lbl_related_itm_cd">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" style="width: 328px">
                <asp:TextBox ID="txt_related_itm_cd" runat="server" Width="320px" MaxLength="20"></asp:TextBox>
            </td>
            <td class="class_tdlbl" style="width: 150px" align="right">
                <dx:ASPxLabel ID="lbl_warrantable" runat="server" meta:resourcekey="lbl_warrantable">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" style="width: 102px">
                <asp:TextBox ID="txt_warrantable" runat="server" Width="10px" MaxLength="2"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;
                <dx:ASPxLabel ID="lbl_treatable" runat="server" meta:resourcekey="lbl_treatable">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" style="width: 20px">
                <asp:TextBox ID="txt_treatable" runat="server" Width="10px" MaxLength="2"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_pos_price" runat="server" meta:resourcekey="lbl_pos_price">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_pos_price" runat="server" Width="310px" MaxLength="20" ></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_promo" runat="server" meta:resourcekey="lbl_promo">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="4">
                <asp:TextBox ID="txt_promo_cd" runat="server" Width="40px" MaxLength="5"></asp:TextBox>
                <asp:TextBox ID="txt_promo_des" runat="server" Width="532px" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <%-- sabrina add fran_cost--%>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_landed_cost" runat="server" meta:resourcekey="lbl_landed_cost">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_landed_cost" runat="server" Width="110px" MaxLength="20" ></asp:TextBox>
            </td>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_fran_cost" runat="server" meta:resourcekey="lbl_fran_cost">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_fran_cost" runat="server" Width="110px" MaxLength="20"></asp:TextBox>
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" meta:resourcekey="lbl_cost_note">
                </dx:ASPxLabel>
            </td>
           <%-- 'Alice added for request 387 start--%>
             <td class="class_tdlbl"  align="right" colspan="1" >
                <dx:ASPxLabel ID="lbl_comm_cost" runat="server" meta:resourcekey="lbl_comm_cost">
                </dx:ASPxLabel>
            </td>
             <td class="class_tdtxt"  colspan="2" >
               <asp:TextBox ID="txt_comm_cost" runat="server" Width="110px" MaxLength="20" >
                </asp:TextBox>
            </td>
             <%-- 'Alice added for request 387 end--%>
           </tr>
        <tr>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_staff_price" runat="server" meta:resourcekey="lbl_staff_price">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_staff_price" runat="server" Width="310px" MaxLength="20"></asp:TextBox>
            </td>
        </tr>
        <%-- sabrina --%>
        <tr>
<%--            <td colspan="7" align="center">&nbsp;</td>
    --%>
            <td class="class_tdlbl" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_spiff_des" runat="server" meta:resourcekey="lbl_spiff_des" Width="90px">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="1">
                <asp:TextBox ID="txt_spiff_des" runat="server" Width="310px" MaxLength="50"></asp:TextBox>
                <asp:TextBox ID="txt_spiff_cd" runat="server" Width="40px" MaxLength="5" Visible="false"></asp:TextBox>
            </td>
            <td class="class_tdtxt" colspan="1" align="right">
                <dx:ASPxLabel ID="lbl_spiff_eff_date" runat="server" meta:resourcekey="lbl_spiff_eff_date" Width="100px">
                </dx:ASPxLabel>
            </td>
            <td class="class_tdtxt" colspan="4">
                <asp:TextBox ID="txt_spiff_eff_date" runat="server" Width="90px" MaxLength="15"></asp:TextBox>
                &nbsp;
                <dx:ASPxLabel ID="lbl_spiff_end_date" runat="server" meta:resourcekey="lbl_spiff_end_date">
                </dx:ASPxLabel>
                <asp:TextBox ID="txt_spiff_end_date" runat="server" Width="90px" MaxLength="15"></asp:TextBox>
                &nbsp;
                <dx:ASPxLabel ID="lbl_spiff_amount" runat="server" meta:resourcekey="lbl_spiff_amount">
                </dx:ASPxLabel>
                <asp:TextBox ID="txt_spiff_amount" runat="server" Width="50px" MaxLength="15"></asp:TextBox>
                &nbsp;
                <dx:ASPxLabel ID="lbl_spiff_min_retail" runat="server" meta:resourcekey="lbl_spiff_min_retail">
                </dx:ASPxLabel>
                <asp:TextBox ID="txt_spiff_min_retail" runat="server" Width="50px" MaxLength="15"></asp:TextBox>
             </td>
        </tr>
        <tr>
            <td colspan="7" align="center">
                <dx:ASPxLabel ID="lbl_resultsInfo" runat="server" Text="" Visible="false" Font-Size="Small" Font-Bold="true">
                </dx:ASPxLabel>
            </td>
        </tr>

        <tr>
            <td class="class_tdbtn" colspan="7" align="right">
                <dx:ASPxButton ID="btn_pkg_pricing" runat="server" meta:resourcekey="lbl_btn_pkg_pricing">
                </dx:ASPxButton>
                &nbsp;
                <dx:ASPxButton ID="btn_pkg_components" runat="server" meta:resourcekey="lbl_btn_pkg_components">
                </dx:ASPxButton>

                 <dx:ASPxButton ID="btn_pkg" runat="server" meta:resourcekey="lbl_btn_pkg">
                </dx:ASPxButton>

<%--                &nbsp;
                <dx:ASPxButton ID="btn_pkg_ret_price" runat="server" meta:resourcekey="lbl_btn_pkg_ret_price">
                </dx:ASPxButton>
--%>
                </td>
        </tr>




    </table>

<%--
    <dx:ASPxLabel ID="lbl_invisible1" runat="server" Text="" Visible="false">
    </dx:ASPxLabel>
    <dx:ASPxLabel ID="lbl_invisible2" runat="server" Text="" Visible="false">
    </dx:ASPxLabel>
--%>
    <br />

    <div runat="server" id="div_GridViews">
        
    <dx:ASPxGridView ID="GridViewPrices" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="PRICE_GROUP"  ClientInstanceName="GridViewPrices" Visible="true">
        <SettingsEditing Mode="Inline" />
        <Columns>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_price_group" FieldName="PRICE_GROUP" VisibleIndex="1">
           </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_price_type" FieldName="PRICE_TYPE" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_eff_date" FieldName="EFF_DATE" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_term_date" FieldName="TERM_DATE" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_price" FieldName="PRICE" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>

    <dx:ASPxGridView ID="GridViewPackageComponents" runat="server" AutoGenerateColumns="False"  Width="100%" KeyFieldName="ITM_CD"  ClientInstanceName="GridViewPackageComponents" Visible="False">

        <SettingsEditing Mode="Inline" />
        <Columns>
           <dx:GridViewDataTextColumn meta:resourcekey="gv_col_item_code" FieldName="ITM_CD" VisibleIndex="1" Width="100px">
           </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_qty" FieldName="QTY" VisibleIndex="2" Width="50px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_vsn" FieldName="VSN" VisibleIndex="3" Width="150px">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_price" FieldName="DES" VisibleIndex="4" Width="340px">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_pos_price" FieldName="POS_PRICE" VisibleIndex="5" Width="80px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_ret_price" FieldName="RET_PRICE" VisibleIndex="6" Width="80px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_pkg_promo" FieldName="PKG_PRC_PROMO" VisibleIndex="7" Width="150px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_pkg_ret" FieldName="PKG_PRC_RETAIL" VisibleIndex="8" Width="150px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="right">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>

         <dx:ASPxGridView ID="GridViewPackages" runat="server" AutoGenerateColumns="False"  Width="100%" KeyFieldName="ITM_CD"  ClientInstanceName="GridViewPackages" Visible="False">

        <SettingsEditing Mode="Inline" />
        <Columns>
           <dx:GridViewDataTextColumn meta:resourcekey="gv_col_packagecode" FieldName="PKG_ITM_CD_NEW" VisibleIndex="1" Width="100px">
           </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn meta:resourcekey="gv_col_vsn" FieldName="VSN" VisibleIndex="3" Width="150px">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_des" FieldName="DES" VisibleIndex="4" Width="340px">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_pos_price" FieldName="POS_PRICE" VisibleIndex="5" Width="80px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="right">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn meta:resourcekey="gv_col_ret_price" FieldName="RET_PRICE" VisibleIndex="6" Width="80px" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="right">
            </dx:GridViewDataTextColumn>
          
        </Columns>
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>
<%--
    <dx:ASPxGridView ID="GridViewPackageComponentsPOSPrice" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="ITM_CD"  ClientInstanceName="GridViewPackageComponentsPOSPrice" Visible="False">

        <SettingsEditing Mode="Inline" />
        <Columns>
           <dx:GridViewDataTextColumn Caption="Item Code" FieldName="ITM_CD" VisibleIndex="1">
           </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Qty" FieldName="ALT_ID_QTY" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="VSN" FieldName="VSN" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description" FieldName="DES" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="POS Price" FieldName="CURR_PRC" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="RET Price" FieldName="RET_PRC" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Package Item Price" FieldName="CURR_PRC" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>

    <dx:ASPxGridView ID="GridViewPackageComponentsRETPrice" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="ITM_CD"  ClientInstanceName="GridViewPackageComponentsRETPrice" Visible="False">

        <SettingsEditing Mode="Inline" />
        <Columns>
           <dx:GridViewDataTextColumn Caption="Item Code" FieldName="ITM_CD" VisibleIndex="1">
           </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Qty" FieldName="ALT_ID_QTY" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="VSN" FieldName="VSN" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description" FieldName="DES" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="POS Price" FieldName="CURR_PRC" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="RET Price" FieldName="RET_PRC" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Package Item Price" FieldName="CURR_PRC" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>

    <dx:ASPxGridView ID="GridViewPackageComponentsPromoPrice" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="ITM_CD"  ClientInstanceName="GridViewPackageComponentsPromoPrice" Visible="False">

        <SettingsEditing Mode="Inline" />
        <Columns>
           <dx:GridViewDataTextColumn Caption="Item Code" FieldName="ITM_CD" VisibleIndex="1">
           </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Qty" FieldName="ALT_ID_QTY" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="VSN" FieldName="VSN" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description" FieldName="DES" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="POS Price" FieldName="CURR_PRC" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="RET Price" FieldName="RET_PRC" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Package Item Price" FieldName="CURR_PRC" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior AllowSort="False" />
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>
--%>

    </div>

    <br />
    <br />
    
    <div runat="server" id="div_version">
    <dx:ASPxLabel ID="lbl_versionInfo" runat="server" Text="" Visible="true" Font-Size="X-Small" Font-Bold="false">
    </dx:ASPxLabel>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
