﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MultiWarranties.ascx.vb" Inherits="Usercontrols_MultiWarranties" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxPopup" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dxNav" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxEditor" %>
<%@ Register Src="~/Usercontrols/LinkWarranties.ascx" TagPrefix="ucLinkWarr" TagName="LinkWarranties" %>

<script type="text/javascript">
    function SelectionChanged(s, e) {
        var ctrlName = s.name;
        var warrCd = s.GetValue();
        var warrText = s.GetSelectedItem().text;

        if (ctrlName.indexOf("rdoAddWarr") >= 0) {
            ctrlName = ctrlName.replace("rdoAddWarr", "rdoPrevWarr")
        }
        else {
            ctrlName = ctrlName.replace("rdoPrevWarr", "rdoAddWarr")
        }

        $.ajax({
            type: "POST",
            url: "Services/HBCG_Services.asmx/AddWarranty",
            data: JSON.stringify({ ctrlName: ctrlName, warrItmCd: warrCd, warrItmText: warrText }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            error: function (error) {
                //alert(error.responseText);
            },
            success: function (data) {
                if (null != warrCd && null != data.d && data.d.length > 0 && ctrlName.indexOf("rdoPrevWarr") >= 0) {
                    popupLinkWarranties.PerformCallback(ctrlName + ":" + warrCd + ":" + warrText);
                    popupLinkWarranties.Show();
                }
            }
        });

        var ctrl = ASPxClientControl.GetControlCollection().GetByName(ctrlName);
        if (null != ctrl) {
            ctrl.SetSelectedItem(null);
        }
    }
</script>

<style>
    .dxnb-content {
        border: #8ba0bc 1px solid;
        border-top-style: none;
    }

    .dxnb-tmpl {
        padding-bottom: 6px;
        padding-left: 7px;
        padding-right: 7px;
        padding-top: 6px;
    }
</style>

<ucLinkWarr:LinkWarranties runat="server" ID="ucLinkWarranties" ClientIDMode="Static" />
<dxPopup:ASPxPopupControl ID="popupWarranties" runat="server" ClientInstanceName="popupWarranties" Modal="true"
    ShowCloseButton="false" CloseAction="None" AllowDragging="true" HeaderText="<%$ Resources:LibResources, Label739 %>" HeaderStyle-Font-Bold="false"
    HeaderStyle-HorizontalAlign="Left" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    ShowPageScrollbarWhenModal="true" Width="679px" MaxHeight="800px" ShowLoadingPanel="False">
    <ContentCollection>
        <dxPopup:PopupControlContentControl ID="pccWarranties" runat="server" ClientIDMode="Static" Width="100%">
            <asp:HiddenField ID="hidDelDocNum" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hidFocusItem" runat="server" ClientIDMode="Static" />

            <div style="display: table; margin: auto; width: 95%;">
                <div style="display: table-row;">
                    <div style="display: table-cell;">
                        <div style="width: 100%; height: 300px; overflow: auto;">
                            <dxNav:ASPxNavBar ID="dxNavBar" runat="server" ClientIDMode="Static" GroupSpacing="5px"
                                AutoCollapse="true" AutoPostBack="True" EnableAnimation="True" ShowLoadingPanel="False"
                                ShowLoadingPanelImage="False" ItemStyle-HorizontalAlign="Left" Width="100%"
                                GroupHeaderStyleCollapsed-HorizontalAlign="Left" GroupContentStyle-CssClass="NavBarGroup">
                            </dxNav:ASPxNavBar>
                        </div>
                    </div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell; text-align: right; padding-top: 10px; padding-bottom: 10px;">
                        <dxEditor:ASPxButton ID="btnAddWarr" runat="server" ClientIDMode="Static" Text="<%$ Resources:LibResources, Label741 %>"></dxEditor:ASPxButton>
                        <dxEditor:ASPxButton ID="btnNoThanks" runat="server" ClientIDMode="Static" Text="<%$ Resources:LibResources, Label55 %>"></dxEditor:ASPxButton>
                    </div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell">
                        <dxEditor:ASPxLabel ID="lblError" runat="server" ClientInstanceName="lblErrorMultiWarr" ForeColor="Red"></dxEditor:ASPxLabel>
                    </div>
                </div>
            </div>
        </dxPopup:PopupControlContentControl>
    </ContentCollection>
</dxPopup:ASPxPopupControl>
