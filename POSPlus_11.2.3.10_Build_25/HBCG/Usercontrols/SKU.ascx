﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SKU.ascx.vb" Inherits="Usercontrols_SKU" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<asp:Panel ID="pnlDisc" runat="server" ClientIDMode="Static" DefaultButton="btnAddSKU">
    <div style="margin: auto; display: table;">
        <div style="display: table-row;">
            <div style="display: table-cell;">
                <div style="display: table; margin: auto; padding-bottom: 10px;">
                    <div style="display: table-row;">
                        <div style="display: table-cell; text-align: center; vertical-align: middle; padding-bottom: 10px;">
                            <dx:ASPxLabel ID="lblError" runat="server" ClientInstanceName="lblError" ForeColor="Red">
                            </dx:ASPxLabel>
                            <dx:ASPxValidationSummary ID="valSumDisc" runat="server" ValidationGroup="valgrpDisc"
                                ClientInstanceName="valSumDisc" RenderMode="Table" ShowErrorAsLink="False">
                            </dx:ASPxValidationSummary>
                            <dx:ASPxLabel ID="lblMessage" runat="server" ClientInstanceName="lblMessage" ForeColor="Green">
                            </dx:ASPxLabel>
                        </div>
                    </div>
                    <div style="display: table-row;">
                        <div style="display: table-cell;">
                            <div style="margin: auto; display: table; width: 100%;">
                                <div style="display: table-row;">
                                    <div style="display: table-cell; text-align: left; vertical-align: middle;">
                                        <label>Enter SKU:</label>
                                    </div>
                                </div>
                                <div style="display: table-row;">
                                    <div style="display: table-cell; width: 320px; text-align: left; vertical-align: middle;">
                                        <dx:ASPxTextBox ID="txtSKU" runat="server" ClientInstanceName="txtSKU" Native="true" Width="150px" AutoPostBack ="true"  >
                                            <ValidationSettings CausesValidation="true" SetFocusOnError="True" ValidationGroup="valgrpDisc">
                                                <%--<RequiredField IsRequired="true" ErrorText="Please Enter a SKU." />--%>
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                        <%--<asp:Button ID="btnSearch" runat="server" ToolTip="Search" Text ="Search" Height="15px" Width="15px"/>--%>
                                        <%--<asp:I ID="btnSearch" runat="server" UseSubmitBehavior="false" CausesValidation="true" Text="Search">
                                        </asp:I>--%>
                                    </div>
                                </div>
                                <br />
                                <div style="display: table-row;">
                                    <div style="display: table-cell; text-align: center; vertical-align: middle; padding-bottom: 10px;">
                                        <dx:ASPxGridView ID="grdSKU" runat="server" Width="100%" Styles-Header-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataColumn FieldName="VE_CD" VisibleIndex="1" Visible="true" Caption="Vendor" />
                                                <dx:GridViewDataColumn FieldName="VSN" VisibleIndex="2" Visible="true" Caption="VSN" />
                                                <dx:GridViewDataColumn FieldName="DES" VisibleIndex="3" Visible="true" Caption="Description" />
                                                <dx:GridViewDataColumn FieldName="ITM_TP_CD" VisibleIndex="4" Visible="true" Caption="Item Type" />
                                                <dx:GridViewDataColumn FieldName="RET_PRC" VisibleIndex="5" Visible="true" Caption="Ret Price" />

                                                <%--<dx:GridViewDataColumn FieldName="SORTCODE" VisibleIndex="6" Visible="true" Caption="Sort Code" />--%>
                                                <dx:GridViewDataColumn Caption="Sort Codes" VisibleIndex="6">
                                                    <DataItemTemplate>
                                                        <label><%# StringUtils.EliminateCommas(If ( IsDBNull(Eval("SORTCODE") ),string.Empty ,Eval("SORTCODE") ) )%></label>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                            </Columns>

                                        </dx:ASPxGridView>
                                    </div>
                                </div>
                                <div style="display: table-row;">
                                    <div style="display: table-cell; text-align: center; vertical-align: middle; padding-bottom: 10px;">
                                        <dx:ASPxButton ID="btnAddSKU" runat="server" UseSubmitBehavior="false" CausesValidation="true" Text="Add SKU">
                                        </dx:ASPxButton>
                                        &nbsp;
                                         <dx:ASPxButton ID="btnClose" runat="server" UseSubmitBehavior="false" CausesValidation="true" Text="Cancel">
                                        </dx:ASPxButton>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <asp:HiddenField ID ="hidIsSelfPostBack" runat ="server" />
</asp:Panel>
