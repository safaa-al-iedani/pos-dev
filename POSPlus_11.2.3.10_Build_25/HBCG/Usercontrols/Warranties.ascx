﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Warranties.ascx.vb" Inherits="Usercontrols_Warranties" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<dx:ASPxPopupControl ID="popupWarranties" runat="server" ClientInstanceName="popupWarranties" Modal="true" CloseAction="CloseButton" AllowDragging="true"
    HeaderText="<%$ Resources:LibResources, Label739 %>" HeaderStyle-Font-Bold="false" HeaderStyle-HorizontalAlign="Left" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    ShowPageScrollbarWhenModal="true" Width="679px">
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentWarranties" runat="server">
            <asp:HiddenField ID="hidDelDocNum" runat="server" />
            <asp:HiddenField ID="hidSkus" runat="server" />
            <asp:HiddenField ID="hidRowNo" runat="server" />
            <asp:HiddenField ID="hidWarrSku" runat="server" />
            <asp:HiddenField ID="hidWarrRowNo" runat="server" />
            <div style="display: table;">
                <div style="display: table-row">
                    <div style="display: table-cell">
                        <asp:Label ID="lblVSN" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell">
                        <asp:Label ID="lblCurrWarr" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblExtWarr" runat="server" Text="<%$ Resources:LibResources, Label740 %>"></asp:Label>
                    </div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell">
                        <asp:RadioButtonList ID="rdoAddWarr" runat="server" AutoPostBack="true">
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell; padding-top: 10px; padding-bottom: 10px;">
                        <asp:Label ID="lblExtWarr2" runat="server" Text="Or you may choose to warranty this item with one of these previously selected extended warranties:"></asp:Label>
                    </div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell">
                        <asp:RadioButtonList ID="rdoPrevWarrList" runat="server" AutoPostBack="true">
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell; text-align: right; padding-top: 10px; padding-bottom: 10px;">
                        <asp:Button ID="btnAddWarr" runat="server" Text="Add Selected Item" />
                        <asp:Button ID="btnNoThanks" runat="server" Text="<%$ Resources:LibResources, Label350 %>" />
                    </div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell;">
                        <dx:ASPxLabel ID="lblError" runat="server" Width="100%">
                        </dx:ASPxLabel>
                    </div>
                </div>
            </div>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
