﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RelatedSKU.ascx.vb" Inherits="Usercontrols_RelatedSKU" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxg" %>

<dxp:ASPxPopupControl ID="popupRelatedSKUs" runat="server" ClientInstanceName="popupRelatedSKUs" Modal="true"
    ShowCloseButton="false" CloseAction="None" AllowDragging="true" HeaderText="<%$ Resources:LibResources, Label763 %>" HeaderStyle-Font-Bold="false"
    HeaderStyle-HorizontalAlign="Left" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    ShowPageScrollbarWhenModal="true" Width="679px" MaxHeight="800px" ShowLoadingPanel="False">
    <ContentCollection>
        <dxp:PopupControlContentControl runat="server" SupportsDisabledAttribute="True" Width="100%">
            <asp:HiddenField ID="hidSku" runat="server" />
            <div style="display: table; margin: auto; width: 95%;">
                <div style="display: table-row;">
                    <div style="display: table-cell; text-align: left; padding: 10px;">
                        <dxe:ASPxLabel ID="lblDesc" runat="server"
                            Text="<%$ Resources:LibResources, Label764 %>">
                        </dxe:ASPxLabel>
                    </div>
                </div>
                <div style="display: table-row;">
                    <div style="display: table-cell; vertical-align: middle;">
                        <dxg:ASPxGridView ID="gvRelatedSKUs" runat="server" ClientInstanceName="gvRelatedSKUs"
                            AutoGenerateColumns="false" KeyFieldName="CompositeKey" ViewStateMode="Disabled" Border-BorderStyle="None"
                            Settings-GridLines="None" Settings-ShowColumnHeaders="true" Settings-ShowFooter="False"
                            SettingsText-EmptyDataRow="No related SKUS were found for this order." SettingsPager-Visible="False"
                            SettingsLoadingPanel-Mode="Disabled" SettingsLoadingPanel-ShowImage="False" Width="98%">
                            <SettingsBehavior AllowSort="false" AllowFocusedRow="true" />
                            <Styles AlternatingRow-Enabled="True" Header-Font-Bold="true" Header-HorizontalAlign="Center"
                                Header-Border-BorderStyle="None">
                            </Styles>
                            <Settings ShowVerticalScrollBar="true" VerticalScrollBarMode="Auto" />
                            <Columns>
                                <dxg:GridViewCommandColumn ShowSelectCheckbox="True" Caption="<%$ Resources:LibResources, Label528 %>" VisibleIndex="0" />
                                <dxg:GridViewDataColumn FieldName="RELATED_ITM_CD" Caption="<%$ Resources:LibResources, Label765 %>"  VisibleIndex="1" />
                                <dxg:GridViewDataColumn FieldName="ITM_CD" Caption="<%$ Resources:LibResources, Label766 %>" VisibleIndex="2">
                                    <DataItemTemplate>
                                        <label><%# Eval("ITM_CD")%></label>
                                    </DataItemTemplate>
                                </dxg:GridViewDataColumn>
                                <dxg:GridViewDataColumn FieldName="VSN" Caption="<%$ Resources:LibResources, Label653 %>" VisibleIndex="3" />
                                <dxg:GridViewDataColumn FieldName="DES" Caption="Description" VisibleIndex="4" />
                                <dxg:GridViewDataColumn Caption="<%$ Resources:LibResources, Label427 %>" CellStyle-HorizontalAlign="Right" VisibleIndex="5">
                                    <DataItemTemplate>
                                        <label><%# Eval("RET_PRC", "{0:c}")%></label>
                                    </DataItemTemplate>
                                </dxg:GridViewDataColumn>
                                <dxg:GridViewDataColumn FieldName="CompositeKey" VisibleIndex="6" UnboundType="String" Visible="false">
                                </dxg:GridViewDataColumn>
                            </Columns>
                        </dxg:ASPxGridView>
                    </div>
                </div>
                <div style="display: table-row;">
                    <div style="display: table-cell; text-align: right; padding-top: 20px; padding-bottom: 10px;">
                        <dxe:ASPxButton ID="btnAddSKU" runat="server" ClientIDMode="Static" UseSubmitBehavior="true"
                            Text="<%$ Resources:LibResources, Label767 %>">
                        </dxe:ASPxButton>
                        <dxe:ASPxButton ID="btnNoSKU" runat="server" Text="<%$ Resources:LibResources, Label55 %>" UseSubmitBehavior="False"
                            CausesValidation="false">
                        </dxe:ASPxButton>
                    </div>
                </div>
                <div style="display: table-row;">
                    <div style="display: table-cell; text-align: left;">
                        <dxe:ASPxLabel ID="lblError" runat="server" ClientInstanceName="RelSKUErr" ForeColor="Red" Text=" "></dxe:ASPxLabel>
                    </div>
                </div>
            </div>
        </dxp:PopupControlContentControl>
    </ContentCollection>
</dxp:ASPxPopupControl>
