﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Discount.ascx.vb" Inherits="Usercontrols_Discount" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Src="~/Usercontrols/ManagerOverride.ascx" TagPrefix="ucMgr" TagName="ManagerOverride" %>
<asp:HiddenField ID="hidReqSource" runat="server" />
<asp:HiddenField ID="hidDelDocNum" runat="server" />
<asp:HiddenField ID="prcOverrideSetting" runat="server" />
<asp:HiddenField ID="hidItmSeq" runat="server" ClientIDMode="Static" />
<asp:Panel ID="pnlDisc" runat="server" ClientIDMode="Static" DefaultButton="btnAdd">

    <div style="margin: auto; display: table;">
        <div style="display: table-row;">
            <div id="divErr" style="display: table-cell; text-align: center; vertical-align: middle; padding-bottom: 10px;">
                <dx:ASPxLabel ID="lblError" runat="server" ClientInstanceName="lblError" ForeColor="Red" EncodeHtml="false">
                </dx:ASPxLabel>
                <dx:ASPxValidationSummary ID="valSumDisc" runat="server" ValidationGroup="valgrpDisc"
                    ClientInstanceName="valSumDisc" RenderMode="Table" ShowErrorAsLink="False">
                </dx:ASPxValidationSummary>
                <dx:ASPxLabel ID="lblMessage" runat="server" ClientInstanceName="lblMessage" ForeColor="Green" EncodeHtml="false">
                </dx:ASPxLabel>
            </div>
        </div>
        <div style="display: table-row;">
            <div style="display: table-cell;">
                <div style="display: table; margin: auto; padding-bottom: 10px;">
                    <div style="display: table-row;">
                        <div style="display: table-cell;">
                            <div style="margin: auto; display: table; width: 100%;">
                                <div style="display: table-row;">
                                    <div style="display: table-cell; text-align: left; vertical-align: middle;">
                                        <label><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label529 %>" />:</label>
                                    </div>
                                    <div style="display: table-cell; text-align: left; vertical-align: middle;">
                                        <label style="padding-left: 10px;"><asp:Literal runat="server" Text="<%$ Resources:LibResources, Label168 %>" />:</label>
                                    </div>
                                </div>
                                <div style="display: table-row;">
                                    <div style="display: table-cell; width: 320px; text-align: left; vertical-align: middle;">
                                        <dx:ASPxComboBox ID="cbDiscounts" runat="server" ClientInstanceName="cbDiscounts"
                                            AutoPostBack="True" Width="300px" IncrementalFilteringMode="StartsWith">
                                            <ValidationSettings CausesValidation="false" SetFocusOnError="True"
                                                EnableCustomValidation="true" ValidationGroup="valgrpDisc">
                                                <RequiredField IsRequired="true" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="function(s,e) {
                                                    if (e.value == 0){
                                                        e.isValid = false;}
                                                }" />
                                        </dx:ASPxComboBox>
                                    </div>
                                    <div style="display: table-cell; text-align: left;">
                                        <dx:ASPxRadioButtonList ID="rdoDiscType" runat="server" ClientEnabled="false" AutoPostBack="true"
                                            ClientInstanceName="rdoDiscType" RepeatColumns="3" RepeatLayout="Table" Border-BorderStyle="None">
                                            <Items>
                                                <dx:ListEditItem Value="0" Text="<%$ Resources:LibResources, Label227 %>" Selected="true" />
                                                <dx:ListEditItem Value="1" Text="<%$ Resources:LibResources, Label267 %>" />
                                            </Items>
                                        </dx:ASPxRadioButtonList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="display: table-row;">
                        <div style="display: table-cell;">
                            <div style="margin: auto; display: table; width: 100%;">
                                <div style="display: table-row;">
                                    <div style="display: table-cell; text-align: left; vertical-align: middle;">
                                        <dx:ASPxLabel ID="lblLineItem" runat="server" AssociatedControlID="lstLineItem"
                                            ClientInstanceName="lblLineItem" Text="Select item(s) to discount:" ClientVisible="false">
                                        </dx:ASPxLabel>
                                    </div>
                                </div>
                                <div style="display: table-row;">
                                    <div style="display: table-cell; text-align: left; width: 320px;">
                                        <dx:ASPxListBox ID="lstLineItem" runat="server" ClientInstanceName="lstLineItem"
                                            Width="300px" SelectionMode="CheckColumn" ClientVisible="false">
                                            <ValidationSettings CausesValidation="true" SetFocusOnError="true"
                                                EnableCustomValidation="True" ValidationGroup="valgrpDisc">
                                                <RequiredField IsRequired="true" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="function(s, e) {
                                                    if(rdoDiscType.GetSelectedItem().value == 1){
                                                        var arr = s.GetSelectedItems();
                                                        e.isValid = s.GetSelectedItems().length != 0; }
                                                    else {
                                                        e.isValid = true; }
                                                }" />
                                        </dx:ASPxListBox>
                                    </div>
                                    <div style="display: table-cell; text-align: right; vertical-align: top; padding-right: 10px;">
                                        <dx:ASPxButton ID="btnAdd" runat="server" ClientInstanceName="btnAdd" UseSubmitBehavior="true"
                                            CausesValidation="true" ValidationGroup="valgrpDisc" Text="<%$ Resources:LibResources, Label11 %>">
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnReset" runat="server" ClientInstanceName="btnReset" Text="<%$ Resources:LibResources, Label474 %>"
                                            UseSubmitBehavior="False">
                                            <ClientSideEvents Click="function(s, e) {
                                                ASPxClientEdit.ClearEditorsInContainerById('divErr'); }" />
                                        </dx:ASPxButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="display: table-row;">
                        <div style="display: table-cell; padding-top: 2em;">
                            <dx:ASPxGridView ID="grdDiscItems" runat="server" KeyFieldName="ROW_IDX" Width="100%" ClientIDMode="Static"
                                Styles-Header-HorizontalAlign="Center" SettingsBehavior-AllowSort="false">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataColumn FieldName="ROW_IDX" VisibleIndex="1" Visible="false" />
                                    <dx:GridViewDataColumn FieldName="LNSEQ_NUM" VisibleIndex="2" Visible="false" />
                                    <dx:GridViewDataColumn FieldName="DISC_CD" VisibleIndex="3" Visible="false" />
                                    <dx:GridViewDataColumn FieldName="DISC_AMT" VisibleIndex="4" Visible="false" />
                                    <dx:GridViewDataColumn Caption="<%$ Resources:LibResources, Label752 %>" VisibleIndex="5">
                                        <DataItemTemplate>
                                            <label><%# String.Format("({0}) {1}", Eval("ITM_CD"), Eval("ITM_DES"))%></label>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="QTY" Caption="<%$ Resources:LibResources, Label753 %>" CellStyle-HorizontalAlign="Right" VisibleIndex="6" />
                                    <dx:GridViewDataColumn Caption="<%$ Resources:LibResources, Label754 %>" VisibleIndex="7">
                                        <DataItemTemplate>
                                            <div style="display: table; width: 100%;">
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell; text-align: left;">
                                                        <label><%# String.Format("{0}", Eval("DISC_DES"))%></label>
                                                    </div>
                                                </div>
                                                <div style="display: table-row;">
                                                    <div style="display: table-cell; text-align: right;">
                                                        <label style="font-weight: bold;">
                                                            <%# If(CDbl(Eval("DISC_PCT")) > 0,
                                                            String.Format("({0}%) {1}", Eval("DISC_PCT"), Eval("DISC_AMT", "{0:c}")),
                                                            String.Format("{0}", Eval("DISC_AMT", "{0:c}")))%></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="<%$ Resources:LibResources, Label427 %>" CellStyle-HorizontalAlign="Right" VisibleIndex="8">
                                        <DataItemTemplate>
                                            <label><%# Eval("ITM_RETAIL", "{0:c}")%></label>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="DISC_LEVEL" VisibleIndex="9" Visible="false" />
                                    <dx:GridViewDataColumn FieldName="SEQ_NUM" VisibleIndex="10" Visible="false" />                                
							</Columns>
                            </dx:ASPxGridView>
                        </div>
                    </div>
                    <div style="display: table-row;">
                        <div style="display: table-cell; text-align: left; vertical-align: middle; padding-top: 10px;">
                            <dx:ASPxButton ID="btnRemove" runat="server" UseSubmitBehavior="false" CausesValidation="false" Text="<%$ Resources:LibResources, Label467 %>">
                            </dx:ASPxButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
<dxpc:ASPxPopupControl ID="popupMgrOverride" runat="server" ClientInstanceName="popupMgrOverride"
    HeaderText="Manager Approval" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"
    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Modal="true" CloseAction="None"
    AllowDragging="true" ShowCloseButton="false" ShowPageScrollbarWhenModal="true" Width="400px">
    <ContentCollection>
        <dxpc:PopupControlContentControl runat="server">
            <ucMgr:ManagerOverride runat="server" ID="ucMgrOverride" />
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>
