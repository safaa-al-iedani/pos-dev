﻿Imports DevExpress.Web.ASPxEditors
Imports System.Collections.Generic

Partial Class Usercontrols_LinkWarranties
    Inherits System.Web.UI.UserControl

    Public Event LinkWarrantyToSKUs As EventHandler
    Public Delegate Sub EventHandler(sender As Object, e As EventArgs)

    Protected Sub btnLinkSKUs_Click(sender As Object, e As EventArgs) Handles btnLinkSKUs.Click
        Dim selWarr As String = hidWarrCd.Value
        Dim selRows As String = hidSelectedRowIds.Value
        Dim triggeredSKU As String = hidTriggeredSKU.Value

        Dim selectedRows = chkItems.SelectedValues()

        If Not String.IsNullOrEmpty(selRows) AndAlso Not IsNothing(Session("WARRINFO")) Then
            Dim rowIds As String() = selRows.Split(",")
            Dim dtWarr As DataTable = DirectCast(Session("WARRINFO"), DataTable)
            Dim drTriggeredRow() As DataRow = dtWarr.Select("ROW_ID = '" + triggeredSKU + "' ")

            Dim drFilter() As DataRow
            For Each rowId As String In rowIds
                drFilter = dtWarr.Select("ROW_ID = '" + rowId + "' ")

                drFilter(0)("WARR_ITM_CD") = drTriggeredRow(0)("WARR_ITM_CD")
                drFilter(0)("WARR_ITM_TEXT") = drTriggeredRow(0)("WARR_ITM_TEXT")
                drFilter(0)("WARR_ROW_ID") = drTriggeredRow(0)("WARR_ROW_ID")
                drFilter(0)("IS_PREV_SEL") = True
            Next
        End If

        RaiseEvent LinkWarrantyToSKUs(sender, e)
    End Sub

    Protected Sub popupLinkWarranties_WindowCallback(source As Object, e As DevExpress.Web.ASPxPopupControl.PopupWindowCallbackArgs) Handles popupLinkWarranties.WindowCallback
        chkItems.Items.Clear()

        If Not String.IsNullOrEmpty(e.Parameter) AndAlso _
            e.Parameter.IndexOf("rdoPrevWarr") = 0 AndAlso Not IsNothing(Session("LINKWARR2SKUS")) Then
            Dim warDtl As String() = e.Parameter.Split(":")
            If warDtl.Length > 2 Then
                Dim rowIds As String() = warDtl(0).Split("_")
                hidTriggeredSKU.Value = rowIds(2)
                hidWarrCd.Value = warDtl(1)
                lblCommonWarranty.Text = warDtl(2)
            End If

            chkItems.DataSource = Session("LINKWARR2SKUS")
            chkItems.DataBind()
            'Else
            'popupLinkWarranties.ShowOnPageLoad = False
        End If
    End Sub
End Class
