﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Partial Class Usercontrols_ZoneDisplay
    Inherits System.Web.UI.UserControl
    Public Event ZoneCodeSelected As EventHandler
    Public Delegate Sub EventHandler(sender As Object, e As EventArgs, zoneCd As String, store As String, deliveryCharge As String, setupChrge As String)
    Private theTMBiz As TransportationBiz = New TransportationBiz()

    ''' <summary>
    ''' Populates the zone information
    ''' </summary>
    ''' <param name="pinCode">pincode</param>
    ''' <remarks></remarks>
    Public Sub PopulateZoneInformation(pinCode As String)
        hidPincode.Value = pinCode
        Session("ZoneData") = Nothing

        gridZone.FilterExpression = String.Empty
        gridZone.PageIndex = 0
        gridZone.SettingsPager.PageSize = 10

        gridZone.DataBind()
    End Sub

    ''' <summary>
    ''' Binds the zones to grid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function BindZones() As DataTable
        Dim dt As New DataTable

        If (Not IsNothing(Session("ZoneData"))) Then
            dt = Session("ZoneData")
        Else
            dt = UpdateZonesDisplay(hidPincode.Value)

            If (Not IsNothing(dt) AndAlso dt.Rows.Count > 0) Then
                Session("ZoneData") = dt
            End If
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Event to bind the data to grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridZone_DataBinding(sender As Object, e As EventArgs) Handles gridZone.DataBinding
        gridZone.DataSource = BindZones()
    End Sub

    ''' <summary>
    ''' Execcutes when the user clicks on an editable component(in this case the 'select' button) in the tax grid.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridZone_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles gridZone.RowCommand
        If e.CommandArgs.CommandName = "Select" Then

            Dim index As Integer = Convert.ToInt32(e.CommandArgs.CommandArgument)

            Dim zoneCd As String = System.Web.HttpUtility.HtmlEncode(gridZone.GetRowValues(index, "ZONE_CD").ToString())
            Dim store As String = System.Web.HttpUtility.HtmlDecode(gridZone.GetRowValues(index, "DELIVERY_STORE_CD").ToString())
            Dim deliveryCharge As String = System.Web.HttpUtility.HtmlEncode(gridZone.GetRowValues(index, "DEFAULT_DEL_CHG").ToString())
            Dim setupChrge As String = System.Web.HttpUtility.HtmlEncode(gridZone.GetRowValues(index, "DEFAULT_SETUP_CHG").ToString())
            Session.Remove("ZoneData")
            RaiseEvent ZoneCodeSelected(sender, e, zoneCd, store, deliveryCharge, setupChrge)

        End If
    End Sub

    ''' <summary>
    ''' Clears the filter on grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkClearFilter_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim lnk As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim container As GridViewFilterCellTemplateContainer = TryCast(lnk.NamingContainer, GridViewFilterCellTemplateContainer)
        lnk.Visible = Not String.IsNullOrEmpty(container.Grid.FilterExpression)
    End Sub

    ''' <summary>
    ''' Retrieves the ZONES in the system, if no zones are found for the postal code passed in,
    ''' then ALL zones may be retrieved if ConfigurationManager.AppSettings("open_zones") is Y.
    ''' </summary>
    ''' <param name="postalCd">the postal code to retrieve ZONES for</param>
    ''' <returns>number of rows placed in the GridView2 DataTable</returns>
    Private Function UpdateZonesDisplay(ByVal postalCd As String) As DataTable
        Dim dtZones As New DataTable
        Try
            Dim dsZones As DataSet = theTMBiz.GetZoneCodesForZip(postalCd)
            ' if NO zones are found, it may have to fetch them all
            If (dsZones.Tables(0).Rows.Count = 0 AndAlso
                ConfigurationManager.AppSettings("open_zones").ToString = "Y") Then
                dsZones = theTMBiz.GetZoneCodes()
                If Not IsNothing(dsZones) AndAlso dsZones.Tables.Count > 0 Then
                    dtZones = dsZones.Tables(0)
                End If
            Else
                dtZones = dsZones.Tables(0)
            End If
        Catch ex As Exception
            Throw
        End Try        
        Return dtZones
    End Function
End Class
