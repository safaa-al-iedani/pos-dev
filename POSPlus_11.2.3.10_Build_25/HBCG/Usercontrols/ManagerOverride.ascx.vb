﻿
Partial Class Usercontrols_ManagerOverride
    Inherits System.Web.UI.UserControl

    Public Event wasApproved As EventHandler
    Public Event wasCanceled As EventHandler
    Public Delegate Sub EventHandler(sender As Object, e As EventArgs)
    Private theSystemBiz As SystemBiz = New SystemBiz()

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lblErrMgrOverride.Text = String.Empty
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Not String.IsNullOrEmpty(txtMgrPwd.Text) Then
            spnErrMgrPwd.Visible = False
            Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()  'MM-11224
            If (AppConstants.ManagerOverride.BY_ORDER = prcOverride) Then
                Session("hidUpdLnNos") = Nothing
            End If
            If CanApprove(txtMgrPwd.Text) Then
                lblErrMgrOverride.Text = String.Empty
                RaiseEvent wasApproved(sender, e)
            Else
                lblErrMgrOverride.Text = Resources.POSMessages.MSG0017
            End If
        Else
            spnErrMgrPwd.Visible = True
            lblErrMgrOverride.Text = "Please enter a password."
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        spnErrMgrPwd.Visible = False
        lblErrMgrOverride.Text = String.Empty
        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()  'MM-11224
        If (AppConstants.ManagerOverride.BY_ORDER = prcOverride) Then
            Session("hidUpdLnNos") = Nothing
        End If
        RaiseEvent wasCanceled(sender, e)
    End Sub

    ''' <summary>
    ''' Checks for various conditions to determine if the user can approve the change 
    ''' or not. It checks for conditions such as the user being setup as an 'approver', 
    ''' security priveleges, etc.
    ''' </summary>
    ''' <param name="pwd">Entered Manager's password</param>
    ''' <returns>flag indicating if the user can approve the change requested.</returns>
    ''' <remarks></remarks>
    Private Function CanApprove(pwd As String) As Boolean

        Dim rtnVal As Boolean = False
        Dim effDt As Date = Date.Today

        'check if there was a valid date set
        If (hidEffDt.Value.ToString.isNotEmpty AndAlso
            IsDate(hidEffDt.Value.ToString) AndAlso
            Not (hidEffDt.Value = DateTime.MinValue)) Then
            effDt = hidEffDt.Value.ToString()
        End If

        'first check if the user is an approver or not        
        Dim empCd As String = theSystemBiz.GetApprover(pwd, effDt)
        If (empCd.isNotEmpty) Then

            'set the value back on the hidden field for the caller to access
            rtnVal = True
            hidAppEmpCd.Value = empCd

            If hidSecCd.Value.ToString.isNotEmpty Then
                'If a sec code is being passed, then see if the approver has that security                
                rtnVal = SecurityUtils.hasSecurity(hidSecCd.Value.ToString, empCd)
            Else
                'user is an approver and no security code was passed-in, so return true
                rtnVal = True
            End If
        End If

        Return rtnVal
    End Function
    Public Sub SetDisplayMessage(ByVal msg As String)
        lblMessage.Text = msg
        lblMessage.Visible = True
    End Sub
    Public Sub HideDisplayMessage()
        lblMessage.Text = String.Empty
        lblMessage.Visible = False
    End Sub

    Public Property ApprovalFor As String
        Get
            Return hidSource.Value
        End Get
        Set(value As String)
            hidSource.Value = value
        End Set
    End Property
End Class
