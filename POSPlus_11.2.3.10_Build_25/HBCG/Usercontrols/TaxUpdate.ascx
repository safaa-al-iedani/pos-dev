﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="TaxUpdate.ascx.vb" Inherits="Usercontrols_TaxUpdate" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<div style="width: 100%; text-align: left; vertical-align: top;">
    <asp:HiddenField ID="hidZip" runat="server" />
    <asp:HiddenField ID="hidDelDocNo" runat="server" />
    <asp:HiddenField ID="hidTaxCd" runat="server" />
    <asp:HiddenField ID="hidTaxDt" runat="server" />
    <asp:HiddenField ID="hidTaxDesc" runat="server" />
    <asp:HiddenField ID="hidTaxRate" runat="server" />
    <asp:HiddenField ID="hidTaxMethod1" runat="server" />
    <asp:HiddenField ID="hidIsInvokeManually" runat="server" />

    <dx:ASPxGridView ID="gridTax" runat="server" ClientInstanceName="gridTax"
        KeyFieldName="TAX_CD" SettingsCookies-StorePaging="true" Width="98%"
        SettingsPager-AlwaysShowPager="true" SettingsPager-PageSizeItemSettings-Visible="true" ViewStateMode="Disabled"
        SettingsLoadingPanel-Mode="Disabled" SettingsLoadingPanel-ShowImage="False">
        <SettingsBehavior AllowSort="true" AllowFocusedRow="true" AllowSelectSingleRowOnly="true" ProcessFocusedRowChangedOnServer="true" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowHeaderFilterButton="false" UseFixedTableLayout="true" />
        <Styles Header-HorizontalAlign="Center" AlternatingRow-Enabled="True" FilterCell-HorizontalAlign="Center">
        </Styles>
        <Columns>
            <dx:GridViewDataColumn FieldName="TAX_CD" Caption="<%$ Resources:LibResources, Label587 %>" SortOrder="Ascending"
                Settings-AutoFilterCondition="BeginsWith" Width="17%" VisibleIndex="1" />
            <dx:GridViewDataColumn FieldName="DES" Caption="<%$ Resources:LibResources, Label158 %>"
                Settings-AutoFilterCondition="Contains" VisibleIndex="2" />
            <dx:GridViewDataColumn FieldName="SUMOFPCT" Caption="<%$ Resources:LibResources, Label591 %>"
                Settings-AllowAutoFilter="False" Width="17%" VisibleIndex="3" />
            <dx:GridViewDataColumn FieldName="TAX_METHOD" Caption="Tax Method" Visible="false" VisibleIndex="4" />
            <dx:GridViewDataTextColumn Caption="<%$ Resources:LibResources, Label528 %>" Settings-AllowSort="False"
                HeaderStyle-Cursor="default" Width="15%" VisibleIndex="5">
                <CellStyle HorizontalAlign="Center"></CellStyle>
                <DataItemTemplate>
                    <dx:ASPxButton ID="btnSelect" runat="server" Text="<%$ Resources:LibResources, Label528 %>" CommandName="Select" CommandArgument="<%# Container.ItemIndex %>"></dx:ASPxButton>
                </DataItemTemplate>
                <FilterTemplate>
                    <dx:ASPxHyperLink ID="lnkClearFilter" runat="server" Text="Clear Filter" NavigateUrl="javascript:void(0);" OnInit="lnkClearFilter_Init">
                        <ClientSideEvents Click="function(s, e) {gridTax.ClearFilter();}" />
                    </dx:ASPxHyperLink>
                </FilterTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
</div>
