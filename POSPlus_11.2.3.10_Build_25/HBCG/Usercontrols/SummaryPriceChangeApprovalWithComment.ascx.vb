﻿
Partial Class Usercontrols_SummaryPriceChangeApprovalWithComment
    Inherits System.Web.UI.UserControl

    Public Event WasPCApproved As EventHandler
    Public Event WasPCCanceled As EventHandler
    Public Delegate Sub EventHandler(sender As Object, e As EventArgs)
    Private theSystemBiz As SystemBiz = New SystemBiz()

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lblErrMgrOverride.Text = String.Empty
            If Session("ApprovalComment1") & "" <> "" Then
                txtCommentL1.Text = Session("ApprovalComment1")
            End If
            If Session("ApprovalComment2") & "" <> "" Then
                txtCommentL2.Text = Session("ApprovalComment2")
            End If
            If Session("ApprovalComment3") & "" <> "" Then
                txtCommentL3.Text = Session("ApprovalComment3")
            End If
        End If
    End Sub

    Private Function CanApprove(pwd As String) As Boolean

        Dim rtnVal As Boolean = False
        Dim effDt As Date = Date.Today

        'check if there was a valid date set
        If (hidEffDt.Value.ToString.isNotEmpty AndAlso
            IsDate(hidEffDt.Value.ToString) AndAlso
            Not (hidEffDt.Value = DateTime.MinValue)) Then
            effDt = hidEffDt.Value.ToString()
        End If

        'first check if the user is an approver or not        
        Dim empCd As String = theSystemBiz.GetApprover(pwd, effDt)
        If (empCd.isNotEmpty) Then

            'set the value back on the hidden field for the caller to access
            rtnVal = True
            hidAppEmpCd.Value = empCd

            If hidSecCd.Value.ToString.isNotEmpty Then
                'If a sec code is being passed, then see if the approver has that security                
                rtnVal = SecurityUtils.hasSecurity(hidSecCd.Value.ToString, empCd)
            Else
                'user is an approver and no security code was passed-in, so return true
                rtnVal = True
            End If
        End If

        Return rtnVal
    End Function
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Not String.IsNullOrEmpty(txtCommentL1.Text.Trim()) Then
            If Not String.IsNullOrEmpty(txtMgrPwd.Text) Then
                spnErrMgrPwd.Visible = False
                Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()  'MM-11224
                If (AppConstants.ManagerOverride.BY_ORDER = prcOverride) Then
                    Session("hidUpdLnNos") = Nothing
                End If
                If CanApprove(txtMgrPwd.Text) Then
                    HideDisplayMessage()
                    Session("IsCanceled") = False
                    'popupPriceChangeAppr.ShowOnPageLoad = False
                    Session("SHOWPOPUP") = Nothing
                    RaiseEvent WasPCApproved(sender, e)
                Else
                    SetDisplayMessage(Resources.POSMessages.MSG0017)
                End If
            Else
                spnErrMgrPwd.Visible = True
                SetDisplayMessage(Resources.LibResources.Label1033)
            End If

        Else
            SetDisplayMessage(Resources.LibResources.Label1032)
            txtCommentL1.Focus()
        End If

    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        spnErrMgrPwd.Visible = False
        lblErrMgrOverride.Text = String.Empty
        Dim prcOverride As String = SalesUtils.GetPriceMgrOverrideSetting()  'MM-11224
        If (AppConstants.ManagerOverride.BY_ORDER = prcOverride) Then
            Session("hidUpdLnNos") = Nothing
            Session("ApprovalComment1") = Nothing
            Session("ApprovalComment2") = Nothing
            Session("ApprovalComment3") = Nothing
        End If
        Session("IsCanceled") = True
        RaiseEvent WasPCCanceled(sender, e)
        popupPriceChangeAppr.ShowOnPageLoad = False
        Session("SHOWPOPUP") = Nothing  'mariam -sep 17,2016 - price change approval
    End Sub

    Public Sub BindData(dt As DataTable)

        If dt.Rows.Count > 0 Then
            rptrLines.DataSource = dt
            rptrLines.DataBind()

            Dim totalSavings As Decimal = 0, qty As Double = 0, origPrice As Decimal = 0, unitPrice As Decimal = 0
            For Each dr As DataRow In dt.Rows
                Double.TryParse(dr("QTY"), qty)
                Decimal.TryParse(dr("ORIG_PRC"), origPrice)
                Decimal.TryParse(dr("UNIT_PRC"), unitPrice)

                'totalSavings += CDbl(dr("QTY")) * (CDec(dr("ORIG_PRC")) - CDec(dr("UNIT_PRC")))
                totalSavings += qty * (origPrice - unitPrice)
            Next

            lblTotPrChangeAmt.Text = String.Format("{0:c}", totalSavings)

        End If
    End Sub

    Public Sub SetDisplayMessage(ByVal msg As String)
        lblErrMgrOverride.Text = msg
        lblErrMgrOverride.Visible = True
    End Sub
    Public Sub HideDisplayMessage()
        lblErrMgrOverride.Text = String.Empty
        lblErrMgrOverride.Visible = False
    End Sub

End Class
