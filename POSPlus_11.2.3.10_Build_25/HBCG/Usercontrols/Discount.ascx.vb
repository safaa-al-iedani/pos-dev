Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Generic
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxTabControl
Imports System.Linq
Partial Class Usercontrols_Discount
    Inherits System.Web.UI.UserControl

    Public Event DiscountApplied As EventHandler
    Public Event DiscountApproved As EventHandler
    Public Event CancelledManagerApproval As EventHandler
    'MM-9870
    Public Event Canceled As EventHandler    
	Public Delegate Sub EventHandler(sender As Object, e As EventArgs)

    Private gobjSalesBiz As New SalesBiz()
    Private LeonsBiz As New LeonsBiz()

    ''' <summary>
    ''' Need to mention the operation flow. eg, SOE/SOM
    ''' </summary>
    ''' <value>SOE / SOM</value>
    ''' <returns>SOE / SOM</returns>
    ''' <remarks></remarks>
    Public Property ReqSource() As String
        Get
            Return hidReqSource.Value.ToUpper
        End Get

        Set(value As String)
            hidReqSource.Value = value.ToUpper
        End Set
    End Property

    ''' <summary>
    ''' Enter DelDocNo needs to pass
    ''' </summary>
    ''' <value>DelDocNo</value>
    ''' <returns>DelDocNo</returns>
    ''' <remarks></remarks>
    Public Property DelDocNum() As String
        Get
            Return hidDelDocNum.Value
        End Get
        Set(value As String)
            hidDelDocNum.Value = value
        End Set
    End Property

    ''' <summary>
    ''' Will load this controls' data
    ''' </summary>
    ''' <param name="orderStatus">the current status of the order</param>
    ''' <remarks>Needs to call this method just after this control initiates</remarks>
    Public Sub LoadDiscount(ByVal orderStatus As String)
        Dim allowDiscounts As Boolean
        If hidReqSource.Value = "RELN" Then
            allowDiscounts = True
        Else
            allowDiscounts = (AppConstants.Order.TYPE_SAL = Session("ord_tp_cd") AndAlso
                                             AppConstants.Order.STATUS_OPEN = orderStatus)
        End If

        If (Not IsNothing(Session("DiscTable"))) Then Session.Remove("DiscTable")

        cbDiscounts.ValidationSettings.ErrorText = Resources.POSErrors.ERR0012
        cbDiscounts.ValidationSettings.RequiredField.ErrorText = Resources.POSErrors.ERR0012

        lstLineItem.ValidationSettings.ErrorText = Resources.POSErrors.ERR0013
        lstLineItem.ValidationSettings.RequiredField.ErrorText = Resources.POSErrors.ERR0013
        'Bind the Discounts codes to the Combobox
        BindDiscounts()

        'Bind the Discounts that have been added to this order already (if any)
        grdDiscItems.DataBind()

        'enables/disables the components accordingly
        ResetControls(allowDiscounts)

        'MM-5661
        ' Reads the Manager approval setting
        prcOverrideSetting.Value = SalesUtils.GetPriceMgrOverride()

        'MM-5661
        If (SysPms.discWithAppSec.Equals("N")) Then
            ' discount(s) not all applicable then disable the page            
            DisableFields()
        End If
    End Sub

    'MM-5661
    ''' <summary>
    ''' Method to Disable the controls on the page
    ''' </summary>    
    ''' <param name="Enabled">Its a default parameter which is set to FALSE </param>
    ''' <remarks></remarks>  
    Public Sub DisableFields(Optional Enabled As Boolean = False)
        For Each Control As Control In Me.FindControl("pnlDisc").Controls
            Dim type As String = Control.GetType().ToString()

            If type.Equals("DevExpress.Web.ASPxEditors.ASPxLabel") Then
                Dim lable As ASPxLabel = CType(Control, ASPxLabel)
                lable.Enabled = Enabled
            ElseIf type.Equals("DevExpress.Web.ASPxGridView.ASPxGridView") Then
                Dim grid As ASPxGridView = CType(Control, ASPxGridView)
                grid.Enabled = Enabled
            ElseIf type.Equals("System.Web.UI.WebControls.Panel") Then
                Dim panel As Panel = CType(Control, Panel)
                panel.Enabled = Enabled
            ElseIf type.Equals("DevExpress.Web.ASPxEditors.ASPxComboBox") Then
                Dim combo As ASPxComboBox = CType(Control, ASPxComboBox)
                combo.Enabled = Enabled
            ElseIf type.Equals("DevExpress.Web.ASPxEditors.ASPxRadioButtonList") Then
                Dim rdb As ASPxRadioButtonList = CType(Control, ASPxRadioButtonList)
                rdb.Enabled = Enabled
            ElseIf type.Equals("DevExpress.Web.ASPxEditors.ASPxListBox") Then
                Dim listBox As ASPxListBox = CType(Control, ASPxListBox)
                listBox.Enabled = Enabled
            ElseIf type.Equals("DevExpress.Web.ASPxEditors.ASPxButton") Then
                Dim button As ASPxButton = CType(Control, ASPxButton)
                button.Enabled = Enabled
            ElseIf type.Equals("DevExpress.Web.ASPxGridView.ASPxGridView") Then
                Dim grid As ASPxGridView = CType(Control, ASPxGridView)
                grid.Enabled = Enabled
            End If
        Next
    End Sub

    'MM-5661
    Public Sub ShowManagerPopUp()
        popupMgrOverride.ShowOnPageLoad = True
    End Sub
   Public Sub HideManagerPopUp()
        popupMgrOverride.ShowOnPageLoad = False
    End Sub
    ''' <summary>
    ''' Populates the Discounts that have been applied so far to the current order
    ''' </summary>
    ''' <param name="dtDiscApplied">the table with the discounts applied so far</param>
    ''' <remarks>
    ''' NOTE: *** This method made was made public ONLY for  the reason that the 
    ''' tab changed event from SOM doesn't fire after mgr approval is displayed for 
    ''' an 'order case' and the discount tab is selected. Therefore, to
    ''' refresh the data on the discount tab, we need to call this method.
    ''' </remarks>
    Public Sub BindDiscountsApplied(ByVal dtDiscApplied As DataTable)
        Session("DISCOUNT") = If(dtDiscApplied.Rows.Count > 0, dtDiscApplied, Nothing)
        grdDiscItems.DataBind()
    End Sub

    ''' <summary>
    ''' Gets the list of non-expired discounts
    ''' </summary>
    ''' <returns>a data table that contains the discounts to choose from</returns>
    Private Function GetDiscounts() As DataTable
        Dim dt As New DataTable
		If (Not IsNothing(Session("DiscTable"))) Then
            dt = Session("DiscTable")
        Else
            'dt = gobjSalesBiz.GetDiscounts()
            ' Daniela add multi company logic improve performance Nov 04
            'Dim emp_init As String
            'If isEmpty(Session("emp_init")) And isNotEmpty(Session("EMP_CD")) Then
            '    emp_init = LeonsBiz.GetEmpInit(Session("EMP_CD"))
            '    Session("emp_init") = emp_init
            'Else : emp_init = Session("emp_init")
            'End If
            dt = LeonsBiz.GetDiscounts(Session("CO_CD"))
            Session("DiscTable") = dt
        End If
        Return dt

    End Function

    ''' <summary>
    ''' Gets the Discounts applied so far to the current order
    ''' </summary>
    ''' <returns>a data table that contains the discounts applied</returns>
    Private Function GetDiscountsApplied() As DataTable

        Dim dt As DataTable = DiscountUtils.GetLineDiscountTable()  'the template required by the display
        Select Case hidReqSource.Value
            Case "SOE"
                dt = gobjSalesBiz.GetDiscountsApplied(Session.SessionID.ToString.Trim, SessVar.discCd, True, False)
            Case "SOM"
                If (Not IsNothing(Session("DISC_APPLIED"))) Then
                    Dim dtDisc As DataTable = Session("DISC_APPLIED")
                    If dtDisc.Rows.Count > 0 AndAlso (dtDisc.Select("DISC_IS_DELETED = false").Length() > 0) Then
                        dt = dtDisc.Select("DISC_IS_DELETED = false").CopyToDataTable()
                    End If
                End If
            Case "RELN"
                If (Not IsNothing(Session("DISC_APPLIED_RELN"))) Then
                    Dim dtDisc As DataTable = Session("DISC_APPLIED_RELN")
                    If dtDisc.Rows.Count > 0 AndAlso (dtDisc.Select("DISC_IS_DELETED = false").Length() > 0) Then
                        dt = dtDisc.Select("DISC_IS_DELETED = false").CopyToDataTable()
                    End If
                End If
        End Select
        Return dt
    End Function

    ''' <summary>
    ''' Returns the Discount level currently selected
    ''' </summary>
    ''' <returns>AppConstants.Discount.LEVEL_HEADER or AppConstants.Discount.LEVEL_LINE</returns>
    Private Function GetDiscountLevelSelected() As String
        Return (If(rdoDiscType.SelectedItem.Value = 0, AppConstants.Discount.LEVEL_HEADER, AppConstants.Discount.LEVEL_LINE))
    End Function

    ''' <summary>
    ''' Creates a discount object with all its details corresponding to the discount the user
    ''' selected in the drop down list
    ''' </summary>
    ''' <returns>the discount selected as an object with all its details</returns>
    Private Function GetDiscountSelected() As DiscountDtc

        Dim objDisc As DiscountDtc = Nothing
        Dim dtDisc As DataTable = GetDiscounts()
        Dim drFiltered As DataRow() = dtDisc.Select("CD = '" & cbDiscounts.SelectedItem.Value & "'")

        For Each dr As DataRow In drFiltered
            objDisc = DiscountUtils.GetDiscountDtc(dr("CD"),
                                                   dr("DES"),
                                                   If(IsDBNull(dr("AMT")), 0, CDbl(dr("AMT"))),
                                                   If(IsDBNull(dr("TP_CD")), "", (dr("TP_CD"))),
                                                   GetDiscountLevelSelected(),
                                                   If(IsDBNull(dr("BY_MNR")), False, ("Y" = (dr("BY_MNR")))),
                                                   If(IsDBNull(dr("COND_DISC")), False, ("Y" = (dr("COND_DISC")))),
                                                   If(IsDBNull(dr("APPLY_PACKAGE")), False, ("Y" = (dr("APPLY_PACKAGE")))), String.Empty)
        Next

        Return objDisc
    End Function

    ''' <summary>
    ''' Updates the lines table found in the session variable.  This variable is
    ''' set in the Session during Order Retrieval and it represents ALL lines in
    ''' the order being processed, therefore it will always be present.  It needs
    ''' to be updated whenever discounts are added or removed
    ''' </summary>
    ''' <param name="dtLines">table that contains the updated lines</param>
    Private Sub SetOrderLinesSOM(ByVal dtLines As DataTable)
        Dim dsLines As DataSet = Session("DEL_DOC_LN#")
        dsLines.Tables.RemoveAt(0)
        dsLines.Tables.Add(dtLines)
    End Sub
    ''' <summary>
    ''' Updates the lines table found in the session variable.  This variable is
    ''' set in the Session during relationship Retrieval and it represents ALL lines in
    ''' the relationship being processed, therefore it will always be present.  It needs
    ''' to be updated whenever discounts are added or removed
    ''' </summary>
    ''' <param name="dtLines">table that contains the updated lines</param>
    Private Sub SetRelationshipLines(ByVal dtLines As DataTable)
        Dim dsLines As DataSet = Session("REL_LN")
        dsLines.Tables.RemoveAt(0)
        dsLines.Tables.Add(dtLines)
    End Sub

    ''' <summary>
    ''' Gets the lines table found in the session variable.   This variable is
    ''' set in the Session during Order Retrieval and it represents ALL lines
    ''' in the order being processed, therefore it will always be present.
    ''' </summary>
    ''' <returns>the lines found for the SOM order being processed</returns>
    Private Function GetOrderLinesSOM() As DataTable
        Dim dsLines As DataSet = Session("DEL_DOC_LN#")
        Dim dtLines As DataTable = dsLines.Tables(0)
        Return dtLines
    End Function

    ''' <summary>
    ''' Gets the lines table found in the session variable.   This variable is
    ''' set in the Session during Order Retrieval and it represents ALL lines
    ''' in the order being processed, therefore it will always be present.
    ''' </summary>
    ''' <returns>the lines found for the Relationship order being processed</returns>
    Private Function GetRelationshipLines() As DataTable
        Dim dsLines As DataSet = Session("REL_LN")
        Dim dtLines As DataTable = dsLines.Tables(0)
        Return dtLines
    End Function

    ''' <summary>
    ''' Creates an instance of the request object needed to make a call when
    ''' adding a new discount to the order
    ''' </summary>
    ''' <param name="hdrLevel">TRUE if the discount is header level; FALSE for line level</param>
    ''' <returns>the custom object populated with the correct details</returns>
    Private Function GetAddDiscountRequest(ByVal hdrLevel As Boolean) As AddDiscountRequestDtc

        Dim objRequest As AddDiscountRequestDtc = Nothing
        If (hdrLevel) Then 'HEADER level discount was selected

            Select Case hidReqSource.Value
                Case "SOE"
                    objRequest = New AddDiscountRequestDtc(Session("PD"), GetDiscountSelected(),
                                                           Session.SessionID.ToString)
                Case "SOM"
                    objRequest = New AddDiscountRequestDtc(Session("PD"), GetDiscountSelected(),
                                                           GetOrderLinesSOM(), GetDiscountsApplied())
                Case "RELN"
                    objRequest = New AddDiscountRequestDtc(Session("PD"), GetDiscountSelected(),
                                                          GetRelationshipLines(), GetDiscountsApplied())
            End Select
        Else  '----------------------------------------------Line level discount
            'gathers the LnSeqNum from the selected lines
            Dim lnSeqNums As New ArrayList()
            For Each item As DevExpress.Web.ASPxEditors.ListEditItem In lstLineItem.SelectedItems
                lnSeqNums.Add(item.Value)
            Next
            Select Case hidReqSource.Value
                Case "SOE"
                    objRequest = New AddDiscountRequestDtc(Session("PD"), GetDiscountSelected(),
                                                           Session.SessionID.ToString, lnSeqNums)
                Case "SOM"
                    objRequest = New AddDiscountRequestDtc(Session("PD"), GetDiscountSelected(),
                                                           GetOrderLinesSOM(), GetDiscountsApplied(),
                                                           lnSeqNums)
                Case "RELN"
                    objRequest = New AddDiscountRequestDtc(Session("PD"), GetDiscountSelected(),
                                                           GetRelationshipLines(), GetDiscountsApplied(),
                                                           lnSeqNums)
            End Select
        End If

        Return objRequest
    End Function

    ''' <summary>
    ''' Makes sure that the proper selections are in place before Adding a discount
    ''' </summary>
    ''' <returns>TRUE if the discount can be added; FALSE otherwise</returns>
    Private Function DoPreAddValidation() As Boolean

        lblError.Text = String.Empty

        If cbDiscounts.SelectedIndex < 1 Then
            'A discount code has not been selected yet.
            lblError.Text = Resources.POSErrors.ERR0012

        ElseIf (rdoDiscType.SelectedItem.Value = 1 AndAlso lstLineItem.SelectedItems.Count <= 0) Then
            'A LINE discount code was selected but NO lines were checked yet
            lblError.Text = Resources.POSErrors.ERR0013

        ElseIf (rdoDiscType.SelectedItem.Value = 0 AndAlso Not SessVar.discCd.isEmpty) Then
            'falling here, means that the discount code in the session can either be as follows:
            '1) if SysPMs "AllowMultiDisc is N", then it is the one selected by the user 
            '    (since only header discounts are allowed in this scenario)
            '2) if SysPMs "AllowMultiDisc is Y", the it is the value in the SysPm-MultiDisc code (given by E1)
            '    (in which case, retrieval of the data is needed to properly determine if a header exists)

            If (Not SysPms.multiDiscCd.Equals(SessVar.discCd)) Then   'number 1 above
                lblError.Text = String.Format(Resources.POSErrors.ERR0014, "")

            Else '---------------------------------------------------- number 2 above 
                'MM-5661
                ' Prompts for an approval when manager approval is BY_LINE
                If (SysPms.discWithAppSec.Equals("W") AndAlso AppConstants.ManagerOverride.BY_LINE.Equals(prcOverrideSetting.Value)) Then
                    Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = popupMgrOverride.FindControl("txtMgrPwd")
                    If txtMgrPwd IsNot Nothing Then
                        txtMgrPwd.Focus()
                    End If
                    popupMgrOverride.ShowOnPageLoad = True
                    lblError.Text = String.Empty
                Else
                    'No need of prompt when manager approval is "NONE or BY_ORDER"
                    Dim strHdrDiscCd As String = String.Empty
                    Select Case hidReqSource.Value
                        Case "SOE"
                            strHdrDiscCd = gobjSalesBiz.GetHeaderDiscountCode(Session.SessionID.ToString(), True)
                        Case "SOM"
                            strHdrDiscCd = gobjSalesBiz.GetHeaderDiscountCode(GetDiscountsApplied())
                        Case "RELN"
                            strHdrDiscCd = gobjSalesBiz.GetHeaderDiscountCode(GetDiscountsApplied())
                    End Select

                    If (strHdrDiscCd.isNotEmpty()) Then
                        Dim strInsert As String = String.Format(Resources.POSMessages.MSG0030, strHdrDiscCd)
                        lblError.Text = String.Format(Resources.POSErrors.ERR0014, strInsert)
                    End If
                End If
            End If
        Else
            ' Prompts for an approval when manager approval is BY_LINE and discount type is also by LINE level         
            If (SysPms.discWithAppSec.Equals("W") AndAlso AppConstants.ManagerOverride.BY_LINE.Equals(prcOverrideSetting.Value)) Then
                Dim txtMgrPwd As DevExpress.Web.ASPxEditors.ASPxTextBox = popupMgrOverride.FindControl("txtMgrPwd")
                If txtMgrPwd IsNot Nothing Then
                    txtMgrPwd.Focus()
                End If
                'MM-5661
                popupMgrOverride.ShowOnPageLoad = True
                lblError.Text = String.Empty
            End If
        End If

        Return (lblError.Text.isEmpty())
    End Function

    ''' <summary>
    ''' Makes sure the data is ready to call the REMOVE discount process
    ''' </summary>
    ''' <returns>TRUE if Discount REMOVE can be run; FALSE otherwise</returns>
    Private Function DoPreRemoveValidation() As Boolean
        Dim bolPassedValidation As Boolean = False

        If grdDiscItems.VisibleRowCount <= 0 Then
            lblError.Text = Resources.POSErrors.ERR0015
            lblMessage.Text = String.Empty
        ElseIf grdDiscItems.Selection.Count <= 0 Then
            lblError.Text = Resources.POSErrors.ERR0016
            lblMessage.Text = String.Empty
        Else
            lblError.Text = String.Empty
            bolPassedValidation = True
        End If

        Return bolPassedValidation
    End Function

    ''' <summary>
    ''' Does the required processing after a call to ADD discount completes
    ''' </summary>
    ''' <param name="objResponse">custom object resulting of the call to ADD discounts</param>
    ''' <param name="discountDtc">the discount object for the discount just applied</param>
    Private Sub DoAfterAddDiscount(ByVal objResponse As AddDiscountResponseDtc, ByVal discountDtc As DiscountDtc)

        'NOTE Discounts would have been updated unless Status is HasErro (means that for Warning/Success is ok to update here)
        If (DiscountUtils.DiscountStatus.HasError <> objResponse.status AndAlso objResponse.numLnsDiscounted > 0) Then
            'The session value represents the value to be saved to the SO table, therefore
            'if SysPms.allowMultiDisc is Y, the discount code from SysPms has to be used
            'if SysPms.allowMultiDisc is N, the discount code applied (only HEADER is allowed)
            SessVar.discCd = If(SysPms.allowMultDisc, SysPms.multiDiscCd, discountDtc.cd)
        End If

        'For SOM, OrderLines and DiscountsApplied were updated, update them in the session
        If ("SOM" = hidReqSource.Value) Then
            SetOrderLinesSOM(objResponse.orderLines)
            Session("DISC_APPLIED") = objResponse.discountsApplied
            grdDiscItems.DataBind()
        ElseIf ("RELN" = hidReqSource.Value) Then
            SetRelationshipLines(objResponse.orderLines)
            Session("DISC_APPLIED_RELN") = objResponse.discountsApplied
            grdDiscItems.DataBind()
        Else
            BindDiscountsApplied(objResponse.discountsApplied)
        End If

        ResetControls()
        SessVar.Remove(SessVar.discReCalcVarNm)

        If DiscountUtils.DiscountStatus.Success = objResponse.status Then
            lblMessage.Text = String.Format(Resources.POSMessages.MSG0031, discountDtc.desc)
        Else
            lblError.Text = objResponse.message
        End If

    End Sub

    ''' <summary>
    ''' Does the required processing after a call to REMOVE discount completes
    ''' </summary>
    ''' <param name="objResponse">custom object resulting of the call to REMOVE discounts</param>
    Private Sub DoAfterRemoveDiscount(ByVal objResponse As RemoveDiscountResponseDtc)
        'if ALL discounts have been removed, then resets the value in the session
        Dim nonDelDisc As Integer = 0

        If ("SOM" = hidReqSource.Value) Then
            'For SOM, OrderLines and DiscountsApplied were updated, update them in the session
            SetOrderLinesSOM(objResponse.orderLines)
            Session("DISC_APPLIED") = objResponse.discountsApplied
            grdDiscItems.DataBind()

            nonDelDisc = objResponse.discountsApplied.Select("DISC_IS_DELETED = false").Length()
            ' if the process of removing a line or a discount has eliminated the queried header discount code, then clear the session variable
            If nonDelDisc > 0 AndAlso gobjSalesBiz.GetHeaderDiscountCode(objResponse.discountsApplied) <> SessVar.existHdrDiscCd Then
                SessVar.Remove(SessVar.existHdrDiscCdVarNm)
            End If
        ElseIf ("RELN" = hidReqSource.Value) Then
            'For Relationship, relationship and DiscountsApplied were updated, update them in the session
            SetRelationshipLines(objResponse.orderLines)
            Session("DISC_APPLIED_RELN") = objResponse.discountsApplied
            grdDiscItems.DataBind()

            nonDelDisc = objResponse.discountsApplied.Select("DISC_IS_DELETED = false").Length()
            ' if the process of removing a line or a discount has eliminated the queried header discount code, then clear the session variable
            If nonDelDisc > 0 AndAlso gobjSalesBiz.GetHeaderDiscountCode(objResponse.discountsApplied) <> SessVar.existHdrDiscCd Then
                SessVar.Remove(SessVar.existHdrDiscCdVarNm)
            End If
        Else
            BindDiscountsApplied(objResponse.discountsApplied)

            nonDelDisc = objResponse.discountsApplied.Rows.Count
            If nonDelDisc > 0 Then
                If objResponse.discountsApplied.Columns.Contains("DISC_IS_DELETED") Then
                    nonDelDisc = objResponse.discountsApplied.Select("DISC_IS_DELETED = false").Length()
                Else
                    nonDelDisc = nonDelDisc
                End If
            End If
            'If nonDelDisc > 0 Then nonDelDisc = objResponse.discountsApplied.Select("DISC_IS_DELETED = false").Length()
            ' if the process of removing a line or a discount has eliminated the queried header discount code, then clear the session variable
            If nonDelDisc > 0 AndAlso gobjSalesBiz.GetHeaderDiscountCode(objResponse.discountsApplied) <> SessVar.existHdrDiscCd Then
                SessVar.Remove(SessVar.existHdrDiscCdVarNm)
            End If
        End If

        'grdDiscItems.DataBind()

        SessVar.discCd = If(nonDelDisc > 0, SessVar.discCd, String.Empty)

        ResetControls()

        grdDiscItems.Selection.UnselectAll()
        lblMessage.Text = Resources.POSMessages.MSG0029
    End Sub

    ''' <summary>
    ''' Populates the Discounts drop-down list box
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindDiscounts()
        Dim dt As DataTable = GetDiscounts()

        cbDiscounts.Items.Clear()
        cbDiscounts.DataSource = dt
        cbDiscounts.ValueField = "CD"
        cbDiscounts.TextField = "DES"
        cbDiscounts.DataBind()
        ' Daniela french
        cbDiscounts.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(Resources.LibResources.Label530, ""))
        cbDiscounts.Items.FindByText(Resources.LibResources.Label530).Value = "0"
        cbDiscounts.SelectedIndex = 0
    End Sub

    Protected Sub grdDiscItems_DataBinding(sender As Object, e As EventArgs) Handles grdDiscItems.DataBinding
        If (hidReqSource.Value = "SOM" OrElse hidReqSource.Value = "RELN" OrElse hidReqSource.Value = "SOE") Then
            Dim disdt As DataTable
            Dim _seq As New List(Of Integer)
            If hidReqSource.Value = "SOE" Then
                If Session("IsAdded") = False Then
                    disdt = GetDiscountsApplied()
                    Dim seq = From u In disdt.AsEnumerable()
                                    Select u.Field(Of Integer)("SEQ_NUM")
                    For i As Integer = 0 To seq.Count() - 1
                        If Not _seq.Contains(seq(i)) Then
                            _seq.Add(seq(i))
                        End If
                    Next
                    Session("isSeqNum") = _seq
                    grdDiscItems.DataSource = GetDiscountsApplied()
                Else
                    disdt = GetDiscountsApplied()
                    If Not IsNothing(Session("isSeqNum")) Then
                        Dim seqnum As List(Of Integer) = Session("isSeqNum")
                        For Each num In seqnum.Distinct()
                            For i As Integer = 0 To disdt.Rows.Count() - 1
                                If disdt.Rows(i)("SEQ_NUM") = num Then
                                    disdt.Rows(i)("Approved") = "Y"
                                End If
                            Next
                        Next
                    End If
                    'MM-9870
                    Session("discountsApplied") = disdt
                    grdDiscItems.DataSource = disdt
                End If

            Else
                Session("discountsApplied") = GetDiscountsApplied()
                grdDiscItems.DataSource = GetDiscountsApplied()
            End If
            grdDiscItems.Selection.UnselectAll()
        ElseIf Not IsNothing(Session("DISCOUNT")) Then
            Dim dt As DataTable = DirectCast(Session("DISCOUNT"), DataTable)
            grdDiscItems.DataSource = dt
        Else
            grdDiscItems.DataSource = GetDiscountsApplied()
        End If
    End Sub

    ''' <summary>
    ''' Populates the Order Lines grid, with all those lines that are qualified
    ''' to receive the discount selected by the user. 
    ''' NOTE: this method gets called regardless of the Discount level, so that
    ''' a message is displayed to the user if NO lines are found.
    ''' </summary>
    ''' <param name="isHeaderDisc">TRUE when header discount is being applied; FALSE otherwise</param>
    Private Sub BindOrderLines(ByVal isHeaderDisc As Boolean)
        Dim dtLines As New DataTable
        Dim objDisc As DiscountDtc = GetDiscountSelected()

        Dim isEnableBtnAdd As Boolean = True
        Dim strErr As String = String.Empty

        '-- retrieves the lines that are eligible for the discount selected
        If (Not IsNothing(objDisc)) Then
            Select Case hidReqSource.Value
                Case "SOE"
                    dtLines = gobjSalesBiz.GetDiscountableLines(Session.SessionID.ToString(), objDisc)
                Case "SOM"
                    Dim dtSOMLines As DataTable = GetOrderLinesSOM()

                    Dim allLinesCount As Integer = dtSOMLines.Rows.Count
                    Dim shudLinesCount As Integer = dtSOMLines.Select("SHU = 'Y'").Length

                    If shudLinesCount > 0 Then
                        'informs user that SHU'd lines will NOT participate of NEW discounts
                        strErr = String.Format(Resources.POSErrors.ERR0020, "discount")

                        'HEADER discounts will be allowed, as long as there are NON SHU'd lines present
                        If (AppConstants.Discount.LEVEL_HEADER = objDisc.level) Then
                            isEnableBtnAdd = (allLinesCount > shudLinesCount)
                        End If
                    End If

                    dtLines = gobjSalesBiz.GetDiscountableLines(dtSOMLines, objDisc, True)
                Case "RELN"
                    Dim dtRELNLines As DataTable = GetRelationshipLines()
                    If dtRELNLines.Rows.Count > 0 Then
                        dtLines = gobjSalesBiz.GetDiscountableLines(dtRELNLines, objDisc, False)
                    End If
            End Select
        End If

        '-- Displays components accordingly based on data found and discount level
        Dim bolLinesFound As Boolean = (Not IsNothing(dtLines) AndAlso dtLines.Rows.Count > 0)
        Dim bolDispLines As Boolean = (bolLinesFound AndAlso Not isHeaderDisc)

        btnAdd.ClientEnabled = isEnableBtnAdd And bolLinesFound
        lblError.Text = If(String.IsNullOrEmpty(strErr), If(bolLinesFound, String.Empty, Resources.POSErrors.ERR0017), strErr)

        'Lines should NOT display for Header level discount
        lstLineItem.Enabled = bolDispLines
        lstLineItem.ClientVisible = bolDispLines
        lblLineItem.ClientVisible = bolDispLines

        '-- clears the previous records
        lstLineItem.Items.Clear()

        If (bolDispLines) Then
            Dim dcCdDesc As New DataColumn("CD_DESC")
            dcCdDesc.Expression = String.Format("'('+{0}+') '+{1}", "ITM_CD", "DES")
            dtLines.Columns.Add(dcCdDesc)

            lstLineItem.ValueField = "LN_SEQNUM"
            lstLineItem.TextField = "CD_DESC"
            lstLineItem.DataSource = dtLines
            lstLineItem.DataBind()
        End If
    End Sub

    ''' <summary>
    ''' Reset all the controls to their default state.  
    ''' Also, since Discounts can only be entered for SALE type documents, 
    ''' disables/enables the components accordingly.
    ''' </summary>
    Private Sub ResetControls(Optional ByVal allowDiscounts As Boolean = True)

        lblError.Text = If(allowDiscounts, String.Empty, Resources.POSErrors.ERR0018)
        lblMessage.Text = String.Empty

        cbDiscounts.SelectedIndex = 0
        cbDiscounts.ClientEnabled = allowDiscounts
        rdoDiscType.SelectedIndex = 0
        rdoDiscType.ClientEnabled = False

        lblLineItem.ClientVisible = False
        lstLineItem.ClientVisible = False
        lstLineItem.ClientEnabled = allowDiscounts
        lstLineItem.Items.Clear()
        Dim appliedDisc As Boolean = (grdDiscItems.VisibleRowCount > 0)
        grdDiscItems.Visible = appliedDisc
        grdDiscItems.Enabled = allowDiscounts

        btnAdd.ClientEnabled = allowDiscounts
        btnRemove.ClientEnabled = allowDiscounts
        btnRemove.ClientVisible = allowDiscounts And appliedDisc
        btnReset.ClientEnabled = allowDiscounts
    End Sub

    Protected Sub cbDiscounts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbDiscounts.SelectedIndexChanged

        lblError.Text = String.Empty
        lblMessage.Text = String.Empty
        Dim discLevel As String = String.Empty

        'when a value other than 'Please select discount' is selected, finds out the discount level for it
        If (DirectCast(sender, DevExpress.Web.ASPxEditors.ASPxComboBox).SelectedIndex >= 1) Then
            Dim dt As DataTable = GetDiscounts()
            Dim discCd As String = DirectCast(sender, DevExpress.Web.ASPxEditors.ASPxComboBox).SelectedItem.Value
            Dim result As DataRow() = dt.Select(String.Format("CD = '{0}'", discCd))
            discLevel = result(0)("DISC_LEVEL")
        End If

        'enables/disables components accordingly
        'When the E1 system parameter 'Allow Multi Discounts' is N, then no line discounts are allowed
        'By default only header discounts will be allowed
        If (SysPms.allowMultDisc AndAlso
            (AppConstants.Discount.LEVEL_EITHER = discLevel OrElse AppConstants.Discount.LEVEL_LINE = discLevel)) Then

            'radio button should only be enabled when the discount type is either
            rdoDiscType.SelectedIndex = 1       'line level selection
            rdoDiscType.ClientEnabled = (AppConstants.Discount.LEVEL_EITHER = discLevel)
            BindOrderLines(False)
        Else
            rdoDiscType.SelectedIndex = 0       'header level selection
            rdoDiscType.ClientEnabled = False
            BindOrderLines(True)
        End If
    End Sub

    Protected Sub rdoDiscType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoDiscType.SelectedIndexChanged
        Dim isHdr As Boolean = (DirectCast(sender, DevExpress.Web.ASPxEditors.ASPxRadioButtonList).SelectedIndex = 0)

        lblError.Text = String.Empty
        lblMessage.Text = String.Empty
        BindOrderLines(isHdr)
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        grdDiscItems.DataBind()
        ResetControls()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        'MM-9870
        Session("discountsApplied") = String.Empty
        'makes sure that the discount can be added
        If Not DoPreAddValidation() Then Exit Sub

        'MM-5661
        If Not ("L".Equals(prcOverrideSetting.Value)) Or (SysPms.discWithAppSec.Equals("O")) Then
            Dim objResponse As AddDiscountResponseDtc = Nothing
            Dim objRequest As AddDiscountRequestDtc = Nothing

            If (rdoDiscType.SelectedItem.Value = 0) Then 'HEADER level discount was selected

                objRequest = GetAddDiscountRequest(True)
                If ("SOM".Equals(hidReqSource.Value.ToString())) Then
                    objResponse = gobjSalesBiz.AddHeaderDiscount(objRequest, True)
                Else
                    Session("IsAdded") = True
                    objResponse = gobjSalesBiz.AddHeaderDiscount(objRequest, False)
                End If

            Else  '----------------------------------------------Line level discount
                objRequest = GetAddDiscountRequest(False)
                If ("SOM".Equals(hidReqSource.Value.ToString())) Then
                    objResponse = gobjSalesBiz.AddLineDiscount(objRequest, True)
                Else
                    Session("IsAdded") = True
                    objResponse = gobjSalesBiz.AddLineDiscount(objRequest, False)
                End If
                'objResponse = gobjSalesBiz.AddLineDiscount(objRequest)
            End If

            DoAfterAddDiscount(objResponse, objRequest.discount)

            RaiseEvent DiscountApplied(sender, e)
        End If
        'Session("discountsApplied") = objResponse.discountsApplied
        Session("IsCanceled") = False
        hidItmSeq.Value = getNotApprovedCount()    
	
	End Sub

    Protected Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        'makes sure that proper selection has been made to execute the removal
        If Not DoPreRemoveValidation() Then Exit Sub

        'Here we are forcing to select all the header item, if any of them selected
        Dim isHeader As Boolean = False
        Dim selIsHeader As List(Of Object) = grdDiscItems.GetSelectedFieldValues("DISC_LEVEL")
        For Each item As String In selIsHeader
            If AppConstants.Discount.LEVEL_HEADER = item Then
                isHeader = True
                Exit For
            End If
        Next

        'auto checks ALL the lines that share the HEADER discount
        If isHeader AndAlso grdDiscItems.VisibleRowCount > 0 Then
            For index = 0 To grdDiscItems.VisibleRowCount - 1
                If AppConstants.Discount.LEVEL_HEADER.Equals(grdDiscItems.GetRowValues(index, "DISC_LEVEL").ToString().ToUpper()) Then
                    grdDiscItems.Selection.SelectRow(index)
                End If
            Next
        End If

        Dim strPkpDelFlag As String = Session("PD")
        Dim lstDiscToRemove As New List(Of RemoveDiscountDtc)
        Dim objResponse As New RemoveDiscountResponseDtc
        Dim dtSOMLines As DataTable = Nothing
        Dim dtRELLines As DataTable = Nothing
        Dim shudLinesSelected As Boolean = False

        'gathers ALL check-marked records in the table
        Dim selRows As List(Of Object) = grdDiscItems.GetSelectedFieldValues(New String() {"LNSEQ_NUM", "DISC_CD", "DISC_AMT", "SEQ_NUM"})

        'gathers the discounts that should be removed
        If ("SOM".Equals(hidReqSource.Value.ToString())) Then
            'For SOM, has to check that NON-SHUd lines are sent over for removal
            dtSOMLines = GetOrderLinesSOM()

            For Each item As Object() In selRows
                ' do not remove discount from lines that are SHU'd with disc amt > 0; if zero doesn't make any difference and if zero, then might stop discount removal for no reason
                If (dtSOMLines.Select("[DEL_DOC_LN#] = " & item(0).ToString() & " AND SHU = 'Y' AND DISC_AMT > 0 ").Length > 0) Then
                    shudLinesSelected = True
                Else
                    lstDiscToRemove.Add(New RemoveDiscountDtc(item(0).ToString(), item(1).ToString(), item(2).ToString(), item(3).ToString()))
                End If
            Next item
        ElseIf ("RELN".Equals(hidReqSource.Value.ToString())) Then
            'for Relationship, the SHU doesn't apply, adds all discounts for removal
            dtRELLines = GetRelationshipLines()
            For Each item As Object() In selRows
                lstDiscToRemove.Add(New RemoveDiscountDtc(item(0).ToString(), item(1).ToString(), item(2).ToString(), item(3).ToString()))
            Next
        Else
            'for SOE, the SHU doesn't apply, adds all discounts for removal
            For Each item As Object() In selRows
                lstDiscToRemove.Add(New RemoveDiscountDtc(item(0).ToString(), item(1).ToString(), item(2).ToString(), item(3).ToString()))
            Next item
        End If

        'Removes the discounts and reapplies the remaining discounts accordingly
        Select Case hidReqSource.Value
            Case "SOE"
                'MM-9484
                objResponse = gobjSalesBiz.RemoveDiscount(Session.SessionID.ToString, strPkpDelFlag, lstDiscToRemove, True)

            Case "SOM"
                If shudLinesSelected Then
                    lblError.Text = Resources.POSErrors.ERR0021
                End If

                If (lstDiscToRemove.Count = 0) Then
                    ResetControls()
                    grdDiscItems.Selection.UnselectAll()
                    Exit Sub
                Else
                    Dim dtDiscApplied As DataTable = GetDiscountsApplied()
                    objResponse = gobjSalesBiz.RemoveDiscount(dtSOMLines, dtDiscApplied, strPkpDelFlag, lstDiscToRemove, True)
                End If
            Case "RELN"
                If (lstDiscToRemove.Count = 0) Then
                    ResetControls()
                    grdDiscItems.Selection.UnselectAll()
                    Exit Sub
                Else
                    Dim dtDiscApplied As DataTable = GetDiscountsApplied()
                    objResponse = gobjSalesBiz.RemoveDiscount(dtRELLines, dtDiscApplied, strPkpDelFlag, lstDiscToRemove, False)
                End If
        End Select

        DoAfterRemoveDiscount(objResponse)
        'MM-9870
        Dim dt1 As DataTable = objResponse.discountsApplied
        Dim dec As Boolean = False
        Dim dt2 As DataTable = Session("discountsApplied")

        'we needd to remove the items from Session("discountsApplied") for which the discounts are removed.
        'look for the items that are not present after re-calculating the discounts and delete them.
        If (dt1.Rows.Count > 0) Then
            'loop through all the rows of the table Session("discountsApplied")
            For i As Integer = dt2.Rows.Count - 1 To 0 Step -1
                dec = False
                For Each dr In dt1.Rows
                    'if the row found in both the places, then continue
                    If dr("LNSEQ_NUM") = dt2.Rows(i)("LNSEQ_NUM") AndAlso dr("SEQ_NUM") = dt2.Rows(i)("SEQ_NUM") Then
                        dec = True
                        Exit For
                    Else

                    End If
                Next
                'If row does not exists, then delete it
                If Not dec Then
                    dt2.Rows.Remove(dt2.Rows(i))
                End If
            Next
        End If
        'Update back the latest to the session
        Session("discountsApplied") = dt2
        'Update the hidden field with the lines exists which are not yet aproved
        hidItmSeq.Value = getNotApprovedCount()
        Dim dt As New DataTable
        If Not IsNothing(Session("discountsApplied")) Then
            dt = Session("discountsApplied")
            If dt.Rows.Count() = 0 Then
                Session("isSeqNum") = Nothing
            End If
        End If
        Session("IsAdded") = False
        ' Session("discountsApplied") = objResponse.discountsApplied
        RaiseEvent DiscountApplied(sender, e)
    End Sub

    'MM-5661
    ''' <summary>
    ''' Executes when the manager override is successfully carried out via the popup. Any updates to the price
    ''' on the line and other specific updates take place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasApproved(sender As Object, e As EventArgs) Handles ucMgrOverride.wasApproved
        ' Part of DoPreAddValidation() method
        Dim strHdrDiscCd As String = String.Empty
        Session("mgrApproval") = "Y"

        If (AppConstants.ManagerOverride.BY_ORDER.Equals(SalesUtils.GetPriceMgrOverride()) AndAlso grdDiscItems.VisibleRowCount > 0) Then
            ' Do nothing
            ' Just need the manager approval here
            RaiseEvent DiscountApproved(sender, e)
        Else
            'Need to execute the steps if the current applied discount is header discount
            If (rdoDiscType.SelectedItem.Value = 0) Then
                Select Case hidReqSource.Value
                    Case "SOE"
                        strHdrDiscCd = gobjSalesBiz.GetHeaderDiscountCode(Session.SessionID.ToString(), True)
                    Case "SOM"
                        strHdrDiscCd = gobjSalesBiz.GetHeaderDiscountCode(GetDiscountsApplied())
                End Select

                If (strHdrDiscCd.isNotEmpty()) Then
                    Dim strInsert As String = String.Format(Resources.POSMessages.MSG0030, strHdrDiscCd)
                    lblError.Text = String.Format(Resources.POSErrors.ERR0014, strInsert)
                    popupMgrOverride.ShowOnPageLoad = False
                    Exit Sub
                End If
            End If

            Dim objResponse As AddDiscountResponseDtc = Nothing
            Dim objRequest As AddDiscountRequestDtc = Nothing

            If (rdoDiscType.SelectedItem.Value = 0) Then 'HEADER level discount was selected

                objRequest = GetAddDiscountRequest(True)
                If ("SOM".Equals(hidReqSource.Value.ToString())) Then
                    objResponse = gobjSalesBiz.AddHeaderDiscount(objRequest, True)
                Else
                    objResponse = gobjSalesBiz.AddHeaderDiscount(objRequest, False)
                End If
                'objResponse = gobjSalesBiz.AddHeaderDiscount(objRequest)

            Else  '----------------------------------------------Line level discount
                objRequest = GetAddDiscountRequest(False)
                If ("SOM".Equals(hidReqSource.Value.ToString())) Then
                    objResponse = gobjSalesBiz.AddLineDiscount(objRequest, True)
                Else
                    objResponse = gobjSalesBiz.AddLineDiscount(objRequest, False)
                End If
                'objResponse = gobjSalesBiz.AddLineDiscount(objRequest)
            End If

            DoAfterAddDiscount(objResponse, objRequest.discount)

            RaiseEvent DiscountApplied(sender, e)
        End If
        popupMgrOverride.ShowOnPageLoad = False
        'MM-9870
        'Updating 'Y' in the Approved column if click 'Submit in the Manager Approval popup
        If (SysPms.discWithAppSec = "W") AndAlso (Session("mgrApproval") = "Y") AndAlso (grdDiscItems.VisibleRowCount > 0) AndAlso (AppConstants.ManagerOverride.BY_ORDER.Equals(SalesUtils.GetPriceMgrOverride())) Then
            If Not IsNothing(Session("discountsApplied")) Then
                Dim discountsApplied As DataTable = Session("discountsApplied")
                ' If discountsApplied.Columns.Contains("Approved") Then
                Dim drs = (From u In discountsApplied.AsEnumerable() _
                                               Where u.Field(Of String)("Approved") = "A"
                                                Select u)
                For Each Row As DataRow In drs
                    If Row("Approved").ToString.Equals("A") Then
                        Row("Approved") = "Y"
                    End If
                Next
                'End If
            End If
        End If
    End Sub

    'MM-5661
    ''' <summary>
    ''' Gets fired when the manager override for 'line level' fails. Any resetting of values takes place here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ucMgrOverride_wasCanceled(sender As Object, e As EventArgs) Handles ucMgrOverride.wasCanceled
        popupMgrOverride.ShowOnPageLoad = False
        Session("mgrApproval") = "N"
        Select Case hidReqSource.Value
            Case "SOM"
                RaiseEvent CancelledManagerApproval(sender, e)
            Case "SOE"
                RaiseEvent CancelledManagerApproval(sender, e)
                'MM-9870
            Case "RELN"
                RaiseEvent Canceled(sender, e)
        End Select
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ' MM - 5661
        If Request.Form("__EVENTTARGET") = "popupMgrOverride" Then
            popupMgrOverride.ShowOnPageLoad = True
        End If
        Session("IsAdded") = False
        hidItmSeq.Value = 0
    End Sub

    Private Function SetSeqNumSession() As Integer
        If Not IsNothing(Session("DESCSEQNUM")) Then
            Session("DESCSEQNUM") = Session("DESCSEQNUM") + 1
        Else
            Session("DESCSEQNUM") = 1
        End If
        Return Session("DESCSEQNUM")
    End Function

    Private Function getNotApprovedCount() As Integer

        Dim count As Integer = 0
        If Not IsNothing(Session("discountsApplied")) AndAlso Session("discountsApplied").GetType() = GetType(DataTable) Then
            Dim dt As DataTable = Session("discountsApplied")
            count = (From u In dt.AsEnumerable()
                     Where u.Field(Of String)("Approved") <> "Y"
                     Select u).Count()
        End If
        Return count
    End Function
End Class
