﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MessagePopup.ascx.vb" Inherits="Usercontrols_MessagePopup" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<dx:ASPxPopupControl ID="popupMsg" runat="server" ClientInstanceName="popupMsg" Modal="true" CloseAction="None" AllowDragging="true"
    HeaderText="Message" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    ShowPageScrollbarWhenModal="true" Width="400px" ShowCloseButton="false" AllowResize="true">
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <asp:Panel ID="pnlMsg" runat="server" DefaultButton="btnClose">
                <table>
                    <tr>
                        <td style="text-align: center;">
                            <dx:ASPxImage ID="imgIcon" runat="server" Style="padding-right: 20px;" ></dx:ASPxImage>                          
                        </td>
                        <td style="white-space: nowrap;">
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxMemo ID="txtMsg" runat="server" Width="400"  Height ="60" Font-Bold="true" Enabled="false"></dx:ASPxMemo>                                        
                                    </td>                                  
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right;">
                            <table style="float: right;">
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="btnOK" runat="server" Text="OK"></dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel"></dx:ASPxButton>
                                    </td>     
                                    <td>
                                        <dx:ASPxButton ID="btnClose" runat="server" Text="Close"></dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
