﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ZoneCode.ascx.vb" Inherits="Usercontrols_ZoneCode" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:HiddenField ID="hidZoneCode" runat="server" />
<asp:HiddenField ID="hidSaleType" runat="server" />
<asp:HiddenField ID="hidZip" runat="server" />
<table>
    <tr style="padding-top: 10px; padding-bottom: 10px; width: 40px;">
        <td style="width: 40px;">
            <dx:ASPxTextBox ID="txtzip" runat="server" Width="43px" CssClass="style5"></dx:ASPxTextBox>
        </td>
        <td>
            <dx:ASPxButton ID="btnChangeZone" runat="server" Text="Change Zone" OnClick="btnChangeZone_Click"></dx:ASPxButton>
        </td>
    </tr>
    <tr style="padding-top: 10px; padding-bottom: 10px">
        <td colspan="2">
            <dx:ASPxLabel ID="lblInfo" runat="server" Height="8px" Width="300px" Visible="false"></dx:ASPxLabel>
        </td>
    </tr>
</table>
<br />
<dx:ASPxGridView ID="gridzonecode" runat="server" AutoGenerateColumns="False" KeyFieldName="ZONE_CD"
    SettingsBehavior-ProcessSelectionChangedOnServer="true"
    SettingsBehavior-AllowFocusedRow="true"
    Width="473px" Font-Size="XX-Small" Settings-ShowFilterRow="true">
    <Columns>
        <dx:GridViewDataTextColumn FieldName="ZONE_CD" VisibleIndex="1" Caption="Zone">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="DES" Caption="Description" VisibleIndex="2">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="DELIVERY_STORE_CD" Caption="Store" VisibleIndex="3">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="DEFAULT_DEL_CHG" Caption="Delivery" VisibleIndex="4">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="DEFAULT_SETUP_CHG" Caption="Setup" VisibleIndex="5">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Select" Settings-AllowSort="False"
            HeaderStyle-Cursor="default" Width="15%" VisibleIndex="6">
            <CellStyle HorizontalAlign="Center"></CellStyle>
            <DataItemTemplate>
                <dx:ASPxButton ID="btnSelect" runat="server" Text="Select" CommandName="Select"
                    CommandArgument="<%# Container.ItemIndex %>">
                </dx:ASPxButton>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        <dx:GridViewCommandColumn ShowClearFilterButton="true"></dx:GridViewCommandColumn>
    </Columns>
    <SettingsPager PageSize="5">
        <PageSizeItemSettings Items="5, 10, 20,30" Visible="True">
        </PageSizeItemSettings>
    </SettingsPager>
    <Styles>
        <AlternatingRow Enabled="true" BackColor="Beige" />
    </Styles>
</dx:ASPxGridView>
