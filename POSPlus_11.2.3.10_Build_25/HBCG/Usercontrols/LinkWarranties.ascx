﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LinkWarranties.ascx.vb" Inherits="Usercontrols_LinkWarranties" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxPopup" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxCallback" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxEditor" %>

<script type="text/javascript">
    function CloseLinkWarrantiesPopup(s, e) {
        popupLinkWarranties.Hide();
        ASPxClientEdit.ClearEditorsInContainerById('divErrLinkWarr');
        $("#hidSelectedRowIds").val('');
    }

    function onItemsCheckValueChanged(s, e) {
        $("#hidSelectedRowIds").val(s.GetSelectedValues().join(","));
    }
</script>

<dxPopup:ASPxPopupControl ID="popupLinkWarranties" runat="server" ClientInstanceName="popupLinkWarranties" Modal="true"
    ShowCloseButton="false" CloseAction="None" AllowDragging="true" HeaderText="Link Warranty" HeaderStyle-Font-Bold="false"
    HeaderStyle-HorizontalAlign="Left" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    ShowPageScrollbarWhenModal="true" Width="679px" MaxHeight="800px" ShowLoadingPanel="False">
    <ContentCollection>
        <dxPopup:PopupControlContentControl ID="pccLinkWarranties" runat="server" ClientIDMode="Static" Width="100%">
            <asp:HiddenField ID="hidTriggeredSKU" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hidSelectedRowIds" runat="server" ClientIDMode="Static" />
            <div style="display: table; margin: auto; width: 95%;">
                <div style="display: table-row;">
                    <div style="display: table-cell; border: #8ba0bc 1px solid;">
                        <div style="display: table; margin: auto; width: 98%;">
                            <div style="display: table-row;">
                                <div id="divErrLinkWarr" style="display: table-cell; text-align: left; padding-top: 10px;">
                                    <dxEditor:ASPxValidationSummary ID="valSumLinkWarr" runat="server" ValidationGroup="valGrpLinkWarr"
                                        ClientInstanceName="valSumLinkWarr" RenderMode="Table" ShowErrorAsLink="False">
                                    </dxEditor:ASPxValidationSummary>
                                </div>
                            </div>
                            <div style="display: table-row;">
                                <div style="display: table-cell; text-align: left; padding-top: 10px;">
                                    <dxEditor:ASPxLabel ID="lblCmnWarDesc" runat="server" ClientIDMode="Static"
                                        Text="The following items included on your purchase are also eligible for previously selected Warranty:">
                                    </dxEditor:ASPxLabel>
                                </div>
                            </div>
                            <div style="display: table-row; text-align: left;">
                                <div style="display: table-cell;">
                                    <asp:HiddenField ID="hidWarrCd" runat="server" ClientIDMode="Static" />
                                    <dxEditor:ASPxLabel ID="lblCommonWarranty" runat="server" ClientIDMode="Static"></dxEditor:ASPxLabel>
                                </div>
                            </div>
                            <div style="display: table-row;">
                                <div style="display: table-cell; text-align: left; padding: 10px;">
                                    <dxEditor:ASPxCheckBoxList ID="chkItems" runat="server" ClientIDMode="Static" AutoPostBack="false"
                                        Border-BorderStyle="None" ValueField="ROW_ID" TextField="ITEM" ItemSpacing="5px" Width="98%">
                                        <ValidationSettings EnableCustomValidation="True" ValidationGroup="valGrpLinkWarr" SetFocusOnError="True"
                                            ErrorDisplayMode="Text" ErrorTextPosition="Bottom" CausesValidation="True">
                                            <RequiredField ErrorText="Please select at least an item to link the Warranty." IsRequired="True" />
                                            <RegularExpression ErrorText="Please select at least an item to link the Warranty." />
                                            <ErrorFrameStyle Font-Size="10px">
                                                <ErrorTextPaddings PaddingLeft="0px" />
                                            </ErrorFrameStyle>
                                        </ValidationSettings>
                                    </dxEditor:ASPxCheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="display: table-row;">
                    <div style="display: table-cell; text-align: right; padding-top: 10px; padding-bottom: 10px;">
                        <dxEditor:ASPxButton ID="btnLinkSKUs" runat="server" ClientIDMode="Static" Text="Link multiple SKUs" AutoPostBack="true"
                            UseSubmitBehavior="true" CausesValidation="true" ValidationGroup="valGrpLinkWarr">
                            <ClientSideEvents Click="function(s, e) { if(ASPxClientEdit.ValidateGroup('valGrpLinkWarr')){
                                    popupLinkWarranties.Hide(); 
                                    ASPxClientEdit.ClearEditorsInContainerById('divErrLinkWarr');
                                    $('#hidSelectedRowIds').val(chkItems.GetSelectedValues().join(','));
                                }}" />
                        </dxEditor:ASPxButton>
                        <dxEditor:ASPxButton ID="btnNoLinkSKUs" runat="server" ClientIDMode="Static" Text="Do not link to other SKUs" AutoPostBack="false">
                            <ClientSideEvents Click="CloseLinkWarrantiesPopup" />
                        </dxEditor:ASPxButton>
                    </div>
                </div>
            </div>
        </dxPopup:PopupControlContentControl>
    </ContentCollection>
</dxPopup:ASPxPopupControl>
