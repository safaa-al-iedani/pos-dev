﻿Imports System.Data.OracleClient
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors

Partial Class Usercontrols_TaxUpdate
    Inherits System.Web.UI.UserControl

    Private theSalesBiz As SalesBiz = New SalesBiz()

    Public Event TaxUpdated As EventHandler
    Public Delegate Sub EventHandler(sender As Object, e As EventArgs)

    ''' <summary>
    ''' Load the controls' data
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadTaxData()

        ViewState("TaxData") = Nothing

        gridTax.FilterExpression = String.Empty
        gridTax.PageIndex = 0
        gridTax.SettingsPager.PageSize = 10
        gridTax.DataBind()

    End Sub

    Private Function GetTaxData() As DataTable
        Dim dt As New DataTable

        If (Not IsNothing(ViewState("TaxData"))) Then
            dt = ViewState("TaxData")
        Else
            dt = GetTaxes()
            ViewState("TaxData") = dt
        End If

        Return dt
    End Function

    Private Function GetTaxes() As DataTable

        Dim ds As New DataSet

        'If the page was given a zip code, fetch tax codes based on that first
        If Not String.IsNullOrEmpty(hidZip.Value.ToString()) Then
            '** Implies that tax codes were successfully retrieved based on zip code
            ds = theSalesBiz.GetTaxCodesByZip(hidZip.Value.ToString())
        End If

        If hidZip.Value.Equals(String.Empty) Or Not (ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0) Then
            '*** implies that Tax codes are supposed to be retrieved based on:
            '***    1. current date - because zip code did not find any records  OR
            '***    2. date passed-in, uses that date if valid or the current date
            Dim taxDt As String = FormatDateTime(Now(), DateFormat.ShortDate)

            If Not String.IsNullOrEmpty(hidTaxDt.Value) AndAlso
               IsDate(hidTaxDt.Value.ToString()) Then

                taxDt = FormatDateTime(hidTaxDt.Value, DateFormat.ShortDate)
            End If

            ds = theSalesBiz.GetTaxCodesByDate(taxDt)
        End If

        Return ds.Tables(0)

    End Function

    ''' <summary>
    ''' Updates the temp item taxes
    ''' </summary>
    Private Sub UpdateTempItmTax()

        Dim taxability As String = ""
        Dim reader As OracleDataReader
        Dim conn As OracleConnection = SystemUtils.GetConn(SystemUtils.Connection_Constants.CONN_ERP)
        Dim cmd As OracleCommand = DisposablesManager.BuildOracleCommand("", conn)
        conn.Open()

        Dim sql As String = "UPDATE TEMP_ITM SET TAX_CD=NULL, TAX_PCT=0 WHERE SESSION_ID=:sessionID AND TAKE_WITH IS NULL"
        With cmd
            .CommandText = sql
            .Parameters.Add(":sessionID", OracleType.VarChar)
            .Parameters(":sessionID").Value = Session.SessionID.ToString.Trim
        End With
        cmd.ExecuteNonQuery()

        sql = "select row_id, itm_tp_cd, package_parent from temp_itm where session_id=:sessionID AND take_with IS NULL "
        cmd.Parameters.Clear()
        cmd.CommandText = sql
        cmd.Parameters.Add(":sessionID", OracleType.VarChar)
        cmd.Parameters(":sessionID").Value = Session.SessionID.ToString

        Try
            reader = DisposablesManager.BuildOracleDataReader(cmd)

            Do While reader.Read
                If (Session("TAX_CD") & "" <> "") Then
                    taxability = TaxUtils.Determine_Taxability(reader.Item("ITM_TP_CD").ToString, Date.Today, Session("TAX_CD"))
                End If
                If taxability = "Y" Then
                    If reader.Item("PACKAGE_PARENT").ToString & "" = "" Then
                        sql = "UPDATE TEMP_ITM SET TAXABLE_AMT=RET_PRC, TAX_AMT=ROUND((TAXABLE_AMT*(NVL(" & Session("TAX_RATE") & ",0)/100)), " &
                              TaxUtils.Tax_Constants.taxPrecision & "), TAX_CD='" & Session("TAX_CD") & "', TAX_PCT=" & Session("TAX_RATE") &
                              " WHERE ROW_ID='" & reader.Item("ROW_ID").ToString & "' AND TAKE_WITH IS NULL"
                    Else
                        sql = "UPDATE TEMP_ITM SET TAX_CD='" & Session("TAX_CD") & "', TAX_AMT=ROUND((TAXABLE_AMT*(NVL(" & Session("TAX_RATE") & ",0)/100)), " & TaxUtils.Tax_Constants.taxPrecision & "), TAX_PCT=" & Session("TAX_RATE") & " WHERE ROW_ID='" & reader.Item("ROW_ID").ToString & "' AND TAKE_WITH IS NULL"
                    End If
                    cmd = DisposablesManager.BuildOracleCommand(sql, conn)
                    cmd.ExecuteNonQuery()
                End If
            Loop
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Execcutes when the user clicks on an editable component(in this case the 'select' button) in the tax grid.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridTax_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles gridTax.RowCommand
        If e.CommandArgs.CommandName = "Select" Then
            '** The 'manual' tax cd represents the tax code, before the user consciously modifies the 
            '** tax code( via the tax icon button). Initialize it to nothing.
            Session("MANUAL_TAX_CD") = Nothing

            If Not String.IsNullOrEmpty(hidIsInvokeManually.Value) AndAlso hidIsInvokeManually.Value.ToString().ToLower().Equals("true") Then
                'cache the existing session tax-cd as the manual one, before it is reset
                Session("MANUAL_TAX_CD") = Session("TAX_CD")
            End If

            Dim index As Integer = Convert.ToInt32(e.CommandArgs.CommandArgument)

            hidTaxCd.Value = System.Web.HttpUtility.HtmlEncode(gridTax.GetRowValues(index, "TAX_CD").ToString())
            hidTaxDesc.Value = System.Web.HttpUtility.HtmlDecode(gridTax.GetRowValues(index, "DES").ToString())
            hidTaxRate.Value = System.Web.HttpUtility.HtmlEncode(gridTax.GetRowValues(index, "SUMOFPCT").ToString())
            hidTaxMethod1.Value = System.Web.HttpUtility.HtmlEncode(gridTax.GetRowValues(index, "TAX_METHOD").ToString())

            Session("TAX_CD") = hidTaxCd.Value
            Session("TAX_RATE") = hidTaxRate.Value

            If String.IsNullOrEmpty(hidDelDocNo.Value.ToString()) Then
                UpdateTempItmTax()
            End If

            '** get and set  the delivery tax 
            Session("DEL_TAX") = theSalesBiz.GetTaxPctForDelv(hidTaxCd.Value)

            '** get and set  the setup tax 
            Session("SU_TAX") = theSalesBiz.GetTaxPctForSetup(hidTaxCd.Value)

            RaiseEvent TaxUpdated(sender, e)
        End If
    End Sub

    Protected Sub gridTax_DataBinding(sender As Object, e As EventArgs) Handles gridTax.DataBinding
        gridTax.DataSource = GetTaxData()
    End Sub

    Protected Sub lnkClearFilter_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim lnk As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim container As GridViewFilterCellTemplateContainer = TryCast(lnk.NamingContainer, GridViewFilterCellTemplateContainer)

        lnk.Visible = Not String.IsNullOrEmpty(container.Grid.FilterExpression)
    End Sub
End Class
