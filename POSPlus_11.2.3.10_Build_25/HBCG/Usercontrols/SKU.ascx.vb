Imports DevExpress.Web.ASPxGridView
Imports System.Collections.Generic
Imports DevExpress.Web.ASPxEditors

Partial Class Usercontrols_SKU
    Inherits System.Web.UI.UserControl

    Private theSkuBiz As New SKUBiz()
    Public Event EventItemSelected As EventHandler
    Public Event EventHandleCancel As EventHandler

    ''' <summary>
    ''' Page load events
    ''' </summary>
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        hidIsSelfPostBack.Value = "N"
        btnAddSKU.Enabled = False
        txtSKU.Focus()
    End Sub

    Protected Sub SearchSKU(sender As Object, e As EventArgs) Handles txtSKU.TextChanged

        Dim dsSKUs As System.Data.DataSet = New DataSet()
        Dim items As ArrayList = New ArrayList()
        Dim dtItems As DataTable = New DataTable()
        btnAddSKU.Enabled = False
        hidIsSelfPostBack.Value = "Y"
        If Not txtSKU.Text.Trim() = String.Empty Then
            If ExistingSKU.Trim().ToUpper() = txtSKU.Text.Trim().ToUpper() Then
                'if both the keys are same, do not perform search!
                lblError.Text = "Please enter another SKU for search, you cant replace " + txtSKU.Text.ToUpper().Trim() + " with " + txtSKU.Text.ToUpper().Trim() + "!"
            Else
                items.Add(txtSKU.Text.Trim().ToUpper())
                dsSKUs = theSkuBiz.GetItemDetailsWithSortCodes(items)
                dtItems = dsSKUs.Tables(0).DefaultView.ToTable() 'False, "ITM_CD", "VE_CD", "VSN", "DES", "ITM_TP_CD", "SORTCODE", "RET_PRC")
                grdSKU.DataSource = dtItems
                grdSKU.DataBind()
                dsSKUs.Dispose()
                SearchedSKU = dtItems
                If dtItems.Rows.Count = 0 Then
                    lblError.Text = "No items found for the SKU:" + txtSKU.Text
                Else
                    lblError.Text = ""
                    If (theSkuBiz.IsSellable(txtSKU.Text)) Then
                        btnAddSKU.Enabled = True
                    Else
                        lblError.Text = "SKU : " + txtSKU.Text.Trim().ToUpper() + " is prevented from sale!<BR/>"
                    End If

                    If dtItems.Rows(0)("ITM_TP_CD").ToString = "PKG" Then
                        lblError.Text += "SKU : " + txtSKU.Text.Trim().ToUpper() + " is Package, Package SKU are not allowed!"
                        btnAddSKU.Enabled = False
                    Else
                        btnAddSKU.Enabled = True
                    End If
                End If
            End If
        Else
            lblError.Text = "Please Enter a SKU for search!"
        End If
    End Sub
    Public Function ClearSearchContents()
        txtSKU.Text = String.Empty
        grdSKU.DataSource = Nothing
        grdSKU.DataBind()
    End Function
    Protected Sub AddSKU(sender As Object, e As EventArgs) Handles btnAddSKU.Click
        SelectedSKU = SearchedSKU

        grdSKU.DataSource = Nothing
        grdSKU.DataBind()
        txtSKU.Text = ""
        RaiseEvent EventItemSelected(sender, e)
    End Sub

    Protected Sub CancelEvent(sender As Object, e As EventArgs) Handles btnClose.Click
        txtSKU.Text = ""
        grdSKU.DataSource = Nothing
        grdSKU.DataBind()
        RaiseEvent EventHandleCancel(sender, e)
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value> </value>
    ''' <returns> </returns>
    ''' <remarks></remarks>
    Private Property SearchedSKU() As DataTable
        Get
            Return If(Not ViewState("SEARCHEDSKU") Is Nothing, CType(ViewState("SEARCHEDSKU"), DataTable), Nothing)
        End Get
        Set(value As DataTable)
            ViewState("SEARCHEDSKU") = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value> </value>
    ''' <returns> </returns>
    ''' <remarks></remarks>
    Public Property SelectedSKU() As DataTable
        Get
            Return CType(ViewState("SELECTEDSKU"), DataTable)
        End Get
        Set(value As DataTable)
            ViewState("SELECTEDSKU") = value
        End Set
    End Property
    Private sourceSKU As String
    Public Property ExistingSKU() As String
        Set(value As String)
            ViewState("SOURCESKU") = value
        End Set
        Get
            Return If(Not ViewState("SOURCESKU") = Nothing, ViewState("SOURCESKU").ToString(), "")
        End Get
    End Property
    Public Property IsSelfPostBack() As String
        Set(value As String)

        End Set
        Get
            Return If(hidIsSelfPostBack.Value, "N")
        End Get
    End Property


    'hidIsSelfPostBack
End Class
