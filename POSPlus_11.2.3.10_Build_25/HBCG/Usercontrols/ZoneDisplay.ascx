﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ZoneDisplay.ascx.vb" Inherits="Usercontrols_ZoneDisplay" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<div style="width: 100%; text-align: left; vertical-align: top;">
    <asp:HiddenField ID="hidPincode" runat="server" />

    <dx:ASPxGridView ID="gridZone" runat="server" ClientInstanceName="gridZone"
        KeyFieldName="ZONE_CD" SettingsCookies-StorePaging="true" Width="98%"
        SettingsPager-AlwaysShowPager="true" SettingsPager-PageSizeItemSettings-Visible="true" ViewStateMode="Disabled"
        SettingsLoadingPanel-Mode="Disabled" SettingsLoadingPanel-ShowImage="False" AutoGenerateColumns="False">
        <SettingsBehavior AllowSort="true" AllowFocusedRow="true" AllowSelectSingleRowOnly="true" ProcessFocusedRowChangedOnServer="true" />

<SettingsPager AlwaysShowPager="True">
<PageSizeItemSettings Visible="True"></PageSizeItemSettings>
</SettingsPager>

        <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowHeaderFilterButton="false" UseFixedTableLayout="true" />

<SettingsLoadingPanel Mode="Disabled" ShowImage="False"></SettingsLoadingPanel>

        <Styles Header-HorizontalAlign="Center" AlternatingRow-Enabled="True" FilterCell-HorizontalAlign="Center">
<Header HorizontalAlign="Center"></Header>

<AlternatingRow Enabled="True"></AlternatingRow>

<FilterCell HorizontalAlign="Center"></FilterCell>
        </Styles>
        <Columns>
            <dx:GridViewDataColumn FieldName="ZONE_CD" Caption="Zone Code" SortOrder="Ascending"
                Settings-AutoFilterCondition="BeginsWith" Width="20%" VisibleIndex="1" />                        
            <dx:GridViewDataColumn FieldName="DES" Caption="Description"
                Settings-AutoFilterCondition="Contains" VisibleIndex="2" />             
            <dx:GridViewDataColumn FieldName="DELIVERY_STORE_CD" Caption="Store"
                Settings-AllowAutoFilter="False" Width="10%" VisibleIndex="3" /> 
            <dx:GridViewDataColumn FieldName="DEFAULT_DEL_CHG" Caption="Delivery"
                Settings-AllowAutoFilter="False" Width="10%" VisibleIndex="4" /> 
            <dx:GridViewDataColumn FieldName="DEFAULT_SETUP_CHG" Settings-AllowAutoFilter="False" Caption="Setup" VisibleIndex="5" Width="10%" />                                                              
           <dx:GridViewDataTextColumn Caption="Select" 
                HeaderStyle-Cursor="default" Width="15%" VisibleIndex="5">
                <CellStyle HorizontalAlign="Center"></CellStyle>
                <DataItemTemplate>
                    <dx:ASPxButton ID="btnSelect" runat="server" Text="Select" CommandName="Select" CommandArgument="<%# Container.ItemIndex %>"></dx:ASPxButton>
                </DataItemTemplate>
                <FilterTemplate>
                    <dx:ASPxHyperLink ID="lnkClearFilter" runat="server" Text="Clear Filter" NavigateUrl="javascript:void(0);" OnInit="lnkClearFilter_Init">
                        <ClientSideEvents Click="function(s, e) {gridZone.ClearFilter();}" />
                    </dx:ASPxHyperLink>
                </FilterTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
</div>
