﻿
Imports System.Collections.Generic
Imports DevExpress.Web.ASPxGridView

Partial Class Usercontrols_ZoneCode
    Inherits System.Web.UI.UserControl

    Private theTMBiz As TransportationBiz = New TransportationBiz()
    Private selectedValues As List(Of Object)
    Public Event ZoneUpdated As EventHandler
    Public Delegate Sub EventHandler(sender As Object, evt As ZoneInfo)

    Public Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If String.IsNullOrEmpty(hidZip.Value) AndAlso String.IsNullOrEmpty(txtzip.Text) Then
            Exit Sub
        End If
        Dim numRows As Integer = 0
        'To maintain state of new value manually entered in Zip code textbox
        If Not String.IsNullOrEmpty(hidZip.Value) AndAlso String.IsNullOrEmpty(txtzip.Text) Then
            numRows = UpdateZonesDisplay(hidZip.Value)
        ElseIf Not String.IsNullOrEmpty(txtzip.Text) Then
            numRows = UpdateZonesDisplay(txtzip.Text)
        End If
        UpdateZoneInfo(numRows, sender)
    End Sub
    Public Function UpdateZoneInfo(RowsCount As Integer, sender As Object) As Boolean
        If (RowsCount = 0) Then
            lblInfo.Text = "Sorry, no zones were found for the specified zip code.  Please re-enter"
            lblInfo.Visible = True
        ElseIf (RowsCount = 1) Then
            'ONE ZONE found
            Dim zoneInfo As ZoneInfo = New ZoneInfo()
            zoneInfo.ZoneCode = gridzonecode.GetRowValues(0, "ZONE_CD").ToString()
            zoneInfo.ZoneDescription = gridzonecode.GetRowValues(0, "DES").ToString()
            zoneInfo.DeliveryStoreCode = gridzonecode.GetRowValues(0, "DELIVERY_STORE_CD").ToString()
            zoneInfo.SetupCharges = gridzonecode.GetRowValues(0, "DEFAULT_SETUP_CHG").ToString()
            zoneInfo.Deliverycharges = gridzonecode.GetRowValues(0, "DEFAULT_DEL_CHG").ToString()
            zoneInfo.ZipCode = txtzip.Text
            RaiseEvent ZoneUpdated(sender, zoneInfo)
            lblInfo.Text = String.Empty
        Else
            'MULTIPLE ZONES found, therefore displays grid to force user to choose
            lblInfo.Text = Resources.POSMessages.MSG0003
        End If
        Return True
    End Function
    ''' <summary>
    ''' Retrieves the ZONES in the system, if no zones are found for the postal code passed in,
    ''' then ALL zones may be retrieved if ConfigurationManager.AppSettings("open_zones") is Y.
    ''' </summary>
    ''' <param name="postalCd">the postal code to retrieve ZONES for</param>
    ''' <returns>number of rows placed in the dsZones dataset</returns>
    Public Function UpdateZonesDisplay(ByVal postalCd As String) As Integer
        Dim numRows As Integer
        Try
            Dim dsZones As DataSet
            If hidSaleType IsNot Nothing AndAlso Not String.IsNullOrEmpty(hidSaleType.Value) Then
                'To get the filtered output for PickUp and Delivery Zone
                ' Add MCCL
                dsZones = theTMBiz.GetZonesFromZPFMForZip(postalCd, Session("CO_CD"), hidSaleType.Value)
	Else
		'Dim dsZones As DataSet = theTMBiz.GetZoneCodesForZip(postalCd)
            ' Daniela Jan 20 MCCL
		dsZones = theTMBiz.lucy_GetZoneCodesForZip(postalCd, Session("CO_CD"))
        End If
               
	    ' if NO zones are found, it may have to fetch them all
            If (dsZones.Tables(0).Rows.Count = 0 AndAlso
                ConfigurationManager.AppSettings("open_zones").ToString = "Y") Then
                ' Daniela Jan 20 MCCL
                dsZones = theTMBiz.GetZoneCodes(Session("emp_init"))
                'dsZones = theTMBiz.GetZoneCodes()
            End If
            numRows = dsZones.Tables(0).Rows.Count
            'dt = dsZones.Tables(0)
            gridzonecode.DataSource = dsZones.Tables(0).DefaultView()
            gridzonecode.DataBind()

        Catch ex As Exception
            Throw
        End Try
        Return numRows

    End Function
    ''' <summary>
    ''' when user clicks change zone,retrieves the ZONES in the system with entered zipcode
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub btnChangeZone_Click(sender As Object, e As EventArgs)
        If Not String.IsNullOrEmpty(txtzip.Text) Then
            'In this case textbox should have right zip code.
            hidZip.Value = txtzip.Text
            UpdateZonesDisplay(txtzip.Text)
        End If
    End Sub
    ''' <summary>
    ''' when user selects the zone,The selected row values are stored in the custom event arguments
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub gridzonecode_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles gridzonecode.RowCommand
        If e.CommandArgs.CommandName = "Select" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgs.CommandArgument)
            Dim fieldValues As List(Of Object) = New List(Of Object)
            Dim zoneInfo As ZoneInfo = New ZoneInfo()
            zoneInfo.ZoneCode = gridzonecode.GetRowValues(index, "ZONE_CD").ToString()
            zoneInfo.ZoneDescription = gridzonecode.GetRowValues(index, "DES").ToString()
            zoneInfo.DeliveryStoreCode = gridzonecode.GetRowValues(index, "DELIVERY_STORE_CD").ToString()
            zoneInfo.SetupCharges = gridzonecode.GetRowValues(index, "DEFAULT_SETUP_CHG").ToString()
            zoneInfo.Deliverycharges = gridzonecode.GetRowValues(index, "DEFAULT_DEL_CHG").ToString()
            zoneInfo.ZipCode = txtzip.Text
            RaiseEvent ZoneUpdated(sender, zoneInfo)
        End If
    End Sub

End Class

