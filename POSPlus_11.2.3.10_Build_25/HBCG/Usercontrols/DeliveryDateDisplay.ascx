﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DeliveryDateDisplay.ascx.vb"
    Inherits="Usercontrols_DeliveryDateDisplay" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:HiddenField ID="hidZone" runat="server" />

<dx:ASPxGridView ID="griddeldate" runat="server" AutoGenerateColumns="False" KeyFieldName="DEL_DT"  
    SettingsBehavior-AllowFocusedRow="true" OnHtmlDataCellPrepared="griddeldate_HtmlDataCellPrepared" OnRowCommand="griddeldate_RowCommand"
    Width="473px" Font-Size="XX-Small" Settings-ShowFilterRow="true"  OnAutoFilterCellEditorCreate="griddeldate_AutoFilterCellEditorCreate">
    <Columns>
        <dx:GridViewDataDateColumn FieldName="DEL_DT" VisibleIndex="1" Caption="<%$ Resources:SalesExchange, Label71 %>" PropertiesDateEdit-DisplayFormatString="MM/dd/yyyy">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataTextColumn FieldName="DAY" Caption="<%$ Resources:SalesExchange, Label72 %>" VisibleIndex="2">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="DEL_STOPS" Caption="<%$ Resources:SalesExchange, Label73 %>" VisibleIndex="3">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="ACTUAL_STOPS" Caption="<%$ Resources:SalesExchange, Label74 %>" VisibleIndex="4">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="REMAINING_STOPS" Caption="<%$ Resources:SalesExchange, Label76 %>" VisibleIndex="5">
            <CellStyle Font-Size="X-Small"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Select" Settings-AllowSort="False"
            HeaderStyle-Cursor="default" Width="15%" VisibleIndex="6">
            <CellStyle HorizontalAlign="Center"></CellStyle>
            <DataItemTemplate>
                <dx:ASPxButton ID="btnSelect" runat="server" Text="Select" CommandName="Select"
                    CommandArgument="<%# Container.ItemIndex %>">
                </dx:ASPxButton>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        <dx:GridViewCommandColumn ShowClearFilterButton="true"></dx:GridViewCommandColumn>
    </Columns>
        <SettingsPager PageSize="10" PageSizeItemSettings-Caption="<%$ Resources:SalesExchange, Label75 %>">
        <PageSizeItemSettings Items="10, 20,30,40" Visible="True">
        </PageSizeItemSettings>
    </SettingsPager>
    <Styles>
        <AlternatingRow Enabled="true" BackColor="Beige" />
    </Styles>
</dx:ASPxGridView>
