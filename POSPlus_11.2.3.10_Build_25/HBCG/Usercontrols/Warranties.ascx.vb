﻿Imports System.Data.OracleClient

Partial Class Usercontrols_Warranties
    Inherits System.Web.UI.UserControl

    Protected Const sameWarrSeparator As String = "%"

    Public Event WarrantyAdded As EventHandler
    Public Event NoWarrantyAdded As EventHandler

    Public Delegate Sub EventHandler(sender As Object, e As EventArgs)

    Public Sub LoadWarranties()

        If String.IsNullOrEmpty(hidSkus.Value) Then 'If skus & "" = "" Then

            lblExtWarr.Text = "No Warranty SKUS were found for this order."
            lblVSN.Visible = False
            lblCurrWarr.Visible = False
            rdoAddWarr.Visible = False
            btnAddWarr.Visible = False
            rdoPrevWarrList.Visible = False
            lblExtWarr2.Visible = False
            btnNoThanks.Text = "Close Window"

        Else
            BindData()
        End If

    End Sub

    Public Sub BindData()

        Dim datSet As DataSet = New DataSet
        ' if there are multiple SKUs in the input, do as before for now
        ' TODO DSA - handle multiple SKUs; assuming one for now

        If hidSkus.Value.Contains(",") Then 'If skus.Contains(",") Then
            Dim tmpNum = 20 / 0    ' force an error for now if multiples  ' TODO - need to resolve multiple in input - really can only get warr for one SKU at a time

        Else
            Dim req4ExtWarr As SalesUtils.GetCalRetPrcReq = New SalesUtils.GetCalRetPrcReq
            req4ExtWarr.itmCd = hidSkus.Value
            req4ExtWarr.cashFlg = "N"
            req4ExtWarr.itmRetPrc = Nothing
            If (Not IsNothing(Session("tran_dt"))) AndAlso SystemUtils.isNotEmpty(Session("tran_dt")) AndAlso IsDate(Session("tran_dt")) Then  ' TODO DSA - test the nothings
                req4ExtWarr.effDt = Session("tran_dt")
            Else
                req4ExtWarr.effDt = FormatDateTime(DateTime.Today, DateFormat.ShortDate)
            End If
            If (Not IsNothing(Session("store_cd"))) AndAlso SystemUtils.isNotEmpty(Session("store_cd")) Then
                req4ExtWarr.storeCd = Session("store_cd")
            Else
                req4ExtWarr.storeCd = ""
            End If
            If (Not IsNothing(Session("cust_tp_prc_cd"))) AndAlso SystemUtils.isNotEmpty(Session("cust_tp_prc_cd")) Then
                req4ExtWarr.custTpPrcCd = Session("cust_tp_prc_cd")
            Else
                req4ExtWarr.custTpPrcCd = ""
            End If
            datSet = SalesUtils.GetItmExtWarrs(req4ExtWarr)

            If SysPms.isOne2ManyWarr Then

                Dim prevWarrDatSet As DataSet = New DataSet

                prevWarrDatSet = GetTempWarrExtWarrs()
                Session("PrevWarrList") = prevWarrDatSet

                If SystemUtils.dataSetHasRows(prevWarrDatSet) AndAlso SystemUtils.dataRowIsNotEmpty(prevWarrDatSet.Tables(0).Rows(0)) Then

                    rdoPrevWarrList.DataSource = prevWarrDatSet
                    rdoPrevWarrList.DataValueField = "war_itm_cd"
                    rdoPrevWarrList.DataTextField = "des"
                    rdoPrevWarrList.DataBind()
                    rdoPrevWarrList.Visible = True
                    lblExtWarr2.Visible = True

                Else
                    rdoPrevWarrList.Visible = False
                    lblExtWarr2.Visible = False
                End If
            Else
                rdoPrevWarrList.Visible = False
                lblExtWarr2.Visible = False
            End If
        End If

        rdoAddWarr.DataSource = datSet
        rdoAddWarr.DataValueField = "WAR_ITM_CD"
        rdoAddWarr.DataTextField = "DES"
        rdoAddWarr.DataBind()

        lblCurrWarr.Text = String.Empty

        If datSet.ToString & "" <> "" Then
            ' can only get and relate 1 warranty at a time, even if can relate to multiple
            Dim manuWarrs As DataTable = SalesUtils.GetItmManuWarrs(hidSkus.Value).Tables(0)

            lblVSN.Text = ""  ' need to clear so it updates text for last SKU in a multiple list which is going to be the data that is displayed
            For Each mWarr As DataRow In manuWarrs.Rows
                If lblVSN.Text & "" = "" Then
                    lblVSN.Text = "The item you have selected, " & mWarr.Item("VSN").ToString & ", comes with the following warranties:"
                End If
                lblCurrWarr.Text = lblCurrWarr.Text & "<li>" & mWarr.Item("DES").ToString & "</li>" & vbCrLf
            Next
            lblCurrWarr.Text = "<ul>" & lblCurrWarr.Text & "</ul>"
        End If
    End Sub

    ''' <summary>
    ''' Extracts temp_warr table entries for the existing temp_itm warranty records that can be applied to the SKU
    ''' </summary>
    Private Function GetTempWarrExtWarrs() As DataSet

        Dim warr As New DataSet
        Dim sql As New StringBuilder
        ' add ln_num to itm_cd to make same SKU different in usage, add % to be able to strip off the trailer and get the SKU
        sql.Append("SELECT itm_cd || '" + sameWarrSeparator + "' || ln_num AS war_itm_cd, TO_CHAR(ret_prc, '$9999.99') || ' -- ' || vsn || ' (' || days || ' DAYS)' AS des, ln_num ").Append(
                   "FROM temp_warr tw ").Append(
                   "WHERE session_id = :sessId ORDER BY days ")
        Try
            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                cmd As New OracleCommand(UCase(sql.ToString), conn), oAdp As OracleDataAdapter = DisposablesManager.BuildOracleDataAdapter(cmd)

                cmd.Parameters.Add(":sessId", OracleType.VarChar)
                If String.IsNullOrEmpty(hidDelDocNum.Value) Then
                    'If "".Equals(Request("referrer") & "") Then
                    cmd.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                Else
                    cmd.Parameters(":sessId").Value = hidDelDocNum.Value
                End If

                oAdp.Fill(warr)
            End Using
        Finally
        End Try

        Return warr
    End Function

    ''' <summary>
    ''' Deletes temp_warr table entries before exit
    ''' </summary>
    Private Sub DeleteTempWarr()

        Dim sql As New StringBuilder
        sql.Append("DELETE FROM temp_warr WHERE session_id = :sessId ")

        Try
            Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP), _
                cmd As New OracleCommand(UCase(sql.ToString), conn)

                conn.Open()

                cmd.Parameters.Add(":sessId", OracleType.VarChar)
                'cmd.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                If String.IsNullOrEmpty(hidDelDocNum.Value) Then
                    'If "".Equals(Request("referrer") & "") Then
                    cmd.Parameters(":sessId").Value = Session.SessionID.ToString.Trim
                Else
                    cmd.Parameters(":sessId").Value = hidDelDocNum.Value
                End If

                cmd.ExecuteNonQuery()
            End Using
        Finally
        End Try
    End Sub

    Protected Sub rdoAddWarr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoAddWarr.SelectedIndexChanged
        If rdoPrevWarrList.SelectedValue.ToString() & "" <> "" Then
            rdoPrevWarrList.ClearSelection()
        End If
    End Sub

    Protected Sub rdoPrevWarrList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoPrevWarrList.SelectedIndexChanged
        If rdoAddWarr.SelectedValue.ToString() & "" <> "" Then
            rdoAddWarr.ClearSelection()
        End If
    End Sub

    Protected Sub btnAddWarr_Click(sender As Object, e As EventArgs) Handles btnAddWarr.Click
        ' button to select a warranty
        lblError.Text = ""

        If rdoAddWarr.SelectedValue.ToString() & "" = "" AndAlso rdoPrevWarrList.SelectedValue.ToString() & "" = "" Then
            lblError.Text = "You must select a valid warranty"
            Exit Sub
        End If

        Dim selWarr As String = ""
        Dim warrItmRowNum As String = ""
        Dim selIndx As Integer = 0

        If rdoAddWarr.SelectedValue.ToString() & "" <> "" Then
            selWarr = rdoAddWarr.SelectedValue.ToString()
            selIndx = rdoAddWarr.SelectedIndex

        ElseIf SysPms.isOne2ManyWarr Then  'If PrevWarrList.SelectedValue.ToString() & "" <> "" Then ' if not, logic problem

            selWarr = rdoPrevWarrList.SelectedValue.ToString()
            selWarr = selWarr.Substring(0, selWarr.IndexOf(sameWarrSeparator))
            selIndx = rdoPrevWarrList.SelectedIndex
            Dim warrSet As DataSet = Session("PrevWarrList")
            Dim warrTbl As DataTable = warrSet.Tables(0)
            Dim warrCnt As Integer = warrTbl.Rows.Count
            Dim warrIndx As Integer = 0
            Dim lnNum As Integer = 0

            Do While warrIndx <= (warrCnt - 1) AndAlso warrIndx <= selIndx
                lnNum = warrTbl.Rows(warrIndx)("ln_num")
                warrIndx = warrIndx + 1
            Loop

            If warrIndx - 1 = selIndx Then
                warrItmRowNum = lnNum.ToString
            End If
        End If

        hidWarrSku.Value = selWarr
        hidWarrRowNo.Value = warrItmRowNum

        DeleteTempWarr()

        RaiseEvent WarrantyAdded(sender, e)
        popupWarranties.ShowOnPageLoad = False

    End Sub

    Protected Sub btnNoThanks_Click(sender As Object, e As EventArgs) Handles btnNoThanks.Click

        hidWarrSku.Value = String.Empty
        hidWarrRowNo.Value = String.Empty

        DeleteTempWarr()

        RaiseEvent NoWarrantyAdded(sender, e)
        popupWarranties.ShowOnPageLoad = False
    End Sub

End Class
