﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SummaryPriceChangeApprovalNew.ascx.vb" Inherits="Usercontrols_SummaryPriceChangeApprovalNew" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Src="~/Usercontrols/ManagerOverride.ascx" TagPrefix="uc1" TagName="ManagerOverride" %>

<dx:ASPxPopupControl ID="popupPriceChangeAppr" runat="server" ClientInstanceName="popupPriceChangeAppr" Modal="true" AllowDragging="true"
    HeaderText="Price Change Approval" ShowCloseButton="false" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" 
    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="true" Width="550px" CloseAction="None">
    <HeaderStyle HorizontalAlign="Center" Font-Bold="True"></HeaderStyle>
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <table style="width: 100%;">
                
                <tr>
                    <td>
                        <asp:Label ID ="lblMessage" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:ManagerOverride runat="server" ID="ucManagerOverride" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <div style="overflow-y: auto; max-height: 200px; margin-bottom: 8px;">
                            <asp:Repeater ID="rptrLines" runat="server">
                                <HeaderTemplate>
                                    <table style="width: 100%; text-align: left; border: 1px solid #eaf1fa;" cellspacing="0">
                                        <tr>
                                            <th style="background-color: #eaf1fa;">
                                                <label>SKU</label></th>
                                            <th style="background-color: #eaf1fa;">
                                                <label>Description</label></th>
                                            <th style="background-color: #eaf1fa;">
                                                <label>Qty</label></th>
                                            <th style="background-color: #eaf1fa;">
                                                <label>Orig. Price</label></th>
                                            <th style="background-color: #eaf1fa;">
                                                <label>New Price</label></th>
                                            <th style="background-color: #eaf1fa;">
                                                <label>Ext. Price</label></th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: left;">
                                            <asp:Label ID="lbl_sku" runat="server" Text=<%# Eval("ITM_CD")%>></asp:Label>
                                        </td>
                                        <td style="text-align: left;">
                                            <label><%# Eval("DES")%></label></td>
                                        <td style="text-align: right;">
                                            <label><%# Eval("QTY")%></label></td>
                                        <td style="text-align: right;">
                                           <asp:Label ID="lbl_origPrc" runat="server" Text=<%# String.Format("{0:c}", Eval("ORIG_PRC"))%>></asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                           
                                             <asp:Label ID="lbl_unitPrc" runat="server" Text=<%# String.Format("{0:c}", Eval("UNIT_PRC"))%>></asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <label><%# String.Format("{0:c}", Decimal.Multiply(CDec(Eval("UNIT_PRC")), CDec(Eval("QTY"))))%></label>
                                        </td>
                                        <td style="text-align: right; color:red">
                                          
                                            <asp:Label ID="lbl_max" runat="server" Text=<%# Eval("STYLE_CD")%>></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr>
                                        <td style="text-align: left; background-color: aliceblue;">
                                             <asp:Label ID="lbl_sku" runat="server" Text=<%# Eval("ITM_CD")%>></asp:Label></td>
                                        <td style="text-align: left; background-color: aliceblue;">
                                            <label><%# Eval("DES")%></label></td>
                                        <td style="text-align: right; background-color: aliceblue;">
                                            <label><%# Eval("QTY")%></label></td>
                                        <td style="text-align: right; background-color: aliceblue;">
                                            <asp:Label ID="lbl_origPrc" runat="server" Text=<%# String.Format("{0:c}", Eval("ORIG_PRC"))%>></asp:Label>

                                        </td>
                                        <td style="text-align: right; background-color: aliceblue;">
                                             <asp:Label ID="lbl_unitPrc" runat="server" Text=<%# String.Format("{0:c}", Eval("UNIT_PRC"))%>></asp:Label>

                                        </td>
                                        <td style="text-align: right; background-color: aliceblue;">
                                            <label><%# String.Format("{0:c}", Decimal.Multiply(CDec(Eval("UNIT_PRC")), CDec(Eval("QTY"))))%></label></td>
                                        <td style="text-align: right; color:red">
                                          <asp:Label ID="lbl_max" runat="server" Text=<%# Eval("STYLE_CD")%>></asp:Label>

                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; background-color: #eaf1fa;">
                        <label>Total Price Change:</label>
                        <dx:ASPxLabel ID="lblTotPrChangeAmt" runat="server" Font-Bold="true" Style="padding-left: 40px; padding-right: 2px;"></dx:ASPxLabel>
                    </td>
                </tr>
            </table>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
