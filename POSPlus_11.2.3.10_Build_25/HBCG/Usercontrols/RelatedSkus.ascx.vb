﻿Imports System.Data.OracleClient
Imports System.Web.UI.ClientScriptManager

Partial Class Usercontrols_RelatedSkus
    Inherits System.Web.UI.UserControl

    Public Event AddSelection As EventHandler
    Public Event NoneAdded As EventHandler
    Public Delegate Sub EventHandler(sender As Object, e As EventArgs, selectedRelSKUs As DataTable)

    Private theSkuBiz As SKUBiz = New SKUBiz()
    Private availableRelatedSKUs As New DataTable

    ''' <summary>
    ''' Will load this controls' data
    ''' </summary>
    ''' <param name="relatedSKUs">comma delimited list of SKUs</param>
    ''' <remarks>Needs to call this method just after this control initiates</remarks>
    Public Sub LoadRelatedSKUs(ByVal relatedSKUs As DataTable)
        availableRelatedSKUs = relatedSKUs
        lblInfo.Text = "The following items compliment your selection.  Would you like to add them to your order?"
        Bindgrid()
    End Sub

    ''' <summary>
    ''' Populates the grid to display the ALL the available Related SKUs to the user.
    ''' NOTE: for a Package SKU or a group of SKUs (when entered comma delimited or thru the
    '''       search mechanism), this represents related SKUs for all the SKUs, to display
    '''       the window ONCE per SKU(s) entry, rather than ONCE per SKU
    ''' </summary>
    Private Sub Bindgrid()

        ' Updates the Retail Price, by calling the Pricing API, so that correct pricing is displayed/used
        Dim retPrice As Double
        For Each dRow As DataRow In availableRelatedSKUs.Rows
            retPrice = If(IsNumeric(dRow("RET_PRC")), CDbl(dRow("RET_PRC")), 0)

            retPrice = theSkuBiz.GetCurrentPricing(SessVar.homeStoreCd,
                                                   SessVar.wrStoreCd,
                                                   SessVar.custTpPrcCd,
                                                   Session("tran_dt"),
                                                   dRow("REL_SKU"),
                                                   retPrice)
            dRow("RET_PRC") = retPrice
        Next

        grdRelatedSkus.DataSource = availableRelatedSKUs
        grdRelatedSkus.Visible = True
        grdRelatedSkus.DataBind()
    End Sub


    Protected Sub btnAddSku_Click(sender As Object, e As EventArgs) Handles btnAddSku.Click

        'creates an empty table, that will be populated with all selected SKUs from the grid
        Dim selectedRelatedSKUs As DataTable = availableRelatedSKUs.Clone()
        Dim strWhereClause As String
        Dim cBoxComponent As CheckBox

        ' --- finds out if the user checked any Related SKUs that need to be added
        For Each dgItem As DataGridItem In grdRelatedSkus.Items
            cBoxComponent = dgItem.FindControl("SelectThis")
            If cBoxComponent.Checked Then
                strWhereClause = ("REL_SKU = '" & dgItem.Cells(1).Text().Trim)
                selectedRelatedSKUs.ImportRow(availableRelatedSKUs.Select(strWhereClause)(0))
            End If
        Next

        popupRelatedSKUs.ShowOnPageLoad = False
        If (selectedRelatedSKUs.Rows.Count > 0) Then
            RaiseEvent AddSelection(sender, e, selectedRelatedSKUs)
        Else
            RaiseEvent NoneAdded(sender, e, Nothing)
        End If
    End Sub


    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        popupRelatedSKUs.ShowOnPageLoad = False
        RaiseEvent NoneAdded(sender, e, Nothing)
    End Sub


End Class
