﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SummaryPriceChangeApprovalWithComment.ascx.vb" Inherits="Usercontrols_SummaryPriceChangeApprovalWithComment" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%--<%@ Register Src="~/Usercontrols/ManagerOverride.ascx" TagPrefix="uc1" TagName="ManagerOverride" %>--%>

<dx:ASPxPopupControl ID="popupPriceChangeAppr" runat="server" ClientInstanceName="popupPriceChangeAppr" Modal="true" AllowDragging="true"
    HeaderText="<%$ Resources:LibResources, Label1027 %>" ShowCloseButton="false" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" 
    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"  DefaultButton="btnSubmit" ShowPageScrollbarWhenModal="true" Width="550px" CloseAction="None">
    <HeaderStyle HorizontalAlign="Center" Font-Bold="True"></HeaderStyle>
    <ContentCollection>
        
        <dx:PopupControlContentControl runat="server">
            <table style="width: 100%;">
                
               <%-- <tr>
                    <td>
                        <asp:Label ID ="lblMessage" runat="server" Visible="false" ForeColor="Red" />
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        <%--<uc1:ManagerOverride runat="server" ID="ucManagerOverride" />--%>
                         <table style="margin: auto;">
                            <tr>
                                <td style="text-align: center;">
                                    <img style="padding-right: 20px;" alt="Stop" src="images/icons/Symbol-Stop.png" />
                                </td>
                                <td style="white-space: nowrap;">
                                    <table style="margin: auto;">
                                       
                                        <tr>
                                            <td style="text-align: right; width: 100px;">
                                            </td>
                                            <td style="width: 175px;">
                                                <asp:Label ID="lblErrMgrOverride" runat="server" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right; width: 100px;">
                                               <asp:Label ID ="lblpassword" runat="server" Text="<%$ Resources:LibResources, Label383 %>" />
                                            </td>
                                            <td style="width: 175px;">
                                                <dx:ASPxTextBox ID="txtMgrPwd" runat="server" Password="true" onkeydown = "return (event.keyCode!=13);" ></dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <span id="spnErrMgrPwd" runat="server" visible="false" style="color: red;">*</span></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right; width: 100px;">
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnSubmit" runat="server" Text="<%$ Resources:LibResources, Label975 %>" ClientInstanceName="btnSubmit" UseSubmitBehavior="true">
                                                </dx:ASPxButton>  &nbsp &nbsp
                                                    <dx:ASPxButton ID="btnCancel" runat="server" Text="<%$ Resources:LibResources, Label55 %>" ClientInstanceName="btnCancel" UseSubmitBehavior="false">
                                                </dx:ASPxButton>
                                            </td>
                                             <td>
                                            </td>
                                           
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
               <%-- Alice added for request 294 -start--%>
                <tr>
                    <td style="text-align: left;">
                         <table style="margin: auto;">
                            <tr>
                                <td >
                                    <asp:Label ID="lblcomment" runat="server" Text="<%$ Resources:LibResources, Label95 %>" ></asp:Label>
                                </td>
                                <td>
                                      <asp:TextBox ID="txtCommentL1" runat="server" Width ="430px" MaxLength ="55" onkeydown = "return (event.keyCode!=13);" ></asp:TextBox>
                                </td>
                                </tr>
                             <tr>
                                <td >
                                    
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCommentL2" runat="server" Width ="430px" MaxLength ="55" onkeydown = "return (event.keyCode!=13);" ></asp:TextBox>
                                </td>
                                </tr>
                             <tr>
                                <td >
                                    
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCommentL3" runat="server" Width ="430px" MaxLength ="55" onkeydown = "return (event.keyCode!=13);" ></asp:TextBox>
                                </td>
                                </tr>
                          </table>
                       
                      
                         
                         
                    </td>
                </tr>
               <%--request 294 end--%>
                <tr>
                    <td style="text-align: center;">
                        <div style="overflow-y: auto; max-height: 200px; margin-bottom: 8px;">
                            <asp:Repeater ID="rptrLines" runat="server">
                                <HeaderTemplate>
                                    <table style="width: 100%; text-align: left; border: 1px solid #eaf1fa;" cellspacing="0">
                                        <tr>
                                            <th style="background-color: #eaf1fa;">
                                                 <asp:Label ID="lblsku" runat="server" Text="<%$ Resources:LibResources, Label550 %>" ></asp:Label></th>
                                            <th style="background-color: #eaf1fa;">
                                                 <asp:Label ID="lbldesc" runat="server" Text="<%$ Resources:LibResources, Label158 %>" ></asp:Label></th>
                                            <th style="background-color: #eaf1fa;">
                                                 <asp:Label ID="lblqty" runat="server" Text="<%$ Resources:LibResources, Label447 %>" ></asp:Label></th>
                                            <th style="background-color: #eaf1fa;">
                                                 <asp:Label ID="lblOrigPrice" runat="server" Text="<%$ Resources:LibResources, Label1028 %>" ></asp:Label></th>
                                            <th style="background-color: #eaf1fa;">
                                                 <asp:Label ID="lblnewPrice" runat="server" Text="<%$ Resources:LibResources, Label1029 %>" ></asp:Label></th>
                                            <th style="background-color: #eaf1fa;">
                                                 <asp:Label ID="lblextPrice" runat="server" Text="<%$ Resources:LibResources, Label1030 %>" ></asp:Label></th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: left;">
                                            <asp:Label ID="lbl_sku" runat="server" Text=<%# Eval("ITM_CD")%>></asp:Label>
                                        </td>
                                        <td style="text-align: left;">
                                            <label><%# Eval("DES")%></label></td>
                                        <td style="text-align: right;">
                                            <label><%# Eval("QTY")%></label></td>
                                        <td style="text-align: right;">
                                           <asp:Label ID="lbl_origPrc" runat="server" Text=<%# String.Format("{0:c}", Eval("ORIG_PRC"))%>></asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                           
                                             <asp:Label ID="lbl_unitPrc" runat="server" Text=<%# String.Format("{0:c}", Eval("UNIT_PRC"))%>></asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <label><%# String.Format("{0:c}", Decimal.Multiply(CDec(Eval("UNIT_PRC")), CDec(Eval("QTY"))))%></label>
                                        </td>
                                        <td style="text-align: right; color:red">
                                          
                                            <asp:Label ID="lbl_max" runat="server" Text=<%# Eval("STYLE_CD")%>></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr>
                                        <td style="text-align: left; background-color: aliceblue;">
                                             <asp:Label ID="lbl_sku" runat="server" Text=<%# Eval("ITM_CD")%>></asp:Label></td>
                                        <td style="text-align: left; background-color: aliceblue;">
                                            <label><%# Eval("DES")%></label></td>
                                        <td style="text-align: right; background-color: aliceblue;">
                                            <label><%# Eval("QTY")%></label></td>
                                        <td style="text-align: right; background-color: aliceblue;">
                                            <asp:Label ID="lbl_origPrc" runat="server" Text=<%# String.Format("{0:c}", Eval("ORIG_PRC"))%>></asp:Label>

                                        </td>
                                        <td style="text-align: right; background-color: aliceblue;">
                                             <asp:Label ID="lbl_unitPrc" runat="server" Text=<%# String.Format("{0:c}", Eval("UNIT_PRC"))%>></asp:Label>

                                        </td>
                                        <td style="text-align: right; background-color: aliceblue;">
                                            <label><%# String.Format("{0:c}", Decimal.Multiply(CDec(Eval("UNIT_PRC")), CDec(Eval("QTY"))))%></label></td>
                                        <td style="text-align: right; color:red">
                                          <asp:Label ID="lbl_max" runat="server" Text=<%# Eval("STYLE_CD")%>></asp:Label>

                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; background-color: #eaf1fa;">
                         <asp:Label ID="lbltotalPrice" runat="server" Text="<%$ Resources:LibResources, Label1031 %>" ></asp:Label>
                        <dx:ASPxLabel ID="lblTotPrChangeAmt" runat="server" Font-Bold="true" Style="padding-left: 40px; padding-right: 2px;"></dx:ASPxLabel>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hidEffDt" runat="server" />
    <asp:HiddenField ID="hidSecCd" runat="server" />
    <asp:HiddenField ID="hidAppEmpCd" runat="server" />
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
