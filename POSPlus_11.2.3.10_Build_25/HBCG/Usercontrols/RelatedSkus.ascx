﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RelatedSkus.ascx.vb" Inherits="Usercontrols_RelatedSkus" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<style type="text/css">
    body {
        font-family: Tahoma, Arial, Helvetica;
        font-size: 11px;
    }

    td {
        font-family: Tahoma, Arial, Helvetica;
        font-size: 11px;
        line-height: 17px;
        color: #000000;
    }

    input {
        font-family: Tahoma, Arial, Helvetica;
        font-size: 11px;
        line-height: 17px;
    }

    select {
        font-family: Tahoma, Arial, Helvetica;
        font-size: 11px;
        line-height: 17px;
        color: #000000;
    }
</style>
<dx:ASPxPopupControl ID="popupRelatedSKUs" runat="server" ClientInstanceName="popupRelatedSKUs" Modal="true" CloseAction="CloseButton" AllowDragging="true"
    HeaderText="Related SKUS" HeaderStyle-Font-Bold="false" HeaderStyle-HorizontalAlign="Left" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    ShowPageScrollbarWhenModal="true" Width="679px">
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentRelatedSKUS" runat="server">
            <div>
                <table>
                    <tr>
                        <td valign="middle" align="center">
                            <img src="images/icons/Symbol-Information.png" style="width: 128px; height: 128px" />
                        </td>
                        <td>&nbsp;&nbsp;</td>
                        <td valign="middle" align="center">
                            <asp:Label ID="lblInfo" runat="server" Style="position: relative" Text="" Width="304px"></asp:Label>
                            <br />
                            <br />
                            <asp:DataGrid ID="grdRelatedSkus" runat="server" Height="155px" Style="position: relative; left: 0px; top: 0px;"
                                Width="479px" AutoGenerateColumns="False"
                                DataKeyField="REL_SKU" ShowHeader="False" BorderColor="White" GridLines="None">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <font face="Webdings" size="4">a</font>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="SelectThis" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="REL_SKU" HeaderText="Related SKU" ReadOnly="True">
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="VSN" HeaderText="VSN" ReadOnly="True">
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="DES" HeaderText="DESC" ReadOnly="True">
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="RET_PRC" HeaderText="Price" ReadOnly="True" DataFormatString="{0:C}">
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ITM_TP_CD" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="VE_CD" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="MNR_CD" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="CAT_CD" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="STYLE_CD" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="COMM_CD" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="REPL_CST" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="SPIFF" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="SIZ" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="FINISH" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="COVER" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="GRADE" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="POINT_SIZE" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="TREATABLE" Visible="false" ReadOnly="True" />
                                    <asp:BoundColumn DataField="WARRANTABLE" Visible="false" ReadOnly="True" />
                                </Columns>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:DataGrid>
                            <br />
                            <asp:Button ID="btnAddSku" runat="server" Text="Add to Order" />
                            <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:LibResources, Label350 %>" />
                        </td>
                    </tr>
                </table>
            </div>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
