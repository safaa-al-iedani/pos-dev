﻿
Imports System.Data.OracleClient
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridView

Partial Class Usercontrols_DeliveryDateDisplay
    Inherits System.Web.UI.UserControl
    Dim pt_cnt As String
    Private theTMBiz As TransportationBiz = New TransportationBiz()
    Public Event DeliveryDateSelected As EventHandler
    Dim MaxDate As DateTime
    Dim MinDate As DateTime
    Public Delegate Sub EventHandler(sender As Object, Selecteddate As Date)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not String.IsNullOrEmpty(hidZone.Value) Then
            GetDeliveryDates("")
        End If
    End Sub
    ''' <summary>
    ''' Get delivery date available calculated based on the stops
    ''' </summary>
    ''' <param name="deldocnum"></param>
    Public Sub GetDeliveryDates(ByVal deldocnum As String)
        Dim sql As String
        Dim objSql As OracleCommand
        ' Dim MyDataReader As OracleDataReader
        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter
        Dim dv As DataView
        Dim CurrentDate As DateTime = DateTime.Today

        Using conn As OracleConnection = SystemLibrary.DbConnectionUtils.GetConn(SystemLibrary.DbConnectionUtils.Connection_Constants.CONN_ERP)
            conn.Open()
            If SysPms.isDelvByPoints And Not String.IsNullOrEmpty(deldocnum) Then
                sql = "SELECT SUM(nvl(DELIVERY_POINTS,0) * QTY) As Point_CNT FROM SO_LN WHERE DEL_DOC_NUM='" & deldocnum & "' AND VOID_FLAG='N'"
                objSql = DisposablesManager.BuildOracleCommand(sql, conn)
                Try
                    Using MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

                        'Store Values in String Variables 
                        If (MyDataReader.HasRows) Then
                            MyDataReader.Read()
                            pt_cnt = MyDataReader.Item("Point_CNT").ToString
                        Else
                            pt_cnt = "0"
                        End If

                        MyDataReader.Close()
                    End Using
                Catch ex As Exception
                    conn.Close()
                    Throw
                End Try
            Else
                pt_cnt = 1
            End If

            sql = "SELECT DEL_DT, DAY, DEL_STOPS, ACTUAL_STOPS, CLOSED, (DEL_STOPS - ACTUAL_STOPS) AS REMAINING_STOPS,EXMPT_DEL_CD "
            sql = sql & "FROM DEL "
            If hidZone.Value & "" <> "" Then
                sql = sql & "WHERE (ZONE_CD = '" & hidZone.Value & "') AND (DEL_DT >= TO_DATE('" & CurrentDate & "','mm/dd/RRRR')) "
            End If
            sql = sql & "AND NVL(CLOSED,'N')='N' "
            sql = sql & "ORDER BY DEL_DT"

            objSql = DisposablesManager.BuildOracleCommand(sql, conn)
            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            dv = ds.Tables(0).DefaultView
            Try
                If SystemUtils.dataSetHasRows(ds) Then
                    Dim totalrows As Integer = 0
                    totalrows = ds.Tables(0).Rows.Count()

                    If IsDate(ds.Tables(0).Rows(totalrows - 1)("DEL_DT")) Then
                        MaxDate = Convert.ToDateTime(ds.Tables(0).Rows(totalrows - 1)("DEL_DT"))
                    Else
                        MaxDate = Today
                    End If
                    If IsDate(ds.Tables(0).Rows(0)("DEL_DT")) Then
                        MinDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("DEL_DT"))
                    Else
                        MinDate = Today
                    End If
                End If

            Catch ex As Exception

                Throw
            End Try

            Try
                Using MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)
                    griddeldate.DataSource = dv
                    griddeldate.DataBind()
                    MyDataReader.Close()
                End Using
            Catch ex As Exception
                conn.Close()
                Throw
            End Try
        End Using
    End Sub
    ''' <summary>
    ''' Enable and disable of select based on calculation of stops
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub griddeldate_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs)
        If (e.DataColumn.Caption = "Select") Then
            Dim selectbtn As ASPxButton = TryCast(griddeldate.FindRowCellTemplateControl(e.VisibleIndex, TryCast(griddeldate.Columns("Select"), GridViewDataTextColumn), "btnSelect"), ASPxButton)
            If Not selectbtn Is Nothing Then
                Dim remainingPtsStps As String = griddeldate.GetRowValues(e.VisibleIndex, "REMAINING_STOPS")
                selectbtn.Enabled = theTMBiz.GetDisableDeliveryDates(pt_cnt, remainingPtsStps, griddeldate.GetRowValues(e.VisibleIndex, "EXMPT_DEL_CD") & "")
            End If
        End If


    End Sub
    ''' <summary>
    ''' when user selects the date in the gird, selected date is passed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub griddeldate_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs)
        If e.CommandArgs.CommandName = "Select" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgs.CommandArgument)
            Dim Selecteddate As Date = Nothing
            Selecteddate = griddeldate.GetRowValues(index, "DEL_DT")
            RaiseEvent DeliveryDateSelected(sender, Selecteddate)
        End If
    End Sub
    ''' <summary>
    ''' setting date range in the calender serach
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub griddeldate_AutoFilterCellEditorCreate(sender As Object, e As ASPxGridViewEditorCreateEventArgs)
        If e.Column.FieldName = "DEL_DT" Then
            e.EditorProperties = New DateEditProperties()
            DirectCast(e.EditorProperties, DateEditProperties).MinDate = MinDate
            DirectCast(e.EditorProperties, DateEditProperties).MaxDate = MaxDate
        End If
    End Sub
End Class
