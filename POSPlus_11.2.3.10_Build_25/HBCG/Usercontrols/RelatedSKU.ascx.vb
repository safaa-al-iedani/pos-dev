﻿Imports System.Collections.Generic

Partial Class Usercontrols_RelatedSKU
    Inherits System.Web.UI.UserControl

    Public Event AddSelection As EventHandler
    Public Event NoneAdded As EventHandler
    Public Delegate Sub EventHandler(sender As Object, e As EventArgs, selectedRelSKUs As List(Of Object))

    Private objSKUBiz As SKUBiz = New SKUBiz()

    ''' <summary>
    ''' Load the controls' data
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadRelatedSKUData(ByVal strSKUs As String)

        hidSku.Value = strSKUs

        gvRelatedSKUs.PageIndex = 0
        gvRelatedSKUs.SettingsPager.PageSize = 10
        gvRelatedSKUs.Selection.UnselectAll()
        gvRelatedSKUs.DataBind()

    End Sub

    Protected Sub gvRelatedSKUs_CustomUnboundColumnData(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDataEventArgs) Handles gvRelatedSKUs.CustomUnboundColumnData
        If e.Column.FieldName = "CompositeKey" Then
            Dim relatedItmCd As String = Convert.ToString(e.GetListSourceFieldValue("RELATED_ITM_CD"))
            Dim itmCd As String = Convert.ToString(e.GetListSourceFieldValue("ITM_CD"))
            e.Value = relatedItmCd & "-" & itmCd
        End If
    End Sub

    Protected Sub gvRelatedSKUs_DataBinding(sender As Object, e As EventArgs) Handles gvRelatedSKUs.DataBinding
        Dim dtRelatedSKUs As New DataTable
        If Not String.IsNullOrEmpty(hidSku.Value) Then
            dtRelatedSKUs = GetRelatedSKUsDetails(hidSku.Value)

            popupRelatedSKUs.ShowOnPageLoad = dtRelatedSKUs.Rows.Count > 0

            gvRelatedSKUs.DataSource = dtRelatedSKUs

            'Setting the height of the Grid
            Dim gridHeight As Integer = IIf(dtRelatedSKUs.Rows.Count > 10, 250, dtRelatedSKUs.Rows.Count * 30)
            gvRelatedSKUs.Settings.VerticalScrollableHeight = gridHeight
        End If
    End Sub

    Private Function GetRelatedSKUsDetails(strSKUs As String) As DataTable
        Dim dtRelatedSKUs As New DataTable
        If Not IsNothing(strSKUs) AndAlso Not String.IsNullOrEmpty(strSKUs.Trim()) Then
            strSKUs = strSKUs.Trim().TrimStart(",").TrimEnd(",")

            Dim aStrSKUs As String() = strSKUs.Split(",")
            dtRelatedSKUs = objSKUBiz.GetRelatedItems(aStrSKUs, Session("PD_STORE_CD"))
        End If

        Return dtRelatedSKUs
    End Function

    Protected Sub btnAddSKU_Click(sender As Object, e As EventArgs) Handles btnAddSKU.Click
        'gathers ALL check-marked records in the table
        Dim selRows As List(Of Object) = gvRelatedSKUs.GetSelectedFieldValues("RELATED_ITM_CD")

        If selRows.Count > 0 Then
            lblError.Text = String.Empty
            RaiseEvent AddSelection(sender, e, selRows)
            popupRelatedSKUs.ShowOnPageLoad = False
        Else
            lblError.Text = Resources.POSErrors.ERR0033
        End If

    End Sub

    Protected Sub btnNoSKU_Click(sender As Object, e As EventArgs) Handles btnNoSKU.Click
        lblError.Text = String.Empty
        RaiseEvent NoneAdded(sender, e, New List(Of Object))
        popupRelatedSKUs.ShowOnPageLoad = False
    End Sub
End Class
