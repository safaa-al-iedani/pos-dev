﻿
Partial Class Usercontrols_SummaryPriceChangeApproval
    Inherits System.Web.UI.UserControl

    Public Event WasPCApproved As EventHandler
    Public Event WasPCCanceled As EventHandler
    Public Delegate Sub EventHandler(sender As Object, e As EventArgs)

    Public Sub BindData(dt As DataTable)

        If dt.Rows.Count > 0 Then
            rptrLines.DataSource = dt
            rptrLines.DataBind()

            Dim totalSavings As Decimal = 0, qty As Double = 0, origPrice As Decimal = 0, unitPrice As Decimal = 0
            For Each dr As DataRow In dt.Rows
                Double.TryParse(dr("QTY"), qty)
                Decimal.TryParse(dr("ORIG_PRC"), origPrice)
                Decimal.TryParse(dr("UNIT_PRC"), unitPrice)

                'totalSavings += CDbl(dr("QTY")) * (CDec(dr("ORIG_PRC")) - CDec(dr("UNIT_PRC")))
                totalSavings += qty * (origPrice - unitPrice)
            Next

            lblTotPrChangeAmt.Text = String.Format("{0:c}", totalSavings)

        End If
    End Sub

    Protected Sub ucManagerOverride_wasApproved(sender As Object, e As EventArgs) Handles ucManagerOverride.wasApproved
        Session("IsCanceled") = False
        RaiseEvent WasPCApproved(sender, e)
        popupPriceChangeAppr.ShowOnPageLoad = False
        Session("SHOWPOPUP") = Nothing  'mariam -sep 17,2016 - price change approval
    End Sub

    Protected Sub ucManagerOverride_wasCanceled(sender As Object, e As EventArgs) Handles ucManagerOverride.wasCanceled
        Session("IsCanceled") = True
        RaiseEvent WasPCCanceled(sender, e)
        popupPriceChangeAppr.ShowOnPageLoad = False
        Session("SHOWPOPUP") = Nothing  'mariam -sep 17,2016 - price change approval
    End Sub
    Public Sub SetDisplayMessage(ByVal msg As String)
        lblMessage.Text = msg
        lblMessage.Visible = True
    End Sub
    Public Sub HideDisplayMessage()
        lblMessage.Text = String.Empty
        lblMessage.Visible = False
    End Sub
    
End Class
