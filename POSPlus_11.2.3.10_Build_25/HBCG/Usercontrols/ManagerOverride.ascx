﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ManagerOverride.ascx.vb" Inherits="Usercontrols_ManagerOverride" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:HiddenField ID="hidEffDt" runat="server" />
<asp:HiddenField ID="hidSecCd" runat="server" />
<asp:HiddenField ID="hidAppEmpCd" runat="server" />
<asp:Panel ID="pnlMgrOverride" runat="server" DefaultButton="btnSubmit">
    <table style="margin: auto;">
        <tr>
            <td style="text-align: center;">
                <img style="padding-right: 20px;" alt="Stop" src="images/icons/Symbol-Stop.png" />
            </td>
            <td style="white-space: nowrap;">
                <table style="margin: auto;">
                    <tr>
                        <td colspan="3" style="text-align: center;">
                            <asp:Label ID="lblMessage" runat="server" visible="false" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: center;">
                            <asp:Label ID="lblErrMgrOverride" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 100px;">
                            <label style="white-space: nowrap;">Password:</label>
                        </td>
                        <td style="width: 175px;">
                            <dx:ASPxTextBox ID="txtMgrPwd" runat="server" Password="true"></dx:ASPxTextBox>
                        </td>
                        <td>
                            <span id="spnErrMgrPwd" runat="server" visible="false" style="color: red;">*</span></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table align="Center">
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" ClientInstanceName="btnSubmit" UseSubmitBehavior="true"></dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" ClientInstanceName="btnCancel" UseSubmitBehavior="false">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID ="hidSource" runat="server" />
</asp:Panel>
