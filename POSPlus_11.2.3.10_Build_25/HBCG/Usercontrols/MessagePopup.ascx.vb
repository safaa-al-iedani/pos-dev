﻿Partial Class Usercontrols_MessagePopup
    Inherits System.Web.UI.UserControl

    Public Event Choice As EventHandler
    Public Delegate Sub EventHandler(sender As Object, e As EventArgs, choice As MessageResponse)

    ' indicates what type of icon & its path
    Private Const INFO_URL As String = "~/images/icons/Symbol-Information.png"
    Private Const WARNING_URL As String = "~/images/icons/Symbol-Exclamation.png"
    Private Const ERROR_URL As String = "~/images/icons/Symbol-Delete.png"

    ' indicates the type of response when a question type of message is shown
    Public Enum MessageResponse
        YES_OK_SELECTION = 1        ' Return value from the DisplayConfirmMsg if YES/OK is chosen
        NO_SELECTION = 2            ' Return value from the DisplayConfirmMsg if NO is chosen
    End Enum


    ''' <summary>
    ''' Displays an 'information' type of message using the passed-in text.
    ''' </summary>
    ''' <param name="msg">the text of the message</param>
    Public Sub DisplayInfoMsg(ByVal msg As String, Optional ByVal caption As String = "")

        caption = If(caption.isEmpty, Resources.POSMessages.MSG0010, Resources.POSMessages.MSG0010 & " - " & caption)
        DisplayMessage(msg, INFO_URL, caption)
        btnCancel.Visible = False
        btnOK.Visible = False
    End Sub

    ''' <summary>
    ''' Displays an 'warning' type of message using the passed-in text.
    ''' </summary>
    ''' <param name="msg">the text of the message</param>
    Public Sub DisplayWarningMsg(ByVal msg As String, Optional ByVal caption As String = "")

        caption = If(caption.isEmpty, Resources.POSMessages.MSG0011, Resources.POSMessages.MSG0011 & " - " & caption)
        DisplayMessage(msg, WARNING_URL, caption)
        btnCancel.Visible = False
        btnOK.Visible = False
    End Sub

    ''' <summary>
    ''' Displays an 'error' type of message using the passed-in text.
    ''' </summary>
    ''' <param name="msg">the text of the message</param>
    Public Sub DisplayErrorMsg(ByVal msg As String, Optional ByVal caption As String = "")

        caption = If(caption.isEmpty, Resources.POSMessages.MSG0012, Resources.POSMessages.MSG0012 & "  - " & caption)
        DisplayMessage(msg, ERROR_URL, caption)
        btnCancel.Visible = False
        btnOK.Visible = False
    End Sub

    ''' <summary>
    ''' Displays an 'confirmation' or 'question' type of message using the passed-in text.
    ''' </summary>
    ''' <param name="msg">the text of the message</param>
    Public Sub DisplayConfirmMsg(ByVal msg As String, Optional ByVal caption As String = "", Optional ByVal btnOkText As String = "OK")
        caption = If(caption.isEmpty, Resources.POSMessages.MSG0013, Resources.POSMessages.MSG0013 & "  - " & caption)
        DisplayMessage(msg, ERROR_URL, caption)
        btnOK.Text = btnOkText
        btnCancel.Visible = True
        btnOK.Visible = True
        btnClose.Visible = False
    End Sub

    ''' <summary>
    ''' Displays as 'Alert' of javascript alert.
    ''' </summary>
    ''' <param name="msg">the text of the message</param>
    Public Sub DisplayAlertMsg(ByVal msg As String, Optional ByVal caption As String = "")

        caption = If(caption.isEmpty, Resources.POSMessages.MSG0011, Resources.POSMessages.MSG0011 & "  - " & caption)
        DisplayMessage(msg, WARNING_URL, caption)
        btnCancel.Visible = False
        btnOK.Visible = True
        btnClose.Visible = False
    End Sub

    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click

        txtMsg.ForeColor = Color.Black
        popupMsg.ShowOnPageLoad = False
    End Sub

    Protected Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        popupMsg.ShowOnPageLoad = False
        txtMsg.ForeColor = Color.Black
        RaiseEvent Choice(sender, e, MessageResponse.YES_OK_SELECTION)
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

        RaiseEvent Choice(sender, e, MessageResponse.NO_SELECTION)
        popupMsg.ShowOnPageLoad = False
        txtMsg.ForeColor = Color.Black

    End Sub

    ''' <summary>
    ''' Displays the message passed-in with the specified caption and image.
    ''' </summary>
    ''' <param name="msg">the message to be displayed</param>
    ''' <param name="iconUrl">the icon to be used for the message</param>
    ''' <param name="caption">the caption to be set on the window</param>
    Private Sub DisplayMessage(ByVal msg As String, ByVal iconUrl As String, ByVal caption As String)

        popupMsg.HeaderText = caption
        txtMsg.Text = msg
        imgIcon.ImageUrl = iconUrl
    End Sub

End Class
