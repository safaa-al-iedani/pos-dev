﻿Imports System.Data.OracleClient
Imports DevExpress.Web.ASPxNavBar
Imports System.Linq
Imports System.Web.Services
Imports System.Web.Script.Services
Imports DevExpress.Web.ASPxEditors

Partial Class Usercontrols_MultiWarranties
    Inherits System.Web.UI.UserControl

    Private objSalesBiz As SalesBiz = New SalesBiz()

    Protected Const sameWarrSeparator As String = "%"

    Public Event WarrantyAdded As EventHandler
    Public Event NoWarrantyAdded As EventHandler

    Public Delegate Sub EventHandler(sender As Object, e As EventArgs)

    Protected Sub dxNavBar_ExpandedChanged(source As Object, e As NavBarGroupEventArgs) Handles dxNavBar.ExpandedChanged
        SetGroupTextWithSelectedWarranty()
    End Sub

    Protected Sub dxNavBar_HeaderClick(source As Object, e As NavBarGroupCancelEventArgs) Handles dxNavBar.HeaderClick
        LoadWarrantyForSKU(e.Group)
    End Sub

    Protected Sub btnAddWarr_Click(sender As Object, e As EventArgs) Handles btnAddWarr.Click
        Dim sessionId As String = IIf(String.IsNullOrEmpty(hidDelDocNum.Value), Session.SessionID.ToString.Trim, hidDelDocNum.Value)
        objSalesBiz.DeleteTempWarrEntry(sessionId)

        RaiseEvent WarrantyAdded(sender, e)
        popupWarranties.ShowOnPageLoad = False
    End Sub

    Protected Sub btnNoThanks_Click(sender As Object, e As EventArgs) Handles btnNoThanks.Click
        Session.Remove("WARRINFO")

        Dim sessionId As String = IIf(String.IsNullOrEmpty(hidDelDocNum.Value), Session.SessionID.ToString.Trim, hidDelDocNum.Value)
        objSalesBiz.DeleteTempWarrEntry(sessionId)

        RaiseEvent NoWarrantyAdded(sender, e)
        popupWarranties.ShowOnPageLoad = False
    End Sub

    Protected Sub ucLinkWarranties_LinkWarrantyToSKUs(sender As Object, e As EventArgs) Handles ucLinkWarranties.LinkWarrantyToSKUs
        If Not IsNothing(Session("WARRINFO")) Then
            Dim dtWarr As DataTable = DirectCast(Session("WARRINFO"), DataTable)
            LoadWarranties(dtWarr)
        End If
    End Sub

    ''' <summary>
    ''' Call this for loading this web user control
    ''' </summary>
    ''' <param name="dtItem">Data table with multiple ITM_CD and ROW_ID</param>
    ''' <remarks></remarks>
    Public Sub LoadWarranties(dtItem As DataTable)
        If IsNothing(dtItem) OrElse dtItem.Rows.Count < 1 Then
            lblError.Text = "No Warranty SKUS were found for this order."
            btnAddWarr.Visible = False
            btnNoThanks.Text = "Close Window"
        Else
            dxNavBar.Groups.Clear()

            Session("WARRINFO") = dtItem

            lblError.Text = String.Empty

            Dim dtPrevWarr As New DataTable

            If SysPms.isOne2ManyWarr Then
                Dim sessionId As String = IIf(String.IsNullOrEmpty(hidDelDocNum.Value), Session.SessionID.ToString.Trim, hidDelDocNum.Value)
                dtPrevWarr = objSalesBiz.GetTempExistWarrs(sessionId)
            End If

            For idx = 0 To dtItem.Rows.Count - 1
                dxNavBar.Groups.Add(dtItem.Rows(idx)("ITM_CD"))
                dxNavBar.Groups(dxNavBar.Groups.Count - 1).Name = dtItem.Rows(idx)("ROW_ID")
                LoadWarrantyForSKU(dxNavBar.Groups(dxNavBar.Groups.Count - 1))
            Next

            SetGroupTextWithSelectedWarranty()

            If Not String.IsNullOrEmpty(hidFocusItem.Value) Then
                For Each grp As DevExpress.Web.ASPxNavBar.NavBarGroup In dxNavBar.Groups
                    If grp.Name = hidFocusItem.Value Then
                        grp.Expanded = True
                        Exit For
                    End If
                Next

                hidFocusItem.Value = String.Empty
            ElseIf dxNavBar.Groups.Count > 0 Then
                dxNavBar.Groups(0).Expanded = False
            End If
        End If

    End Sub

    ''' <summary>
    ''' Load the Navigation Bar template data
    ''' </summary>
    ''' <param name="navBarGroup">Navigation Bar Group</param>
    ''' <remarks></remarks>
    Private Sub LoadWarrantyForSKU(navBarGroup As DevExpress.Web.ASPxNavBar.NavBarGroup, Optional ByVal existWarr As String = "")

        Dim req4ExtWarr As SalesUtils.GetCalRetPrcReq = New SalesUtils.GetCalRetPrcReq

        Dim splitChar As String = String.Format("{0,10}", String.Empty)
        Dim navbarText As String() = navBarGroup.Text.Split(splitChar)
        Dim itmCd As String = navbarText(0).Trim()

        req4ExtWarr.itmCd = itmCd
        req4ExtWarr.cashFlg = "N"
        req4ExtWarr.itmRetPrc = Nothing

        If (Not IsNothing(Session("tran_dt"))) AndAlso SystemUtils.isNotEmpty(Session("tran_dt")) AndAlso IsDate(Session("tran_dt")) Then  ' TODO DSA - test the nothings
            req4ExtWarr.effDt = Session("tran_dt")
        Else
            req4ExtWarr.effDt = FormatDateTime(DateTime.Today, DateFormat.ShortDate)
        End If

        If (Not IsNothing(Session("store_cd"))) AndAlso SystemUtils.isNotEmpty(Session("store_cd")) Then
            req4ExtWarr.storeCd = Session("store_cd")
        Else
            req4ExtWarr.storeCd = String.Empty
        End If

        If (Not IsNothing(Session("cust_tp_prc_cd"))) AndAlso SystemUtils.isNotEmpty(Session("cust_tp_prc_cd")) Then
            req4ExtWarr.custTpPrcCd = Session("cust_tp_prc_cd")
        Else
            req4ExtWarr.custTpPrcCd = String.Empty
        End If

        Dim dt As DataTable = Session("WARRINFO")

        Dim dsAddWarr As DataSet = SalesUtils.GetItmExtWarrs(req4ExtWarr)
        Dim dtAddWarr As DataTable = dsAddWarr.Tables(0)

        Dim filterPrevWarr = From dtAWarr In dtAddWarr.AsEnumerable()
                      Join dTable In dt.Select("ROW_ID <> '" + navBarGroup.Name + "' AND IS_PREV_SEL = FALSE").AsEnumerable()
                      On dtAWarr.Field(Of String)("WAR_ITM_CD") Equals dTable.Field(Of String)("WARR_ITM_CD") Select dtAWarr Distinct

        Dim dtPrevWarr As New DataTable
        If filterPrevWarr.Count() > 0 Then
            dtPrevWarr = filterPrevWarr.CopyToDataTable()
        End If

        navBarGroup.Items.Clear()
        Dim navBarItem As NavBarItem = New NavBarItem()
        navBarItem.Text = String.Empty
        navBarItem.Template = New NavSKUTemplate(itmCd, navBarGroup.Name, dtPrevWarr, dtAddWarr, existWarr)
        navBarGroup.Items.Add(navBarItem)

        Dim drFilter As DataRow() = dt.Select("ROW_ID = '" + navBarGroup.Name + "'")
        If drFilter.Length > 0 Then
            If String.IsNullOrEmpty(drFilter(0)("WARR_ITM_TEXT") & "") Then
                Dim Warrval = From dtItemInfo In dt.Select("ROW_ID = '" + navBarGroup.Name + "'").AsEnumerable()
                             Join dtWarr In dtAddWarr.AsEnumerable()
                             On dtItemInfo.Field(Of String)("WARR_ITM_CD") Equals dtWarr.Field(Of String)("WAR_ITM_CD")
                             Select dtWarr.Field(Of String)("DES") Distinct
                drFilter(0)("WARR_ITM_TEXT") = Warrval(0)
            End If
        End If

    End Sub

    Private Sub SetGroupTextWithSelectedWarranty()
        For Each navbarGrp As DevExpress.Web.ASPxNavBar.NavBarGroup In dxNavBar.Groups
            Dim navBarGroupText As String = navbarGrp.Text

            Dim splitChar As String = String.Format("{0,10}", String.Empty)
            Dim navbarText As String() = navBarGroupText.Split(splitChar)
            Dim itmCd As String = navbarText(0).Trim()

            Dim dtWarr As DataTable = Session("WARRINFO")
            Dim drFilter() As DataRow = dtWarr.Select("ITM_CD = '" + itmCd + "' AND ROW_ID = '" + navbarGrp.Name + "'")

            If drFilter.Length > 0 Then
                If Not String.IsNullOrEmpty(drFilter(0)("WARR_ITM_CD") & "") Then
                    navbarGrp.Text = String.Format("{0}{1,10}{2}{3}", itmCd, String.Empty, "Warranted by: ", drFilter(0)("WARR_ITM_TEXT"))
                Else
                    navbarGrp.Text = String.Format("{0}", itmCd)
                End If
            Else
                navbarGrp.Text = navBarGroupText
            End If
        Next
    End Sub
End Class

Public Class NavSKUTemplate
    Implements ITemplate

    Private _itmCd As String
    Private _rowId As String
    Private _existWarr As String
    Private _dtPrevWarr As DataTable
    Private _dtAddWarr As DataTable

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="itmCd"></param>
    ''' <param name="rowId"></param>
    ''' <param name="dtPrevWarr"></param>
    ''' <param name="dtAddWarr"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal itmCd As String, ByVal rowId As String, ByVal dtPrevWarr As DataTable, ByVal dtAddWarr As DataTable, ByVal existWarr As String)
        _itmCd = itmCd
        _rowId = rowId
        _dtPrevWarr = dtPrevWarr
        _dtAddWarr = dtAddWarr
        _existWarr = existWarr

        Dim drAddWarr As DataRow = _dtAddWarr.NewRow()
        drAddWarr(0) = "W"
        drAddWarr(1) = "0"
        drAddWarr(2) = "   None"

        _dtAddWarr.Rows.InsertAt(drAddWarr, 0)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="container"></param>
    ''' <remarks></remarks>
    Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn

        Dim _lblVSN As New DevExpress.Web.ASPxEditors.ASPxLabel
        _lblVSN.ID = String.Format("lblVSN_{0}_{1}", _itmCd, _rowId)
        _lblVSN.ClientInstanceName = String.Format("lblVSN_{0}_{1}", _itmCd, _rowId)
        Dim _bl As New BulletedList
        _bl.ID = String.Format("bl_{0}_{1}", _itmCd, _rowId)
        _bl.ClientIDMode = ClientIDMode.Static
        Dim _lblWarr1 As New DevExpress.Web.ASPxEditors.ASPxLabel
        _lblWarr1.ID = String.Format("lblWarr1_{0}_{1}", _itmCd, _rowId)
        _lblWarr1.ClientInstanceName = String.Format("lblWarr1_{0}_{1}", _itmCd, _rowId)

        Dim _rdoAddWarr As New ASPxRadioButtonList
        _rdoAddWarr.Border.BorderStyle = BorderStyle.None
        _rdoAddWarr.ID = String.Format("rdoAddWarr_{0}_{1}", _itmCd, _rowId)
        _rdoAddWarr.ClientInstanceName = String.Format("rdoAddWarr_{0}_{1}", _itmCd, _rowId)
        _rdoAddWarr.ClientIDMode = ClientIDMode.Static
        _rdoAddWarr.AutoPostBack = False
        _rdoAddWarr.ClientSideEvents.SelectedIndexChanged = "SelectionChanged"

        Dim _lblWarr2 As New DevExpress.Web.ASPxEditors.ASPxLabel
        _lblWarr2.ID = String.Format("lblWarr2_{0}_{1}", _itmCd, _rowId)
        _lblWarr2.ClientInstanceName = String.Format("lblWarr2_{0}_{1}", _itmCd, _rowId)

        Dim _rdoPrevWarr As New ASPxRadioButtonList
        _rdoPrevWarr.Border.BorderStyle = BorderStyle.None
        _rdoPrevWarr.ID = String.Format("rdoPrevWarr_{0}_{1}", _itmCd, _rowId)
        _rdoPrevWarr.ClientInstanceName = String.Format("rdoPrevWarr_{0}_{1}", _itmCd, _rowId)
        _rdoPrevWarr.ClientIDMode = ClientIDMode.Static
        _rdoPrevWarr.AutoPostBack = False
        _rdoPrevWarr.ClientSideEvents.SelectedIndexChanged = "SelectionChanged"

        _lblWarr1.Text = "Your purchase is also eligible for the following extended warranties:"
        _lblWarr2.Text = "Or you may choose to warranty this item with one of these previously selected extended warranties:"

        _rdoAddWarr.DataSource = _dtAddWarr
        _rdoAddWarr.ValueField = "WAR_ITM_CD"
        _rdoAddWarr.TextField = "DES"
        _rdoAddWarr.DataBind()

        If _dtAddWarr.Rows.Count > 0 Then
            ' can only get and relate 1 warranty at a time, even if can relate to multiple
            Dim manuWarrs As DataTable = SalesUtils.GetItmManuWarrs(_itmCd).Tables(0)

            _lblVSN.Text = String.Empty  'need to clear so it updates text for last SKU in a multiple list which is going to be the data that is displayed

            For Each mWarr As DataRow In manuWarrs.Rows
                If String.IsNullOrEmpty(_lblVSN.Text.Trim()) Then
                    _lblVSN.Text = "The item you have selected, " & mWarr.Item("VSN").ToString & ", comes with the following warranties:"
                End If

                Dim li As New ListItem()
                li.Text = mWarr.Item("DES").ToString
                _bl.Items.Add(li)
            Next
        End If

        If SysPms.isOne2ManyWarr Then
            If _dtPrevWarr.Rows.Count > 0 AndAlso SystemUtils.dataRowIsNotEmpty(_dtPrevWarr.Rows(0)) Then
                _lblWarr2.Visible = True
                _rdoPrevWarr.Visible = True

                _rdoPrevWarr.DataSource = _dtPrevWarr
                _rdoPrevWarr.ValueField = "WAR_ITM_CD"
                _rdoPrevWarr.TextField = "DES"
                _rdoPrevWarr.DataBind()
            Else
                _lblWarr2.Visible = False
                _rdoPrevWarr.Visible = False
            End If
        Else
            _lblWarr2.Visible = False
            _rdoPrevWarr.Visible = False
        End If

        _rdoAddWarr.SelectedIndex = -1

        If Not String.IsNullOrEmpty(_existWarr) Then
            If IsNothing(_rdoPrevWarr.Items.FindByValue(_existWarr)) Then
                _rdoAddWarr.Items.FindByValue(_existWarr).Selected = True
            Else
                _rdoPrevWarr.Items.FindByValue(_existWarr).Selected = True
            End If
        Else
            Dim dtWarr As New DataTable
            If Not IsNothing(HttpContext.Current.Session("WARRINFO")) Then
                dtWarr = DirectCast(HttpContext.Current.Session("WARRINFO"), DataTable)
            End If
            Dim drFilter() As DataRow = dtWarr.Select("ITM_CD = '" + _itmCd + "' AND ROW_ID = '" + _rowId + "'")

            If drFilter.Length > 0 AndAlso Not String.IsNullOrEmpty(drFilter(0)("WARR_ITM_CD") & "") Then
                If drFilter(0)("IS_PREV_SEL") = True Then
                    If IsNothing(_rdoPrevWarr.Items.FindByValue(drFilter(0)("WARR_ITM_CD"))) Then
                        drFilter(0)("IS_PREV_SEL") = False
                        _rdoAddWarr.Items.FindByValue(drFilter(0)("WARR_ITM_CD")).Selected = True
                    Else
                        _rdoPrevWarr.Items.FindByValue(drFilter(0)("WARR_ITM_CD")).Selected = True
                    End If
                Else
                    'Daniela surperss yellow triangle
                    Try
                        _rdoAddWarr.Items.FindByValue(drFilter(0)("WARR_ITM_CD")).Selected = True
                    Catch
                        _rdoAddWarr.SelectedIndex = 0
                    End Try
                    'End Daniela
                End If
            Else
                _rdoAddWarr.SelectedIndex = 0
            End If
        End If

        container.Controls.Add(_lblVSN)
        container.Controls.Add(_bl)
        container.Controls.Add(_lblWarr1)
        container.Controls.Add(_rdoAddWarr)
        container.Controls.Add(_lblWarr2)
        container.Controls.Add(_rdoPrevWarr)
    End Sub

End Class
