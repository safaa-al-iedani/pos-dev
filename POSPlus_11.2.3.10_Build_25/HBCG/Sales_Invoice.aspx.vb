
Imports System.Data.OracleClient

Partial Class Sales_Invoice
    Inherits POSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim fname, lname, addr1, addr2, city, state, zip, hphone, bphone As String
        Dim store_cd, slsp1, wr_dt, tax_chg, follow_up_dt, cust_cd, del_chg, prospect_total, discounts As String
        Dim slsp_fname, slsp_lname As String
        Dim conn2 As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim objsql2 As OracleCommand = DisposablesManager.BuildOracleCommand

        Dim MyDataReader2 As OracleDataReader
        Dim sql As String

        store_cd = ""
        slsp1 = ""
        wr_dt = ""
        tax_chg = ""
        cust_cd = ""
        del_chg = ""
        fname = ""
        lname = ""
        addr1 = ""
        addr2 = ""
        city = ""
        state = ""
        zip = ""
        hphone = ""
        bphone = ""
        slsp_fname = ""
        slsp_lname = ""
        follow_up_dt = ""
        prospect_total = ""
        discounts = ""

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn2.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        Dim DEL_DOC_NUM As String
        DEL_DOC_NUM = Request("DEL_DOC_NUM")
        If DEL_DOC_NUM & "" = "" Then
            Response.Write("No Sales Order found.")
            Response.End()
        End If

        sql = "SELECT * FROM SO WHERE DEL_DOC_NUM = '" & DEL_DOC_NUM & "'"

        'Open Connection 
        conn2.Open()

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If (MyDataReader2.Read()) Then
                store_cd = MyDataReader2.Item("SO_STORE_CD").ToString.Trim
                slsp1 = MyDataReader2.Item("SO_EMP_SLSP_CD1").ToString.Trim
                wr_dt = MyDataReader2.Item("SO_WR_DT").ToString.Trim
                tax_chg = MyDataReader2.Item("TAX_CHG").ToString.Trim
                cust_cd = MyDataReader2.Item("cust_cd").ToString.Trim
                del_chg = MyDataReader2.Item("del_chg").ToString.Trim
                fname = MyDataReader2.Item("SHIP_TO_F_NAME").ToString.Trim
                lname = MyDataReader2.Item("SHIP_TO_L_NAME").ToString.Trim
                addr1 = MyDataReader2.Item("SHIP_TO_ADDR1").ToString.Trim
                addr2 = MyDataReader2.Item("SHIP_TO_ADDR2").ToString.Trim
                city = MyDataReader2.Item("SHIP_TO_CITY").ToString.Trim
                state = MyDataReader2.Item("SHIP_TO_ST_CD").ToString.Trim
                zip = MyDataReader2.Item("SHIP_TO_ZIP_CD").ToString.Trim
                hphone = MyDataReader2.Item("SHIP_TO_H_PHONE").ToString.Trim
                bphone = MyDataReader2.Item("SHIP_TO_B_PHONE").ToString.Trim
            End If
            MyDataReader2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

        sql = "SELECT AMT FROM AR_TRN WHERE IVC_CD = '" & DEL_DOC_NUM & "'"

        'Set SQL OBJECT 
        objsql2 = DisposablesManager.BuildOracleCommand(sql, conn2)

        Try
            'Execute DataReader 
            MyDataReader2 = DisposablesManager.BuildOracleDataReader(objsql2)

            'Store Values in String Variables 
            If (MyDataReader2.Read()) Then
                prospect_total = MyDataReader2.Item("AMT")
            End If
            'Close Connection 
            MyDataReader2.Close()
        Catch ex As Exception
            conn2.Close()
            Throw
        End Try

        conn2.Close()

        lbl_cust.Text = fname & " " & lname
        lbl_addr.Text = addr1
        lbl_city.Text = city & ", " & state & "     " & zip
        lbl_home.Text = hphone
        lbl_bus.Text = bphone
        lbl_Sales_Name.Text = slsp_fname & " " & slsp_lname
        lbl_REL_NO.Text = DEL_DOC_NUM
        lbl_cust.Text = cust_cd
        lbl_follow_up_dt.Text = FormatDateTime(follow_up_dt, 2)
        lbl_slsp1.Text = slsp1
        lbl_Account.Text = cust_cd
        lbl_Delivery.Text = FormatCurrency(CDbl(del_chg))
        lbl_tax.Text = FormatCurrency(CDbl(tax_chg))
        lbl_grand_total.Text = FormatCurrency(CDbl(prospect_total))
        lbl_Discount.Text = FormatCurrency(CDbl(discounts))
        lbl_Subtotal.Text = FormatCurrency(CDbl(prospect_total) - (CDbl(tax_chg) + CDbl(del_chg)))
        bindgrid()

    End Sub

    Public Sub bindgrid()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader
        Dim ds As DataSet
        Dim oAdp As OracleDataAdapter
        Dim mytable As DataTable
        Dim numrows As Integer
        Dim DEL_DOC_NUM As String
        DEL_DOC_NUM = Request("DEL_DOC_NUM")

        ds = New DataSet

        Gridview1.DataSource = ""
        'CustTable.Visible = False

        Gridview1.Visible = True
        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If
        conn.Open()

        sql = "SELECT * FROM SO_LN where DEL_DOC_NUM='" & DEL_DOC_NUM & "' order by [LN#] ASC"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)
        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)
        mytable = New DataTable
        mytable = ds.Tables(0)
        numrows = mytable.Rows.Count

        Try
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            Else
                Gridview1.Visible = False
                'Response.Redirect("order.aspx")
            End If

            'Close Connection 
            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

    End Sub
End Class
