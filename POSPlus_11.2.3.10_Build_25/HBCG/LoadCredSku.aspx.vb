Imports System.Data.OracleClient
Imports System.Data
Imports System.IO
Imports System.Net
Imports HBCG_Utils
Imports System.Globalization


Partial Class LoadCredSku
    Inherits POSBasePage

    Private theSystemBiz As SystemBiz = New SystemBiz()
    Private LeonsBiz As LeonsBiz = New LeonsBiz()
   

    Protected Sub upload_credsku_file(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
            Dim objSql As OracleCommand = DisposablesManager.BuildOracleCommand

            Dim MyDataReader As OracleDataReader
            Dim ds As DataSet
            Dim oAdp As OracleDataAdapter
            Dim mytable As DataTable
            Dim numrows As Integer
            Dim v_emp_init As String = Session("emp_init")

            Gridview1.DataSource = ""

            conn = DisposablesManager.BuildOracleConnection(ConfigurationManager.ConnectionStrings("ERP").ConnectionString)
            conn.Open()
            ds = New DataSet

            objSql.CommandText = "select event_cd, itm_cd, vsn, credit_amt, qty, " &
            " std_credsku.is_valid_data('" & Session("emp_init") & "',nvl(event_cd,'x'),nvl(itm_cd,'x'),nvl(credit_amt,0),nvl(qty,0))  err_msg  " &
                                 "from credsku_generation_ext a "
            objSql.Connection = conn

            oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
            oAdp.Fill(ds)
            mytable = New DataTable
            mytable = ds.Tables(0)
            numrows = mytable.Rows.Count


            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            If (MyDataReader.Read()) Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
            End If

            Dim i As Integer
            Dim err_cnt As Integer = 0

            For i = 0 To numrows - 1
                If err_cnt = 0 Then
                    If InStr(ds.Tables(0).Rows(i)("err_msg").ToString(), "Error") > 0 Then
                        btn_upload_file.Visible = False
                        err_cnt = 1
                    End If
                End If
            Next

            If ds.Tables(0).Rows.Count = 0 Then
                Gridview1.DataSource = ds
                Gridview1.DataBind()
                lbl_msg.Text = "No data found. Please upload the file."
                btn_insert_credsku.Visible = False
                btn_upload_file.Visible = True
            Else
                If err_cnt = 0 Then
                    btn_upload_file.Visible = True
                    lbl_msg.Text = "No Error Found - press 'Load Data' button to load"
                    btn_insert_credsku.Visible = True
                    btn_upload_file.Visible = False
                Else
                    lbl_msg.Text = "Error found - Please correct the data"
                    btn_insert_credsku.Visible = False
                    btn_upload_file.Visible = True
                End If
            End If

            MyDataReader.Close()
            conn.Close()
        Catch ex As Exception
            btn_insert_credsku.Visible = False
            btn_upload_file.Visible = True
            lbl_msg.Text = "Error found. Please correct the data"
            Throw
        End Try


    End Sub

    Protected Sub btn_insert_credsku_Click(sender As Object, e As EventArgs) Handles btn_insert_credsku.Click

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection
        Dim sql As String
        Dim Mydatareader As OracleDataReader
        Dim objsql As OracleCommand

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        conn.Open()

        sql = " INSERT INTO credevt_sku_dtl (event_cd,itm_cd,credit_amt,stat_cd,qty)  " &
             " select nvl(n.event_cd,'x'), nvl(n.itm_cd,'x'),nvl(n.credit_amt,0),null,nvl(n.qty,0) " &
             " from credsku_generation_ext n "

        objsql = DisposablesManager.BuildOracleCommand(Sql, conn)

        Try
            Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)
            Mydatareader.Close()

        Catch ex As Exception
            conn.Close()
            If ex.ToString.Contains("unique constraint") Then
                Throw New Exception("Duplicate record. To update existing record please enter search criteria and click Search.")
            Else
                Throw New Exception("Error inserting the record. Please try again.")
            End If
        End Try
        conn.Close()

        lbl_msg.Text = "All SKUs are added"
        btn_insert_credsku.Visible = False


    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub
    Protected Sub upload_file_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Gridview1.DataSource = ""
        Gridview1.DataBind()

        Try
            If FileUpload1.HasFile Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(FileUpload1.FileName)
                Dim filePath = FileUpload1.PostedFile.FileName
                Dim fileNameUnix = "credsku.csv"

                Dim ipAdr = LeonsBiz.GetFtpIpAdress(ConfigurationManager.AppSettings("system_mode") & "_IP")

                Dim env As String = ""

                If ConfigurationManager.AppSettings("system_mode") = "RPTGEN" Then
                    env = "live" 'RPTGEN is an exception
                Else
                    env = ConfigurationManager.AppSettings("system_mode").ToLower
                End If

                Dim unixPath = "ftp://" & ipAdr & "//gers/" & env & "/adhoc/loaddata/ext_tables/"

                If (fileExt = ".csv") Then
                    Try
                        'FileUpload1.SaveAs("C:\Uploads\" & FileUpload1.FileName)
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadCredSku\done\" & FileUpload1.FileName)
                        FileUpload1.SaveAs("\\10.128.11.60\Upload\LoadCredSku\done\" & FileUpload1.FileName & "." & Format(Date.Now(), "ddMMMyyyy"))
                        Label1.Text = "File name: " & _
                          FileUpload1.PostedFile.FileName & "<br>" & _
                          "File Size: " & _
                          FileUpload1.PostedFile.ContentLength

                        Dim userId As String = Nothing
                        Dim password As String = Nothing
                        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                        Dim dsn_text As String = ""
                        Dim conStr As String() = Split(ConfigurationManager.ConnectionStrings("ERP").ConnectionString, ";")
                        Dim x As Integer
                        For x = LBound(conStr) To UBound(conStr) - 1
                            If LCase(Left(conStr(x), 3)) = "uid" Then
                                userId = conStr(x)
                                userId = userId.Substring(userId.IndexOf("=") + 1)
                            End If
                            If LCase(Left(conStr(x), 3)) = "pwd" Then
                                password = conStr(x)
                                password = password.Substring(password.IndexOf("=") + 1)
                            End If
                        Next

                        'Upload file using FTP
                        'UploadFile("C:\Uploads\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)
                        LeonsBiz.UploadFile("\\10.128.11.60\Upload\LoadCredSku\done\" & FileUpload1.FileName, unixPath & fileNameUnix, userId, password)

                        ' Log the file name and size in AUDIT_LOG
                        theSystemBiz.SaveAuditLogComment("upload_credsku_file", "Unix file name and size - " & fileNameUnix & ";" & FileUpload1.PostedFile.ContentLength, Session("emp_cd"))


                        Label1.Text = Label1.Text & "<br>" & "File " & FileUpload1.FileName & " is Uploaded "

                        btn_upload_file.Visible = False

                        'Display data from external table
                        upload_credsku_file(sender, e)

                    Catch ex As Exception
                        Label1.Text = "ERROR: " & ex.Message.ToString()
                        Exit Sub
                    End Try
                Else
                    Label1.Text = "Only .csv files allowed!"
                End If
            Else
                Label1.Text = "You have not specified a file."
            End If

        Catch ex As Exception
            ' Log the file name and size in AUDIT_LOG
            theSystemBiz.SaveAuditLogComment("upload_credsku_file", "LOAD_FAILURE - " & ex.Message, Session("emp_cd"))
            Label1.Text = Label1.Text & "<br>" & _
                            "Upload failed. Please check the errors."
        End Try

    End Sub


    'HOW TO USE:

    ' Upload file using FTP
    'UploadFile("c:\UploadFile.doc", "ftp://FTPHostName/UploadPath/UploadFile.doc", "UserName", "Password")

End Class
