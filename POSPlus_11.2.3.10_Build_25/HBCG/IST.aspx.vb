Imports HBCG_Utils
Imports System.Data.OracleClient

Partial Class IST
    Inherits POSBasePage

    Protected Sub Populate_Results()

        Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

        Dim sql As String
        Dim objSql As OracleCommand
        Dim MyDataReader As OracleDataReader

        If ConfigurationManager.ConnectionStrings("ERP") Is Nothing OrElse _
                            ConfigurationManager.ConnectionStrings("ERP").ConnectionString.Trim() = "" Then
            Throw New Exception("Connection Error")
        Else
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End If

        sql = "SELECT a.IST_WR_DT, a.IST_SEQ_NUM, a.IST_STORE_CD, a.DEST_STORE_CD, a.SRT_CD, a.FINAL_DT, a.STAT_CD, a.DOC_NUM, a.TRANSFER_DT, "
        sql = sql & "a.ZONE_CD, b.LN#, b.ITM_CD, b.OLD_STORE_CD, b.OLD_LOC_CD, b.QTY, b.CRPT_ID_NUM, b.UNIT_CST, b.VOID_FLAG, "
        sql = sql & "b.NEW_STORE_CD, b.NEW_LOC_CD, b.CRPT_ID_NUM, c.VSN "
        sql = sql & "FROM IST a, IST_LN b, ITM c WHERE a.IST_SEQ_NUM = b.IST_SEQ_NUM AND a.IST_STORE_CD = b.IST_STORE_CD "
        sql = sql & "AND a.IST_WR_DT = b.IST_WR_DT AND b.ITM_CD=c.ITM_CD "
        sql = sql & "AND a.DOC_NUM='" & Request("ist") & "'"
        sql = sql & "ORDER BY LN# ASC"

        'Set SQL OBJECT 
        objSql = DisposablesManager.BuildOracleCommand(sql, conn)

        Dim ds As New DataSet
        Dim oAdp As OracleDataAdapter

        oAdp = DisposablesManager.BuildOracleDataAdapter(objSql)
        oAdp.Fill(ds)

        Try
            'Open Connection 
            conn.Open()
            'Execute DataReader 
            MyDataReader = DisposablesManager.BuildOracleDataReader(objSql)

            'Store Values in String Variables 
            If MyDataReader.Read Then
                txt_ist.Text = MyDataReader.Item("DOC_NUM").ToString
                txt_store_cd.Text = MyDataReader.Item("IST_STORE_CD").ToString
                txt_dest_store_cd.Text = MyDataReader.Item("DEST_STORE_CD").ToString
                txt_zone.Text = MyDataReader.Item("ZONE_CD").ToString
                txt_status.Text = MyDataReader.Item("STAT_CD").ToString
                lbl_wr_dt.Text = MyDataReader.Item("IST_WR_DT").ToString
                lbl_seq_num.Text = MyDataReader.Item("IST_SEQ_NUM").ToString
                If IsDate(MyDataReader.Item("TRANSFER_DT").ToString) Then
                    txt_transfer.Text = FormatDateTime(MyDataReader.Item("TRANSFER_DT").ToString, DateFormat.ShortDate)
                End If
                MyDataReader.Close()
            End If

            'Close Connection 
            conn.Close()
        Catch ex As Exception
            conn.Close()
            Throw
        End Try

        GridView2.DataSource = ds
        GridView2.DataBind()

        txt_ist.Enabled = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Find_Security("ISTM", Session("emp_cd")) = "N" Then
                btn_Lookup.Enabled = False
                btn_Clear.Enabled = False
                lbl_Cust_Info.Text = "You don't have the appropriate securities to view Inter-Store Transfers."
            End If
            'A query has already been initiated, don't fill in the drop downs without customer data
            If Request("query_returned") = "Y" Then
                Populate_Results()
                btn_Lookup.Enabled = False
            Else
                lbl_Cust_Info.Text = ""
            End If
        End If

    End Sub

    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

        txt_ist.Enabled = True
        txt_ist.Text = ""
        txt_store_cd.Text = ""
        txt_dest_store_cd.Text = ""
        txt_zone.Text = ""
        txt_status.Text = ""
        txt_b_phone.Text = ""
        lbl_Cust_Info.Text = ""
        Response.Redirect("IST.aspx")

    End Sub

    Protected Sub btn_Lookup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Lookup.Click

        Response.Redirect("Main_IST_Lookup.aspx?ist=" & HttpUtility.UrlEncode(txt_ist.Text) & "&store_cd=" & HttpUtility.UrlEncode(txt_store_cd.Text) _
            & "&dest_store_cd=" & HttpUtility.UrlEncode(txt_dest_store_cd.Text) & "&zone_cd=" & HttpUtility.UrlEncode(txt_zone.Text) & "&stat_cd=" & HttpUtility.UrlEncode(txt_status.Text) & "&transfer_dt=" & HttpUtility.UrlEncode(txt_transfer.Text))

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        HBCG_Utils.Update_Theme()

        If Session("IPAD") = "TRUE" Then
            Page.MasterPageFile = "~/Mobile_Wide.Master"
        End If

    End Sub

    Protected Sub GridView2_HtmlRowCreated(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles GridView2.HtmlRowCreated

        If e.RowType <> DevExpress.Web.ASPxGridView.GridViewRowType.Data Then
            Return
        End If

        If e.GetValue("LN#").ToString() & "" <> "" Then
            Dim lbl_so As DevExpress.Web.ASPxEditors.ASPxLabel = TryCast(GridView2.FindRowCellTemplateControl(e.VisibleIndex, Nothing, "lbl_so"), DevExpress.Web.ASPxEditors.ASPxLabel)
            If Not IsNothing(lbl_so) Then
                Dim conn As OracleConnection = DisposablesManager.BuildOracleConnection

                Dim sql As String

                conn.ConnectionString = ConfigurationManager.ConnectionStrings("ERP").ConnectionString
                conn.Open()

                Dim Mydatareader As OracleDataReader
                Dim objsql As OracleCommand

                sql = "select ist_ln$so_ln.del_doc_num || ' - Line#' || ist_ln$so_ln.del_doc_ln# as ist_desc from ist_ln$so_ln "
                sql = sql & "where IST_LN$SO_LN.IST_WR_DT=TO_DATE('" & FormatDateTime(lbl_wr_dt.Text, DateFormat.ShortDate) & "','mm/dd/RRRR') "
                sql = sql & "and IST_LN$SO_LN.IST_STORE_CD='" & txt_store_cd.Text & "' "
                sql = sql & "and IST_LN$SO_LN.IST_SEQ_NUM='" & lbl_seq_num.Text & "' "
                sql = sql & "and IST_LN$SO_LN.IST_LN#=" & e.GetValue("LN#").ToString

                'Set SQL OBJECT 
                objsql = DisposablesManager.BuildOracleCommand(sql, conn)

                Try
                    'Execute DataReader 
                    Mydatareader = DisposablesManager.BuildOracleDataReader(objsql)

                    'Store Values in String Variables 
                    If Mydatareader.Read() Then
                        lbl_so.Text = Mydatareader.Item("IST_DESC").ToString
                    End If
                    Mydatareader.Close()
                Catch
                    Throw
                    conn.Close()
                End Try
                conn.Close()
            End If
        End If
    End Sub

End Class
