<%@ Page Language="VB" MasterPageFile="~/MasterPages/NoWizard2.master" AutoEventWireup="false"
    CodeFile="Settlement.aspx.vb" Inherits="Settlement" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" valign="top">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 13px" bgcolor="#ffffff">
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td align="left" valign="top" width="49%" bgcolor="#ffffff">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td bgcolor="#ffffff">
                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" Text="SETTLEMENT OPTIONS">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2" bgcolor="#ffffff">
                                    </td>
                                    <td width="95%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2" bgcolor="#ffffff">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <dx:ASPxHyperLink ID="HyperLink1" runat="server" Text="Release Hold" NavigateUrl="~/Settlement_Release_Hold.aspx">
                                        </dx:ASPxHyperLink>
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <dx:ASPxHyperLink ID="HyperLink2" runat="server" Text="Batch History" NavigateUrl="~/Settlement_Batch_History.aspx">
                                        </dx:ASPxHyperLink>
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <dx:ASPxHyperLink ID="HyperLink3" runat="server" Text="Balancing" NavigateUrl="~/Settlement_Balancing.aspx">
                                        </dx:ASPxHyperLink>
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <dx:ASPxHyperLink ID="HyperLink4" runat="server" Text="Promo Plans" NavigateUrl="~/Settlement_Promo_Plans.aspx">
                                        </dx:ASPxHyperLink>
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <dx:ASPxHyperLink ID="HyperLink8" runat="server" Text="MOP Maintenance" NavigateUrl="~/MOP_Maint.aspx">
                                        </dx:ASPxHyperLink>
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <dx:ASPxHyperLink ID="HyperLink5" runat="server" Text="Users" NavigateUrl="~/Settlement_User_Maint.aspx">
                                        </dx:ASPxHyperLink>
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <dx:ASPxHyperLink ID="HyperLink6" runat="server" Text="Admin" NavigateUrl="~/Admin.aspx">
                                        </dx:ASPxHyperLink>
                                        <br />
                                        <br />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle" style="text-align: center" bgcolor="#ffffff" class="style2">
                                    </td>
                                    <td width="95%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr valign="bottom" align="right">
                                    <td bgcolor="#ffffff" class="style5">
                                        &nbsp;<dx:ASPxLabel ID="lbl_info" runat="server" Width="281px">
                                        </dx:ASPxLabel>
                                        &nbsp;<br />
                                        <br />
                                        <br />
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
