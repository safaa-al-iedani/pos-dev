﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class TransactionDtc

    <DataMember> _
        Public tranPK As Decimal

    <DataMember> _
    Public companyCd As String

    <DataMember> _
    Public storeCd As String

    <DataMember> _
    Public cashDrawerCd As String

    <DataMember> _
    Public seqNo As Integer

    <DataMember> _
    Public version As String

    <DataMember> _
    Public customerName As String

    <DataMember> _
    Public mopTp As String

    <DataMember> _
    Public mopCd As String

    <DataMember> _
    Public mopDesc As String

    <DataMember> _
    Public invoice As String

    <DataMember> _
    Public postDate As Date

    <DataMember> _
    Public tranTpCd As String

    <DataMember> _
    Public dcCd As String

    <DataMember> _
    Public checkName As String

    <DataMember> _
    Public currCd As String

    <DataMember> _
    Public amount As Decimal

    <DataMember> _
    Public mopAmount As Decimal

    <DataMember> _
    Public isDwrCloseAccess As Boolean

End Class
