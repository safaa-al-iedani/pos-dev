﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class EmpDtc
    <DataMember> _
    Public empCd As String

    <DataMember> _
    Public empInit As String

    <DataMember> _
    Public firstName As String

    <DataMember> _
    Public lastName As String

    <DataMember> _
    Public fullName As String

    <DataMember> _
    Public homeStoreCd As String

    <DataMember> _
    Public shipToStoreCd As String
End Class
