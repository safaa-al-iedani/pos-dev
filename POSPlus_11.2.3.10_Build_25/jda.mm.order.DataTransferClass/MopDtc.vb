﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class MopDtc
    <DataMember> _
    Public mopCd As String

    <DataMember> _
    Public mopDesc As String

    <DataMember> _
    Public mopTp As String
End Class
