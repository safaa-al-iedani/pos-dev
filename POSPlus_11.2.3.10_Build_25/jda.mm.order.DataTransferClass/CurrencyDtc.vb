﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class CurrencyDtc
    <DataMember> _
    Public currencyCd As String

    <DataMember> _
    Public currencyDesc As String

    <DataMember> _
    Public exchangeRate As Decimal

    <DataMember> _
    Public cubeRate As Decimal

    <DataMember> _
    Public lastChangeDate As Date

    <DataMember> _
    Public currencySymbol As String
End Class
