﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class CashDrawerDepositCkDtc
    '<DataMember> _
    'Public companyCd As String

    '<DataMember> _
    'Public storeCd As String

    '<DataMember> _
    'Public cashDrawerCd As String

    '<DataMember> _
    'Public empCd As String

    '<DataMember> _
    'Public transactionDate As Date

    '<DataMember> _
    'Public currencyCd As String

    <DataMember> _
    Public checkAmount As Decimal

    <DataMember> _
    Public checkName As String

    <DataMember> _
    Public seqenceNo As Decimal

    <DataMember> _
    Public version As Decimal
End Class
