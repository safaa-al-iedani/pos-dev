﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class CashDrawerCashCountDtc

    <DataMember> _
    Public companyCd As String

    <DataMember> _
    Public storeCd As String

    <DataMember> _
    Public cashDrawerCd As String

    <DataMember> _
    Public transactionDate As Date

    <DataMember> _
    Public countType As String

    <DataMember> _
    Public empCd As String

    <DataMember> _
    Public oneCount As Decimal

    <DataMember> _
    Public twoCount As Decimal

    <DataMember> _
    Public fiveCount As Decimal

    <DataMember> _
    Public tenCount As Decimal

    <DataMember> _
    Public twentyCount As Decimal

    <DataMember> _
    Public fiftyCount As Decimal

    <DataMember> _
    Public oneHundredCount As Decimal

    <DataMember> _
    Public oneThousandCount As Decimal

    <DataMember> _
    Public penniesCount As Decimal

    <DataMember> _
    Public nickelsCount As Decimal

    <DataMember> _
    Public dimesCount As Decimal

    <DataMember> _
    Public quartersCount As Decimal

    <DataMember> _
    Public halfCount As Decimal

    <DataMember> _
    Public oneCoinCount As Decimal

    <DataMember> _
    Public currencyCd As String

    <DataMember> _
    Public depositBag As String

    <DataMember> _
    Public depositSlip As String
End Class