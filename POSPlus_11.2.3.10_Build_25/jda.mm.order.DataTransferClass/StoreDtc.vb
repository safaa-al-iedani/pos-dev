﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class StoreDtc
    <DataMember> _
    Public storeCd As String

    <DataMember> _
    Public storeName As String

    <DataMember> _
    Public companyCd As String

    <DataMember> _
    Public multiCurrency As String

    <DataMember> _
    Public defCurrCd As String

    <DataMember> _
    Public transRoute As String

    <DataMember> _
    Public bankAccNo As String

    <DataMember> _
    Public bankCd As String

    <DataMember> _
    Public bankAccCd As String

    <DataMember> _
    Public CashDrawerList As CashDrawerDtc()

    <DataMember> _
    Public CashierList As EmpDtc()

    <DataMember> _
    Public CurrencyList As CurrencyDtc()

End Class
