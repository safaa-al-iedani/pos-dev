﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class CashDrawerDepositDtc
    <DataMember> _
    Public companyCd As String

    <DataMember> _
    Public storeCd As String

    <DataMember> _
    Public cashDrawerCd As String

    <DataMember> _
    Public transactionDate As Date

    <DataMember> _
    Public empCdCashier As String

    <DataMember> _
    Public currencyCd As String

    <DataMember> _
    Public oneCount As Decimal

    <DataMember> _
    Public twoCount As Decimal

    <DataMember> _
    Public fiveCount As Decimal

    <DataMember> _
    Public tenCount As Decimal

    <DataMember> _
    Public twentyCount As Decimal

    <DataMember> _
    Public fiftyCount As Decimal

    <DataMember> _
    Public oneHundredCount As Decimal

    <DataMember> _
    Public oneThousandCount As Decimal

    <DataMember> _
    Public penniesCount As Decimal

    <DataMember> _
    Public nickelsCount As Decimal

    <DataMember> _
    Public dimesCount As Decimal

    <DataMember> _
    Public quartersCount As Decimal

    <DataMember> _
    Public halfCount As Decimal

    <DataMember> _
    Public oneCoinCount As Decimal

    <DataMember> _
    Public depositBag As String

    <DataMember> _
    Public depositSlip As Integer

    <DataMember> _
    Public lastActTime As Date

    <DataMember> _
    Public version As Integer

End Class
