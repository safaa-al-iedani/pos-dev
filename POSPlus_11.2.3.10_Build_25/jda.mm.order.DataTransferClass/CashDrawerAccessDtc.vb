﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class CashDrawerAccessDtc

    <DataMember> _
    Public empCd As String

    <DataMember> _
    Public isCDBAccess As Boolean

    <DataMember> _
    Public isAllCshrAccess As Boolean

    <DataMember> _
    Public isDwrUpdAccess As Boolean

    <DataMember> _
    Public isDetailAccess As Boolean

    <DataMember> _
    Public isUpdateAccess As Boolean

    <DataMember> _
    Public isCloseAccess As Boolean

    <DataMember> _
    Public isRePrtAccess As Boolean

    <DataMember> _
    Public isBalDiffAccess As Boolean

    <DataMember> _
    Public isBalAccess As Boolean

    <DataMember> _
    Public isClsBalAccess As Boolean
End Class
