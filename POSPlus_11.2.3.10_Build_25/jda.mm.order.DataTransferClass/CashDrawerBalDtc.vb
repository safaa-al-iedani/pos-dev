﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class CashDrawerBalDtc
    <DataMember> _
    Public companyCd As String

    <DataMember> _
    Public empCd As String

    <DataMember> _
    Public loggedEmpCd As String

    <DataMember> _
    Public empInit As String

    <DataMember> _
    Public storeCd As String

    <DataMember> _
    Public cashDrawerCd As String

    <DataMember> _
    Public transactionDate As Date

    <DataMember> _
    Public currencyCd As String

    <DataMember> _
    Public openBalance As Decimal

    <DataMember> _
    Public balanced As String

    <DataMember> _
    Public closed As String

    <DataMember> _
    Public closeBalance As Decimal

    <DataMember> _
    Public transBal As Decimal

    <DataMember> _
    Public mopCd As String

    <DataMember> _
    Public mopAmount As Decimal

    <DataMember> _
    Public overShortAmt As Decimal

    <DataMember> _
    Public isOpen As Boolean

    <DataMember> _
    Public depositCash As CashDrawerDepositDtc

    <DataMember> _
    Public depositCk As List(Of CashDrawerDepositCkDtc)

    <DataMember> _
    Public depositOther As List(Of TransactionDtc)

    <DataMember> _
    Public transactionList As List(Of TransactionDtc)

    <DataMember> _
    Public csTransTotalAmt As Decimal

    <DataMember> _
    Public ckTransTotalAmt As Decimal

    <DataMember> _
    Public othTransTotalAmt As Decimal

    <DataMember> _
    Public cdbPrintData As CashDrawerPrintDtc

    <DataMember> _
    Public printCount As Integer

    <DataMember> _
    Public cdbAccess As CashDrawerAccessDtc

    <DataMember> _
    Public storeList As StoreDtc()

End Class
