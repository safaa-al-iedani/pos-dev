﻿Imports System.Runtime.Serialization

<Serializable> _
<DataContract> _
Public Class CashDrawerDtc
    <DataMember> _
    Public cashDrawerCd As String

    <DataMember> _
    Public cashDrawerDesc As String

    <DataMember> _
    Public billCd As String
End Class
