﻿Imports System.Runtime.Serialization
<Serializable> _
<DataContract> _
Public Class CashDrawerPrintDtc
   
    <DataMember> _
    Public storeCd As String

    <DataMember> _
    Public cashDrawerCd As String

    <DataMember> _
    Public dt As Date

    <DataMember> _
    Public currencyCd As String

    <DataMember> _
    Public companyCd As String

    <DataMember> _
    Public depSlipNum As String

    <DataMember> _
    Public empCd As String

    <DataMember> _
    Public printDtTime As String
End Class
