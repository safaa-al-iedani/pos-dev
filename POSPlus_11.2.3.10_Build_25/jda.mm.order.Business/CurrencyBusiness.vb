﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class CurrencyBusiness
    Private objCurrDa As New CurrencyDa()
    Public Function GetAllCurrency(multiCrr As String, defCurrCd As String) As CurrencyDtc()
        Return objCurrDa.GetAllCurrency(multiCrr, defCurrCd)
    End Function

    Public Function GetDefaultCurrenctFromCurrencyRates() As CurrencyDtc
        Return objCurrDa.GetDefaultCurrenctFromCurrencyRates()
    End Function
End Class
