﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class CashDrawerDepositBusiness
    Private objCashDrawerDepositDa As New CashDrawerDepositDa()

    Public Function InsertCashDrawerCash(objCsDwrDepDtc As CashDrawerDepositDtc) As CashDrawerDepositDtc
        Return objCashDrawerDepositDa.InsertCashDrawerCash(objCsDwrDepDtc)
    End Function

    Public Sub InsertCDBCash(objCdbBal As CashDrawerBalDtc)
        objCashDrawerDepositDa.InsertCDBCash(objCdbBal)
    End Sub

    Public Function GetCashDrawerCash(cashDrawer As CashDrawerBalDtc) As CashDrawerBalDtc
        Return objCashDrawerDepositDa.GetCashDrawerCash(cashDrawer)
    End Function
End Class
