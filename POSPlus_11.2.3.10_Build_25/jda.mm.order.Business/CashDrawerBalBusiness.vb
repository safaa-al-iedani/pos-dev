﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class CashDrawerBalBusiness

    Private objCashDrawerBalDa As New CashDrawerBalDa()

    Public Function GetAllCashDrawer(storeCd As String, filterText As String) As CashDrawerDtc()
        Dim objCashDrawerDa As New CashDrawerDa()
        Return objCashDrawerDa.GetAllCashDrawer(storeCd, filterText)
    End Function

    Public Function GetCashDrawerCash(companyCd As String, storeCd As String, cashDrawerCd As String, transactionDate As Date) As CashDrawerCashCountDtc()
        Return objCashDrawerBalDa.GetCashDrawerCash(companyCd, storeCd, cashDrawerCd, transactionDate)
    End Function

    Public Sub InsertCashDrawerCash(objCdbCash As CashDrawerCashCountDtc)
        objCashDrawerBalDa.InsertCashDrawerCash(objCdbCash)
    End Sub

    Public Sub UpdateCashDrawerCash(objCdbCash As CashDrawerCashCountDtc)
        objCashDrawerBalDa.UpdateCashDrawerCash(objCdbCash)
    End Sub

    'Public Sub InsertCloseDrawerBalance(objCashDrawerBal As CashDrawerBalDtc)
    '    objCashDrawerBalDa.InsertCloseDrawerBalance(objCashDrawerBal)
    'End Sub

    Public Sub UpdateCloseDrawerBalance(objCashDrawerBal As CashDrawerBalDtc)
        objCashDrawerBalDa.UpdateCloseDrawerBalance(objCashDrawerBal)
    End Sub

    Public Sub InsertCashDrawerBalMop(objCashDrawerBal As CashDrawerBalDtc)
        objCashDrawerBalDa.InsertCashDrawerBalMop(objCashDrawerBal)
    End Sub

End Class
