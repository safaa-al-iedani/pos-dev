﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class StoreBusiness
    Private objStoreDa As New StoreDa()

    Public Function GetAllStore(empCd As String, hasAllStoreAccess As Boolean, filterText As String) As StoreDtc()
        Return objStoreDa.GetAllStore(empCd, hasAllStoreAccess, filterText)
    End Function
End Class
