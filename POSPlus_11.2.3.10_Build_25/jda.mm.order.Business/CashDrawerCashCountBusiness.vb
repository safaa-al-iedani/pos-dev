﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class CashDrawerCashCountBusiness
    Private objCashDrawerCashCountDa As New CashDrawerCashCountDa()

    Public Function GetCashDrawerCash(objCashDrawerBalDtc As CashDrawerBalDtc) As CashDrawerCashCountDtc()
        Return objCashDrawerCashCountDa.GetCashDrawerCash(objCashDrawerBalDtc)
    End Function
End Class
