﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class CdbBusiness
    Private objCdbDa As New CashDrawerBalDa()

    Public Sub IsCashDrawerOpen(objCdbBal As CashDrawerBalDtc)
        objCdbDa.IsCashDrawerOpen(objCdbBal)
    End Sub

    Public Sub GetOpenBalance(objCdbBal As CashDrawerBalDtc)
        objCdbDa.GetOpenBalance(objCdbBal)
    End Sub

    Public Sub GetCloseBalance(objCdbBal As CashDrawerBalDtc)
        objCdbDa.GetCloseBalance(objCdbBal)
    End Sub

    Public Sub SaveOpenBalance(objCdbBal As CashDrawerBalDtc)
        If objCdbBal.isOpen = True Then
            objCdbDa.UpdateOpenDrawerBalance(objCdbBal)
        Else
            objCdbDa.InsertOpenDrawerBalance(objCdbBal)
        End If
    End Sub
End Class
