﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class TransactionBusiness
    Private objTransactionDa As New TransactionDa()

    Public Sub GetCashDrawerOther(cashDrawer As CashDrawerBalDtc)
        objTransactionDa.GetCashDrawerOther(cashDrawer)
        objTransactionDa.GetTransactionBalance(cashDrawer)
    End Sub

    Public Function GetTransactionDetail(companyCd As String, storeCd As String, cashDrawerCd As String, postDate As Date, mop As String) As TransactionDtc()
        Return objTransactionDa.GetTransactionDetail(companyCd, storeCd, cashDrawerCd, postDate, mop)
    End Function

    Public Sub UpdateTransactionDetail(transList As List(Of TransactionDtc))
        objTransactionDa.UpdateTransactionDetail(transList)
    End Sub

    Public Function GetCdbCheck(companyCd As String, store As String, cashDrawerCd As String, postDate As Date) As TransactionDtc()
        Return objTransactionDa.GetCdbCheck(companyCd, store, cashDrawerCd, postDate)
    End Function

    Public Function GetCdbOther(empCd As String, companyCd As String, store As String, cashDrawerCd As String, postDate As Date) As TransactionDtc()
        Return objTransactionDa.GetCdbOther(empCd, companyCd, store, cashDrawerCd, postDate)
    End Function

    Public Function GetFullTransaction(companyCd As String, storeCd As String, cashDrawerCd As String, postDate As Date) As TransactionDtc()
        Return objTransactionDa.GetFullTransaction(companyCd, storeCd, cashDrawerCd, postDate)
    End Function

    Public Function GetTransactionBal(objCdbBal As CashDrawerBalDtc) As TransactionDtc()
        Return objTransactionDa.GetTransactionBal(objCdbBal)
    End Function

    Public Sub InsertCashDrawerCheck(objTransList As List(Of TransactionDtc))
        objTransactionDa.InsertCashDrawerCheck(objTransList)
    End Sub

    Public Sub DeleteCashDrawerCheck(objTrans As TransactionDtc)
        objTransactionDa.DeleteCashDrawerCheck(objTrans)
    End Sub

    Public Sub BalanceTranscation(companyCd As String, store As String, cashDrawerCd As String, postDate As Date)
        objTransactionDa.BalanceTranscation(companyCd, store, cashDrawerCd, postDate)
    End Sub

    Public Sub GetTranscationTotal(cdbBal As CashDrawerBalDtc)
        objTransactionDa.GetTranscationTotal(cdbBal)
    End Sub
End Class
