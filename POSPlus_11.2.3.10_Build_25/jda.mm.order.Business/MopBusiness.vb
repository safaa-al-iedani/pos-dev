﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class MopBusiness
    Private objMopDa As New MopDa()

    Public Function GetAllMop() As MopDtc()
        Return objMopDa.GetAllMop()
    End Function

    Public Function GetMopList() As MopDtc()
        Return objMopDa.GetMopList()
    End Function
End Class
