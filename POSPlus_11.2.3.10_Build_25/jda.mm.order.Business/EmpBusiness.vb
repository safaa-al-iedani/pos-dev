﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class EmpBusiness
    Private objEmpDa As New EmpDa()

    Public Sub GetEmpDetail(objEmp As EmpDtc)
        objEmpDa.GetEmpDetail(objEmp)
    End Sub

    Public Function GetAllCashier(empCd As String, hasAllCDBAccess As Boolean) As EmpDtc()
        Return objEmpDa.GetAllCashier(empCd, hasAllCDBAccess)
    End Function
    Public Function lucy_GetAllCashier(empCd As String, hasAllCDBAccess As Boolean, p_co_cd As String) As EmpDtc()
        'lucy copy from   GetAllCashier

        Return objEmpDa.lucy_GetAllCashier(empCd, hasAllCDBAccess, p_co_cd)

    End Function
End Class
