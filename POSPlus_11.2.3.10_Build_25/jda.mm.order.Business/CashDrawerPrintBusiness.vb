﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess
Public Class CashDrawerPrintBusiness
    Private objCdbPrintDa As New CashDrawerPrintDa()

    Public Sub SavePrintData(cashDrawer As CashDrawerBalDtc)
        objCdbPrintDa.SavePrintData(cashDrawer)
    End Sub

    Public Sub GetPrintCount(objCdbBal As CashDrawerBalDtc)
        objCdbPrintDa.GetPrintCount(objCdbBal)
    End Sub
End Class
