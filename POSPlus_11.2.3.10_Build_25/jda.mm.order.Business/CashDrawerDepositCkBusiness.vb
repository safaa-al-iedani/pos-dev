﻿Imports jda.mm.order.DataTransferClass
Imports jda.mm.order.DataAccess

Public Class CashDrawerDepositCkBusiness
    Private objCashDrawerDepositCkDa As New CashDrawerDepositCkDa()

    'Public Sub InsertCashDrawerCheck(objCsDwrDepCkList As List(Of CashDrawerDepositCkDtc))
    '    objCashDrawerDepositCkDa.InsertCashDrawerCheck(objCsDwrDepCkList)
    'End Sub

    Public Sub DeleteCashDrawerCheck(objCsDwrDepCk As CashDrawerDepositCkDtc)
        objCashDrawerDepositCkDa.DeleteCashDrawerCheck(objCsDwrDepCk)
    End Sub

    'Public Function GetCashDrawerCheck(objCsDwrDepCk As CashDrawerDepositCkDtc, empCd As String) As CashDrawerDepositCkDtc()
    '    Return objCashDrawerDepositCkDa.GetCashDrawerCheck(objCsDwrDepCk, empCd)
    'End Function

    Public Sub GetCashDrawerCk(cashDrawer As CashDrawerBalDtc)
        objCashDrawerDepositCkDa.GetCashDrawerCk(cashDrawer)
    End Sub

    Public Sub InsertCDBCheck(cashDwr As CashDrawerBalDtc)
        objCashDrawerDepositCkDa.InsertCDBCheck(cashDwr)
    End Sub

    Public Sub DeleteCDBCheck(cashDwr As CashDrawerBalDtc)
        objCashDrawerDepositCkDa.DeleteCDBCheck(cashDwr)
    End Sub
End Class
