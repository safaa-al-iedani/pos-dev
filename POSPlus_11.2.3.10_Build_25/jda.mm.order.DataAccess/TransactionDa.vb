﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text

Public Class TransactionDa
    Inherits BaseDac

    Public Function GetTransactionDetail(companyCd As String, storeCd As String, cashDrawerCd As String, postDate As Date, mop As String) As TransactionDtc()
        Dim transactionList As New List(Of TransactionDtc)

        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT A.AR_TRN_PK, M.MOP_CD, A.IVC_CD, A.POST_DT, A.TRN_TP_CD, T.DC_CD, A.AMT ")
            'qry.Append("FROM SETTLEMENT_MOP M JOIN AR_TRN A ON S.MOP_CD = A.MOP_CD ")
            'qry.Append("JOIN AR_TRN_TP T ON A.TRN_TP_CD = T.TRN_TP_CD ")
            qry.Append("FROM AR_TRN A LEFT JOIN MOP M ON A.MOP_CD = M.MOP_CD ")
            qry.Append("LEFT JOIN AR_TRN_TP T ON A.TRN_TP_CD = T.TRN_TP_CD ")
            qry.Append("WHERE A.STAT_CD = 'T' ")

            Select Case mop
                Case "CS"
                    qry.Append("AND NVL(M.MOP_TP, 'CS') = 'CS' ")
                Case "CK"
                    qry.Append("AND NVL(M.MOP_TP, 'CS') = 'CK' ")
                Case ""
                    qry.Append("AND NVL(M.MOP_TP, 'CS') NOT IN ('CS', 'CK', 'TC') ")
                Case Else
                    qry.Append("AND NVL(M.MOP_TP, 'CS') NOT IN ('CS', 'CK', 'TC') AND M.MOP_CD = '" + mop + "'")
            End Select

            qry.Append("AND A.CO_CD = :CO_CD AND NVL(A.PMT_STORE, A.ORIGIN_STORE) = :STORE_CD ")
            qry.Append("AND A.CSH_DWR_CD = :CSH_DWR_CD AND A.POST_DT = :POST_DT ")
            qry.Append("ORDER BY A.TRN_TP_CD, A.IVC_CD")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = postDate
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        Dim objTransactionDtc As New TransactionDtc
                        ParseTransaction(objTransactionDtc, dr)
                        transactionList.Add(objTransactionDtc)
                    End While
                End Using
            End Using

        Catch ex As Exception

        End Try

        Return transactionList.ToArray
    End Function

    Public Sub UpdateTransactionDetail(transList As List(Of TransactionDtc))
        Try
            For Each objTrans As TransactionDtc In transList

                Dim qry As New StringBuilder
                qry.Append("UPDATE AR_TRN SET MOP_CD = :MOP_CD, AMT = :AMT ")
                qry.Append("WHERE STAT_CD = 'T' AND CO_CD = :CO_CD ")
                qry.Append("AND NVL (PMT_STORE, ORIGIN_STORE) = :STORE_CD ")
                qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND POST_DT = :POST_DT ")
                qry.Append("AND IVC_CD = :IVC_CD ")
                qry.Append("AND TRN_TP_CD = :TRN_TP_CD ")
                qry.Append("AND AR_TRN_PK = :AR_TRN_PK")

                Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                    Dim param As DbParameter = cmd.CreateParameter()
                    param.ParameterName = "CO_CD"
                    param.Value = objTrans.companyCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "STORE_CD"
                    param.Value = objTrans.storeCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "CSH_DWR_CD"
                    param.Value = objTrans.cashDrawerCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "POST_DT"
                    param.Value = objTrans.postDate
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "IVC_CD"
                    param.Value = objTrans.invoice
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "TRN_TP_CD"
                    param.Value = objTrans.tranTpCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "MOP_CD"
                    param.Value = objTrans.mopCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "AMT"
                    param.Value = objTrans.amount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "AR_TRN_PK"
                    param.Value = objTrans.tranPK
                    cmd.Parameters.Add(param)

                    cmd.ExecuteNonQuery()
                End Using

            Next
        Catch ex As Exception

        End Try
    End Sub

    Public Function GetCdbCheck(companyCd As String, store As String, cashDrawerCd As String, postDate As Date) As TransactionDtc()
        Dim transactionList As New List(Of TransactionDtc)

        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT SEQ_NUM, CK_NAME, CK_AMT FROM CSH_DWR_DEP_CK ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND DT = :DT ORDER BY SEQ_NUM DESC")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = store
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = postDate
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objTransaction As New TransactionDtc
                        ParseTransaction(objTransaction, dr)
                        transactionList.Add(objTransaction)
                    End While

                End Using
            End Using
        Catch ex As Exception

        End Try

        Return transactionList.ToArray
    End Function

    Public Sub GetCashDrawerOther(objCdbBal As CashDrawerBalDtc)

        objCdbBal.depositOther = New List(Of TransactionDtc)

        Try
            Dim qry As New StringBuilder

            qry.Append("SELECT A.MOP_CD, M.DES MOP_DES, SUM(DECODE(T.DC_CD,'C',  A.AMT, -A.AMT)) AMT ")
            qry.Append("FROM AR_TRN A JOIN MOP M ON A.MOP_CD = M.MOP_CD ")
            qry.Append("JOIN AR_TRN_TP T ON A.TRN_TP_CD  = T.TRN_TP_CD ")
            qry.Append("WHERE  A.MOP_CD IS NOT NULL AND A.STAT_CD = 'T' AND M.DEPOSIT_FLAG = 'Y' ") 'A.EMP_CD_CSHR =:EMP_INIT AND
            qry.Append("AND NVL(M.MOP_TP, 'CS') NOT IN ('CS', 'CK',  'TC') ")
            'qry.Append("AND NVL(A.BALANCED,'N') = 'N' ")
            qry.Append("AND A.CO_CD = :CO_CD AND NVL(A.PMT_STORE, A.ORIGIN_STORE) = :STORE ")
            qry.Append("AND A.CSH_DWR_CD = :CSH_DWR_CD AND A.POST_DT = :POST_DT ")
            qry.Append("GROUP BY A.MOP_CD, M.DES")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                'param = cmd.CreateParameter()
                'param.ParameterName = "EMP_INIT"
                'param.Value = empCd
                'cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objTransaction As New TransactionDtc
                        ParseTransaction(objTransaction, dr)
                        objCdbBal.depositOther.Add(objTransaction)
                    End While

                End Using
            End Using

        Catch ex As Exception

        End Try
    End Sub

    Public Sub GetTransactionBalance(objCdbBal As CashDrawerBalDtc)
        objCdbBal.transactionList = New List(Of TransactionDtc)

        Try
            Dim sql As New StringBuilder

            sql.Append("SELECT NVL (M.MOP_TP, 'CS') MOP_TP, ")
            sql.Append("SUM (DECODE (T.DC_CD, 'C', A.AMT, -A.AMT)) MOPAMT ")
            sql.Append("FROM AR_TRN A JOIN MOP M ON A.MOP_CD = M.MOP_CD ")
            sql.Append("JOIN AR_TRN_TP T ")
            sql.Append("ON A.TRN_TP_CD = T.TRN_TP_CD ")
            sql.Append("WHERE A.MOP_CD IS NOT NULL ")
            sql.Append("AND A.STAT_CD = 'T' AND M.DEPOSIT_FLAG = 'Y' ")
            sql.Append("AND NVL (A.BALANCED, 'N') = 'N' ")
            sql.Append("AND A.CO_CD = :CO_CD ")
            sql.Append("AND NVL (A.PMT_STORE, A.ORIGIN_STORE) = :STORE ")
            sql.Append("AND A.CSH_DWR_CD = :CSH_DWR_CD AND A.POST_DT = :POST_DT ")
            sql.Append("GROUP BY M.MOP_TP")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, sql.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objTransaction As New TransactionDtc
                        ParseTransaction(objTransaction, dr)
                        objCdbBal.transactionList.Add(objTransaction)
                    End While

                End Using
            End Using

        Catch ex As Exception

        End Try

    End Sub

    Public Function GetCdbOther(empCd As String, companyCd As String, store As String, cashDrawerCd As String, postDate As Date) As TransactionDtc()
        Dim transactionList As New List(Of TransactionDtc)

        Try
            Dim qry As New StringBuilder
            'qry.Append("SELECT M.MOP_CD, M.DES MOP_DES, A.AMT + NVL(A.CHNG_OUT,0) AMT FROM AR_TRN A LEFT JOIN AR_TRN_TP T ON A.TRN_TP_CD = T.TRN_TP_CD ")
            'qry.Append("LEFT JOIN MOP M ON A.MOP_CD = M.MOP_CD WHERE STAT_CD = 'T' AND NVL(BALANCED,'N') = 'N' AND A.CO_CD = :CO_CD ")
            'qry.Append("AND NVL(PMT_STORE, ORIGIN_STORE) = :STORE AND A.CSH_DWR_CD = :CSH_DWR_CD AND POST_DT = :POST_DT ")
            'qry.Append("AND EMP_CD_CSHR = :EMP_INIT AND M.MOP_TP NOT IN('CK', 'CS', 'TC')")

            qry.Append("SELECT A.MOP_CD, M.DES MOP_DES, SUM(DECODE(T.DC_CD,'C',  A.AMT, -A.AMT)) AMT ")
            qry.Append("FROM AR_TRN A JOIN MOP M ON A.MOP_CD = M.MOP_CD ")
            qry.Append("JOIN AR_TRN_TP T ON A.TRN_TP_CD  = T.TRN_TP_CD ")
            qry.Append("WHERE  A.MOP_CD IS NOT NULL AND A.STAT_CD = 'T' AND M.DEPOSIT_FLAG = 'Y' ") 'A.EMP_CD_CSHR =:EMP_INIT AND
            qry.Append("AND NVL(M.MOP_TP, 'CS') NOT IN ('CS', 'CK',  'TC') ")
            qry.Append("AND NVL(A.BALANCED,'N') = 'N' ")
            qry.Append("AND A.CO_CD = :CO_CD AND NVL(A.PMT_STORE, A.ORIGIN_STORE) = :STORE ")
            qry.Append("AND A.CSH_DWR_CD = :CSH_DWR_CD AND A.POST_DT = :POST_DT ")
            qry.Append("GROUP BY A.MOP_CD, M.DES")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE"
                param.Value = store
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = postDate
                cmd.Parameters.Add(param)

                'param = cmd.CreateParameter()
                'param.ParameterName = "EMP_INIT"
                'param.Value = empCd
                'cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objTransaction As New TransactionDtc
                        ParseTransaction(objTransaction, dr)
                        transactionList.Add(objTransaction)
                    End While

                End Using
            End Using
        Catch ex As Exception

        End Try

        Return transactionList.ToArray
    End Function

    Public Function GetFullTransaction(companyCd As String, storeCd As String, cashDrawerCd As String, postDate As Date) As TransactionDtc()
        Dim transactionList As New List(Of TransactionDtc)
        Try
            Dim sql As New StringBuilder

            sql.Append("SELECT A.MOP_CD, M.DES MOP_DES, NVL (M.MOP_TP, 'CS') MOP_TP, ")
            sql.Append("SUM (DECODE (T.DC_CD, 'C', A.AMT, -A.AMT)) MOPAMT ")
            sql.Append("FROM AR_TRN A JOIN MOP M ON A.MOP_CD = M.MOP_CD ")
            sql.Append("JOIN AR_TRN_TP T ")
            sql.Append("ON A.TRN_TP_CD = T.TRN_TP_CD ")
            sql.Append("WHERE A.MOP_CD IS NOT NULL ")
            sql.Append("AND A.STAT_CD = 'T' AND M.DEPOSIT_FLAG = 'Y' ")
            sql.Append("AND NVL (A.BALANCED, 'N') = 'N' ")
            sql.Append("AND A.CO_CD = :CO_CD ")
            sql.Append("AND NVL (A.PMT_STORE, A.ORIGIN_STORE) = :STORE ")
            sql.Append("AND A.CSH_DWR_CD = :CSH_DWR_CD AND A.POST_DT = :POST_DT")
            sql.Append("GROUP BY A.MOP_CD, M.DES, M.MOP_TP")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, sql.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE"
                param.Value = storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = postDate
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objTransaction As New TransactionDtc
                        ParseTransaction(objTransaction, dr)
                        transactionList.Add(objTransaction)
                    End While

                End Using
            End Using
        Catch ex As Exception

        End Try

        Return transactionList.ToArray
    End Function

    Public Function GetTransactionBal(objCdbBal As CashDrawerBalDtc) As TransactionDtc()
        Dim transactionList As New List(Of TransactionDtc)

        Try
            Dim sql As New StringBuilder

            sql.Append("SELECT NVL (M.MOP_TP, 'CS') MOP_TP, ")
            sql.Append("SUM (DECODE (T.DC_CD, 'C', A.AMT, -A.AMT)) MOPAMT ")
            sql.Append("FROM AR_TRN A JOIN MOP M ON A.MOP_CD = M.MOP_CD ")
            sql.Append("JOIN AR_TRN_TP T ")
            sql.Append("ON A.TRN_TP_CD = T.TRN_TP_CD ")
            sql.Append("WHERE A.MOP_CD IS NOT NULL ")
            sql.Append("AND A.STAT_CD = 'T' AND M.DEPOSIT_FLAG = 'Y' ")
            sql.Append("AND NVL (A.BALANCED, 'N') = 'N' ")
            sql.Append("AND A.CO_CD = :CO_CD ")
            sql.Append("AND NVL (A.PMT_STORE, A.ORIGIN_STORE) = :STORE ")
            sql.Append("AND A.CSH_DWR_CD = :CSH_DWR_CD AND A.POST_DT = :POST_DT ")
            sql.Append("GROUP BY M.MOP_TP")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, sql.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objTransaction As New TransactionDtc
                        ParseTransaction(objTransaction, dr)
                        transactionList.Add(objTransaction)
                    End While

                End Using
            End Using

        Catch ex As Exception

        End Try

        Return transactionList.ToArray
    End Function

    Public Sub InsertCashDrawerCheck(objTransList As List(Of TransactionDtc))

        Try
            If objTransList.Count > 0 Then

                Dim qryBuild As New StringBuilder
                qryBuild.Append("INSERT ALL ")

                For i As Integer = 0 To objTransList.Count - 1
                    Dim objCdb As New TransactionDtc
                    objCdb = objTransList(i)

                    qryBuild.Append("INTO CSH_DWR_DEP_CK (CO_CD, STORE_CD, CSH_DWR_CD, DT, CURR_CD, CK_AMT, CK_NAME, SEQ_NUM) ")
                    qryBuild.Append("VALUES ('" & objCdb.companyCd & "', '" & objCdb.storeCd & "', '" & objCdb.cashDrawerCd & "', ")
                    qryBuild.Append("TO_DATE('" & objCdb.postDate & "', 'DD/MM/YYYY'),'")
                    qryBuild.Append(objCdb.currCd & "', " & objCdb.amount & ", '" & objCdb.checkName & "'," & i & ") ")
                Next

                qryBuild.Append("SELECT * FROM dual")

                Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qryBuild.ToString)
                    cmd.ExecuteNonQuery()
                End Using

            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub DeleteCashDrawerCheck(objTrans As TransactionDtc)
        Dim qry As New StringBuilder
        qry.Append("DELETE FROM CSH_DWR_DEP_CK WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD AND CSH_DWR_CD = :CSH_DWR_CD ")
        qry.Append("AND DT =  :DT ")

        Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
            Dim param As DbParameter = cmd.CreateParameter()
            param.ParameterName = "CO_CD"
            param.Value = objTrans.companyCd
            cmd.Parameters.Add(param)

            param = cmd.CreateParameter()
            param.ParameterName = "STORE_CD"
            param.Value = objTrans.storeCd
            cmd.Parameters.Add(param)

            param = cmd.CreateParameter()
            param.ParameterName = "CSH_DWR_CD"
            param.Value = objTrans.cashDrawerCd
            cmd.Parameters.Add(param)

            param = cmd.CreateParameter()
            param.ParameterName = "DT"
            param.Value = objTrans.postDate
            cmd.Parameters.Add(param)

            cmd.ExecuteNonQuery()

        End Using
    End Sub

    Public Sub BalanceTranscation(companyCd As String, store As String, cashDrawerCd As String, postDate As Date)
        Try
            Dim sql As String = "LOCK TABLE AR_TRN IN SHARE UPDATE MODE"
            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, sql)
                cmd.ExecuteNonQuery()
            End Using

            Dim qry As New StringBuilder
            qry.Append("UPDATE AR_TRN SET BALANCED = 'Y' ")
            qry.Append("WHERE CO_CD = :CO_CD ") 'AND MOP_CD = :MOP_CD 
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND POST_DT = :POST_DT ")
            qry.Append("AND STAT_CD = 'T' AND PMT_STORE = :PMT_STORE ")
            qry.Append("AND NVL (BALANCED, 'N') = 'N'")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = companyCd
                cmd.Parameters.Add(param)

                'param = cmd.CreateParameter()
                'param.ParameterName = "MOP_CD"
                'param.Value = mopCd
                'cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = postDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "PMT_STORE"
                param.Value = store
                cmd.Parameters.Add(param)

                cmd.ExecuteNonQuery()
            End Using

        Catch ex As Exception

        End Try
    End Sub

    Public Sub GetTranscationTotal(cdbBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT Mop_Tp, SUM(AMT) TrAmount FROM (")
            qry.Append("SELECT CASE NVL(S.MOP_TP,'CS') WHEN 'CS' THEN 'CS' ")
            qry.Append("WHEN 'CK' THEN 'CK' ELSE 'OTH' END MOP_TP, ")
            qry.Append("CASE T.DC_CD WHEN 'C' THEN AMT ELSE -1 * AMT END AMT ")
            qry.Append("FROM SETTLEMENT_MOP S,  AR_TRN A, AR_TRN_TP T ")
            qry.Append("WHERE S.MOP_CD = A.MOP_CD AND A.TRN_TP_CD = T.TRN_TP_CD ")
            qry.Append("AND A.STAT_CD = 'T' AND A.CO_CD = :CO_CD ")
            qry.Append("AND NVL (A.PMT_STORE, A.ORIGIN_STORE) = :STORE_CD ")
            qry.Append("AND A.CSH_DWR_CD = :CSH_DWR_CD AND A.POST_DT = :POST_DT")
            qry.Append(") GROUP BY MOP_TP")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = cdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = cdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = cdbBal.transactionDate
                cmd.Parameters.Add(param)

                Dim trAmt As String
                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        trAmt = dr("TrAmount").ToString
                        If Not String.IsNullOrEmpty(trAmt) Then

                            Dim value As Decimal = 0
                            Decimal.TryParse(trAmt, value)
                            Select Case dr("Mop_Tp").ToString().Trim()
                                Case "CS"
                                    cdbBal.csTransTotalAmt = value
                                Case "CK"
                                    cdbBal.ckTransTotalAmt = value
                                Case Else
                                    cdbBal.othTransTotalAmt = value
                            End Select
                        End If
                    End While
                End Using
            End Using

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ParseTransaction(objTransaction As TransactionDtc, dr As IDataReader)

        If ColumnExists(dr, "AR_TRN_PK") Then
            objTransaction.tranPK = MyBase.GetDataValue(Of Decimal)(dr, "AR_TRN_PK")
        End If

        If ColumnExists(dr, "CK_NAME") Then
            objTransaction.checkName = MyBase.GetDataValue(Of String)(dr, "CK_NAME")
        End If
        If ColumnExists(dr, "CK_AMT") Then
            objTransaction.amount = MyBase.GetDataValue(Of Decimal)(dr, "CK_AMT")
        End If
        If ColumnExists(dr, "SEQ_NUM") Then
            objTransaction.seqNo = MyBase.GetDataValue(Of Integer)(dr, "SEQ_NUM")
        End If
        If ColumnExists(dr, "CUST_NAME") Then
            objTransaction.customerName = MyBase.GetDataValue(Of String)(dr, "CUST_NAME")
        End If
        If ColumnExists(dr, "MOP_CD") Then
            objTransaction.mopCd = MyBase.GetDataValue(Of String)(dr, "MOP_CD")
        End If
        If ColumnExists(dr, "MOP_DES") Then
            objTransaction.mopDesc = MyBase.GetDataValue(Of String)(dr, "MOP_DES")
        End If
        If ColumnExists(dr, "MOP_TP") Then
            objTransaction.mopTp = MyBase.GetDataValue(Of String)(dr, "MOP_TP")
        End If
        If ColumnExists(dr, "IVC_CD") Then
            objTransaction.invoice = MyBase.GetDataValue(Of String)(dr, "IVC_CD")
        End If
        If ColumnExists(dr, "INVOICE") Then
            objTransaction.invoice = MyBase.GetDataValue(Of String)(dr, "INVOICE")
        End If
        If ColumnExists(dr, "POST_DT") Then
            objTransaction.postDate = MyBase.GetDataValue(Of Date)(dr, "POST_DT")
        End If
        If ColumnExists(dr, "TRN_TP_CD") Then
            objTransaction.tranTpCd = MyBase.GetDataValue(Of String)(dr, "TRN_TP_CD")
        End If
        If ColumnExists(dr, "DC_CD") Then
            objTransaction.dcCd = MyBase.GetDataValue(Of String)(dr, "DC_CD")
        End If
        If ColumnExists(dr, "AMT") Then
            objTransaction.amount = MyBase.GetDataValue(Of Decimal)(dr, "AMT")
        End If
        If ColumnExists(dr, "MOPAMT") Then
            objTransaction.mopAmount = MyBase.GetDataValue(Of Decimal)(dr, "MOPAMT")
        End If
    End Sub

End Class
