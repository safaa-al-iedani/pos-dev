﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text

Public Class MopDa
    Inherits BaseDac

    Public Function GetMopList() As MopDtc()
        Dim mopList As New List(Of MopDtc)

        Try
            Dim qry As New StringBuilder

            qry.Append("SELECT MOP_CD, DES MOP_DES FROM MOP ")
            qry.Append("WHERE DEPOSIT_FLAG = 'Y' ORDER BY MOP_CD")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        Dim objMop As New MopDtc
                        ParseMop(objMop, dr)
                        mopList.Add(objMop)
                    End While
                End Using
            End Using

        Catch ex As Exception

        End Try

        Return mopList.ToArray
    End Function

    Public Function GetAllMop() As MopDtc()
        Dim mopList As New List(Of MopDtc)

        Try
            Dim qry As New StringBuilder

            qry.Append("SELECT MOP_CD, DES MOP_DES, MOP_TP FROM MOP ")
            qry.Append("WHERE DEPOSIT_FLAG = 'Y' AND NVL(MOP_TP, 'CS') ")
            qry.Append("NOT IN ('CS', 'CK',  'TC') ORDER BY MOP_CD")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        Dim objMop As New MopDtc
                        ParseMop(objMop, dr)
                        mopList.Add(objMop)
                    End While
                End Using
            End Using

        Catch ex As Exception

        End Try

        Return mopList.ToArray
    End Function

    Private Sub ParseMop(objMop As MopDtc, dr As IDataReader)
        If ColumnExists(dr, "MOP_CD") Then
            objMop.mopCd = MyBase.GetDataValue(Of String)(dr, "MOP_CD")
        End If
        If ColumnExists(dr, "MOP_DES") Then
            objMop.mopDesc = MyBase.GetDataValue(Of String)(dr, "MOP_DES")
        End If
        If ColumnExists(dr, "MOP_TP") Then
            objMop.mopTp = MyBase.GetDataValue(Of String)(dr, "MOP_TP")
        End If
    End Sub

End Class
