﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text

Public Class CashDrawerCashCountDa
    Inherits BaseDac

    Public Function GetCashDrawerCash(objCashDrawerBalDtc As CashDrawerBalDtc) As CashDrawerCashCountDtc()
        Dim CashDrawerCashCountDtcList As New List(Of CashDrawerCashCountDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT COUNT_TYPE, ONE_CNT, TWO_CNT, FIVE_CNT, TEN_CNT, TWENTY_CNT")
            qry.Append(", FIFTY_CNT, ONE_HUNDRED_CNT, ONE_THOUSAND_CNT")
            qry.Append(", PENNIES_CNT, NICKELS_CNT, DIMES_CNT, QUARTERS_CNT, HALF_CNT, ONE_COIN_CNT")
            qry.Append(", CURRENCY_CD, DEPOSIT_BAG, DEPOSIT_SLIP ")
            qry.Append("FROM CSH_DWR_CASH_COUNT ")
            qry.Append("WHERE CO_CD= :CO_CD AND STORE_CD = :STORE_CD AND CSH_DWR_CD = :CSH_DWR_CD ")
            qry.Append("AND DT = :DT AND EMP_CD = :EMP_CD ")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCashDrawerBalDtc.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCashDrawerBalDtc.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCashDrawerBalDtc.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD"
                param.Value = objCashDrawerBalDtc.empCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = objCashDrawerBalDtc.transactionDate
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        Dim objCashDrawerCashCount As New CashDrawerCashCountDtc
                        ParseDrawerCashCount(objCashDrawerCashCount, dr)
                        CashDrawerCashCountDtcList.Add(objCashDrawerCashCount)
                    End While
                End Using
            End Using
        Catch ex As Exception

        End Try

        Return CashDrawerCashCountDtcList.ToArray
    End Function

    Private Sub ParseDrawerCashCount(objCashDrawerCashCount As CashDrawerCashCountDtc, dr As IDataReader)
        If ColumnExists(dr, "COUNT_TYPE") Then
            objCashDrawerCashCount.countType = MyBase.GetDataValue(Of String)(dr, "COUNT_TYPE")
        End If
        If ColumnExists(dr, "ONE_CNT") Then
            objCashDrawerCashCount.oneCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_CNT")
        End If
        If ColumnExists(dr, "TWO_CNT") Then
            objCashDrawerCashCount.twoCount = MyBase.GetDataValue(Of Decimal)(dr, "TWO_CNT")
        End If
        If ColumnExists(dr, "FIVE_CNT") Then
            objCashDrawerCashCount.fiveCount = MyBase.GetDataValue(Of Decimal)(dr, "FIVE_CNT")
        End If
        If ColumnExists(dr, "TEN_CNT") Then
            objCashDrawerCashCount.tenCount = MyBase.GetDataValue(Of Decimal)(dr, "TEN_CNT")
        End If
        If ColumnExists(dr, "TWENTY_CNT") Then
            objCashDrawerCashCount.twentyCount = MyBase.GetDataValue(Of Decimal)(dr, "TWENTY_CNT")
        End If
        If ColumnExists(dr, "FIFTY_CNT") Then
            objCashDrawerCashCount.fiftyCount = MyBase.GetDataValue(Of Decimal)(dr, "FIFTY_CNT")
        End If
        If ColumnExists(dr, "ONE_HUNDRED_CNT") Then
            objCashDrawerCashCount.oneHundredCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_HUNDRED_CNT")
        End If
        If ColumnExists(dr, "ONE_THOUSAND_CNT") Then
            objCashDrawerCashCount.oneThousandCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_THOUSAND_CNT")
        End If
        If ColumnExists(dr, "PENNIES_CNT") Then
            objCashDrawerCashCount.penniesCount = MyBase.GetDataValue(Of Decimal)(dr, "PENNIES_CNT")
        End If
        If ColumnExists(dr, "NICKELS_CNT") Then
            objCashDrawerCashCount.nickelsCount = MyBase.GetDataValue(Of Decimal)(dr, "NICKELS_CNT")
        End If
        If ColumnExists(dr, "DIMES_CNT") Then
            objCashDrawerCashCount.dimesCount = MyBase.GetDataValue(Of Decimal)(dr, "DIMES_CNT")
        End If
        If ColumnExists(dr, "QUARTERS_CNT") Then
            objCashDrawerCashCount.quartersCount = MyBase.GetDataValue(Of Decimal)(dr, "QUARTERS_CNT")
        End If
        If ColumnExists(dr, "HALF_CNT") Then
            objCashDrawerCashCount.halfCount = MyBase.GetDataValue(Of Decimal)(dr, "HALF_CNT")
        End If
        If ColumnExists(dr, "ONE_COIN_CNT") Then
            objCashDrawerCashCount.oneCoinCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_COIN_CNT")
        End If
        If ColumnExists(dr, "CURRENCY_CD") Then
            objCashDrawerCashCount.currencyCd = MyBase.GetDataValue(Of String)(dr, "CURRENCY_CD")
        End If
        If ColumnExists(dr, "DEPOSIT_BAG") Then
            objCashDrawerCashCount.depositBag = MyBase.GetDataValue(Of String)(dr, "DEPOSIT_BAG")
        End If
        If ColumnExists(dr, "DEPOSIT_SLIP") Then
            objCashDrawerCashCount.depositSlip = MyBase.GetDataValue(Of String)(dr, "DEPOSIT_SLIP")
        End If
    End Sub

End Class