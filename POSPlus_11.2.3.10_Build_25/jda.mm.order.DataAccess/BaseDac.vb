﻿Imports System.Configuration
'Imports Microsoft.Practices.EnterpriseLibrary.Data
'Imports Microsoft.Practices.EnterpriseLibrary.Data.Oracle
Imports System.Data.OracleClient
Imports System.Data.Common


Public MustInherit Class BaseDac
    ''' <summary>
    ''' Return the "ERP" Connection String from the web.config
    ''' </summary>
    ''' <value></value>
    ''' <returns>Connection String for "ERP"</returns>
    ''' <remarks></remarks>
    Protected ReadOnly Property ConnectionString() As String
        Get
            Return ConfigurationManager.ConnectionStrings("ERP").ConnectionString
        End Get
    End Property

    ''' <summary>
    ''' Return the Database
    ''' </summary>
    ''' <value></value>
    ''' <returns>Database</returns>
    ''' <remarks>Initiate the Database</remarks>
    Protected ReadOnly Property erpDB() As OracleConnection
        Get
            Dim rv As New OracleConnection(ConnectionString)
            rv.Open()
            Return rv
            'Return New DatabaseProviderFactory().Create(ConnectionString)
        End Get
    End Property

    'Protected Function BuildCommand(sql As String) As OracleCommand
    '    Dim rv As New OracleCommand(sql, erpDB)
    '    Return rv
    'End Function


    Protected Function BuildCommand(db As DbConnection, sql As String) As OracleCommand
        Dim rv As New OracleCommand(sql, db)
        Return rv
    End Function

    Protected Function BuildReader(cmd As DbCommand) As OracleDataReader
        Dim rv As OracleDataReader = cmd.ExecuteReader()
        Return rv
    End Function



    ''' <summary>
    ''' Cast the value and fill to the reader
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="vobjDataReader">Data reader</param>
    ''' <param name="vstrColumnName">Column Name</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetDataValue(Of T)(vobjDataReader As IDataReader, vstrColumnName As String) As T
        ' NOTE: GetOrdinal() is used to automatically determine where the column
        '       is physically located in the database table. This allows the
        '       schema to be changed without affecting this piece of code.
        '       This of course sacrifices a little performance for maintainability.

        Dim intOrdinal As Integer = vobjDataReader.GetOrdinal(vstrColumnName)

        If Not vobjDataReader.IsDBNull(intOrdinal) Then
            Return DirectCast(vobjDataReader.GetValue(intOrdinal), T)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Cast the value and fill to the reader
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="vobjDataReader">Data reader</param>
    ''' <param name="vintColumnIndex">Column Index</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetDataValue(Of T)(vobjDataReader As IDataReader, vintColumnIndex As Integer) As T
        If Not vobjDataReader.IsDBNull(vintColumnIndex) Then
            Return DirectCast(vobjDataReader.GetValue(vintColumnIndex), T)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Check the column existance to this datareader
    ''' </summary>
    ''' <param name="reader">Data Reader</param>
    ''' <param name="columnName">Column Name</param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Check the column existance to this datareader</remarks>
    Public Function ColumnExists(reader As IDataReader, columnName As String) As Boolean
        For i As Integer = 0 To reader.FieldCount - 1
            If reader.GetName(i) = columnName Then
                Return True
            End If
        Next

        Return False
    End Function
End Class