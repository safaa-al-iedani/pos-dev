﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text

Public Class CashDrawerDepositDa
    Inherits BaseDac

    Public Function GetCashDrawerCash(cashDrawer As CashDrawerBalDtc) As CashDrawerBalDtc

        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT ONE_CNT, TWO_CNT, FIVE_CNT, TEN_CNT, TWENTY_CNT, ")
            qry.Append("FIFTY_CNT, ONE_HUNDRED_CNT, ONE_THOU_CNT, PENNIES_CNT, ")
            qry.Append("NICKELS_CNT, DIMES_CNT, QUARTERS_CNT, HALF_DOL_CNT, ")
            qry.Append("ONE_COIN_CNT, DEP_BAG, DEP_SLIP_NUM, LST_ACT_DT_TIME, VERSION ")
            qry.Append("FROM CSH_DWR_DEP WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND DT = :DT ")
            qry.Append("AND EMP_CD_CSHR = :EMP_CD_CSHR AND CURR_CD = :CURR_CD ")
            qry.Append("ORDER BY LST_ACT_DT_TIME")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = cashDrawer.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = cashDrawer.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cashDrawer.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = cashDrawer.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD_CSHR"
                param.Value = cashDrawer.empCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = cashDrawer.currencyCd
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        cashDrawer.depositCash = New CashDrawerDepositDtc
                        ParseCashDrawerDeposit(cashDrawer.depositCash, dr)
                    End While
                End Using
            End Using

        Catch ex As Exception

        End Try

        Return cashDrawer
    End Function

    Public Function InsertCashDrawerCash(objCsDwrDepDtc As CashDrawerDepositDtc) As CashDrawerDepositDtc
        Try
            GetDepositSlipNumber(objCsDwrDepDtc)

            If UpdateCashDrawerCash(objCsDwrDepDtc) < 1 Then

                If objCsDwrDepDtc.depositSlip < 0 Then objCsDwrDepDtc.depositSlip = 0

                Dim qry As New StringBuilder
                qry.Append("INSERT INTO CSH_DWR_DEP ")
                qry.Append("(CO_CD, STORE_CD, CSH_DWR_CD, DT, EMP_CD_CSHR, CURR_CD, ")
                qry.Append("ONE_CNT, TWO_CNT, FIVE_CNT, TEN_CNT, TWENTY_CNT, FIFTY_CNT, ")
                qry.Append("ONE_HUNDRED_CNT, ONE_THOU_CNT, PENNIES_CNT, NICKELS_CNT, ")
                qry.Append("DIMES_CNT, QUARTERS_CNT, HALF_DOL_CNT, ONE_COIN_CNT, ")
                qry.Append("DEP_BAG, DEP_SLIP_NUM , LST_ACT_DT_TIME) VALUES ")
                qry.Append("(:CO_CD, :STORE_CD, :CSH_DWR_CD, :DT, :EMP_CD_CSHR, :CURR_CD, ")
                qry.Append(":ONE_CNT, :TWO_CNT, :FIVE_CNT, :TEN_CNT, :TWENTY_CNT, :FIFTY_CNT, ")
                qry.Append(":ONE_HUNDRED_CNT, :ONE_THOU_CNT, :PENNIES_CNT, :NICKELS_CNT, ")
                qry.Append(":DIMES_CNT, :QUARTERS_CNT, :HALF_DOL_CNT, :ONE_COIN_CNT, ")
                qry.Append(":DEP_BAG_CD, :DEP_SLIP_NUM, SYSDATE)")

                Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                    Dim param As DbParameter = cmd.CreateParameter()
                    param.ParameterName = "CO_CD"
                    param.Value = objCsDwrDepDtc.companyCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "STORE_CD"
                    param.Value = objCsDwrDepDtc.storeCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "CSH_DWR_CD"
                    param.Value = objCsDwrDepDtc.cashDrawerCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "DT"
                    param.Value = objCsDwrDepDtc.transactionDate
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "EMP_CD_CSHR"
                    param.Value = objCsDwrDepDtc.empCdCashier
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "CURR_CD"
                    param.Value = objCsDwrDepDtc.currencyCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "ONE_CNT"
                    param.Value = objCsDwrDepDtc.oneCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "TWO_CNT"
                    param.Value = objCsDwrDepDtc.twoCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "FIVE_CNT"
                    param.Value = objCsDwrDepDtc.fiveCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "TEN_CNT"
                    param.Value = objCsDwrDepDtc.tenCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "TWENTY_CNT"
                    param.Value = objCsDwrDepDtc.twentyCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "FIFTY_CNT"
                    param.Value = objCsDwrDepDtc.fiftyCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "ONE_HUNDRED_CNT"
                    param.Value = objCsDwrDepDtc.oneHundredCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "ONE_THOU_CNT"
                    param.Value = objCsDwrDepDtc.oneThousandCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "PENNIES_CNT"
                    param.Value = objCsDwrDepDtc.penniesCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "NICKELS_CNT"
                    param.Value = objCsDwrDepDtc.nickelsCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "DIMES_CNT"
                    param.Value = objCsDwrDepDtc.dimesCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "QUARTERS_CNT"
                    param.Value = objCsDwrDepDtc.quartersCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "HALF_DOL_CNT"
                    param.Value = objCsDwrDepDtc.halfCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "ONE_COIN_CNT"
                    param.Value = objCsDwrDepDtc.oneCoinCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "DEP_BAG_CD"
                    param.Value = objCsDwrDepDtc.depositBag
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "DEP_SLIP_NUM"
                    param.Value = objCsDwrDepDtc.depositSlip + 1
                    cmd.Parameters.Add(param)

                    cmd.ExecuteNonQuery()
                End Using

            End If
        Catch ex As Exception

        End Try
        Return objCsDwrDepDtc
    End Function

    Public Sub InsertCDBCash(objCdbBal As CashDrawerBalDtc)

        Try
            GetDepSlipNumber(objCdbBal)

            If UpdateCDBCash(objCdbBal) < 1 Then

                If (objCdbBal.depositCash.depositSlip < 0) Then
                    objCdbBal.depositCash.depositSlip = 0
                End If

                Dim qry As New StringBuilder
                qry.Append("INSERT INTO CSH_DWR_DEP ")
                qry.Append("(CO_CD, STORE_CD, CSH_DWR_CD, DT, EMP_CD_CSHR, CURR_CD, ")
                qry.Append("ONE_CNT, TWO_CNT, FIVE_CNT, TEN_CNT, TWENTY_CNT, FIFTY_CNT, ")
                qry.Append("ONE_HUNDRED_CNT, ONE_THOU_CNT, PENNIES_CNT, NICKELS_CNT, ")
                qry.Append("DIMES_CNT, QUARTERS_CNT, HALF_DOL_CNT, ONE_COIN_CNT, ")
                qry.Append("DEP_BAG, DEP_SLIP_NUM , LST_ACT_DT_TIME) VALUES ")
                qry.Append("(:CO_CD, :STORE_CD, :CSH_DWR_CD, :DT, :EMP_CD_CSHR, :CURR_CD, ")
                qry.Append(":ONE_CNT, :TWO_CNT, :FIVE_CNT, :TEN_CNT, :TWENTY_CNT, :FIFTY_CNT, ")
                qry.Append(":ONE_HUNDRED_CNT, :ONE_THOU_CNT, :PENNIES_CNT, :NICKELS_CNT, ")
                qry.Append(":DIMES_CNT, :QUARTERS_CNT, :HALF_DOL_CNT, :ONE_COIN_CNT, ")
                qry.Append(":DEP_BAG_CD, :DEP_SLIP_NUM, SYSDATE)")

                Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                    Dim param As DbParameter = cmd.CreateParameter()
                    param.ParameterName = "CO_CD"
                    param.Value = objCdbBal.companyCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "STORE_CD"
                    param.Value = objCdbBal.storeCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "CSH_DWR_CD"
                    param.Value = objCdbBal.cashDrawerCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "DT"
                    param.Value = objCdbBal.transactionDate
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "EMP_CD_CSHR"
                    param.Value = objCdbBal.empCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "CURR_CD"
                    param.Value = objCdbBal.currencyCd
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "ONE_CNT"
                    param.Value = objCdbBal.depositCash.oneCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "TWO_CNT"
                    param.Value = objCdbBal.depositCash.twoCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "FIVE_CNT"
                    param.Value = objCdbBal.depositCash.fiveCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "TEN_CNT"
                    param.Value = objCdbBal.depositCash.tenCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "TWENTY_CNT"
                    param.Value = objCdbBal.depositCash.twentyCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "FIFTY_CNT"
                    param.Value = objCdbBal.depositCash.fiftyCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "ONE_HUNDRED_CNT"
                    param.Value = objCdbBal.depositCash.oneHundredCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "ONE_THOU_CNT"
                    param.Value = objCdbBal.depositCash.oneThousandCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "PENNIES_CNT"
                    param.Value = objCdbBal.depositCash.penniesCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "NICKELS_CNT"
                    param.Value = objCdbBal.depositCash.nickelsCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "DIMES_CNT"
                    param.Value = objCdbBal.depositCash.dimesCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "QUARTERS_CNT"
                    param.Value = objCdbBal.depositCash.quartersCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "HALF_DOL_CNT"
                    param.Value = objCdbBal.depositCash.halfCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "ONE_COIN_CNT"
                    param.Value = objCdbBal.depositCash.oneCoinCount
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "DEP_BAG_CD"
                    param.Value = objCdbBal.depositCash.depositBag
                    cmd.Parameters.Add(param)

                    param = cmd.CreateParameter()
                    param.ParameterName = "DEP_SLIP_NUM"
                    param.Value = objCdbBal.depositCash.depositSlip + 1
                    cmd.Parameters.Add(param)

                    cmd.ExecuteNonQuery()
                End Using
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Function UpdateCashDrawerCash(objCsDwrDepDtc As CashDrawerDepositDtc) As Integer
        Try
            Dim qry As New StringBuilder
            qry.Append("UPDATE CSH_DWR_DEP SET ")
            qry.Append("ONE_CNT = :ONE_CNT, TWO_CNT = :TWO_CNT, FIVE_CNT = :FIVE_CNT, TEN_CNT = :TEN_CNT, ")
            qry.Append("TWENTY_CNT = :TWENTY_CNT, FIFTY_CNT = :FIFTY_CNT, ONE_HUNDRED_CNT = :ONE_HUNDRED_CNT, ")
            qry.Append("ONE_THOU_CNT = :ONE_THOU_CNT, PENNIES_CNT = :PENNIES_CNT, NICKELS_CNT = :NICKELS_CNT, ")
            qry.Append("DIMES_CNT = :DIMES_CNT, QUARTERS_CNT = :QUARTERS_CNT, HALF_DOL_CNT = :HALF_DOL_CNT, ")
            qry.Append("ONE_COIN_CNT = :ONE_COIN_CNT, DEP_BAG = :DEP_BAG_CD, LST_ACT_DT_TIME = SYSDATE ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND DT = :DT ")
            qry.Append("AND EMP_CD_CSHR = :EMP_CD_CSHR AND CURR_CD = :CURR_CD ")
            qry.Append("AND DEP_SLIP_NUM = :DEP_SLIP_NUM ")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCsDwrDepDtc.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCsDwrDepDtc.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCsDwrDepDtc.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = objCsDwrDepDtc.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD_CSHR"
                param.Value = objCsDwrDepDtc.empCdCashier
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = objCsDwrDepDtc.currencyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_CNT"
                param.Value = objCsDwrDepDtc.oneCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TWO_CNT"
                param.Value = objCsDwrDepDtc.twoCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "FIVE_CNT"
                param.Value = objCsDwrDepDtc.fiveCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TEN_CNT"
                param.Value = objCsDwrDepDtc.tenCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TWENTY_CNT"
                param.Value = objCsDwrDepDtc.twentyCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "FIFTY_CNT"
                param.Value = objCsDwrDepDtc.fiftyCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_HUNDRED_CNT"
                param.Value = objCsDwrDepDtc.oneHundredCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_THOU_CNT"
                param.Value = objCsDwrDepDtc.oneThousandCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "PENNIES_CNT"
                param.Value = objCsDwrDepDtc.penniesCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "NICKELS_CNT"
                param.Value = objCsDwrDepDtc.nickelsCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DIMES_CNT"
                param.Value = objCsDwrDepDtc.dimesCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "QUARTERS_CNT"
                param.Value = objCsDwrDepDtc.quartersCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "HALF_DOL_CNT"
                param.Value = objCsDwrDepDtc.halfCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_COIN_CNT"
                param.Value = objCsDwrDepDtc.oneCoinCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DEP_BAG_CD"
                param.Value = objCsDwrDepDtc.depositBag
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DEP_SLIP_NUM"
                param.Value = objCsDwrDepDtc.depositSlip
                cmd.Parameters.Add(param)

                Return cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Function UpdateCDBCash(objCdbBal As CashDrawerBalDtc) As Integer

        Try
            Dim qry As New StringBuilder

            qry.Append("UPDATE CSH_DWR_DEP SET ")
            qry.Append("EMP_CD_CSHR = :EMP_CD_CSHR, ")
            qry.Append("ONE_CNT = :ONE_CNT, TWO_CNT = :TWO_CNT, FIVE_CNT = :FIVE_CNT, TEN_CNT = :TEN_CNT, ")
            qry.Append("TWENTY_CNT = :TWENTY_CNT, FIFTY_CNT = :FIFTY_CNT, ONE_HUNDRED_CNT = :ONE_HUNDRED_CNT, ")
            qry.Append("ONE_THOU_CNT = :ONE_THOU_CNT, PENNIES_CNT = :PENNIES_CNT, NICKELS_CNT = :NICKELS_CNT, ")
            qry.Append("DIMES_CNT = :DIMES_CNT, QUARTERS_CNT = :QUARTERS_CNT, HALF_DOL_CNT = :HALF_DOL_CNT, ")
            qry.Append("ONE_COIN_CNT = :ONE_COIN_CNT, ")
            qry.Append("DEP_SLIP_NUM = :DEP_SLIP_NUM, ")
            qry.Append("DEP_BAG = :DEP_BAG_CD, LST_ACT_DT_TIME = SYSDATE ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND DT = :DT ")
            'qry.Append("AND EMP_CD_CSHR = :EMP_CD_CSHR ")
            qry.Append("AND CURR_CD = :CURR_CD ")
            'qry.Append("AND DEP_SLIP_NUM = :DEP_SLIP_NUM ")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD_CSHR"
                param.Value = objCdbBal.empCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = objCdbBal.currencyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_CNT"
                param.Value = objCdbBal.depositCash.oneCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TWO_CNT"
                param.Value = objCdbBal.depositCash.twoCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "FIVE_CNT"
                param.Value = objCdbBal.depositCash.fiveCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TEN_CNT"
                param.Value = objCdbBal.depositCash.tenCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TWENTY_CNT"
                param.Value = objCdbBal.depositCash.twentyCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "FIFTY_CNT"
                param.Value = objCdbBal.depositCash.fiftyCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_HUNDRED_CNT"
                param.Value = objCdbBal.depositCash.oneHundredCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_THOU_CNT"
                param.Value = objCdbBal.depositCash.oneThousandCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "PENNIES_CNT"
                param.Value = objCdbBal.depositCash.penniesCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "NICKELS_CNT"
                param.Value = objCdbBal.depositCash.nickelsCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DIMES_CNT"
                param.Value = objCdbBal.depositCash.dimesCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "QUARTERS_CNT"
                param.Value = objCdbBal.depositCash.quartersCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "HALF_DOL_CNT"
                param.Value = objCdbBal.depositCash.halfCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_COIN_CNT"
                param.Value = objCdbBal.depositCash.oneCoinCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DEP_BAG_CD"
                param.Value = objCdbBal.depositCash.depositBag
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DEP_SLIP_NUM"
                param.Value = objCdbBal.depositCash.depositSlip
                cmd.Parameters.Add(param)

                Return cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Sub GetDepositSlipNumber(objCsDwrDepDtc As CashDrawerDepositDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT DEP_SLIP_NUM FROM CSH_DWR_DEP ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND DT = :DT ")
            qry.Append("AND EMP_CD_CSHR = :EMP_CD_CSHR AND CURR_CD = :CURR_CD ")
            qry.Append("AND ROWNUM = 1 ")
            qry.Append("ORDER BY DEP_SLIP_NUM DESC")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCsDwrDepDtc.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCsDwrDepDtc.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCsDwrDepDtc.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = objCsDwrDepDtc.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD_CSHR"
                param.Value = objCsDwrDepDtc.empCdCashier
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = objCsDwrDepDtc.currencyCd
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        ParseCashDrawerDeposit(objCsDwrDepDtc, dr)
                    End While
                End Using
            End Using

        Catch ex As Exception

        End Try

    End Sub

    Private Sub GetDepSlipNumber(objCdbBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT DEP_SLIP_NUM FROM CSH_DWR_DEP ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND DT = :DT ")
            qry.Append("AND EMP_CD_CSHR = :EMP_CD_CSHR AND CURR_CD = :CURR_CD ")
            qry.Append("AND ROWNUM = 1 ")
            qry.Append("ORDER BY DEP_SLIP_NUM DESC")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD_CSHR"
                param.Value = objCdbBal.empCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = objCdbBal.currencyCd
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        ParseCashDrawerDeposit(objCdbBal.depositCash, dr)
                    End While
                End Using
            End Using

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ParseCashDrawerDeposit(objCsDwrDepDtc As CashDrawerDepositDtc, dr As IDataReader)
        If ColumnExists(dr, "CO_CD") Then
            objCsDwrDepDtc.companyCd = MyBase.GetDataValue(Of String)(dr, "CO_CD")
        End If
        If ColumnExists(dr, "STORE_CD") Then
            objCsDwrDepDtc.storeCd = MyBase.GetDataValue(Of String)(dr, "STORE_CD")
        End If
        If ColumnExists(dr, "CSH_DWR_CD") Then
            objCsDwrDepDtc.cashDrawerCd = MyBase.GetDataValue(Of String)(dr, "CSH_DWR_CD")
        End If
        If ColumnExists(dr, "DT") Then
            objCsDwrDepDtc.transactionDate = MyBase.GetDataValue(Of String)(dr, "DT")
        End If
        If ColumnExists(dr, "EMP_CD_CSHR") Then
            objCsDwrDepDtc.empCdCashier = MyBase.GetDataValue(Of String)(dr, "EMP_CD_CSHR")
        End If
        If ColumnExists(dr, "CURR_CD") Then
            objCsDwrDepDtc.currencyCd = MyBase.GetDataValue(Of String)(dr, "CURR_CD")
        End If
        If ColumnExists(dr, "ONE_CNT") Then
            objCsDwrDepDtc.oneCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_CNT")
        End If
        If ColumnExists(dr, "TWO_CNT") Then
            objCsDwrDepDtc.twoCount = MyBase.GetDataValue(Of Decimal)(dr, "TWO_CNT")
        End If
        If ColumnExists(dr, "FIVE_CNT") Then
            objCsDwrDepDtc.fiveCount = MyBase.GetDataValue(Of Decimal)(dr, "FIVE_CNT")
        End If
        If ColumnExists(dr, "TEN_CNT") Then
            objCsDwrDepDtc.tenCount = MyBase.GetDataValue(Of Decimal)(dr, "TEN_CNT")
        End If
        If ColumnExists(dr, "TWENTY_CNT") Then
            objCsDwrDepDtc.twentyCount = MyBase.GetDataValue(Of Decimal)(dr, "TWENTY_CNT")
        End If
        If ColumnExists(dr, "FIFTY_CNT") Then
            objCsDwrDepDtc.fiftyCount = MyBase.GetDataValue(Of Decimal)(dr, "FIFTY_CNT")
        End If
        If ColumnExists(dr, "ONE_HUNDRED_CNT") Then
            objCsDwrDepDtc.oneHundredCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_HUNDRED_CNT")
        End If
        If ColumnExists(dr, "ONE_THOU_CNT") Then
            objCsDwrDepDtc.oneThousandCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_THOU_CNT")
        End If
        If ColumnExists(dr, "PENNIES_CNT") Then
            objCsDwrDepDtc.penniesCount = MyBase.GetDataValue(Of Decimal)(dr, "PENNIES_CNT")
        End If
        If ColumnExists(dr, "NICKELS_CNT") Then
            objCsDwrDepDtc.nickelsCount = MyBase.GetDataValue(Of Decimal)(dr, "NICKELS_CNT")
        End If
        If ColumnExists(dr, "DIMES_CNT") Then
            objCsDwrDepDtc.dimesCount = MyBase.GetDataValue(Of Decimal)(dr, "DIMES_CNT")
        End If
        If ColumnExists(dr, "QUARTERS_CNT") Then
            objCsDwrDepDtc.quartersCount = MyBase.GetDataValue(Of Decimal)(dr, "QUARTERS_CNT")
        End If
        If ColumnExists(dr, "HALF_DOL_CNT") Then
            objCsDwrDepDtc.halfCount = MyBase.GetDataValue(Of Decimal)(dr, "HALF_DOL_CNT")
        End If
        If ColumnExists(dr, "ONE_COIN_CNT") Then
            objCsDwrDepDtc.oneCoinCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_COIN_CNT")
        End If
        If ColumnExists(dr, "DEP_BAG") Then
            objCsDwrDepDtc.depositBag = MyBase.GetDataValue(Of String)(dr, "DEP_BAG")
        End If
        If ColumnExists(dr, "DEP_SLIP_NUM") Then
            objCsDwrDepDtc.depositSlip = MyBase.GetDataValue(Of Integer)(dr, "DEP_SLIP_NUM")
        End If
        If ColumnExists(dr, "LST_ACT_DT_TIME") Then
            objCsDwrDepDtc.lastActTime = MyBase.GetDataValue(Of Date)(dr, "LST_ACT_DT_TIME")
        End If
        If ColumnExists(dr, "VERSION") Then
            objCsDwrDepDtc.version = MyBase.GetDataValue(Of Integer)(dr, "VERSION")
        End If
    End Sub

End Class
