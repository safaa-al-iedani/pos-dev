﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text

Public Class EmpDa
    Inherits BaseDac

    Public Sub GetEmpDetail(objEmp As EmpDtc)
        Dim qry As New StringBuilder
        qry.Append("SELECT EMP_INIT, FNAME, LNAME, HOME_STORE_CD, SHIP_TO_STORE_CD FROM EMP ")
        qry.Append("JOIN STORE ON EMP.HOME_STORE_CD = STORE.STORE_CD WHERE EMP_CD = :EMP_CD ")

        Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
            Try
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "EMP_CD"
                param.Value = objEmp.empCd
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        ParseEmpDtc(objEmp, dr)
                    End While

                End Using
            Catch ex As Exception
                'EventLog objLog = new EventLog();
                'objLog.LogError(ex);

                Throw ex
            End Try
        End Using
    End Sub
    Public Function lucy_GetAllCashier(empCd As String, hasAllCDBAccess As Boolean, p_co_cd As String) As EmpDtc()
        'lucy copied from GetAllCashier
        Dim empList As New List(Of EmpDtc)


        Try
            Dim qry As New StringBuilder

            If Not hasAllCDBAccess Then
                qry.Append("SELECT EMP_CD, EMP_INIT, EMP_INIT || ' - ' || FNAME || ' ' || LNAME AS FULL_NAME ")
                qry.Append("FROM EMP WHERE EMP_CD = :EMP_CD")


            Else


                qry.Append("SELECT E.EMP_CD, EMP_INIT, E.EMP_INIT || ' - ' || E.FNAME || ' ' || E.LNAME AS FULL_NAME ")
                qry.Append("FROM EMP E JOIN EMP$EMP_TP ET ON E.EMP_CD = ET.EMP_CD ")
                ' qry.Append("WHERE E.home_store_cd in(select store_cd from store where co_cd in(select co_cd from co_grp where co_grp_cd in (select co_grp_cd from co_grp where co_cd ='BW1')))")
                qry.Append("WHERE ET.EMP_TP_CD = 'CSH'  ")

                'start lucy add
                ' If p_co_cd = "LFL" Then  'commentted 24-oct-14, as per John Luft, the rule is the same as Leon's
                ' qry.Append("AND E.home_store_cd in(select store_cd from store where co_cd in(select co_cd from co_grp where co_grp_cd in (select co_grp_cd from co_grp where co_cd ='LFL' )))")
                qry.Append("AND E.home_store_cd in(select store_cd from store where co_cd='" & p_co_cd & "')")

                ' Else
                ' qry.Append("AND E.home_store_cd in(select store_cd from store where co_cd in(select co_cd from co_grp where co_grp_cd in (select co_grp_cd from co_grp where co_cd ='BW1' )))")
                ' End If

                'end lucy add

                qry.Append("ORDER BY E.EMP_INIT")
            End If
            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                If Not hasAllCDBAccess Then
                    Dim param As DbParameter = cmd.CreateParameter()

                    param.ParameterName = "EMP_CD"
                    param.Value = empCd
                    cmd.Parameters.Add(param)


                End If

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objEmp As New EmpDtc
                        ParseEmpDtc(objEmp, dr)
                        empList.Add(objEmp)
                    End While

                End Using
            End Using

        Catch ex As Exception

        End Try

        Return empList.ToArray

    End Function

    Public Function GetAllCashier(empCd As String, hasAllCDBAccess As Boolean) As EmpDtc()

        Dim empList As New List(Of EmpDtc)

        Try
            Dim qry As New StringBuilder

            If Not hasAllCDBAccess Then
                qry.Append("SELECT EMP_CD, EMP_INIT, EMP_INIT || ' - ' || FNAME || ' ' || LNAME AS FULL_NAME ")
                qry.Append("FROM EMP WHERE EMP_CD = :EMP_CD")
            Else
                qry.Append("SELECT E.EMP_CD, EMP_INIT, E.EMP_INIT || ' - ' || E.FNAME || ' ' || E.LNAME AS FULL_NAME ")
                qry.Append("FROM EMP E JOIN EMP$EMP_TP ET ON E.EMP_CD = ET.EMP_CD ")
                qry.Append("WHERE ET.EMP_TP_CD = 'CSH' ")
                qry.Append("ORDER BY E.EMP_INIT")
            End If

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                If Not hasAllCDBAccess Then
                    Dim param As DbParameter = cmd.CreateParameter()
                    param.ParameterName = "EMP_CD"
                    param.Value = empCd
                    cmd.Parameters.Add(param)
                End If

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objEmp As New EmpDtc
                        ParseEmpDtc(objEmp, dr)
                        empList.Add(objEmp)
                    End While

                End Using
            End Using

        Catch ex As Exception

        End Try

        Return empList.ToArray

    End Function

    Private Sub ParseEmpDtc(objEmp As EmpDtc, dr As IDataReader)
        If ColumnExists(dr, "EMP_CD") Then
            objEmp.empCd = MyBase.GetDataValue(Of String)(dr, "EMP_CD")
        End If
        If ColumnExists(dr, "EMP_INIT") Then
            objEmp.empInit = MyBase.GetDataValue(Of String)(dr, "EMP_INIT")
        End If
        If ColumnExists(dr, "FNAME") Then
            objEmp.firstName = MyBase.GetDataValue(Of String)(dr, "FNAME")
        End If
        If ColumnExists(dr, "LNAME") Then
            objEmp.lastName = MyBase.GetDataValue(Of String)(dr, "LNAME")
        End If
        If ColumnExists(dr, "FULL_NAME") Then
            objEmp.fullName = MyBase.GetDataValue(Of String)(dr, "FULL_NAME")
        End If
        If ColumnExists(dr, "HOME_STORE_CD") Then
            objEmp.homeStoreCd = MyBase.GetDataValue(Of String)(dr, "HOME_STORE_CD")
        End If
        If ColumnExists(dr, "SHIP_TO_STORE_CD") Then
            objEmp.shipToStoreCd = MyBase.GetDataValue(Of String)(dr, "SHIP_TO_STORE_CD")
        End If
    End Sub

End Class
