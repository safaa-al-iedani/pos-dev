﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text
Imports System.Globalization

Public Class CashDrawerDepositCkDa
    Inherits BaseDac

    'Public Function GetCashDrawerCheck(objCsDwrDepCkDtc As CashDrawerDepositCkDtc, empCd As String) As CashDrawerDepositCkDtc()
    '    Dim cdDepCkList As New List(Of CashDrawerDepositCkDtc)
    '    Try
    '        Dim qry As New StringBuilder
    '        qry.Append("SELECT CK_NAME, CK_AMT, SEQ_NUM, VERSION FROM CSH_DWR_DEP_CK ")
    '        qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
    '        qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND DT = :DT AND CURR_CD = :CURR_CD  ") 'AND EMP_CD_CSHR = :EMP_CD_CSHR
    '        'qry.Append("ORDER BY SEQ_NUM DESC")

    '        Using cmd As DbCommand = BuildCommand(qry.ToString)
    '            Dim param As DbParameter = cmd.CreateParameter()
    '            param.ParameterName = "CO_CD"
    '            param.Value = objCsDwrDepCkDtc.companyCd
    '            cmd.Parameters.Add(param)

    '            param = cmd.CreateParameter()
    '            param.ParameterName = "STORE_CD"
    '            param.Value = objCsDwrDepCkDtc.storeCd
    '            cmd.Parameters.Add(param)

    '            param = cmd.CreateParameter()
    '            param.ParameterName = "CSH_DWR_CD"
    '            param.Value = objCsDwrDepCkDtc.cashDrawerCd
    '            cmd.Parameters.Add(param)

    '            param = cmd.CreateParameter()
    '            param.ParameterName = "DT"
    '            param.Value = objCsDwrDepCkDtc.transactionDate
    '            cmd.Parameters.Add(param)

    '            param = cmd.CreateParameter()
    '            param.ParameterName = "CURR_CD"
    '            param.Value = objCsDwrDepCkDtc.currencyCd
    '            cmd.Parameters.Add(param)

    '            'param = cmd.CreateParameter()
    '            'param.ParameterName = "EMP_CD_CSHR"
    '            'param.Value = empCd
    '            'cmd.Parameters.Add(param)

    '            Using dr As IDataReader = BuildReader(cmd)

    '                While dr.Read()
    '                    Dim objcsDepCk As New CashDrawerDepositCkDtc

    '                    objcsDepCk.companyCd = objCsDwrDepCkDtc.companyCd
    '                    objcsDepCk.storeCd = objCsDwrDepCkDtc.storeCd
    '                    objcsDepCk.cashDrawerCd = objCsDwrDepCkDtc.cashDrawerCd
    '                    objcsDepCk.transactionDate = objCsDwrDepCkDtc.transactionDate
    '                    objcsDepCk.currencyCd = objCsDwrDepCkDtc.currencyCd
    '                    objcsDepCk.empCd = objCsDwrDepCkDtc.empCd
    '                    ParseCdDepCheck(objcsDepCk, dr)
    '                    cdDepCkList.Add(objcsDepCk)
    '                End While

    '            End Using
    '        End Using
    '    Catch ex As Exception

    '    End Try
    '    Return cdDepCkList.ToArray
    'End Function

    Public Sub GetCashDrawerCk(objCdbBal As CashDrawerBalDtc)

        'Dim cdDepCkList As New List(Of CashDrawerDepositCkDtc)
        objCdbBal.depositCk = New List(Of CashDrawerDepositCkDtc)

        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT CK_NAME, CK_AMT, SEQ_NUM, VERSION FROM CSH_DWR_DEP_CK ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND DT = :DT AND CURR_CD = :CURR_CD  ") 'AND EMP_CD_CSHR = :EMP_CD_CSHR
            'qry.Append("ORDER BY SEQ_NUM DESC")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = objCdbBal.currencyCd
                cmd.Parameters.Add(param)

                'param = cmd.CreateParameter()
                'param.ParameterName = "EMP_CD_CSHR"
                'param.Value = empCd
                'cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()

                        Dim objcsDepCk As New CashDrawerDepositCkDtc

                        'objcsDepCk.companyCd = objCsDwrDepCkDtc.companyCd
                        'objcsDepCk.storeCd = objCsDwrDepCkDtc.storeCd
                        'objcsDepCk.cashDrawerCd = objCsDwrDepCkDtc.cashDrawerCd
                        'objcsDepCk.transactionDate = objCsDwrDepCkDtc.transactionDate
                        'objcsDepCk.currencyCd = objCsDwrDepCkDtc.currencyCd
                        'objcsDepCk.empCd = objCsDwrDepCkDtc.empCd
                        ParseCdDepCheck(objcsDepCk, dr)
                        'cdDepCkList.Add(objcsDepCk)

                        objCdbBal.depositCk.Add(objcsDepCk)
                    End While

                End Using
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Sub InsertCashDrawerCheck(objCsDwrDepCkList As List(Of CashDrawerDepositCkDtc))
        Try
            If objCsDwrDepCkList.Count > 0 Then
                Dim qryBuild As New StringBuilder
                qryBuild.Append("INSERT ALL ")

                For i As Integer = objCsDwrDepCkList.Count - 1 To 0 Step -1

                    Dim objCsDwrDepCk = objCsDwrDepCkList(i)

                    'qryBuild.Append("INTO CSH_DWR_DEP_CK (CO_CD, STORE_CD, CSH_DWR_CD, DT, CURR_CD, CK_AMT, CK_NAME, SEQ_NUM) ")
                    'qryBuild.Append("VALUES ('" & objCsDwrDepCk.companyCd & "', '" & objCsDwrDepCk.storeCd & "', '" & objCsDwrDepCk.cashDrawerCd & "', ")
                    'qryBuild.Append("TO_DATE('" & objCsDwrDepCk.transactionDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) & "', 'MM/DD/YYYY'),'")
                    'qryBuild.Append(objCsDwrDepCk.currencyCd & "', " & objCsDwrDepCk.checkAmount & ", '" & objCsDwrDepCk.checkName & "'," & i & " ) ")
                Next

                qryBuild.Append("SELECT * FROM dual")

                Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qryBuild.ToString)
                    cmd.ExecuteNonQuery()
                End Using
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub InsertCDBCheck(cashDwr As CashDrawerBalDtc)
        Try
            If cashDwr.depositCk.Count > 0 Then
                Dim qryBuild As New StringBuilder
                qryBuild.Append("INSERT ALL ")

                For i As Integer = cashDwr.depositCk.Count - 1 To 0 Step -1

                    Dim objCsDwrDepCk = cashDwr.depositCk(i)

                    qryBuild.Append("INTO CSH_DWR_DEP_CK (CO_CD, STORE_CD, CSH_DWR_CD, DT, CURR_CD, CK_AMT, CK_NAME, SEQ_NUM) ")
                    qryBuild.Append("VALUES ('" & cashDwr.companyCd & "', '" & cashDwr.storeCd & "', '" & cashDwr.cashDrawerCd & "', ")
                    qryBuild.Append("TO_DATE('" & cashDwr.transactionDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) & "', 'MM/DD/YYYY'),'")
                    qryBuild.Append(cashDwr.currencyCd & "', " & objCsDwrDepCk.checkAmount & ", '" & objCsDwrDepCk.checkName & "'," & i & " ) ")
                Next

                qryBuild.Append("SELECT * FROM dual")

                Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qryBuild.ToString)
                    cmd.ExecuteNonQuery()
                End Using
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub UpdateCashDrawerCheck(objCsDwrDepCk As CashDrawerDepositCkDtc)

    End Sub

    Public Sub DeleteCashDrawerCheck(objCsDwrDepCk As CashDrawerDepositCkDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("DELETE FROM CSH_DWR_DEP_CK WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD AND CSH_DWR_CD = :CSH_DWR_CD ")
            qry.Append("AND DT =  :DT AND CURR_CD = :CURR_CD")

            'Using cmd As DbCommand = BuildCommand(qry.ToString)
            '    Dim param As DbParameter = cmd.CreateParameter()
            '    param.ParameterName = "CO_CD"
            '    param.Value = objCsDwrDepCk.companyCd
            '    cmd.Parameters.Add(param)

            '    param = cmd.CreateParameter()
            '    param.ParameterName = "STORE_CD"
            '    param.Value = objCsDwrDepCk.storeCd
            '    cmd.Parameters.Add(param)

            '    param = cmd.CreateParameter()
            '    param.ParameterName = "CSH_DWR_CD"
            '    param.Value = objCsDwrDepCk.cashDrawerCd
            '    cmd.Parameters.Add(param)

            '    param = cmd.CreateParameter()
            '    param.ParameterName = "DT"
            '    param.Value = objCsDwrDepCk.transactionDate
            '    cmd.Parameters.Add(param)

            '    param = cmd.CreateParameter()
            '    param.ParameterName = "CURR_CD"
            '    param.Value = objCsDwrDepCk.currencyCd
            '    cmd.Parameters.Add(param)

            '    cmd.ExecuteNonQuery()
            'End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Sub DeleteCDBCheck(cashDwr As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("DELETE FROM CSH_DWR_DEP_CK WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD AND CSH_DWR_CD = :CSH_DWR_CD ")
            qry.Append("AND DT =  :DT AND CURR_CD = :CURR_CD")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = cashDwr.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = cashDwr.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cashDwr.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = cashDwr.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = cashDwr.currencyCd
                cmd.Parameters.Add(param)

                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ParseCdDepCheck(objCsDwrDepCkDtc As CashDrawerDepositCkDtc, dr As IDataReader)
        'If ColumnExists(dr, "CO_CD") Then
        '    objCsDwrDepCkDtc.companyCd = MyBase.GetDataValue(Of String)(dr, "CO_CD")
        'End If
        'If ColumnExists(dr, "EMP_CD_CSHR") Then
        '    objCsDwrDepCkDtc.empCd = MyBase.GetDataValue(Of String)(dr, "EMP_CD_CSHR")
        'End If
        'If ColumnExists(dr, "STORE_CD") Then
        '    objCsDwrDepCkDtc.storeCd = MyBase.GetDataValue(Of String)(dr, "STORE_CD")
        'End If
        'If ColumnExists(dr, "CSH_DWR_CD") Then
        '    objCsDwrDepCkDtc.cashDrawerCd = MyBase.GetDataValue(Of String)(dr, "CSH_DWR_CD")
        'End If
        'If ColumnExists(dr, "DT") Then
        '    objCsDwrDepCkDtc.transactionDate = MyBase.GetDataValue(Of Date)(dr, "DT")
        'End If
        'If ColumnExists(dr, "CURR_CD") Then
        '    objCsDwrDepCkDtc.currencyCd = MyBase.GetDataValue(Of String)(dr, "CURR_CD")
        'End If
        If ColumnExists(dr, "CK_AMT") Then
            objCsDwrDepCkDtc.checkAmount = MyBase.GetDataValue(Of Decimal)(dr, "CK_AMT")
        End If
        If ColumnExists(dr, "CK_NAME") Then
            objCsDwrDepCkDtc.checkName = MyBase.GetDataValue(Of String)(dr, "CK_NAME")
        End If
        If ColumnExists(dr, "SEQ_NUM") Then
            objCsDwrDepCkDtc.seqenceNo = MyBase.GetDataValue(Of Decimal)(dr, "SEQ_NUM")
        End If
        If ColumnExists(dr, "VERSION") Then
            objCsDwrDepCkDtc.version = MyBase.GetDataValue(Of Decimal)(dr, "VERSION")
        End If
    End Sub

End Class
