﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text

Public Class CashDrawerDa
    Inherits BaseDac

    Public Function GetAllCashDrawer(storeCd As String, filterText As String) As CashDrawerDtc()
        Dim CashDrawerList As New List(Of CashDrawerDtc)

        Dim qry As New StringBuilder

        Try
            qry.Append("SELECT CSH_DWR_CD, DES CSH_DWR_DES , BILL_CD FROM CSH_DWR WHERE STORE_CD = :STORE_CD ")

            If Not String.IsNullOrEmpty(filterText) Then
                qry.Append("AND CSH_DWR_CD LIKE '%" + filterText + "%' ")
            End If

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = storeCd
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objCashDrawer As New CashDrawerDtc
                        ParseCashDrawerObj(objCashDrawer, dr)
                        CashDrawerList.Add(objCashDrawer)
                    End While
                End Using
            End Using
        Catch ex As Exception

        End Try

        Return CashDrawerList.ToArray
    End Function

    Private Sub ParseCashDrawerObj(objCashDrawer As CashDrawerDtc, dr As IDataReader)
        If ColumnExists(dr, "CSH_DWR_CD") Then
            objCashDrawer.cashDrawerCd = MyBase.GetDataValue(Of String)(dr, "CSH_DWR_CD")
        End If
        If ColumnExists(dr, "CSH_DWR_DES") Then
            objCashDrawer.cashDrawerDesc = MyBase.GetDataValue(Of String)(dr, "CSH_DWR_DES")
        End If
        If ColumnExists(dr, "BILL_CD") Then
            objCashDrawer.billCd = MyBase.GetDataValue(Of String)(dr, "BILL_CD")
        End If
    End Sub

End Class
