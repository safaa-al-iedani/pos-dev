﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text

Public Class StoreDa
    Inherits BaseDac

    Public Function GetAllStore(empCd As String, hasAllStoreAccess As Boolean, filterText As String) As StoreDtc()

        Dim storeList As New List(Of StoreDtc)

        Dim qry As New StringBuilder
        qry.Append("SELECT STORE.STORE_CD, STORE.CO_CD, STORE.STORE_NAME AS STORE_DES,STORE.BNK_CD,STORE.BNK_ACCT_CD ")
        qry.Append(", NVL(STORE.MULT_CURR, 'N') MULT_CURR, NVL(STORE.DEF_CURR_CD, '')  DEF_CURR_CD ")
        qry.Append(", BNK.TRANS_ROUTE, BNK_ACCT.BNK_ACCT_NUM ")
        qry.Append("FROM STORE LEFT JOIN BNK ON STORE.BNK_CD = BNK.BNK_CD ")
        qry.Append("LEFT JOIN BNK_ACCT ON STORE.BNK_ACCT_CD = BNK_ACCT.BNK_ACCT_CD ")

        If Not String.IsNullOrEmpty(filterText) Then
            qry.Append("WHERE STORE.STORE_CD LIKE '%" + filterText + "%' ")
        End If

        If hasAllStoreAccess Then
            If String.IsNullOrEmpty(filterText) Then
                qry.Append("WHERE ")
            Else
                qry.Append("AND ")
            End If

            qry.Append("STORE.STORE_CD IN (SELECT STORE_GRP$STORE.STORE_CD FROM STORE_GRP$STORE, EMP2STORE_GRP ")
            qry.Append("WHERE EMP2STORE_GRP.STORE_GRP_CD=STORE_GRP$STORE.STORE_GRP_CD AND EMP2STORE_GRP.EMP_CD = :EMP_CD) ")
        End If

        qry.Append("ORDER BY STORE.STORE_CD")

        Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
            Try
                If hasAllStoreAccess Then
                    Dim param As DbParameter = cmd.CreateParameter()
                    param.ParameterName = "EMP_CD"
                    param.Value = empCd
                    cmd.Parameters.Add(param)
                End If

                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        Dim objStore As New StoreDtc
                        ParseStoreDtc(objStore, dr)
                        storeList.Add(objStore)
                    End While

                End Using
            Catch ex As Exception
                'EventLog objLog = new EventLog();
                'objLog.LogError(ex);

                Throw ex
            End Try
        End Using
        Return storeList.ToArray
    End Function

    Private Sub ParseStoreDtc(objStore As StoreDtc, dr As IDataReader)
        If ColumnExists(dr, "STORE_CD") Then
            objStore.storeCd = MyBase.GetDataValue(Of String)(dr, "STORE_CD")
        End If
        If ColumnExists(dr, "CO_CD") Then
            objStore.companyCd = MyBase.GetDataValue(Of String)(dr, "CO_CD")
        End If
        If ColumnExists(dr, "STORE_DES") Then
            objStore.storeName = MyBase.GetDataValue(Of String)(dr, "STORE_DES")
        End If
        If ColumnExists(dr, "MULT_CURR") Then
            objStore.multiCurrency = MyBase.GetDataValue(Of String)(dr, "MULT_CURR")
        End If
        If ColumnExists(dr, "DEF_CURR_CD") Then
            objStore.defCurrCd = MyBase.GetDataValue(Of String)(dr, "DEF_CURR_CD")
        End If
        If ColumnExists(dr, "TRANS_ROUTE") Then
            objStore.transRoute = MyBase.GetDataValue(Of String)(dr, "TRANS_ROUTE")
        End If
        If ColumnExists(dr, "BNK_ACCT_NUM") Then
            objStore.bankAccNo = MyBase.GetDataValue(Of String)(dr, "BNK_ACCT_NUM")
        End If
        If ColumnExists(dr, "BNK_ACCT_CD") Then
            objStore.bankAccCd = MyBase.GetDataValue(Of String)(dr, "BNK_ACCT_CD")
        End If
        If ColumnExists(dr, "BNK_CD") Then
            objStore.bankCd = MyBase.GetDataValue(Of String)(dr, "BNK_CD")
        End If
    End Sub

End Class
