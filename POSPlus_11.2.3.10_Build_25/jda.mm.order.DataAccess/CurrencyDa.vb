﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text

Public Class CurrencyDa
    Inherits BaseDac

    Public Function GetAllCurrency(multiCrr As String, defCurrCd As String) As CurrencyDtc()

        Dim currencyList As New List(Of CurrencyDtc)
        Dim result As Boolean = False

        Try

            Dim qry As New StringBuilder
            qry.Append("SELECT CURRENCY_CD, EX_RATE, CUBE_RATE, CURR_DES, LST_CHNG_DT FROM CURRENCY_RATES ")

            If Not multiCrr.Equals("Y") AndAlso Not defCurrCd.Equals("") Then
                qry.Append("WHERE CURRENCY_CD = :CURRENCY_CD ")
            End If

            qry.Append("ORDER BY CURRENCY_CD ")

            Using db As DbConnection = erpDB(), cmdSub As DbCommand = BuildCommand(db, qry.ToString)
                If Not multiCrr.Equals("Y") AndAlso Not defCurrCd.Equals("") Then
                    Dim param As DbParameter = cmdSub.CreateParameter()
                    param.ParameterName = "CURRENCY_CD"
                    param.Value = defCurrCd
                    cmdSub.Parameters.Add(param)
                End If

                Using dr As IDataReader = BuildReader(cmdSub)

                    While dr.Read()
                        Dim objCurr As New CurrencyDtc
                        ParseCurrency(objCurr, dr)
                        currencyList.Add(objCurr)
                    End While

                End Using
            End Using

        Catch ex As Exception

        End Try

        Return currencyList.ToArray

    End Function
    Public Function GetDefaultCurrenctFromCurrencyRates() As CurrencyDtc

        Dim currency As New CurrencyDtc
        Dim objCurr As New CurrencyDtc

        Try

            Dim qry As New StringBuilder
            qry.Append(" SELECT CURRENCY_CD, EX_RATE, CUBE_RATE, CURR_DES, LST_CHNG_DT FROM CURRENCY_RATES WHERE DEF_CURR_FLG='Y' and rownum = 1  ")
            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Using dr As IDataReader = BuildReader(cmd)

                    While dr.Read()
                        ParseCurrency(objCurr, dr)
                    End While
                End Using
            End Using

        Catch ex As Exception

        End Try
        Return objCurr

    End Function


    Private Sub ParseCurrency(objCurr As CurrencyDtc, dr As IDataReader)
        If ColumnExists(dr, "CURRENCY_CD") Then
            objCurr.currencyCd = MyBase.GetDataValue(Of String)(dr, "CURRENCY_CD")
        End If
        If ColumnExists(dr, "CURR_DES") Then
            objCurr.currencyDesc = MyBase.GetDataValue(Of String)(dr, "CURR_DES")
        End If
        If ColumnExists(dr, "EX_RATE") Then
            objCurr.exchangeRate = MyBase.GetDataValue(Of Decimal)(dr, "EX_RATE")
        End If
        If ColumnExists(dr, "CUBE_RATE") Then
            objCurr.cubeRate = MyBase.GetDataValue(Of Decimal)(dr, "CUBE_RATE")
        End If
        If ColumnExists(dr, "LST_CHNG_DT") Then
            objCurr.lastChangeDate = MyBase.GetDataValue(Of Date)(dr, "LST_CHNG_DT")
        End If
    End Sub

End Class
