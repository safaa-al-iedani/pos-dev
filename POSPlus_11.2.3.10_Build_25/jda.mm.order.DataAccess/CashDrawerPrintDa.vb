﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text
Imports System.Globalization
Public Class CashDrawerPrintDa
    Inherits BaseDac

    Public Sub SavePrintData(cashDrawer As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder

            qry.Append("INSERT INTO CSH_DWR_DEP_PRT (STORE_CD, CSH_DWR_CD,DT,CURR_CD, CO_CD,DEP_SLIP_NUM,EMP_CD,PRT_DT_TIME) ")
            qry.Append("VALUES (:STORE_CD, :CSH_DWR_CD, :DT, :CURR_CD, :CO_CD, :DEP_SLIP_NUM, :EMP_CD, SYSDATE)")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = cashDrawer.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cashDrawer.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = cashDrawer.currencyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = cashDrawer.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DEP_SLIP_NUM"
                param.Value = cashDrawer.depositCash.depositSlip
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD"
                param.Value = cashDrawer.empCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = cashDrawer.transactionDate
                cmd.Parameters.Add(param)

                cmd.ExecuteNonQuery()

                GetPrintCount(cashDrawer)

            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Sub GetPrintCount(objCdbBal As CashDrawerBalDtc)

        Try
            Dim qry As New StringBuilder

            qry.Append("SELECT COUNT(*) PRT_CNT FROM CSH_DWR_DEP_PRT ")
            qry.Append("WHERE STORE_CD = :STORE_CD AND CSH_DWR_CD = :CSH_DWR_CD ")
            qry.Append("AND DT = :DT AND CO_CD = :CO_CD")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString)
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        objCdbBal.printCount = Int(MyBase.GetDataValue(Of Decimal)(dr, "PRT_CNT"))
                    End While
                End Using
            End Using

        Catch ex As Exception

        End Try
    End Sub
End Class
