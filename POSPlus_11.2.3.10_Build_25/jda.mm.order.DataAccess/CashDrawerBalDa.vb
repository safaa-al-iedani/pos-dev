﻿Imports jda.mm.order.DataTransferClass
Imports System.Data.Common
Imports System.Text
Imports System.Globalization

Public Class CashDrawerBalDa
    Inherits BaseDac

    Public Sub IsCashDrawerOpen(objCdbBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT  CSHR_INIT ")
            qry.Append("FROM CSHR_ACTIVATION ")
            qry.Append("WHERE       CO_CD = :CO_CD ")
            qry.Append("AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD ")
            qry.Append("AND POST_DT = :POST_DT ")
            qry.Append("AND CSHR_INIT = :CSHR_INIT ")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSHR_INIT"
                param.Value = objCdbBal.empInit
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        objCdbBal.isOpen = True
                    End While
                End Using
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Function GetCashDrawerCash(companyCd As String, storeCd As String, cashDrawerCd As String, transactionDate As Date) As CashDrawerCashCountDtc()
        Dim CashDrawerCashCountDtcList As New List(Of CashDrawerCashCountDtc)

        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT COUNT_TYPE, EMP_CD_CSHR, CURR_CD, ONE_CNT, TWO_CNT, FIVE_CNT, TEN_CNT, ")
            qry.Append("TWENTY_CNT, FIFTY_CNT, ONE_HUNDRED_CNT, ONE_THOU_CNT, ")
            qry.Append("PENNIES_CNT, NICKELS_CNT, DIMES_CNT, QUARTERS_CNT, ")
            qry.Append("HALF_DOL_CNT, ONE_COIN_CNT, DEP_BAG, DEP_SLIP_NUM ")
            qry.Append("FROM CSH_DWR_DEP ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD AND CSH_DWR_CD= :CSH_DWR_CD AND DT = :DT")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = transactionDate
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        Dim objCashDrawerCashCount As New CashDrawerCashCountDtc
                        ParseDrawerCashCount(objCashDrawerCashCount, dr)
                        CashDrawerCashCountDtcList.Add(objCashDrawerCashCount)
                    End While
                End Using

            End Using
        Catch ex As Exception

        End Try

        Return CashDrawerCashCountDtcList.ToArray
    End Function

    Public Sub GetOpenBalance(objCdbBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT NVL(OPEN_BALANCE,0) OPEN_BALANCE, CLOSED FROM CSH_DWR_BAL ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD AND CSH_DWR_CD = :CSH_DWR_CD AND POST_DT = :POST_DT AND CSHR_INIT =:CSHR_INIT ")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSHR_INIT"
                param.Value = objCdbBal.empInit
                cmd.Parameters.Add(param)

                'objCdbBal.isOpen = False
                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        ParseCdb(objCdbBal, dr)
                        'objCdbBal.isOpen = True
                    End While
                End Using

            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Sub GetCloseBalance(objCdbBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("SELECT CLOSE_BALANCE FROM (")
            qry.Append("SELECT NVL (CLOSE_BALANCE, 0) CLOSE_BALANCE ")
            qry.Append("FROM CSH_DWR_BAL WHERE CO_CD = :CO_CD ")
            qry.Append("AND STORE_CD = :STORE_CD AND CSH_DWR_CD = :CSH_DWR_CD ")
            qry.Append("AND POST_DT < :POST_DT AND NVL (CLOSED, 'N') IN ('S', 'F') ")
            qry.Append("ORDER BY POST_DT DESC")
            qry.Append(") WHERE ROWNUM = 1")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCdbBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCdbBal.transactionDate
                cmd.Parameters.Add(param)

                Using dr As IDataReader = BuildReader(cmd)
                    While dr.Read()
                        ParseCdb(objCdbBal, dr)
                    End While
                End Using

            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Sub InsertCashDrawerCash(objCdbCash As CashDrawerCashCountDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("INSERT INTO CSH_DWR_DEP ")
            qry.Append("(CO_CD, STORE_CD, CSH_DWR_CD, DT, COUNT_TYPE, EMP_CD_CSHR, CURR_CD, ")
            qry.Append("ONE_CNT, TWO_CNT, FIVE_CNT, TEN_CNT, TWENTY_CNT, FIFTY_CNT, ONE_HUNDRED_CNT, ONE_THOU_CNT, ")
            qry.Append("PENNIES_CNT, NICKELS_CNT, DIMES_CNT, QUARTERS_CNT, HALF_DOL_CNT, ONE_COIN_CNT, DEP_BAG, DEP_SLIP_NUM , LST_ACT_DT_TIME) ")
            qry.Append("Values (:CO_CD, :STORE_CD, :CSH_DWR_CD, :DT, :COUNT_TYPE, :EMP_CD_CSHR, :CURR_CD, ")
            qry.Append(":ONE_CNT, :TWO_CNT, :FIVE_CNT, :TEN_CNT, :TWENTY_CNT, :FIFTY_CNT, :ONE_HUNDRED_CNT, :ONE_THOU_CNT, ")
            qry.Append(":PENNIES_CNT, :NICKELS_CNT, :DIMES_CNT, :QUARTERS_CNT, :HALF_DOL_CNT, :ONE_COIN_CNT, :DEP_BAG_CD, :DEP_SLIP_NUM, SYSDATE)")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbCash.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCdbCash.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbCash.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = objCdbCash.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "COUNT_TYPE"
                param.Value = objCdbCash.countType
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD_CSHR"
                param.Value = objCdbCash.empCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = objCdbCash.currencyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_CNT"
                param.Value = objCdbCash.oneCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TWO_CNT"
                param.Value = objCdbCash.twoCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "FIVE_CNT"
                param.Value = objCdbCash.fiveCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TEN_CNT"
                param.Value = objCdbCash.tenCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TWENTY_CNT"
                param.Value = objCdbCash.twentyCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "FIFTY_CNT"
                param.Value = objCdbCash.fiftyCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_HUNDRED_CNT"
                param.Value = objCdbCash.oneHundredCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_THOU_CNT"
                param.Value = objCdbCash.oneThousandCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "PENNIES_CNT"
                param.Value = objCdbCash.penniesCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "NICKELS_CNT"
                param.Value = objCdbCash.nickelsCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DIMES_CNT"
                param.Value = objCdbCash.dimesCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "QUARTERS_CNT"
                param.Value = objCdbCash.quartersCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "HALF_DOL_CNT"
                param.Value = objCdbCash.halfCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_COIN_CNT"
                param.Value = objCdbCash.oneCoinCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DEP_BAG_CD"
                param.Value = objCdbCash.depositBag
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DEP_SLIP_NUM"
                param.Value = objCdbCash.depositSlip
                cmd.Parameters.Add(param)

                cmd.ExecuteNonQuery()

            End Using

        Catch ex As Exception

        End Try
    End Sub

    Public Sub UpdateCashDrawerCash(objCdbCash As CashDrawerCashCountDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("UPDATE CSH_DWR_DEP ")
            qry.Append("SET COUNT_TYPE = :COUNT_TYPE, EMP_CD_CSHR = :EMP_CD_CSHR, CURR_CD = :CURR_CD, ")
            qry.Append("ONE_CNT = :ONE_CNT, TWO_CNT = :TWO_CNT, FIVE_CNT = :FIVE_CNT, TEN_CNT = :TEN_CNT, ")
            qry.Append("TWENTY_CNT = :TWENTY_CNT, FIFTY_CNT = :FIFTY_CNT, ONE_HUNDRED_CNT = :ONE_HUNDRED_CNT, ")
            qry.Append("ONE_THOU_CNT = :ONE_THOU_CNT, PENNIES_CNT = :PENNIES_CNT, NICKELS_CNT = :NICKELS_CNT, ")
            qry.Append("DIMES_CNT = :DIMES_CNT, QUARTERS_CNT = :QUARTERS_CNT, HALF_DOL_CNT = :HALF_DOL_CNT, ONE_COIN_CNT = :ONE_COIN_CNT, ")
            qry.Append("DEP_BAG = :DEP_BAG_CD, DEP_SLIP_NUM = :DEP_SLIP_NUM, LST_ACT_DT_TIME = SYSDATE ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD AND CSH_DWR_CD = :CSH_DWR_CD AND DT = :DT")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCdbCash.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCdbCash.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCdbCash.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DT"
                param.Value = objCdbCash.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "COUNT_TYPE"
                param.Value = objCdbCash.countType
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD_CSHR"
                param.Value = objCdbCash.empCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CURR_CD"
                param.Value = objCdbCash.currencyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_CNT"
                param.Value = objCdbCash.oneCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TWO_CNT"
                param.Value = objCdbCash.twoCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "FIVE_CNT"
                param.Value = objCdbCash.fiveCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TEN_CNT"
                param.Value = objCdbCash.tenCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "TWENTY_CNT"
                param.Value = objCdbCash.twentyCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "FIFTY_CNT"
                param.Value = objCdbCash.fiftyCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_HUNDRED_CNT"
                param.Value = objCdbCash.oneHundredCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_THOU_CNT"
                param.Value = objCdbCash.oneThousandCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "PENNIES_CNT"
                param.Value = objCdbCash.penniesCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "NICKELS_CNT"
                param.Value = objCdbCash.nickelsCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DIMES_CNT"
                param.Value = objCdbCash.dimesCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "QUARTERS_CNT"
                param.Value = objCdbCash.quartersCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "HALF_DOL_CNT"
                param.Value = objCdbCash.halfCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "ONE_COIN_CNT"
                param.Value = objCdbCash.oneCoinCount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DEP_BAG_CD"
                param.Value = objCdbCash.depositBag
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "DEP_SLIP_NUM"
                param.Value = objCdbCash.depositSlip
                cmd.Parameters.Add(param)

                cmd.ExecuteNonQuery()

            End Using

        Catch ex As Exception

        End Try
    End Sub

    Public Sub UpdateCloseDrawerBalance(objCashDrawerBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("UPDATE CSH_DWR_BAL SET ")
            qry.Append("CLOSED = DECODE (:CLOSED, 'N', 'F', 'Y'), ")
            qry.Append("BALANCED = :BALANCED, CLOSE_TIME = SYSDATE,")
            qry.Append("CLOSE_BALANCE = :CLOSE_BALANCE, EMP_CD_OP = :EMP_CD ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND POST_DT = :POST_DT")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCashDrawerBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCashDrawerBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCashDrawerBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCashDrawerBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CLOSED"
                param.Value = objCashDrawerBal.closed
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "BALANCED"
                param.Value = objCashDrawerBal.balanced
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CLOSE_BALANCE"
                param.Value = objCashDrawerBal.closeBalance
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD"
                param.Value = objCashDrawerBal.empCd
                cmd.Parameters.Add(param)

                Dim a As Integer = cmd.ExecuteNonQuery()

                If a = 0 Then
                    InsertCloseDrawerBalance(objCashDrawerBal)
                End If
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Sub UpdateOpenDrawerBalance(objCashDrawerBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("UPDATE CSH_DWR_BAL SET ")
            qry.Append("EMP_CD_OP = :EMP_CD,OPEN_BALANCE =:OPEN_BALANCE ")
            qry.Append("WHERE CO_CD = :CO_CD AND STORE_CD = :STORE_CD ")
            qry.Append("AND CSH_DWR_CD = :CSH_DWR_CD AND POST_DT = :POST_DT")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCashDrawerBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCashDrawerBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCashDrawerBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCashDrawerBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "OPEN_BALANCE"
                param.Value = objCashDrawerBal.openBalance
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD"
                param.Value = objCashDrawerBal.empCd
                cmd.Parameters.Add(param)

                Dim a As Integer = cmd.ExecuteNonQuery()

                If a = 0 Then
                    InsertCloseDrawerBalance(objCashDrawerBal)
                End If
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Sub InsertCloseDrawerBalance(objCashDrawerBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("INSERT INTO CSH_DWR_BAL (")
            qry.Append("CO_CD, STORE_CD, CSH_DWR_CD, CSHR_INIT, POST_DT, OPEN_TIME, OPEN_BALANCE,")
            qry.Append("BALANCED, CLOSED, CLOSE_TIME, CLOSE_BALANCE, EMP_CD_OP) VALUES (")
            qry.Append(":CO_CD, :STORE_CD, :CSH_DWR_CD, :CSHR_INIT, :POST_DT, SYSDATE, :OPEN_BALANCE, ")
            qry.Append("DECODE(:BALANCED,'N','F','Y'), :CLOSED, SYSDATE, :CLOSE_BALANCE, :EMP_CD_OP)")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCashDrawerBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCashDrawerBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCashDrawerBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSHR_INIT"
                param.Value = objCashDrawerBal.empCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCashDrawerBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "OPEN_BALANCE"
                param.Value = objCashDrawerBal.openBalance
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "BALANCED"
                param.Value = objCashDrawerBal.balanced
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CLOSED"
                param.Value = objCashDrawerBal.closed
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CLOSE_BALANCE"
                param.Value = objCashDrawerBal.closeBalance
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD_OP"
                param.Value = objCashDrawerBal.empCd
                cmd.Parameters.Add(param)

                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Sub InsertOpenDrawerBalance(objCashDrawerBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("INSERT INTO CSH_DWR_BAL (")
            qry.Append("CO_CD, STORE_CD, CSH_DWR_CD, CSHR_INIT, POST_DT, OPEN_TIME, OPEN_BALANCE,")
            qry.Append("BALANCED, EMP_CD_OP,CLOSED ) VALUES (")
            qry.Append(":CO_CD, :STORE_CD, :CSH_DWR_CD, :CSHR_INIT, :POST_DT, SYSDATE, :OPEN_BALANCE, ")
            qry.Append("DECODE(:BALANCED,'N','F','Y'), :EMP_CD_OP,'N' )")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCashDrawerBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCashDrawerBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCashDrawerBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSHR_INIT"
                param.Value = objCashDrawerBal.empInit
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCashDrawerBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "OPEN_BALANCE"
                param.Value = objCashDrawerBal.openBalance
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "BALANCED"
                param.Value = objCashDrawerBal.balanced
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "EMP_CD_OP"
                param.Value = objCashDrawerBal.empCd
                cmd.Parameters.Add(param)

                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Sub InsertCashDrawerBalMop(objCashDrawerBal As CashDrawerBalDtc)
        Try
            Dim qry As New StringBuilder
            qry.Append("INSERT INTO CSH_DWR_BAL2MOP (CO_CD, STORE_CD, CSH_DWR_CD, POST_DT, MOP_CD, MOP_AMT, OVER_SHORT_AMT) ")
            qry.Append("VALUES (:CO_CD, :STORE_CD, :CSH_DWR_CD, :POST_DT, :MOP_CD, :MOP_AMT, :OVER_SHORT_AMT)")

            Using db As DbConnection = erpDB(), cmd As DbCommand = BuildCommand(db, qry.ToString())
                Dim param As DbParameter = cmd.CreateParameter()
                param.ParameterName = "CO_CD"
                param.Value = objCashDrawerBal.companyCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "STORE_CD"
                param.Value = objCashDrawerBal.storeCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "CSH_DWR_CD"
                param.Value = objCashDrawerBal.cashDrawerCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "POST_DT"
                param.Value = objCashDrawerBal.transactionDate
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "MOP_CD"
                param.Value = objCashDrawerBal.mopCd
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "MOP_AMT"
                param.Value = objCashDrawerBal.mopAmount
                cmd.Parameters.Add(param)

                param = cmd.CreateParameter()
                param.ParameterName = "OVER_SHORT_AMT"
                param.Value = objCashDrawerBal.overShortAmt
                cmd.Parameters.Add(param)

                cmd.ExecuteNonQuery()

            End Using

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ParseDrawerCashCount(objCashDrawerCashCount As CashDrawerCashCountDtc, dr As IDataReader)

        If ColumnExists(dr, "CO_CD") Then
            objCashDrawerCashCount.companyCd = MyBase.GetDataValue(Of String)(dr, "CO_CD")
        End If
        If ColumnExists(dr, "STORE_CD") Then
            objCashDrawerCashCount.storeCd = MyBase.GetDataValue(Of String)(dr, "STORE_CD")
        End If
        If ColumnExists(dr, "CSH_DWR_CD") Then
            objCashDrawerCashCount.cashDrawerCd = MyBase.GetDataValue(Of String)(dr, "CSH_DWR_CD")
        End If
        If ColumnExists(dr, "DT") Then
            objCashDrawerCashCount.transactionDate = MyBase.GetDataValue(Of String)(dr, "DT")
        End If
        If ColumnExists(dr, "COUNT_TYPE") Then
            objCashDrawerCashCount.countType = MyBase.GetDataValue(Of String)(dr, "COUNT_TYPE")
        End If
        If ColumnExists(dr, "EMP_CD") Then
            objCashDrawerCashCount.empCd = MyBase.GetDataValue(Of String)(dr, "EMP_CD")
        End If
        If ColumnExists(dr, "CURR_CD") Then
            objCashDrawerCashCount.currencyCd = MyBase.GetDataValue(Of String)(dr, "CURR_CD")
        End If

        If ColumnExists(dr, "ONE_CNT") Then
            objCashDrawerCashCount.oneCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_CNT")
        End If
        If ColumnExists(dr, "TWO_CNT") Then
            objCashDrawerCashCount.twoCount = MyBase.GetDataValue(Of Decimal)(dr, "TWO_CNT")
        End If
        If ColumnExists(dr, "FIVE_CNT") Then
            objCashDrawerCashCount.fiveCount = MyBase.GetDataValue(Of Decimal)(dr, "FIVE_CNT")
        End If
        If ColumnExists(dr, "TEN_CNT") Then
            objCashDrawerCashCount.tenCount = MyBase.GetDataValue(Of Decimal)(dr, "TEN_CNT")
        End If
        If ColumnExists(dr, "TWENTY_CNT") Then
            objCashDrawerCashCount.twentyCount = MyBase.GetDataValue(Of Decimal)(dr, "TWENTY_CNT")
        End If
        If ColumnExists(dr, "FIFTY_CNT") Then
            objCashDrawerCashCount.fiftyCount = MyBase.GetDataValue(Of Decimal)(dr, "FIFTY_CNT")
        End If
        If ColumnExists(dr, "ONE_HUNDRED_CNT") Then
            objCashDrawerCashCount.oneHundredCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_HUNDRED_CNT")
        End If
        If ColumnExists(dr, "ONE_THOU_CNT") Then
            objCashDrawerCashCount.oneThousandCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_THOU_CNT")
        End If
        If ColumnExists(dr, "PENNIES_CNT") Then
            objCashDrawerCashCount.penniesCount = MyBase.GetDataValue(Of Decimal)(dr, "PENNIES_CNT")
        End If
        If ColumnExists(dr, "NICKELS_CNT") Then
            objCashDrawerCashCount.nickelsCount = MyBase.GetDataValue(Of Decimal)(dr, "NICKELS_CNT")
        End If
        If ColumnExists(dr, "DIMES_CNT") Then
            objCashDrawerCashCount.dimesCount = MyBase.GetDataValue(Of Decimal)(dr, "DIMES_CNT")
        End If
        If ColumnExists(dr, "QUARTERS_CNT") Then
            objCashDrawerCashCount.quartersCount = MyBase.GetDataValue(Of Decimal)(dr, "QUARTERS_CNT")
        End If
        If ColumnExists(dr, "HALF_DOL_CNT") Then
            objCashDrawerCashCount.halfCount = MyBase.GetDataValue(Of Decimal)(dr, "HALF_DOL_CNT")
        End If
        If ColumnExists(dr, "ONE_COIN_CNT") Then
            objCashDrawerCashCount.oneCoinCount = MyBase.GetDataValue(Of Decimal)(dr, "ONE_COIN_CNT")
        End If
        If ColumnExists(dr, "DEP_BAG") Then
            objCashDrawerCashCount.depositBag = MyBase.GetDataValue(Of String)(dr, "DEP_BAG")
        End If
        If ColumnExists(dr, "DEP_SLIP_NUM") Then
            objCashDrawerCashCount.depositSlip = MyBase.GetDataValue(Of String)(dr, "DEP_SLIP_NUM")
        End If
    End Sub

    Private Sub ParseCdb(objCdbBal As CashDrawerBalDtc, dr As IDataReader)
        If ColumnExists(dr, "OPEN_BALANCE") Then
            objCdbBal.openBalance = MyBase.GetDataValue(Of Decimal)(dr, "OPEN_BALANCE")
        End If
        If ColumnExists(dr, "CLOSED") Then
            objCdbBal.closed = MyBase.GetDataValue(Of String)(dr, "CLOSED")
        End If
        If ColumnExists(dr, "CLOSE_BALANCE") Then
            objCdbBal.closeBalance = MyBase.GetDataValue(Of Decimal)(dr, "CLOSE_BALANCE")
        End If
    End Sub
End Class
